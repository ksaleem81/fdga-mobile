//
//  CompleteOrderViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/6/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//
//

/*****************************************NOTE****************************************************/
//For EVENT Controller Same json made "fromEventController" check works some where only
/*****************************************NOTE****************************************************/
import UIKit
import ActionSheetPicker_3_0
import SideMenu
import mangopay
import Braintree
import Stripe



class CompleteOrderOldViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var parentViewHeightConstraint: NSLayoutConstraint!//1120
    @IBOutlet weak var selectPlayerLbl: UILabel!
    @IBOutlet weak var lessonTypLbl: UILabel!
    @IBOutlet weak var monthYearView: MonthYearPickerView!
    @IBOutlet weak var monthYearParentView: UIView!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var durationLblHightCons: NSLayoutConstraint!
    @IBOutlet weak var coachLbl: UILabel!
    @IBOutlet weak var academyLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var costLbl: UILabel!
    var fromRecommnded = false
    var fromEventController = false
    var redeemedData = [String:AnyObject]()
    @IBOutlet weak var fNameFld: UITextField!
    @IBOutlet weak var lNameFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var phonFl: UITextField!
    @IBOutlet weak var userNameFld: UITextField!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var studentTypeLbl: UILabel!
    @IBOutlet weak var paymntFld: UITextField!
    @IBOutlet weak var adressFld: UITextField!
    @IBOutlet weak var cityFld: UITextField!
    @IBOutlet weak var zipFld: UITextField!
    @IBOutlet weak var nameOnCardFld: UITextField!
    @IBOutlet weak var cardNmberFld: UITextField!
    @IBOutlet weak var datePaymntFld: UITextField!
    @IBOutlet weak var ccvFld: UITextField!
    @IBOutlet weak var discountFld: UITextField!
    @IBOutlet weak var paymntViewHeightConstraint: NSLayoutConstraint! //380all,40one,80Two
    @IBOutlet weak var paymentHeaderViewConstraint: NSLayoutConstraint!//30d
    @IBOutlet weak var detailViewHeightConstaint: NSLayoutConstraint!//280
    @IBOutlet weak var dobViewHeightConstraint: NSLayoutConstraint!//80d,35One
    var paymentMethodsArray = [[String:AnyObject]]()
    var paymentMethodsTrack = [[String:AnyObject]]()
    var vochersArray = [[String:AnyObject]]()
    var packagesArray = [[String:AnyObject]]()
    var usersDataArray = [[String:AnyObject]]()
    var ObjectOfMangoPay = [String:AnyObject]()
    var isPaymentSelected = false
    var selectedData = [String:AnyObject]()
    var secondLastControllerData = [String:AnyObject]()
     var previousData = [String:AnyObject]()
    var delegatedDictionary = [String:AnyObject]()
    var currentStudentInfo = [String:AnyObject]()
    var delegatedVocherDic = [String:AnyObject]()
    var delegatedPackageDic = [String:AnyObject]()
    @IBOutlet weak var voucherView: UIView!
    @IBOutlet weak var voucherBtn: UIButton!
    var amountDeductedForMangoPay = ""
    var selectedPaymentType = "0"
    @IBOutlet weak var selectStudentViewHeight: NSLayoutConstraint!
    var currentPlayerSelectedId = ""
    var discountInfoGotOrNot = false
    var discountPercentage = 0.0
    var totalDiscountIfClasesMore = 0.0
    var discountPercentageStr = ""
    var selectedCoachData = [String:AnyObject]()
    var forClassBooking = false
    var cart = [[String:AnyObject]]()
    var totalAmountForClass = 0.0
    var totalAmntForClasHasDiscount = 0.0
    var discountDic = NSDictionary()
    var classIDs = ""
    var programIDs = ""
    var mangoPayPaid = false
    var brainTreePaid = false

    var isRedeemedVoucher = false
    @IBOutlet weak var mangoPayImgHeightCons: NSLayoutConstraint!
    @IBOutlet weak var blakeHeaderHeightCons: NSLayoutConstraint!
    @IBOutlet weak var mangoCheckViewHeightCons: NSLayoutConstraint!
    var mangoPayTermsSelected = false
    @IBOutlet weak var footerHeightViewCons: NSLayoutConstraint!//make 205- 85 to remove
    @IBOutlet weak var termsViewHeightCons: NSLayoutConstraint!//make 0 to remove
    var emailBtnFlage = false
    var privacyBtnFlage = false
    @IBOutlet weak var emailCheckBtn: UIButton!
    @IBOutlet weak var privacyCheckBtn: UIButton!
    var underAgeFlage = false
    var authorizeFlage = false
    @IBOutlet weak var childTermViewHeightCons: NSLayoutConstraint!//168
    @IBOutlet weak var dobChildFld: UITextField!
    @IBOutlet weak var parentFld: UITextField!
    @IBOutlet weak var UnderEighteenBtn: UIButton!
    @IBOutlet weak var authoRizeBtn: UIButton!
    var braintree: BTAPIClient?
    var nonceFromTheClient = ""
    var isStripePaid = false
    var actualPriceOfLesson = ""
    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mangoCheckViewHeightCons.constant = 0
//        print(selectedData)
//        print(previousData)
//        print(secondLastControllerData)
//        print(selectedCoachData)
//        print(redeemedData)
        self.cardNmberFld.delegate = self

        if forClassBooking {
            
            var userId = ""
            if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
                userId = "\(id)"
            }
            var cartKey = ""
            if currentUserLogin == 4 {
                cartKey = "studentCart" +  userId
            }else{
                cartKey = "myCart" + userId
            }
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: cartKey){
             cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: cartKey) as! Data)) as? [[String:AnyObject]])!
            }
        }
        

        settingDefaultView()
        assigningInfoToViews()
        settingRighMenuBtn()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
              
              if range.length > 0
                 {
                     return true
                 }

                 //Don't allow empty strings
                 if string == " "
                 {
                     return false
                 }

                 //Check for max length including the spacers we added
                 if range.location == 20
                 {
                     return false
                 }

                 var originalText = textField.text
      //           let replacementText = string.stringByReplacingOccurrencesOfString(" ", withString: "")
              let replacementText = string.replacingOccurrences(of: " ", with: "")


                 //Verify entered text is a numeric value
      //        let digits = NSCharacterSet.decimalDigitCharacterSet
              let digits = NSCharacterSet.decimalDigits

                 for char in replacementText.unicodeScalars
                 {
                      if digits.contains(char)
                     {
                      }else{
                          return false
                  }
                 }

                 //Put an empty space after every 4 places
                 if originalText!.count % 5 == 0
                 {

      //               originalText?.appendContentsOf(" ")
                  originalText?.append(" ")
                     textField.text = originalText
                 }

                 return true
              
          }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(CompleteOrderOldViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    func assigningInfoToViews()  {
      
        if forClassBooking{
          var programNames = ""
            var coachesNamePlayerLogin = ""
            var i = 0
            for item in cart {
                print(item)
                if let amountFor = item["Price"] as? Double{
                    totalAmountForClass = totalAmountForClass + amountFor
                }
                
                if let prviousData =  cart[i]["previousData"] as? [String:AnyObject]{
                    if let idOFprogram = prviousData["ProgramTypeId"] as? Int{
                         programIDs = programIDs + "\(idOFprogram)" + ","
                    }
                    
                }

                //refix here give recomended class ids
                
                if let idOFClass = item["ClassId"] as? Int{
                    classIDs = classIDs + "\(idOFClass)" + ","
                }
//                if fromRecommnded {
//                if let idOFprogram = item["ProgramTypeId"] as? Int{
//
//                    programIDs = programIDs + "\(idOFprogram)" + ","                }
//                }
                if let days = item["ClassDuration"] as? String{
                    durationLbl.text = "\(days)"
                }
                if let date = item["ClassStartDate"] as? String{
                    
                    let dateOrignal = date.components(separatedBy: "T")
                    if dateOrignal.count > 0 {
                        dateTimeLbl.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
                    }
                }
                
        //code done after isue report by zain if player login to show coaches name on class booking
                if let coachsArray = item["CoachListClasses"] as? [[String:AnyObject]]{
                    for coach in coachsArray {
                        if let coachName = coach["CoachName"] as? String{
                            
                            //check if subtring exhist with same name
                            if coachesNamePlayerLogin.range(of:coachName) != nil {
                            
                                print("exists")
                            }else{
                                coachesNamePlayerLogin = coachesNamePlayerLogin  + coachName + ","
                                
                            }

                        }
                    }
                }
                //till here

                    if let program = item["ClassName"] as? String{
                    programNames = programNames  + program + ","
                    }
                
                i = i + 1
            }
            if classIDs.count > 0 {
                
                classIDs = classIDs.substring(to: classIDs.index(before: classIDs.endIndex))
                programNames = programNames.substring(to: programNames.index(before: programNames.endIndex))

            }
            if programIDs.count > 0 {
                programIDs = programIDs.substring(to: programIDs.index(before: programIDs.endIndex))
            }
            
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                costLbl.text = "COST \(totalAmountForClass) \(currencySign)"

            }else{
                costLbl.text = "COST \(currencySign) \(totalAmountForClass)"
                
            }

               self.lessonTypLbl.text = programNames

            if isRedeemedVoucher{
                if let voucherPin = redeemedData["VoucherPin"] as? String{
                    self.voucherBtn.setTitle(voucherPin, for: .normal)
                }
            }
            
            if currentUserLogin == 4 {
                
                //removing last coma from appending string
                
                if coachesNamePlayerLogin.count > 0 {
                    coachesNamePlayerLogin = coachesNamePlayerLogin.substring(to: coachesNamePlayerLogin.index(before: coachesNamePlayerLogin.endIndex))
                }

                coachLbl.text = "With " + coachesNamePlayerLogin
                
            }else{
                
               var coachName = ""
                if let titl =  DataManager.sharedInstance.currentUser()?["firstName"] as? String{
                    coachName =  titl
                }

                if let titl =  DataManager.sharedInstance.currentUser()?["lastName"] as? String{
                  coachName  =   coachName + " " + titl
                }

                coachLbl.text = "With " + coachName
         
            }

        }
        else{
            //from recommended controller
            if fromRecommnded {
                if let price = selectedData["Price"] as? Double{
                    
                     actualPriceOfLesson = "\(price)"
                    
                    if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                        costLbl.text = "COST \(price) \(currencySign)"
                    }else{
                        costLbl.text = "COST \(currencySign) \(price)"
                        
                    }
                }
                if let title = selectedData["LessonName"] as? String{
                    lessonTypLbl.text = title

                }
                if let duration = selectedData["Duration"] as? String{
                    durationLbl.text =   "\(duration)"
                }
                if let date = selectedData["CoachesNames"] as? String{
                    coachLbl.text = "With " +  date
                }
                var fullTime = ""
                if let time = selectedData["StartTime"] as? String{
                    fullTime =  "\(time)  - "
                }
                if let date = selectedData["LessonDate"] as? String{
                    
                    fullTime = fullTime + DataManager.sharedInstance.getFormatedDate(date:date,formate: globalDateFormate)
                }
                dateTimeLbl.text = fullTime
                
            }else if isRedeemedVoucher{
                
                
                if let durations = redeemedData["Details"] as? String{
                    let components = durations.components(separatedBy: ")")
                    //becasue we select time from controller if (15 mint) (20 mint)
                    if components.count > 2 {
                        if let price = selectedData["Price"] as? String{
                             actualPriceOfLesson = "\(price)"
                            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                                costLbl.text = "COST \(price) \(currencySign)"

                            }else{
                                costLbl.text = "COST \(currencySign) \(price)"
                                
                            }
                        }
                        if let id = secondLastControllerData["Id"] as? Int{
                            durationLbl.text = "\(id) MINUTES"
                        }

                    }else{
                        if let price = redeemedData["Amount"] as? Double{
                            actualPriceOfLesson = "\(price)"

                            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                                costLbl.text = "COST \(price) \(currencySign)"

                            }else{
                                costLbl.text = "COST \(currencySign) \(price)"
                                
                            }
                        }
                        
                        if let id = redeemedData["DurationId"] as? Int{
                            durationLbl.text = "\(id) MINUTES"
                        }

                    }
                }
                
                if let voucherPin = redeemedData["VoucherPin"] as? String{
                    self.voucherBtn.setTitle(voucherPin, for: .normal)
                }
                if let coach = selectedData["CoachName"] as? String{
                    
                    self.coachLbl.text = "With " + coach
                }
                var fullTime = ""
                if let time = selectedData["BookingStartTime"] as? String{
                    fullTime =  "\(time)  - "
                }
                if let date = selectedData["RBookingDate"] as? String{
                    
                    fullTime = fullTime + DataManager.sharedInstance.getFormatedDate(date:date,formate: globalDateFormate)
                }
                dateTimeLbl.text = fullTime
                
                if let program = previousData["ProgramTypeName"] as? String{
                    
                    self.lessonTypLbl.text = program
                }

            }
            else {
                
                if let price = selectedData["Price"] as? String{
                    
                    actualPriceOfLesson = "\(price)"

                    if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                        costLbl.text = "COST \(price) \(currencySign)"

                    }else{
                        costLbl.text = "COST \(currencySign) \(price)"
                        
                    }
                }
                
                if let id = secondLastControllerData["Id"] as? Int{
                    durationLbl.text = "\(id) MINUTES"
                }
    
                if let coach = selectedData["CoachName"] as? String{
                    self.coachLbl.text = "With " + coach
                }
                
                var fullTime = ""
                if let time = selectedData["BookingStartTime"] as? String{
                    fullTime =  "\(time)  - "
                }
                if let date = selectedData["RBookingDate"] as? String{
                    
                    if fromEventController{
                    fullTime = fullTime + DataManager.sharedInstance.getFormatedDate(date:date,formate: "yyyy/MM/dd")
                        
                        var coachName = ""
                        if let titl =  DataManager.sharedInstance.currentUser()?["firstName"] as? String{
                            coachName =  titl
                        }
                        
                        if let titl =  DataManager.sharedInstance.currentUser()?["lastName"] as? String{
                         coachName   =    coachName + " " + titl
                        }
                            coachLbl.text  = "With " + coachName
                        //commented after client reported user name not appearing
//                        if let titl =  DataManager.sharedInstance.currentUser()?["UserName"] as? String{
//                            coachLbl.text = "With " + titl
//                        }
                        
                    }else{
                        fullTime = fullTime + DataManager.sharedInstance.getFormatedDate(date:date,formate: globalDateFormate)
                    }
                }
                dateTimeLbl.text = fullTime
                
                if let program = previousData["ProgramTypeName"] as? String{
                    
                    self.lessonTypLbl.text = program
                }
            }
        }
        
                if let titl =  DataManager.sharedInstance.currentAcademy()!["AcademyName"] as? String{
                    academyLbl.text = "At " + titl
                }

//        if let titl =  DataManager.sharedInstance.currentAcademy()!["AcademyTitle"] as? String{
//            academyLbl.text = "AT " + titl
//        }
        
        
        if cart.count > 1 {
            durationLblHightCons.constant = 32//10
            durationLbl.adjustsFontSizeToFitWidth = true
            durationLbl.lineBreakMode = .byCharWrapping
        }
        
        
    }
    
    func settingDefaultView() {
        
        paymntViewHeightConstraint.constant = 30
        //        hidepassword
        detailViewHeightConstaint.constant = 225//250
        childTermViewHeightCons.constant = 0//forFDGS 45 to 0

        if currentAcademyOnlinePayment == 0 || currentAcademyOnlinePayment == 2{
            self.mangoPayImgHeightCons.constant = 0
            blakeHeaderHeightCons.constant = 180
            parentViewHeightConstraint.constant = 700 + 255//740//Removing Select Is Student Junior option
            //added 85 to add terms View in footer

        }else if currentAcademyOnlinePayment == 1{
            self.mangoPayImgHeightCons.constant = 42
            blakeHeaderHeightCons.constant = 222
            parentViewHeightConstraint.constant = 742  + 255            //added 85 to add terms View in footer

        }
        
       
        if currentUserLogin == 4 {
            selectStudentViewHeight.constant = 0
            getUserInfoIfStudentLogin()
        }else{
            selectStudentViewHeight.constant = 50
            getUsersMthods()
        }
        
        //GDPR
        getPaymntMEthods()
        if currentUserLogin == 4 {
            fillingPlayerGDPRInfo(gdprDic: Utility.GDPRstatusDic)
            getVochers()

        }else{
            self.paymntFld.text = "SELECT PAYMENT"
        }
        
//        getallPackagesOfSelectedPlayer()
    }
    
    func fillingPlayerGDPRInfo(gdprDic:[String:AnyObject])   {
        if let id = gdprDic["RecEmail"] as? Bool,id == true{
            emailBtnFlage = true
            emailCheckBtn.isSelected = true
            
        }else{
            emailBtnFlage = false
            emailCheckBtn.isSelected = false
            
        }
        privacyBtnFlage = true
        privacyCheckBtn.isSelected = true
        
        
        if let id = gdprDic["IsUnderEighteen"] as? Bool,id == true{
            underAgeFlage = true
            UnderEighteenBtn.isSelected = true
            
            
            if let parent = gdprDic["ParentName"] as? String{
                parentFld.text = parent
            }
            
            
            
            if let dob = gdprDic["ChildDob"] as? String{
                
                let dateOrignal = dob.components(separatedBy: "T")
                if dateOrignal.count > 0 {
                    self.dobChildFld.text = DataManager.sharedInstance.getFormatedDateGivenPAttern(date: dateOrignal[0], formate: "yyyy-MM-dd", formateReturn: "dd MMM, yyyy")
                }
            }
            
            view.layoutIfNeeded()
            UIView.animate(withDuration: 1.0, animations: {
                self.childTermViewHeightCons.constant = 148
                self.detailViewHeightConstaint.constant =  285//355
                self.view.layoutIfNeeded()
            })
            
        }else{
            underAgeFlage = false
            UnderEighteenBtn.isSelected = false
            
            
            view.layoutIfNeeded()
            
            UIView.animate(withDuration: 1.0, animations: {
                self.childTermViewHeightCons.constant = 0//forFDGS 45 to 0
                self.detailViewHeightConstaint.constant =  180
                
                self.view.layoutIfNeeded()
            })
            
            
            if currentUserLogin == 4 {
            }else{
                dobChildFld.text = ""
                parentFld.text = ""
                authoRizeBtn.isSelected = false
                authorizeFlage = false
            }
            
        }
        
        
        if let id = gdprDic["ParentApproval"] as? Bool,id == true{
            authorizeFlage = true
            authoRizeBtn.isSelected = true
        }else{
            authorizeFlage = false
            authoRizeBtn.isSelected = false
            
        }
        
    }

    func getUserInfoIfStudentLogin() {
        
        var playerID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["playerId"]! as? Int{
            playerID = "\(userId)"
        }
        
        NetworkManager.performRequest(type:.get, method: "academy/GetPlayerData/\(playerID)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let object2 = object as! [[String:AnyObject]]
            switch object2 {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            if (object2.count) > 0 {
                print(object2[0])
            self.currentStudentInfo = object2[0] 
            self.fillUserInfoIfStudent()
                
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    //MARK:- fetching packages of selected player
    func getallPackagesOfSelectedPlayer() {
        
        //http://app.glfbeta.com/OrbisWebApi/api/Booking/GetUserVoucher?UserId=24515&keyTofind=30&ProgramTypeId=188

        //AcademyID=73&UserId=33989&ProgramTypeID=188&LessonDuration=30&CoachID=4468

        var customeUrl = ""

        var academyID1 = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyID1 = "\(academy)"
        }
        
        var typeID = ""
        if let typeId = previousData["ProgramTypeId"] as? Int{
            typeID = "\(typeId)"
        }

        var userID = ""
        if delegatedDictionary.count > 0 {
            if let userId =  delegatedDictionary["UserId"] as? Int{
                userID = "\(userId)"
            }
        }else{
            if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
                userID = "\(userId)"
            }
        }
        var keyToFind = "0"
        
            if let id = secondLastControllerData["Id"] as? Int{
                keyToFind = "\(id)"
            }
        
        
        if fromRecommnded{
            if let typeId1 = selectedData["ProgramTypeId"] as? Int{
                typeID = "\(typeId1)"
            }
            
            if let id = selectedData["LessonDurationId"] as? Int{
                keyToFind = "\(id)"
            }
        }

        let coachId = getCurrentCoachId()
        
        customeUrl = "AcademyID=\(academyID1)&UserId=\(userID)&ProgramTypeID=\(typeID)&LessonDuration=\(keyToFind)&CoachID=\(coachId)"
        //AcademyID=73&UserId=33989&ProgramTypeID=188&LessonDuration=30&CoachID=4468
        NetworkManager.performRequest(type:.get, method: "Student/GetPlayerAvailabePackages?\(customeUrl)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let object2 = object as! [[String:AnyObject]]
            switch object2 {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
           
            self.packagesArray = object as! [[String : AnyObject]]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    //MARK:- fetching vouchers of selected player

    func getVochers()  {
        
//        AcademyID=73&UserId=33989&ProgramTypeID=188&LessonDuration=30&CoachID=4468
        
        var typeID = ""
        if let typeId = previousData["ProgramTypeId"] as? Int{
            typeID = "\(typeId)"
        }
        
           var userID = ""
        if delegatedDictionary.count > 0 {
            if let userId =  delegatedDictionary["UserId"] as? Int{
                userID = "\(userId)"
            }
        }else{
            if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
            }
        }
        var keyToFind = "0"
        if forClassBooking {
           
            if let id = previousData["ProgramTypeName"] as? String{
                keyToFind = "\(id)"
            }
            
            if cart.count > 0 {
                //                dic["previousData"] = self.previousData as AnyObject?
                //                dic["secondLastControllerData"]
                if let prviousData =  cart[0]["previousData"] as? [String:AnyObject]{
                    if let programId = prviousData["ProgramTypeId"] as? Int{
                        typeID = "\(programId)"
                    }
                    if let id = prviousData["ProgramTypeName"] as? String{
                        keyToFind = "\(id)"
                    }
                }
            }

      
            //remove space to hit url
             keyToFind = keyToFind.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        }else{
        
            if let id = secondLastControllerData["Id"] as? Int{
            keyToFind = "\(id)"
            }
        }
        
        if isRedeemedVoucher {
            if let durations = redeemedData["Details"] as? String{
                let components = durations.components(separatedBy: ")")
            //becasue we select time from controller if (15 mint) (20 mint)
                if components.count > 2 {
                }else{
                    if let durations = redeemedData["DurationId"] as? Int{
                        keyToFind = "\(durations)"
                }
                }
            }
        }
        
        var customeUrl = ""
        if fromRecommnded{
            customeUrl = userID
        }else if fromEventController{
           
            customeUrl = "\(userID)&keyTofind=\(keyToFind)&ProgramTypeId=\(typeID)"
        }else{
            customeUrl = "\(userID)&keyTofind=\(keyToFind)&ProgramTypeId=\(typeID)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Booking/GetUserVoucher?UserId=\(customeUrl)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
                
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                  break
            }
            
            self.vochersArray = object as! [[String : AnyObject]]
            
            if self.vochersArray.count > 0{
                if self.isRedeemedVoucher{
                }else{
                    self.voucherBtn.setTitle("Select Voucher", for: .normal)
                }
            }else{  if self.isRedeemedVoucher{
            }else{
                self.voucherBtn.setTitle("No Voucher Available", for: .normal)
                }
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    //MARK:- fetching payments methods from server
    func getPaymntMEthods()  {
        
        var currentLoginID = ""
        if currentUserLogin == 4 {
            currentLoginID = "4"
        }else{
              currentLoginID = "3"
        }
        NetworkManager.performRequest(type:.get, method: "academy/GetPaymentMethods/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(currentLoginID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.paymentMethodsArray = object as! [[String : AnyObject]]
            for dic in self.paymentMethodsArray{
                if let val = dic["PaymentName"] as? String{
                    
                    if val == "Package"{
                        if self.forClassBooking{
                            
                        }else{
                            self.getallPackagesOfSelectedPlayer()
                            break
                        }
                    }
                }
                
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    //MARK:- fetching all players of coache from server

    func getUsersMthods()  {
        
        var classID = "0"
        if let id = selectedData["ClassId"] as? Int{
            classID = "\(id)" //"\(1188)"//
        }
        
        print(["AcademyId":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"UserId":DataManager.sharedInstance.currentUser()?["Userid"] as AnyObject!,"ClassId":classID as AnyObject!])
        
        NetworkManager.performRequest(type:.post, method: "academy/GetAcademyPlayerList/", parameter:["AcademyId":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"UserId":DataManager.sharedInstance.currentUser()?["Userid"] as AnyObject!,"ClassId":classID as AnyObject!], view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.usersDataArray = object as! [[String : AnyObject]]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    var messageString : String = ""
    
    func validatingTextFields(){
        
        if let text = fNameFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter First \n"
        }
        if let text = lNameFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter Last Name\n"
        }
        
        if let text = emailFld.text , !text.isEmpty{
            if  DataManager.sharedInstance.isValidEmail(self.emailFld.text!){
            }else{
                messageString = "\(messageString) Enter Valid Email\n"
            }
        }else {
            messageString = "\(messageString) Enter Email\n"
        }
    
        if let text = phonFl.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter Phone Number \n"
        }
        
        if delegatedDictionary.count > 0 {
            
        }else{
            if currentUserLogin == 4 {
            //if login is student than skip
            }else{
        if let text = userNameFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter User Name \n"
        }
        
                //                hidepassword

//        if let text = passwordFld.text , !text.isEmpty{
//          messageString =   Utility.passwordStrength(passwordString: passwordFld.text!, messageString1: messageString)
//        }else {
//            messageString = "\(messageString) Enter Password\n"
//        }
                //Removing Select Is Student Junior option
           /*
            if studentTypeLbl.text! == "Is the student a junior (under 18 years)" {
                messageString = "\(messageString) Select Is Student Junior?\n"
            }else if studentTypeLbl.text! == "YES"{
                if let text = dobFld.text , !text.isEmpty{
                }else {
                    messageString = "\(messageString) Enter Date of Birth Of Student\n"
                }
              
                }*/
            }
        }
      
        if selectedPaymentType == "1" {
            
            //updatedstripe
            if currentAcademyOnlinePayment == 2{
            }else{
            
            if let text = adressFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  Address\n"
            }
            if let text = cityFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  City\n"
            }
            if let text = zipFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  Zip Code\n"
            }
            if let text = nameOnCardFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  Name On Card\n"
            }
            
            if let text = cardNmberFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  Card Number\n"
            }
            
            if let text = datePaymntFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  expire Date\n"
            }
            if let text = ccvFld.text , !text.isEmpty{
            }else {
                messageString = "\(messageString) Enter  CVV Code\n"
            }
            
            }
            
                if let text = discountFld.text , !text.isEmpty{
                }else {
                   // messageString = "\(messageString) Enter Discount Code\n"
                
            }
            
            if currentAcademyOnlinePayment == 0 || currentAcademyOnlinePayment == 2{//stripe
            }else{
            if mangoPayTermsSelected {
            }else{
                messageString = "\(messageString) Please Agree Mango Pay Term & Condition Check\n"
                }
            }
            
            }else if selectedPaymentType == "2"{
            

                if  self.voucherBtn.titleLabel?.text! == "Select Voucher" || self.voucherBtn.titleLabel?.text! == "No Voucher Available" {
                    messageString = "\(messageString) Select Voucher\n"
                }

            }    //refix because i exchange placeholder with adress field
            else if selectedPaymentType == "3"{
            //because getting from adress
            if let text = adressFld.text , !text.isEmpty{
            }else {
               // messageString = "\(messageString) Enter Discount Code\n"
            }
        }else if selectedPaymentType == "5" {
            
            if  self.voucherBtn.titleLabel?.text! == "Select Package" || self.voucherBtn.titleLabel?.text! == "No Package Available" {
                messageString = "\(messageString) Select Package\n"
            }

            if let type = self.delegatedPackageDic["PackageType"] as? String{
                
                if type == "1"{
                    if let credits = self.delegatedPackageDic["RemainingCredits"] as? Int{
                    if credits < getLessonTime(){
                    messageString = "\(messageString) The customer needs to top up their credits or choose a shorter duration"
                        //"Package Credit should be greater than or equal to duration"
                        }
                    }
                }else{
                    
                    if let credits = self.delegatedPackageDic["RemainingCredits"] as? Int{
                        if credits < Int(getLessonPrice()){
                            messageString = "\(messageString) Package Credit should be greater than or equal to lesson price"
                        }
                    }
                }
            }
    
        }else if selectedPaymentType == "0" {
            messageString = "\(messageString) Select Payment Method\n"
        }
        else {
            
        }
        
        //GDPR
        if privacyBtnFlage{

        }else{
            messageString = messageString + "Please Agree Terms & Condition Check\n\n"

        }
        

    }
    
    //MARK:- Mango Pay methods

    func gettingCardInfo(price:String,discount:String,parameters:[String:AnyObject])  {
        
        var academyID1 = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyID1 = "\(academy)"
        }
        var playerID = ""
        
        if currentUserLogin == 4 {

            if let playerId1 =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
                playerID = "\(playerId1)"
            }
            
        }else{
            
            if isNewUser() {
                playerID =  self.currentPlayerSelectedId
            }
            
            if let player = delegatedDictionary["UserId"] as? Int{
                playerID = "\(player)"
            }
            
        }
        
        var titleAcademy = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyName"] as? String{
            titleAcademy = "\(academy)"
            
            titleAcademy = titleAcademy.replacingOccurrences(of: " ", with: "%20", options: .regularExpression)
        }
        
        NetworkManager.performRequest(type:.get, method: "academy/CreateMangoPayTransaction/\(playerID)/\(academyID1)/\(titleAcademy)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let object2 = object as! [String:AnyObject]
            self.ObjectOfMangoPay = object2
            switch object2 {
            case _ as NSNull:
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            
            self.ObjectOfMangoPay.updateValue(self.classIDs as AnyObject, forKey: "ClassIds")
           
            if let playerId1 =  parameters["PlayerId"] as? String{
                self.ObjectOfMangoPay.updateValue(playerId1 as AnyObject, forKey: "PlayerId")
            }
            
            
            var discountPut = ""
            if discount == "" || discount == "0.0"{
                discountPut = "0"
            }else{
                discountPut  = discount
            }
            
            if self.forClassBooking {
               
                self.ObjectOfMangoPay.updateValue(discountPut as AnyObject, forKey: "Discounts")
                
            }else{
                
                self.ObjectOfMangoPay.updateValue(discountPut as AnyObject, forKey: "Discount")
            }
            
            if self.isNewUser(){
                self.ObjectOfMangoPay.updateValue("1" as AnyObject, forKey: "IsnNewUser")
            }
            
            //this will run if new user is being registered by coach
            
            
            self.ObjectOfMangoPay.updateValue(price as AnyObject, forKey: "Amount")
            
            var userName1 = self.userNameFld.text!.trimmingCharacters(in: .whitespacesAndNewlines)//self.userNameFld.text!
            if currentUserLogin == 4 {
                
                if let userName2 =   DataManager.sharedInstance.currentUser()!["UserName"]! as? String{
                    userName1 = "\(userName2)"
                }
                
            }else{
                if let player = self.delegatedDictionary["UserName"] as? String{
                    userName1 = "\(player)"
                }
                
            }
            
            var fullName = ""
            
            fullName = self.fNameFld.text! + " " + self.lNameFld.text!
            
            self.ObjectOfMangoPay.updateValue(fullName as AnyObject, forKey: "PlayerName")

            self.ObjectOfMangoPay.updateValue(userName1 as AnyObject, forKey: "UserName")
            
            var accessKey = ""
            if let accessKey1 = object2["Accesskey"] as? String{
                accessKey = accessKey1
            }
            var baseURL2 = ""
            if let baseURL1 = object2["BaseUrl"] as? String{
                baseURL2 = baseURL1
            }
            var cardPreregistrationId = ""
            
            //cardPreregistrationId
            if let cardPreregistrationId1 = object2["CardRegistrationId"] as? String{
                cardPreregistrationId = cardPreregistrationId1
            }
            var cardRegistrationURL = ""
            if let cardRegistrationURL1 = object2["CardRegistrationURL"] as? String{
                cardRegistrationURL = cardRegistrationURL1
            }
            var cardType = "CB_VISA_MASTERCARD"
            if let cardType1 = object2["CardType"] as? String{
                cardType = cardType1
            }
            var clientId = ""
            if let clientId1 = object2["ClientId"] as? String{
                clientId = clientId1
            }
            var preregistrationData = ""
            if let preregistrationData1 = object2["PreregistrationData"] as? String{
                preregistrationData = preregistrationData1
            }
            
            let parameters =  ["accessKey": accessKey,
                               "baseURL":baseURL2,
                               "cardPreregistrationId": cardPreregistrationId,
                               "cardRegistrationURL": cardRegistrationURL,
                               "cardType": cardType,
                               "clientId": clientId,
                               "preregistrationData": preregistrationData]
            
            _ = self.callMangoApi(cardInfo: parameters as [String : AnyObject])
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func isNewUser() -> Bool {
        
        if currentUserLogin == 4 {
            return false
        }else{
            if self.delegatedDictionary.count > 0 || self.currentStudentInfo.count > 0{
                
            }else{
                if let text = self.userNameFld.text , !text.isEmpty{
                    return true
                    
                }
            }
        }
        return false
    }
    
    //updatedstripe
    func registeringNewUserForStripe(parameters:[String:AnyObject])  {
        
        
        
        NetworkManager.performRequest(type:.post, method: "academy/RegisterPlayerForStripe/", parameter:parameters, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case   _ as NSNull :
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object)", view:self)
                
                return
                
            case _ as String:
                DataManager.sharedInstance.printAlertMessage(message:"\(object)", view:self)
                
                return
            default: break
                
            }
            
            let responce = object as! [[String:AnyObject]]
            
            if responce.count < 1 {
                                  DataManager.sharedInstance.printAlertMessage(message:"User name or email Already exist!", view:self)

                                  return
                              }else{
                                  
                              }
            
            self.currentStudentInfo = responce[0]
            self.delegatedDictionary = responce[0]

            if let idOfPlayer = self.currentStudentInfo["PlayerId"] as? Int{
                self.currentPlayerSelectedId = "\(idOfPlayer)"
            }

            self.handleAddPaymentOptionButtonTapped()
            
            
            print("success")
            
            
            
        }) { (error) in
            self.showInternetError(error: error!)
        }
        
    }
    
    func registeringNewUser(parameters:[String:AnyObject])  {
        
        //getting ids from userdefaults
        NetworkManager.performRequest(type:.post, method: "academy/RegisterPlayerForMangoPay/", parameter:parameters, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            if #available(iOS 13.0, *) {
            DispatchQueue.main.async(execute: {
                MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
            }else{
                MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
            }

            switch object {
            case   _ as NSNull :
                
                DataManager.sharedInstance.printAlertMessage(message:"Invalid Discount Code", view:self)
                
                return
                
            case _ as [String:AnyObject]: break
            default: break
                
            }
            
            let responce = object as! Int
            if responce > 0 {
                
                self.currentPlayerSelectedId = "\(responce)"
                if self.forClassBooking {
                    if let discounts = parameters["Discounts"] as? String{
                        self.gettingCardInfo(price: "\(self.totalAmountForClass)",discount:discounts,parameters: parameters as [String : AnyObject])
                    }
                    
                    
                }else{
                    if let amount = parameters["AmountPaid"] as? String{
                        
                        
                        self.gettingCardInfo(price: amount,discount:self.discountPercentageStr,parameters: parameters as [String : AnyObject])
                    }
                    
                    
                }
            }else if responce == -3 {
                DataManager.sharedInstance.printAlertMessage(message:"Already user registered!", view:self)
            }
            
            
            print("success")
            
            
            
        }) { (error) in
            self.showInternetError(error: error!)
        }
        
    }
    
    func callMangoApi(cardInfo:[String:AnyObject])->Bool  {
        print(cardInfo)
        print(ObjectOfMangoPay)
        let connected: Bool = Reachability.forInternetConnection().isReachable()
        if (connected == true) {
            if view != nil {
                DispatchQueue.main.async(execute: {
                                               MBProgressHUD.showAdded(to:self.view, animated: true)
                                               
                                             })            }
        }
        
        let objectMango = MPAPIClient.init(card: cardInfo as [AnyHashable : Any])
        let calendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM"
        let aDate = formatter.date(from: self.monthYearView.months[monthYearView.month-1])
        let components1 = calendar!.components(.month , from: aDate!)
        var dateExpire = ""
        if components1.month! > 9{
            dateExpire = "\(components1.month!)" + "\(monthYearView.year%100)"
        }else{
            dateExpire = "0\(components1.month!)" + "\(monthYearView.year%100)"
        }
        print(dateExpire)
        
        objectMango?.appendCardInfo(self.cardNmberFld.text!, cardExpirationDate:dateExpire , cardCvx: ccvFld.text!)
        objectMango?.registerCard({ (response: [AnyHashable : Any]?, error) in
            if (error != nil){
                if #available(iOS 13.0, *) {
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
                }else{
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
                }
                DataManager.sharedInstance.printAlertMessage(message: "Invalid Card Information", view:self)
                print(error)
            }else{
                if #available(iOS 13.0, *) {
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
                }else{
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
                }
                print(response)
                var CardId = ""
                if let CardId1 = response?["CardId"] as? String{
                    CardId = CardId1
                    self.ObjectOfMangoPay.updateValue(CardId as AnyObject, forKey: "CardId")
                    self.appendCardIdToMangoPay()
                }
            }
        })
        
        return true
    }
    
    func appendCardIdToMangoPay()  {
        
        print(ObjectOfMangoPay)
        NetworkManager.performRequest(type:.post, method: "booking/PerformUserTransaction", parameter:ObjectOfMangoPay, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case   _ as NSNull :
                DataManager.sharedInstance.printAlertMessage(message:"Error Occured", view:self)
                return
            case _ as [String:AnyObject]: break
            default: break
                
            }
            let responce = object as? String
            if responce == "1" {
                self.mangoPayPaid = true
                self.submitFormOrder()
            }else if responce == "-1"{
                self.mangoPayPaid = false
                var classOrLesson = ""
            
                if self.forClassBooking{
                classOrLesson = "Class"
                }else{
                    classOrLesson = "Lesson"
                }
                
                DataManager.sharedInstance.printAlertMessage(message:"\(classOrLesson) already book", view:self)
                if #available(iOS 13.0, *) {
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
                }else{
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
                }
            } else{
                self.mangoPayPaid = false
                DataManager.sharedInstance.printAlertMessage(message:"Error Occured", view:self)
                if #available(iOS 13.0, *) {
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
                }else{
                    MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
                }
            }
            
        }) { (error) in
            self.showInternetError(error: error!)
        }
    }
    
    //MARK:- booking lessons or class
    func submitFormOrder()  {
       
        messageString = ""
        validatingTextFields()
       
        if messageString.count > 0{
            
            let alertController = UIAlertController(title: "Error", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                MBProgressHUD.hideAllHUDs(for:self.appDelegate.window, animated: true )

            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        
        let FirstName1 = fNameFld.text!
        let LastName1 = lNameFld.text!
        let UserName1 = userNameFld.text!.trimmingCharacters(in: .whitespacesAndNewlines)//userNameFld.text!

        let Password1 = passwordFld.text!
        let Email1 = emailFld.text!
        let PayeeFirstName1 = fNameFld.text!
        let PayeeLastName1	= lNameFld.text!
        let PayeeAddress1	= ""
        let PayeeEmail1 = ""
        let PayeePhone1	= phonFl.text!
        
        var coachID1 = ""
        if currentUserLogin == 4{
            print(selectedCoachData)
            if fromRecommnded{
                if let coachId = selectedData["CoachId"] as? Int{
                    coachID1 = "\(coachId)"
                }
            }else{
            if let coachId = selectedCoachData["CoachID"] as? Int{
                coachID1 = "\(coachId)"
                }
            }
        }else{
            
          
        if let coachId = DataManager.sharedInstance.currentUser()?["coachId"] as? Int{
            coachID1 = "\(coachId)"
                }
            
        }
        
        var academyID1 = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyID1 = "\(academy)"
        }
        
        var date1 = ""
        var startTime1 = ""
        var endTime1 = ""
        if fromRecommnded {
            if let date2 = selectedData["LessonDate"] as? String{
                
                date1 = DataManager.sharedInstance.getFormatedDateForJson(date: date2, formate:globalDateFormate)
            }
            if let time = selectedData["StartTime"] as? String{
                startTime1 = time
            }
            
            if let time = selectedData["EndTime"] as? String{
                endTime1 = time
            }

        }else{
        if let date2 = selectedData["RBookingDate"] as? String{
        
            if fromEventController {
                date1 = date2
            }else{
                date1 = DataManager.sharedInstance.getFormatedDateForJson(date: date2, formate:globalDateFormate)
            }
        }
        if let time = selectedData["BookingStartTime"] as? String{
            startTime1 = time
        }
   
        if let time = selectedData["BookingEndTime"] as? String{
            endTime1 = time
            }
        }
        
        var vocherID1 = ""
        if delegatedVocherDic.count > 0  {
            if let vohcherCod = delegatedVocherDic["VoucherPin"] as? String{
                vocherID1 = vohcherCod
            }
        }else{
            if isRedeemedVoucher {
                if let voucherPin = redeemedData["VoucherPin"] as? String{
                   vocherID1 = voucherPin
                }
            }
        }
       
        if selectedPaymentType == "5" {
            if let vohcherCod = delegatedPackageDic["Pin"] as? String{
                vocherID1 = vohcherCod
            }
        }
        
        let PaymentModeId1 = selectedPaymentType
        
        var userID1 = ""
        if let userId = DataManager.sharedInstance.currentUser()?["Userid"] as? Int{
            userID1 = "\(userId)"
        }
        
        var  Dob1 = ""
        var  DobForChild = DataManager.sharedInstance.getTodayDate()
        //GDPR
        if underAgeFlage{
            
            DobForChild = DataManager.sharedInstance.getFormatedDateGivenPAttern(date: dobChildFld.text!, formate: "dd MMM, yyyy", formateReturn: "yyyy-MM-dd")
        }else{
            authorizeFlage = false
            authoRizeBtn.isSelected = false
            parentFld.text = ""
            dobChildFld.text = ""
        }

        var PlayerUserId1 = ""
        if currentUserLogin == 4 {
            if let userId =   DataManager.sharedInstance.currentUser()!["playerId"]! as? Int{
                currentPlayerSelectedId = "\(userId)"
            }
            
            if let userId =  DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
                PlayerUserId1 = "\(userId)"
            }
            
        }else{
            

        if let player = delegatedDictionary["UserId"] as? Int{
            PlayerUserId1 = "\(player)"
            
            }
        }
       var BillingAdd11 = adressFld.text!
       var BillingAdd21	 = ""
       var BillingEmail1 = emailFld.text!
        
       var BillingCity1 = cityFld.text!

       var BillingCode1	= ccvFld.text!
        let BillingCountry	= "0"
        var AmountPaid1 = ""

        if isRedeemedVoucher {
            
            if let price = redeemedData["Amount"] as? Double{
                AmountPaid1 = "\(price)"
            }
            
        }else{
        if let price = selectedData["Price"] as? String{
            AmountPaid1 = "\(price)"
            }
            
            if fromRecommnded {
                if let price = selectedData["Price"] as? Double{
                    AmountPaid1 = "\(price)"
                }
            }
        }

        
        //if selected from user Than replace else pick from manualy entered
        if let dob = delegatedDictionary["DateofBirth"] as? String{
            Dob1 = dob
        }
        var CreditCardNbr1 = cardNmberFld.text!
        
       var CardName1 = nameOnCardFld.text!
        
        
      var  CardExpiry1 = ""
    
        if (datePaymntFld.text?.count)! > 0 {
            CardExpiry1 = datePaymntFld.text!
        }
       let TransactionCode1 = ""
       let ChildName1 = ""
        let ParentApp1	= "false"
       var LessonDurationId1 = ""

        if let id = secondLastControllerData["DurationId"] as? Int{
            LessonDurationId1 = "\(id)"
        }
        
        var IsStudio1 =    ""
        var IsPuttingLab1 =  "false"
        let defaults = UserDefaults.standard
        if let isStudio2 = defaults.object(forKey: "isStudio") as? String{
            if isStudio2 == "YES"{
                IsStudio1 =    "true"
                IsPuttingLab1 = "false"
            }else{
                IsStudio1 =    "false"
            }
        }
        
        let IsPuttingLab2 = DataManager.sharedInstance.detectIsPuttingLab(key: "isPuttingLab")
        if IsPuttingLab2 == "true" {
            IsPuttingLab1 = "true"
            IsStudio1 =    "false"
        }else{
            IsPuttingLab1 = "false"
        }

        
        //will run for lessons only
        
            if forClassBooking {
                
            }else{

                if isRedeemedVoucher {
                    if let id = redeemedData["LessonDurationId"] as? Int{
                        LessonDurationId1 = "\(id)"
                    }
                    
                    if let id = redeemedData["IsPuttingLab"] as? Bool{
                        
                        if id {
                            IsPuttingLab1 = "true"
                            IsStudio1 =    "false"
                        }
                    }
                }else if fromRecommnded{
                    if let id = selectedData["LessonDurationId"] as? Int{
                        LessonDurationId1 = "\(id)"
                    }
                    
                    if let putingLab = selectedData["IsPuttingLab"] as? Bool,putingLab == true{
                        IsPuttingLab1 = "true"
                        IsStudio1 =    "false"
                    }
                    if let studioEna = selectedData["isStudioEnable"] as? Bool,studioEna == true{
                        IsPuttingLab1 = "false"
                        IsStudio1 =  "true"
                    }
                }
            }
        
        
        var isMangoPay1 = 0
        var isStripe = 0
        var isSquare = 0
        //stripe

        if selectedPaymentType == "1" {
            
            //updatedstripe

            var discountTxt =  discountFld.text

            
            if currentAcademyOnlinePayment == 0 {//braintree payment
                
            }else if currentAcademyOnlinePayment == 1{//mangopaye payment
                isMangoPay1 = 1
            }else if currentAcademyOnlinePayment == 2{//stripe payment
                isStripe = 1
                discountTxt = adressFld.text
                
            }else if currentAcademyOnlinePayment == 3{//square payment
                isSquare = 1
            }else if currentAcademyOnlinePayment == -1{
                DataManager.sharedInstance.printAlertMessage(message: "Credit Card Booking Disabled", view: self)
                return
            }
            
            vocherID1 = ""

                        if discountInfoGotOrNot {
                        }else if let text = discountTxt , !text.isEmpty {
                            if forClassBooking {
                                gettingDiscountForClass()
                            }else{
                                gettingDiscountInfo()
                            }
                            return
                            
                        }else{
                        }
            
        }else if  selectedPaymentType == "2" {
        
            BillingAdd11 = ""
            BillingCity1 = ""
            BillingCode1 = ""
            CardName1  = ""
            CreditCardNbr1 = ""
            CardExpiry1  = ""
         
            
        }else  if selectedPaymentType == "3" {
                vocherID1 = ""
            BillingAdd11 = ""
            BillingCity1 = ""
            BillingCode1 = ""
            CardName1  = ""
            CreditCardNbr1 = ""
            CardExpiry1  = ""
            

                        if discountInfoGotOrNot {
                        }else if let text = adressFld.text , !text.isEmpty {
                            if forClassBooking {
                                gettingDiscountForClass()
                            }else{
                                gettingDiscountInfo()
                            }
                            return
                            
                        }else{
                        }
//
        }else  if selectedPaymentType == "4" {
               vocherID1 = ""
            BillingAdd11 = ""
            BillingCity1 = ""
            BillingCode1 = ""
            CardName1  = ""
            CreditCardNbr1 = ""
            CardExpiry1  = ""
            
        }
        
        if discountPercentage == 0.0 {
            discountPercentageStr = ""
            
        }else{
           discountPercentageStr = "\(discountPercentage)"
        }
        var IsRecommended1 = "false"
        if fromRecommnded {
            IsRecommended1 = "true"
        }
   
        var parameters : [String:String] = [String:String]()
        var urlStr = ""
        
        
        var IsnNewUser1 = "0"
        if self.isNewUser(){
            IsnNewUser1 = "1"
            userID1 = self.currentPlayerSelectedId
        }
        
        var discounts = ""
        
        var parentAcademyId = ""
        if let parentAcademyid = DataManager.sharedInstance.currentAcademy()?["ParentAcademyId"] as? Int{
            parentAcademyId = "\(parentAcademyid)"
        }
        
        var discountCod = ""
        if selectedPaymentType == "3" {
            discountCod = self.adressFld.text!
        }else{
            discountCod = discountFld.text!
        }
        
        
        print(discountCod)

        
        var secureKey = ""
        if let publicKey1 = DataManager.sharedInstance.currentAcademy()?["StandardPrivateKey"] as? String{
            secureKey = "\(publicKey1)"
        }
        
        if forClassBooking {
            
            urlStr = "BookingCartClass"
          //getting discount for each class
                for item in 0...cart.count-1 {
                    if let type = discountDic["DiscountType"] as? Int {
                        //if 0ne handle percentage logic
                        if type == 1 {
                            if let amountFor = cart[item]["Price"] as? Double{
                                let amountForDoub = Double(amountFor)
                                if let discount = discountDic["DiscountPercentage"] as? Int {
                                    
                                    if let programId = discountDic["ProgrameTypeId"] as? Int  {

                                        if let prviousData =  cart[item]["previousData"] as? [String:AnyObject]{
                                            if let idOFprogram = prviousData["ProgramTypeId"] as? Int{
                                                if idOFprogram == programId || programId == -1{
                                                    discountPercentage = Double(discount) / 100.0 * amountForDoub
                                                    print(discountPercentage)
                                                    discounts = discounts + "\(discountPercentage)" + ","
                                                }else{
                                                    //runs when id not match return in discount json
                                                     discounts = discounts + "\(0)" + ","
                                                }
                                            }
                                        }
                                        
                                    }
                             
                                }
                                // sending empty discounts array
                                if discountDic.count < 1 {
                                    discounts = discounts + "\(0)" + ","
                                }
                            }
                        }else{
                            
                        var totalDiscount = 0.0
                        if let discount = discountDic["Amount"] as? Double {
                            
                         if let amountFor = cart[item]["Price"] as? Double{
                            
                            
                            if let discountProgram = discountDic["ProgrameTypeId"] as? Int{
                                
                                
                                if let prviousData =  cart[item]["previousData"] as? [String:AnyObject]{
                                    if let idOFprogram = prviousData["ProgramTypeId"] as? Int{
                                if idOFprogram == discountProgram || discountProgram == -1{
                                            if discount > amountFor {
                                                discounts = discounts + "\(amountFor)" + ","
                                                
                                            }else{
                                                discounts = discounts  + "\(discount)" +  ","
                                            }
                                        }else{
                                    //run if discountid not match with program
                                    discounts = discounts + "\(0)" + ","
                                }
                    }
                    }
                    }
                }
                }
                if discountDic.count < 1 {
                    discounts = discounts + "\(0)" + ","
                            }

                        }
         
                    }
            }
            if discounts.count > 0 {
                discounts = discounts.substring(to: discounts.index(before: discounts.endIndex))
            }
            
            if let dob = currentStudentInfo["DateofBirth"] as? String{
                let dateOrignal = dob.components(separatedBy: "T")
                if dateOrignal.count > 0 {
                    Dob1  =  dateOrignal[0]
                    
                    
                }
            }
            
            if discounts.count < 1 {
                discounts = ""
              
                if cart.count > 1 {
                    for item in 0...cart.count-1 {
                            discounts = discounts + "0" + ","
                            _=item
                        }
                        discounts = discounts.substring(to: discounts.index(before: discounts.endIndex))
                }else if cart.count == 1{
                    discounts = "0"

                }
            }

            parameters = ["prntaprvl":ParentApp1 ,"ChldName":ChildName1,"ParntName":"" ,"TransactionCode":TransactionCode1 ,"CardExpiry":CardExpiry1 ,"CardName":CardName1 ,"CreditCardNbr":CreditCardNbr1 ,"Dob":Dob1 ,"Discounts":"\(discounts)","BillingCode":BillingCode1,"BillingCountry":BillingCountry,"BillingCity":BillingCity1 ,"BillingEmail":BillingEmail1 ,"BillingAdd2":BillingAdd21 ,"BillingAdd1":BillingAdd11 ,"PlayerUserId":PlayerUserId1 ,"UserId":userID1 ,"PaymentModeId":PaymentModeId1 ,"VoucherCode":vocherID1,"AcademyID":academyID1 ,"PlayerId":currentPlayerSelectedId ,"PayeePhone":PayeePhone1 ,"PayeeEmail":PayeeEmail1 ,"PayeeAddress":PayeeAddress1 ,"PayeeLastName":PayeeLastName1 ,"PayeeFirstName":PayeeFirstName1 ,"Email":Email1 ,"Password":"2019newgolflockerchanges" ,"UserName":UserName1 ,"LastName":LastName1 ,"FirstName":FirstName1,"ClassIds":classIDs,"isMangoPay":"\(isMangoPay1)","IsnNewUser":IsnNewUser1,"DiscountCode":discountCod,"AgreeTerms": "\(privacyBtnFlage)" ,"RecEmail": "\(emailBtnFlage)","IsUnderEighteen":"\(underAgeFlage)","ParentApproval":"\(authorizeFlage)" ,"ParentName":parentFld.text! ,"ChildDob":DobForChild,"nonceFromTheClient":self.nonceFromTheClient,"isStripe":"\(isStripe)","isSquare":"\(isSquare)","ParentAcademyId":parentAcademyId,"Is12HoursTimeFormat":"\(is12Houre)","StandardPrivateKey":secureKey]
        }else{
          urlStr = "BookingLesson"
            parameters = ["IsRecommended":IsRecommended1 ,"IsStudio":IsStudio1 ,"LessonDurationId":LessonDurationId1 ,"ParentApp":ParentApp1 ,"ChildName":ChildName1 ,"TransactionCode":TransactionCode1 ,"CardExpiry":CardExpiry1 ,"CardName":CardName1 ,"CreditCardNbr":CreditCardNbr1 ,"Dob":Dob1 ,"Discount":"\(discountPercentageStr)","AmountPaid":AmountPaid1 ,"BillingCode":BillingCode1 ,"BillingCity":BillingCity1 ,"BillingEmail":BillingEmail1 ,"BillingAdd2":BillingAdd21 ,"BillingAdd1":BillingAdd11 ,"PlayerUserId":PlayerUserId1 ,"UserID":userID1 ,"PaymentModeId":PaymentModeId1 ,"VoucherCode":vocherID1 ,"EndTime":endTime1,"StartTime":startTime1,"Date":date1 ,"AcademyID":academyID1 ,"CoachID":coachID1 ,"PlayerId":currentPlayerSelectedId ,"PayeePhone":PayeePhone1 ,"PayeeEmail":PayeeEmail1 ,"PayeeAddress":PayeeAddress1 ,"PayeeLastName":PayeeLastName1 ,"PayeeFirstName":PayeeFirstName1 ,"Email":Email1 ,"Password":"2019newgolflockerchanges" ,"UserName":UserName1 ,"LastName":LastName1 ,"FirstName":FirstName1,"isMangoPay":"\(isMangoPay1)","IsnNewUser":IsnNewUser1,"IsPuttingLab":IsPuttingLab1,"DiscountCode":discountCod,"AgreeTerms": "\(privacyBtnFlage)" ,"RecEmail": "\(emailBtnFlage)" ,"IsUnderEighteen":"\(underAgeFlage)","ParentApproval":"\(authorizeFlage)" ,"ParentName":parentFld.text! ,"ChildDob":DobForChild,"nonceFromTheClient":self.nonceFromTheClient,"isStripe":"\(isStripe)","isSquare":"\(isSquare)","ParentAcademyId":parentAcademyId,"Is12HoursTimeFormat":"\(is12Houre)","StandardPrivateKey":secureKey]
    }
      print(parameters)
        
            if currentAcademyOnlinePayment == 0{
                
                print("braintree")
                
                if brainTreePaid{
                    
                }else{
                    
                    if selectedPaymentType == "1" {
                        
                        self.createTokenForBraintree()
                        return
                    }
                }
                
            }else if currentAcademyOnlinePayment == 1{
                print("mangoe pay")
                
        if mangoPayPaid {
            
        }else{
            if selectedPaymentType == "1" {
                
                
                if isNewUser(){
                    
                    self.registeringNewUser(parameters: parameters as [String : AnyObject])
                }else{
                    
                    if forClassBooking {
                        gettingCardInfo(price: "\(totalAmountForClass)",discount:discounts,parameters: parameters as [String : AnyObject])
                    }else{
                        gettingCardInfo(price: AmountPaid1,discount:discountPercentageStr,parameters: parameters as [String : AnyObject])
                    }
                }
                
                return
            }
            
            
        }
            }else if currentAcademyOnlinePayment == 2{//stripe payment gateway
                
                //updatedstripe
                if selectedPaymentType == "1" {

                if self.amountToDeductForBooking() > 0{
                    if isStripePaid {
                        
                    }else{
                        
                        
                        if isNewUser(){
                            
                            
                            registeringNewUserForStripe(parameters: parameters as [String : AnyObject])
                            return
                        }else{
                        
                        if let stripeCust =  currentStudentInfo["StripeUserId"] as? String,stripeCust == "" {
                            creatingStripeCustomer()
                            return
                        }else{
                            
                            handleAddPaymentOptionButtonTapped()
                            return
                            
                            }
                            
                        }
                    }
                    
                }
                
                }
                
                
        }
        
        NetworkManager.performRequest(type:.post, method: "booking/\(urlStr)/", parameter:parameters as [String : AnyObject]?, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            if let object1 = object as? [String:AnyObject]{
                var messageToShow = ""
                if self.selectedPaymentType == "2"{
                    messageToShow = "InValid Voucher"
                }else{
                    messageToShow = "Unable To Complete Order!"
                }
                
                DataManager.sharedInstance.printAlertMessage(message:messageToShow, view:self)
                _=object1
                return;
            }
            
            let responce = object as! String
            switch responce {
            case   "-1" :
                DataManager.sharedInstance.printAlertMessage(message:"Unable To Complete Order!", view:self)
                 self.discountInfoGotOrNot = false
                self.brainTreePaid = false

                return
            case   "-3" :
                DataManager.sharedInstance.printAlertMessage(message:"Username Already exist\nPlease try another username", view:self)
                 self.discountInfoGotOrNot = false
                self.brainTreePaid = false

                return
            case   "-2" :
                DataManager.sharedInstance.printAlertMessage(message:"Invalid Credit Card Info.", view:self)
                 self.discountInfoGotOrNot = false
                self.brainTreePaid = false

                return

            case   "0" :
                var bookMessage = ""
                if self.forClassBooking{
                    bookMessage = "Class Already Booked"
                }else{
                    bookMessage = "Lesson Already Booked"
                }
                DataManager.sharedInstance.printAlertMessage(message:bookMessage, view:self)
                 self.discountInfoGotOrNot = false
                self.brainTreePaid = false

                return
            default: break
                
            }
           
            print("success")
            
            //freeing the type of lesson from userDefaults
            let defaults = UserDefaults.standard
            defaults.set("NO", forKey: "isStudio")
            defaults.set("NO", forKey: "isPuttingLab")
            defaults.synchronize()

            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController") as! OrderConfirmationViewController
            controller.responceString = responce
            if self.discountInfoGotOrNot{
                controller.discountInfoGotOrNot = true
               controller.totalDiscount = self.totalDiscountIfClasesMore
            }
            controller.allBookingData =  parameters as [String : AnyObject]
            if self.forClassBooking{
                if let prviousData =  self.cart[0]["previousData"] as? [String:AnyObject]{
                    controller.detailData = prviousData
                }
                controller.forClassBooking = true
                 controller.titleOfClass = self.lessonTypLbl.text!

            }else{
                controller.detailData = self.selectedData
            }
            controller.emailId = self.emailFld.text!
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            if self.forClassBooking{
               self.emptyingCart()
            }
             self.discountInfoGotOrNot = false
            
        }) { (error) in
            print(error!)
            self.brainTreePaid = false

            self.showInternetError(error: error!)
        }
    }
//
    func emptyingCart()  {
       
        self.cart.removeAll()
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        var cartKey = ""
        if currentUserLogin == 4 {
            cartKey = "studentCart" +  userId
        }else{
            cartKey = "myCart" + userId
        }
        let archiveddata = NSKeyedArchiver.archivedData(withRootObject: self.cart )
        UserDefaults.standard.set(archiveddata, forKey: cartKey)
    }
    
    
    func createCustomTokenFromServer()  {
        
        var academyID1 = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyID1 = "\(academy)"
        }
        
        DispatchQueue.main.async(execute: {
                                       MBProgressHUD.showAdded(to:self.view, animated: true)
                                       
                                     })
        NetworkManager.performRequest(type:.get, method: "academy/GetBraintreeClientToken/\(academyID1)", parameter:nil, view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                
                return
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                break
            }
            
            
            let clientToken = object as! String
            self.braintree = BTAPIClient.init(authorization: clientToken)?.copy(with: BTClientMetadataSourceType.form, integration: .custom)
            
            let cardClient = BTCardClient(apiClient: self.braintree!)
            var  year = ""
            var month = ""
            if self.monthYearView.month == 0 {
                
                let date =  DataManager.sharedInstance.getTodayDate()
                let components = date.components(separatedBy: "-")
                year =   "\(components[0])"
                month = "\(components[1])"
                
            }else{
                year =   "\(self.monthYearView.year)"
                month = "\(self.monthYearView.month)"
            }
            
            
            print(year)
            let card = BTCard(number: self.cardNmberFld.text!, expirationMonth: "\(month)", expirationYear: "\(year)", cvv: self.ccvFld.text!)
            
            cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                
                if error == nil {
                    print((tokenizedCard?.nonce)!)
                    self.nonceFromTheClient = (tokenizedCard?.nonce)!
                    
                    //                    self.postNonceToServer(paymentMethodNonce: (tokenizedCard?.nonce)!)
                    self.brainTreePaid = true
                    self.submitFormOrder()
                }else{
                    self.brainTreePaid = false
                }
                // Communicate the tokenizedCard.nonce to your server, or handle error
            }
            
            
            
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func createTokenForBraintree()  {
        
        createCustomTokenFromServer()
    
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- discounts handling methods from server
    func gettingDiscountInfo()  {
        
        var typeID = ""
        var date1 = ""
        if fromRecommnded {
            
            if let typeId = selectedData["ProgramTypeId"] as? Int{
                typeID = "\(typeId)"
            }
            if let date2 = selectedData["LessonDate"] as? String{
                date1 = DataManager.sharedInstance.getFormatedDateForJson(date: date2, formate:globalDateFormate)
            }

        }else{
            if let typeId = previousData["ProgramTypeId"] as? Int{
                typeID = "\(typeId)"
            }
     
            if let date2 = selectedData["RBookingDate"] as? String{
                if fromEventController {
                     date1 = date2
                }else{
                    date1 = DataManager.sharedInstance.getFormatedDateForJson(date: date2, formate:globalDateFormate)
                }
            }
        }
    
        var discountVal = ""
        if selectedPaymentType == "3" {
            
            
            discountVal = self.adressFld.text!
        }else{
            
            //updatedstripe
            if currentAcademyOnlinePayment == 2{
                discountVal = self.adressFld.text!
                
            }else{

                discountVal = discountFld.text!
                
            }
            
        }
        let playerID = getSelectedPlayerId()

        
        var parameters : [String:String] = [String:String]()

        var academyID1 = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyID1 = "\(academy)"
        }

        
         parameters = ["AcademyId":academyID1,"ProgramTypeId":typeID ,"DiscountCoupnId": discountVal,"date":date1 ,"PlayerId":playerID ]
        
        print(parameters)

        NetworkManager.performRequest(type:.post, method: "booking/GetDiscount/", parameter:parameters as [String : AnyObject], view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
        
            switch object {
            case   _ as NSNull :
                
                DataManager.sharedInstance.printAlertMessage(message:"Invalid Discount Code", view:self)
                self.discountInfoGotOrNot = false
                return
                
               case _ as [String:AnyObject]: break
            default: break
                
            }
           
            let responce = object as! NSDictionary
            
            if let id = responce["AcademyId"] as? Int{
                 if let id = responce["CouponId"] as? String , id == "-1" {
                    DataManager.sharedInstance.printAlertMessage(message:"This discount code has been expired. Please try some other", view:self)
                    self.discountInfoGotOrNot = false
                }else if let id = responce["CouponId"] as? String , id == "-2" {
                    DataManager.sharedInstance.printAlertMessage(message:"You have already used this Discount", view:self)
                    self.discountInfoGotOrNot = false
                }else if id == 0 {
                    DataManager.sharedInstance.printAlertMessage(message:"Invalid Discount Code", view:self)
                    self.discountInfoGotOrNot = false
                }else{
                    self.showingDiscountAlert(dic: responce)
                }
            }
            
            print("success")
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
              self.discountInfoGotOrNot = false
        }

    }
    
    func gettingDiscountForClass()  {
        
        var typeID = ""
        if let typeId = previousData["ProgramTypeId"] as? Int{
            typeID = "\(typeId)"
        }
        _ = typeID
        if fromRecommnded {
            if let typeId = selectedData["ProgramTypeId"] as? Int{
                typeID = "\(typeId)"
            }
        }
        
        print(programIDs,classIDs)
        var discountVal = ""
        if selectedPaymentType == "3" {
            discountVal = self.adressFld.text!
        }else{
            //updatedstripe
            if currentAcademyOnlinePayment == 2 {
                
                discountVal = adressFld.text!
                
            }else{
                discountVal = discountFld.text!
                
            }
        }
        
        var academyID1 = ""
        if let academy = DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyID1 = "\(academy)"
        }
    
        let playerID = getSelectedPlayerId()
        
        var parameters : [String:String] = [String:String]()
        
        parameters = ["AcademyId":academyID1 ,"ProgramTypeIds":programIDs ,"DiscountCoupnId": discountVal ,"ClassId":classIDs ,"PlayerId":playerID,"date":""]
        
        print(parameters)
        //getting ids from userdefaults
        NetworkManager.performRequest(type:.post, method: "booking/CheckDiscountForClass/", parameter:parameters as [String : AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case   _ as NSNull :
                
                DataManager.sharedInstance.printAlertMessage(message:"Invalid Discount Code", view:self)
                self.discountInfoGotOrNot = false
                return
                
            case _ as [String:AnyObject]: break
            default: break
                
            }
            
            let responce = object as! NSDictionary
            if let id = responce["AcademyId"] as? Int{
                 if let id = responce["CouponId"] as? String , "\(id)" == "-1" {
                    DataManager.sharedInstance.printAlertMessage(message:"This discount code has been expired. Please try some other", view:self)
                    self.discountInfoGotOrNot = false
                }else if let id = responce["CouponId"] as? String , id == "-2" {
                    DataManager.sharedInstance.printAlertMessage(message:"You have already used this Discount", view:self)
                    self.discountInfoGotOrNot = false
                }else if id == 0 {
                    DataManager.sharedInstance.printAlertMessage(message:"Invalid Discount Code", view:self)
                    self.discountInfoGotOrNot = false
                }else{
                    self.showingDiscountAlert(dic: responce)
                }
            }
    
            print("success")
            
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
              self.discountInfoGotOrNot = false
        }
        
    }
    
    func getSelectedPlayerId() -> String {
        
        var playerID = ""

        if currentUserLogin == 4 {
            
            if let playerId1 =   DataManager.sharedInstance.currentUser()!["playerId"]! as? Int{
                playerID = "\(playerId1)"
            }
            
        }else{
            
            //            if isNewUser() {
            //                playerID =  self.currentPlayerSelectedId
            //            }
//            if let player = delegatedDictionary["UserId"] as? Int{
//                playerID = "\(player)"
//            }
            
                        if delegatedDictionary.count > 0{
                           playerID =  currentPlayerSelectedId
                            
                        }
        }

        return playerID
        
    }
    
    
    //MARK:- Discounts Alerts methods
    var totalAmount = 0.0
    var totalAmountForMember = 0.0
    
    func showingDiscountAlert(dic:NSDictionary)  {
        discountDic = dic
        var dicountInfo = ""
//    var totalAmount = 0.0
        if forClassBooking {
            totalAmount = Double(totalAmountForClass)
            dicountInfo = "Price: \(totalAmount) \n"
    var totalAmountForDiscount = 0.0
    
            for index  in 0...cart.count-1 {
                if let discountProgram = dic["ProgrameTypeId"] as? Int{
                    if let prviousData =  cart[index]["previousData"] as? [String:AnyObject]{
                        if let idOFprogram = prviousData["ProgramTypeId"] as? Int{
                            if idOFprogram == discountProgram || discountProgram == -1 {
                                totalAmountForDiscount = totalAmountForDiscount + (cart[index]["Price"] as? Double)!
                            }
                        }
                        
                    }
                   
                }
            }
            
            
            if let type = dic["DiscountType"] as? Int {
                //if 0ne handle percentage logic
                if type == 1 {
                    if let discount = dic["DiscountPercentage"] as? Int {
                        discountPercentage = Double(discount) / 100.0 * totalAmountForDiscount
                        print(discountPercentage)
                        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                            dicountInfo = dicountInfo + "Discount: \(discountPercentage) \(currencySign) \n"

                        }else{
                            dicountInfo = dicountInfo + "Discount: \(currencySign) \(discountPercentage) \n"
                            
                        }
                        totalAmount = totalAmount - discountPercentage
                        totalDiscountIfClasesMore = discountPercentage
                    }
                }else{
                    
                var totalDiscount = 0.0
                    if let discount = dic["Amount"] as? Double {
                        
                        for index  in 0...cart.count-1 {
                            
                            if let discountProgram = dic["ProgrameTypeId"] as? Int{
                            
                                
                                if let prviousData =  cart[index]["previousData"] as? [String:AnyObject]{
                                    if let idOFprogram = prviousData["ProgramTypeId"] as? Int{
                                        if idOFprogram == discountProgram || discountProgram == -1{
                                if discount > (cart[index]["Price"] as? Double)! {
                                    totalDiscount = totalDiscount + (cart[index]["Price"] as? Double)!
                                            }else{
                                    totalDiscount = totalDiscount + discount
                                        }
                                        
                                        }
                                    }
                                }
                            }
                        }

                        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                            dicountInfo = dicountInfo + " Discount: \(totalDiscount) \(currencySign) \n"

                        }else{
                            dicountInfo = dicountInfo + " Discount: \(currencySign) \(totalDiscount) \n"
                            
                        }
                        totalAmount = totalAmount - Double(totalDiscount)
                        totalDiscountIfClasesMore = totalDiscount
                        }
                }
            }
            
        }else{
            
            if fromRecommnded {
                if let price = selectedData["Price"] as? Double{
                    if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                        dicountInfo = "Lesson Price: \(price)\(currencySign)"

                    }else{
                    dicountInfo = "Lesson Price: \(currencySign)\(price)"
                    
                }
                    totalAmount = Double(price)
                }

            }else{
           if let price = selectedData["Price"] as? String{
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                dicountInfo = "Lesson Price: \(price)\(currencySign)"

            }else{
                dicountInfo = "Lesson Price: \(currencySign)\(price)"
                
            }
            totalAmount = Double(price)!
                }
            }
            
            //calculating discount
            if let type = dic["DiscountType"] as? Int {
                //if 0ne handle percentage logic
                if type == 1 {
                    if let discount = dic["DiscountPercentage"] as? Int {
                        discountPercentage = Double(discount) / 100.0 * totalAmount
                        print(discountPercentage)
                        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                            dicountInfo = dicountInfo + "Discount: \(discountPercentage) \(currencySign) \n"

                        }else{
                            dicountInfo = dicountInfo + "Discount: \(currencySign) \(discountPercentage) \n"
                            
                        }
                        totalAmount = totalAmount - discountPercentage
                    }
                }else{
                    
                    if let discount = dic["Amount"] as? Double {
                        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                            dicountInfo = dicountInfo + " Discount: \(discount) \(currencySign) \n"

                        }else{
                            
                            dicountInfo = dicountInfo + " Discount: \(currencySign) \(discount) \n"
                            
                        }
                        discountPercentage = Double(discount)
                        totalAmount = totalAmount - Double(discount)
                    }
                }
            }
        }
        
       totalAmount =  Double(round(100*totalAmount)/100)
        
        if totalAmount < 0 {
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                dicountInfo = dicountInfo + "Total Price:\(0) \(currencySign)\n"

            }else{
                dicountInfo = dicountInfo + "Total Price:\(currencySign) \(0) \n"
                
            }

        }else{
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                dicountInfo = dicountInfo + "Total Price:\(totalAmount) \(currencySign) \n"

            }else{
                dicountInfo = dicountInfo + "Total Price:\(currencySign) \(totalAmount) \n"
                
            }
        }
   
        let alertController = UIAlertController(title: "Discount Details", message: dicountInfo, preferredStyle: UIAlertControllerStyle.alert)
      
//        if let pric = dic["Amount"] as? String {
//            dicountInfo = dicountInfo + "Lesson Price:£ \(pric)"
//        }
        
        let okAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.discountInfoGotOrNot = true
            self.submitFormOrder()
            print("OK")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.discountInfoGotOrNot = false
            self.discountDic = NSDictionary()
        
            self.discountPercentageStr = ""
            self.discountPercentage = 0.0
            print("cancel")
        }
         alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)

    
    }

    
    //MARK:- buttons Actions
    @IBAction func pdfTermsBtnAction(_ sender: UIButton) {
        
        var titleAcademy = ""
        if let titl =  DataManager.sharedInstance.currentAcademy()!["PrivacyPolicy"] as? String{
            privacyTermsPDF = titl
        }
//        privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"

//        privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"

        UIApplication.shared.openURL(NSURL(string:privacyTermsPDF)! as URL)
        dismiss(animated: true, completion: nil)


    }
    
    @IBAction func ageBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            underAgeFlage = false
            
            
            view.layoutIfNeeded()
            
            UIView.animate(withDuration: 1.0, animations: {
                
                if self.delegatedDictionary.count > 0 || currentUserLogin == 4 {
                    self.detailViewHeightConstaint.constant = 180
                }else{
                    //                hidepassword
                    self.detailViewHeightConstaint.constant = 225//250
                }
                self.childTermViewHeightCons.constant = 0//forFDGS 45 to 0
                
                self.view.layoutIfNeeded()
            })
            
        }else{
            sender.isSelected = true
            underAgeFlage = true
            view.layoutIfNeeded()
            
            UIView.animate(withDuration: 1.0, animations: {
                if self.delegatedDictionary.count > 0 || currentUserLogin == 4 {
                    self.detailViewHeightConstaint.constant = 285
                }else{
                    // hidepassword
                    self.detailViewHeightConstaint.constant = 330//355
                }
                
                self.childTermViewHeightCons.constant = 148
                self.view.layoutIfNeeded()
            })
            
            
        }
        
    }
    
    @IBAction func authorizeBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            authorizeFlage = false
        }else{
            sender.isSelected = true
            authorizeFlage = true
        }
        
    }
    
    @IBAction func dobAgeBtnAction(_ sender: UIButton) {
        
        let formator = DateFormatter()
        
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
            self.dobChildFld.text = title
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        
        picker?.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        picker?.show()
        
    }
    

    @IBAction func emailBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            emailBtnFlage = false

        }else{
            sender.isSelected = true
            emailBtnFlage = true
        }
    }
    @IBAction func conditionsBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            privacyBtnFlage = false

        }else{
            sender.isSelected = true
            privacyBtnFlage = true
        }
    }

    
    @IBAction func selectPlayerBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectStudentViewController") as! SelectStudentViewController
        controller.originaldata = usersDataArray
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    @IBOutlet weak var dobFld: UITextField!

    @IBAction func dobBtnAction(_ sender: UIButton) {
        let formator = DateFormatter()
        
        ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
            
            
            self.dobFld.text =  DataManager.sharedInstance.getFormatedDate(date: title,formate: "dd MMM, yyyy")
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView).show()
    }
    
    @IBAction func vocherBtnAction(_ sender: UIButton) {
        
        if selectedPaymentType == "5" {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "VoucherViewController") as! VoucherViewController
            controller.originaldata = packagesArray
            controller.forPackage = true
            controller.delegate = self
            controller.fromBookingScreen = true
            self.navigationController?.pushViewController(controller, animated: true)

        }else{
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VoucherViewController") as! VoucherViewController
        controller.originaldata = vochersArray
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    @IBAction func studentTypeBtnAction(_ sender: UIButton) {
   
        
        ActionSheetStringPicker.show(withTitle: "Is Student a junior (under 18 years)", rows:["Is Student a junior (under 18 years)","YES","NO"] , initialSelection: 0, doneBlock: {
            picker, values, indexes in
              self.studentTypeLbl.text = "\(indexes!)"
            if values == 1{
              
                    self.detailViewHeightConstaint.constant = 280
                self.parentViewHeightConstraint.constant = 775 + 255             //added 85 to add terms View in footer

                    self.dobViewHeightConstraint.constant = 80
            }else{
                self.detailViewHeightConstaint.constant = 245
                self.parentViewHeightConstraint.constant = 740 + 255            //added 85 to add terms View in footer

                self.dobViewHeightConstraint.constant = 35
            }
            
            if  self.isPaymentSelected {
                self.parentViewHeightConstraint.constant = 1120 + 255             //added 85 to add terms View in footer

            }else{
                self.parentViewHeightConstraint.constant = 740 + 255             //added 85 to add terms View in footer

            }
            
     
            return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func cancleBtnAction(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        submitFormOrder()
    }
    @IBAction func paymentByBtnAction(_ sender: UIButton) {
        
        //just to avoid crash
        if paymentMethodsArray.count < 1{
            return
        }
        
        var dataAray = [String]()
        paymentMethodsTrack = [[String:AnyObject]]()
        for dic in paymentMethodsArray{
            if let val = dic["PaymentName"] as? String{
                
                if currentUserLogin == 4 {
                    //if login is student
                }else{
                //if login is coach
                if val == "Voucher" {
                    if delegatedDictionary.count > 0{
                        
                    }else{
                        //skipping voucher if student not selected in picker view
                        continue
                    }
                }
                    
                }
                paymentMethodsTrack.append(dic)
                dataAray.append(val)
            }
        }

        ActionSheetStringPicker.show(withTitle: "SELECT PAYMENT", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.paymntFld.text = "\(indexes!)"
            if let selectedTyp = self.paymentMethodsTrack[values]["PaymentId"] as? Int{
                self.selectedPaymentType = "\(selectedTyp)"
            }
       
                self.paymentHeaderViewConstraint.constant = 30
            self.adressFld.placeholder = "Address"
              self.voucherView.isHidden = true
            if  self.selectedPaymentType == "1"//"\(indexes!)" == "Pay By Debit Or Credit Card"
            {
                if currentAcademyOnlinePayment == 2{
                    self.settingFieldForPayAtAcademy()
                    DataManager.sharedInstance.printAlertMessage(message: "Click on SUBMIT for adding Credit Card Details!", view: self)

                    return
                }

                 self.adressFld.text = ""
                 self.adressFld.placeholder = "Address"
               
                if currentAcademyOnlinePayment == 0 || currentAcademyOnlinePayment == 2{//stripe
                    self.paymntViewHeightConstraint.constant = 380
                    self.parentViewHeightConstraint.constant = 1020 + 255             //added 85 to add terms View in footer
                    self.mangoCheckViewHeightCons.constant = 0

                }else if currentAcademyOnlinePayment == 1{
                    self.paymntViewHeightConstraint.constant = 410
                    self.parentViewHeightConstraint.constant = 1260 + 255             //added 85 to add terms View in footer
                    self.mangoCheckViewHeightCons.constant = 30
                }

            self.isPaymentSelected = true
            }else if self.selectedPaymentType == "3"//"\(indexes!)" == "Pay At Academy"
            {
                self.settingFieldForPayAtAcademy()
                
            }else if  self.selectedPaymentType == "2"//"\(indexes!)" == "Voucher"
            {
                self.paymentHeaderViewConstraint.constant = 0
                self.paymntViewHeightConstraint.constant  = 70
                self.adressFld.placeholder = "Select Voucher"
                self.voucherView.isHidden = false
                if currentAcademyOnlinePayment == 1{
                    self.parentViewHeightConstraint.constant = 782 + 255             //added 85 to add terms View in footer

                }else{
                    self.parentViewHeightConstraint.constant = 740 + 255             //added 85 to add terms View in footer

                }
                //self.paymntViewHeightConstraint.constant = 30
                 self.isPaymentSelected = false
                if self.delegatedVocherDic.count > 0  {
                    if let pin = self.delegatedVocherDic["Pin"] as? String{
                        self.voucherBtn.setTitle(pin, for: .normal)
                    }
                }else{
                    if self.isRedeemedVoucher {
                        
                    }else{
                        self.voucherBtn.setTitle("Select Voucher", for: .normal)
                    }
                }

                if self.vochersArray.count < 1{
                    self.voucherBtn.setTitle("No Voucher Available", for: .normal)
                }

            }else if  self.selectedPaymentType == "5"//"\(indexes!)" == "Voucher"
            {
                self.paymentHeaderViewConstraint.constant = 0
                self.paymntViewHeightConstraint.constant = 70
                self.adressFld.placeholder = "Select Package"
                self.voucherView.isHidden = false
                if currentAcademyOnlinePayment == 1{
                    self.parentViewHeightConstraint.constant = 782 + 255             //added 85 to add terms View in footer

                }else{
                    self.parentViewHeightConstraint.constant = 740 + 255             //added 85 to add terms View in footer
                }
                
                if self.delegatedPackageDic.count > 0  {

                if let pin = self.delegatedPackageDic["Pin"] as? String{
                    self.voucherBtn.setTitle(pin, for: .normal)
                    }
                    
                }else{

                    if self.isRedeemedVoucher{
                        
                    }else{
                        self.voucherBtn.setTitle("Select Package", for: .normal)
                        
                    }
                    
                }
                //self.paymntViewHeightConstraint.constant = 30
                self.isPaymentSelected = false
                if self.packagesArray.count < 1{
                    self.voucherBtn.setTitle("No Package Available", for: .normal)
                }

            }else{
                
                if currentAcademyOnlinePayment == 1{
                    self.parentViewHeightConstraint.constant = 782 + 255             //added 85 to add terms View in footer

                }else{
                    self.parentViewHeightConstraint.constant = 740 + 255             //added 85 to add terms View in footer

                }

                self.paymntViewHeightConstraint.constant = 30
                self.isPaymentSelected = false
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    
    //updatedstripe
    func settingFieldForPayAtAcademy()  {
        
        self.paymentHeaderViewConstraint.constant = 0
        self.adressFld.placeholder = "Enter Discount Code"
        self.adressFld.text = ""
        self.paymntViewHeightConstraint.constant = 70
        self.isPaymentSelected = false
        if currentAcademyOnlinePayment == 1{
            self.parentViewHeightConstraint.constant = 782 + 255             //added 85 to add terms View in footer
            
        }else{
            self.parentViewHeightConstraint.constant = 740 + 255             //added 85 to add terms View in footer
            
        }
        //740Removing Select Is Student Junior option

        
    }
    
    @IBAction func paymentBtnAction(_ sender: UIButton) {

        self.monthYearParentView.isHidden = false
        self.monthYearView.selectRow(48, inComponent: 1, animated: false)
        
        return
        
        
    }
    @IBAction func mangocheckBtnAAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            self.mangoPayTermsSelected = false

        }else{
            sender.isSelected = true
            self.mangoPayTermsSelected = true
        }

    }
    @IBAction func agreeBtnAction(_ sender: UIButton) {
        
        var url = ""
        if let titl1 =  DataManager.sharedInstance.currentAcademy()!["MangoPayPDFLink"] as? String{
            url = titl1
        }
        UIApplication.shared.openURL(NSURL(string:url)! as URL)
        dismiss(animated: true, completion: nil)
    }
    
    //called after user selected
    func fillUserInfo()  {
        var fullName = ""
        
        if let fname = delegatedDictionary["FirstName"] as? String{
            fNameFld.text = fname
            self.selectPlayerLbl.text = fname
           fullName = fname
        }
        if let lname = delegatedDictionary["LastName"] as? String{
            lNameFld.text = lname
         fullName = fullName + " " + lname
        }
        self.selectPlayerLbl.text = fullName
        if let email = delegatedDictionary["PersonalEmail"] as? String{
            emailFld.text = email
        }
        if let mobile = delegatedDictionary["Mobile"] as? String{
            phonFl.text = mobile
        }
        
        if  self.isPaymentSelected {
            if currentAcademyOnlinePayment == 1{
                self.parentViewHeightConstraint.constant = 1102 - 142 + 255             //added 85 to add terms View in footer


            }else{
                 self.parentViewHeightConstraint.constant = 1060 - 142 + 255//1120 - 142Removing Select Is Student Junior option
                //added 85 to add terms View in footer

            }

           
            detailViewHeightConstaint.constant
                = 142
        }else{
            
            if currentAcademyOnlinePayment == 1{
                self.parentViewHeightConstraint.constant = 822 - 142 + 255             //added 85 to add terms View in footer


            }else{
                self.parentViewHeightConstraint.constant = 780 - 142 + 255             //added 85 to add terms View in footer


            }
            if self.selectedPaymentType == "3" {
                if currentAcademyOnlinePayment == 1{
                    self.parentViewHeightConstraint.constant = 892 - 142 + 255             //added 85 to add terms View in footer

                    
                }else{
                    self.parentViewHeightConstraint.constant = 850 - 142 + 255            //added 85 to add terms View in footer

                    
                }

            }
            detailViewHeightConstaint.constant
             = 142
            self.paymntFld.text = "SELECT PAYMENT"
        }
        
      
        
        //GDPR
        if currentUserLogin == 4 {
            
        }else{
            fillingPlayerGDPRInfo(gdprDic: delegatedDictionary)
        }


    }
    @IBAction func monthYearBtnAction(_ sender: UIButton) {
        
//        print("\("view is:",self.monthYearView,"view month:",self.monthYearView.month,"month is:",self.monthYearView.months[monthYearView.month],"year is:",monthYearView.year)")
       
        if self.monthYearView.month == 0 {
            
           let date =  DataManager.sharedInstance.getTodayDate()
             let components = date.components(separatedBy: "-")
            self.datePaymntFld.text =  String(format: "%@ %d", self.monthYearView.months[monthYearView.month - 1 + (Int(components[1]))!], (Int(components[0]))!)
        }else{
            self.datePaymntFld.text =  String(format: "%@ %d", self.monthYearView.months[monthYearView.month-1], monthYearView.year)
        }
        monthYearParentView.isHidden = true
    }
    @IBAction func monthYearCancelBtn(_ sender: UIButton) {
        monthYearParentView.isHidden = true

    }
    //if student is login
    func fillUserInfoIfStudent()  {
        var fullName = ""
        
        if let fname = currentStudentInfo["FirstName"] as? String{
            fNameFld.text = fname
            self.selectPlayerLbl.text = fname
            fullName = fname
        }
        
        if let lname = currentStudentInfo["LastName"] as? String{
            lNameFld.text = lname
            fullName = fullName + " " + lname
        }
        
        self.selectPlayerLbl.text = fullName
        
        if let email = currentStudentInfo["PersonalEmail"] as? String{
            emailFld.text = email
        }
        
        if let mobile = currentStudentInfo["Mobile"] as? String{
            phonFl.text = mobile
        }
        
        self.paymntFld.text = "SELECT PAYMENT"
        
        if  self.isPaymentSelected {
            self.parentViewHeightConstraint.constant = 1120 - 133 + 255//1120 - 133Removing Select Is Student Junior option
            //added 85 to add terms View in footer
        }else{
            self.parentViewHeightConstraint.constant = 740 - 133 + 255// 740 - 133Removing Select Is Student Junior option
                        //added 85 to add terms View in footer
        }
        
        if forClassBooking{
            checkingBookedClases()
        }

    }
    
    
    func getCurrentCoachId()->String  {
    
        var coachID1 = ""
        if currentUserLogin == 4{
            print(selectedCoachData)
            if fromRecommnded{
                if let coachId = selectedData["CoachId"] as? Int{
                    coachID1 = "\(coachId)"
                }
            }else{
                if let coachId = selectedCoachData["CoachID"] as? Int{
                    coachID1 = "\(coachId)"
                }
            }
        }else{
            
            if let coachId = DataManager.sharedInstance.currentUser()?["coachId"] as? Int{
                coachID1 = "\(coachId)"
            }
        }

        return coachID1
    }
   
    
    func getLessonPrice() -> Double {
        
        var AmountPaid1 :Double=0.0
        
        if isRedeemedVoucher {
            
            if let price = redeemedData["Amount"] as? Double{
                AmountPaid1 = price
            }
            
        }else{
            if let price = selectedData["Price"] as? String{
                AmountPaid1 = Double(price)!
            }
            
            if fromRecommnded {
                if let price = selectedData["Price"] as? Double{
                    AmountPaid1 = price
                }
            }
        }
       return AmountPaid1
    }
    
    func getLessonTime() -> Int {
        
        var lessonTime = 1
        if fromRecommnded {
            if let duration = selectedData["Duration"] as? String{
                lessonTime =   Int("\(duration)")!
            }

        }else if isRedeemedVoucher{
            
            if let durations = redeemedData["Details"] as? String{
                let components = durations.components(separatedBy: ")")
                //becasue we select time from controller if (15 mint) (20 mint)
                if components.count > 2 {
                    
                    if let id = secondLastControllerData["Id"] as? Int{
                        lessonTime = id
                    }
                    
                }else{
                    
                    if let id = redeemedData["DurationId"] as? Int{
                        lessonTime = id
                    }
                    
                }
            }
            
        }
        else{
        
        if let id = secondLastControllerData["Id"] as? Int{
            lessonTime = id
        }
        
        }
        return lessonTime
    }
    
    
    
    func currentUserInformation() -> String {
        
        var userId1 = "",name1 = "",email1 = "",discription1 = ""
        
        
        if let fname = currentStudentInfo["FirstName"] as? String{
            name1 = fname
        }
        
        //        if let lname = currentStudentInfo["LastName"] as? String{
        ////            name1 = name1 + " " + lname
        //        }
        
        
        if let email = currentStudentInfo["PersonalEmail"] as? String{
            email1 = email
        }
        
        if let player = currentStudentInfo["UserId"] as? Int{
            userId1 = "\(player)"
        }
        
        
        var secureKey = ""
        if let publicKey1 = DataManager.sharedInstance.currentAcademy()?["StandardPrivateKey"] as? String{
            secureKey = "\(publicKey1)"
        }
        
        
        discriptionOfPlayer = "IOS-AcademyId:\(currentAcademyId),UserID:\(userId1),Username:\(name1),Email:\(email1)"
        
        return discriptionOfPlayer
        
    }
    
    
    
    func checkingBookedClases()  {
        
        
        var player1 = ""
        if currentUserLogin == 4{
        if let player = currentStudentInfo["PlayerId"] as? Int{
        player1 = "\(player)"
    }
        }else{
        if let player = currentStudentInfo["PlayerId"] as? String{
            player1 = "\(player)"
            }
            
        }
        let parametersTohit = "classIds=\(classIDs)&PlayerId=\(player1)"
        
        NetworkManager.performRequest(type:.get, method: "Student/CheckIfAlreadyBooked?\(parametersTohit)", parameter:nil, view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                
                return
                
            default:
                break
            }
            
            let bookingArray = object as! [[String:AnyObject]]
            
            var anyBookedClass = false
            

            for item in bookingArray {
                
                if let status1 = item["Status"] as? Bool,status1 == true{
                    anyBookedClass = true
                }
                
            }

            if anyBookedClass{
                
                let alertController = UIAlertController(title: "Error", message: "Kindly Remove already booked classes from you cart!", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
                return
            }

            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    
    //MARK:- stripe booking method
    
    var discriptionOfPlayer = ""
    func creatingStripeCustomer()  {
        
        var userId1 = "",name1 = "",email1 = "",discription1 = ""
        
        
        if let fname = currentStudentInfo["FirstName"] as? String{
            name1 = fname
        }
        
        if let lname = currentStudentInfo["LastName"] as? String{
                    name1 = name1 + " " + lname
                }
        
        
        if let email = currentStudentInfo["PersonalEmail"] as? String{
            email1 = email
        }
        
        if let player = currentStudentInfo["UserId"] as? Int{
            userId1 = "\(player)"
        }
        
        
        var secureKey = ""
        if let publicKey1 = DataManager.sharedInstance.currentAcademy()?["StandardPrivateKey"] as? String{
            secureKey = "\(publicKey1)"
        }
        
        discriptionOfPlayer = "IOS-AcademyId:\(currentAcademyId),UserID:\(userId1),Username:\(name1),Email:\(email1)"
        var parametersTohit = "UserId=\(userId1)&Email=\(email1)&Name=\(name1)&Description=\(discriptionOfPlayer)&StandardPrivateKey=\(secureKey)"
        
         parametersTohit = parametersTohit.replacingOccurrences(of: " ", with: "%20")

//        MBProgressHUD.showAdded(to:self.view, animated: true)
        NetworkManager.performRequest(type:.get, method: "Student/CreateStripeCustomerId?\(parametersTohit)", parameter:nil, view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                
                return
                
            default:
                break
            }
            
            let customerId = object as! String
            
            self.currentStudentInfo["StripeUserId"] = customerId as AnyObject
            
            self.handleAddPaymentOptionButtonTapped()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    
    //updatedstripe
    func handleAddPaymentOptionButtonTapped() {
        
        var publicKey = ""
        if let publicKey1 = DataManager.sharedInstance.currentAcademy()?["StandardPublicKey"] as? String{
            publicKey = "\(publicKey1)"
        }
        
        //        StandardPublicKey
        
        Stripe.setDefaultPublishableKey(publicKey)
        //pk_test_56W88FpvXgf05J3JeS3Nr5RO
        
        let viewController = CardFieldViewController()
        viewController.delegate = self
        //        viewController.theme = theme
        let navigationController = UINavigationController(rootViewController: viewController)
        //        navigationController.navigationBar.stp_theme = theme
        present(navigationController, animated: true, completion: nil)
        
        
    }
    
    func amountToDeductForBooking() -> Double {
        
        var amountToDeduct = 0.0
        
        if discountInfoGotOrNot{
            
            if forClassBooking{
                
                amountToDeduct = totalAmount
            }else{
                amountToDeduct = totalAmount
                
            }
            
        }else{
            
            
            if forClassBooking{
                
                amountToDeduct =  totalAmountForClass
            }else{
                
                amountToDeduct = Double(actualPriceOfLesson) as! Double
                
            }
        }
        
        return amountToDeduct
    }
    
    //updatedstripe
    
     fileprivate func getPaymentIntentSecret(isApplePay: Bool,country:String, completion: ((_ secret: String) -> Void)?) {

            MBProgressHUD.showAdded(to:self.appDelegate.window, animated: true)

            let amountToDeduct = self.amountToDeductForBooking()
            
            var stripeConnectAccount = "",paymentCurrencyAbbrivation = ""
           var orbisStripeFee = 0.00
           var orbisFees = 0.00

        
            if let stripeConnectAccount1 = DataManager.sharedInstance.currentAcademy()?["StripeConnectAccountId"] as? String{
                stripeConnectAccount = "\(stripeConnectAccount1)"
            }
            if let paymentCurrencyAbbrivation1 = DataManager.sharedInstance.currentAcademy()?["PaymentCurrencyAbbrivation"] as? String{
                paymentCurrencyAbbrivation = "\(paymentCurrencyAbbrivation1)"
            }

            if let orbisStripeFee1 = DataManager.sharedInstance.currentAcademy()?["OrbisStripeFee"] as? Double{
                orbisStripeFee = orbisStripeFee1
            }
            

            var stripeUser = ""
            if let stripeCust =  currentStudentInfo["StripeUserId"] as? String,stripeCust != "" {
                stripeUser = stripeCust
            }
            
            
                              var params = [String:String]()
                              
                          
                              var generalcalculatedFee = 0.00

                              var orbisCentProcessingFees = 0.00
                              var orbisProcessingFees = 0.00
                              var eurpeonOrbisProcessingFees = 0.00

                          
            
            //                    if let orbisStripeFee1 = DataManager.sharedInstance.currentAcademy()?["StripeProcessingFee"] as? String{
              //                            orbisProcessingFees = Double(orbisStripeFee1)!
              //                    }
                      
                      if let orbisStripeFee1 = cardReturnData["ProcessingFee"] as? Double{
                          orbisProcessingFees = Double(orbisStripeFee1)
                      }

                      
                                  
              //        if let orbisStripeFee1 = DataManager.sharedInstance.currentAcademy()?["StripeProcessingFeeCents"] as? String{
              //            orbisCentProcessingFees = Double(orbisStripeFee1)!
              //        }
                      
                      if let orbisStripeFee1 = cardReturnData["FixedFee"] as? Double{
                          orbisCentProcessingFees = Double(orbisStripeFee1)
                      }
            
                              if let orbisStripeFee1 = DataManager.sharedInstance.currentAcademy()?["EuropeanStripeProcessingFee"] as? String{
                                  eurpeonOrbisProcessingFees = Double(orbisStripeFee1)!
                                                             }
                              
                              var europCountries = String()
                              if let europCountry = DataManager.sharedInstance.currentAcademy()?["EuropeanStripeCountries"] as? String{
                               europCountries = europCountry
                              }
                              
                          
//                          if europCountries.contains(country){
//                                 generalcalculatedFee = amountToDeduct  * eurpeonOrbisProcessingFees
//
//                              }else{
//
//                                  generalcalculatedFee = amountToDeduct   * orbisProcessingFees
//
//                              }
                   generalcalculatedFee = amountToDeduct   * orbisProcessingFees

                              
                        generalcalculatedFee = generalcalculatedFee + orbisCentProcessingFees
                              

                          
                              if let deductFeeOrNot = DataManager.sharedInstance.currentAcademy()?["IsGatewayChargeFee"] as? Bool,deductFeeOrNot == false{
                                  orbisFees = generalcalculatedFee
                                  orbisFees = Double(String(format: "%.2f", orbisFees))!

                                  params = [
                                               "currency": "\(paymentCurrencyAbbrivation)",
                                               "amount": "\(UInt(amountToDeduct*100))",
                                               "payment_method_types[]": "card",
                                               "on_behalf_of" :"\(stripeConnectAccount)",
                          
                                               "application_fee_amount":"\(UInt(orbisFees * 100))", "transfer_data[destination]":"\(stripeConnectAccount)",
                                               "customer":"\(stripeUser)",
                                               "description": "from \(currentUserInformation())"

                                           ]
                                  
                              }else{
                                  
                                    orbisFees = (amountToDeduct * (1/100) ) + orbisStripeFee + generalcalculatedFee
                                  orbisFees = Double(String(format: "%.2f", orbisFees))!
                                      params = [
                                               "currency": "\(paymentCurrencyAbbrivation)",
                                               "amount": "\(UInt(amountToDeduct*100))",
                                               "payment_method_types[]": "card",
                                               "application_fee_amount":"\(UInt(orbisFees * 100))",
                                               "on_behalf_of" :"\(stripeConnectAccount)",
                                               "transfer_data[destination]":"\(stripeConnectAccount)",
                                               "customer":"\(stripeUser)",
                                               "description": "from \(currentUserInformation())"

                                           ]

                              }

            
            
            
    //        let params = [
    //            "currency": "\(paymentCurrencyAbbrivation)",
    //            "amount": "\(UInt(amountToDeduct*100))",
    //            "payment_method_types[]": "card",
    //            "application_fee_amount":"\(UInt(orbisFees * 100))",
    //            "on_behalf_of" :"\(stripeConnectAccount)",
    //            "transfer_data[destination]":"\(stripeConnectAccount)",
    //            "customer":"\(stripeUser)",
    //            "description": "from \(currentUserInformation())"
    //        ]
    //
            print(params)

            NetworkManager.performRequestForStripe(type: .post, method: "https://api.stripe.com/v1/payment_intents", params: params as [String : String], view: self.view, onSuccess: { (data) in
                
                
                print(data)
                if let dict = data as? [String: Any] {
                    if let errorDict = dict["error"] as? [String: Any] {

                        DataManager.sharedInstance.printAlertMessage(message: (errorDict["message"] as? String)!, view: self)
                    }
                    else if let secret = dict["client_secret"] as? String {
                        print(secret)

                        completion?(secret)
                    }
                }

            }) { (error) in
                
            }
        }
    
    
//    fileprivate func getPaymentIntentSecret(isApplePay: Bool, completion: ((_ secret: String) -> Void)?) {
//        //        sk_test_O2hyDvf5OiWubumDGa15cuuZ00TGvFsHR3
//
//
//
//        let amountToDeduct = self.amountToDeductForBooking()
//
//        var stripeConnectAccount = "",paymentCurrencyAbbrivation = ""
//        var orbisStripeFee = 0.0
//        if let stripeConnectAccount1 = DataManager.sharedInstance.currentAcademy()?["StripeConnectAccountId"] as? String{
//            stripeConnectAccount = "\(stripeConnectAccount1)"
//        }
//        if let paymentCurrencyAbbrivation1 = DataManager.sharedInstance.currentAcademy()?["PaymentCurrencyAbbrivation"] as? String{
//            paymentCurrencyAbbrivation = "\(paymentCurrencyAbbrivation1)"
//        }
//
//
//
//        //        StripeConnectAccountId
//        //        StandardPrivateKey
//        //        StandardPublicKey
//        //        PaymentCurrencyAbbrivation
//        //        OrbisStripeFee
//
//        if let orbisStripeFee1 = DataManager.sharedInstance.currentAcademy()?["OrbisStripeFee"] as? Double{
//            orbisStripeFee = orbisStripeFee1
//        }
//
////        let orbisFees = ((amountToDeduct * 24)/1000 ) + orbisStripeFee
//
//        let orbisFees = (amountToDeduct * (1/100) ) + orbisStripeFee
//
//
//        var stripeUser = ""
//        if let stripeCust =  currentStudentInfo["StripeUserId"] as? String,stripeCust != "" {
//            stripeUser = stripeCust
//        }
//
//        let params = [
//            "currency": "\(paymentCurrencyAbbrivation)",//RestaurantSettings.shared.currencyCode,
//            //            0.7 * 100
//            "amount": "\(UInt(amountToDeduct*100))",
//            "payment_method_types[]": "card",
//            "application_fee_amount":"\(UInt(orbisFees * 100))",
//            "on_behalf_of" :"\(stripeConnectAccount)",
//            "transfer_data[destination]":"\(stripeConnectAccount)",
//            "customer":"\(stripeUser)",
//            "description": "from \(currentUserInformation())"
//        ]
//        //        "{{acct_1EehPuHRNANdyxYR}}"
//        //        "application_fee_amount":"\(0.01 * 100)",
//
//        print(params)
//        //        amount=1099 \
//        //        -d currency=usd \
//        //        -d payment_method_types[]=card \
//        //        -d application_fee_amount=200 \
//        //        -d on_behalf_of="{{CONNECTED_ACCOUNT_ID}}" \
//        //        -d transfer_data[destination]="{{CONNECTED_ACCOUNT_ID}}"
//
//        //        Utils.showLoadingView(withMessage: nil)
//
//        NetworkManager.performRequestForStripe(type: .post, method: "https://api.stripe.com/v1/payment_intents", params: params as [String : String], view: self.view, onSuccess: { (data) in
//
//
//            print(data)
//            if let dict = data as? [String: Any] {
//                if let errorDict = dict["error"] as? [String: Any] {
//                    //                    AlertManager.show(title: kErrorString, message: errorDict["message"] as? String)
//                    DataManager.sharedInstance.printAlertMessage(message: (errorDict["message"] as? String)!, view: self)
//                }
//                else if let secret = dict["client_secret"] as? String {
//                    print(secret)
//
//                    completion?(secret)
//                }
//            }
//
//        }) { (error) in
//
//        }
//
//    }
    
    //updatedstripe
    fileprivate var sourceID: String?
    fileprivate var redirectContext: STPRedirectContext?
    
    fileprivate func confirmPaymentIntent(paymentIntentParams: STPPaymentIntentParams, isApplePay: Bool) {
        
        DispatchQueue.main.async(execute: {
                                       MBProgressHUD.showAdded(to:self.view, animated: true)
                                       
                                     })
        
        let client = STPAPIClient.shared()
        client.confirmPaymentIntent(with: paymentIntentParams, completion: { (paymentIntent, error) in
            
            
            
            if #available(iOS 13.0, *) {
            DispatchQueue.main.async(execute: {
                MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
            }else{
                MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
            }

            if let error = error {
                DataManager.sharedInstance.printAlertMessage(message: error.localizedDescription, view: self)
                self.isStripePaid = false
            }
            else if let paymentIntent = paymentIntent {
                self.sourceID = paymentIntent.sourceId
                print(paymentIntent.stripeId)
                
                if paymentIntent.status == .requiresAction {
                    
                    
                    let block: STPRedirectContextPaymentIntentCompletionBlock = { (str, error) in
                        if error == nil {
                            
//                            print(str)
//                            print(error)
                            self.confirmPayment()

//                            self.isStripePaid = true
//                            self.submitFormOrder()
                            
                            
                        }
                        else {
                            //                            AlertManager.show(title: kErrorString, message: error?.localizedDescription)
                            DataManager.sharedInstance.printAlertMessage(message: error!.localizedDescription, view: self)
                            self.isStripePaid = false
                            
                            
                            
                        }
                    }
                    
                    if let redirectContext = STPRedirectContext(paymentIntent: paymentIntent, completion: block) {
                        self.redirectContext = redirectContext
                        redirectContext.startRedirectFlow(from: self)
                    }
                    else {
                        //                        AlertManager.show(title: kErrorString, message: "Not Supported")
                        DataManager.sharedInstance.printAlertMessage(message:"Error", view: self)
                        self.isStripePaid = false
                        
                        
                    }
                }
                else {
                    //                    AlertManager.show(title: kSuccessString, message: "\(isApplePay ? "Apple Pay" : "Credit Card") succeeded")
                    //                    DataManager.sharedInstance.printAlertMessage(message: "\(isApplePay ? "Apple Pay" : "Credit Card") succeeded", view: self)
                    self.isStripePaid = true
                    self.submitFormOrder()
                    
                }
            }
        })
    }
    
    
    fileprivate var paymentIntentSecret: String?

    
    @objc fileprivate func confirmPayment() {
        
        
        
        NetworkManager.performRequestForStripeConfirm(secret:paymentIntentSecret!,type:.get, method: "https://api.stripe.com/v1/sources/\(self.sourceID!)", params:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (response) in
            print(response!)
            
            let strToCheck = response as! String
            
            if strToCheck == "1"{
                self.isStripePaid = true
                self.submitFormOrder()
            }else if strToCheck == "2"{
                
                DataManager.sharedInstance.printAlertMessage(message: "We are unable to authenticate your payment method. Please choose a different payment method and try again.", view: self)
                
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
        
        
     
    }
    
    var cardReturnData = [String:AnyObject]()

    
    func getCountryOfCard(params:STPCardParams)  {
          
        
           let client2 = STPAPIClient.shared()
          
          client2.createToken(withCard: params) { (token, eror) in
              
              var pros = STPPaymentMethodCardParams()
              pros.token = token?.tokenId
              
              var newOBJ =  STPPaymentMethodParams(card:pros , billingDetails: nil, metadata: nil)
              
              client2.createPaymentMethod(with: newOBJ) { (obj, error) in
                     

                if let coun = obj?.card?.country as? String,coun != ""{
                    
                    var europCountries = String()
                                                                          if let europCountry = DataManager.sharedInstance.currentAcademy()?["EuropeanStripeCountries"] as? String{
                                                                           europCountries = europCountry
                                                                          }
                                                                         
                                                           var countryOfMerchant = ""
                                                        if let coun = DataManager.sharedInstance.currentAcademy()?["MerchantCountry"] as? String{
                                                                countryOfMerchant = coun
                                                                }
                                                                      
                                                                      if europCountries.contains(coun){
                                                                         
                                                                        self.getMerchantCountryFee(country: coun,countryMertchant: countryOfMerchant,cardType:"1", params: params)
                                                                          }else{
                                                                        self.getMerchantCountryFee(country: coun,countryMertchant: countryOfMerchant,cardType:"0", params: params)


                                                                          }
                                                    
                    
                    
                }else{
                    
                      DataManager.sharedInstance.printAlertMessage(message: "Invalid Credit Card Info.", view: self)
                    DispatchQueue.main.async(execute: {
                                                 MBProgressHUD.hideAllHUDs(for:self.appDelegate.window, animated: true )})
                    return
                }
                
            }
          }

          
      }

    
    func getMerchantCountryFee(country:String,countryMertchant:String,cardType:String,params:STPCardParams)  {

                 NetworkManager.performRequest(type:.get, method: "academy/GetMerchantCountryFee/\(countryMertchant)/\(cardType)", parameter:nil, view: (self.appDelegate.window), onSuccess: { (object) in
                     print(object!)
                     switch object {
                     case _ as NSNull:
                         
                         DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                         return
                     // do one thing
                     case _ as [[String:AnyObject]]:

                         break
                     case _ as [String:AnyObject]:
                         DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)

                         return
                     default:
                         break
                     }
                     
                     
                     let cardAray = object as! [[String:AnyObject]]
                     
                     if cardAray.count > 0 {
                         self.cardReturnData = cardAray[0]
                        
                        
                        self.getPaymentIntentSecret(isApplePay: false,country:country) { (clientSecret) in
                            
                            print(clientSecret)
                            //STPCardParams
                            self.paymentIntentSecret = clientSecret
                            let sourceParams = STPSourceParams.cardParams(withCard: params)
                            let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
                            paymentIntentParams.sourceParams = sourceParams
                            paymentIntentParams.returnURL = "GLFLocker://stripe-redirect"
                            self.confirmPaymentIntent(paymentIntentParams: paymentIntentParams, isApplePay: false)
                        }

                        
                        
                         
                     }else{
                     DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)

                     }

                   
                     
                 }) { (error) in
                     print(error!)
                     self.showInternetError(error: error!)
                 }
             }
    
    

}
//MARK:- called when user selected
extension CompleteOrderOldViewController : SelectedStudentDelegate{
    func seletedUserData(dic: NSDictionary,selectedUserId:String) {
        print(dic)
        currentPlayerSelectedId = selectedUserId
        delegatedDictionary = dic as! [String : AnyObject]
        currentStudentInfo = dic as! [String : AnyObject]
        fillUserInfo()
        //getting vochers for selected user
        getVochers()
//        getallPackagesOfSelectedPlayer()
        
        if forClassBooking{
            
            self.checkingBookedClases()
            
        }
        
        for dic in self.paymentMethodsArray{
            if let val = dic["PaymentName"] as? String{
                
                if val == "Package"{
                    if forClassBooking{
                    }else{
                        self.getallPackagesOfSelectedPlayer()
                        break
                    }
                }
            }

    }
    }
    
}
//MARK:- called when voucher selected
extension CompleteOrderOldViewController : VoucherDelegate{
    func seletedVoucherData(dic: NSDictionary) {
        print(dic)
        print(cart)
        if let pin = dic["VoucherPin"] as? String{
           self.voucherBtn.setTitle(pin, for: .normal)
        }
        delegatedVocherDic = dic as! [String : AnyObject]

    }
    
    func seletedPackageData(dic: NSDictionary) {
        print(dic)
        print(cart)
        if let pin = dic["Pin"] as? String{
            self.voucherBtn.setTitle(pin, for: .normal)
        }
        delegatedPackageDic = dic as! [String : AnyObject]
        
    }
}

//MARK:- called when stripe card filled
//updatedstripe
extension CompleteOrderOldViewController : FetchCardDetails{
    
    func fetchingCardDetails(params:STPCardParams) {
        
        print(params.number,params.expYear,params.expMonth,params.cvc)
        
        getCountryOfCard(params: params)

    }
}

 //https://app.glflocker.com/OrbisAppBeta/api/Booking/BookingCartClass/
//http://app.glfbeta.com/OrbisWebApi/api/academy/GetAcademyPlayerList/ //post UserId	5093 AcademyId	73 ClassId	0
//http://app.glfbeta.com/OrbisWebApi/api/academy/GetCountryLists/ //get
// http://app.glfbeta.com/OrbisWebApi/api/Academy/GetPaymentMethods/73/3 // for paymnt
//http://app.glfbeta.com/OrbisWebApi/api/Booking/GetUserVoucher?UserId=5093&keyTofind=15&ProgramTypeId=188

//if student login
//https://app.glflocker.com/OrbisAppBeta/api/Booking/GetUserVoucher?UserId=5097&keyTofind=30&ProgramTypeId=188


