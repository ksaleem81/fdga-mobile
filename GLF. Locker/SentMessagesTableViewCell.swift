//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class SentMessagesTableViewCell: UITableViewCell {

    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var studentImage: UIImageView!
    @IBOutlet weak var discriptionLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    var procesing = UIActivityIndicatorView()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        studentImage.layer.borderWidth = 0.25
        studentImage.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
