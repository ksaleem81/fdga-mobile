//
//  MessagesDetailViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 4/3/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
class MessagesDetailViewController: UIViewController{

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var discriptionLbl: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textfield: UITextField!
    var fromSentItem = false
    var messageData: [String: AnyObject]!
    
    @IBOutlet weak var replyBtnHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var replyUnderLine: UILabel!
    //MARK:- life cycles method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
        print(messageData)
        if fromSentItem {
            replyBtnHeightConstraint.constant = 0
            replyUnderLine.isHidden = true
        }else{
        }
        updateUIWithMessageData()
        settingRighMenuBtn()
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MessagesDetailViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
    }
    
    func updateUIWithMessageData() {
        if let title = messageData["MessageSender"] as? String{
            titleLbl.text = title
        }
        
        if let desc = messageData["Subject"] as? String {
            discriptionLbl.text = desc
        }
        
        if let body = messageData["Body"] as? String {
//            let str = summary.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            textView.text = String(body.filter { !"\n\t\r".contains($0) })
        }

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.textView.setContentOffset(.zero, animated: false)

    }

    // MARK - Buttons Actions
    @IBAction func replyBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ComposeMessageViewController") as! ComposeMessageViewController
        controller.isReply = true
        controller.forwardMessageData = messageData
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func forwardBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ComposeMessageViewController") as! ComposeMessageViewController
        controller.isForward = true
        controller.forwardMessageData = messageData
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func deleteBtnAction(_ sender: UIButton) {
        deleteMessage()
    }
    //MARK:- servers call
    
    func deleteMessage()  {
        
        var messageId = ""
        if let id = self.messageData["MessageId"] as? Int{
            messageId = "\(id)"
        }

        NetworkManager.performRequest(type:.get, method: "Message/DeleteMessage?MessageId=\(messageId)&DeleteForReal=\(false)", parameter:nil, view:self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)

                return
                
            default:
                break
            }
            _ = self.navigationController?.popViewController(animated: true)
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

}


