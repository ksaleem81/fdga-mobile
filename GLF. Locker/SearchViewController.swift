//
//  SearchViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import AVFoundation
import AVKit

@available(iOS 10.0, *)
class SearchViewController: UIViewController,AVPlayerViewControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var searchFld: UITextField!
    let destination = AVPlayerViewController()
    @IBOutlet weak var logoTxtImg: UIImageView!
    @IBOutlet weak var bgImg: UIImageView!
    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
       if currentTarget == "Jay Kelly Golf"  || currentTarget == "Chris Ryan Golf"{
        }else{
         let locationManagerObj = LocationManager.sharedInstance
         locationManagerObj.startUpdatingLocation()
        }
        
        if currentTarget == "Playgolf" {
            logoTxtImg.isHidden = true
            bgImg.image = UIImage.init(named: "Search_PageBgPG")
        }else if currentTarget == "TPC Group" {
            logoTxtImg.isHidden = true
            bgImg.image = UIImage.init(named: "SearchScreenTPC")
        }else if currentTarget == "ClubCorp Locker" {
            logoTxtImg.isHidden = true
            bgImg.image = UIImage.init(named: "corpClubLanding")
            if Utility.isiPhonXR() || Utility.isiPhonX() || Utility.isiPhonXMAX(){
                    bgImg.contentMode = .scaleToFill
            }
        }else if currentTarget == "FDGS" {
            logoTxtImg.isHidden = true
            bgImg.image = UIImage.init(named: "FDGASearch")
        }
        
        searchFld.inputAccessoryView = addToolBar()
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func addToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SearchViewController.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
    @objc func donePressed(){
           movingToNext()
          searchFld.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func movingToNext()  {

        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
        }else{
            return
        }

       let controller = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        controller.academyName = self.searchFld.text!
        controller.academySearchMade = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func gotoAcademyBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        if let text = self.searchFld.text , !text.isEmpty{
            controller.academyName = self.searchFld.text!
            controller.academySearchMade = true

            }else {
            }
        self.navigationController?.pushViewController(controller, animated: true)
        
            }
    
    func playVideo(url:URL,muted:Bool)  {
        
        destination.player = AVPlayer(url: url)
        destination.allowsPictureInPicturePlayback = true
        self.destination.delegate = self
        destination.player?.isMuted = muted
        destination.view.frame = self.view.bounds
        UIView.animate(withDuration: 0.3,
                       delay: 0.2,
                       options: UIViewAnimationOptions.transitionCurlUp,
                       animations: { () -> Void in
                        self.view.addSubview(self.destination.view)
                        self.destination.delegate = self
                        self.destination.player?.play()
        }, completion: { (finished) -> Void in
        })
    
    }
}




/*
 
 
var HUD = MBProgressHUD()
 
 

//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//
//                do {
//                    // Get the directory contents urls (including subfolders urls)
//                    let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
//                    print(directoryContents)
//
//                    // if you want to filter the directory contents you can do like this:
//                    let mp3Files = directoryContents.filter{ $0.pathExtension == "MOV" }
//                    print("mp3 urls:",mp3Files)
//                    let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
//                    print("mp3 list:", mp3FileNames)
//
//                } catch let error as NSError {
//                    print(error.localizedDescription)
//                }

//audioUrl should be of type URL

downloadingVideo(audioUrl: URL(string:"http://devteam.glfbeta.com/Content/MediaFiles/Videos/trim.FE11F083-F24E-4E09-A825-E72EC6901F27-5093-516787562-1000.MOV")!)

func downloadingVideo(audioUrl:URL)  {
    
    self.HUD = MBProgressHUD.init(view: self.view)
    self.view.addSubview(self.HUD)
    self.HUD.mode = MBProgressHUDModeDeterminate
    self.HUD.show(true)
    let audioFileName = String((audioUrl.lastPathComponent)) as NSString
    
    //path extension will consist of the type of file it is, m4a or mp4
    let pathExtension = audioFileName.pathExtension
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        // the name of the file here I kept is yourFileName with appended extension
        documentsURL.appendPathComponent("yourFileName4."+pathExtension)
        return (documentsURL, [.removePreviousFile])
    }
    
    Alamofire.download(audioUrl, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
        print("Progress: \(progress.fractionCompleted)")
        self.HUD.progress = Float(progress.fractionCompleted)
        } .validate().responseData { ( response ) in
            print(response.destinationURL!.lastPathComponent)
            self.HUD.removeFromSuperview()
            self.playVideo(url: response.destinationURL!, muted: false)
    }
    
    //        Alamofire.download(audioUrl, to: destination).response { response in
    //            if response.destinationURL != nil {
    //                print(response.destinationURL!)
    //                self.playVideo(url: response.destinationURL!, muted: false)
    //
    //            }
    //        }
}

*/
