//
//  MediaDetailViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/24/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation
import SideMenu
import WebKit
class MediaDetailViewController: UIViewController {

    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var discriptionLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var recomendMediaBtn: UIButton!
    @IBOutlet weak var deletMediaBtn: UIButton!
    @IBOutlet weak var editMediaBtn: UIButton!
    var selectedUser  = [String:AnyObject]()
    @IBOutlet weak var imageBtn: UIButton!
    var playersArray = [[String:AnyObject]]()
    var mediaType = "1"
    var dataDictionary = [String:AnyObject]()
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coachesFld: UITextField!
    @IBOutlet weak var coachesRecomendViewCons: NSLayoutConstraint!
    var selectedCoaches  = [[String:AnyObject]]()
    var jsonStringForCoaches = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print(dataDictionary)
        if mediaType == "2" {
        playBtn.isHidden = true
            dataDictionary["MediaTypeId"] = "2" as AnyObject?
            recomendMediaBtn.setTitle("RECOMMEND PHOTO", for: .normal)
            self.imageBtn.isHidden = false
        }else if mediaType == "1"{
              playBtn.isHidden = false
            recomendMediaBtn.setTitle("RECOMMEND VIDEO", for: .normal)
            dataDictionary["MediaTypeId"] = "1" as AnyObject?
        }else {
            playBtn.isHidden = false
            recomendMediaBtn.setTitle("RECOMMEND DRILL", for: .normal)
            dataDictionary["MediaTypeId"] = "3" as AnyObject?
        }
        fillingFields()
        settingRighMenuBtn()
        
        if currentUserLogin == 4 {
            buttonsView.isHidden = true
            scrollView.isScrollEnabled = false
            if mediaType == "1"{
                getCoachesInfo()
            }else{
                self.coachesRecomendViewCons.constant = 0

            }
            }else{
            self.coachesRecomendViewCons.constant = 0
            getPlayersList()
        }
    }
    
    

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MediaDetailViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    func fillingFields()  {
        
        if let title
            = dataDictionary["Description"] as? String{
            discriptionLbl.text = title
        }
            var duration = ""
        
        if mediaType == "2" {
        }else{
        
            if let title = dataDictionary["Duration"] as? Int{
                
                let realTime = self.secondsToMinutesSeconds(seconds: title)
                var minutes = "0"
                var seconds = "0"
                if realTime.0 > 9 {
                    minutes = "\(realTime.0)"
                }else{
                    minutes = "0"+"\(realTime.0)"
                }
                
                if realTime.1 > 9 {
                    seconds = "\(realTime.1)"
                }else{
                    seconds = "0"+"\(realTime.1)"
                }
                
                duration = "Duration: " + "\(minutes):\(seconds) | "
            }
            
        }
        
        if let  dateCreate = dataDictionary["CreationDate"] as? String{
            let dateOrignal = dateCreate.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                duration = duration + "Uploaded " + "\( dateOrignal[0])"
            }
            }
        
        durationLbl.text = duration
        
        var fileUrl = ""
        if let url = dataDictionary["ThumbnilURL"] as? String {
            fileUrl = url
        }
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
        fileUrl = baseUrlForVideo + fileUrl
        
        videoImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
            
            DispatchQueue.main.async (execute: {
             
                if let _ = image{
                    
                    self.videoImage.image = image;
                }
                else{
                    self.videoImage.image = UIImage(named:"photoNot")
                    
                }
            });
        })
        
        if mediaType == "1" {
            embedWebViewIfyoutubeVideo()
        }
    }
    
    func secondsToMinutesSeconds (seconds : Int) -> ( Int, Int) {
        return ((seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func embedWebViewIfyoutubeVideo()  {
        

        var fileUrl2 = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl2 = url
        }
        
        if let url = dataDictionary["IsEmbedded"] as? Bool  {
            
            if url {
                MBProgressHUD.showAdded(to:view, animated: true)
                
                
                self.playBtn.isHidden = true
                                let customFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.videoImage.superview?.frame.size.height)!)
                //
               let webConfiguration = WKWebViewConfiguration()
                webConfiguration.allowsInlineMediaPlayback = false
                                
                let videoView = WKWebView(frame: customFrame , configuration: webConfiguration)
                videoView.sizeToFit()
                videoView.invalidateIntrinsicContentSize()
                videoView.frame = customFrame
                videoView.allowsBackForwardNavigationGestures = true
                self.scrollView.addSubview(videoView)
                                
                                            var url2 = fileUrl2
                                            let videoToken = url2.components(separatedBy: "=")
                                            if videoToken.count > 1 {
                                            url2 = "https://www.youtube.com/embed/\(videoToken[1])"
                                            }else{
                                                
                                            let videoToken2 = fileUrl2.components(separatedBy: "/")
                                            if videoToken2.count > 1 {
                                            url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
                                                                     }
                                                             }
                                
                        videoView.loadHTMLString("<iframe width=\"\(self.view.frame.size.width*3)\" height=\"\(self.view.frame.size.height*2)\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>", baseURL: nil)
                                

                                //UIWebView deprecations
                
//                self.playBtn.isHidden = true
//
//                let videoView = UIWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.videoImage.superview?.frame.size.height)!))
//                videoView.allowsInlineMediaPlayback = false
//                videoView.mediaPlaybackRequiresUserAction = false
//                self.videoImage.superview?.addSubview(videoView)
//                videoView.delegate = self
//                var url2 = fileUrl2
//                let videoToken = fileUrl2.components(separatedBy: "=")
//                if videoToken.count > 1 {
//                    url2 = "https://www.youtube.com/embed/\(videoToken[1])"
//                }else{
//                    let videoToken2 = fileUrl2.components(separatedBy: "/")
//                    if videoToken2.count > 1 {
//                        url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
//                    }
//                }
//                print("<iframe width=\"\(self.view.frame.size.width)\" height=\"\((self.videoImage.superview?.frame.size.height)!)\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>")
//                videoView.loadHTMLString("<iframe width=\"\(self.view.frame.size.width)\" height=\"\((self.videoImage.superview?.frame.size.height)!)\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>" , baseURL: nil)
                
            }else{
                
                
            }
        }

    }
    
    func getPlayersList()  {
        
        var playerID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            playerID = "\(userId)"
        }
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "academy/GetPlayerList/\(academyID)/\(playerID)/0", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let object2 = object as! [[String:AnyObject]]
            switch object2 {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
                self.playersArray = (object as? [[String:AnyObject]])!
                break
                
            default:

                return
            }
          
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - buttons actions 
    @IBAction func recomendMediaBtnAction(_ sender: UIButton) {
        moveToPlayerSelection()
    }
    @IBAction func deleteMediaBtnAction(_ sender: UIButton) {
        deleteMedia()
    }
  
    @IBAction func editMediaBtnAction(_ sender: UIButton) {
        
        let uploadMediaVC = storyboard?.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
        uploadMediaVC.forEditingMedia = true
        uploadMediaVC.showTitle = false
       
        uploadMediaVC.mediaInfo = dataDictionary
        navigationController?.pushViewController(uploadMediaVC, animated: true)
    }
    
    @IBAction func coachesBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectStudentViewController") as! SelectStudentViewController
        controller.originaldata = playersArray
        controller.delegateForUpload = self
        controller.fromPlayerMedia = true
        controller.selectedDataArray = selectedCoaches
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    var messageString : String = ""
    
    func valiDateFields(){
        
        if let text = coachesFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Choose Coach \n"
        }
        
    }
    @IBAction func recomendCoachesBtnAction(_ sender: UIButton) {

        messageString = ""
        valiDateFields()
        if messageString.count > 0{
            let name: String = messageString
            let truncated = name.substring(to: name.index(before: name.endIndex))
            
            let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        
        recommendMediaToCoaches()
    }
  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WebVideoPlay" {
            
            let destination = segue.destination as! AVPlayerViewController
            var fileUrl = ""
            if let url = dataDictionary["FileURL"] as? String {
                fileUrl = url
            }
            fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            fileUrl = baseUrlForVideo + fileUrl
            let url = NSURL(string: fileUrl)!
            destination.player = AVPlayer(url: url as URL)
            destination.player?.play()
            
            NotificationCenter.default.addObserver(self, selector:#selector(MediaDetailViewController.playerDidFinishPlaying),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
        }
    }
    
    func moveToPlayerSelection()  {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectStudentViewController") as! SelectStudentViewController
        controller.originaldata = playersArray
        controller.delegate = self
        controller.formediaRecomend = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
     @IBAction func imageBtnAction(_ sender: UIButton) {
     
        let imgPreview = ImagePreview.loadFromXib()
        var fileUrl = ""
        if let url = dataDictionary["ThumbnilURL"] as? String {
            fileUrl = url
        }
        imgPreview.mainImageView.clipsToBounds = true
        imgPreview.mainImageView.contentMode = .scaleAspectFit
        imgPreview.clipsToBounds = true
        imgPreview.contentMode = .scaleAspectFill
        imgPreview.imagePath = fileUrl
        imgPreview.addInView(parentView: self.appDelegate.window!)
     }
    //MARK:- notify when video ends
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        self.dismiss(animated: true, completion:nil)
    }
    
    //MARK:- reccomend media service
    func recommendMedia()  {
        var userID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
        }
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
         var mediaID = ""
        if let userId =  dataDictionary["MediaId"]! as? Int{
            mediaID = "\(userId)"
        }
        
        var curentSelectedPlayer = ""
        if let userId =  selectedUser["PlayerId"]! as? Int{
            curentSelectedPlayer = "\(userId)"
        }
        
        NetworkManager.performRequest(type:.post, method: "Academy/RecommendMediaItems", parameter:["AcademyId":academyID as AnyObject!,"UserId":userID as AnyObject!,"MediaId":mediaID as AnyObject!,"PlayerIds":curentSelectedPlayer as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            if object as! Int == 1 {
                  DataManager.sharedInstance.printAlertMessage(message:"Successfully Recommended Media", view:UIApplication.getTopestViewController()!)
            }else if object as! Int == -1 {
                DataManager.sharedInstance.printAlertMessage(message:"Media Already Recommended", view:UIApplication.getTopestViewController()!)
            }
            else{
                DataManager.sharedInstance.printAlertMessage(message:"Something went wrong", view:self)
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    func recommendMediaToCoaches()  {
        var userID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
        }

        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        var mediaID = ""
        if let userId =  dataDictionary["MediaId"]! as? Int{
            mediaID = "\(userId)"
        }
        
        NetworkManager.performRequest(type:.post, method: "Academy/RecommendMediaItemsToCoach", parameter:["AcademyId":academyID as AnyObject!,"UserId":userID as AnyObject!,"MediaId":mediaID as AnyObject!,"CoachIds":jsonStringForCoaches as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            if object as! Int == 1 {
                self.coachesFld.text = ""
                DataManager.sharedInstance.printAlertMessage(message:"Successfully Recommended Media", view:UIApplication.getTopestViewController()!)
            }else if object as! Int == -1 {
                DataManager.sharedInstance.printAlertMessage(message:"Media Already Recommended", view:UIApplication.getTopestViewController()!)
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"Something went wrong", view:self)
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    
    func deleteMedia()  {
        var mediaId = ""
        if let userId =   dataDictionary["MediaId"]! as? Int{
            mediaId = "\(userId)"
        }
    
        
        NetworkManager.performRequest(type:.post, method: "MediaSection/DeleteMediaItem", parameter:["MediaId":mediaId as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
            break
            }
            _ = self.navigationController?.popViewController(animated: true)
            DataManager.sharedInstance.printAlertMessage(message:"Successfully Deleted Media", view:UIApplication.getTopestViewController()!)
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    
    @IBAction func playVideoAction(_ sender: UIButton) {
     
        playVideo()
    }
    
    func playVideo()  {
        
        var fileUrl = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl = url
        }
        
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
        fileUrl = baseUrlForVideo + fileUrl
    
        let destination = AVPlayerViewController()
        let url = NSURL(string: fileUrl)!
        destination.player = AVPlayer(url: url as URL)
        self.present(destination, animated: true) {
            destination.player?.play()
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(MediaDetailViewController.playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
        
    }
    
    func getCoachesInfo()  {
        //http://app.glfbeta.com/OrbisWebApi/api/Academy/GetPlayerCoaches/4689
        var playerId = ""
        if currentUserLogin == 4 {
            
            if let id = DataManager.sharedInstance.currentUser()!["playerId"] as? Int{
                playerId = "\(id)"
            }
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerCoaches/\(playerId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
            }
            
            let playersArray1 = object as! [[String:AnyObject]]
            self.playersArray = self.noDuplicates(playersArray1 as [[String : AnyObject]]) as [[String : AnyObject]]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    func noDuplicates(_ arrayOfDicts: [[String: AnyObject]]) -> [[String: AnyObject]] {
        var noDuplicates = [[String: AnyObject]]()
        var usedNames = [String]()
        for dict in arrayOfDicts {
            if let name = dict["CoachFullName"], !usedNames.contains(name as! String) {
                noDuplicates.append(dict)
                usedNames.append(name as! String)
            }
        }
        return noDuplicates
    }
    
}


extension MediaDetailViewController : SelectedStudentDelegate{
    func seletedUserData(dic: NSDictionary,selectedUserId:String) {
       self.selectedUser = dic as! [String : AnyObject]
        recommendMedia()
    }
}


extension MediaDetailViewController : SelectedStudentDelegateForUpload{
    func seletedStudentData(selectedData: [[String:AnyObject]],selectedUserId:String){

        
        
        selectedCoaches = selectedData
        print(selectedData)
        jsonStringForCoaches = ""
        var jsonNamesForPlayers = ""
        for index in selectedData {
            //PlayerCoachId
            if let value  =  index["CoachId"] as? Int {
                jsonStringForCoaches = jsonStringForCoaches +  "\(value)" + ","
            }
              if let value  =  index["CoachFullName"] as? String {
                jsonNamesForPlayers = jsonNamesForPlayers + value  + ","
            }
            
        }
       
        if jsonStringForCoaches != "" {
            jsonStringForCoaches = jsonStringForCoaches.substring(to: jsonStringForCoaches.index(before: jsonStringForCoaches.endIndex))
        }
        
        if jsonNamesForPlayers != "" {
            jsonNamesForPlayers = jsonNamesForPlayers.substring(to: jsonNamesForPlayers.index(before: jsonNamesForPlayers.endIndex))
        }
        
         coachesFld.text = jsonNamesForPlayers
        
        print(jsonStringForCoaches)
    }
}

//UIWebView deprecations

//extension MediaDetailViewController :UIWebViewDelegate{
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//
//        print("done")
//        if #available(iOS 13.0, *) {
//        DispatchQueue.main.async(execute: {
//            MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
//        }else{
//            MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
//        }
//
//
//    }
//
//}


//this is updation of uiwebview
//self.playBtn.isHidden = true
//             let customFrame = self.view.bounds
//             let webConfiguration = WKWebViewConfiguration()
//             webConfiguration.allowsInlineMediaPlayback = false
//
//             let videoView = WKWebView(frame: customFrame , configuration: webConfiguration)
//             videoView.sizeToFit()
//             videoView.invalidateIntrinsicContentSize()
//             videoView.frame = customFrame
//             videoView.allowsBackForwardNavigationGestures = true
//
//             self.view.addSubview(videoView)
//             self.view.bringSubview(toFront: self.footerView)
//                             var url2 = fileUrl2
//                             let videoToken = fileUrl.components(separatedBy: "=")
//                                 if videoToken.count > 1 {
//                              url2 = "https://www.youtube.com/embed/\(videoToken[1])"
//                                 }else{
//                                  let videoToken2 = fileUrl2.components(separatedBy: "/")
//                                     if videoToken2.count > 1 {
//                                 url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
//                                     }
//                             }
//
//
//             videoView.loadHTMLString("<iframe width=\"\(self.view.frame.size.width*3)\" height=\"\(self.view.frame.size.height*2)\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>", baseURL: nil)
//
