//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
class AttendiesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
     var latitude = 0.00
     var longitude = 0.00
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var map:GMSMapView?
    @IBOutlet weak var searchFld: UITextField!
    var isLoading = false
    var allDataFetched = false
    let pageSize = 100
    var pageNumber = 1
    var  previousData = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        ////GetAllAcademiesOrderByLatLong
        searchFld.inputAccessoryView = addToolBar()
        searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        settingRighMenuBtn()
            tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        if (tabBarController?.selectedIndex == 2) {
            print("Sent Items")
        }
        getClassAttendees()
    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AttendiesViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
    }
    
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "MessageSender CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        print ("filterd = \(data)")
       self.tableView.reloadData()
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AttendiesViewController.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
        
    }
    
   @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
        print(data.count)
        searchFld.resignFirstResponder()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       self.tabBarController?.navigationItem.title  = "MY MESSAGES"
       self.tabBarController?.navigationController?.isNavigationBarHidden = false
        let view1 = self.tabBarController?.view.viewWithTag(999)
        view1?.isHidden = false
            pageNumber = 1
        self.data.removeAll()
        self.originaldata.removeAll()
        
    }
    
    func refreshTableView()  {

        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        
        let params = ["UserID": userId,
                      "PageNumber": "\(pageNumber)",
                      "PagerSize": "\(pageSize)"]
//        print(params)
        NetworkManager.performRequest(type:.get, method: "Message/GetAllMessages", parameter:params as [String : AnyObject]?, view: UIApplication.getTopestViewController()?.view, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            case _ as [[String:AnyObject]]: break
          
            default:
                     break
            }
            
           let DataReturn = object as! [[String : AnyObject]]
            if DataReturn.count < self.pageNumber {
                self.isLoading = true
            }else{
                self.isLoading = false
            }
            self.data.append(contentsOf: DataReturn)
            self.originaldata =  self.data
            self.tableView.reloadData()
            GlobalClass.sharedInstance().messageRecipients = self.originaldata
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func getClassAttendees()  {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
            self.data = object as! [[String : AnyObject]]
            self.originaldata = object as! [[String : AnyObject]]
            self.tableView.reloadData()
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendiesTableViewCell", for: indexPath) as! AttendiesTableViewCell
        
        if let title = data[indexPath.row]["StudentName"] as? String{
           cell.companyLbl.text = title
        }
        
        if let price1 = data[indexPath.row]["LessonPrice"] as? Double {
            let roundedString = String(format: "%.2f", price1)
            
        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
            
                cell.discriptionLbl.text = "Amount Paid: \(roundedString)\(currencySign)"

            }else{
        
                cell.discriptionLbl.text = "Amount Paid: \(currencySign)\(roundedString)"
            }
        }

        cell.timeLbl.text = ""
        cell.deletBtn.isHidden = true
        cell.deletBtn.tag = indexPath.row
        cell.deletBtn.addTarget(self, action: #selector(AttendiesViewController.deletBtnAction(btn:)), for: .touchUpInside)
      
        if let url = data[indexPath.row]["PhotoURL"] as? String{
            var originalUrl = url.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            
            originalUrl = imageBaseUrl + originalUrl
            cell.studentImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNotSmall"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
                
                DispatchQueue.main.async (execute: {
                    if let _ = image{
                        cell.studentImage.image = image;
                    }
                    else{
                        cell.studentImage.image = UIImage(named:"photoNotSmall")
                    }
                });
            })
        }
        
        return cell
    }
    
    
    @objc func deletBtnAction(btn:UIButton)  {
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    

}

