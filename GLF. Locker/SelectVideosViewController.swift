//
//  SelectVideosViewController.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 16/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
import ActionSheetPicker_3_0
import AVKit
import WebKit
class SelectVideosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, OrbisCameraViewControllerDelegate,
                            GalleryViewControllerDelegate, ShapeViewControllerDelegate, VideoWebViewControllerDelegate {
    
    @IBOutlet weak var buttonsView: UIView!
//    @IBOutlet weak var buttonsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var galleryButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var seperator: UILabel!
    @IBOutlet weak var seperatorSpace: NSLayoutConstraint!
    
    @IBOutlet weak var table: UITableView!
    
    private var dataSource = [[String: Any]]()
    private var mediaInfo = [String: Any]()
   
    var previousData  = [String:AnyObject]()
    var mediaType: OrbisMediaType = kMediaTypeVideo
    @IBOutlet weak var playerLbl: UILabel!
    @IBOutlet weak var heightViewConstraint: NSLayoutConstraint!
    
    var classPlayerData = [[String:AnyObject]]()
    var studentTrackData = [[String:AnyObject]]()
    var selecteStduentId = "0"
    var forclass = false
    
    /************************************************************************************************************/
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if mediaType.rawValue == 1 {
             self.title = "VIDEOS"
        }else if mediaType.rawValue == 2{
             self.title = "PHOTOS"
        }else if mediaType.rawValue == 3{
             self.title = "DRILLS"
        }
        
       
        if (currentUserLogin == 4) {
            buttonsView.frame = CGRect.zero
            buttonsView.isHidden = true
            heightViewConstraint.constant = 0
            if let type = previousData["ProgramType"] as? String {
                if type == "2" {
                     forclass = true//this is class
                }else{
                    forclass = false
                }
            }
        }
        else {
            if let type = previousData["ProgramType"] as? String {
                if type == "2" {//this is class
                    
                    heightViewConstraint.constant = 40
                    getClassPlayer()
                    forclass = true
                    
                }else{
                    heightViewConstraint.constant = 0
                    forclass = false
                    
                }
            }

            if (mediaType == kMediaTypePhoto) {
                self.title = "PHOTOS"
                firstButton.setTitle("TAKE PHOTO", for: .normal)
            }
            
            if (mediaType == kMediaTypeDrill) {
                 self.title = "DRILLS"
                galleryButtonHeight.constant = 0
                galleryButton.isHidden = true
                
                seperatorSpace.constant = 0
                seperator.isHidden = true
                
                buttonsView.frame.size.height = buttonsView.frame.size.height - 47.0
                
            }
        }
        table.tableFooterView = UIView()
        table.reloadData()
        settingRighMenuBtn()
        table.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.table.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(SelectVideosViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
   
   @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

        getLessonMedia()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

    }
    
    func getMediaTypeValue() -> Int {
        return (mediaType == kMediaTypePhoto) ? 1 : 0
    }
    
    /************************************************************************************************************/
    // MARK: - IBActions
    
    @IBAction func takeVideoAction(_ sender: UIButton) {
        
        let options: [String: Any] = ["shouldSaveVideoToGallery": (true),
                                      "videoSource": (1),
                                      "mediaType": getMediaTypeValue()]
        
        let callBackID = Utility.createCallBackID()
        let orbisCameraVC = OrbisCameraViewController.getCamera()
        orbisCameraVC?.delegate = self
        orbisCameraVC?.captureVideo(withCallbackID: callBackID, withOptions: options)
    }
    
    @IBAction func choosFromAppAction(_ sender: UIButton) {
        
        let galleryViewController = GalleryViewController.init(nibName: "GalleryViewController", bundle: Bundle.main)
        galleryViewController.delegate = self
        
        if (mediaType == kMediaTypePhoto) {
            galleryViewController.galleryType = GalleryTypePhoto
            galleryViewController.isFromLessonPhotosPage = true
        }
        else {
            galleryViewController.galleryType = GalleryTypeVideo
            galleryViewController.isFromLessonVideosPage = true
        }        
        
        let navigationController = UINavigationController.init(rootViewController: galleryViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    
    @IBAction func chooseFromMediaAction(_ sender: UIButton) {
        
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VideoWebViewController") as! VideoWebViewController
        controller.delegate = self
        controller.isComeFromSelectVideos = true
        controller.mediaType = "\(mediaType.rawValue)"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func chooseFromDeviceAction(_ sender: UIButton) {
        let options: [String: Any] = ["shouldSaveVideoToGallery": (true),
                                      "videoSource": (0),
                                      "mediaType": getMediaTypeValue()]
        
        let callBackID = Utility.createCallBackID()
        let orbisCameraVC = OrbisCameraViewController.getCamera()
        orbisCameraVC?.delegate = self
        orbisCameraVC?.captureVideo(withCallbackID: callBackID, withOptions: options)
    }
    
    
    @IBAction func playerBtnAction(_ sender: UIButton) {
        
        var dataAray = [String]()
        studentTrackData = [[String:AnyObject]]()
        dataAray.append("ALL")
        let dic = ["0":"StudentID"]
        studentTrackData.append(dic as [String : AnyObject])
        for dic in classPlayerData{
            if let val = dic["StudentName"] as? String{
                studentTrackData.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT PLAYER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.playerLbl.text = "\(indexes!)"
            if let selectedTyp = self.studentTrackData[values]["StudentID"] as? Int{
                self.selecteStduentId = "\(selectedTyp)"
            }
            if "\(indexes!)" == "ALL" {
                self.selecteStduentId = "0"
            }
            self.getLessonMedia()
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    /************************************************************************************************************/
    // MARK: - Network Calls
    
    private func getLessonMedia() {
        
        var lessonTyp = "1"
        var studentId = ""
        if forclass {
            lessonTyp = "2"
            if currentUserLogin == 4 {
                if let id = DataManager.sharedInstance.currentUser()!["playerId"] as? Int{
                    studentId = "\(id)"
                }
            }else{
                studentId = self.selecteStduentId
            }
        }else{
            lessonTyp = "1"
            if let id = previousData["StudentID"] as? Int{
                studentId = "\(id)"
            }
            
        }
        
        let parameters = ["LessonID": previousData["LessonID"]!,
                          "MediaType": mediaType.rawValue,
                          "StudentID": studentId,
                          "LessonType": lessonTyp] as [String : Any]

        print(parameters)
        
        NetworkManager.performRequest(type:.post, method: "Academy/GetMyNewLessonMedia", parameter: parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
                self.dataSource = object as! [[String : Any]]
                self.table.reloadData()
                break
                
            default:
                break
            }
            
        }) { (error) in
            print(error!)
            DataManager.sharedInstance.printAlertMessage(message:(error?.description)!, view:self)
        }
    }
    
    
    private func deleteMedia(at index: Int) {
        
        let videoData = dataSource[index]
        //MediaTypeId, MediaId
        let URL = "DeleteLessonMedia/\(videoData["MediaId"]!)/\(videoData["MediaTypeId"]!)"
        NetworkManager.performRequest(type:.get, method: "Academy/\(URL)", parameter: nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as String:
                let strObj = object as! String
                if (strObj == "1" || strObj == "2" || strObj == "3") {
                    print("Success")
                    self.getLessonMedia()
                }
                break
                
            default:
                break
            }
            
        }) { (error) in
            print(error!)
            DataManager.sharedInstance.printAlertMessage(message:(error?.description)!, view:self)
        }
    }

    func getClassPlayer()  {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
            }
            
            self.classPlayerData = (object as? [[String:AnyObject]])!
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    
    /************************************************************************************************************/
    // MARK: - UItableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoWebTableViewCell") as! VideoWebTableViewCell
        let videoData = dataSource[indexPath.row]
        
        if let title = videoData["Description"] as? String{
            cell.companyLbl.text = title
        }
        
        if let detail = videoData["Details"] as? String {
            cell.detailLabel.text = detail
        }
        else {
            cell.detailLabel.text = ""
        }
        
        if (mediaType == kMediaTypeVideo || mediaType == kMediaTypeDrill), let title = videoData["Duration"] as? Int{
            
                let realTime = self.secondsToMinutesSeconds(seconds: title)
                var minutes = "0"
                var seconds = "0"
                if realTime.0 > 9 {
                    minutes = "\(realTime.0)"
                }else{
                    minutes = "0"+"\(realTime.0)"
                }
                
                if realTime.1 > 9 {
                    seconds = "\(realTime.1)"
                }else{
                    seconds = "0"+"\(realTime.1)"
                }
                
                cell.durationLbl.text = "Duration: " + "\(minutes):\(seconds)"
            

        } else {
            cell.durationLbl.text = ""
        }
        
        var fileUrl = ""
        if let url = videoData["ImageURL"] as? String {
            fileUrl = url
        }
        
        if let url = videoData["IsEmbedded"] as? Bool  {
            
            if url {
                cell.youtubeBtn.isHidden = false
                embedWebViewIfyoutubeVideo(dataDictionary: videoData,cell: cell)
                
            }else{
                cell.youtubeBtn.isHidden = true
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
                fileUrl = baseUrlForVideo + fileUrl}
        }
        
        var placeHolderImage = UIImage(named:"photoNot")
        
        if ((videoData["IsConverted"] as! NSNumber).intValue == 0 && mediaType != kMediaTypePhoto) {
            placeHolderImage = UIImage(named:"videoconversion")
        }
        
        cell.videoImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: placeHolderImage, options: .refreshCached, completed: { (image, error, cacheType, url) in
            DispatchQueue.main.async (execute: {
                if let _ = error {
                    cell.videoImage.image = UIImage(named:"photoNot")
                }
            });
        })
        
        return cell
    }
    
    
  
    func embedWebViewIfyoutubeVideo(dataDictionary:[String:Any],cell:VideoWebTableViewCell)  {
        var fileUrl2 = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl2 = url
        }
        
        if let url = dataDictionary["IsEmbedded"] as? Bool  {
            
            
            if url {
                

                let customFrame =  CGRect(x: cell.videoImage.frame.minX, y: cell.videoImage.frame.minY, width: cell.videoImage.frame.size.width , height: cell.videoImage.frame.height )//cell.videoImage.bounds
                                          let webConfiguration = WKWebViewConfiguration()
                                          webConfiguration.allowsInlineMediaPlayback = false
                             
                                          let videoView = WKWebView(frame: customFrame , configuration: webConfiguration)
                                          videoView.sizeToFit()
                                          videoView.invalidateIntrinsicContentSize()
                                          videoView.frame = customFrame
                                          videoView.allowsBackForwardNavigationGestures = true
                                        
                             cell.contentView.addSubview(videoView)
                                                          var url2 = fileUrl2
                                                          let videoToken = url2.components(separatedBy: "=")
                                                              if videoToken.count > 1 {
                                                           url2 = "https://www.youtube.com/embed/\(videoToken[1])"
                                                              }else{
                                                               let videoToken2 = fileUrl2.components(separatedBy: "/")
                                                                  if videoToken2.count > 1 {
                                                              url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
                                                                  }
                                                          }
                             
                             
                                          videoView.loadHTMLString("<iframe width=\"\(self.view.frame.size.width*3)\" height=\"\(self.view.frame.size.height*2)\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>", baseURL: nil)
                
                
//                let videoView = UIWebView(frame: CGRect(x: cell.videoImage.frame.origin.x, y: cell.videoImage.frame.origin.y, width: cell.videoImage.frame.size.width, height: (cell.videoImage.frame.size.height)))
//                videoView.allowsInlineMediaPlayback = false
//                videoView.mediaPlaybackRequiresUserAction = false
//                cell.addSubview(videoView)
//
//                var url2 = fileUrl2
//                let videoToken = fileUrl2.components(separatedBy: "=")
//                if videoToken.count > 1 {
//                    url2 = "https://www.youtube.com/embed/\(videoToken[1])"
//                }else{
//                    let videoToken2 = fileUrl2.components(separatedBy: "/")
//                    if videoToken2.count > 1 {
//                        url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
//                    }
//                }
//
//                videoView.loadHTMLString("<iframe width=\"\(cell.videoImage.frame.size.width)\" height=\"\((cell.videoImage.frame.size.height))\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>" , baseURL: nil)
                
            }else{
                
                
            }
        }
        
    }
    
    func secondsToMinutesSeconds (seconds : Int) -> ( Int, Int) {
        return ((seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
//        let videoData = dataSource[indexPath.row]
//        if let url = videoData["IsEmbedded"] as? Bool  {
//            if url {
//            }else{
                showActionSheet(at: indexPath.row)
    }
       // }
        
    

    
    
    /************************************************************************************************************/
    // MARK: - ActionSheet Methods
    
    private func showActionSheet(at index: Int) {
        
        let sheet = UIAlertController.init(title: "CHOOSE AN OPTION", message: "", preferredStyle: .actionSheet)
        let playAction = UIAlertAction.init(title: "Play", style: .default) { (a) in
            //deprecations MPMoviePlayerViewController
//            self.playVideoForDepricateMethod(index: index )

            self.playVideo(at: index)
        }
        let viewImageAction = UIAlertAction.init(title: "View Image", style: .default) { (a) in
            self.viewImage(at: index)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        if mediaType == kMediaTypePhoto {
            sheet.addAction(viewImageAction)
        } else {
            if let url = dataSource[index]["IsEmbedded"] as? Bool  {
                if url {
                }else{
                    sheet.addAction(playAction)
                }
            }
        }
        if currentUserLogin == 4 {
            
        }else{
        let deleteAction = UIAlertAction.init(title: "Delete", style: .default) { (a) in
            self.deleteMedia(at: index)
            }
             sheet.addAction(deleteAction)
        }
       
        
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    
    
    /************************************************************************************************************/
    // MARK: - Show Media
    
    private func playVideo(at index: Int) {
        let videoInfo = dataSource[index]
        Utility.showVideoPlayerController(with: videoInfo, presenter: self, animated: true, isOnline: true)
    }
    
    private func viewImage(at index: Int) {
        
        let videoInfo = dataSource[index]
        let imgPreview = ImagePreview.loadFromXib()
        imgPreview.mainImageView.clipsToBounds = true
        imgPreview.mainImageView.contentMode = .scaleAspectFit
        imgPreview.clipsToBounds = true
        imgPreview.contentMode = .scaleAspectFill
        imgPreview.imagePath = videoInfo["ImageURL"] as? String
        imgPreview.addInView(parentView: self.view)
        
    }
    
    
    
    /************************************************************************************************************/
    // MARK: - VideoInfo Handlers
    
    func updatedVideoInfo(info: [AnyHashable: Any], callBack: String?) -> [String: Any] {
        var newInfo = info
        
        if let callBackID = callBack {
            newInfo["callBackID"] = callBackID
        }
        newInfo["lessonID"] = previousData["LessonID"]
        newInfo["studentID"] = (0)
        
        if forclass {
                newInfo["studentID"] = self.selecteStduentId
        }else{
            if ((previousData["LessonType"] as! NSNumber).intValue != 1) {
                newInfo["studentID"] = previousData["StudentID"]
            }
        }
        
        return newInfo as! [String : Any]
    }
    
    private func showUploadVC(info: [String: Any], isFromSelectMedia: Bool) {
        let uploadVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
        uploadVC.mediaInfo = info
        uploadVC.studentData = previousData
        uploadVC.mediaType = mediaType
        if forclass {
            uploadVC.forClass = true
        }
        if (isFromSelectMedia == true) {
            uploadVC.showSelectCategory = false
            uploadVC.showTitle = false
        }
        
        self.navigationController?.pushViewController(uploadVC, animated: true)
    }
    
    
    
    /************************************************************************************************************/
    // MARK: - OrbisCameraViewController Delegate
    
    func orbisCameraVC(_ controller: OrbisCameraViewController!, didFinishSavingVideo info: [AnyHashable : Any]!, withCallBackID callBackID: String!) {
        let newInfo = updatedVideoInfo(info: info, callBack: callBackID)
        Utility.showShapeViewController(with: newInfo, presenter: self, animated: true, delegate: self)
    }
    
    func orbisCameraVC(_ controller: OrbisCameraViewController!, didFinishCapturingImage info: [AnyHashable : Any]!, withCallBackID callBackID: String!) {
        let newInfo = updatedVideoInfo(info: info, callBack: callBackID)
        showUploadVC(info: newInfo, isFromSelectMedia: false)
    }
    
    
    /************************************************************************************************************/
    // MARK: - GalleryViewController Delegate
    
    func galleryViewControllerDidSelectedVideo(_ videoInfo: [AnyHashable : Any]!) {
        let newInfo = updatedVideoInfo(info: videoInfo, callBack: nil)
        showUploadVC(info: newInfo, isFromSelectMedia: false)
    }
    
    func galleryViewControllerDidSelectedImage(_ imageInfo: [AnyHashable : Any]!) {
        let newInfo = updatedVideoInfo(info: imageInfo, callBack: nil)
        showUploadVC(info: newInfo, isFromSelectMedia: false)
    }
    
    
    
    /************************************************************************************************************/
    // MARK: - VideosWebViewController Delegate
    
    func videosWebViewControllerDidSelectedVideo(info: [String : Any], skillCategory: String, skillType: String) {
        let newInfo = updatedVideoInfo(info: info, callBack: nil)
        print("Info: \(newInfo)")
        showUploadVC(info: newInfo, isFromSelectMedia: true)
    }

    /************************************************************************************************************/
    // MARK: - ShapeViewController Delegate
    
    func shapeViewController(_ controller: ShapeViewController!, didSelectSavetoLesson videoInfo: [AnyHashable : Any]!) {
        showUploadVC(info: videoInfo as! [String : Any], isFromSelectMedia: false)
    }
    
    
    
    // MARK: - playing video changed

    func playVideoForDepricateMethod(index:Int)  {
           
        
        let dataDictionary = dataSource[index] as! [String:AnyObject]

           var fileUrl = ""
           if let url = dataDictionary["FileURL"] as? String {
               fileUrl = url
           }
           
                  fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
               fileUrl = baseUrlForVideo + fileUrl
           
           
           
           let destination = AVPlayerViewController()
        

           let url = NSURL(string: fileUrl)!
           destination.player = AVPlayer(url: url as URL)
           self.present(destination, animated: true) {
                destination.player?.play()
           }
          
           NotificationCenter.default.addObserver(self, selector:#selector(VideoViewController.playerDidFinishPlaying),
                                                  name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
           
       }
       
       //playVideo
       
       func playerItemDidReachEnd(notification: NSNotification) {
           let p: AVPlayerItem = notification.object as! AVPlayerItem
           p.seek(to: kCMTimeZero)
       }

    
       @objc func playerDidFinishPlaying(note: NSNotification) {
           print("Video Finished")
           self.dismiss(animated: true, completion:nil)
       }

}


//this is updation of uiwebview
//self.playBtn.isHidden = true
//             let customFrame = self.view.bounds
//             let webConfiguration = WKWebViewConfiguration()
//             webConfiguration.allowsInlineMediaPlayback = false
//
//             let videoView = WKWebView(frame: customFrame , configuration: webConfiguration)
//             videoView.sizeToFit()
//             videoView.invalidateIntrinsicContentSize()
//             videoView.frame = customFrame
//             videoView.allowsBackForwardNavigationGestures = true
//
//             self.view.addSubview(videoView)
//             self.view.bringSubview(toFront: self.footerView)
//                             var url2 = fileUrl2
//                             let videoToken = fileUrl.components(separatedBy: "=")
//                                 if videoToken.count > 1 {
//                              url2 = "https://www.youtube.com/embed/\(videoToken[1])"
//                                 }else{
//                                  let videoToken2 = fileUrl2.components(separatedBy: "/")
//                                     if videoToken2.count > 1 {
//                                 url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
//                                     }
//                             }
//
//
//             videoView.loadHTMLString("<iframe width=\"\(self.view.frame.size.width*3)\" height=\"\(self.view.frame.size.height*2)\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>", baseURL: nil)
//
