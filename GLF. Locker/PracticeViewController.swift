//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
class PracticeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var dataComingLessons:[[String:AnyObject]] = [[String:AnyObject]]()
    var dataPreviosLessons:[[String:AnyObject]] = [[String:AnyObject]]()
    var dataGeneral:[[String:AnyObject]] = [[String:AnyObject]]()
    var upComing = false
    var studentData = [String:AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    var fromDashBoard = false
    @IBOutlet weak var footerHeigthViewConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print(studentData)
        if upComing {
                
            }else{
           previousBtnAction(UIButton())
            }
        settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PracticeViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
     

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    
    func refreshTableViewForRecent()  {
    
        var studentId = ""
        if let id = studentData["playerId"] as? Int{
            studentId = "\(id)"
        }
    
   //     http://app.glfbeta.com/OrbisWebApi/api/Academy/GerPlayerPracticedDrills/4689
        NetworkManager.performRequest(type:.get, method: "Academy/GerPlayerPracticedDrills/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
          
            default:
                     DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                     return
                
                
              
            }

            self.dataComingLessons = object as! [[String : AnyObject]]
           self.dataGeneral = self.dataComingLessons
            self.tableView.reloadData()
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func refreshTableViewForWorking()  {
       
       //http://app.glfbeta.com/OrbisWebApi/api/Academy/GerPlayerRecommendedDrills/4689
        var studentId = ""
        if let id = studentData["playerId"] as? Int{
            studentId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GerPlayerRecommendedDrills/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.dataPreviosLessons = object as! [[String : AnyObject]]
             self.dataGeneral = self.dataPreviosLessons
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func notPracticeToday(btn:UIButton)  {
        
       
        var studentId = ""
        if let title = dataGeneral[btn.tag]["RecommendationId"] as? Int{
            studentId = "\(title)"
        }

        NetworkManager.performRequest(type:.get, method: "Academy/SetPlayerPractice/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
           if object as! String == "1"{
                self.refreshTableViewForWorking()
           }else{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataGeneral.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        if upComing {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PracticeTableViewCell", for: indexPath) as! PracticeTableViewCell
            
                    if let title = dataGeneral[indexPath.row]["ClassName"] as? String{
                        cell.drilLbl.text = title
                    }
                    if let sTime = dataGeneral[indexPath.row]["DrillDetail"] as? String{
                            cell.marksLbl.text = sTime
                    }
            cell.practiceBtn.tag = indexPath.row
            cell.practiceBtn.addTarget(self, action: #selector(PracticeViewController.notPracticeTodayBtnAction(btn:)), for: .touchUpInside)
         
            if let title = dataGeneral[indexPath.row]["IsPracticedToday"] as? Int{
                if title == 1 {
                    cell.practiceBtn.setTitle("Practice Today", for: .normal)
                }else{
                    cell.practiceBtn.setTitle("Not Practiced Today", for: .normal)
                }
            }
            return cell
         
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentPracticeTableViewCell", for: indexPath) as! RecentPracticeTableViewCell
            
                        if let title = dataGeneral[indexPath.row]["ClassName"] as? String{
                            cell.drilLbl.text = title
                        }
            
                        if let date = dataGeneral[indexPath.row]["DrillDate"] as? String{
                                cell.dateLbl.text = date
                        }
            
                        if let sTime = dataGeneral[indexPath.row]["DrillDetail"] as? String{
                                cell.marksLbl.text =  sTime
                        }
            return cell

        }

     return UITableViewCell()
        

    }
    
    @objc func notPracticeTodayBtnAction(btn:UIButton)  {
        notPracticeToday(btn: btn)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        
    }
    
    //MARK:- Buttons Actions
    @IBOutlet weak var previousView: UIView!
   
    @IBOutlet weak var nextView: UIView!
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        
         upComing = false
        self.nextView.backgroundColor = UIColor.init(red: 199.0/255, green: 209/255, blue: 44.0/255, alpha: 1.0)
        self.previousView.backgroundColor = UIColor.init(red: 55.0/255, green: 55/255, blue: 55.0/255, alpha: 1.0)
        
        if upComing {
            
        }else{
            self.refreshTableViewForRecent()
        }
    }

    @IBAction func previousBtnAction(_ sender: UIButton) {
        
        upComing = true
         self.previousView.backgroundColor = UIColor.init(red: 199.0/255, green: 209/255, blue: 44.0/255, alpha: 1.0)
         self.nextView.backgroundColor = UIColor.init(red: 55.0/255, green: 55/255, blue: 55.0/255, alpha: 1.0)
        if upComing {
               self.refreshTableViewForWorking()
        }else{
        }
    }
    
}

