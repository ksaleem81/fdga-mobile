//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
protocol VoucherDelegate {
    func seletedVoucherData(dic:NSDictionary)
    func seletedPackageData(dic:NSDictionary)

    
}

class VoucherViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
    var delegate : VoucherDelegate?

    @IBOutlet weak var tableView: UITableView!
    var forVocher = false
    var forPackage = false
    var fromBookingScreen = false
    
    @IBOutlet weak var searchFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
       data = originaldata
       settingRighMenuBtn()
       tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        if forPackage {
            self.title = "Package Selection"
        }
    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(VoucherViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }
    
       override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherTableViewCell", for: indexPath) as! VoucherTableViewCell
       
        if forPackage {
            
            if let title = data[indexPath.row]["Pin"] as? String{
                cell.voucherPin.text = title
            }
            if let title = data[indexPath.row]["TotalCredits"] as? Double{
                cell.valueLbl.text =  "\(title)"
            }
            
            if let title = data[indexPath.row]["RemainingCredits"] as? Double{
                cell.detailLbl.text = "\(title)"
            }
            if let title = data[indexPath.row]["ProgramTypeName"] as? String{
                cell.lessonValueLbl.text = "\(title)"
            }
            if let title = data[indexPath.row]["CoachName"] as? String{
                cell.remainingValueLbl.text = "\(title)"
            }
            
            cell.remainingLbl.textAlignment = .left
            cell.lessonTypLbl.textAlignment = .left
            cell.detailHeadLbl.textAlignment = .left
            cell.valueHeadLbl.textAlignment = .left
            cell.voucherHeadLbl.textAlignment = .left
            cell.remainingLbl.text = "Coach Name"
            cell.lessonTypLbl.text = "Lesson Type"
            cell.detailHeadLbl.text = "Remaining Credits"
            cell.valueHeadLbl.text = "Total Credits"
            cell.voucherHeadLbl.text = "Package Code"

        }else{
        
        if let title = data[indexPath.row]["VoucherPin"] as? String{
           cell.voucherPin.text = title
        }
        if let title = data[indexPath.row]["Amount"] as? Double{
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                cell.valueLbl.text =  "\(title)" + "\(currencySign)"

            }else{
                cell.valueLbl.text =  "\(currencySign)" + "\(title)"
                
            }
        }

        if let title = data[indexPath.row]["Details"] as? String{
            cell.detailLbl.text = title
            }
            cell.remainingLbl.text = ""
            cell.lessonTypLbl.text = ""
            cell.remainingValueLbl.text = ""
            cell.lessonValueLbl.text = ""

            
        }
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action:#selector(selectCheckBtn(sender:)) , for:.touchUpInside)
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if forPackage {
            return 135.0
        }else{
            return 120.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    
    }
    
    @objc func selectCheckBtn(sender:UIButton)  {
        
        let indexPath =  IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! VoucherTableViewCell
        
        if cell.radioBtn.isSelected {
            
            cell.radioBtn.isSelected = false
        }else{
            
            cell.radioBtn.isSelected = true
            if forPackage{
                if fromBookingScreen{
                self.delegate?.seletedPackageData(dic:  data[sender.tag] as NSDictionary)
                }else{
                self.delegate?.seletedVoucherData(dic:  data[sender.tag] as NSDictionary)
                    
                }


            }else{
                self.delegate?.seletedVoucherData(dic:  data[sender.tag] as NSDictionary)
                
            }
           _ = self.navigationController?.popViewController(animated: true)
//            getUserInfo(tag: cell.radioBtn.tag)
        }
    }
    
    func getUserInfo(tag:Int)  {
     
        var playerID = ""
        if let id = data[tag]["PlayerId"] as? Int{
            playerID = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "academy/GetPlayerData/\(playerID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            let data = object as! [[String : AnyObject]]
            self.delegate?.seletedVoucherData(dic: data[0] as NSDictionary)
           _ = self.navigationController?.popViewController(animated: true)
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

}


