
//
//  ComposeTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 4/4/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class ComposeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
