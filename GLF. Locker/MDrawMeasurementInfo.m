//
//  MDrawMeasurementInfo.m
//  MDraw
//
//  Created by Gao Yongqing on 9/5/14.
//  Copyright (c) 2014 Motic China Group Co., Ltd. All rights reserved.
//

#import "MDrawMeasurementInfo.h"
//#import "AppDelegate.h"

@implementation MDrawMeasurementInfo

@synthesize tool = _tool;

- (id)init
{
    [NSException raise:@"InitException" format:@"Should call initWithTool to create an instance."];
    
    return nil;
}

- (instancetype)initWithTool:(MDrawTool *)aTool
{
    if (self = [super init]) {
        _tool = aTool;
        if (self.delObj == nil) {
            self.delObj = [GlobalClass sharedInstance];
        }
        
    }
    
    return self;
}

- (void)draw:(CGContextRef)ctx
{
    NSString *text = self.tool.measureText;
    self.delObj = [GlobalClass sharedInstance];
    
    if (self.delObj.changeVal == 0) {
        if ([text intValue] < self.delObj.degree)
        {
            
            text = [NSString stringWithFormat:@"%i",[text intValue]+1];
            
            
        }
        else if ([text intValue] > self.delObj.degree)
        {
            
            text = [NSString stringWithFormat:@"%i",[text intValue]-1];
            
        }
    }

    
  //  NSMutableAttributedString *AttText =  [[NSMutableAttributedString alloc]initWithString:self.tool.measureText];
  //  [AttText setAttributes:@{ NSForegroundColorAttributeName:[UIColor clearColor]} range:NSMakeRange(0, 20)];

    
    if(text == Nil)
    {
        return;
    }
    
    CGSize textSize = CGSizeZero;
    UIFont *textFont = [UIFont systemFontOfSize:17];
    
    if(text != Nil){
        
        textSize= [text sizeWithFont:textFont
                   constrainedToSize:CGSizeMake(1024, 99999.0)
                       lineBreakMode:NSLineBreakByWordWrapping];
    }

    CGRect textFrame = [self calculateTextFrameWithTextSize:textSize forToolFrame:self.tool.frame];
    CGRect textBgFrame = CGRectMake(textFrame.origin.x - 10, textFrame.origin.y - 10, textFrame.size.width + 20, textFrame.size.height + 20);
    CGContextSetFillColorWithColor(ctx, [UIColor clearColor].CGColor);
    
    CGContextFillRect(ctx, textBgFrame);
    
    [text drawWithRect:textFrame options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: textFont,NSForegroundColorAttributeName:[UIColor redColor]} context:Nil];
    
}

-(CGRect)calculateTextFrameWithTextSize:(CGSize)textSize forToolFrame:(CGRect)toolFrame
{
    CGPoint point1;
    CGPoint point2 ;
    CGPoint point3 ;
    
 
    if (self.delObj.pointsA.count> 0) {
         point1 = [[self.delObj.pointsA objectAtIndex:0] CGPointValue];
         point2 = [[self.delObj.pointsA objectAtIndex:1] CGPointValue];
         point3 = [[self.delObj.pointsA objectAtIndex:2] CGPointValue];
        
    }

    CGRect textFrame = CGRectZero;
  //  CGPoint midPoint = CGRectMid(toolFrame);
  //  textFrame = CGRectMake(midPoint.x - textSize.width / 2.0f, toolFrame.origin.y - textSize.height - MARGIN, textSize.width, textSize.height);
    
    textFrame = CGRectMake(point2.x-20, point2.y-10, textSize.width, textSize.height);

    
    return textFrame;
    
}

@end
