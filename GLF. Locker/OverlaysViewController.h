//
//  OverlaysViewController.h
//  Orbis
//
//  Created by Nasir Mehmood on 08/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "ShapeViewController.h"
@interface OverlaysViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
{

    ShapeViewController *svc;
}

@property (nonatomic, strong) NSArray *overlaysArray;
@property (nonatomic, strong) NSString *thumbImagePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil thumbnailImagePath:(NSString*)thumbnailImagePath overlaysArray:(NSArray*)overlaysarray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil thumbnailImagePath:(NSString*)thumbnailImagePath overlaysArray:(NSArray*)overlaysarray presentingController:(UIViewController*)presentingController;

@property UIDeviceOrientation dOrientation;

@end
