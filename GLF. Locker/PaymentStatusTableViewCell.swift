//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit


class PaymentStatusTableViewCell: UITableViewCell,UITextFieldDelegate {


    @IBOutlet weak var pamentStatusLbl: UILabel!
    @IBOutlet weak var markBtn: UIButton!
   
    @IBOutlet weak var pamentTypeLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
   
        // Initialization code
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
         }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
