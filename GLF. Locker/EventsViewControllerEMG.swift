//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

import SDWebImage
import SideMenu
//import Crashlytics

class EventsViewControllerEMG: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var dataSource = [String: AnyObject]()
    var timeArray = [Date]()
    @IBOutlet weak var tableView: UITableView!
    var secondLastControllerData = [String:AnyObject]()
    var selectedDate = NSDate()
    var selectedDateString = ""
    @IBOutlet weak var dateLbl: UILabel!
    var academyScheduleData = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
     self.dateLbl.text = DataManager.sharedInstance.getFormatedDate(date:selectedDateString,formate: "yyyy-MM-dd")
        //dynamicChange
        if isNewUrl{
        }else{
            appendDataSource(timeArray: true, data: nil, startTime: "07:00:00", endTime: "22:00:00")
            
        }
        settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        if Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() {
            dateLbl.superview?.translatesAutoresizingMaskIntoConstraints = true
            dateLbl.superview?.frame  = CGRect.init(x: 0, y: 85, width: (dateLbl.superview?.frame.size.width)!, height: (dateLbl.superview?.frame.size.height)!)
        }

    }
    
    func settingRighMenuBtn()  {

        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(EventsViewControllerEMG.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func getLesssonsAndEvents()  {
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }

        var urlStr = ""

        urlStr =  "\(selectedDateString)/\(selectedDateString)/\(userId)"
        NetworkManager.performRequest(type:.get, method: "academy/GetBookedLessonsAndEventsForCalendar/\(urlStr)".trimmingCharacters(in: .whitespaces), parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            case _ as [String:AnyObject]:
                self.tableView.reloadData()
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                break
            }
            
            let array = object as! [[String : AnyObject]]
            
            var array1 =  [[String : AnyObject]]()
            var array2 =  [[String : AnyObject]]()
            
            
            for data  in array {
                //crocodile
//                if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == true{
                if currentCoachIdCoachLogin == "\(String(describing: data["CoachID"] as! Int))"{
                    
                    var dataCopy = [String:AnyObject]()
                    dataCopy = data
                    //dynamicChange
                    if isNewUrl{

                        dataCopy = self.arrangingDataForFullLeaveOnly(data: data)
                        
                    }
                    array1.append(dataCopy)

                }else{
                        array2.append(data)
                }
                
            }
            
            let newAray = array2 + array1
            
            //dynamicChange
            if isNewUrl{

                if newAray.count < 1 {
                    self.parseData(object: newAray)
                    return
                }
                
            }

            self.parseData(object: newAray)
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
        
    }
    
    func arrangingDataForFullLeaveOnly(data:[String:AnyObject])->[String:AnyObject]  {
        
        var dataCopy = [String:AnyObject]()
        dataCopy = data
        if let type =  data["Notes"] as? String, type == "Full Day Leave"{
            
            if let startTime =  self.academyScheduleData["AcademyStartTime"] as? String  {
                
                if startTime.count < 6{
                    dataCopy["StartTime"] = startTime + ":00" as AnyObject
                    
                }else{
                    dataCopy["StartTime"] = startTime  as AnyObject
                }
                
            }
            if let endTime =  self.academyScheduleData["AcademyEndTime"] as? String  {
                
                if endTime.count < 6{
                    dataCopy["EndTime"] = endTime + ":00" as AnyObject
                }else{
                    dataCopy["EndTime"] = endTime  as AnyObject
                }
            }
        }
        
        return dataCopy
        
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        if isNewUrl{
           creatingSlotsForDay()
            
        }else{
            getLesssonsAndEvents()

        }


    }
    
    func creatingSlotsForDay()  {
        
        //dynamicChange
        Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(selectedDateString,dateFormater:"yyyy-MM-dd"))", controller: self, Success: { data  in
            
            self.academyScheduleData = data as! [String : AnyObject]
            guard let startTime =  self.academyScheduleData["AcademyStartTime"] as? String else {
                return
            }
            
            guard let endTime =  self.academyScheduleData["AcademyEndTime"] as? String else {
                return
            }
            
            if startTime.count > 5 || endTime.count > 5 || startTime.count < 5 || endTime.count < 5{
             DataManager.sharedInstance.printAlertMessage(message: "Time formate is incorrect from admin", view: self)
                return
            }
            self.timeArray.removeAll()
            self.appendDataSource(timeArray: true, data: nil, startTime: "\(startTime):00", endTime: "\(endTime):00")
            self.tableView.reloadData()
            self.getLesssonsAndEvents()

        }, onFailure: {(error) in
            
        })
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func arrowRightBtnAction(_ sender: UIButton) {
        let today = selectedDate
        selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: today as Date)! as NSDate
        print(selectedDate)
        //dynamicChange
        self.dateLbl.text = gettingStringFromDate(date: selectedDate)
        if isNewUrl{
            creatingSlotsForDay()
        }else{
            getLesssonsAndEvents()
        }
    }
   
    @IBAction func arrowLeftBtnAction(_ sender: UIButton) {
        let today = selectedDate
        selectedDate = Calendar.current.date(byAdding: .day, value: -1, to: today as Date)! as NSDate
        print(selectedDate)
        //dynamicChange
        self.dateLbl.text = gettingStringFromDate(date: selectedDate)
        if isNewUrl{
            creatingSlotsForDay()
        }else{
            getLesssonsAndEvents()
        }
    }
    
    func gettingStringFromDate(date:NSDate) -> String {
        
        let formatter = DateFormatter()
      ///this is you want to convert format
          formatter.dateFormat = "yyyy-MM-dd"
        selectedDateString = formatter.string(from: date as Date)
        let timeOrigEnd = selectedDateString.components(separatedBy: " ")
        selectedDateString = timeOrigEnd[0]
        formatter.dateFormat = "EEE MMM dd , yyyy"
        let timeStamp = formatter.string(from: date as Date)
        return timeStamp
        
    }
    
    /************************************************************************************************************/
    // MARK: - Private Methods
    
    private func appendDataSource(timeArray isTmArr: Bool, data: [String: AnyObject]?, startTime: String, endTime: String) {
        
        
        if startTime.count < 8 || endTime.count < 8 || startTime.count > 8 || endTime.count > 8  || startTime == "" || endTime == "" {
            print("this is here")
            return
        }
        
        let startTime = getTimeFromString(timeString: startTime)
        let endTime = getTimeFromString(timeString: endTime)
        
        if (startTime >= endTime) {
            return
        }
        
        let components = Calendar.current.dateComponents([.hour, .minute], from: startTime, to: endTime)
        var minutes = components.hour! * 60
        minutes += components.minute!
        
        for i in 0..<(minutes/15) {
            
            let index = i*15
            let newDate = startTime.addingTimeInterval(TimeInterval(index*60))
            
            if (isTmArr == true) {
                timeArray.append(newDate)
            }
            else {
                let newDateString = getStringFromTime(time: newDate, showAM_PM: false)
                if (self.dataSource[newDateString] == nil || data?["Notes"] as! String != "AVAILABLE") {
                    self.dataSource[newDateString] = data as AnyObject?
                }
            }
        }
        
    }
    
    func parseData(object: [[String:AnyObject]]) {
    
        dataSource.removeAll()
        for data in object {
            
            appendDataSource(timeArray: false,
                             data: data,
                             startTime: data["StartTime"] as! String,
                             endTime: data["EndTime"] as! String)
        }
        self.tableView.reloadData()
    }
    
    func getTimeFromString(timeString: String) -> Date {
        
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "HH:mm:ss"
        if globalDateFormate == "dd/MM/yyyy" {
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }
        return dateFormatter.date(from: timeString)!

    }
    
    func getStringFromTime(time: Date, showAM_PM show: Bool) -> String {
        
        let dateFormatter = DateFormatter.init()
        if globalDateFormate == "dd/MM/yyyy" {
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }
        dateFormatter.dateFormat = (show == true) ? "hh:mm a" : "HH:mm:ss"
        return dateFormatter.string(from: time)
        
    }

 //MARK:- tableview dataSource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if timeArray.count == indexPath.row{
         return 70
        }else{
            return 25.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return timeArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == timeArray.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventsEndTableViewCellEMG", for: indexPath) as! EventsEndTableViewCellEMG
            cell.viewTodayBtn.addTarget(self, action: #selector(EventsViewControllerEMG.todayDateBtnAction(btn:)), for: .touchUpInside)
            return cell
        }else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "ChartCellEMG") as? ChartCellEMG
            if (cell == nil) {
                cell = Bundle.main.loadNibNamed("ChartCellEMG", owner: self, options: nil)?.last as? ChartCellEMG
            }
            //tortoise
            cell!.otherStudioImgWidthCons.constant = 0
            cell!.lesnTypImgwidCons.constant = 0

            let timeString = getStringFromTime(time: timeArray[indexPath.row], showAM_PM: true)
            
            if (indexPath.row % 4 == 0) {
                if timeString == "12:00 PM"{
                    cell?.timeLabel.text = "NOON"
                    
                }else if timeString == "07:00 AM"{
//                    cell?.timeLabel.text = ""
                    cell?.timeLabel.text = timeString

                }
                else{
                    cell?.timeLabel.text = timeString
                }
                
            } else {
                cell?.timeLabel.text = ""
            }
            
            let keyString = getStringFromTime(time: timeArray[indexPath.row], showAM_PM: false)
            if dataSource[keyString] != nil {
                let data = dataSource[keyString] as! [String: AnyObject]
                
                if (data["Type"] as! String == "Schedule") {//green
                    
                    cell?.detailView.backgroundColor = UIColor.init(red: 132/255, green: 204/255, blue: 105/255, alpha: 1.0)
                    cell?.blackOverlay.image = UIImage(named:"green")
                    cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
                    
                }else if (data["Type"] as! String == "Lesson") {//red
                    
                    if data["PaidStatus"] as! String == "Paid" {
                        //bang condition
                        if showAlertForStudioOrNotAlreadyBooked(keyString: keyString) || showAlertForPuttingOrNotAlreadyBooked(keyString: keyString){
                            cell?.detailView.backgroundColor =  UIColor.init(red: 247/255, green: 181/255, blue: 72/255, alpha: 1.0)
                            cell?.blackOverlay.image = UIImage(named:"studioNot")
                            cell?.headingLabel.textColor = UIColor.white
                            
                        }else{
                            //tortoise
                            if ((currentAcademyId == "1228") || (currentAcademyId == "1227")) && ( currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"){

                                if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == false{
                                cell!.otherStudioImgWidthCons.constant = 16
                                cell?.detailView.backgroundColor = UIColor.init(red: 247/255, green: 181/255, blue: 72/255, alpha: 1.0)//UIColor.orange
                                cell?.blackOverlay.image = UIImage(named:"studioNot")
                                cell?.headingLabel.textColor = UIColor.white

                                }else if let flag5 = data["IsPuttingLabAvailable"] as? Bool ,flag5 == false{
                                    cell!.otherStudioImgWidthCons.constant = 0
                                    cell?.detailView.backgroundColor = UIColor.init(red: 247/255, green: 181/255, blue: 72/255, alpha: 1.0)//UIColor.orange
                                    cell?.blackOverlay.image = UIImage(named:"studioNot")
                                    cell?.headingLabel.textColor = UIColor.white
                                    
                                }else{
                                    cell!.otherStudioImgWidthCons.constant = 0
                                    cell?.detailView.backgroundColor = UIColor.init(red: 195/255, green: 155/255, blue: 211/255, alpha: 1.0)
                                    cell?.blackOverlay.image = UIImage(named:"oneStudioBooked")
                                    cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
                                }
                                
                            }else{
                                
                            cell?.detailView.backgroundColor = UIColor.init(red: 0/255, green: 162/255, blue: 80/255, alpha: 1.0)
                            cell?.blackOverlay.image = UIImage(named:"green_dark")
                                cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
                                
                            }
                            
                        }
                        
                    }else{
                        //bang condition

                        if showAlertForStudioOrNotAlreadyBooked(keyString: keyString) || showAlertForPuttingOrNotAlreadyBooked(keyString: keyString) {
                            
                            cell?.detailView.backgroundColor = UIColor.init(red: 247/255, green: 181/255, blue: 72/255, alpha: 1.0)//UIColor.orange
                            cell?.blackOverlay.image = UIImage(named:"studioNot")
                            cell?.headingLabel.textColor = UIColor.white
                            
                        }else{
                            
                            //tortoise
                            if ((currentAcademyId == "1228") || (currentAcademyId == "1227")) && ( currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"){
                                
                                if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == false{
                                    cell!.otherStudioImgWidthCons.constant = 16
                                    cell?.detailView.backgroundColor = UIColor.init(red: 247/255, green: 181/255, blue: 72/255, alpha: 1.0)//UIColor.orange
                                    cell?.blackOverlay.image = UIImage(named:"studioNot")
                                    cell?.headingLabel.textColor = UIColor.white
                                    
                                } else if let flag5 = data["IsPuttingLabAvailable"] as? Bool ,flag5 == false{
                                    cell!.otherStudioImgWidthCons.constant = 0
                                    cell?.detailView.backgroundColor = UIColor.init(red: 247/255, green: 181/255, blue: 72/255, alpha: 1.0)//UIColor.orange
                                    cell?.blackOverlay.image = UIImage(named:"studioNot")
                                    cell?.headingLabel.textColor = UIColor.white
                                    
                                }else{
                                    cell!.otherStudioImgWidthCons.constant = 0
                                    cell?.detailView.backgroundColor = UIColor.init(red: 195/255, green: 155/255, blue: 211/255, alpha: 1.0)
                                    cell?.blackOverlay.image = UIImage(named:"oneStudioBooked")
                                    cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)                                }
                                
                            }else{
                                
                            cell?.detailView.backgroundColor = UIColor.init(red: 245/255, green: 126/255, blue: 127/255, alpha: 1.0)
                            cell?.blackOverlay.image = UIImage(named:"pink")
                                cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
                                
                            }
                            
                        }
                    }
                }
                else if (data["Type"] as! String == "Leave") {//chocolate
                    
                    cell?.detailView.backgroundColor = UIColor.init(red: 85/255, green: 57/255, blue: 71/255, alpha: 1.0)
                    cell?.headingLabel.textColor = UIColor.white
                    cell?.blackOverlay.image = UIImage(named:"dark")
                }
                else {//yellow
                    cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
                    cell?.detailView.backgroundColor = UIColor.init(red: 225/255, green: 231/255, blue: 91/255, alpha: 1.0) //UIColor.yellow
                    cell?.blackOverlay.image = UIImage(named:"yellow")
                }
                // Heading
                //           print(data["StartTime"] as! String)
                var timeAfter15 = getTimeFromString(timeString: data["StartTime"] as! String)
                timeAfter15 = timeAfter15.addingTimeInterval(15*60)
                let after15MinStr = getStringFromTime(time: timeAfter15, showAM_PM: false)
                
                if (data["StartTime"] as! String == keyString) {
                    cell?.blackOverlay.isHidden = false
                    cell?.headingLabel.text = timeString
                    cell?.headingLabel.font = UIFont.boldSystemFont(ofSize: 11.0)
                    self.fillingCell(data: data, cell: cell!)
                    
                }else if (after15MinStr == keyString) {
                    cell?.lesnTypImgwidCons.constant = 0
                    cell?.otherStudioImgWidthCons.constant = 0
                    cell?.blackOverlay.isHidden = true
                    cell?.headingLabel.text = (data["Name"] as! String == "") ? data["Notes"] as! String : data["Name"] as! String
                    
                    
                    if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == false{
                        if (data["Type"] as! String == "Schedule") {
                            
                        }else{
                            //already
                            if currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as? Int))"{
                                cell?.headingLabel.text = "Lesson time available but the studio is booked"
                            }
                        }
                    }else if let flag5 = data["IsPuttingLabAvailable"] as? Bool ,flag5 == false{
                        if (data["Type"] as! String == "Schedule") {
                            
                        }else{
                            //already
                            if currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"{
                                
                                cell?.headingLabel.text = "Lesson time available but putting lab is booked"
                            }
                            
                        }
                    }else {
                    
                    //bang
                        
                    if ((currentAcademyId == "1228") || (currentAcademyId == "1227")) && ( currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"){
                        
                        if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == true{
                            cell?.headingLabel.text = ""
                        }
                        }
                        
                    }
                    //till here

                    
                    cell?.headingLabel.font = UIFont.systemFont(ofSize: 9.0)
                    cell?.lesnTypImgwidCons.constant = 0
                    
                }
                else {
                    cell?.lesnTypImgwidCons.constant = 0
                    cell?.otherStudioImgWidthCons.constant = 0
                    
                    cell?.blackOverlay.isHidden = true
                    cell?.headingLabel.text = ""
                    cell?.headingLabel.font = UIFont.systemFont(ofSize: 11.0)
                    cell?.lesnTypImgwidCons.constant = 0
                    
                }
                // Seperator
                cell?.hideSeperators(true)
            }
            else {
                cell?.lesnTypImgwidCons.constant = 0
                cell?.otherStudioImgWidthCons.constant = 0

                cell?.detailView.backgroundColor = UIColor.white
                cell?.blackOverlay.isHidden = true
                cell?.headingLabel.text = ""
                cell?.hideSeperators(false)
                cell?.headingLabel.font = UIFont.systemFont(ofSize: 11.0)
                cell?.blackOverlay.image = UIImage(named:"")
                cell?.headingLabel.textColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
            }
            
            return cell!
        }
        
    }
    
    func fillingCell(data:[String:AnyObject],cell:ChartCellEMG)  {
        
        var imgName = ""
        if let flag = data["IsStudio"] as? Bool{
            if let flag2 = data["isPuttingLab"] as? Bool{
                if flag{
                    imgName = "dashboard"
                    cell.lesnTypImgwidCons.constant = 16
                    
                } else if flag2{
                    imgName = "putter1"
                    cell.lesnTypImgwidCons.constant = 16
                }
                else{
                    imgName = ""
                    cell.lesnTypImgwidCons.constant = 0
                }
                cell.lessonTypeImg.image = UIImage(named: imgName)

            }
        }
        
        if let flag3 = data["IsStudioAvailable"] as? Bool,flag3 == false{
            
            if (data["Type"] as! String == "Schedule") {
                cell.lesnTypImgwidCons.constant = 0
            }else{
                
                if currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"{
                    imgName = "studioNotAvailable"
                    cell.lessonTypeImg.image = UIImage(named: imgName)
                    cell.otherStudioImg.image = UIImage(named: imgName)
                }
            }
        }
        
        //        bang
        
        if let flag3 = data["isPuttingLab"] as? Bool,flag3 == true{

        if let flag3 = data["IsPuttingLabAvailable"] as? Bool,flag3 == false{
            
            if (data["Type"] as! String == "Schedule") {
                cell.lesnTypImgwidCons.constant = 0
            }else{
                
                if currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"{
                    
                    imgName = "putter1NotAvailable"
                    cell.lessonTypeImg.image = UIImage(named: imgName)

                }
            }
            }
            
        }
        
        //jugarh
        if cell.otherStudioImgWidthCons.constant == 16{
            imgName = "studioNotAvailable"
            cell.lessonTypeImg.image = UIImage(named: imgName)
            cell.otherStudioImg.image = UIImage(named: imgName)
        }
        

    }

    @objc func todayDateBtnAction(btn:UIButton){
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        selectedDate = date as NSDate
        self.dateLbl.text = gettingStringFromDate(date: selectedDate)
        getLesssonsAndEvents()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let keyString = getStringFromTime(time: timeArray[indexPath.row], showAM_PM: false)
        if dataSource[keyString] != nil {
            //            let data = dataSource[keyString] as! [String: AnyObject]
            
            let data = dataSource[keyString] as! [String: AnyObject]
            
            
            if (data["Type"] as! String == "Schedule") {
                self.navigateToPastController(keyString: keyString)
            }
            else if (data["Type"] as! String == "Lesson") {
//
//                if let flag = data["IsStudioAvailable"] as? Bool,flag == false{
                //bang
                if (showAlertForStudioOrNotAlreadyBooked(keyString: keyString) || showAlertForPuttingOrNotAlreadyBooked(keyString: keyString)) || currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"{
                    self.navigateToPastController(keyString: keyString)
                }else{
                    
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "AppointMentViewController") as! AppointMentViewController
                    controller.previousData = data
                    controller.isComeFromEvents = true
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }else if (data["Type"] as! String == "Class"){
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "AppointMentViewController") as! AppointMentViewController
                controller.previousData = data
                controller.isComeFromEvents = true
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else if (data["Type"] as! String == "Leave") {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddLeaveViewController") as! AddLeaveViewController
                controller.shouldEditLeave = true
                controller.leaveData = data
                controller.selectedDate = selectedDate
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else {
                self.navigateToPastController(keyString: keyString)
            }
            
            
        }else{
            self.navigateToPastController(keyString: keyString)
            
        }
    }
    
    
    func showAlertForStudioOrNotAlreadyBooked(keyString:String) ->Bool {
        
        if (currentAcademyId == "1228" || currentAcademyId == "1227" ) {
            return false
        }
        
        if let data = dataSource[keyString] as? [String: AnyObject] ,data != nil{
            
            if let studioAvailability = data["IsStudioAvailable"] as? Bool, studioAvailability == false {
                
                if currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as? Int))"{
                    return true
                }
            }else{
                return false
            }

        }else{
            
            return false
        }
        
        return false
    }
    
    //bang

    func showAlertForPuttingOrNotAlreadyBooked(keyString:String) ->Bool {
        
        
        if (currentAcademyId == "1228" || currentAcademyId == "1227" ) {
            return false
        }
        
        if let data = dataSource[keyString] as? [String: AnyObject] ,data != nil{
            
            if let studioAvailability = data["IsPuttingLabAvailable"] as? Bool, studioAvailability == false {
                
                if currentCoachIdCoachLogin != "\(String(describing: data["CoachID"] as! Int))"{
                    return true
                }
            }else{
                return false
            }
            
        }else{
            
            return false
        }
        
        
        return false
    }
    //till here
    
    func navigateToPastController(keyString:String)  {
        //bang condition
        if (showAlertForStudioOrNotAlreadyBooked(keyString: keyString) || showAlertForPuttingOrNotAlreadyBooked(keyString: keyString)) || ((currentAcademyId == "1228" || currentAcademyId == "1227" )) {
            let defaults = UserDefaults.standard
            defaults.set("NO", forKey: "isStudio")
            self.pushingToPastController(keyString: keyString)
            return
        }else{
            
        }
        
        if let id =   DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            //1119 for FDGS
            //uncomment replace 1119 for acadmy id

            if id == 1 {
                self.pushingToPastController(keyString: keyString)
                return
            }else{
                
            }
        }
         DispatchQueue.main.async { () -> Void in
        
        var alertMessage = ""
        if currentTarget == "Jay Kelly Golf" {
            alertMessage = "Please select whether you would like your lesson in the Coaching Studio.  Please note all lessons after day light have to be in the Studio?"
        }else{
            alertMessage = "Is this Studio Lesson?"
        }
        
        let alertController = UIAlertController(title: "STUDIO CONFIRMATION", message: alertMessage, preferredStyle: .alert)
        
        if let fromAcdemyOneToOne = DataManager.sharedInstance.currentAcademy()?["IsStudioEnable"] as? Bool{
            
            if fromAcdemyOneToOne{
                let defaults = UserDefaults.standard
                let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    
                    defaults.set("YES", forKey: "isStudio")
                    self.pushingToPastController(keyString: keyString)
                    
                }
                let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                    defaults.set("NO", forKey: "isStudio")
                    self.pushingToPastController(keyString: keyString)
                    
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                let defaults = UserDefaults.standard
                defaults.set("NO", forKey: "isStudio")
                self.pushingToPastController(keyString: keyString)
                
            }
            }
            
        }
    }

    func pushingToPastController(keyString:String)  {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PastViewController") as! PastViewController
        controller.fromEventController = true
        controller.selectedDate = selectedDate
        controller.selectedDateString = selectedDateString
        print(selectedDate)
        controller.selectedTime = keyString
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func rightMenuBtnAction(_ sender: UIBarButtonItem) {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
   
}

