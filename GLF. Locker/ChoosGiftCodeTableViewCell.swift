//
//  ChoosGiftCodeTableViewCell.swift
//  GLF. Locker
//
//  Created by Kashif Jawad on 26/10/2018.
//  Copyright © 2018 Nasir Mehmood. All rights reserved.
//

import UIKit

class ChoosGiftCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var coupnKeyLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var usedLbl: UILabel!
    @IBOutlet weak var remaingLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
