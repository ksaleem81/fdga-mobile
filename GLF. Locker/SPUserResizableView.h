//
//  SPUserResizableView.h
//  SPUserResizableView
//
//  Created by Stephen Poletto on 12/10/11.
//
//  SPUserResizableView is a user-resizable, user-repositionable
//  UIView subclass.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

typedef struct SPUserResizableViewAnchorPoint {
    CGFloat adjustsX;
    CGFloat adjustsY;
    CGFloat adjustsH;
    CGFloat adjustsW;
} SPUserResizableViewAnchorPoint;


typedef NS_ENUM(NSInteger, ShapeType)
{
    ShapeTypeNone=0,
    ShapeTypeLine,
    ShapeTypeSquare,
    ShapeTypeCircle,
    ShapeTypeImage
};


@protocol SPUserResizableViewDelegate;
@class SPGripViewBorderView;

@interface SPUserResizableView : UIView {
    SPGripViewBorderView *borderView;
    UIView __weak *contentView;
    CGPoint touchStart;
    CGFloat minWidth;
    CGFloat minHeight;
    
    // Used to determine which components of the bounds we'll be modifying, based upon where the user's touch started.
    SPUserResizableViewAnchorPoint anchorPoint;
    
    id <SPUserResizableViewDelegate> __weak delegate;
}

+ (SPUserResizableView*) getResizeableViewWithShapeType:(ShapeType)shapeType frame:(CGRect)frame;
+ (SPUserResizableView*) getResizeableViewWithShapeType:(ShapeType)shapeType;
+ (SPUserResizableView*) getResizeableViewWithImage:(UIImage*)image;


@property (nonatomic, weak) id <SPUserResizableViewDelegate> delegate;

// Will be retained as a subview.
@property (nonatomic, weak) UIView *contentView;

@property (nonatomic, strong) UIImage *imageToDraw;

// Default is 48.0 for each.
@property (nonatomic) CGFloat minWidth;
@property (nonatomic) CGFloat minHeight;

// Defaults to YES. Disables the user from dragging the view outside the parent view's bounds.
@property (nonatomic) BOOL preventsPositionOutsideSuperview;

- (void)hideEditingHandles;
- (void)showEditingHandles;
- (id)initWithFrame:(CGRect)frame shapeType:(ShapeType)shapeType;

- (CGFloat) getTotalRotatedAngle;
- (ShapeType) shapeType;
- (UIImage*) getImage;

@end

@protocol SPUserResizableViewDelegate <NSObject>

@optional

// Called when the resizable view receives touchesBegan: and activates the editing handles.
- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView;

// Called when the resizable view receives touchesEnded: or touchesCancelled:
- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView;

@end


