//
//  GalleryViewController.m
//  Orbis
//
//  Created by Nasir Mehmood on 8/25/15.
//
//

#import "GalleryViewController.h"
#import "GalleryCollectionViewCell.h"
#import "OrbisMediaGallery.h"
#import "OrbisMediaGallery.h"
#import "GlobalClass.h"
#import "GLF__Locker-Swift.h"
//#import "Jay_Kelly_Golf-swift.h"

#import <SideMenu/SideMenu-Swift.h>
@interface GalleryViewController ()
{
    NSMutableArray *videoGalleryItems;
    NSMutableArray *photoGalleryItems;
    NSIndexPath *selectedIndexPath;
}

@end

@implementation GalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.galleryView registerNib:[UINib nibWithNibName:@"GalleryCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"GalleryCollectionViewCell"];
//    [self.galleryView registerClass:[GalleryCollectionViewCell class] forCellWithReuseIdentifier:@"GalleryCollectionViewCell"];
    
    [self loadData];
    
    UIBarButtonItem * rightDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"lines_3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:NO];
    
    UIBarButtonItem * leftDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arr_left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    leftDrawerButton.imageInsets=UIEdgeInsetsMake(2, 0, 0, 0);
    
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:NO];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:147.0/255.0 green:149.0/255.0 blue:151.0/255.0 alpha:1.0]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    self.navigationItem.title=@"App Gallery";
    
//    NSLog(@"playback rate: %f", [[UIScreen mainScreen] nativeBounds].size.height);

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    self.navigationItem.title=@"";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadData
{
    if(self.galleryType==GalleryTypePhoto)
    {
        NSArray *photos=[[OrbisMediaGallery sharedGallery] getAllPhotoDictionaries];
        [self setPhotoGalleryItems:photos];
    }
    else if(self.galleryType==GalleryTypeVideo)
    {
        NSArray *videos=[[OrbisMediaGallery sharedGallery] getAllVideoDictionaries];
        [self setVideoGalleryItems:videos];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) setVideoGalleryItems:(NSArray*)videoItems
{
    videoGalleryItems=[NSMutableArray arrayWithArray:videoItems];
}

- (void) setPhotoGalleryItems:(NSArray*)photoItems
{
    photoGalleryItems=[NSMutableArray arrayWithArray:photoItems];
}

- (void) showVideoOptions
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Analyse", @"Trim", @"Save to Camera Roll", @"Delete", nil];
    [actionSheet showInView:self.view];
}

- (void) showPhotoOptions
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"View", @"Save to Camera Roll", @"Delete", nil];
    [actionSheet showInView:self.view];
}

- (void)viewImage:(NSDictionary *)info {
    
    ImagePreview *imgPreview = [ImagePreview loadFromXib];
    imgPreview.mainImageView.image = [UIImage imageWithContentsOfFile:info[@"photoPath"]];
    [imgPreview addInViewWithParentView:self.view];
    
}



#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(videoGalleryItems)
    {
        return videoGalleryItems.count;
    }
    else if(photoGalleryItems)
    {
        return photoGalleryItems.count;
    }
    
    return 0;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseableIdentifier=@"GalleryCollectionViewCell";
    GalleryCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:reuseableIdentifier forIndexPath:indexPath];
    if(cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:reuseableIdentifier owner:self options:nil] firstObject];
    }
    
    NSString *thumbnailPath=nil;
    NSString *durationText=@"00:00";
    BOOL isVideo=NO;
    if(videoGalleryItems)
    {
        isVideo=YES;
        NSDictionary *videoInfo=[videoGalleryItems objectAtIndex:indexPath.row];
        thumbnailPath=[videoInfo objectForKey:@"videoThumbnailPath"];
        
        durationText=[GlobalClass getTimeStringFromSeconds:[[videoInfo objectForKey:@"duration"] intValue]];
    }
    else if(photoGalleryItems)
    {
        NSDictionary *imageInfo=[photoGalleryItems objectAtIndex:indexPath.row];
        thumbnailPath = imageInfo[@"photoPath"];
    }
    else
    {
        return nil;
    }
    
    cell.movieInfoView.hidden=!isVideo;
    cell.movieDurationLabel.text=durationText;
    cell.imageView.image=[UIImage imageWithContentsOfFile:thumbnailPath];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndexPath=indexPath;
    if(videoGalleryItems)
    {
        NSDictionary *videoInfo=[videoGalleryItems objectAtIndex:indexPath.row];
        if(self.isFromLessonVideosPage)
        {
            [self backButtonClicked:nil];
            // CHM Jawad
//            AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//            NSString *jsonString=[NSString stringWithFormat:@"lessonVideosPage.videoSelectedFromOrbisGallery(%@)", [AppDelegate getJSONDictionaryWithDictionary:videoInfo]];
//            
//            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//            [mvc.commandDelegate evalJs:jsonString];
            if (_delegate && [_delegate respondsToSelector:@selector(galleryViewControllerDidSelectedVideo:)]) {
                [_delegate galleryViewControllerDidSelectedVideo:videoInfo];
            }
        }
        else if(self.isFromSelectMediaPage)
        {
            [self backButtonClicked:nil];
            // CHM Jawad
//            AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//            NSString *jsonString=[NSString stringWithFormat:@"selectMediaPage.uploadMediaWithInfo(%@)", [AppDelegate getJSONDictionaryWithDictionary:videoInfo]];
//            
//            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//            [mvc.commandDelegate evalJs:jsonString];
            if (_delegate && [_delegate respondsToSelector:@selector(galleryViewControllerDidSelectedVideo:)]) {
                [_delegate galleryViewControllerDidSelectedVideo:videoInfo];
            }
         }
        else
        {
            [self showVideoOptions];
        }
    }
    else if(photoGalleryItems)
    {
        if (_isFromLessonPhotosPage || -_isFromSelectMediaPage)
        {
            NSDictionary *imageInfo = [photoGalleryItems objectAtIndex:indexPath.row];
            
            [self backButtonClicked:nil];
            if (_delegate && [_delegate respondsToSelector:@selector(galleryViewControllerDidSelectedImage:)]) {
                [_delegate galleryViewControllerDidSelectedImage:imageInfo];
            }
        }
        else {
            [self showPhotoOptions];
        }
    }
}

#pragma mark - UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex  // after animation
{
    
    if (_galleryType == GalleryTypeVideo) {
        NSDictionary *videoInfo=[videoGalleryItems objectAtIndex:selectedIndexPath.row];
        switch (buttonIndex) {

            case 0: //Analyse
            {
                // CHM Jawad
                [self backButtonClicked:nil];
    //            AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    //            NSString *jsonString=[NSString stringWithFormat:@"window.orbisVideoGalleryPage.editVideoWithInfo(%@)", [AppDelegate getJSONDictionaryWithDictionary:videoInfo]];
    //            
    //            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
    //            [mvc.commandDelegate evalJs:jsonString];
                if (self.delegate && [self.delegate respondsToSelector:@selector(galleryViewControllerDidSelectedAnalyse:)]) {
                    [self.delegate galleryViewControllerDidSelectedAnalyse:videoInfo];
                }
                
            }
            break;
            case 1: //Trim
            {
                //CHM Jawad
                [self backButtonClicked:nil];
    //            AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    //            NSString *jsonString=[NSString stringWithFormat:@"window.orbisVideoGalleryPage.trimVideoWithInfo(%@)", [AppDelegate getJSONDictionaryWithDictionary:videoInfo]];
    //            
    //            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
    //            [mvc.commandDelegate evalJs:jsonString];
                if (self.delegate && [self.delegate respondsToSelector:@selector(galleryViewControllerDidSelectedTrim:)]) {
                    [self.delegate galleryViewControllerDidSelectedTrim:videoInfo];
                }
            }
            break;
            case 2: //Save To Camera Roll
            {
                [self saveVideoToLibrary:videoInfo];
            }
            break;
            case 3: //Delete
            {
                NSString *videoPath=[videoInfo objectForKey:@"videoPath"];
                BOOL isDeleted=[[OrbisMediaGallery sharedGallery] deleteVideoFromGalleryAtPath:videoPath];
                if(isDeleted)
                {
                    [self loadData];
                    [[self galleryView] reloadData];
                }
                else
                {
                    
                    [[[UIAlertView alloc] initWithTitle:GlobalClass.sharedInstance.currentTargetC message:@"Error occurred while deleting video" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil] show];
                }

            }
            break;
            
            default:
            break;
        }
    }
    else {
        NSDictionary *imageInfo=[photoGalleryItems objectAtIndex:selectedIndexPath.row];
        switch (buttonIndex) {
            case 0: // View
                [self viewImage:imageInfo];
                break;
            
            case 1: // Save to Camera Roll
                [self savePhotoToLibrary:imageInfo];
                break;
                
            case 2: // Delete
            {
                BOOL isDeleted = [[OrbisMediaGallery sharedGallery] deletePhotoFromGalleryAtPath:imageInfo[@"photoPath"]];
                if (isDeleted) {
                    [self loadData];
                    [[self galleryView] reloadData];
                }
                else {
                    [Utility showAlertViewWithTitle:GlobalClass.sharedInstance.currentTargetC message:@"Error occurred while deleting photo"];
                }
            }
                break;
                
            default:
                break;
        }
    }
}

//#pragma mark - IBAction Methods

- (void) toggleSlideMenu:(id)sender {
    //[_mmDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
//       present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)}
    //cheema
   [self presentViewController:SideMenuManager.menuRightNavigationController animated:true completion:nil];
    
    
    
    
}

- (void) backButtonClicked:(id)sender
{
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 

- (void)saveVideoToLibrary:(NSDictionary*)videoInfo
{
    NSString *videoPath=[videoInfo objectForKey:@"videoPath"];
    if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoPath))
    {
        UISaveVideoAtPathToSavedPhotosAlbum(videoPath, self, @selector(video:didFinishSavingWithError:contextInfo:),nil);
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:GlobalClass.sharedInstance.currentTargetC message:@"Error occurred while saving video to Camera Roll" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil] show];
    }
}


- (void) savePhotoToLibrary:(NSDictionary *)imageInfo
{
    NSString *photoPath=[imageInfo objectForKey:@"photoPath"];
    UIImage *imageToSave=[UIImage imageWithContentsOfFile:photoPath];
    UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil);
}


-(void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if(error)
    {
        [[[UIAlertView alloc] initWithTitle:@"GLF. Locker" message:@"Error occurred while saving video to Camera Roll" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil] show];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:GlobalClass.sharedInstance.currentTargetC message:@"video saved to Camera Roll" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
}


@end
