//
//  GoToClassTableViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/31/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import JYRadarChart
import SideMenu
//import Charts
class GoToClassTableViewController: UITableViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var graphLbl: UILabel!
    @IBOutlet weak var graphImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var phoneViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var academyLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var parentView: UIView!
    var previousData  = [String:AnyObject]()
     var forPlayerTypeIdData  = [String:AnyObject]()
    var individualInfo  = [String:AnyObject]()
    var imageData = UIImage()
    var isComeFromEvents = false
    var graphInfo  = [[String:AnyObject]]()
    var isForClass = false
    @IBOutlet weak var graphView: UIView!//PieChartView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: UIButton!
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "LESSON"
        headerViewHeightConstraint.constant = self.view.frame.size.width / 2
        if currentUserLogin == 4 {
            phoneViewHeightConstraint.constant = 0
        }
        print(previousData)
       settingRighMenuBtn()
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(GoToClassTableViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if currentUserLogin == 4{
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

        
        var heightOfView : CGFloat = 0
        if isForClass {
            self.sendBtn.isHidden = true
            phoneViewHeightConstraint.constant = 0
            self.graphView.isHidden = true
            self.graphView.superview?.backgroundColor = UIColor.init(red: 204/255, green: 213/255, blue: 35/255, alpha: 1.0)
             heightOfView = 90
        }else{
          
            gettingServerData()
            if currentUserLogin == 4 {
                heightOfView = 110
                self.sendBtn.isHidden = true
            }else{
            heightOfView = 130
            self.sendBtn.isHidden = false
            }
        }
        
        parentView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:self.view.frame.size.width / 2 + heightOfView )
        
         gettingIndividualLessonDetail()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gettingIndividualLessonDetail()  {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
                    lessonID = "\(id)"
        }
        
        //localechanges
        var parameters = ""
        parameters = "\(lessonID)/\(is12Houre)"
        NetworkManager.performRequest(type:.get, method: "Academy/GetIndividualLessonDetails/\(parameters)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
            }
            
            let individualInfoObjec = object as? [[String:AnyObject]]
            if (individualInfoObjec?.count)! > 0{
                self.individualInfo = (individualInfoObjec?[0])!
                self.fillingCurrentControllerInfo()
            }
           
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func gettingServerData()  {
        
        var studentId = ""
        if let id = previousData["StudentID"] as? Int{
            studentId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetStudentSkillGraph/\(studentId)/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
                // should never happen!
            }
            
           // self.graphInfo
            let data = (object as? [[String:AnyObject]])!
//              self.graphView =  DataManager.sharedInstance.countingCounterForSocialMedias(view: self.graphView, graphData: data)
            self.graphView.isHidden = true
            let chart = JYRadarChart.init(frame: self.graphView.frame)
            self.graphView.superview?.addSubview(DataManager.sharedInstance.gameScoreGraph(chart: chart, data:data,controller: self))
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func fillingCurrentControllerInfo()  {
        
        if let phon = individualInfo["PlayerPhoneNo"] as? String {
            phoneLbl.text = phon
        }
        var startTime = ""
        if let phon = individualInfo["StartTime"] as? String {
            startTime = phon
        }
        var endTime = ""
        if let phon = individualInfo["EndTime"] as? String {
            endTime = phon
        }
        
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
                graphLbl.isHidden = false
                
            }else{
                graphLbl.isHidden = true
            }
        }

        if isComeFromEvents{
            if let name = previousData["Name"] as? String {
                titleLbl.text = name
                graphLbl.text = name
            }
        }else{
        if let name = previousData["LessonName"] as? String {
            titleLbl.text = name
            graphLbl.text = name

            }
        }
        
      
        if let name = DataManager.sharedInstance.currentAcademy()!["AcademyName"] as? String{
            academyLbl.text = name
        }
        
        userImage.image = imageData
        
        if isForClass {
            if let name = previousData["StartTime"] as? String {
                
                //            localechanges
                if is12Houre == "true"{
                    startTime = name
                    
                }else{
                    if (name.count) > 5 {
                        
                        startTime = String(name.dropLast(3))
                    }else{
                        startTime = name
                    }
                }
            }
            if let name = previousData["EndTime"] as? String {
                //            localechanges
                if is12Houre == "true"{
                    endTime = name
                    
                }else{
                    if (name.count) > 5 {
                        endTime = String(name.dropLast(3))
                    }else{
                        endTime = name
                    }
                    
                }
                
            }

        }
        
     
        if let date = previousData["LessonDate"] as? String {
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                dateLbl.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd") + " From " + startTime + " to " + endTime
            }
        }
        
        
    }
    
    
    func showSelectVideosViewContoller(at index: Int) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectVideosViewController") as! SelectVideosViewController
        controller.previousData = previousData

        controller.mediaType = OrbisMediaType.init(rawValue: UInt(index))
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isForClass {
            if  currentUserLogin == 4{
            return 5
            }else{
                return 9
            }
        }else if  currentUserLogin == 4{
            return 5
        }else{
            return 8
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        if indexPath.row == 0 {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "NoteViewController") as! NoteViewController
            if currentUserLogin == 4 {
                if isForClass{
                    if let id = forPlayerTypeIdData["StudentID"] as? Int{
                        previousData["StudentID"] = id as AnyObject?
                    }
                }
                    controller.previousData = previousData

            
            }else{
                controller.previousData =  previousData

            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if indexPath.row == 1 {
            showSelectVideosViewContoller(at: indexPath.row)
        }
        else if indexPath.row == 2 {

            showSelectVideosViewContoller(at: indexPath.row)
        }
        else if indexPath.row == 3 {

            showSelectVideosViewContoller(at: indexPath.row)
        }
        else if indexPath.row == 4 {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LessonTagsViewController") as! LessonTagsViewController
            controller.previousData =  previousData
            controller.forPlayerTypeIdData = forPlayerTypeIdData
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 6{
       
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "UpdatePlayerLevelViewController") as! UpdatePlayerLevelViewController
            controller.previousData =  previousData
            controller.forPlayerTypeIdData = forPlayerTypeIdData
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 5{
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "GameScoreViewController") as! GameScoreViewController
            controller.previousData =  previousData
            controller.forPlayerTypeIdData = forPlayerTypeIdData
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 7{
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentStatusViewController") as! PaymentStatusViewController
            controller.previousData =  previousData
            controller.individualInfo = individualInfo
            controller.forPlayerTypeIdData = forPlayerTypeIdData
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 8{
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "GroupViewController") as! GroupViewController
            controller.fromClassAttendess =  true
            controller.previousData = previousData
            self.navigationController?.pushViewController(controller, animated: true)
        }


    }
 
    // MARK: - buttins actions

    @IBAction func messageSentBtnAction(_ sender: UIButton) {
        blurView.frame = self.view.bounds
        blurView.tag = 1010
        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.curveEaseIn,
                                  animations: { self.view.addSubview(self.blurView)
        }, completion: nil)
       
        presentTableViewAsPopOver(sender: sender)
    }
   
    @IBAction func callBtnAction(_ sender: UIButton) {
//        guard let number = URL(string: "telprompt://" + phoneLbl.text!) else { return }
//        UIApplication.shared.open(number, options: [:], completionHandler: nil)
        
        if let url = URL(string: "telprompt://\(phoneLbl.text!)") {
            UIApplication.shared.openURL(url)
        }
    }
    
    func presentTableViewAsPopOver(sender:UIButton) {
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "ComposeMessageViewController") as! ComposeMessageViewController
        if let id = forPlayerTypeIdData["UserID"] as? Int{
            menuViewController.idToSend = "\(id)"
        }
        menuViewController.blureView = blurView
        if let id = forPlayerTypeIdData["StudentName"] as? String{
            menuViewController.NameToSend = "\(id)"
        }
        
        menuViewController.isForLessonOrClass = true
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: self.view.frame.size.width-20, height: 440)//CGSize(width: 300, height: 440)//CGSize(width: view.frame.size.width-10, height: self.view.frame.size.height-20)
        
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .init(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.view as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: sender.bounds.size.width/2,
                y: sender.bounds.origin.y+sender.bounds.size.height,
                width: 1,
                height: 1)
        present(
            menuViewController,
            animated: true,
            completion: nil)
    }


}

extension GoToClassTableViewController:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return 	UIModalPresentationStyle.none
    }
    internal func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
        self.blurView.removeFromSuperview()
    }
}

