//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
import ActionSheetPicker_3_0

class CoachScheduleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var dataArray = [[String:AnyObject]]()
    var selectedDataArray = [[String:AnyObject]]()
    var editBtnActive = false
    @IBOutlet weak var tableView: UITableView!
    var adhocScheduleOn = false
    @IBOutlet weak var dayOrDateLbl: UILabel!
    var academyScheduleData = [String : AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
     tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
     self.tableView.tableFooterView = UIView(frame: CGRect.zero)
     settingRighMenuBtn()
        if adhocScheduleOn {
            self.title = "Coach Adhoc Schedule"
            dayOrDateLbl.text = "Date"
        }
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(CoachScheduleViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
 
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            getCoachSchedule()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count //+ 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
//        if indexPath.row == self.dataArray.count {
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleFooterTableViewCell", for: indexPath) as! ScheduleFooterTableViewCell
//            cell.addBtn.addTarget(self, action:#selector(addBtnAction(sender:)) , for:.touchUpInside)
//
//            cell.editBtn.addTarget(self, action:#selector(editBtnAction(sender:)) , for:.touchUpInside)
//
//            cell.doneBtn.addTarget(self, action:#selector(doneBtnAction(sender:)) , for:.touchUpInside)
//            return cell
//        }else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoachScheduleTableViewCell", for: indexPath) as! CoachScheduleTableViewCell
            
            if adhocScheduleOn{

               if let title = dataArray[indexPath.row]["ScheduleDate"] as? String{
                   var dateOrignal = title.components(separatedBy: "T")
                   if dateOrignal.count > 0 {
////                        if (dateOrignal[1].count) > 5 {
////                            dateOrignal[1] = String(dateOrignal[1].dropLast(3))
////                        }
                   }
                cell.dayLbl.text = DataManager.sharedInstance.getFormatedDateGivenPAttern(date: dateOrignal[0], formate: "yyyy-MM-dd", formateReturn: "EEE dd MMM, yyyy")
 //"\( dateOrignal[0])"
                }

            }else{
                if let title = dataArray[indexPath.row]["dayOfWeekName"] as? String{
                    cell.dayLbl.text = title
                }
           }
        if let title = dataArray[indexPath.row]["startTime"] as? String{
            var dateOrignal = title.components(separatedBy: "T")
            if dateOrignal.count > 0 {
              
                
                //            localechanges
                if is12Houre == "true"{
                    cell.startTimeLbl.text =  "\( title)"
                    
                }else{
                    if (dateOrignal[1].count) > 5 {
                        dateOrignal[1] = String(dateOrignal[1].dropLast(3))
                    }
                    cell.startTimeLbl.text =  "\( dateOrignal[1])"
                }
                
            }
        }
            
        if let title = dataArray[indexPath.row]["EndTime"] as? String{
            var dateOrignal = title.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                //            localechanges
                if is12Houre == "true"{
                    cell.endTimeLbl.text =  "\(title)"
                    
                }else{
                    if (dateOrignal[1].count) > 5 {
                        dateOrignal[1] = String(dateOrignal[1].dropLast(3))
                    }
                    cell.endTimeLbl.text =  "\( dateOrignal[1])"
                }

            }

        }
            
            if editBtnActive{
                cell.deleteBtn.isHidden = false
                cell.startTimeBtn.isHidden = false
                cell.endTimeBtn.isHidden = false
            }else{
                cell.deleteBtn.isHidden = true
                cell.startTimeBtn.isHidden = true
                cell.endTimeBtn.isHidden = true
            }
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action:#selector(deleteBtnAction(sender:)) , for:.touchUpInside)
            cell.startTimeBtn.tag = indexPath.row
            cell.startTimeBtn.addTarget(self, action:#selector(startTimeBtnAction(sender:)) , for:.touchUpInside)
        cell.endTimeBtn.tag = indexPath.row
        cell.endTimeBtn.addTarget(self, action:#selector(endTimeBtnAction(sender:)) , for:.touchUpInside)

                return cell
//        }
//        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == dataArray.count {
            return 180.0
        }else{
            return 60.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
    @objc func deleteBtnAction(sender:UIButton)  {
        
     
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
              self.deleteCoachSchedule(tag: sender.tag)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func startTimeBtnAction(sender:UIButton)  {
        
        if isNewUrl{
    
            //dynamicChange
        var dayNumber = ""
        if adhocScheduleOn{
            
                    if let title = dataArray[sender.tag]["ScheduleDate"] as? String{
                        var dateOrignal = title.components(separatedBy: "T")
                        if dateOrignal.count > 0 {
            
                  dayNumber =
                            "\(String(describing: (DataManager.sharedInstance.getDayOfWeek(dateOrignal[0], dateFormater: "yyyy-MM-dd"))))"
            
                        }
            
                        }
            
        }else{

        if let day = dataArray[sender.tag]["dayOfWeek"] as? Int{
            
            dayNumber = "\(day)"

        }
        }
        
        Utility.getScheduleTimeOfAcademy(selectedDay: "\(dayNumber)", controller: self, Success: { data  in
            self.academyScheduleData = data as! [String : AnyObject]
            self.startTimeBtnEndHandling(sender: sender)
        }, onFailure: {(error) in
            
        })

        }else{
            self.startTimeBtnEndHandling(sender: sender)

        }
   
    }
    
    
    @objc func endTimeBtnAction(sender:UIButton)  {
        
        
        if isNewUrl{
            
            //dynamicChange
            var dayNumber = ""
            if adhocScheduleOn{
                
                if let title = dataArray[sender.tag]["ScheduleDate"] as? String{
                    var dateOrignal = title.components(separatedBy: "T")
                    if dateOrignal.count > 0 {
                        
                        //dynamicChange
                        dayNumber =
                        "\(String(describing: (DataManager.sharedInstance.getDayOfWeek(dateOrignal[0], dateFormater: "yyyy-MM-dd"))))"

                    }
                    
                }
                
            }else{
                
                if let day = dataArray[sender.tag]["dayOfWeek"] as? Int{
                    
                    dayNumber = "\(day)"
                    
                }
            }
            
            Utility.getScheduleTimeOfAcademy(selectedDay: "\(dayNumber)", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
                self.endTimeBtnHandling(sender: sender)
            }, onFailure: {(error) in
                
            })
            
            
        }else{
            self.endTimeBtnHandling(sender: sender)
            
        }
    }
    
    func startTimeBtnEndHandling(sender:UIButton)  {
        
        
        
        let formator = DateFormatter()
        var picker =  ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in
            
            if is12Houre == "true"{
                formator.dateFormat = "hh:mm a"
                
            }else{
                
                formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let formatter2 = DateFormatter()
                formatter2.timeStyle = .medium
                print(formatter2.string(from: selectedTime as! Date))
                
                
            }
            let selectedTimeCurrent = selectedTime as! NSDate
            let date = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            
         print(date)
            
            self.saveForm(selectedDate: "\(date)", tag: sender.tag,forStart: true)
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
            
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
            
        }
        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
        picker?.show()
    }
    
    func endTimeBtnHandling(sender:UIButton)  {
        
        let formator = DateFormatter()
        var picker =  ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in
            //localechanges
            if is12Houre == "true"{
                formator.dateFormat = "hh:mm a"
            }else{
                formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
                

            }
            
            let selectedTimeCurrent = selectedTime as! NSDate
            let date = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            self.saveForm(selectedDate: "\(date)", tag: sender.tag,forStart: false)
            
        }, cancel: { (picker) -> Void in
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
        }
        if #available(iOS 13.4, *) {
            picker?.datePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        }

        
        picker?.show()
    }
    
    
    func convertStrinToDate(time:String)->Date{
        //crashiphon6SPlus

        print(time)
        let dateFormatter = DateFormatter()
        
        //localechanges
        if is12Houre == "true"{
            
            
            dateFormatter.dateFormat = "hh:mm a"
            
        }else{
            
            if time.count < 17{
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            }else{
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            }
        }

        
        let result = time.replacingOccurrences(of: "T", with: " ",
                                              options: NSString.CompareOptions.literal, range:nil)
        
        let item =   result
        let date = dateFormatter.date(from: item)
        if date == nil{
            
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM-dd HH:mm"

            print(formatter2.date(from: item))
            return formatter2.date(from: item)!
        }
        
        return date!
        
    }

    
    func saveForm(selectedDate:String,tag:Int,forStart:Bool)  {
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }
       
        var coachScheduleId = "0"
        if let id = dataArray[tag]["CoachScheduleId"] as? Int {
            coachScheduleId = "\(id)"
        }
        
        var dayOfWeek = "0"
        if let id = dataArray[tag]["dayOfWeek"] as? Int {
            dayOfWeek = "\(id)"
        }
        var startTime = ""
        var endTime = ""
        if forStart {
            startTime = selectedDate
            if let id = dataArray[tag]["EndTime"] as? String {
                endTime="\(id)"
            }
            
        }else{
            endTime = selectedDate
            if let id = dataArray[tag]["startTime"] as? String {
                startTime="\(id)"
            }
        }
        
        

        if Utility.isDevice12Hour(){
            
        }else{
        
        //crashiphon6SPlus
        let endTimeToCheck = convertStrinToDate(time: endTime)

        let startTimeToCheck  = convertStrinToDate(time: startTime)

        if startTimeToCheck > endTimeToCheck || startTimeToCheck == endTimeToCheck{

            DataManager.sharedInstance.printAlertMessage(message: "Select valid time", view: self)
            return
        }
            
        }
        
        var parameters  = [String:AnyObject]()
        var urlStr = ""
        if adhocScheduleOn {
            if let title = dataArray[tag]["ScheduleDate"] as? String {
                var dateOrignal = title.components(separatedBy: "T")
                dayOfWeek =  "\( dateOrignal[0])"
            }
            
      parameters = ["ScheduleDate":"\(dayOfWeek)" as AnyObject,"CoachID":userId as AnyObject!,"CoachScheduleId":coachScheduleId as AnyObject!,"startTime":startTime as AnyObject!,"endTime":endTime as AnyObject!,"Is12HoursTimeFormat" : "\(is12Houre)" as AnyObject]
            urlStr = "SaveCoachAdhcoSchedule"
            
        }else{
            
      parameters  = ["dayOfWeek":"\(dayOfWeek)" as AnyObject,"CoachID":userId as AnyObject!,"CoachScheduleId":coachScheduleId as AnyObject!,"startTime":startTime as AnyObject!,"endTime":endTime as AnyObject!,"Is12HoursTimeFormat" : "\(is12Houre)" as AnyObject]
            urlStr = "SaveCoachSchedual"
        }
        print(parameters)
        
        NetworkManager.performRequest(type:.post, method: "Academy/\(urlStr)", parameter:parameters, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            if let responce = object as? Int{
             
                _ = self.navigationController?.popViewController(animated: true)
                if responce == 8 {
                    DataManager.sharedInstance.printAlertMessage(message:"Error\nStart time is greater than end time", view:UIApplication.getTopestViewController()!)
                }else if responce == 0 {
                       DataManager.sharedInstance.printAlertMessage(message:"Successfully updated", view:UIApplication.getTopestViewController()!)
                }else{
                    DataManager.sharedInstance.printAlertMessage(message:"Unavailable slot", view:UIApplication.getTopestViewController()!)

                    
                }
          
            }
           
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    @IBAction func addScheBtnAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddCoachScheduleViewController") as! AddCoachScheduleViewController
        
        if adhocScheduleOn {
            controller.adhocScheduleOn = true
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func editScheBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
                   sender.isSelected = false
                   editBtnActive = false
               }else{
                   editBtnActive = true
                   sender.isSelected = true
               }
        
               self.tableView.reloadData()
        
    }
    
    @IBAction func doneScheBtnAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    @objc func addBtnAction(sender:UIButton)  {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddCoachScheduleViewController") as! AddCoachScheduleViewController
        
        if adhocScheduleOn {
            controller.adhocScheduleOn = true
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func editBtnAction(sender:UIButton)  {
        
        if sender.isSelected {
            sender.isSelected = false
            editBtnActive = false
        }else{
            editBtnActive = true
            sender.isSelected = true
        }
        self.tableView.reloadData()
    }
    
    @objc func doneBtnAction(sender:UIButton)  {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func getCoachSchedule()  {
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }

        var urlStr = ""
        if adhocScheduleOn{
          urlStr = "GetCoachAdhocSchedules"
        }else{
            urlStr = "GetCoachSchedules"
        }
        
        urlStr = "\(urlStr)/\(coachId)/\(acadmyId)/\(is12Houre)"
        //localechanges
        NetworkManager.performRequest(type:.get, method: "academy/\(urlStr)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
            self.dataArray =  object as! [[String : AnyObject]]
            self.tableView.reloadData()
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    //http://app.glfbeta.com/OrbisWebApi/api/
    func deleteCoachSchedule(tag:Int)  {
        
        ///2409
        var schduleId = ""
        if let id = dataArray[tag]["CoachScheduleId"] as? Int{
            schduleId = "\(id)"
        }
        
        var urlStr = ""
        if adhocScheduleOn{
            urlStr = "DeleteAdhocSchedule"
        }else{
            urlStr = "DeleteCoachSchedule"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/\(urlStr)/\(schduleId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
                
            }
        self.getCoachSchedule()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    func getDayOfWeekString(today:String)->String? {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            switch weekDay {
            case 1:
                return "Sunday"
            case 2:
                return "Monday"
            case 3:
                return "Tuesday"
            case 4:
                return "Wednesday"
            case 5:
                return "Thursday"
            case 6:
                return "Friday"
            case 7:
                return "Saturday"
            default:
                print("Error fetching days")
                return "Day"
            }
        } else {
            return nil
        }
    }
    
}


