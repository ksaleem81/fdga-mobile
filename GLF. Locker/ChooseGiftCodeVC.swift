//
//  ChooseGiftCodeVC.swift
//  GLF. Locker
//
//  Created by Kashif Jawad on 26/10/2018.
//  Copyright © 2018 Nasir Mehmood. All rights reserved.
//

import UIKit

protocol selectGifTCards{
    
    func selectedGiftCards(selectedArray:[[String:AnyObject]])
}

class ChooseGiftCodeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var selectedDataArray = [[String:AnyObject]]()
    var dataArray = [[String:AnyObject]]()
    var delegate : selectGifTCards?
    var userData  = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Gift Details"
        
        fetchGiftCards()

    }
    
    func fetchGiftCards()  {
        
        var userId = ""
        if let id = userData["Userid"] as? Int{
            userId = "\(id)"
        }
        
        if userId == ""{
        if let id = userData["UserId"] as? Int{
            userId = "\(id)"
        }
        }
        
        NetworkManager.performRequest(type:.get, method: "booking/GetPlayerAvailabeGiftsForDialogue?UserId=\(userId)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
            DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
            DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
            self.dataArray = object as! [[String:AnyObject]]
            self.tableView.reloadData()
        }) { (error) in
            self.showInternetError(error: error!)
        }
        
    }
    
    // MARK: - TableView delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChoosGiftCodeTableViewCell", for: indexPath) as! ChoosGiftCodeTableViewCell
        cell.checkBtn.tag = indexPath.row
        
        cell.checkBtn.addTarget(self, action:#selector(checkBoxAction(sender:)) , for:.touchUpInside)
        
        if let coupen = dataArray[indexPath.row]["GiftCardCouponCode"] as? String{
            cell.coupnKeyLbl.text = coupen
        }
        
        if let coupen = dataArray[indexPath.row]["TotalAmount"] as? Double{
            cell.totalLbl.text = "\(coupen)"
        }
        
        if let coupen = dataArray[indexPath.row]["UsedAmount"] as? Double{
            cell.usedLbl.text = "\( coupen)"
        }
        
        if let coupen = dataArray[indexPath.row]["RemainingAmount"] as? Double{
            cell.remaingLbl.text = "\(coupen)"
        }
        
        let k = self.selectedDataArray
        
        let index = k.index {
            if let dic = $0 as? Dictionary<String,AnyObject> {
                if let value = dic["GiftCardCouponCode"]  as? String, value == cell.coupnKeyLbl.text!{
                    cell.checkBtn.isSelected = true
                    return true
                }
            }
            cell.checkBtn.isSelected = false
            return false
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
   @objc func checkBoxAction(sender:UIButton)  {
    
    let indexPath =  IndexPath(row: sender.tag, section: 0)
    let cell = tableView.cellForRow(at: indexPath) as! ChoosGiftCodeTableViewCell
    
    if cell.checkBtn.isSelected {
        cell.checkBtn.isSelected = false
        //  self.selectedDataArray.
        let k = self.selectedDataArray
        let index = k.index {
            if let dic = $0 as? Dictionary<String,AnyObject> {
                if let value = dic["GiftCardCouponCode"]  as? String, value == cell.coupnKeyLbl.text!{
                    return true
                }
            }
            return false
        }
        self.selectedDataArray.remove(at: index!)
    }else{
        cell.checkBtn.isSelected = true
        selectedDataArray.append(dataArray[indexPath.row])
    }
    
    }

      // MARK: - Buttons actions
    
    @IBAction func useGiftBtnAction(_ sender: UIButton) {
        delegate?.selectedGiftCards(selectedArray: self.selectedDataArray)
        self.navigationController?.popViewController(animated: true)
    }

    
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
