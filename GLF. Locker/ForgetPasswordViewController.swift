//
//  ForgetPasswordViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/29/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {

    @IBOutlet weak var emailFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetPasswordBtnAction(_ sender: UIButton) {
       
        if  DataManager.sharedInstance.isValidEmail(self.emailFld.text!){
        }else{
           DataManager.sharedInstance.printAlertMessage(message:"Enter Valid Email", view:self)
            return
        }
        
        NetworkManager.performRequest(type:.post, method: "login/ResetPassword", parameter: ["PersonalEmail":emailFld.text! as AnyObject,"AcademyID":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let responce = object as! Bool
            if responce{
//                  DataManager.sharedInstance.printAlertMessage(message:"Email Successfully Sent", view:UIApplication.getTopestViewController()!)
                Utility.showAlertView(title: "Success!", message:"Email Successfully Sent")

                self.dismiss(animated: true, completion:nil)
           
            }else{
                 DataManager.sharedInstance.printAlertMessage(message:"Please enter a valid email", view:UIApplication.getTopestViewController()!)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
    }

}
