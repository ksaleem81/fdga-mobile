//
//  ForgetPasswordViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/29/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
protocol PopOverDismissed {
    func popoverDismissedOrNot()
}
class PaymentPopUpViewController: UIViewController {

    var filterPaymentTrack = [[String:AnyObject]]()
    @IBOutlet weak var paymentTypeBtn: UIButton!
    var  eventId = ""
    var delegateForBack : PopOverDismissed?
    var selectedPaymentTypeId = ""
    var paymentMethdsArray = [[String:AnyObject]]()
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.paymentTypeBtn.setTitle("Select Payment Type", for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- buttons actions
    @IBAction func resetPasswordBtnAction(_ sender: UIButton) {
       
       
        //https://app.glflocker.com/OrbisAppBeta/api/Academy/UpdatePaidStatus/57114/true/1

        NetworkManager.performRequest(type:.get, method: "Academy/UpdatePaidStatus/\(eventId)/\("True")/\(selectedPaymentTypeId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            if  let flage = object as? Bool {
                
                if flage{
                    self.dismiss(animated: false, completion:nil)
                    self.delegateForBack?.popoverDismissedOrNot()
                  // DataManager.sharedInstance.printAlertMessage(message:"Succefully Updated", view:self)
                }else{
                     DataManager.sharedInstance.printAlertMessage(message:"SomeThing Wrong!", view:self)
                }
            }
            
            
        }) { (error) in
            print(error!)
//            DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
            self.showInternetError(error: error!)
        }
        
    
    }
    
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
    }

    @IBAction func pickerBtnAction(_ sender: UIButton) {
        
        if paymentMethdsArray.count > 0{
        }else{
            return
        }
        var dataAray = [String]()
        filterPaymentTrack = [[String:AnyObject]]()
        for dic in paymentMethdsArray{
            if let val = dic["SubPaymentName"] as? String{
                
                
                filterPaymentTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT TYPE", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            
            self.paymentTypeBtn.setTitle("\(indexes!)", for: .normal)
            
            if let id = self.filterPaymentTrack[values]["SubPaymentId"] as? Int{
                self.selectedPaymentTypeId = "\(id)"
            }
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }
}
