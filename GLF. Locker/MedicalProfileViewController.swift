//
//  PersonalProfileViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/13/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class MedicalProfileViewController: UIViewController,UITextViewDelegate{


    @IBOutlet weak var treatmentFld: UITextField!
    @IBOutlet weak var injuryFld: UITextField!
    var playerData = [String:AnyObject]()
    var playerInfo = [String:AnyObject]()
    var controllerInfo = [String:AnyObject]()
    @IBOutlet weak var notableFld: UITextView!
    
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        print(playerData)
       settingPlaceHolderForTextView()
         getPlayerInfo()
        // Do any additional setup after loading the view.
        settingRighMenuBtn()
        
    }
     let placeholderLabel = UILabel()
    func settingPlaceHolderForTextView()  {
        notableFld.delegate = self
        placeholderLabel.text = "Any notable medical issues:"
        placeholderLabel.font = notableFld.font
        placeholderLabel.sizeToFit()
        notableFld.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (notableFld.font?.pointSize)! / 2)
        placeholderLabel.textColor = notableFld.textColor
        placeholderLabel.isHidden = !notableFld.text.isEmpty
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MedicalProfileViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    func getPlayerInfo()  {
        
        var playerId = ""
        if currentUserLogin == 4 {
            
            if let id = playerData["Userid"] as? Int{
                playerId = "\(id)"
            }
        }else{
        if let id = playerData["UserID"] as? Int{
            playerId = "\(id)"
            }
        }
 
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerMedicalInfo/\(playerId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
                
                
            }
            
            let data = object as! [String : AnyObject]
            self.playerInfo = data
            self.fillingPlayerInformation()
        
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    

   
   func fillingPlayerInformation()  {
        
        if let titl = playerInfo["Treatments"] as? String{
            treatmentFld.text = titl
        }
        if let fname = playerInfo["Injuries"] as? String{
            injuryFld.text = fname
        }
        if let fname = playerInfo["MedicalIssues"] as? String{
            if fname == "" {
                notableFld.text = ""
            placeholderLabel.isHidden = !notableFld.text.isEmpty
            }else{
                notableFld.text = fname
            }
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func saveBtnAction(_ sender: UIButton) {
       submitForm()
    }
    
    func submitForm()  {
        var userID1 = ""
        if currentUserLogin == 4 {
            if let id = playerData["Userid"] as? Int{
                userID1 = "\(id)"
            }
        }else{
        if let user1 = playerData["UserID"] as? Int{
             userID1 = "\(user1)"
            }
        }

        let parameters : [String:String] = ["Injuries":injuryFld.text! ,"Treatments":treatmentFld.text! ,"MedicalIssues":notableFld.text!,"UserId":userID1]
            print(parameters)
        NetworkManager.performRequest(type:.post, method: "Student/UpdatePlayerMedicalInfo", parameter:parameters as [String : AnyObject]?, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let responce = object as! String
            switch responce {
            case   "-1" :
                
                DataManager.sharedInstance.printAlertMessage(message:"Unable To Complete Order!", view:self)
                return
                
            default: break
                
            }
          
            print("success")
            
            if responce == "1" {
                _ = self.navigationController?.popViewController(animated: true)
                DataManager.sharedInstance.printAlertMessage(message: "Successfully Updated Medical Details!", view:UIApplication.getTopestViewController()!)
            }
           
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    @IBAction func cancelBtnAction(_ sender: UIButton) {
      _ =  self.navigationController?.popViewController(animated: true)
    }

  

}
