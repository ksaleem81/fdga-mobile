//
//  SideMenuTableViewController.swift
//  MyAlist
//
//  Created by Apple PC on 26/08/2016.
//  Copyright © 2016 ArhamSoft. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class SideMenuForCoachTableViewController: UITableViewController, OrbisCameraViewControllerDelegate, GalleryViewControllerDelegate {

    @IBOutlet weak var versionLbl: UILabel!
    var defaultArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STUDENTS","image":"student_list-1"],["name":"STORE","image":"icon_26"],["name":"SCHEDULE","image":"schedule_icon2"],["name":"UPCOMING LESSONS","image":"book_lesson2-1"],["name":"VIEW ALL BOOKINGS","image":"bookinall_btnsamll"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"VIDEO>","image":"plus_icon-1"],["name":"MORE>","image":"plus_icon-1"]]
    var backupDefaultArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STUDENTS","image":"student_list-1"],["name":"STORE","image":"icon_26"],["name":"SCHEDULE","image":"schedule_icon2"],["name":"UPCOMING LESSONS","image":"book_lesson2-1"],["name":"VIEW ALL BOOKINGS","image":"bookinall_btnsamll"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"VIDEO>","image":"plus_icon-1"],["name":"MORE>","image":"plus_icon-1"]]
    
    var videoOpendArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STUDENTS","image":"student_list-1"],["name":"STORE","image":"icon_26"],["name":"SCHEDULE","image":"schedule_icon2"],["name":"UPCOMING LESSONS","image":"book_lesson2-1"],["name":"VIEW ALL BOOKINGS","image":"bookinall_btnsamll"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"VIDEO>","image":"minus_icon-1"],["name":"RECORD VIDEO","image":"video_icon-1"],["name":"APP GALLERY","image":"media_icon"],["name":"IMPORT VIDEO FROM DEVICE","image":"video_icon-1"],["name":"SNAPSHOTS","image":"video_icon-1"],["name":"UPLOAD PROGRESS","image":"video_icon-1"],["name":"VIDEO SETTINGS","image":"video_icon-1"],["name":"MORE>","image":"plus_icon-1"]]
    var moreArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STUDENTS","image":"student_list-1"],["name":"STORE","image":"icon_26"],["name":"SCHEDULE","image":"schedule_icon2"],["name":"UPCOMING LESSONS","image":"book_lesson2-1"],["name":"VIEW ALL BOOKINGS","image":"bookinall_btnsamll"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"VIDEO>","image":"plus_icon-1"],["name":"MORE>","image":"minus_icon-1"],["name":"SWITCH ACCOUNT","image":"switchaccount"],["name":"T&C'S","image":"term_icon"],["name":"PRIVACY","image":"privacy_icon"],["name":"LOGOUT","image":"logout-1"]]
        var selectedCellColor = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    //tableView.register(MyAlist.SideMenuTableViewCell.self, forCellReuseIdentifier: "SideMenuTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let appVersion: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
     //   versionLbl.text = "GLF. App " + (appVersion as! String?)!
        
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "home")
        self.tableView.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        self.view.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        let tableViewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        tableViewFooter.backgroundColor = UIColor.clear
        let version = UILabel(frame: CGRect(x:self.view.frame.midX-100, y:0, width:150, height:30))
        version.font = version.font.withSize(11)
        version.text = "\(currentTarget) App " + (appVersion as! String?)!
        version.textColor = UIColor.init(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0)
        version.textAlignment = .center;
        tableViewFooter.addSubview(version)
        tableView.tableFooterView  = tableViewFooter
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let sideView = UIView.init(frame: CGRect(x: 0, y: -80, width: 1, height: 1000))
        sideView.backgroundColor = UIColor.init(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0)
        sideView.tag = 123
        self.view.addSubview(sideView)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.view.viewWithTag(123)?.removeFromSuperview()
         self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42.0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of rows
        return defaultArray.count//17
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        cell.cellTitle.text = defaultArray[indexPath.row]["name"]
        cell.cellImage.image = UIImage(named: defaultArray[indexPath.row]["image"]!)
        if selectedCellColor == indexPath.row {
            cell.backgroundColor =  UIColor.init(red: 204/255, green: 213/255, blue: 35/255, alpha: 1.0)
        }else{
            cell.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let window = UIApplication.shared.delegate?.window
        var controller  = UIViewController()
        if defaultArray.count == 10 {
            
            if defaultArray[indexPath.row]["name"] == "VIDEO>" {
                defaultArray = videoOpendArray
                selectedCellColor = indexPath.row
                tableView.reloadData()
                return

            }else if defaultArray[indexPath.row]["name"] == "MORE>"{
                defaultArray = moreArray
                 selectedCellColor = indexPath.row
                tableView.reloadData()
                return

            }
            
        }else if defaultArray.count  == 16 {
            if defaultArray[indexPath.row]["name"] == "VIDEO>" {
                defaultArray = backupDefaultArray
                 selectedCellColor = indexPath.row
                tableView.reloadData()
                return
            }else if defaultArray[indexPath.row]["name"] == "MORE>"{
                defaultArray = moreArray
                 selectedCellColor = indexPath.row
                tableView.reloadData()
                return
            }
        
            
        }else if defaultArray.count == 14 {
            if defaultArray[indexPath.row]["name"] == "VIDEO>" {
                defaultArray = videoOpendArray
                 selectedCellColor = indexPath.row
                tableView.reloadData()
                return
            }else if defaultArray[indexPath.row]["name"] == "MORE>"{
                defaultArray = backupDefaultArray
                selectedCellColor = indexPath.row
                tableView.reloadData()
                return
            }
           
        }
        
        
        if indexPath.row == 0 {
//            let vcOnTop = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-1]
//            
//            if (vcOnTop?.isKind(of:DashBoardViewController.self))!{
//            dismiss(animated: true, completion: nil)
//                return
//            }
            controller = (self.storyboard?.instantiateViewController(withIdentifier: "Dashboard"))!
                        self.parent?.dismiss(animated: true, completion: {
                window??.rootViewController = controller
            })
        }
        else if indexPath.row == 13 && defaultArray.count == 14 {
//        self.logoutService()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.parent?.dismiss(animated: false, completion: {
                appDelegate.showInitialController()
            })

           
        }
        else if indexPath.row == 7 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            self.navigationController?.pushViewController(controller, animated: true)
            
//           controller = (self.storyboard?.instantiateViewController(withIdentifier: "CartNavigation"))!
//            dismiss(animated: true, completion: {
//                window??.rootViewController = controller
//            })
        }
        else if indexPath.row == 2 {
 
            var url = storeLink
//            if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
//                url = url + "\(id)"
//            }
            
            UIApplication.shared.openURL(NSURL(string:url)! as URL)
                        self.parent?.dismiss(animated: true, completion: nil)
            
        }else if indexPath.row == 3 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 5 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AllBookingViewController") as! AllBookingViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if indexPath.row == 14 {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecommendedViewController") as! RecommendedViewController
            controller.fromVideoQuality = true
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 10 && defaultArray.count == 14 {
      
            controller = (self.storyboard?.instantiateViewController(withIdentifier: "SwitchAccountViewController"))!
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else if indexPath.row == 4 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LessonsViewController") as! LessonsViewController
            controller.fromDashBoard = true
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 1 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CustomeTabViewController") as! CustomeTabViewController
            controller.viewAppearedOrNot = true
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 6 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            self.navigationController?.pushViewController(controller, animated: true)
       
        }
        else if (indexPath.row == 13) {
            let progressVC = UploadProgressViewController(nibName: "UploadProgressViewController", bundle: nil)
            self.navigationController?.pushViewController(progressVC, animated: true)
        }
        else if (indexPath.row == 9) {
            sendNotification("RecordVideo")
        }
        else if (indexPath.row == 10) {
            sendNotification("ShowVideoGallery")
        }
        else if (indexPath.row == 11 && defaultArray.count == 16){
            sendNotification("ImportFromDevice")
        }
        else if (indexPath.row == 12 && defaultArray.count == 16) {
            sendNotification("ShowImageGallery")
            }
        else if indexPath.row == 11 && defaultArray.count == 14{
        showingTT()

        
        }else if indexPath.row == 12 && defaultArray.count == 14{
            
           showingPP()

        }
        else {
            self.parent?.dismiss(animated: true, completion: nil)
        }
    }

    func showingTT()  {
        
        var titleAcademy = ""
                        if let titl =  DataManager.sharedInstance.currentAcademy()!["Terms"] as? String{
                            titleAcademy = titl
                        }
                        
            //            privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
        //                privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"

                        print(privacyTermsPDF)
                        UIApplication.shared.openURL(NSURL(string:titleAcademy)! as URL)
                        self.parent?.dismiss(animated: true, completion: nil)

    }
func showingPP()  {
     var titleAcademy = ""
                if let titl =  DataManager.sharedInstance.currentAcademy()!["PrivacyPolicy"] as? String{
                    privacyTermsPDF = titl
                }
                
    //            privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
//                privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"

                print(privacyTermsPDF)
                UIApplication.shared.openURL(NSURL(string:privacyTermsPDF)! as URL)
                self.parent?.dismiss(animated: true, completion: nil)
}
    
    func logoutService() {
        
        var userID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
        }
        
        NetworkManager.performRequest(type:.post, method: "Login/logout", parameter: ["Userid":userID as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"Error\nInvalid username/password", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                break
            }
            
            let success = object as! Bool
            if success{
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                self.parent?.dismiss(animated: false, completion: {
                    appDelegate.showInitialController()
                })
                
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            }
            
            
        }) { (error) in
//            print(error!)
            self.showInternetError(error: error!)
        }
    }

    

    func sendNotification(_ notification: String) {
        DispatchQueue.main.async {
                        self.parent?.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: nil)
            })
        }
    }
    
    
    
}
