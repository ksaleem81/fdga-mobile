//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
import EventKit
import EventKitUI
@available(iOS 10.0, *)
class TotalStudentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
     var latitude = 0.00
     var longitude = 0.00
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var map:GMSMapView?
    @IBOutlet weak var searchFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        searchFld.inputAccessoryView = addToolBar()
        searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        settingRighMenuBtn()
            tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.contentInset = .init(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(TotalStudentsViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)}
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
     
        if #available(iOS 11, *) {
            self.navigationController?.isNavigationBarHidden = true
        }else{
        }
        self.tabBarController?.navigationController?.isNavigationBarHidden = false
        
        let view1 = self.tabBarController?.view.viewWithTag(999)
        view1?.isHidden = false
        
        if currentUserLogin == 4 {
        }else{
            refreshTableView()
        }

    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "StudentName CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        print ("filterd = \(data)")
       self.tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(TotalStudentsViewController.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
   @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
       
     print(data.count)
        searchFld.resignFirstResponder()
    }


    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       self.tabBarController?.navigationItem.title  = "STUDENTS"
    }
    
    func refreshTableView()  {
        
       //GetAllStudents/73/4163
        NetworkManager.performRequest(type:.get, method: "academy/GetAllStudents/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(DataManager.sharedInstance.currentUser()!["coachId"] as AnyObject)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
          
            default:
                     DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                     return
                
                
              
            }

            self.data = object as! [[String : AnyObject]]
              DispatchQueue.main.async {
            self.originaldata = object as! [[String : AnyObject]]
            let lab = self.tabBarController?.view.viewWithTag(222) as? UILabel
            lab?.text = "\(self.originaldata.count)"
           print(lab?.text!)
                self.tableView.reloadData()
            }
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "TotalStudentsTableViewCell", for: indexPath) as! TotalStudentsTableViewCell
        if let title = data[indexPath.row]["StudentName"] as? String{
           cell.companyLbl.text = title
        }

        if let url = data[indexPath.row]["PhotoURL"] as? String{
            var originalUrl = url.replacingOccurrences(of: "~", with: "", options: .regularExpression)
             originalUrl = imageBaseUrl + originalUrl
            cell.studentImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNotSmall"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
                
                DispatchQueue.main.async (execute: {
                    
                    if let _ = image{
                        
                        cell.studentImage.image = image;
                    }
                    else{
                        cell.studentImage.image = UIImage(named:"photoNotSmall")
                    }
                });
                
            })
          
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath) as! TotalStudentsTableViewCell
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailsViewController") as! StudentDetailsViewController
        if let id = data[indexPath.row]["StudentID"] as? Int{
            controller.studentData = data[indexPath.row]
            controller.userImageData = currentCell.studentImage.image!
            controller.studentTitle = currentCell.companyLbl.text!
            _ = id
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

}

