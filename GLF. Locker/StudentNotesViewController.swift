//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
//import Crashlytics

class StudentNotesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
    var studentData = [String:AnyObject]()

    var skillCategory: String = "1"
    var skillTypes: String = "1"
    var filteredApplied = false
    var mediaType = "4"
    @IBOutlet weak var tableView: UITableView!
    var forVocher = false
    @IBOutlet weak var searchFld: UITextField!
    @IBOutlet weak var addNoteBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
//        print(originaldata)
       searchFld.inputAccessoryView = addToolBar()
       data = originaldata
       searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
       settingRighMenuBtn()
       tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
         self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        if currentUserLogin == 4 {
            
        }else{
            self.addNoteBtn.setTitle("BELOW ARE STUDENT NOTES", for: .normal)
        }
    }

    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(StudentNotesViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
  
   @objc func rightBarBtnAction() {
    present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "Description CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        print ("filterd = \(data)")
       self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(StudentNotesViewController.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
   @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
        }else{
            return
        }
       
     print(data.count)
        searchFld.resignFirstResponder()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if filteredApplied{
            gettingNotesFromServer(skilTyp: skillTypes, skilCategory: skillCategory)
        }else{
            gettingNotesFromServer(skilTyp: "1", skilCategory: "1")
        }
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentNotesTableViewCell", for: indexPath) as! StudentNotesTableViewCell
        if let title = data[indexPath.row]["Description"] as? String{
           cell.notesLbl.text = title
        }
        
            if let date = data[indexPath.row]["CreationDate"] as? String{
                let dateOrignal = date.components(separatedBy: "T")
                if dateOrignal.count > 0 {
                    cell.dateLbl.text = "Uploaded " + DataManager.sharedInstance.getFormatedDateGivenPAttern(date: dateOrignal[0], formate: "yyyy-MM-dd", formateReturn: "\(globalDateFormate)")//dateOrignal[0]
                }
            }
        
               return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NoteViewController") as! NoteViewController
        controller.previousData = self.data[indexPath.row]
        controller.fromPlayerMedia = true
        controller.fromPlayerMediaEditting = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func gettingNotesFromServer(skilTyp:String,skilCategory:String)  {
     
        var playerId = ""
        if currentUserLogin == 4 {
            
            if let id = DataManager.sharedInstance.currentUser()?["Userid"] as? Int{
                playerId = "\(id)"
            }

        }else{
            
            if let id = studentData["UserID"] as? Int{
                playerId = "\(id)"
            }
        }
       
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        let parameters : [String:String] = ["UserId":playerId ,"AcademyId":acadmyId ,"MediaTypeId":mediaType,"SkillTypes":skilTyp,"SkillCategory":skilCategory]
        
        print(parameters)
        
        NetworkManager.performRequest(type:.post, method: "Academy/GetUserMedia", parameter:parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            self.filteredApplied = false
            self.originaldata  = object as! [[String : AnyObject]]
            self.data = self.originaldata
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    @IBAction func filterBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
   
    @IBAction func addNoteBtnAction(_ sender: UIButton) {
        
        if currentUserLogin == 4 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "NoteViewController") as! NoteViewController
            controller.fromPlayerMedia = true
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
        }
    
    }

}
extension StudentNotesViewController : FilterDataAcordingToThisJson{

    func seletedFilteredKeys(skilCategory:String,skillType:String){
        print(skilCategory,skillType)
         filteredApplied = true
        skillCategory = skilCategory
        skillTypes = skillType
        gettingNotesFromServer(skilTyp: skillTypes, skilCategory: skillCategory)
    }
}

