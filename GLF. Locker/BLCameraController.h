//
//  BLCameraController.h
//  AVCam
//
//  Created by Nasir Mehmood on 7/27/14.
//  Copyright (c) 2014 Nasir Mehmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class BLCameraController;
@protocol BLCameraControllerDelegate <NSObject>

@optional
- (void) blCameraControllerDidStartRecordingVideo:(BLCameraController*)blCameraController;
- (void) blCameraControllerDidStopRecordingVideo:(BLCameraController*)blCameraController;
- (void) blCameraControllerWillStartRecordingVideo:(BLCameraController*)blCameraController;
- (void) blCameraControllerWillStopRecordingVideo:(BLCameraController*)blCameraController;

- (void) blCameraControllerDidCaptureSampleBuffer:(CMSampleBufferRef)sampleBuffer;

@end


@interface BLCameraController : UIViewController<AVCaptureAudioDataOutputSampleBufferDelegate,AVCaptureVideoDataOutputSampleBufferDelegate>

@property(nonatomic, weak) id<BLCameraControllerDelegate> delegate;
@property BOOL rotate;

- (void) setOverlayView:(UIView*)overlayView;
- (void) setSessionPreset:(NSString*)captureSessionPreset;
- (void) setCaptureDevicePosition:(AVCaptureDevicePosition)captureDevicePosition;
- (void) setCapturedFilePath:(NSString*)capturedFilePath;
- (void) setCaptureDuation:(int)captureDuation;

- (BOOL) isFlashAvailable;
- (void) turnFlash:(BOOL)on_off;

- (void) setupAndStartCaptureSession;
- (void) startVideoRecording;
- (void) stopVideoRecording;

- (void) stopAndTearDownCaptureSession;

- (void) capturePhoto;

@end
