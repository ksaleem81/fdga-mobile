//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu

protocol VideoWebViewControllerDelegate {
    func videosWebViewControllerDidSelectedVideo(info: [String: Any], skillCategory: String, skillType: String)
}


class VideoWebViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()

    var isComeFromSelectVideos = false
    var delegate: VideoWebViewControllerDelegate?
    var skillCategory: String = "1"
    var skillTypes: String = "1"
    
    var filteredApplied = false
    
    var mediaType = "1"
    @IBOutlet weak var tableView: UITableView!
    var forVocher = false
    var fromPlayerRecended = false
 
    @IBOutlet weak var searchFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        
        //GetAllAcademiesOrderByLatLong
        if mediaType == "2" {
        self.navigationItem.title = "PHOTOS"
        }else if mediaType == "3"{
        self.navigationItem.title = "DRILLS"
        }else{
        self.navigationItem.title = "VIDEOS"
        }
     
       searchFld.inputAccessoryView = addToolBar()
       data = originaldata
       searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
       settingRighMenuBtn()
            tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
         self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(VideoWebViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "Description CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        
        print ("filterd = \(data)")
        
        

       self.tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(VideoWebViewController.donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar
    }
    
    @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
       
        print(data.count)
        searchFld.resignFirstResponder()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if filteredApplied {
            gettingVideosFromServer(skilTyp: skillTypes, skilCategory: skillCategory)

        }else{
        if fromPlayerRecended{
            gettingVideosRecommended()
        }else{
            gettingVideosFromServer(skilTyp: skillTypes, skilCategory: skillCategory)
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoWebTableViewCell", for: indexPath) as! VideoWebTableViewCell
        if let title = data[indexPath.row]["Description"] as? String{
           cell.companyLbl.text = title
        }
        
        if fromPlayerRecended {
            if let title = data[indexPath.row]["RecomendedBy"] as? String{
                cell.recomndByLbl.text = "Recomended By: " + title
            }else{
                cell.recomndByLbl.text = ""
            }
            
        }else{
            cell.recomndByLbl.text = ""
        }
      
        
        
        if mediaType == "2" {
       cell.durationLbl.isHidden = true
        }else{
            
        }
        if let title = data[indexPath.row]["Duration"] as? Int{

           let realTime = self.secondsToMinutesSeconds(seconds: title)
            var minutes = "0"
            var seconds = "0"
            if realTime.0 > 9 {
                minutes = "\(realTime.0)"
            }else{
                minutes = "0"+"\(realTime.0)"
            }
            
            if realTime.1 > 9 {
                seconds = "\(realTime.1)"
            }else{
                seconds = "0"+"\(realTime.1)"
            }
            
            cell.durationLbl.text = "Duration: " + "\(minutes):\(seconds)"
        }
      
        var fileUrl = ""
        if let url = data[indexPath.row]["ThumbnilURL"] as? String {
            fileUrl = url
        }
        if let url = data[indexPath.row]["IsEmbedded"] as? Bool  {
            
            if url {
                cell.youtubeBtn.isHidden = false
            }else{
                cell.youtubeBtn.isHidden = true
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
                fileUrl = baseUrlForVideo + fileUrl
            }
        }
        
        cell.videoImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
            
            DispatchQueue.main.async (execute: {             
                if let _ = image{
                    
                    cell.videoImage.image = image;
                }
                else{
                    cell.videoImage.image = UIImage(named:"photoNot")
                    
                }
            });
            
        })

        
               return cell
    }
    
    func secondsToMinutesSeconds (seconds : Int) -> ( Int, Int) {
        return ((seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isComeFromSelectVideos == false {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MediaDetailViewController") as! MediaDetailViewController
            controller.mediaType = mediaType
            controller.dataDictionary = data[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else {
            if delegate != nil {
                delegate?.videosWebViewControllerDidSelectedVideo(info: data[indexPath.row], skillCategory: skillCategory, skillType: skillTypes)
            }
        }
        
    }
    
    func gettingVideosFromServer(skilTyp:String,skilCategory:String)  {
     
      
        var playerId = ""
        if let id = DataManager.sharedInstance.currentUser()?["Userid"] as? Int{
            playerId = "\(id)"
        }
    
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        let parameters : [String:String] = ["UserId":playerId ,"AcademyId":acadmyId ,"MediaTypeId":mediaType,"SkillTypes":skilTyp,"SkillCategory":skilCategory]
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "Academy/GetUserMedia", parameter:parameters as [String : AnyObject]?, view:self.appDelegate.window , onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.filteredApplied = false
           self.originaldata  = object as! [[String : AnyObject]]
             self.data = self.originaldata
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func gettingVideosRecommended()  {
        
        var playerId = ""
        if let id = DataManager.sharedInstance.currentUser()?["Userid"] as? Int{
            playerId = "\(id)"
        }
  
        let parameters : [String:String] = ["CoachID":playerId]
        
        print(parameters)
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetMediaRecommendedToCoach/\(playerId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.originaldata  = object as! [[String : AnyObject]]
            self.data = self.originaldata
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    @IBAction func filterBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
   

}
extension VideoWebViewController : FilterDataAcordingToThisJson{

    func seletedFilteredKeys(skilCategory:String,skillType:String){
        print(skilCategory,skillType)
         filteredApplied = true
        skillCategory = skilCategory
        skillTypes = skillType
        
        gettingVideosFromServer(skilTyp: skillType, skilCategory: skilCategory)
    }
}

