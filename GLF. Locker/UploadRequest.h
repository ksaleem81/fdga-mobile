//
//  UploadRequest.h
//  sibme
//
//  Created by Nasir Mehmood on 3/21/13.
//
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <AVFoundation/AVFoundation.h>
#import "OrbisConstants.h"

#define kLessonTypeOneToOne 1
#define kLessonTypeClass 2
#define kLessonTypeMedia 1000
#define kChunkSize 200*1024



//Live
//urlsplacechanged


//#define kMediaUploadingURLOld @"https://app.firstdegree.golf/WebAPI/api/Coach/UploadLessonMediaFile"
//#define kAddToLessonURLOld @"https://app.firstdegree.golf/WebAPI/api/Coach/AddLessonMedia"
//#define kVideoStatusURLOld @"https://app.firstdegree.golf/WebAPI/api/Academy/getmediastatusbyid/"
//
//#define kMediaUploadingURL @"https://app.firstdegree.golf/WebAPI/api/Coach/UploadLessonMediaFile"
//#define kAddToLessonURL @"https://app.firstdegree.golf/WebAPI/api/Coach/AddLessonMedia"
//#define kVideoStatusURL @"https://app.firstdegree.golf/WebAPI/api/Academy/getmediastatusbyid/"



//Beta

#define kMediaUploadingURLOld @"http://appfdga.glfbeta.com/WebAPI/api/Coach/UploadLessonMediaFile"
#define kAddToLessonURLOld @"http://appfdga.glfbeta.com/WebAPI/api/Coach/AddLessonMedia"
#define kVideoStatusURLOld @"http://appfdga.glfbeta.com/WebAPI/api/Academy/getmediastatusbyid/"

#define kMediaUploadingURL @"http://appfdga.glfbeta.com/WebAPI/api/Coach/UploadLessonMediaFile"
#define kAddToLessonURL @"http://appfdga.glfbeta.com/WebAPI/api/Coach/AddLessonMedia"
#define kVideoStatusURL @"http://appfdga.glfbeta.com/WebAPI/api/Academy/getmediastatusbyid/"



typedef enum : NSUInteger {
    
    kMediaTypeVideo=1,
    kMediaTypePhoto,
    kMediaTypeDrill,
    kMediaTypeOverlay
    
} OrbisMediaType;


@class UploadRequest;

@protocol UploadRequestDelgate <NSObject>

- (void) uploadRequestCompleted:(UploadRequest*)uploadRequest;
- (void) uploadRequestFailed:(UploadRequest*)uploadRequest;
- (void) uploadRequestStopped:(UploadRequest*)uploadRequest;

@end

@interface UploadRequest : NSObject{
    
}

@property (nonatomic) BOOL isUploading;
@property (nonatomic) BOOL shouldUpload;
@property (nonatomic) NSUInteger chunkSize;

@property (nonatomic, strong) NSMutableDictionary *requestOptions;

@property (nonatomic, weak) id<UploadRequestDelgate> uploadRequestDelegate;

- (id) initWithOptions:(NSDictionary*) options uploadRequestDelegate:(id<UploadRequestDelgate>) delegate;

- (void) startRequest;
- (void) cancelRequest;
- (NSString*)uniqueMediaID;
- (void)uploadingCompletedWithResponse:(NSHTTPURLResponse *)response responseData:(NSData*)responseData mediaInfo:(NSDictionary*)mediaInfo;
- (void) updatePogressWithMediaInfo:(NSDictionary*)mediaInfo didSendData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite;
- (void) uploadingFailedWithError:(NSError*)error mediaInfo:(NSDictionary*)mediaInfo;

- (void) startUploading;
- (void) stopUploading;
- (void) resumeUploading;
- (void) cancelUploading;
+(void)uploadVideoDimentions:(int )mediaId andWidth:(int )width andHeight:(int  )height;

- (UploadRequestStatus) requestStatus;
- (void) setRequestStatus:(UploadRequestStatus)status;

@end



