//
//  CustomeTabViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/2/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu

class MessagesCustomeTabViewController: UITabBarController {

    var view1 = UIView()
    var buton1 = UIButton()
    var buton2 = UIButton()
    var buton3 = UIButton()
    var buton4 = UIButton()
    var label1Val = UILabel()
    var label2Val = UILabel()
    var label3Val = UILabel()
    var label4Val = UILabel()
    
    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.isHidden = true
        addingCustomeViewToTabbar()
        buton1.isSelected = true
        settingRighMenuBtn()
         self.selectedIndex = 0
    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MessagesCustomeTabViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    //making tabbar programmatically
    func addingCustomeViewToTabbar()  {
        
        var screenHeight = view.frame.size.height
        if Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() {
            screenHeight = screenHeight - 25
        }

        view1 =  UIView.init(frame: CGRect(x:0,y:screenHeight-70,width:view.frame.size.width , height:70))
        view1.tag = 999
         buton1 = UIButton.init(frame:CGRect(x:0, y:0,width:view.frame.size.width/3, height:70) )
        buton1.addTarget(self, action:#selector(tabbarBton1(button:)), for:.touchUpInside)
           //adding bottom label button 1
        let label1 = UILabel.init(frame: CGRect(x:0,y:view1.frame.size.height-25,width:view.frame.size.width/3 , height:25))
        label1.textAlignment = .center
        label1.font = UIFont(name: "Helvetica-Bold", size: 10)
        label1.text = "INBOX"
        label1.textColor = UIColor.white
        label1.bringSubview(toFront: view)
     
        view1.addSubview(buton1)
        view1.addSubview(label1)
        buton1.setImage(UIImage(named:"email"), for: .normal)
        buton1.setImage(UIImage(named:"email_selec"), for: .selected)//
        buton1.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton1.imageView?.contentMode = .scaleAspectFit
        buton1.tag = 0001
        buton1.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton1.layer.borderWidth = 0.5
      
        buton2.imageView?.contentMode = .scaleAspectFit
         buton2 = UIButton.init(frame:CGRect(x:view.frame.size.width/3*1, y:0, width:view.frame.size.width/3,height: 70) )
        buton2.tag = 0002
        buton2.setImage(UIImage(named:"add_white"), for: .normal)//while_circle_ac
        buton2.setImage(UIImage(named:"add"), for: .selected)
        buton2.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton2.imageView?.contentMode = .scaleAspectFit
        buton2.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton2.layer.borderWidth = 0.5
        //adding bottom label button 2
        let label2 = UILabel.init(frame: CGRect(x:view.frame.size.width/3*1,y:view1.frame.size.height-25,width:view.frame.size.width/3 , height:25))
        label2.textAlignment = .center
        label2.font = UIFont(name: "Helvetica-Bold", size: 10)
        label2.text = "NEW"
        label2.numberOfLines = 0
        label2.textColor = UIColor.white
        label2.bringSubview(toFront: view)
        view1.addSubview(buton2)
        view1.addSubview(label2)
        
        label2Val = UILabel.init(frame: CGRect(x:view.frame.size.width/3*1,y:13,width:view.frame.size.width/3 , height:25))
        label2Val.tag = 222
        label2Val.textAlignment = .center
        label2Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label2Val.text = ""
        label2Val.textColor = UIColor.black
        label2Val.bringSubview(toFront: view)
        view1.addSubview(label2Val)
        
         buton3 = UIButton.init(frame:CGRect(x:view.frame.size.width/3*2,y: 0, width:view.frame.size.width/3, height:70) )
        buton3.setImage(UIImage(named:"email_forward"), for: .normal)
        buton3.setImage(UIImage(named:"email_forward_selec"), for: .selected)
        buton3.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton3.imageView?.contentMode = .scaleAspectFit
        buton3.tag = 0003
        buton3.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton3.layer.borderWidth = 0.5
        //adding bottom label button 3
        let label3 = UILabel.init(frame: CGRect(x:view.frame.size.width/3*2,y:view1.frame.size.height-25,width:view.frame.size.width/3 , height:25))
        label3.textAlignment = .center
        label3.font = UIFont(name: "Helvetica-Bold", size: 10)
        label3.text = "SENT"
        label3.textColor = UIColor.white
        label3.bringSubview(toFront: view)
        view1.addSubview(buton3)
        view1.addSubview(label3)

        label3Val = UILabel.init(frame: CGRect(x:view.frame.size.width/3*2,y:13,width:view.frame.size.width/3 , height:25))
        label3Val.tag = 333
        label3Val.textAlignment = .center
        label3Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label3Val.text = ""
        label3Val.textColor = UIColor.black
        label3Val.bringSubview(toFront: view)
        view1.addSubview(label3Val)
      /*
        buton4 = UIButton.init(frame:CGRect(x:view.frame.size.width/4*3, y:0, width:view.frame.size.width/4, height:70) )
        buton4.setImage(UIImage(named:"while_circle"), for: .normal)
        buton4.setImage(UIImage(named:"while_circle_ac"), for: .selected)
        buton4.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton4.imageView?.contentMode = .scaleAspectFit
        buton4.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton4.layer.borderWidth = 0.5
   //adding bottom label button 4
        let label4 = UILabel.init(frame: CGRect(x:view.frame.size.width/4*3,y:view1.frame.size.height-25,width:view.frame.size.width/4 , height:25))
        label4.textAlignment = .center
        label4.font = UIFont(name: "Helvetica-Bold", size: 10)
        label4.text = "TRASH"
        label4.textColor = UIColor.white
        label4.bringSubview(toFront: view)
        view1.addSubview(buton4)
        view1.addSubview(label4)
 
        label4Val = UILabel.init(frame: CGRect(x:view.frame.size.width/4*3,y:13,width:view.frame.size.width/4 , height:25))
        label4Val.tag = 444
        label4Val.textAlignment = .center
        label4Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label4Val.text = ""
        label4Val.textColor = UIColor.black
        label4Val.bringSubview(toFront: view)
          view1.addSubview(label4Val)*/
        
        buton2.addTarget(self, action:#selector(tabbarBton2(button:)), for:.touchUpInside)
         buton3.addTarget(self, action:#selector(tabbarBton3(button:)), for:.touchUpInside)
        // buton4.addTarget(self, action:#selector(tabbarBton4(button:)), for:.touchUpInside)
       
        view1.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        view1.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        view1.layer.borderWidth = 1.0
        self.view.addSubview(view1)

    }
    
    @objc func tabbarBton1(button:UIButton)  {
       self.selectedIndex = 0
       
        
        if button.isSelected{
           // button.isSelected = false
        }else{
            button.isSelected = true
        }
        buton2.isSelected = false
        buton3.isSelected = false
        buton4.isSelected = false

    }
    
    @objc func tabbarBton2(button:UIButton)  {
      self.selectedIndex = 1
        
       if button.isSelected{
       // button.isSelected = false
        }else{
          button.isSelected = true
        }
        buton1.isSelected = false
        buton3.isSelected = false
        buton4.isSelected = false
    }
    
    @objc func tabbarBton3(button:UIButton)  {
       self.selectedIndex = 2
        if button.isSelected{
         //   button.isSelected = false
        }else{
            button.isSelected = true
        }
        buton1.isSelected = false
        buton2.isSelected = false
        buton4.isSelected = false
    }
    
    func tabbarBton4(button:UIButton)  {
       self.selectedIndex = 3
        if button.isSelected{
          //  button.isSelected = false
        }else{
            button.isSelected = true
        }
        buton1.isSelected = false
        buton3.isSelected = false
        buton2.isSelected = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
     
    }

}
