//
//  CardFieldViewController.swift
//  UI Examples
//
//  Created by Ben Guo on 7/19/17.
//  Copyright © 2017 Stripe. All rights reserved.
//

import UIKit
import Stripe

protocol FetchCardDetails {
    
    func fetchingCardDetails(params:STPCardParams)
}

class CardFieldViewController: UIViewController {

    let cardField = STPPaymentCardTextField()
    var theme = STPTheme.default()
    var delegate : FetchCardDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Card Field"
        view.backgroundColor = UIColor.white
        cardField.center = CGPoint(x: view.frame.size.width  / 2,
                                     y: view.frame.size.height / 2)
        view.addSubview(cardField)
        edgesForExtendedLayout = []
        view.backgroundColor = theme.primaryBackgroundColor
        cardField.backgroundColor = theme.secondaryBackgroundColor
        cardField.textColor = theme.primaryForegroundColor
        cardField.placeholderColor = theme.secondaryForegroundColor
        cardField.borderColor = theme.accentColor
        cardField.borderWidth = 1.0
        cardField.textErrorColor = theme.errorColor
        cardField.postalCodeEntryEnabled = true
    
        navigationItem.leftBarButtonItem =  UIBarButtonItem.init(title: "Cancel", style: .plain, target:self , action: #selector(cancel))
        
        navigationItem.rightBarButtonItem =  UIBarButtonItem.init(title: "Pay", style: .plain, target:self , action: #selector(rightBtn))
        
        navigationItem.rightBarButtonItem?.tintColor = .white
        navigationItem.leftBarButtonItem?.tintColor = .white
        navigationController?.navigationBar.stp_theme = theme
        cardField.numberPlaceholder = "Card Number";
   
    }

    @objc func cancel() {
        
        dismiss(animated: true, completion: nil)
   
    }

    @objc func rightBtn() {

        if cardField.isValid{

            let card: STPCardParams = STPCardParams()
            card.number = cardField.cardNumber
            card.expMonth = cardField.expirationMonth
            card.expYear = cardField.expirationYear
            card.cvc = cardField.cvc
            delegate?.fetchingCardDetails(params: card)
            dismiss(animated: true, completion: nil)
            
        }else{
            
            DataManager.sharedInstance.printAlertMessage(message: "Invalid Card info", view: self)
            return
            
        }
        //        if (![Stripe defaultPublishableKey]) {
//            [self.delegate exampleViewController:self didFinishWithMessage:@"Please set a Stripe Publishable Key in Constants.m"];
//            return;
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        cardField.becomeFirstResponder()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let padding: CGFloat = 15
        cardField.frame = CGRect(x: padding,
                                 y: padding,
                                 width: view.bounds.width - (padding * 2),
                                 height: 50)
        
        
        
    }

}
