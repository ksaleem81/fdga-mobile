//
//  PersonalProfileViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/13/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class PersonalProfileViewController: UIViewController {

    @IBOutlet weak var titleFld: UITextField!
    @IBOutlet weak var fNameFld: UITextField!
    @IBOutlet weak var lNameFld: UITextField!
    @IBOutlet weak var dobFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var phoneFld: UITextField!
    @IBOutlet weak var mobileFld: UITextField!
    @IBOutlet weak var userNameFld: UITextField!
    @IBOutlet weak var twiterFld: UITextField!
    @IBOutlet weak var fbFld: UITextField!
    @IBOutlet weak var skypeFld: UITextField!
    @IBOutlet weak var adress1Fld: UITextField!
    @IBOutlet weak var adress2Fld: UITextField!
    @IBOutlet weak var cityFld: UITextField!
    @IBOutlet weak var countryPickerFld: UITextField!
    @IBOutlet weak var countryFld: UITextField!
    @IBOutlet weak var postalCodeFld: UITextField!
    var studentData = [String:AnyObject]()
    var studentInfo = [String:AnyObject]()
    var countryList = [[String:AnyObject]]()
    var selectedCountryId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(studentData)
         getCountryList()
         getStudentInfo()
        // Do any additional setup after loading the view
        emailFld.keyboardType = .emailAddress
        settingRighMenuBtn()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PersonalProfileViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func getStudentInfo()  {
        var studentId = ""
        if currentUserLogin == 4 {
            
            if let id = studentData["Userid"] as? Int{
                studentId = "\(id)"
            }
        }else{
        if let id = studentData["UserID"] as? Int{
            studentId = "\(id)"
            }
        }
        // https://app.glflocker.com/OrbisAppBeta/api/Academy/GetPlayerPersonalInfo/18593
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerPersonalInfo/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
            let data = object as! [String : AnyObject]
            self.studentInfo = data
        self.fillingUserInformation()
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func getCountryList()  {
   //http://app.glfbeta.com/OrbisWebApi/api/academy/GetCountryLists/
        NetworkManager.performRequest(type:.get, method: "academy/GetCountryLists/", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
            self.countryList = object as! [[String:AnyObject]]
           
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func fillingUserInformation()  {
        
        if let titl = studentInfo["Title"] as? String{
            titleFld.text = titl
        }
        if let fname = studentInfo["FirstName"] as? String{
            fNameFld.text = fname
        }
        if let fname = studentInfo["LastName"] as? String{
            lNameFld.text = fname
        }
        if let fname = studentInfo["County"] as? String{
            countryFld.text = fname
        }
        
        if let date = studentInfo["DateofBirth"] as? String{
            
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                
                if dateOrignal[0] == "0001-01-01"{
                    dobFld.text = "Sun Jan 01 , 2017"
                }else{
                    dobFld.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
                }
            }
            
        }
        if let fname = studentInfo["PersonalEmail"] as? String{
            emailFld.text = fname
        }
        if let fname = studentInfo["Mobile"] as? String{
            mobileFld.text = fname
           // phoneFld.text  = fname
        }
        if let fname = studentInfo["HomeTel"] as? String{
            phoneFld.text  = fname
        }
        
        if let fname = studentInfo["UserName"] as? String{
            userNameFld.text = fname
        }
        if let fname = studentInfo["TwitterId"] as? String{
            twiterFld.text = fname
        }
        if let fname = studentInfo["FacebookId"] as? String{
            fbFld.text = fname
        }
        if let fname = studentInfo["SkypeId"] as? String{
            skypeFld.text = fname
        }
        if let fname = studentInfo["AddressLine1"] as? String{
            adress1Fld.text = fname
        }
        if let fname = studentInfo["AddressLine2"] as? String{
            adress2Fld.text = fname
        }
        if let fname = studentInfo["City"] as? String{
            cityFld.text = fname
        }
        if let fname = studentInfo["PostCode"] as? String{
            postalCodeFld.text = fname
        }
        
        for dic in countryList{
        if let val = dic["CountryId"] as? Int{
                if let country2 = studentInfo["Country"] as? Int{
                    if val == country2 {
                        self.countryPickerFld.text = dic["CountryName"] as? String
                    }
                }
          
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dobBtnAction(_ sender: UIButton) {
            let formator = DateFormatter()
        ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
            
            
            self.dobFld.text =  DataManager.sharedInstance.getFormatedDate(date: title,formate: "dd MMM, yyyy")
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView).show()
    }
    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        submitForm()
    }
    
    func submitForm()  {
        
        let title = titleFld.text!
        let FirstName1 = fNameFld.text!
        let LastName1 = lNameFld.text!
        var dob = dobFld.text!
     
        if dob.count > 0 {
            dob = DataManager.sharedInstance.getFormatedDateForJson(date: dob, formate:"EEE MMM dd , yyyy")
        }
        
        let UserName1 = userNameFld.text!
     
        let Email1 = emailFld.text!
        let phone = phoneFld.text!
        let adres1 = adress1Fld.text!
        let adres2  = adress2Fld.text!
        let twiter = twiterFld.text!
        let skype = skypeFld.text!
        let facebook = fbFld.text!
        let city = cityFld.text!
       // let countryPiker = countryPickerFld.text!
        let countri = self.selectedCountryId//countryFld.text!
        let postCode = postalCodeFld.text!
       let county = self.countryFld.text!
        let mobil	= mobileFld.text!
        
        var userID1 = ""
       
        if let user1 = studentInfo["UserId"] as? Int{
             userID1 = "\(user1)"
        }
        let parameters : [String:String] = ["Title":title ,"PersonalEmail":Email1 ,"City":city ,"County":county,"Country":countri,"UserId":userID1 ,"HomeTel":phone ,"PostCode":postCode,"SkypeId":skype  ,"FacebookId":facebook ,"Mobile":mobil ,"AddressLine1":adres1 ,"AddressLine2":adres2 ,"TwitterId":twiter ,"DateofBirth":dob ,"UserName":UserName1 ,"LastName":LastName1 ,"FirstName":FirstName1 ]
        print(parameters)
        
        NetworkManager.performRequest(type:.post, method: "Student/UpdatePlayerPersonlInfo", parameter:parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let responce = object as! String
            switch responce {
            case   "-1" :
                DataManager.sharedInstance.printAlertMessage(message:"Unable To Complete Order!", view:self)
                return
                
            default: break
                
            }
          
            print("success")
            
            if responce == "1" {
                
               _ = self.navigationController?.popViewController(animated: true)
                DataManager.sharedInstance.printAlertMessage(message: "Successfully Updated Personal Details!", view:UIApplication.getTopestViewController()!)
            }
           
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
 
    @IBAction func cancelBtnAction(_ sender: UIButton) {
      _ =  self.navigationController?.popViewController(animated: true)
    }

    @IBAction func countryPickerAction(_ sender: UIButton) {
        var dataAray = [String]()
       
        for dic in countryList{
            if let val = dic["CountryName"] as? String{
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT COUNTRY", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            
            self.countryPickerFld.text = "\(indexes!)"
            print(values,indexes!)
            if let countryI =  self.countryList[values]["CountryId"] as? Int{
                self.selectedCountryId = "\(countryI)"
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }

}
