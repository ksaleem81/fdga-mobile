//
//  BookLessonVC.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/4/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SDWebImage
import SideMenu
class ChooseCoachVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var collectionData:[[String:AnyObject]] = [[String:AnyObject]]()
    var selectedData = [String:AnyObject]()
    var redeemedData = [String:AnyObject]()
    let arrayOfColors = [UIColor.init(red: 31/255, green: 160/255, blue: 133/255, alpha: 1.0),UIColor.init(red: 44/255, green: 169/255, blue: 145/255, alpha: 1.0),UIColor.init(red: 31/255, green: 160/255, blue: 133/255, alpha: 1.0)]
   
    @IBOutlet weak var voucherBtn: UIButton!
     var isRedeemedVoucher = false
  var previousData = [String:AnyObject]()
    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()
      print(redeemedData)
    self.navigationItem.title = "BOOK A LESSON"
      
    refreshCollectionView()
    settingRighMenuBtn()

        if #available(iOS 11, *) {
            self.collectionView.contentInset = .init(top: 50, left: 0, bottom: 0, right: 0)
        }else{
        }
        gettingGDPStatus()
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(ChooseCoachVC.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) 
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func filteringArrayForPuttingLabCoach()  {
        
        for index in self.collectionData{
            if let pId = index["CoachID"] as? Int{
                //OMG
                if pId == 4253{
                    if currentUserLogin == 4 {
                        self.collectionData.removeAll()
                        self.collectionData.append(index)
                        break
                    }
                }else{
                    
                }
            }
        }
        
    }


    //MARK:- fetching server json
    func refreshCollectionView()  {
        
        var typeIdPro = ""
        if let typeId = previousData["ProgramTypeId"] as? Int {
            typeIdPro = "\(typeId)"
        }
        
//blabla
        NetworkManager.performRequest(type:.get, method: "academy/GetCoachListByAcademyId/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(typeIdPro)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            self.collectionData  = object as! [[String:AnyObject]]
            let isPuttingLab1 = DataManager.sharedInstance.detectIsPuttingLab(key: "isPuttingLab")
            if isPuttingLab1 == "true" {
                self.filteringArrayForPuttingLabCoach()
            }else{
            }
            self.collectionView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    //MARK:- UICOllectionview delegate and datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseCoachViewCell", for: indexPath) as! ChooseCoachViewCell
        var index = indexPath.row
        while (index >= 0) {
            index -= 4
        }
        
        if (index == -2 || index == -3) {
            cell.collectionView.backgroundColor = arrayOfColors[1]
        } else {
            cell.collectionView.backgroundColor = arrayOfColors[0]
        }

        if let name = collectionData[indexPath.row]["CoachName"] as? String{
               cell.collectionTitle.text = name
        }
      
        if let url = collectionData[indexPath.row]["PhotoUrl"] as? String{
            var originalUrl = url.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            
            originalUrl = imageBaseUrl + originalUrl
            cell.collectionImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in

                DispatchQueue.main.async (execute: {
                    //                    self.activityIndicator.stopAnimating();
                    if let _ = image{
                        cell.collectionImage.image = image;
                    }
                    else{
                        cell.collectionImage.image = UIImage(named:"photoNot")
                    }
                });
                
            })
            
        }
        
        return cell
    }

    // MARK: - UICollectionViewFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return  CGSize(width:self.view.bounds.width/2+5 , height:self.view.bounds.width/2+5)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, -5, 0, -5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return  0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "DateTimeViewController") as! DateTimeViewController
                 let dic = collectionData[indexPath.row]
                controller.selectedCoachData = dic
                controller.previousData = previousData
                controller.selectedData = selectedData
        if  isRedeemedVoucher{
            controller.redeemedData = redeemedData
            controller.isRedeemedVoucher = true
        }
                self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- buttons actions
    @IBAction func voucherBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func redeemBtnAction(_ sender: UIButton) {
    }

    func gettingGDPStatus()  {

        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerGDPR/\(userId)", parameter:nil, view: UIApplication.getTopestViewController()!.view, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [String:AnyObject]:
                break
            default:
                break
            }
            Utility.GDPRstatusDic = object as! [String : AnyObject]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }


}
