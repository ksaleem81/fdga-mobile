//
//  PastViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/9/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class NoteViewController: UIViewController,UITextViewDelegate{

  
    @IBOutlet weak var textView: UITextView!

  var previousData = [String:AnyObject]()
    @IBOutlet weak var saveBtn: UIButton!
    var classPlayerData = [[String:AnyObject]]()
      var studentTrackData = [[String:AnyObject]]()
    @IBOutlet weak var choosLbl: UILabel!
    @IBOutlet weak var chooseViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var choosBtn: UIButton!
    var selecteStduentId = "0"
    @IBOutlet weak var containerViewHeightConst: NSLayoutConstraint!
    var fromPlayerMedia = false
    var fromPlayerMediaEditting = false
    @IBOutlet weak var typeViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var typeBtn: UIButton!
    var jsonStringForSkillsTypes = ""
    var jsonStringForCetegory = ""
    var jsonStringForCetegoryID = ""
    var filterCategoryTrack = [[String:AnyObject]]()
    var dataArray = [[String:AnyObject]]()
    @IBOutlet weak var parentViewHeightCons: NSLayoutConstraint!

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var textViewHeightConst: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(previousData)
        if fromPlayerMedia {
            
        }else{
            getPlayerNote()
        }
        containerViewHeightConst.constant = 0
        typeViewHeightCons.constant = 0
        parentViewHeightCons.constant = 300
        print(previousData)
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
                if currentUserLogin == 4 {
                }else{
                    getClassPlayer()
                    chooseViewHeightConstraint.constant = 40

                }
            }else{
                chooseViewHeightConstraint.constant = 0
            }
        
        }
        
        if currentUserLogin == 4 {
            if fromPlayerMedia {
                chooseViewHeightConstraint.constant = 0
                containerViewHeightConst.constant = 0
                if fromPlayerMediaEditting {
                    typeViewHeightCons.constant = 0
                    self.saveBtn.setTitle("Edit", for: .normal)
                    self.cancelBtn.setTitle("Delete", for: .normal)
                    assigningEditedData()

                }else{
                    typeViewHeightCons.constant = 100
                     getSkillCategories()
                    settingPlaceHolderForTextView()
                }

                self.parentViewHeightCons.constant = 600
                self.typeBtn.setTitle("Miscellaneous", for: .normal)


            }else{
                self.textViewHeightConst.constant = self.view.frame.height - 200
                self.parentViewHeightCons.constant = self.view.frame.height


            self.textView.isUserInteractionEnabled = true
            saveBtn.isHidden = true
            chooseViewHeightConstraint.constant = 0
            }
        }else{
            if fromPlayerMedia {
                chooseViewHeightConstraint.constant = 0
                containerViewHeightConst.constant = 0
                if fromPlayerMediaEditting {
                    typeViewHeightCons.constant = 0
                    self.saveBtn.isHidden = true
                    self.cancelBtn.isHidden = true
                    assigningEditedData()
                }
                
                self.parentViewHeightCons.constant = 600
               
                
                
            }else{
                self.textViewHeightConst.constant = self.view.frame.height - 200
                self.parentViewHeightCons.constant = self.view.frame.height


            }
        }

        settingRighMenuBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    func assigningEditedData()  {
        
        if let id = previousData["Description"] as? String{
            textView.text = "\(id)"
        }
    
    }

    let placeholderLabel = UILabel()
    func settingPlaceHolderForTextView()  {
        
        textView.delegate = self
        placeholderLabel.text = "Add Note"
        placeholderLabel.font = textView.font
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = textView.textColor
        placeholderLabel.isHidden = !textView.text.isEmpty
        
    }
    func textViewDidChange(_ textView: UITextView) {
        
        placeholderLabel.isHidden = !textView.text.isEmpty
        
    }

    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(NoteViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addBtnAction(_ sender: UIButton) {
     
        if textView.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
        
        if currentUserLogin == 4 {
            if fromPlayerMedia {
                savePlayerNote()

            }
        }else{
            saveData()

        }
    }
    @IBAction func typeBtnAction(_ sender: UIButton) {
        settingPickerView()
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        if fromPlayerMediaEditting{
            deleteMedia()
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func getClassPlayerNotes()  {
           //https://app.glflocker.com/OrbisAppBeta/api/academy/GetClassPlayerNotes/2/8758/4689
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        var typeId = ""
        
        if let type = previousData["ProgramType"] as? String {
            typeId = type
        }
        NetworkManager.performRequest(type:.get, method: "academy/GetClassPlayerNotes/\(typeId)/\(lessonID)/\(selecteStduentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
                
            default:
                break
        }
            let data = object as? String
            self.textView.text = data
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func getClassPlayer()  {
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }

        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            self.classPlayerData = (object as? [[String:AnyObject]])!
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

   
    func getPlayerNote()  {
        
        var studentId = "0"
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        var typeId = ""
        
        if let type = previousData["ProgramType"] as? String {
           typeId = type
            
            if currentUserLogin == 4 {
                
            print(previousData)
                if type == "2" {
//                    if let selectedTyp = self.previousData["StudentID"] as? Int{
//                        studentId = "\(selectedTyp)"
//                    }
                    
                    
                    if let id = DataManager.sharedInstance.currentUser()?["playerId"] as? Int{
                        studentId = "\(id)"
                    }
                }
            }
        }
        
        //https://app.glflocker.com/OrbisAppBeta/api/academy/GetPlayerNotes/2/8758/4689
        NetworkManager.performRequest(type:.get, method: "academy/GetPlayerNotes/\(typeId)/\(lessonID)/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
                
            default:
       
                break
            }
            
            let data = object as? String
            self.textView.text = data
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    func saveData() {
  
         var lessonType = ""
        if let type = previousData["ProgramType"] as? String {
           lessonType = type
        }
   
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        
        NetworkManager.performRequest(type:.post, method: "Academy/UpdateLessonNotes", parameter:["Notes":"\(textView.text!)" as AnyObject,"LessonType":lessonType as AnyObject!,"StudentID":selecteStduentId as AnyObject!,"LessonID":lessonID as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
            break
            }
            
            let responce = "\(object!)"
            if responce == "0"{
               DataManager.sharedInstance.printAlertMessage(message:"Some error occured while updating notes.\n Please try again later.", view:UIApplication.getTopestViewController()!)
            }else{
            _ = self.navigationController?.popViewController(animated: true)
                DataManager.sharedInstance.printAlertMessage(message:"Notes Updated Successfully", view:UIApplication.getTopestViewController()!)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

        
    }
 
    
    func savePlayerNote() {
        
        
        var playerId = ""
        if let id = DataManager.sharedInstance.currentUser()?["Userid"] as? Int{
            playerId = "\(id)"
        }
        
        var parameters : [String:String] = [:]
        
        if fromPlayerMediaEditting {
            
            if let userId =  previousData["SkillTypes"]! as? String{
                jsonStringForSkillsTypes = "\(userId)"
            }
            var cat = ""
            if let categry =  previousData["SkillCategory"]! as? String{
                cat = categry
            }
            var idMedia = ""
            if let categry =  previousData["MediaId"]! as? Int{
                idMedia = "\(categry)"
            }
            
           
            parameters = ["MediaId":idMedia,"UserId":playerId,"MediaTypeId":"4","Description":self.textView.text!,"SkillTypes":jsonStringForSkillsTypes,"SkillCategory":cat]
            
        }else{
                    parameters = ["MediaId":"0","UserId":playerId,"MediaTypeId":"4","Description":self.textView.text!,"SkillTypes":jsonStringForSkillsTypes,"SkillCategory":(self.typeBtn.titleLabel?.text!)!]//,"SkillCategoryID":jsonStringForCetegoryID]
        }

        
      print(parameters)
        
        NetworkManager.performRequest(type:.post, method: "MediaSection/UpdateMedia", parameter:parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            let responce = "\(object!)"
            if responce == "0"{
                DataManager.sharedInstance.printAlertMessage(message:"Some error occured while updating notes.\n Please try again later.", view:UIApplication.getTopestViewController()!)
            }else{
                _ = self.navigationController?.popViewController(animated: true)
                DataManager.sharedInstance.printAlertMessage(message:"Notes Updated Successfully", view:UIApplication.getTopestViewController()!)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
        
    }
    
    @IBAction func choosBtnAction(_ sender: UIButton) {
        
        var dataAray = [String]()
        studentTrackData = [[String:AnyObject]]()
        dataAray.append("ALL")
        let dic = ["0":"StudentID"]
        studentTrackData.append(dic as [String : AnyObject])
        for dic in classPlayerData{
            if let val = dic["StudentName"] as? String{
                studentTrackData.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT PLAYER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.choosLbl.text = "\(indexes!)"
            if let selectedTyp = self.studentTrackData[values]["StudentID"] as? Int{
                self.selecteStduentId = "\(selectedTyp)"
            }
            
            if "\(indexes)" == "ALL"{
                self.selecteStduentId = "0"
            }
            self.getClassPlayerNotes()
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }




func getSkillCategories()  {
    
    var acadmyId = ""
    if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
        acadmyId = "\(id)"
    }
    
    NetworkManager.performRequest(type:.get, method: "Academy/GetSkillCategoriesAndSkills/\(acadmyId)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
        
        print(object)
        switch object {
        case _ as NSNull:
            print("figured")
            DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            return
            
        // do one thing
        case _ as [[String:AnyObject]]: break
            
        default:
            
            break
            
            
        }
        self.dataArray =  object as! [[String : AnyObject]]
        
    }) { (error) in
        print(error!)
      
        self.showInternetError(error: error!)
    }
}
    
    func settingPickerView() {
        
        var dataAray = [String]()
        filterCategoryTrack = [[String:AnyObject]]()
        for dic in dataArray{
            if let val = dic["SkillCategoryName"] as? String{
                
                filterCategoryTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT FILTER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            
            self.typeBtn.setTitle("\(indexes!)", for: .normal)
            self.jsonStringForCetegory = "\(indexes!)"
            //getting id
         /*   let k = self.filterCategoryTrack
            let index = k.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic["SkillCategoryName"]  as? String, value == "\(indexes!)"{
                        
                        if let id = dic["SkillCategoryID"] as? Int{
                         self.jsonStringForCetegoryID = "\(id)"
                        }
                        
                        return true
                    }
                }
                return false
            }*/

            
            if let skillsArray = self.filterCategoryTrack[values]["Skills"] as? [[String:AnyObject]]{
                if skillsArray.count > 0{
                    self.containerViewHeightConst.constant = 200
                    let tv : FilteredOptionViewController = self.childViewControllers[0] as! FilteredOptionViewController
                    tv.dataArray = skillsArray
                    tv.delegate = self
                    tv.viewWillAppear(true)
                    
                }else{
                    self.containerViewHeightConst.constant = 0
                    
                }
            }
            
            
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: typeBtn)
        
    }
    
    func deleteMedia()  {
        var mediaId = ""
        if let userId =  previousData["MediaId"]! as? Int{
            mediaId = "\(userId)"
        }
        
        NetworkManager.performRequest(type:.post, method: "MediaSection/DeleteMediaItem", parameter:["MediaId":mediaId as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
            
            _ = self.navigationController?.popViewController(animated: true)
            DataManager.sharedInstance.printAlertMessage(message:"Successfully Deleted Media", view:UIApplication.getTopestViewController()!)
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

}


extension NoteViewController : SelectedSkillsDelegate{
    func seletedSkillsData(selectedData: [[String:AnyObject]], selectedUserId: String) {
        print("yes")
        
        print(selectedData)
        jsonStringForSkillsTypes = ""
        
        for index in selectedData {
            
            if let value  =  index["SkillName"] as? String {
                jsonStringForSkillsTypes = jsonStringForSkillsTypes +  value + ","
            }
        }
        
        if jsonStringForSkillsTypes != "" {
            jsonStringForSkillsTypes = jsonStringForSkillsTypes.substring(to: jsonStringForSkillsTypes.index(before: jsonStringForSkillsTypes.endIndex))
        }
        
        print(jsonStringForSkillsTypes)
        
    }
}




