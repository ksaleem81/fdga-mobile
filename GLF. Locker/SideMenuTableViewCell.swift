//
//  SideMenuTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 3/24/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
