//
//  DrillViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/20/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage
import SideMenu

class DrillViewController: UIViewController {

        var dataDictionary = [String:AnyObject]()
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var discriptionLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playBtn.isHidden = true
        activityIndicator.startAnimating()
        if let url = dataDictionary["Description"] as? String {
            discriptionLbl.text = url
        }
        var fileUrl = ""
        if let url = dataDictionary["ThumbnilURL"] as? String {
            fileUrl = url
        }
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
        fileUrl = baseUrlForVideo + fileUrl
        
        self.bgImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
            
            DispatchQueue.main.async (execute: {
                self.activityIndicator.isHidden = true
                     self.playBtn.isHidden = false
                if let _ = image{
                    self.bgImage.image = image;
                }
                else{
                    self.bgImage.image = UIImage(named:"photoNot")
                }
            });
            
        })
//        settingRighMenuBtn()
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(DrillViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playBtnAction(_ sender: UIButton) {
      
        var fileUrl = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl = url
        }
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
        fileUrl = baseUrlForVideo + fileUrl
        
        let destination = AVPlayerViewController()
        let url = NSURL(string: fileUrl)!
        destination.player = AVPlayer(url: url as URL)
        self.present(destination, animated: true) {
            destination.player?.play()
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(DrillViewController.playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "playDrilVideo" {
            
            let destination = segue.destination as! AVPlayerViewController
            var fileUrl = ""
            if let url = dataDictionary["FileURL"] as? String {
                fileUrl = url
            }
            fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            fileUrl = baseUrlForVideo + fileUrl
            let url = NSURL(string: fileUrl)!
            destination.player = AVPlayer(url: url as URL)
            destination.player?.play()
            
            NotificationCenter.default.addObserver(self, selector:#selector(DrillViewController.playerDidFinishPlaying),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
        }
    }

    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        self.dismiss(animated: true, completion:nil)
    }

}
