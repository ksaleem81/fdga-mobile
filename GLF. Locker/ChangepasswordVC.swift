//
//  ChangepasswordVC.swift
//  GLF. Locker
//
//  Created by Kashif Jawad on 22/06/2018.
//  Copyright © 2018 Nasir Mehmood. All rights reserved.
//

import UIKit

class ChangepasswordVC: UIViewController {

    var blureView = UIView()
    
    @IBOutlet weak var oldPasswordFld: UITextField!
    @IBOutlet weak var newPasswordFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changePassword()  {
        
        var userID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
        }
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        let params = ["Userid":userID,"NewPassword":newPasswordFld.text!,"Password":oldPasswordFld.text!,"AcademyId":acadmyId]
        
        print(params)
        NetworkManager.performRequest(type:.post, method: "Login/ChangePassword", parameter:params as [String : AnyObject]?, view: UIApplication.getTopestViewController()?.view, onSuccess: { (object) in
            print(object!)
            
            switch object {
                
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }

            if  let flage = object as? Bool {
            if  flage {
            let alertController = UIAlertController(title: "Alert", message: "Successfully Password Changed", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        DispatchQueue.main.async(execute: {
                            
                            let currentUserClone =  DataManager.sharedInstance.currentUser()?.mutableCopy() as? NSMutableDictionary
                            currentUserClone?.setValue(true, forKey: "IsPasswordChanged")
                            currentUserClone?.setValue(self.newPasswordFld.text!, forKey: "userActualPassword")
                            DataManager.sharedInstance.updateLoginDetails(currentUserClone)
                            Utility.savingToAllAccounts()
                            self.dismiss(animated: true, completion:nil)
                            self.blureView.removeFromSuperview()
                        }
                        )
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    DataManager.sharedInstance.printAlertMessage(message: "Unable to change password", view: UIApplication.getTopestViewController()!)
                }
                }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    var messageString = ""
    
    @IBAction func passwordChangBtnAction(_ sender: UIButton) {
        
         messageString = ""
        
        var userPassword = ""
    
        if let userId =   DataManager.sharedInstance.currentUser()!["userActualPassword"]! as? String{
            userPassword = "\(userId)"
        }
        
        if  userPassword != oldPasswordFld.text! {
            DataManager.sharedInstance.printAlertMessage(message: "Old password not match to actual password", view: UIApplication.getTopestViewController()!)
            return
        }
        
        
        if  newPasswordFld.text! != newPasswordFld.text! {
            DataManager.sharedInstance.printAlertMessage(message: "Old password can't be new password!", view: UIApplication.getTopestViewController()!)
            return
        }
        
        messageString = Utility.passwordStrength(passwordString: newPasswordFld.text!, messageString1: messageString)
        if messageString.count > 0 {
            DataManager.sharedInstance.printAlertMessage(message: messageString, view: UIApplication.getTopestViewController()!)
            return
        }
        
        changePassword()
    }
    

}
