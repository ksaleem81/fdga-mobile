//
//  BookLessonVC.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/4/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SDWebImage
import SideMenu
class ChooseDurationVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var collectionData:[[String:AnyObject]] = [[String:AnyObject]]()
    var selectedData = [String:AnyObject]()
    let arrayOfColors = [UIColor.init(red: 31/255, green: 160/255, blue: 133/255, alpha: 1.0),UIColor.init(red: 44/255, green: 169/255, blue: 145/255, alpha: 1.0),UIColor.init(red: 31/255, green: 160/255, blue: 133/255, alpha: 1.0)]
    var vochersArray = [[String:AnyObject]]()
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    var delegatedVocherDic = [String:AnyObject]()
    var redeemedData = [String:AnyObject]()
    var programsData = [[String:AnyObject]]()
    @IBOutlet weak var voucherBtn: UIButton!
   var isRedeemedVoucher = false
    @IBOutlet weak var selectTitle: UILabel!
    var isVocherSelected = false

    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()
      
       self.navigationItem.title = "BOOK A LESSON"
       refreshCollectionView()
       footerViewHeightConstraint.constant = 0
       settingRighMenuBtn()
       if #available(iOS 11, *) {
       self.collectionView.contentInset = .init(top: 50, left: 0, bottom: 0, right: 0)
       }else{
       }
        //mergingcode
        self.selectTitle.text = "Select"

        if isNewUrl{
            self.voucherBtn.setTitle("Select Package", for: .normal)
        }else{
            self.voucherBtn.setTitle("Select Voucher", for: .normal)
        }

    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(ChooseDurationVC.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        let bound = scrollView.bounds
        let height = scrollView.contentSize.height
        let offset = scrollView.contentOffset
        let inset = scrollView.contentInset
        let y = offset.y + bound.size.height - inset.bottom
       
        if y > ( height + 0.0 ) {
            print(y)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let bound =  scrollView.bounds
        let height = scrollView.contentSize.height
        let offset = scrollView.contentOffset
        let inset =  scrollView.contentInset
        let y = offset.y + bound.size.height - inset.bottom
        
        if y > height - 50 {
            hideShowFooter(flage: false)
        }else{
            hideShowFooter(flage: true)
        }
        
    }
    
    func hideShowFooter(flage:Bool)  {
        
        return
        if flage {
            if currentUserLogin == 4  {
                UIView.animate(withDuration: 0.5, animations: {
                    self.footerViewHeightConstraint.constant = 0 // heightCon is the IBOutlet to the constraint
                    self.view.layoutIfNeeded()
                })
            }
        }else{
            
            if currentUserLogin == 4  {
                //mergingcode
                if isNewUrl{
                    if let redeemable = DataManager.sharedInstance.currentAcademy()?["isPackagesRedemable"] as? Bool,redeemable == true {
                        
                        UIView.animate(withDuration: 0.5, animations: {
                            self.footerViewHeightConstraint.constant = 100
                            self.view.layoutIfNeeded()
                        })
                        
                    }
                    
                }else{
                    UIView.animate(withDuration: 0.5, animations: {
                        self.footerViewHeightConstraint.constant = 100 
                        self.view.layoutIfNeeded()
                    })
                }
            }
            
        }
    }
    
    func refreshCollectionView()  {
       
        var programType = ""
        if isRedeemedVoucher {
            
            if let id = redeemedData["ProgramTypeId"] as? Int{
                programType = "\(id)"
            }
        }else{
        if let id = selectedData["ProgramTypeId"] as? Int{
            programType = "\(id)"
            }
        }

        NetworkManager.performRequest(type:.get, method: "academy/GetProgramTypeFilters/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(programType)/\(currentUserLogin!)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            self.collectionData  = self.convertToArray(text: object as! String)!
            if self.collectionData.count < 1{
                  self.footerViewHeightConstraint.constant = 0
            }else if self.collectionData.count < 5{
                self.hideShowFooter(flage: false)
            }
            
            if self.collectionData.count > 0 && self.collectionData.count < 5{
                self.hideShowFooter(flage: false)
            }
            self.collectionView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func convertToArray(text: String) -> [[String: AnyObject]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    //MARK:- UICOllectionview delegate and datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseDurationViewCell", for: indexPath) as! ChooseDurationViewCell
        
        var index = indexPath.row
        while (index >= 0) {
            index -= 4
        }
        
        if (index == -2 || index == -3) {
            cell.collectionView.backgroundColor = arrayOfColors[1]
        } else {
            cell.collectionView.backgroundColor = arrayOfColors[0]
        }
        
        if let title = collectionData[indexPath.row]["Name"] as? String{
            var actualVal  = title.components(separatedBy: " ")
            if actualVal.count > 0 {
                cell.timeLbl.text = actualVal[0]
               }
        }
                cell.collectionTitle.text = "MINS"
        
        return cell
    }

    
    // MARK: - UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return  CGSize(width:self.view.bounds.width/2+5 , height:self.view.bounds.width/2+5)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, -5, 0, -5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return  0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func isVoucherRedemable() -> Bool {
        
        if let redeemStatus = delegatedVocherDic["IsRedeemAble"] as? Bool ,redeemStatus == true{
            
            return true
        }else{
            
            return false
        }
        
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if currentUserLogin == 4 {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCoachVC") as! ChooseCoachVC
            let dic = collectionData[indexPath.row]
            controller.selectedData = dic
            if  isRedeemedVoucher{
                controller.isRedeemedVoucher = true
                controller.redeemedData = redeemedData
            }
            controller.previousData = selectedData
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "DateTimeViewController") as! DateTimeViewController
            let dic = collectionData[indexPath.row]
            controller.selectedData = dic
            if  isRedeemedVoucher{
                controller.isRedeemedVoucher = true
            }
            controller.previousData = selectedData
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    //MARK:-  buttons Actions
    @IBAction func voucherBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VoucherViewController") as! VoucherViewController
        controller.originaldata = vochersArray
        controller.delegate = self
        //mergingcode
        if isNewUrl{
            isVocherSelected = false
            controller.forPackage = true
        }else{
            isVocherSelected = true
            controller.forPackage = false
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func redeemBtnAction(_ sender: UIButton) {
        if delegatedVocherDic.count > 0 {
            
            //mergingcode
            if isNewUrl{
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
                controller.selectedData = delegatedVocherDic//selectedData
                controller.redeemedData = delegatedVocherDic
                controller.isRedeemedVoucher = true
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }

            if isVoucherRedemable(){
                                
            }else{
                
                let alertController = UIAlertController(title: "Alert", message: "Selected Voucher is not Redeemable.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            var isOneToOne = false
            var selectedDataThis = [String:AnyObject]()
            for index in  programsData {
                
                if let selectedVocherTyp = delegatedVocherDic["ProgramTypeId"] as? Int{
                    
                    if selectedVocherTyp == index["ProgramTypeId"] as? Int {
                        
                        selectedDataThis = index
                        if let isOneToone =  index["IsOneToOneLesson"] as? Bool{
                            
                            if isOneToone {
                                isOneToOne = true
                                break
                            }
                        }
                        
                    }
                }
            }
            
            if isOneToOne {
                if let durations = delegatedVocherDic["Details"] as? String {
                    
                    let components = durations.components(separatedBy: ")")
                    print(components)
                    if components.count > 2 {
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
                        controller.selectedData = selectedDataThis
                        controller.redeemedData = delegatedVocherDic
                        controller.isRedeemedVoucher = true
                        self.navigationController?.pushViewController(controller, animated: true)
                    }else{
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCoachVC") as! ChooseCoachVC
                        controller.selectedData = selectedDataThis
                        controller.redeemedData = delegatedVocherDic
                        controller.previousData = selectedDataThis
                        controller.isRedeemedVoucher = true
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                    
                }
                
            }else{
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseProgramTypeVC") as! ChooseProgramTypeVC
                controller.selectedData = selectedDataThis
                controller.redeemedData = delegatedVocherDic
                controller.isRedeemedVoucher = true
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
        }else{
            let alertController = UIAlertController(title: "Alert", message: "Please Select Vocher First!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            
            alertController.addAction(okAction)
            
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    

}
//MARK:- delegate methods
extension ChooseDurationVC : VoucherDelegate{
    func seletedVoucherData(dic: NSDictionary) {
        print(dic)
        
        if let pin = dic["VoucherPin"] as? String{
            self.voucherBtn.setTitle(pin, for: .normal)
        }
        
        
        if isVocherSelected{
        }else{
            
            if let pin = dic["Pin"] as? String{
                self.voucherBtn.setTitle(pin, for: .normal)
            }
        }
         delegatedVocherDic = dic as! [String : AnyObject]
        
    }
    func seletedPackageData(dic: NSDictionary) {
    }
}
