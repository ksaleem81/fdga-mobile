//
//  UploadRequest.m
//  sibme
//
//  Created by Nasir Mehmood on 3/21/13.
//
//

#import "UploadRequest.h"
//#import "AppDelegate.h"
//#import "MainViewController.h"
//#import <Cordova/CDVPluginResult.h>
#import "GlobalClass.h"
#import "Reachability.h"
#import "OrbisMediaGallery.h"
#import "NSData+Base64.h"
#import "UploadManager.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "GLF__Locker-Swift.h"




@interface UploadRequest()
{
    AFHTTPRequestOperation *currentOperation;
    
    unsigned long long totalFileSize, bytesUploaded;
    NSUInteger chunkSize;
    int totalChunks;
    int currentChunk;
    NSFileHandle *fileHandle;
    NSString *mediaID;
    
    BOOL shouldCancelUploading;
    NSURLSession *backgroundSession;
    NSURLSessionUploadTask *uploadTask;
    NSString *  uploadVideoFolderPath;
    
//    __weak AppDelegate *appDelegate;
    __weak GlobalClass *appDelegate;
    NSString *responseFileName;
    UploadRequestStatus requestStatus;
    NSUserDefaults *defObj ;
    NSMutableArray *uploadOverlayA ;
    BOOL isOverlayUploaded;
    int uploadingVal;
    NSString  *overlaysSt;
    NSArray *overlays;
    NSString* cleanedString ;
    BOOL videoUploaded;
    NSArray *pathArray;
}

@end

@implementation UploadRequest

@synthesize requestOptions, chunkSize, isUploading, shouldUpload, uploadRequestDelegate;

- (id) initWithOptions:(NSDictionary*) options uploadRequestDelegate:(id<UploadRequestDelgate>) delegate{
    
    self=[super init];
    if(self) {
        videoUploaded = NO;
        overlaysSt = @"";
        uploadingVal = 0;
        isOverlayUploaded = NO;
        uploadOverlayA = [[NSMutableArray alloc]init];
        defObj = [NSUserDefaults standardUserDefaults];
        self.requestOptions=[NSMutableDictionary dictionaryWithDictionary:options];
//        appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
        appDelegate = [GlobalClass sharedInstance];
        
        NSString *fileName=[[requestOptions objectForKey:@"mediaPath"] lastPathComponent];
        NSString *pathExtension=fileName.pathExtension;
        NSString *filePath=[requestOptions objectForKey:@"mediaPath"];
        if([pathExtension caseInsensitiveCompare:@"png"]!=NSOrderedSame && [pathExtension caseInsensitiveCompare:@"jpg"]!=NSOrderedSame && [pathExtension caseInsensitiveCompare:@"JPEG"]!=NSOrderedSame && [pathExtension caseInsensitiveCompare:@"mov"]!=NSOrderedSame)
        {
            filePath=[[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath] stringByAppendingPathComponent:fileName];
        }
        
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        
        if(fileAttributes != nil) {
            NSString *fileSize = [fileAttributes objectForKey:@"NSFileSize"];
            totalFileSize = [fileSize longLongValue];
        }
        
        chunkSize=kChunkSize;
        currentChunk=0;
        bytesUploaded=0;
        
        if(totalFileSize<chunkSize) {
            totalChunks=1;
            chunkSize=totalFileSize;
        }
        else {
            totalChunks=ceil((double)(totalFileSize)/(double)chunkSize);
        }
        
        self.uploadRequestDelegate=delegate;
        fileHandle=[NSFileHandle fileHandleForReadingAtPath:filePath];
        isUploading=NO;
        shouldUpload=YES;
        currentOperation=nil;
        
        [requestOptions setObject:[NSNumber numberWithUnsignedLongLong:totalFileSize] forKey:@"totalBytes"];
        
        NSString *uniqueMediaID=[requestOptions objectForKey:@"uniqueMediaID"];
        if(!uniqueMediaID)
        {
            uniqueMediaID=[NSString stringWithFormat:@"m%d", (int)[NSDate timeIntervalSinceReferenceDate]];
            [requestOptions setObject:uniqueMediaID forKey:@"uniqueMediaID"];
        }
        
        [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"\n\nUploadRequest options: %@", requestOptions]];
        mediaID=nil;
        
        shouldCancelUploading=NO;
        uploadTask=nil;
        //        [self backgroundSession];
        [self getExistingUploadTask];
        
        NSString *imageURL = @"";
        NSString *fileURL = @"";
        NSNumber *duration = @(0);
        if(![requestOptions objectForKey:@"duration"])
        {
            NSLog(@"%@",[requestOptions objectForKey:@"duration"]);
            [requestOptions setObject:duration forKey:@"duration"];
        }
        
        [requestOptions setObject:fileURL forKey:@"fileURL"];
        [requestOptions setObject:imageURL forKey:@"imageURL"];
        
        responseFileName=[NSString stringWithFormat:@"%@_response.txt", [[[requestOptions objectForKey:@"mediaPath"] lastPathComponent] stringByDeletingPathExtension]];
        
        NSNumber *requestStatusObject=[requestOptions objectForKey:kUploadRequestStatusKey];
        if(!requestStatusObject)
        {
            [requestOptions setObject:@(UploadRequestStatusNotStarted) forKey:kUploadRequestStatusKey];
        }
        
        requestStatus=[[requestOptions objectForKey:kUploadRequestStatusKey] integerValue];
    }
    return self;
}

- (NSURLSession*) backgroundSession{
    //   NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfiguration:[NSString stringWithFormat:@"com.glflocker.orbis.%@", [self uniqueMediaID]]];
    
    //   backgroundSession= [NSURLSession sessionWithConfiguration:config delegate:appDelegate.backgroundSessionDelegate delegateQueue:nil ];
    backgroundSession=[appDelegate createBackgroundSessionWithID:[NSString stringWithFormat:@"com.glflocker.orbis.%@", [self uniqueMediaID]]];
    return backgroundSession;
}

- (void) getExistingUploadTask
{
    [[self backgroundSession] getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        uploadTask=uploadTasks.firstObject;
    }];
}

- (NSString*)uniqueMediaID
{
    return [requestOptions objectForKey:@"uniqueMediaID"];
}

- (void) sendCurrentChunkToServer
{
    if(shouldUpload)
    {
        //        unsigned long long position=chunkSize*currentChunk;
        unsigned long long position=bytesUploaded;
        [fileHandle seekToFileOffset:position];
        if((position+chunkSize)>totalFileSize)
            chunkSize=totalFileSize-position;
        NSData *chunkData=[fileHandle readDataOfLength:chunkSize];
        NSString *encodedDataString=[chunkData base64EncodedString];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *parameters = @{
                                     @"fileString": encodedDataString,
                                     @"beginByte": [NSNumber numberWithUnsignedLongLong:bytesUploaded],
                                     @"EndByte": [NSNumber numberWithUnsignedLongLong:totalFileSize],
                                     @"FileName": [[requestOptions objectForKey:@"mediaPath"] lastPathComponent],
                                     @"MediaType": [requestOptions objectForKey:@"mediaTypeId"]
                                     };
        
        [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\nsendCurrentChunkToServer parameters: %@", parameters]];
        [GlobalClass writeAndSaveLogString];
        

        //mergingcode
//        DataManager *dataManger = [DataManager sharedInstance];
//        NSDictionary *dic = dataManger.currentAcademy;
        NSDictionary *dic = GlobalClass.sharedInstance.academyData;

        NSString *urlToHit = @"";

        BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
        if ( isNew == NO){
            urlToHit = kMediaUploadingURLOld;
        }else{
            urlToHit = kMediaUploadingURL;
        }
        
        currentOperation=[manager POST:urlToHit parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject){
            NSLog(@"JSON: %@", responseObject);
            
            [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\nsendCurrentChunkToServer success Response: %@", responseObject]];
            [GlobalClass writeAndSaveLogString];
            bytesUploaded=bytesUploaded+chunkSize;
            [self chunkUploadedWithResponse:responseObject];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\nsendCurrentChunkToServer error: %@", error]];
            [GlobalClass writeAndSaveLogString];
            [self uploadFailed];
        }];
        
    }
    else
    {
        isUploading=NO;
    }
}


- (void) chunkUploadedWithResponse:(id)response
{
    if(response)
    {
        currentChunk++;
        if(currentChunk<totalChunks)
        {
            [self updateProgress];
            [self sendCurrentChunkToServer];
        }
        else
        {
            isUploading=NO;
            [self mediaUploadingCompletedWithResponse:response];
        }
    }
    else
    {
        shouldUpload=NO;
        isUploading=NO;
        [self cancelUploadOperation];
        [self uploadFailed];
    }
    
}

- (void) mediaUploadingCompletedWithResponse:(id)response
{
    [self updateProgress];
    
    currentOperation=nil;
    
    NSArray *responseArray=response;
    
    NSString *fileURL = @"";
    NSString *imageURL = @"";
    NSNumber *duration = @(0);
    
    if (responseArray.count >= 2 && [responseArray objectAtIndex:1]!=[NSNull null])
        fileURL = [responseArray objectAtIndex:1];
    if (responseArray.count >= 3 && [responseArray objectAtIndex:2]!=[NSNull null])
        imageURL = [responseArray objectAtIndex:2];
    if (responseArray.count >= 4 && [responseArray objectAtIndex:3]!=[NSNull null])
        duration = @(ceil([[responseArray objectAtIndex:3] floatValue]));
    
    if(![requestOptions objectForKey:@"duration"])
    {
        [requestOptions setObject:duration forKey:@"duration"];
    }
    
    [requestOptions setObject:fileURL forKey:@"fileURL"];
    [requestOptions setObject:imageURL forKey:@"imageURL"];
    if ([[requestOptions objectForKey:@"mediaTypeId"] integerValue] == kMediaTypePhoto) {
        [requestOptions setObject:fileURL forKey:@"imageURL"];
    }
    
    [self addMediaToLesson];
}

- (void) addMediaToLesson
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *lessonType=@"Lesson";
    if([[requestOptions objectForKey:@"lessonType"] intValue]==kLessonTypeClass)
    {
        lessonType=@"Class";
    }
    
    NSDictionary *parameters = @{
                                 @"MediaTypeId": [requestOptions objectForKey:@"mediaTypeId"],
                                 @"AcademyID": [requestOptions objectForKey:@"academyID"],
                                 @"FileURL": [requestOptions objectForKey:@"fileURL"],
                                 @"CreatedBy": [requestOptions objectForKey:@"createdBy"],
                                 @"Description": [requestOptions objectForKey:@"description"],
                                 @"ImageURL": [requestOptions objectForKey:@"imageURL"],
                                 @"LessonId": [requestOptions objectForKey:@"lessonID"],
                                 @"ModifiedBy": [requestOptions objectForKey:@"modifiedBy"],
                                 @"Duration": [requestOptions objectForKey:@"duration"],
                                 @"MediaId":@(0),
                                 @"DrillCount":[requestOptions objectForKey:@"numPracticeDrill"],
                                 @"SkillCatetoryID":[requestOptions objectForKey:@"SkillCatetoryID"],
                                 @"SkillTypeId":@"",
                                 @"ProgramTypeID":@(1),
                                 @"LessonType": lessonType
                                 };
    
    [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\naddMediaToLesson Parameters: %@", parameters]];
    [GlobalClass writeAndSaveLogString];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    //mergingcode
//    DataManager *dataManger = [DataManager sharedInstance];
//    NSDictionary *dic = dataManger.currentAcademy;
    NSDictionary *dic = GlobalClass.sharedInstance.academyData;

    NSString *urlToHit = @"";
    
    BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
    if ( isNew == NO){
        urlToHit = kAddToLessonURLOld;
    }else{
        urlToHit = kAddToLessonURL;
    }
    
    
    currentOperation=[manager POST:urlToHit parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"response: %@", string);
        mediaID=string;
        
        [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\naddMediaToLesson success Response: %@", string]];
        [GlobalClass writeAndSaveLogString];
        
        if ([[requestOptions objectForKey:@"mediaTypeId"] integerValue] == kMediaTypePhoto) {
            [self uploadCompleted];
        }
        else {
            [self checkForVideoStatus];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Response String: %@", operation.responseString);
        NSLog(@"Error: %@", error);
        
        [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\naddMediaToLesson error Response: %@", operation.responseString]];
        [GlobalClass appendLogString:[NSString stringWithFormat:@"\n\naddMediaToLesson error: %@", error]];
        [GlobalClass writeAndSaveLogString];
        
        [self cancelUploadOperation];
        [self uploadFailed];
    }];
    
}


- (void) startRequest
{
    if(currentChunk<totalChunks)
    {
        // TODO: Callback
//        MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:requestOptions];
//        [mvc.commandDelegate sendPluginResult:result callbackId:[requestOptions objectForKey:@"callBackID"]];
        
        shouldUpload=YES;
        if(!isUploading)
        {
            isUploading=YES;
            [self sendCurrentChunkToServer];
        }
    }
}

- (void) cancelRequest
{
    shouldUpload=NO;
    shouldCancelUploading=YES;
    if(uploadTask)
    {
        [uploadTask cancel];
    }
    
    [backgroundSession invalidateAndCancel];
    //    [self cancelUploadOperation];
    [self uploadCancelled];
}

- (void) updateProgress{
    
    if(shouldUpload)
    {
        if ([[requestOptions valueForKey:[NSString stringWithFormat:@"%@-%@",[[requestOptions objectForKey:@"mediaPath"] lastPathComponent],[requestOptions valueForKey:@"mediaid"]]] isEqualToString:@"videocompleted"]) {
            
            // CHM Jawad
//            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//            [mvc.commandDelegate evalJs:
//             [NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.updateProgress(%@);",
//              [self getJSONStringFromDictionary:
//               @{
//                 @"uniqueMediaID":[requestOptions objectForKey:@"uniqueMediaID"],
//                 @"uploadedBytes":[NSNumber numberWithUnsignedLongLong:bytesUploaded],
//                 @"totalBytes":[NSNumber numberWithUnsignedLongLong:totalFileSize],
//                 @"isVideoUploaded":@"YES",
//                 @"totalOverlaysUploaded":[NSString stringWithFormat:@"%@",[requestOptions valueForKey:@"totalOverlayUploaded"]],
//                 @"totalOverlays":[NSString stringWithFormat:@"%i",overlays.count],
//                 
//                 }
//               ]
//              ]
//             ];
            NSDictionary *infoDict = @{@"uniqueMediaID":[requestOptions objectForKey:@"uniqueMediaID"],
                                       @"uploadedBytes":[NSNumber numberWithUnsignedLongLong:bytesUploaded],
                                       @"totalBytes":[NSNumber numberWithUnsignedLongLong:totalFileSize],
                                       @"isVideoUploaded":@"YES",
                                       @"totalOverlaysUploaded":[NSString stringWithFormat:@"%@",[requestOptions valueForKey:@"totalOverlayUploaded"]],
                                       @"totalOverlays":[NSString stringWithFormat:@"%i",overlays.count],};
            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"UploadProgressInfo" object:infoDict];
            UploadProgressViewController *uploadProgressVC = (UploadProgressViewController *)[[GlobalClass sharedInstance] getUploadProgressViewController];
            if (uploadProgressVC != nil)
                dispatch_async(dispatch_get_main_queue(),  ^(void){

                    [uploadProgressVC updateProgressWith:infoDict];});
        }
        else
        {
//            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//            [mvc.commandDelegate evalJs:
//             [NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.updateProgress(%@);",
//              [self getJSONStringFromDictionary:
//               @{
//                 @"uniqueMediaID":[requestOptions objectForKey:@"uniqueMediaID"],
//                 @"uploadedBytes":[NSNumber numberWithUnsignedLongLong:bytesUploaded],
//                 @"totalBytes":[NSNumber numberWithUnsignedLongLong:totalFileSize],
//                 @"isVideoUploaded":@"NO"
//                 }
//               ]
//              ]
//             ];
            NSDictionary *infoDict = @{@"uniqueMediaID":[requestOptions objectForKey:@"uniqueMediaID"],
                                       @"uploadedBytes":[NSNumber numberWithUnsignedLongLong:bytesUploaded],
                                       @"totalBytes":[NSNumber numberWithUnsignedLongLong:totalFileSize],
                                       @"isVideoUploaded":@"NO"};
            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"UploadProgressInfo" object:infoDict];
            UploadProgressViewController *uploadProgressVC = (UploadProgressViewController *)[[GlobalClass sharedInstance] getUploadProgressViewController];
            if (uploadProgressVC != nil)
                [uploadProgressVC updateProgressWith:infoDict];
        }
        
        
    }
}

- (void) cancelUploadOperation
{
    [currentOperation cancel];
    currentOperation=nil;
}

- (void) checkForVideoStatus{
    [self removeResponseFile];
    
    
    //mergingcode
//    DataManager *dataManger = [DataManager sharedInstance];
//    NSDictionary *dic = dataManger.currentAcademy;
    NSDictionary *dic = GlobalClass.sharedInstance.academyData;

    NSString *urlToHit = @"";
    
    BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
    if ( isNew == NO){
        urlToHit = kVideoStatusURLOld;
    }else{
        urlToHit = kVideoStatusURL;
    }
    
    NSString *urlString=[urlToHit stringByAppendingString:[self mediaID]];
    NSLog(@"URLString: %@", urlString);
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.cachePolicy=NSURLRequestReloadIgnoringCacheData;
    [request setTimeoutInterval:24*60*60];
    [request setHTTPMethod:@"PUT"];
    [request setValue:[self uniqueMediaID] forHTTPHeaderField:kMediaIDKey];
    [request setValue:kMethodNameCompleteAndPublishMultipart forHTTPHeaderField:kMethodNameKey];
    [request setValue:responseFileName forHTTPHeaderField:kResponseFileNameKey];
    
    NSURLSessionDownloadTask *downloadTask=[backgroundSession downloadTaskWithRequest:request];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        downloadTask.priority=1.0;
    }
    [downloadTask resume];
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     manager.responseSerializer = [AFHTTPResponseSerializer serializer];
     
     [AppDelegate appendLogString:[NSString stringWithFormat:@"\n\ncheckForVideoStatus mediaID: %@", mediaID]];
     [AppDelegate writeAndSaveLogString];
     
     
     currentOperation=[manager GET:[kVideoStatusURL stringByAppendingString:mediaID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
     NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
     NSLog(@"checkForVideoStatus-Success Response: %@", string);
     [AppDelegate appendLogString:[NSString stringWithFormat:@"\n\ncheckForVideoStatus Response: %@", string]];
     [AppDelegate writeAndSaveLogString];
     [AppDelegate resetLogString];
     
     if ([string integerValue] == 1) {
     [self uploadCompleted];
     }
     else {
     [self checkForVideoStatus];
     }
     
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
     NSLog(@"checkForVideoStatus-Error: %@", error);
     NSLog(@"checkForVideoStatus-Response String: %@", operation.responseString);
     
     [AppDelegate appendLogString:[NSString stringWithFormat:@"\n\ncheckForVideoStatus error: %@", error]];
     [AppDelegate appendLogString:[NSString stringWithFormat:@"\n\ncheckForVideoStatus reponse: %@", operation.responseString]];
     [AppDelegate writeAndSaveLogString];
     [AppDelegate resetLogString];
     
     [self uploadCompleted];
     }];
     });
     */
}

#pragma mark

- (void) uploadCompleted{
    
    isUploading=NO;
    [uploadRequestDelegate uploadRequestCompleted:self];
    // CHM Jawad
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    [mvc.commandDelegate evalJs:[NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.uploadingCompleted(%@);", [self getJSONStringFromDictionary:requestOptions]]];
    UploadProgressViewController *uploadProgressVC = (UploadProgressViewController *)[[GlobalClass sharedInstance] getUploadProgressViewController];
    if (uploadProgressVC != nil)
        [uploadProgressVC resetData];
}

- (void) uploadFailed
{
    isUploading=NO;
    [uploadRequestDelegate uploadRequestFailed:self];
    
    // CHM Jawad
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    [mvc.commandDelegate evalJs:[NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.uploadingFailed(%@);", [self getJSONStringFromDictionary:requestOptions]]];
    UploadProgressViewController *uploadProgressVC = (UploadProgressViewController *)[[GlobalClass sharedInstance] getUploadProgressViewController];
    if (uploadProgressVC != nil)
        [uploadProgressVC resetData];
}

- (void) uploadStopped
{
    isUploading=NO;
    [uploadRequestDelegate uploadRequestStopped:self];
    
    // TODO: Callback
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    [mvc.commandDelegate evalJs:[NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.uploadingStopped(%@);", [self getJSONStringFromDictionary:requestOptions]]];
}


- (void) uploadCancelled
{
    isUploading=NO;
    
    [uploadRequestDelegate uploadRequestFailed:self];
    
    // TODO: Callback
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    [mvc.commandDelegate evalJs:[NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.uploadingCancelled(%@);", [self getJSONStringFromDictionary:requestOptions]]];
}

- (void) uploadingStarted
{
    isUploading=YES;
    
    // TODO: UPload Callback
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    [mvc.commandDelegate evalJs:[NSString stringWithFormat:@"window.plugins.VideoHandlerPlugin.uploadingStarted(%@);", [self getJSONStringFromDictionary:requestOptions]]];
}

#pragma mark - NEW Methods

- (void) startUploading
{
    if (![[requestOptions valueForKey:[NSString stringWithFormat:@"%@-%@",[[requestOptions objectForKey:@"mediaPath"] lastPathComponent],[requestOptions valueForKey:@"mediaid"]]] isEqualToString:@"videocompleted"]) {
        
        if(uploadTask)
        {
            return;
        }
        
        isUploading=YES;
        shouldUpload=YES;
        
        // CHM Jawad
//        MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:requestOptions];
//        [mvc.commandDelegate sendPluginResult:result callbackId:[requestOptions objectForKey:@"callBackID"]];
        
        
        [[GlobalClass sharedInstance] videoUploadingStarted:[requestOptions objectForKey:@"callBackID"]];
        
        NSString *lessonType=@"Lesson";
        if([[requestOptions objectForKey:@"lessonType"] intValue]==kLessonTypeClass)
        {
            lessonType=@"Class";
        }
        else if([[requestOptions objectForKey:@"lessonType"] intValue]==kLessonTypeMedia)
        {
            lessonType=@"Media";
        }
        //yahoo bug is this = newFileName:
       //(null)-5093-513532812-0.(null)
        
        //mergingcode
//        DataManager *dataManger = [DataManager sharedInstance];
//        NSDictionary *dic = dataManger.currentAcademy;
        NSDictionary *dic = GlobalClass.sharedInstance.academyData;
        NSString *urlToHit = @"";
        BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
        if ( isNew == NO){
            urlToHit = kMediaUploadURLOld;
        }else{
            urlToHit = kMediaUploadURL;
        }
        
        NSLog(@"all headers: %@", urlToHit);

        
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlToHit]];
        [request setHTTPMethod:@"PUT"];
        
        
        NSString *oldfileName=[[[requestOptions objectForKey:@"mediaPath"] lastPathComponent]  stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *newFileName= nil;
        
        if([lessonType isEqualToString:@"Media"])
        {
            newFileName = [NSString stringWithFormat:@"%@-%@-%d-%@.%@", oldfileName.stringByDeletingPathExtension, [requestOptions objectForKey:@"createdBy"], (int)[NSDate timeIntervalSinceReferenceDate], [requestOptions objectForKey:@"lessonType"], oldfileName.pathExtension];
            //holy
            if ([requestOptions objectForKey:@"PlayersList"]) {
                [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"PlayersList"]] forHTTPHeaderField:@"PlayersList"];
            }
            
        }
        else{
            newFileName = [NSString stringWithFormat:@"%@-%@-%@-%@.%@", oldfileName.stringByDeletingPathExtension, [requestOptions objectForKey:@"lessonID"], [requestOptions objectForKey:@"studentID"], [requestOptions objectForKey:@"lessonType"], oldfileName.pathExtension];
            [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"studentID"]] forHTTPHeaderField:@"PlayerId"];
            [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"lessonID"]] forHTTPHeaderField:@"LessonId"];
        }
        
        [request setValue:newFileName forHTTPHeaderField:@"FileName"];
        [requestOptions setObject:newFileName forKey:@"FileName"];
        
        [request setValue:@"0" forHTTPHeaderField:@"MediaId"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"mediaTypeId"]] forHTTPHeaderField:@"MediaTypeId"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"academyID"]] forHTTPHeaderField:@"AcademyID"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"createdBy"]] forHTTPHeaderField:@"CreatedBy"];
        
        if([[requestOptions objectForKey:@"lessonType"] intValue]==kLessonTypeMedia)
        {
            [request setValue:[requestOptions objectForKey:@"description"] forHTTPHeaderField:@"Description"];
        }
        else
        {
            [request setValue:[requestOptions objectForKey:@"title"] forHTTPHeaderField:@"Description"];
            [request setValue:[requestOptions objectForKey:@"description"] forHTTPHeaderField:@"MediaDetails"];
        }
        

        
        
//        [request setValue:[requestOptions objectForKey:@"description"] forHTTPHeaderField:@"Description"];
        [request setValue:[NSString stringWithFormat:@"%d",(int)(ceil([[requestOptions objectForKey:@"duration"] floatValue]))] forHTTPHeaderField:@"Duration"];
        
        NSLog(@"%@",[NSString stringWithFormat:@"%d",(ceil([[requestOptions objectForKey:@"duration"] floatValue]))]);
        [request setValue:[requestOptions objectForKey:@"fileURL"] forHTTPHeaderField:@"FileURL"];
        
        [request setValue:[requestOptions objectForKey:@"imageURL"] forHTTPHeaderField:@"ImageURL"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"modifiedBy"]] forHTTPHeaderField:@"ModifiedBy"];
        [request setValue:lessonType forHTTPHeaderField:@"LessonType"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"numPracticeDrill"]] forHTTPHeaderField:@"DrillCount"];
        [request setValue:[self uniqueMediaID] forHTTPHeaderField:kMediaIDKey];
        [request setValue:kMethodNameUploadChunk forHTTPHeaderField:kMethodNameKey];
        [request setValue:[NSString stringWithFormat:@"%@", @(totalFileSize)] forHTTPHeaderField:@"mediaFileSize"];
        //        token basedathentication
       [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"AcademyId"]] forHTTPHeaderField:@"AcademyId"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"UserName"]] forHTTPHeaderField:@"UserName"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"UserID"]] forHTTPHeaderField:@"UserID"];
        [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"AuthenticationToken"];
        //till here
        //        [request setValue:@"1" forHTTPHeaderField:@"IsOverlay"];
        
        if([requestOptions objectForKey:@"skillCategory"])
        {
            [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"skillCategory"]] forHTTPHeaderField:@"skillCategory"];
        }
        
        if([requestOptions objectForKey:@"skillTypes"])
        {
            [request setValue:[requestOptions objectForKey:@"skillTypes"] forHTTPHeaderField:@"skillTypes"];
        }
        
        //        @"SkillCatetoryID":[requestOptions objectForKey:@"SkillCatetoryID"],
        
        
        NSString *fileName=[[requestOptions objectForKey:@"mediaPath"] lastPathComponent];
        NSString *pathExtension=fileName.pathExtension;
        NSString *filePath=[requestOptions objectForKey:@"mediaPath"];
        if([pathExtension caseInsensitiveCompare:@"png"]!=NSOrderedSame && [pathExtension caseInsensitiveCompare:@"jpg"]!=NSOrderedSame && [pathExtension caseInsensitiveCompare:@"JPEG"]!=NSOrderedSame && [pathExtension caseInsensitiveCompare:@"mov"]!=NSOrderedSame)
        {
            filePath=[[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath] stringByAppendingPathComponent:fileName];
        }
        
        
        NSError *error;
        
        if ([[requestOptions objectForKey:@"mediaTypeId"] intValue] == kMediaTypeVideo) {
            
            // get image height and width using AVAssetImageGenerator
            CMTime actualTime;
            AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
            AVAssetImageGenerator   *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            imageGenerator.appliesPreferredTrackTransform=TRUE;
            CGImageRef halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
            
            while (halfWayImage  == nil) {
                halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
            }
            
            UIImage  *thumbnail = [[UIImage alloc]initWithCGImage:halfWayImage];
            
            if (thumbnail != nil) {
                [request setValue:[NSString stringWithFormat:@"%f",thumbnail.size.width] forHTTPHeaderField:@"videoWidth"];
                
                [request setValue:[NSString stringWithFormat:@"%f",thumbnail.size.height] forHTTPHeaderField:@"videoHeight"];
            }
            
        }
        
        
        //for youtube video upload
//                if([requestOptions objectForKey:@"IsEmbedded"])
//                {
//                    [request setValue:[requestOptions objectForKey:@"IsEmbedded"] forHTTPHeaderField:@"IsEmbedded"];
//                  [request setValue:[requestOptions objectForKey:@"CreationDate"] forHTTPHeaderField:@"CreationDate"];
//
//
//
//                }else{
//
//                    [request setValue:@"1" forHTTPHeaderField:@"IsEmbedded"];
//
//                }

        NSLog(@"all headers: %@", request.allHTTPHeaderFields);

        NSString *uploadFolderPath = [[OrbisMediaGallery sharedGallery] getUploadMediaGalleryFolderPath];
        uploadVideoFolderPath= [uploadFolderPath stringByAppendingPathComponent:fileName];
        
//        [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:uploadVideoFolderPath error:&error];
        [[NSFileManager defaultManager] copyItemAtPath:filePath toPath:uploadVideoFolderPath error:&error];

//        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
//
//                }else{
//                    NSLog(@" No permissions!");
//
//                    return;
//                }
                
        
        //    NSDictionary *fileAttributes=[[NSFileManager defaultManager] attributesOfItemAtPath:uploadVideoFolderPath error:nil];
        //    NSString *fileSize = [self transformedValue:[fileAttributes valueForKey:@"NSFileSize"]];
        //
        //    NSLog(@"file attributes %@",fileSize);
        

        
        uploadTask=[[self backgroundSession] uploadTaskWithRequest:request fromFile:[NSURL fileURLWithPath:filePath]];
        [uploadTask resume];
        
        [self setRequestStatus:UploadRequestStatusStarted];
        [self uploadingStarted];
        
    }
    
    //  [requestOptions setValue:@"completed" forKey:@"overlayscompleted"];
    else if (![[requestOptions  valueForKey:[NSString stringWithFormat:@"overlayscompleted-%@",[requestOptions valueForKey:@"mediaid"] ]] isEqualToString:@"completed"])
    {
        NSString *overlay =  [requestOptions valueForKey:[NSString stringWithFormat: @"overlayssaved-%@",[requestOptions valueForKey:@"mediaid"] ]];
        if (overlay.length > 0)
        {
            pathArray = [overlay componentsSeparatedByString:@",,"];
            NSString *filePath=[requestOptions objectForKey:@"mediaPath"];
            NSArray *overlayValuesA=[[OrbisMediaGallery sharedGallery] getOverlaysForVideoAtPath:filePath];
            NSMutableArray *tempOverlayA = [[NSMutableArray alloc]init];
            
            for (NSString *elementSt in overlayValuesA)
            {
                if (![pathArray containsObject:elementSt.lastPathComponent])
                {
                    //   int indexOfObj = [pathArray indexOfObject:elementSt];
                    [tempOverlayA addObject:elementSt];
                    //   [self startOverlayUploading:indexOfObj];
                }
            }
            
            if (tempOverlayA.count > 0){
                
                overlays = [[NSArray alloc]initWithArray:tempOverlayA];
                //  [overlays arrayByAddingObjectsFromArray:tempOverlayA];
                [requestOptions setObject:[NSString stringWithFormat:@"%lu",(unsigned long)kMediaTypeOverlay] forKey:@"mediaTypeId"];
                [self   getOverlay];
            }
        }
    }
}

- (void) startOverlayUploading:(int)i
{
    uploadTask = nil;
    NSDictionary *dicObj = [uploadOverlayA objectAtIndex:i];
    
    if(uploadTask)
    {
        return;
    }
    
    isOverlayUploaded = YES;
    isUploading=YES;
    shouldUpload=YES;
    
    // TODO: Callback
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:requestOptions];
//    [mvc.commandDelegate sendPluginResult:result callbackId:[requestOptions objectForKey:@"callBackID"]];
    
    NSString *lessonType=@"Lesson";
    if([[requestOptions objectForKey:@"lessonType"] intValue]==kLessonTypeClass)
    {
        lessonType=@"Class";
    }
    
    
    //mergingcode
//    DataManager *dataManger = [DataManager sharedInstance];
//    NSDictionary *dic = dataManger.currentAcademy;
    NSDictionary *dic = GlobalClass.sharedInstance.academyData;
    NSString *urlToHit = @"";
    BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
    if ( isNew == NO){
        urlToHit = kMediaUploadURLOld;
    }else{
        urlToHit = kMediaUploadURL;
    }
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlToHit]];
    [request setHTTPMethod:@"PUT"];
    
    NSString *oldfileName=[dicObj valueForKey:@"filepath"];
    NSString *UUID = [[NSUUID UUID] UUIDString];
    NSString *newFileName=[NSString stringWithFormat:@"overlay-%@-%@-%@-%i.%@",UUID,[requestOptions objectForKey:@"lessonID"],[requestOptions valueForKey:@"mediaid"] , (int)(pathArray.count+i), oldfileName.pathExtension];
    
    [request setValue:newFileName forHTTPHeaderField:@"FileName"];
    
    [request setValue:[dicObj valueForKey:@"width"] forHTTPHeaderField:@"overlayWidth"];
    [request setValue:[dicObj valueForKey:@"height"]  forHTTPHeaderField:@"overlayHeight"];
    [request setValue:[dicObj valueForKey:@"orientation"]  forHTTPHeaderField:@"orientation"];
    [request setValue:[requestOptions valueForKey:@"mediaid"] forHTTPHeaderField:@"MediaId"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"academyID"]] forHTTPHeaderField:@"AcademyID"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"createdBy"]] forHTTPHeaderField:@"CreatedBy"];
    [request setValue:[NSString stringWithFormat:@"%@", @(totalFileSize)] forHTTPHeaderField:@"mediaFileSize"];
    [request setValue:@"1" forHTTPHeaderField:@"IsOverlay"];
    [request setValue:@"2" forHTTPHeaderField:@"MediaTypeId"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"modifiedBy"]] forHTTPHeaderField:@"ModifiedBy"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"studentID"]] forHTTPHeaderField:@"PlayerId"];
    [request setValue:[self uniqueMediaID] forHTTPHeaderField:kMediaIDKey];
    [request setValue:[NSString stringWithFormat:@"%d",(int)(ceil([[requestOptions objectForKey:@"duration"] floatValue]))] forHTTPHeaderField:@"Duration"];
    [request setValue:lessonType forHTTPHeaderField:@"LessonType"];
    
    
    //        token basedathentication    
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"AcademyId"]] forHTTPHeaderField:@"AcademyId"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"UserName"]] forHTTPHeaderField:@"UserName"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"UserID"]] forHTTPHeaderField:@"UserID"];
    [request setValue:[NSString stringWithFormat:@"%@",[requestOptions objectForKey:@"AuthenticationToken"]] forHTTPHeaderField:@"AuthenticationToken"];
    //till here
    //    @"SkillCatetoryID":[requestOptions objectForKey:@"SkillCatetoryID"],
    
    NSLog(@"all headers: %@", request.allHTTPHeaderFields);
    
    NSString *filePath = [dicObj valueForKey:@"url"];
    uploadTask=[[self backgroundSession] uploadTaskWithRequest:request fromFile:[NSURL fileURLWithPath:filePath]];
    [uploadTask resume];
    
    // [self setRequestStatus:UploadRequestStatusStarted];
    //   [self uploadingStarted];
    isOverlayUploaded = NO;
    NSString * mediaIdString =  [requestOptions valueForKey:[NSString stringWithFormat:@"overlayssaved-%@",[requestOptions valueForKey:@"mediaid"]]];
    
    
    
    /*
     if (![overlaysSt isEqualToString:@""]) {
     overlaysSt = [NSString stringWithFormat:@"%@,,%@",overlaysSt,filePath.lastPathComponent];
     }
     else
     {
     */
    overlaysSt =  filePath.lastPathComponent;
    //   }
    
    
    
    
    if (mediaIdString.length > 0 )
    {
        [requestOptions setValue:[NSString stringWithFormat:@"%@,,%@",mediaIdString,overlaysSt] forKey:[NSString stringWithFormat:@"overlayssaved-%@",[requestOptions valueForKey:@"mediaid"]]];
    }
    else
    {
        [requestOptions setValue:overlaysSt forKey:[NSString stringWithFormat:@"overlayssaved-%@",[requestOptions valueForKey:@"mediaid"]]];
        
    }
    [requestOptions setValue:[NSString stringWithFormat:@"%i",pathArray.count+i] forKey:@"totalOverlayUploaded"];
    
}


- (id)transformedValue:(id)value
{
    
    double convertedValue = [value doubleValue];
    int multiplyFactor = 0;
    
    NSArray *tokens = [NSArray arrayWithObjects:@"bytes",@"KB",@"MB",@"GB",@"TB",nil];
    
    while (convertedValue > 1024) {
        convertedValue /= 1024;
        multiplyFactor++;
    }
    
    return [NSString stringWithFormat:@"%4.2f %@",convertedValue, [tokens objectAtIndex:multiplyFactor]];
}

- (UploadRequestStatus) requestStatus
{
    return [[requestOptions objectForKey:kUploadRequestStatusKey] unsignedIntegerValue];
}

- (void) setRequestStatus:(UploadRequestStatus)status
{
    requestStatus=status;
    [requestOptions setObject:@(status) forKey:kUploadRequestStatusKey];
    [[UploadManager sharedManager] saveInfoOfAllUploadingVideos];
}

- (void) resumeUploading
{
    
}

- (void) stopUploading
{
    shouldCancelUploading=NO;
    [uploadTask cancel];
    uploadTask=nil;
    [backgroundSession invalidateAndCancel];
    [self setRequestStatus:UploadRequestStatusSuspended];
    [self uploadStopped];
}

- (void) cancelUploading
{
    shouldCancelUploading=YES;
    [backgroundSession invalidateAndCancel];
}

- (NSString*) mediaID
{
    return [requestOptions objectForKey:@"mediaID"];
}

- (void) setMediaID:(NSString*)mediaId
{
    mediaID=mediaId;
    [requestOptions setObject:mediaID forKey:@"mediaID"];
    [[UploadManager sharedManager] saveInfoOfAllUploadingVideos];
}

- (void) updatePogressWithMediaInfo:(NSDictionary*)mediaInfo didSendData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite
{
    //    NSString *methodName=[mediaInfo objectForKey:kMethodNameKey];
    //    if([methodName isEqualToString:kMethodNameUploadChunk])
    //    {
    bytesUploaded=totalBytesWritten;
    [self updateProgress];
    //    }
}

- (void)uploadingCompletedWithResponse:(NSHTTPURLResponse *)response responseData:(NSData*)responseData mediaInfo:(NSDictionary*)mediaInfo
{
    NSLog(@"folks see this %@", [responseData description]);
    
    //    NSString *methodName=[mediaInfo objectForKey:kMethodNameKey];
    //    if([methodName isEqualToString:kMethodNameCompleteAndPublishMultipart])
    //    {
    //        NSString *string=[self getResponseStringFromResponseFile];
    //        if([string integerValue]==1)
    //        {
    
    NSString *string=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //["30790"]

//    if this ["0"] type response  return from this method because unecesory calling
    //Q.A relax
//    NSString* serverResponceMadehardcodedbyMe = [NSString stringWithFormat:@"%@\"%i\"%@", @"[",0,@"]"];
    //    {"Message":"Authorization has been denied for this request."}

//    NSString* serverResponceMadehardcodedbyMe = [NSString stringWithFormat:@"%@\"%i\"%@", @"{"Message":"Authorization has been denied for this request."}"];
//
//    if ([string isEqualToString:serverResponceMadehardcodedbyMe]){
//
//        return;
//    }
    //till here can be removed
    
    
    // string = [string stringByReplacingOccurrencesOfString:@"[\"" withString:@""];
    // string = [string stringByReplacingOccurrencesOfString:@"\"]" withString:@""];
    
    if (string.length > 0) {
        NSArray *mediaIdA = [string componentsSeparatedByString:@"\"]"];//["30790,

        if (mediaIdA.count > 0) {
            string = [mediaIdA lastObject];
            int lastObjIndex = (int)[mediaIdA indexOfObject:string];
            //here is crash yahoo
            if (mediaIdA.count < 2) {
                
                dispatch_async(dispatch_get_main_queue(),  ^(void){

                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                                    message:@"Something went wrong during uploading video"  delegate:self
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                    });
                return;
            }
            string = [mediaIdA objectAtIndex:lastObjIndex-1];
            string = [string stringByReplacingOccurrencesOfString:@"[\"" withString:@""];
            
        }
        
        
        if (([[requestOptions objectForKey:@"mediaTypeId"] integerValue] == kMediaTypeVideo) && !videoUploaded  && [string intValue] > 0) {
            cleanedString = string;
            
            [requestOptions setObject:[NSString stringWithFormat:@"%i",(int)kMediaTypeOverlay] forKey:@"mediaTypeId"];
            videoUploaded = YES;
            NSString *filePath=[requestOptions objectForKey:@"mediaPath"];
            [requestOptions setValue:@"videocompleted" forKey:[NSString stringWithFormat:@"%@-%@",filePath.lastPathComponent,string]];
            [requestOptions setValue:cleanedString forKey:@"mediaid"];
            /*
             NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil] ;
             videoUploaded = YES;
             
             
             for (NSArray *dicObj in  jsonDic) {
             cleanedString = [dicObj valueForKey:@"Media_Id"] ;
             
             }
             NSLog(@"string: %@", string);
             
             */
            
            if (overlays.count == 0) {
                overlays=[[OrbisMediaGallery sharedGallery] getOverlaysForVideoAtPath:filePath];
            }
            
            [self getOverlay];
        }
        else if ([[requestOptions objectForKey:@"mediaTypeId"] integerValue] == kMediaTypeOverlay) {
            NSString *filePath=[requestOptions objectForKey:@"mediaPath"];
            if (overlays.count == 0) {
                overlays=[[OrbisMediaGallery sharedGallery] getOverlaysForVideoAtPath:filePath];
            }
            
            [self getOverlay];
            
        }
        else{
            
            [self uploadCompleted];
            [backgroundSession invalidateAndCancel];
        }
    }
    else{
        [self uploadCompleted];
        [backgroundSession invalidateAndCancel];
    }
    
    
    
    
    /*
     
     //    [self uploadCompleted];
     //   [backgroundSession invalidateAndCancel];
     
     
     
     //        }
     //        else
     //        {
     //            [self checkForVideoStatus];
     //        }
     //    }
     //    else
     //    {
     //        BOOL shouldCompleteUpload=YES;
     //        @try {
     //            NSError *parseError=nil;
     //            id responseObject=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&parseError];
     //            NSDictionary *responseDictionary=nil;
     //            if([responseObject isKindOfClass:[NSArray class]])
     //            {
     //                NSArray *responseArray=(NSArray*)responseObject;
     //                responseDictionary=[responseArray firstObject];
     //            }
     //            else if([responseObject isKindOfClass:[NSDictionary class]])
     //            {
     //                responseDictionary=(NSDictionary*)responseObject;
     //            }
     //
     //            if(responseDictionary)
     //            {
     //                //            {"FileNames":null,"Description":"File Uploaded successfully","Media_Id":"9130","ResponseCode":"200","CreatedTimestamp":"0001-01-01T00:00:00","UpdatedTimestamp":"0001-01-01T00:00:00","DownloadLink":null,"ContentTypes":null,"Names":null}
     //                NSString *mediaId=[responseDictionary objectForKey:@"Media_Id"];
     //                if(mediaId)
     //                {
     //                    shouldCompleteUpload=NO;
     //                    [self setMediaID:mediaId];
     //                    [self setRequestStatus:UploadRequestStatusUploaded];
     //                    [self checkForVideoStatus];
     //                    //                mediaID=mediaId;
     //                }
     //                else
     //                {
     //                    shouldCompleteUpload=NO;
     //                    [self uploadCompleted];
     //                }
     //
     //            }
     //            else
     //            {
     //                shouldCompleteUpload=NO;
     //                [self uploadCompleted];
     //            }
     //
     //        }
     //        @catch (NSException *exception) {
     //
     //        }
     //        @finally {
     //            NSError *error;
     //            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:uploadVideoFolderPath error:&error];
     //            if (success) {
     //                NSLog(@"file deleted successfully");
     //            }
     //        }
     //
     //        if(shouldCompleteUpload)
     //        {
     //            [self uploadCompleted];
     //        }
     //    }
     */
    
    /*
     NSString *methodName=[videoInfo objectForKey:kMethodNameKey];
     
     if([methodName isEqualToString:kMethodNameUploadChunk])
     {
     NSString *chunkNumberString=[videoInfo objectForKey:kChunkNumberKey];
     [self setChunkStatus:ChunkUploadStatusUploaded withChunkNumber:chunkNumberString.intValue];
     
     [self updateAllTasksProgressOfSessionWithTask:nil completedStatus:YES];
     
     if(noMoreChunksToUpload)
     {
     noMoreChunksToUpload=NO;
     [self startUploadingParts];
     }
     
     //        if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground)
     //        {
     //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
     //                UILocalNotification *localNotification=[[UILocalNotification alloc] init];
     //                localNotification.fireDate=[NSDate date];
     //                localNotification.alertBody=[NSString stringWithFormat:@"chunk uploaded: %@", chunkNumberString];
     //                localNotification.alertAction=@"View";
     //                localNotification.soundName = UILocalNotificationDefaultSoundName;
     //                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
     //            });
     //        }
     
     //        [self chunkUploaded];  //+-+-
     }
     else if([methodName isEqualToString:kMethodNameCreateAndSignUploadpart])
     {
     NSArray *responseArray=[self getResponseArrayFromResponseFile];
     for(NSDictionary *d in responseArray)
     {
     NSMutableDictionary *chunkInfo=[self getChunkInfoWithChunkNumber:([[d objectForKey:@"PartNumber"] intValue]-1)];
     [chunkInfo setObject:[d objectForKey:@"url"] forKey:@"url"];
     [chunkInfo setObject:[d objectForKey:@"authHeader"] forKey:@"authHeader"];
     [chunkInfo setObject:[d objectForKey:@"dateHeader"] forKey:@"dateHeader"];
     [chunkInfo setObject:[d objectForKey:@"Key"] forKey:@"key"];
     [chunkInfo setObject:[d objectForKey:@"UploadId"] forKey:@"uploadId"];
     [chunkInfo setObject:@(ChunkUploadStatusHaveUploadInfo) forKey:kChunkStatusKey];
     }
     
     //        [self saveInfoOfAllUploadingChunks];
     //        [self startUploadingParts];
     }
     else if([methodName isEqualToString:kMethodNameUploadThumbnail])
     {
     if([self requestStatus]==UploadRequestStatusUploadingThumbnail)
     {
     [self setRequestStatus:UploadRequestStatusThumbnailUploaded];
     }
     
     [self startUploadingParts];
     //        if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground)
     //        {
     //            dispatch_async(dispatch_get_global_queue(0, 0), ^{
     //                UILocalNotification *localNotification=[[UILocalNotification alloc] init];
     //                localNotification.fireDate=[NSDate date];
     //                localNotification.alertBody=@"Thumbnail Uploaded";
     //                localNotification.alertAction=@"View";
     //                localNotification.soundName = UILocalNotificationDefaultSoundName;
     //                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
     //            });
     //        }
     }
     else if([methodName isEqualToString:kMethodNameCompleteAndPublishMultipart])
     {
     [self setRequestStatus:UploadRequestStatusPublished];
     [self uploadingCompleted];
     }
     */
    
}


-(void)getOverlay
{
    if (isOverlayUploaded  == NO ) {
        if (overlays.count> 0) {
            
            if (uploadOverlayA.count == 0) {
                NSString * uploadOverlayFolderPath;
                for (int i = 0; i < overlays.count; i++) {
                    NSMutableDictionary *uploadDic = [[NSMutableDictionary alloc]init];
                    NSString *fileName=[[overlays objectAtIndex:i] lastPathComponent];
                    
                    NSString *overlayFilePath = [[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath] stringByAppendingPathComponent:fileName];
                    NSError *error;
                    NSString *uploadFolderPath = [[OrbisMediaGallery sharedGallery] getUploadMediaGalleryFolderPath];
                    uploadOverlayFolderPath= [uploadFolderPath stringByAppendingPathComponent:fileName];
                    [[NSFileManager defaultManager] copyItemAtPath:overlayFilePath toPath:uploadOverlayFolderPath error:&error];
                    
                    UIImage *img = [UIImage imageWithContentsOfFile:uploadOverlayFolderPath];
                    CGFloat height = img.size.height;
                    CGFloat width = img.size.width;
                    UIImageOrientation orientation = img.imageOrientation;
                    [uploadDic setValue:uploadOverlayFolderPath forKey:@"url"];
                    [uploadDic setValue:[NSString stringWithFormat:@"%f",height] forKey:@"height"];
                    [uploadDic setValue:[NSString stringWithFormat:@"%f",width] forKey:@"width"];
                    [uploadDic setValue:[NSString stringWithFormat:@"%li",(long)orientation] forKey:@"orientation"];
                    [uploadDic setValue:uploadOverlayFolderPath forKey:@"filepath"];
                    [uploadOverlayA addObject:uploadDic];
                }
            }
            
            /*
             
             if (![[defObj valueForKey:uploadOverlayFolderPath.lastPathComponent] isEqualToString:@"completed"] && [[defObj valueForKey:uploadOverlayFolderPath.lastPathComponent] intValue] >= 1) {
             
             for (int i =[[defObj valueForKey:uploadOverlayFolderPath.lastPathComponent] intValue]+1 ; i < uploadOverlayA.count; i++) {
             [self startOverlayUploading:i];
             }
             }
             else
             {
             
             */
            if (uploadingVal < overlays.count) {
                [self startOverlayUploading:uploadingVal];
                uploadingVal++;
            }
            else
            {
                isOverlayUploaded = YES;
                [requestOptions setValue:@"completed" forKey:[NSString stringWithFormat:@"overlayscompleted-%@",[requestOptions valueForKey:@"mediaid"]]];
                [self uploadCompleted];
                [backgroundSession invalidateAndCancel];
            }
        }
        else
        {
            isOverlayUploaded = YES;
            [requestOptions setValue:@"completed" forKey:[NSString stringWithFormat:@"overlayscompleted-%@",[requestOptions valueForKey:@"mediaid"]]];
            [self uploadCompleted];
            [backgroundSession invalidateAndCancel];
            
            
        }
    }
    else{
        [self uploadCompleted];
        [backgroundSession invalidateAndCancel];
    }
    
    
}

- (void) uploadingFailedWithError:(NSError*)error mediaInfo:(NSDictionary*)mediaInfo
{
    NSLog(@"error: %@", error);
    
    if(shouldCancelUploading)
    {
        [self uploadCancelled];
    }
    else
    {
        [self uploadStopped];
    }
    
    /*
     Reachability *reachability=[Reachability reachabilityForInternetConnection];
     NSString *methodName=[videoInfo objectForKey:kMethodNameKey];
     
     if(!reachability.isReachable)
     {
     [self pauseUploading];
     }
     
     if([methodName isEqualToString:kMethodNameUploadChunk])
     {
     NSString *chunkNumberString=[videoInfo objectForKey:kChunkNumberKey];
     NSMutableDictionary *chunkInfo=[self getChunkInfoWithChunkNumber:chunkNumberString.intValue];
     [chunkInfo setObject:@(ChunkUploadStatusCancelled) forKey:kChunkStatusKey];
     [self saveInfoOfAllUploadingChunks];
     
     if(noMoreChunksToUpload)
     {
     noMoreChunksToUpload=NO;
     if(reachability.isReachable && error.code!=NSURLErrorCancelled)
     {
     [self startUploadingParts];
     }
     }
     
     //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
     //            UILocalNotification *localNotification=[[UILocalNotification alloc] init];
     //            localNotification.fireDate=[NSDate date];
     //            localNotification.alertBody=[NSString stringWithFormat:@"Chunk Uploading failed: %@, will retry", chunkNumberString];
     //            localNotification.alertAction=@"View";
     //            localNotification.soundName = UILocalNotificationDefaultSoundName;
     //            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
     //        });
     
     }
     else if ([methodName isEqualToString:kMethodNameCreateAndSignUploadpart])
     {
     [self setRequestStatus:UploadRequestStatusNotStarted];
     if(reachability.isReachable && error.code!=NSURLErrorCancelled)
     {
     [self startUploadingParts];
     }
     }
     else if ([methodName isEqualToString:kMethodNameUploadThumbnail])
     {
     [self setRequestStatus:UploadRequestStatusUploadingParts];
     if(publishTask)
     {
     [publishTask cancel];
     }
     
     if(reachability.isReachable && error.code!=NSURLErrorCancelled)
     {
     [self startUploadingParts];
     }
     }
     else if ([methodName isEqualToString:kMethodNameCompleteAndPublishMultipart])
     {
     [self pauseUploading];
     [self setRequestStatus:UploadRequestStatusUploadingParts];
     
     if(error.code!=NSURLErrorCancelled)
     {
     if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground)
     {
     UILocalNotification *localNotification=[[UILocalNotification alloc] init];
     localNotification.fireDate=[NSDate date];
     localNotification.alertBody=[NSString stringWithFormat:@"Publishing video failed: %@", [[_requestOptions objectForKey:@"videoPath"] lastPathComponent]];
     localNotification.alertAction=@"View";
     localNotification.soundName = UILocalNotificationDefaultSoundName;
     [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
     }
     else
     {
     dispatch_async(dispatch_get_main_queue(), ^{
     [[[UIAlertView alloc] initWithTitle:@"Sibme" message:@"Publishing video failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
     });
     }
     }
     }
     */
    
}

#pragma mark

- (NSString*)getJSONStringFromDictionary:(NSDictionary*)d
{
    //    NSArray *arr=[NSArray arrayWithObject:d];
    
    NSError* error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:d
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (error != nil) {
        NSLog(@"NSArray JSONString error: %@", [error localizedDescription]);
        return nil;
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

- (void) removeResponseFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *responseFilePath=[documentsDirectory stringByAppendingPathComponent:responseFileName];
    [[NSFileManager defaultManager] removeItemAtPath:responseFilePath error:nil];
}

- (NSString*) getResponseStringFromResponseFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *responseFilePath=[documentsDirectory stringByAppendingPathComponent:responseFileName];
    NSString *stringResponse = [[NSString alloc] initWithContentsOfFile:responseFilePath encoding:NSUTF8StringEncoding error:NULL];
    return stringResponse;
}


#pragma mark - upload video dimentions
+(void)uploadVideoDimentions:(int )mediaId andWidth:(int)width andHeight:(int  )height
{
    
    NSLog(@"media id %i , width %i, height %i",mediaId,width,height);

    //mergingcode
//    DataManager *dataManger = [DataManager sharedInstance];
//    NSDictionary *dic = dataManger.currentAcademy;
    NSDictionary *dic = GlobalClass.sharedInstance.academyData;

    NSString *urlToHit = @"";
    
    BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
    if ( isNew == NO){
        urlToHit = kVideoDimentionsUploadURLOld;
    }else{
        urlToHit = kVideoDimentionsUploadURL;
    }

    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%d/%d/%d",urlToHit,mediaId,width,height]];
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    //    [NSURLConnection sendAsynchronousRequest:urlReq queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *responce, NSData *data, NSError *error){
    //    }];
    
    [[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]]
     dataTaskWithRequest:urlReq
     completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
         NSString  *valSt = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"val st %@",valSt);
     }];
    
}




@end




