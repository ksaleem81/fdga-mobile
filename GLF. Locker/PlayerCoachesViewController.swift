//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu


class PlayerCoachesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    
 
    @IBOutlet weak var tableView: UITableView!
 
    var studentData = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()

     getCoachesInfo()
     settingRighMenuBtn()
     tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PlayerCoachesViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }
    
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCoachesTableViewCell", for: indexPath) as! PlayerCoachesTableViewCell
        
        var coachName = ""

        if let title = data[indexPath.row]["CoachFullName"] as? String{
            coachName =  title
        }
        
        cell.companyLbl.text = coachName
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action:#selector(selectDeleteBtn(sender:)) , for:.touchUpInside)
               return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    
    }
    
    @objc func selectDeleteBtn(sender:UIButton)  {
        
    let alertController = UIAlertController(title: "Alert", message: "Are You Sure You What to Delete Coach?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.deleteCoach(tag: sender.tag)
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
         
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
      
    }
    
    func deleteCoach(tag:Int)  {
    //PlayerId
        var playerID = ""
        var playerKey = ""
        if currentUserLogin == 4 {
            
            if let id = studentData["playerId"] as? Int{
                playerID = "\(id)"
            }
           playerKey = "PlayerId"
        }else{
        if let id = studentData["StudentID"] as? Int{
            playerID = "\(id)"
            }
             playerKey = "PlayerId"
        }

        var coachID = ""
        var coachKey = ""
        if currentUserLogin == 4 {
            
            if let id =  data[tag]["PlayerCoachId"] as? Int{
                coachID = "\(id)"
                
            }
            coachKey = "PlayerCoachId"
        }else{
          coachKey =  "PlayerCoachId"//"ClassId"
        if let id = data[tag]["PlayerCoachId"] as? Int{
            coachID = "\(id)"
            }
        }
        
        NetworkManager.performRequest(type:.post, method: "Student/DeletePlayerCoach", parameter:[playerKey:playerID as AnyObject,coachKey:coachID as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return

            case _ as [[String:AnyObject]]: break
                
            default: break
            }
            let responce = object as! Int
            if responce == 1{
                DataManager.sharedInstance.printAlertMessage(message:"Successfully Deleted Coach", view:self)
                self.getCoachesInfo()
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"Unable To Delete", view:self)
            }
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

   
    func getCoachesInfo()  {

        var playerId = ""
        if currentUserLogin == 4 {
            
            if let id = studentData["playerId"] as? Int{
                playerId = "\(id)"
            }
        }else{
        if let id = studentData["StudentID"] as? Int{
            playerId = "\(id)"
            }
        }

        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerCoaches/\(playerId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
           self.data = object as! [[String:AnyObject]]
           self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

}

