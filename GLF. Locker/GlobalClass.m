//
//  GlobalClass.m
//  GLF. Locker
//
//  Created by Jawad Ahmed on 07/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

#import "GlobalClass.h"
#import "GLF__Locker-Swift.h"

#define kiOSCalendarName @"GLF. Locker Calendar"
//#define kiOSCalendarName @"Jay Kelly Golf Calendar"



static NSMutableString *logString=nil;
static NSString *logFileNameString=nil;


@implementation GlobalClass {
    UploadProgressViewController *uploadProgressVC;
}


@synthesize window, deviceToken;
@synthesize backgroundSessions, backgroundSessionDelegate,academyData;


+ (GlobalClass *)sharedInstance {
    static GlobalClass *shared = nil;
    
    
    if (shared == nil) {
        shared = [[self alloc] init];
    }
    
    return shared;
}

- (id)init
{
    /** If you need to do any extra app-specific initialization, you can do it here
     *  -jm
     **/
    
     self.currentTargetC = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];

    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
    int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
#if __has_feature(objc_arc)
    NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
#else
    NSURLCache* sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
#endif
    [NSURLCache setSharedURLCache:sharedCache];
    
    self = [super init];
    [self launch];
    return self;
}

- (void)launch {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if(!backgroundSessionDelegate)
    {
        self.backgroundSessionDelegate = [BLBackgroundSessionDelegate new];
    }
    
    
    self.backgroundSessions=[NSMutableArray array];
//    [UploadManager sharedManager];
    [self resetBackgroundSession];
    
    
#if __has_feature(objc_arc)
    self.window = [[UIWindow alloc] initWithFrame:screenBounds];
#else
    self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
#endif
    self.window.autoresizesSubviews = YES;
    
#if __has_feature(objc_arc)
//    self.viewController = [[MainViewController alloc] init];
#else
//    self.viewController = [[[MainViewController alloc] init] autorelease];
#endif
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *videoQuality=[userDefaults objectForKey:kOrbisVideoQualityKey];
    if(!videoQuality)
    {
        videoQuality=@"high";
        [userDefaults setObject:videoQuality forKey:kOrbisVideoQualityKey];
        [userDefaults synchronize];
    }
    
    NSInteger updateCount=[userDefaults integerForKey:@"newChanges1"];
    if(updateCount==0)
    {
        videoQuality=@"high";
        [userDefaults setObject:videoQuality forKey:kOrbisVideoQualityKey];
        [userDefaults setInteger:++updateCount forKey:@"newChanges1"];
        [userDefaults synchronize];
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge];
    }
    
    logString=[[NSMutableString alloc] init];
}

- (NSURLSession *)backgroundURLSession
{
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *identifier = @"io.objc.backgroundTransferExample";
        NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration backgroundSessionConfiguration:identifier];
        session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                delegate:self
                                           delegateQueue:[NSOperationQueue mainQueue]];
    });
    
    return session;
}

- (void) handleEventsArray:(NSArray*)eventsArray
{
    for(NSDictionary *event in eventsArray)
    {
        if(event)
        {
            NSInteger timeZoneOffset=-1*[[NSTimeZone localTimeZone] secondsFromGMT];
            //        date=[date dateByAddingTimeInterval:n];
            
            NSString *startDateString=[event objectForKey:@"StartDate"];
            NSDate *startDate=nil;
            if(startDateString)
            {
                startDateString=[startDateString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\\/()"]];
                startDateString=[startDateString stringByReplacingOccurrencesOfString:@"Date(" withString:@""];
                
                NSTimeInterval _startInterval = [startDateString doubleValue] / 1000; // strip millis
                NSDate *tstartDate = [NSDate dateWithTimeIntervalSince1970:(_startInterval+timeZoneOffset)];
                NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tstartDate];
                
                id startTimeObject=[event objectForKey:@"StartTime"];
                if([startTimeObject isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *startTime=(NSDictionary*) startTimeObject;
                    [dateComponents setHour:[[startTime objectForKey:@"Hours"] integerValue]];
                    [dateComponents setMinute:[[startTime objectForKey:@"Minutes"] integerValue]];
                    [dateComponents setSecond:[[startTime objectForKey:@"Seconds"] integerValue]];
                }
                else if([startTimeObject isKindOfClass:[NSString class]])
                {
                    NSString *startTime=(NSString*) startTimeObject;
                    NSArray *timeComponents=[startTime componentsSeparatedByString:@":"];
                    if(timeComponents.count>0)
                        [dateComponents setHour:[[timeComponents objectAtIndex:0] integerValue]];
                    if(timeComponents.count>1)
                        [dateComponents setMinute:[[timeComponents objectAtIndex:1] integerValue]];
                    if(timeComponents.count>2)
                        [dateComponents setSecond:[[timeComponents objectAtIndex:2] integerValue]];
                }
                
                
                startDate=[[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            }
            
            NSString *endDateString=[event objectForKey:@"EndDate"];
            NSDate *endDate=nil;
            if(endDateString)
            {
                endDateString=[endDateString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\\/()"]];
                endDateString=[endDateString stringByReplacingOccurrencesOfString:@"Date(" withString:@""];
                
                NSTimeInterval _endInterval = [endDateString doubleValue] / 1000; // strip millis
                NSDate *tendDate = [NSDate dateWithTimeIntervalSince1970:(_endInterval+timeZoneOffset)];
                NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tendDate];
                
                id endTimeObject=[event objectForKey:@"EndTime"];
                if([endTimeObject isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *endTime=(NSDictionary*) endTimeObject;
                    [dateComponents setHour:[[endTime objectForKey:@"Hours"] integerValue]];
                    [dateComponents setMinute:[[endTime objectForKey:@"Minutes"] integerValue]];
                    [dateComponents setSecond:[[endTime objectForKey:@"Seconds"] integerValue]];
                }
                else if([endTimeObject isKindOfClass:[NSString class]])
                {
                    NSString *endTime=(NSString*) endTimeObject;
                    NSArray *timeComponents=[endTime componentsSeparatedByString:@":"];
                    if(timeComponents.count>0)
                        [dateComponents setHour:[[timeComponents objectAtIndex:0] integerValue]];
                    if(timeComponents.count>1)
                        [dateComponents setMinute:[[timeComponents objectAtIndex:1] integerValue]];
                    if(timeComponents.count>2)
                        [dateComponents setSecond:[[timeComponents objectAtIndex:2] integerValue]];
                }
                
                
                endDate=[[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            }
            
//            NSString *title = [event objectForKey:@"LessonId"];
//            
//            Calendar *calendar=[[Calendar alloc] initWithWebView:viewController.webView];
//            
//            BOOL isCanceled=[[event objectForKey:@"IsCanceled"] boolValue];
//            if(isCanceled ==YES)
//            {
//                [calendar deleteEventFromCalendar:kiOSCalendarName withTitle:title];
//            }
//            else
//            {
//                NSString *newTitle = [NSString stringWithFormat:@"%@ %@ - %@", [event objectForKey:@"PlayerName"], [event objectForKey:@"ProgramTypeName"],[event objectForKey:@"LessonId"]];
//                NSString *newLocation = @"";
//                NSString *newNotes = [NSString stringWithFormat:@"%@ %@",[event objectForKey:@"ProgramTypeName"],[event objectForKey:@"PlayerName"]];
//                [calendar modifyEventWithCalendar:kiOSCalendarName title:title newTitle:newTitle newLocation:newLocation newNotes:newNotes newStartDate:startDate newEndDate:endDate];
//            }
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Received remote notification with userInfo %@", userInfo);
    
    NSError *error=nil;
    NSString *jsonString=[userInfo objectForKey:@"Event"];
    id jsonObject=[NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    if([jsonObject isKindOfClass:[NSArray class]])
    {
        [self handleEventsArray:(NSArray*)jsonObject];
    }
    else if([jsonObject isKindOfClass:[NSDictionary class]])
    {
        [self handleEventsArray:@[(NSDictionary*)jsonObject]];
    }
    
    UIApplicationState appState=[[UIApplication sharedApplication] applicationState];
    if(appState==UIApplicationStateActive)
    {
        NSString *message = nil;
        id aps=[userInfo objectForKey:@"aps"];
        if(aps!=nil)
        {
            id alert = [aps objectForKey:@"alert"];
            if ([alert isKindOfClass:[NSString class]]) {
                message = alert;
            } else if ([aps isKindOfClass:[NSDictionary class]]) {
                message = [alert objectForKey:@"body"];
            }
        }
        else
        {
            id alert = [userInfo objectForKey:@"alert"];
            if ([alert isKindOfClass:[NSString class]]) {
                message = alert;
            } else if ([alert isKindOfClass:[NSDictionary class]]) {
                message = [alert objectForKey:@"body"];
            }
        }
        
        if(message==nil)
        {
            message=@"Notification received.";
        }
        
        if (message) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"GLF. Locker"
                                                                message:message  delegate:self
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    //    NSNumber *contentID = @(101);//userInfo[@"content-id"];
    //    NSString *downloadURLString = [NSString stringWithFormat:@"http://108.166.98.107/Telenotes/IMG_0006.MOV"];
    //    NSURL* downloadURL = [NSURL URLWithString:downloadURLString];
    //
    //    NSURLRequest *request = [NSURLRequest requestWithURL:downloadURL];
    //    NSURLSessionDownloadTask *task = [[self backgroundURLSession] downloadTaskWithRequest:request];
    //    task.taskDescription = [NSString stringWithFormat:@"Podcast Episode %d", [contentID intValue]];
    //    [task resume];
    
    completionHandler(UIBackgroundFetchResultNewData);
}


-(void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    NSLog(@"videoPath: %@, error: %@", videoPath, error);
}



#pragma Mark - NSURLSessionDownloadDelegate

- (void) URLSession:(NSURLSession *)session
       downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location
{
    NSLog(@"downloadTask:%@ didFinishDownloadingToURL:%@", downloadTask.taskDescription, location);
    
    // Copy file to your app's storage with NSFileManager
    // ...
    
    NSString *filePath=[NSTemporaryDirectory() stringByAppendingPathComponent:@"vid.MOV"];
    [[NSFileManager defaultManager] copyItemAtURL:location toURL:[NSURL fileURLWithPath:filePath] error:nil];
    
    if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath))
        UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:),nil);
}

- (void)  URLSession:(NSURLSession *)session
        downloadTask:(NSURLSessionDownloadTask *)downloadTask
   didResumeAtOffset:(int64_t)fileOffset
  expectedTotalBytes:(int64_t)expectedTotalBytes
{
}

- (void)         URLSession:(NSURLSession *)session
               downloadTask:(NSURLSessionDownloadTask *)downloadTask
               didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten
  totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    
}




float roundToN(float num, int decimals)
{
    int tenpow = 1;
    for (; decimals; tenpow *= 10, decimals--);
    return round(tenpow * num) / tenpow;
}

+ (void)setLogFileNameString
{
    logFileNameString= [NSString stringWithFormat:@"log%d.txt", (int)[NSDate timeIntervalSinceReferenceDate]];
}

+ (void) appendLogString:(NSString*)string
{
    [logString appendString:string];
}

+ (void) writeAndSaveLogString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName=logFileNameString;
    //    [NSString stringWithFormat:@"log%d.txt", (int)[NSDate timeIntervalSinceReferenceDate]];
    NSString *filePath=[documentsDirectory stringByAppendingPathComponent:fileName];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:nil])
    {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    [logString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    //    [[[UIAlertView alloc] initWithTitle:@"GLF. Locker" message:@"Log File Saved" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
}

+ (void) resetLogString
{
    logString=[[NSMutableString alloc] init];
    [GlobalClass setLogFileNameString];
}

+ (void) writeLocationToLogFile:(NSString*)logToWrite
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath=[documentsDirectory stringByAppendingPathComponent:logFileNameString];
    
    NSError *error=nil;
    NSMutableString *logString=nil;//[NSMutableString string];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:nil])
    {
        logString=[NSMutableString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    }
    else
    {
        [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
        logString=[NSMutableString string];
    }
    
    [logString appendString:[NSString stringWithFormat:@"\n\n%@",logToWrite]];
    [logString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
}


#pragma mark - Background Session Methods

- (NSURLSession*) createBackgroundSessionWithID:(NSString*)sessionID
{
    NSString *identifier = [NSString stringWithFormat:@"%@_%@", kBackgroundUploadIdentifier, sessionID];
    return [self createBackgroundSessionWithIdentifier:identifier];
}

- (NSURLSession*) createBackgroundSessionWithIdentifier:(NSString*)sIdentifier
{
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"createBackgroundSessionWithIdentifier: %@, %@", sIdentifier, [NSDate date]]];
    
    NSString *identifier = sIdentifier;
    if(!identifier)
    {
        identifier=[NSString stringWithFormat:@"%@_%@", kBackgroundUploadIdentifier, @((int)[NSDate date].timeIntervalSince1970)];
    }
    
    if(![self sessionIdentifierAlreadyExists:identifier])
    {
        if(self.backgroundSessions==nil)
        {
            self.backgroundSessions=[NSMutableArray array];
        }
        
        NSURLSession *backgroundSession = [BLBackgroundSessionDelegate createBackgroundURLSessionWithId:identifier shouldWifiSyncOnly:NO delegate:self.backgroundSessionDelegate];
        [self addSession:backgroundSession];
        return backgroundSession;
    }
    else
    {
        NSURLSession *backgroundSession=nil;
        for(NSURLSession *session in backgroundSessions)
        {
            if([session.configuration.identifier isEqualToString:identifier])
            {
                backgroundSession=session;
                break;
            }
        }
        return backgroundSession;
    }
}

- (void) resetBackgroundSession
{
    NSArray *arr=[[NSUserDefaults standardUserDefaults] objectForKey:kBackgroundUploadIdentifiersArrayKey];
    if(arr && arr.count>0)
    {
        for(NSString *identifier in arr)
        {
            [self createBackgroundSessionWithIdentifier:identifier];
        }
    }
}

- (void) addSession:(NSURLSession*)sessionToAdd
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    if(![backgroundSessions containsObject:sessionToAdd])
    {
        [backgroundSessions addObject:sessionToAdd];
        //        [userDefaults setObject:backgroundSessions forKey:kBackgroundUploadSessionsArrayKey];
        //        [userDefaults synchronize];
    }
    
    BOOL identifierExists=NO;
    NSArray *arr=[self savedSessionIdentifiers];
    if(arr && arr.count>0)
    {
        for(NSString *identifier in arr)
        {
            if([identifier isEqualToString:sessionToAdd.configuration.identifier])
            {
                identifierExists=YES;
            }
        }
    }
    
    if(!identifierExists)
    {
        NSMutableArray *sessionsIdentifiers=[NSMutableArray array];
        for(NSURLSession *session in backgroundSessions)
        {
            [sessionsIdentifiers addObject:session.configuration.identifier];
        }
        
        [userDefaults setObject:sessionsIdentifiers forKey:kBackgroundUploadIdentifiersArrayKey];
        [userDefaults synchronize];
    }
}

- (BOOL) sessionIdentifierAlreadyExists:(NSString*)sessionIdentifier
{
    BOOL identifierExists=NO;
    NSArray *arr=[self savedSessionIdentifiers];
    if(arr && arr.count>0)
    {
        for(NSString *identifier in arr)
        {
            if([identifier isEqualToString:sessionIdentifier])
            {
                identifierExists=YES;
            }
        }
    }
    
    BOOL sessionExists=NO;
    for(NSURLSession *session in backgroundSessions)
    {
        if([session.configuration.identifier isEqualToString:sessionIdentifier])
        {
            sessionExists=YES;
        }
    }
    
    
    return (identifierExists && sessionExists);
}

- (NSArray*) savedSessionIdentifiers
{
    NSArray *arr=[[NSUserDefaults standardUserDefaults] objectForKey:kBackgroundUploadIdentifiersArrayKey];
    return arr;
}

- (void) removeSession:(NSURLSession*)sessionToRemove
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSMutableArray *sessionsIdentifiers=[NSMutableArray array];
    
    if(backgroundSessions.count>0  && [backgroundSessions containsObject:sessionToRemove])
    {
        [backgroundSessions removeObject:sessionToRemove];
        //        [userDefaults setObject:backgroundSessions forKey:kBackgroundUploadSessionsArrayKey];
        //        [userDefaults synchronize];
        
        for(NSURLSession *session in backgroundSessions)
        {
            [sessionsIdentifiers addObject:session.configuration.identifier];
        }
    }
    else
    {
        NSArray *arr=[self savedSessionIdentifiers];
        if(arr && arr.count>0)
        {
            for(NSString *identifier in arr)
            {
                if(![identifier isEqualToString:sessionToRemove.configuration.identifier])
                {
                    [sessionsIdentifiers addObject:identifier];
                }
            }
        }
    }
    
    [userDefaults setObject:sessionsIdentifiers forKey:kBackgroundUploadIdentifiersArrayKey];
    [userDefaults synchronize];
}


+ (NSString*) getJSONDictionaryWithDictionary:(NSDictionary*)requestOptions
{
    NSError* error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:requestOptions
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (error != nil) {
        NSLog(@"NSDictionary JSONString error: %@", [error localizedDescription]);
        return nil;
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

+ (NSString*) getTimeStringFromSeconds:(int)secnds
{
    int minutes=secnds/60;
    int seconds=secnds%60;
    
    NSString *minString=[NSString stringWithFormat:@"%d",minutes];
    if(minutes<10)
        minString=[NSString stringWithFormat:@"0%d",minutes];
    
    NSString *secString=[NSString stringWithFormat:@"%d",seconds];
    if(seconds<10)
        secString=[NSString stringWithFormat:@"0%d",seconds];
    
    return [NSString stringWithFormat:@"%@:%@", minString, secString];
}


#pragma mark - VideoUploading CallBacks

- (void)videoUploadingStarted:(NSString *)callBackID {
    
    
    [Utility showAlertViewWithTitle:@"Alert" message:@"Upload has started"];
}

- (void)videoUploadingResumed:(NSString *)callBackID {
    
}

- (void)videoUploadingFailedwithError:(NSString *)errorMessage andCallBackID:(NSString *)callBackID {
    
}

- (void)videoUploadingCancelled:(NSString *)callBackID {
    
}

- (void)videoUploadingStopped:(NSString *)callBackID {
    
}


#pragma mark - UploadProgessVC Methods

- (void)setUploadProgressViewController:(UIViewController *)vc {
    uploadProgressVC = (UploadProgressViewController *)vc;
}

- (UIViewController *)getUploadProgressViewController {
    return (UploadProgressViewController *)uploadProgressVC;
}


#pragma mark - Orientation Methods

- (void)setShouldAllowLandscape:(BOOL)shouldAllowLandscape {
    _shouldAllowLandscape = shouldAllowLandscape;
    
    if (!_shouldAllowLandscape) {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
}

//#pragma mark - Notification Message parsing
+(NSString*)parsingNotificationMessage:(NSDictionary*)userInfo{
    
    
    NSString *message = nil;
    id aps=[userInfo objectForKey:@"aps"];
    if(aps!=nil)
    {
        id alert = [aps objectForKey:@"alert"];
        if ([alert isKindOfClass:[NSString class]]) {
            message = alert;
        } else if ([aps isKindOfClass:[NSDictionary class]]) {
            message = [alert objectForKey:@"body"];
        }
    }
    else
    {
        id alert = [userInfo objectForKey:@"alert"];
        if ([alert isKindOfClass:[NSString class]]) {
            message = alert;
        } else if ([alert isKindOfClass:[NSDictionary class]]) {
            message = [alert objectForKey:@"body"];
        }
    }
    
    if(message==nil)
    {
        message=@"Notification received.";
    }
    return message;
    
}

@end
