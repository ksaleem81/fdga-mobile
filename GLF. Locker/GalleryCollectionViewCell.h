//
//  GalleryCollectionViewCell.h
//  Orbis
//
//  Created by Nasir Mehmood on 8/24/15.
//
//

#import <UIKit/UIKit.h>

@interface GalleryCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *movieInfoView;
@property (weak, nonatomic) IBOutlet UILabel *movieDurationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
