//
//  UploadManager.m
//  sibme
//
//  Created by Nasir Mehmood on 3/21/13.
//
//

#import "UploadManager.h"
//#import "MainViewController.h"
//#import "AppDelegate.h"
#import "Reachability.h"
//#import <Cordova/CDVPluginResult.h>
#import "GlobalClass.h"

#define kCellularDataSettingChangedNotification @"cellularDataSettingSchangedNotification"


static UploadManager *uploadManager=nil;

@implementation UploadManager

@synthesize isUploading, uploadRequestsDictionary;

+ (UploadManager*) sharedManager
{
    if(!uploadManager)
    {
        uploadManager=[[UploadManager alloc] init];
    }
    return uploadManager;
}

- (id) init
{
    self=[super init];
    if(self)
    {
        isUploading=NO;
        self.uploadRequestsDictionary=[[NSMutableDictionary alloc] init];
        
        Reachability *reachability=[Reachability reachabilityForInternetConnection];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        [reachability startNotifier];
        [self startPendingUploads];
        
    }
    
    return self;
}

- (void) reachabilityChanged:(NSNotification*)note
{
    Reachability *reachability=[Reachability reachabilityForInternetConnection];
    if(!reachability.isReachable)
    {
        if([self isUploading])
        {
            [self stopUploadingAllVideos];
        }
    }
}

- (void) stopUploadingAllVideos
{
    for(UploadRequest *request in uploadRequestsDictionary.allValues)
    {
        if(request.isUploading)
        {
            [request stopUploading];
        }
    }
}

- (int) getRequestsCount
{
    return (int)[uploadRequestsDictionary count];
}

- (BOOL) isUploading
{
    for(UploadRequest *request in uploadRequestsDictionary.allValues)
    {
        if(request.isUploading)
            return YES;
    }
    
    return NO;
}

#pragma mark - 

- (void) addUploadingMediaWithDictionary:(NSMutableDictionary*) options
{
//    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
    
    Reachability* internetReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStat = [internetReach currentReachabilityStatus];
    
    if (netStat==NotReachable)
    {
        // CHM Jawad
//        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Internet is not available."];
//        [mvc.commandDelegate sendPluginResult:result callbackId:[options objectForKey:@"callBackID"]];
        [[GlobalClass sharedInstance] videoUploadingFailedwithError:@"Internet is not available." andCallBackID:[options objectForKey:@"callBackID"]];
    }
    else
    {
        UploadRequest *uploadRequest=[[UploadRequest alloc] initWithOptions:options uploadRequestDelegate:self];
        [uploadRequestsDictionary setObject:uploadRequest forKey:uploadRequest.uniqueMediaID];
    }
}

- (void) startPendingUploads
{
    NSArray *uploadRequests=[self getRecentUploadsList];
    if(uploadRequests.count>0)
    {
       for(NSDictionary *d in uploadRequests)
       {
           [self addUploadingMediaWithDictionary:[d mutableCopy]];
       }
    }
}

- (void) resumeUploadingMediaWithDictionary:(NSMutableDictionary*) options
{
    NSString *uniqueMediaID=[options objectForKey:@"uniqueMediaID"];
    UploadRequest *r=[uploadRequestsDictionary objectForKey:uniqueMediaID];
    if(r)
    {
        [r startUploading];

        
    }
    
    // CHM Jawad
//    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    
//    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:options];
//    [mvc.commandDelegate sendPluginResult:result callbackId:[options objectForKey:@"callBackID"]];    
    [[GlobalClass sharedInstance] videoUploadingResumed:[options objectForKey:@"callBackID"]];
    [self saveInfoOfAllUploadingVideos];
}

- (void) startUploadingMediaWithDictionary:(NSMutableDictionary*) options
{
//    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
    
    Reachability* internetReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStat = [internetReach currentReachabilityStatus];
    
    if (netStat==NotReachable)
    {
        // CHM Jawad
//        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Internet is not available."];
//        [mvc.commandDelegate sendPluginResult:result callbackId:[options objectForKey:@"callBackID"]];
        [[GlobalClass sharedInstance] videoUploadingFailedwithError:@"Internet is not available." andCallBackID:[options objectForKey:@"callBackID"]];
    }
    else
    {
        UploadRequest *uploadRequest = [[UploadRequest alloc] initWithOptions:options uploadRequestDelegate:self];
//        [uploadRequest startRequest];
        [uploadRequest startUploading];
        [uploadRequestsDictionary setObject:uploadRequest forKey:uploadRequest.uniqueMediaID];
        [self saveInfoOfAllUploadingVideos];
    }
}




- (void) stopUploadingMediaWithDictionary:(NSMutableDictionary*) options
{

    NSString *uniqueMediaID=[options objectForKey:@"uniqueMediaID"];
    UploadRequest *r=[uploadRequestsDictionary objectForKey:uniqueMediaID];
    if(r)
    {
        [r stopUploading];
    }
    
    // CHM Jawad
//    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    
//    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:options];
//    [mvc.commandDelegate sendPluginResult:result callbackId:[options objectForKey:@"callBackID"]];
    [[GlobalClass sharedInstance] videoUploadingStopped:[options objectForKey:@"callBackID"]];
}

- (void) cancelUploadingMediaWithDictionary:(NSMutableDictionary*) options
{
    NSString *uniqueMediaID=[options objectForKey:@"uniqueMediaID"];
    UploadRequest *r=[uploadRequestsDictionary objectForKey:uniqueMediaID];
    if(r)
    {
        [r cancelRequest];
        [uploadRequestsDictionary removeObjectForKey:uniqueMediaID];
    }
    
    // CHM Jawad
//    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//    MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//    
//    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:options];
//    [mvc.commandDelegate sendPluginResult:result callbackId:[options objectForKey:@"callBackID"]];
    [[GlobalClass sharedInstance] videoUploadingCancelled:[options objectForKey:@"callBackID"]];
}

- (BOOL) anyRequestUploading
{
    BOOL anyRequestUploading=NO;
    for(UploadRequest *request in uploadRequestsDictionary.allValues)
    {
//        if(request.requestStatus!=UploadRequestStatusPaused && request.requestStatus!=UploadRequestStatusNotStarted && request.requestStatus!=UploadRequestStatusSuspended && request.requestStatus!=UploadRequestStatusUploaded)
        if(request.requestStatus==UploadRequestStatusStarted)
        {
            anyRequestUploading=YES;
        }
    }
    
    return anyRequestUploading;
}

- (void) startUploadingNextVideoInQueue
{
    if([self anyRequestUploading])
    {
        return;
    }
    
    for(UploadRequest *request in uploadRequestsDictionary.allValues)
    {
        if(request.requestStatus==UploadRequestStatusNotStarted)
        {
            [request startUploading];
//            [request updatePogressWithVideoInfo:@{kMethodNameKey:kMethodNameUploadChunk} didSendData:0 totalBytesWritten:0 totalBytesExpectedToWrite:0 ignoreUpdateCondition:YES];
            break;
        }
    }
}


#pragma mark - UploadRequestsDelegate Methods

- (void) uploadRequestCompleted:(UploadRequest*)uploadRequest
{
    [uploadRequestsDictionary removeObjectForKey:uploadRequest.uniqueMediaID];
    [self saveInfoOfAllUploadingVideos];
}


- (void) uploadRequestFailed:(UploadRequest*)uploadRequest
{
    [uploadRequestsDictionary removeObjectForKey:uploadRequest.uniqueMediaID];
    [self saveInfoOfAllUploadingVideos];
}

- (void) uploadRequestStopped:(UploadRequest*)uploadRequest
{
//    [uploadRequestsDictionary removeObjectForKey:uploadRequest.uniqueMediaID];
}

#pragma mark -

-(void)requestWithMediaInfo:(NSDictionary*)videoInfo didCompleteWithResponse:(NSHTTPURLResponse *)response responseData:(NSData*)responseData{
    NSString *videoID=[videoInfo objectForKey:kMediaIDKey];
    UploadRequest *request=[uploadRequestsDictionary objectForKey:videoID];
    if(request)
    {
        [request uploadingCompletedWithResponse:response responseData:responseData mediaInfo:videoInfo];
    }
    
    [self saveInfoOfAllUploadingVideos];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self isUploading])
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
    });
}

-(void)requestWithMediaInfo:(NSDictionary*)videoInfo didSendData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite{
    
    NSLog(@"didSendData called: %lld - %lld / %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    NSString *videoID=[videoInfo objectForKey:kMediaIDKey];
//    NSString *fileName=[videoInfo objectForKey:kFileNameKey];
    
//    if([fileName.pathExtension caseInsensitiveCompare:@"png"]!=NSOrderedSame)
//    {
        double progress = ((double)totalBytesWritten/(double)totalBytesExpectedToWrite)*100;
        NSLog(@"percent: %@",[NSString stringWithFormat:@"%.2f%%", progress]);
        UploadRequest *request=[uploadRequestsDictionary objectForKey:videoID];
        if(request)
        {
            [request updatePogressWithMediaInfo:videoInfo didSendData:bytesWritten totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
        }
//    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler{
}

-(void)requestWithMediaInfo:(NSDictionary*)videoInfo didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError called: %@", error);
    
    NSString *videoID=[videoInfo objectForKey:kMediaIDKey];
    UploadRequest *request=[uploadRequestsDictionary objectForKey:videoID];
    if(request)
    {
        [request uploadingFailedWithError:error mediaInfo:videoInfo];
    }
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{

    NSLog(@"%@",error);

}



#pragma mark

- (NSArray*) getRecentUploadsList
{
    NSArray *recentUploads=nil;
    recentUploads=[NSArray arrayWithContentsOfFile:[self getUploadRecordsFilePath]];
    if(!recentUploads)
        recentUploads=[NSArray array];
    return recentUploads;
}

- (NSString*) getUploadRecordsFilePath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath=[documentsDirectory stringByAppendingPathComponent:@"uploadrecords.plist"];
    
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    return filePath;
}


- (void) truncateFile
{
    NSFileHandle *file;
    file = [NSFileHandle fileHandleForUpdatingAtPath: [self getUploadRecordsFilePath]];
    
    if (file == nil)
    {
        NSLog(@"Failed to open file");
    }
    
    [file truncateFileAtOffset: 0];
    [file closeFile];
}


- (void) saveInfoOfAllUploadingVideos{
    
    [self truncateFile];
    NSMutableArray *arrayToWrite=[NSMutableArray array];
    
    for(UploadRequest *request in uploadRequestsDictionary.allValues)
    {
        NSLog(@"%@",request.requestOptions);
        NSDictionary *requestOptions=request.requestOptions;
        [arrayToWrite addObject:requestOptions];
    }
    [arrayToWrite writeToFile:[self getUploadRecordsFilePath] atomically:YES];
}





@end






