//
//  RegisterViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/28/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import ActionSheetPicker_3_0
class RegisterViewController: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var fnameFld: UITextField!
    @IBOutlet weak var lnameFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var dobFld: UITextField!
    @IBOutlet weak var mobileFld: UITextField!
    @IBOutlet weak var userNameFld: UITextField!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var parentFld: UITextField!
    @IBOutlet weak var termsViewHeightCons: NSLayoutConstraint!//450 - 85 for terms remove
    var emailBtnFlage = false
    var privacyBtnFlage = false
    var underAgeFlage = false
    var authorizeFlage = false
    @IBOutlet weak var childTermViewHeightCons: NSLayoutConstraint!//168
    @IBOutlet weak var internalContentHeightCons: NSLayoutConstraint!
    //MARK:- life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dobFld.placeholder = "Date Of Birth"
        parentFld.isHidden = true
        self.childTermViewHeightCons.constant = 0
        internalContentHeightCons.constant = 380

    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- buttons action
    @IBAction func privacyBtnAction(_ sender: Any) {
        
                var titleAcademy = ""
                if let titl =  DataManager.sharedInstance.currentAcademy()!["PrivacyPolicy"] as? String{
                    privacyTermsPDF = titl
                }
        //        privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
                
        //        privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"
        //        https://moorparkacademy.firstdegree.golf/privacy-policy/
                UIApplication.shared.openURL(NSURL(string:privacyTermsPDF)! as URL)
                dismiss(animated: true, completion: nil)

    }
    @IBAction func pdfTermsBtnAction(_ sender: UIButton) {
        
        
        showingTT()
        
    }
    
    @IBAction func ageBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            
            sender.isSelected = false
            underAgeFlage = false
            parentFld.isHidden = true
            internalContentHeightCons.constant = 430
            
            view.layoutIfNeeded()
            
            UIView.animate(withDuration: 1.0, animations: {
                self.childTermViewHeightCons.constant = 45
                self.view.layoutIfNeeded()
            })
            
        }else{
            
            sender.isSelected = true
            underAgeFlage = true
            parentFld.isHidden = false
            internalContentHeightCons.constant = 548
            view.layoutIfNeeded()
            UIView.animate(withDuration: 1.0, animations: {
                self.childTermViewHeightCons.constant = 168
                self.view.layoutIfNeeded()
            })
            
        }
        
    }
    
    @IBAction func authorizeBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            authorizeFlage = false
        }else{
            sender.isSelected = true
            authorizeFlage = true
        }
        
    }
    
    @IBAction func emailBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            emailBtnFlage = false
        }else{
            sender.isSelected = true
            emailBtnFlage = true
        }
        
    }
    
    @IBAction func conditionsBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            privacyBtnFlage = false
        }else{
            sender.isSelected = true
            privacyBtnFlage = true
        }
    }
    
    @IBAction func dobBtnAction(_ sender: UIButton) {
        
       let formator = DateFormatter()
       let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            let title = formator.string(from: date as Date)
            self.dobFld.text = title
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        
        picker?.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        picker?.show()
    
    }
    
    func showingTT()  {
        
        var titleAcademy = ""
                        if let titl =  DataManager.sharedInstance.currentAcademy()!["Terms"] as? String{
                            titleAcademy = titl
                        }
                        
            //            privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
        //                privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"

            UIApplication.shared.openURL(NSURL(string:titleAcademy)! as URL)
                        self.parent?.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func registerBtnAction(_ sender: UIButton) {
        
        messageString = ""
        valiDateRegistrationFields()
        
        if messageString.count > 0{
            let name: String = messageString
            let truncated = name.substring(to: name.index(before: name.endIndex))
            
            let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        registerStudent()
    }
    
    var messageString : String = ""
    func valiDateRegistrationFields(){
        
        if let text = fnameFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter First Name\n"
        }
        
        if let text = lnameFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter Last Name\n"
        }
        

        if let text = emailFld.text , !text.isEmpty{
            if  DataManager.sharedInstance.isValidEmail(self.emailFld.text!){
            }else{
                 messageString = messageString + "Enter Valid Email\n"
            }
        }else {
            messageString = messageString + "Enter Email\n"
        }
        if let text = mobileFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter Mobile Number\n"
        }
        if let text = userNameFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter UserName\n"
        }
        if let text = passwordFld.text , !text.isEmpty{
               messageString = Utility.passwordStrength(passwordString: passwordFld.text!, messageString1: messageString)
        }else {
            messageString = messageString + "Enter Password\n"
        }
        
        //GDPR
        if privacyBtnFlage{

        }else{
            messageString = messageString + "Please Agree Terms & Condition Check\n\n"
        }
        

    }
    
    //method for check of password strength
    func registerStudent()  {
        
        var dob = ""
        if let text = dobFld.text , !text.isEmpty{
             dob = dobFld.text!
             dob = DataManager.sharedInstance.getFormatedDateForJsonMonthFirst(date: dob,formate: "dd MMM, yyyy")
        }else {
            dob = DataManager.sharedInstance.getTodayDate()
            dob = DataManager.sharedInstance.getFormatedDateForJsonMonthFirst(date: dob, formate: "yyyy-MM-dd")
        }
        
        let userNameTxt = userNameFld.text!.trimmingCharacters(in: .whitespacesAndNewlines)

//  print(["firstName":fnameFld.text! as AnyObject,"lastName":lnameFld.text! as AnyObject,"DataOfBirth":dob as AnyObject,"WorkEmail":emailFld.text! as AnyObject,"Mobile":mobileFld.text! as AnyObject,"UserName":userNameFld.text! as AnyObject,"Password":passwordFld.text! as AnyObject,"AcademyID":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"AgreeTerms": "\(privacyBtnFlage)" as AnyObject!,"RecEmail": "\(emailBtnFlage)" as AnyObject!,"IsUnderEighteen":"\(underAgeFlage)" as AnyObject!,"ParentApproval":"\(authorizeFlage)" as AnyObject!,"ParentName":parentFld.text! as AnyObject,"ChildDob":dob as AnyObject ,"IsnNewUser":"0" as AnyObject])
        
        NetworkManager.performRequest(type:.post, method: "Login/RegisterStudent_native", parameter: ["firstName":fnameFld.text! as AnyObject,"lastName":lnameFld.text! as AnyObject,"DataOfBirth":dob as AnyObject,"WorkEmail":emailFld.text! as AnyObject,"Mobile":mobileFld.text! as AnyObject,"UserName":userNameFld.text! as AnyObject,"Password":passwordFld.text! as AnyObject,"AcademyID":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"AgreeTerms": "\(privacyBtnFlage)" as AnyObject!,"RecEmail": "\(emailBtnFlage)" as AnyObject!,"IsUnderEighteen":"\(underAgeFlage)" as AnyObject!,"ParentApproval":"\(authorizeFlage)" as AnyObject!,"ParentName":parentFld.text! as AnyObject,"ChildDob":dob as AnyObject ,"IsnNewUser":"0" as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            case _ as [[String:AnyObject]]: break
            default:
                break
                
            }
         
            let data = object as? [[String:AnyObject]]
            if  ((data?.count)! > 0)  {
                if  let result = data?[0]["Status"] as? String{
                    if result == "1"{
                    _ = self.navigationController?.popViewController(animated:true)
                    DataManager.sharedInstance.printAlertMessage(message:"Successfully Registered", view:UIApplication.getTopestViewController()!)
                    }else if result == "2"{
                        
                          DataManager.sharedInstance.printAlertMessage(message:"User Already exist!", view:self)
                    }
                }
            }else{
                  DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
}
