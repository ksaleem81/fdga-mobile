//
//  OrbisHelper.m
//  GLF. Locker
//
//  Created by Jawad Ahmed on 08/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

#import "OrbisHelper.h"

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

@implementation OrbisHelper

+ (void)getThumbnailOfVideo:(NSString *)videoPath completion:(void(^)(NSError *error, NSDictionary *info))completion
{
    if(videoPath)
    {
        NSError *error=nil;
        CMTime actualTime;
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:videoPath]];
        AVAssetImageGenerator   *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform=TRUE;
        CGImageRef halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
        
        while (halfWayImage  == nil) {
            halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
        }
        
        UIImage  *thumbImage = [[UIImage alloc] initWithCGImage:halfWayImage];
        
        
        NSString *thumbPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_thumb.jpg", videoPath.lastPathComponent.stringByDeletingPathExtension]];
        NSData *thumbImageData=UIImageJPEGRepresentation(thumbImage, 1.0);
        [thumbImageData writeToFile:thumbPath atomically:YES];
        
        if(error)
        {
            completion(error, nil);
        }
        else
        {
            completion(nil, @{@"thumbPath": thumbPath});
        }
    }
}

@end
