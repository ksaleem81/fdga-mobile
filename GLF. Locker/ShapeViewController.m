

#import "ShapeViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "OverlaysViewController.h"
#import "OrbisMediaGallery.h"
//#import "AppDelegate.h"
//#import "MainViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"
#import "CRDScrollSwitch.h"
#import "UIImage+Trim.h"
#import "UIImage+fixOrientation.h"
#import "MDrawView.h"
#import "MDrawAngle.h"
#import <SideMenu/SideMenu-Swift.h>
#import <AVKit/AVKit.h>



@interface ShapeViewController ()<UIGestureRecognizerDelegate, UIActionSheetDelegate>
{
    NSMutableArray *shapesArray;
    __weak IBOutlet UIView *_bottomMenuContainerView;
    
    SPUserResizableView *_currentlyEditingView;
    SPUserResizableView *_lastEditedView;
    
    __weak IBOutlet UIImageView *_thumbnailImageView;
    //    MPMoviePlayerController *moviePlayerController;
//    deprecations
//    MPMoviePlayerViewController *moviePlayerViewController;
    AVPlayerViewController *moviePlayerViewController2;

    NSDictionary *_videoInfo;
    __weak IBOutlet UIButton *snapshotButton;
    
    AVURLAsset *_asset;
    AVAssetImageGenerator *_imageGenerator;
    
    UIActionSheet *actionSheet;
    __weak IBOutlet UIView *playbackControlsView;
    
    float currentPlaybackRate;
    
    
    __weak IBOutlet UIButton *playPuaseButton;
    __weak IBOutlet UISlider *movieSlider;
    
    __weak IBOutlet UIButton *previousFrameButton;
    __weak IBOutlet UIButton *nextFrameButton;
    __weak IBOutlet UILabel *movieProgressLabel;
    __weak IBOutlet UILabel *movieDurationLabel;
    
    UIImageView *playbackImageView;
    NSTimer *playbackTimer;
    float currentPlaybackTime;
    
    __weak IBOutlet UIButton *overlaysButton;
    NSMutableArray *analysisScreenTimes;
    __weak IBOutlet CRDScrollSwitch *pnvideoFramesToggleButton;
    NSString *mediaFolderPath;
  //  IBOutlet UIView* _mainContainerView;
   
  __weak  IBOutlet MDrawView *_mainContainerView;
    NSMutableArray *angleObjs;
   // BOOL isVideoAnalysisDone;
    UIInterfaceOrientation currentOrientation ;
    
    int totalCount  ;
    
}
@end



@implementation ShapeViewController

- (id) initWithVideoInfo:(NSDictionary*)videoInfo{
    [GlobalClass sharedInstance].shouldAllowLandscape = YES;
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    deviceHeight = iOSDeviceScreenSize.height;
    deviceWidth = iOSDeviceScreenSize.width;
    
    totalCount = 0;
      [_mmDrawerController setIsVideoAnalysisDone:NO]  ;
    
    CGRect  windowFrame =  [[[UIApplication sharedApplication]keyWindow] frame];
    NSLog(@"windiw width %f window height %f",windowFrame.size.width, windowFrame.size.height );
    
    //    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
    if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight )))  {
        if(  deviceWidth >= 568 || deviceHeight >= 568 ) {
            
            self =[self initWithNibName:@"ShapeViewController_Landscape" bundle:nil];
        }
        else
        {
           self =  [self initWithNibName:@"ShapeViewController_Landscape_4s" bundle:nil];
        }
    }
    
    else  {
        
        
        if(  IS_IPHONE_5   || IS_IPHONE_6 || IS_IPHONE_6_PLUS)   {
            
            self =[self initWithNibName:@"ShapeViewController_5" bundle:nil];
        }
        else
        {
            
            self =  [self initWithNibName:@"ShapeViewController" bundle:nil];
        }
        
        
    }
    
    
    [self performSelector:@selector(reloadView) withObject:nil afterDelay:0.1];
    
    if(self){
        _videoInfo=videoInfo;
    }
    //  [self reloadView];
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadView)
                                                     name:UIApplicationDidChangeStatusBarOrientationNotification
                                                   object:nil];
       [self showStatusBar];
    }
    

    if (angleObjs == nil) {
        angleObjs =[[NSMutableArray alloc]init];
    }
    
   //  _mainContainerView.showMeasurement  = YES;
    // initialize shape images array
    shapeImages = [[NSMutableArray alloc]init];
    
    //    if (shapeViewImageA == nil) {
    //         shapeViewImageA = [[NSMutableArray alloc]init];
    //    }
    
    if (dicA == nil) {
        dicA = [[NSMutableArray alloc]init];
    }
    
    
    yOrigin = superView.frame.origin.y;
    
    isViewHidden = NO;
    
    UITapGestureRecognizer *showViews = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showHideView)];
    showViews.numberOfTapsRequired = 1;
    
    
//    if( Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() ){
//            NSLog(@"playback rate: %f", [[UIScreen mainScreen] bounds].size.height);

    if (IS_IPHONE_X || IS_IPHONE_XMAX || IS_IPHONE_XR){

        _mainContainerView.frame = CGRectMake(0, 40,self.view.frame.size.width
                                              , self.view.frame.size.height - 94);
      

    }

    [_mainContainerView setColor:[UIColor redColor]];
    [_mainContainerView  addGestureRecognizer:showViews];
    showViews.delegate = (id)self;
    
    /*
     //change device orientation
     [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
     
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
     
     orientation = [[UIDevice currentDevice] orientation];
     
     if (orientation == UIDeviceOrientationUnknown || orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown) {
     
     orientation = UIDeviceOrientationPortrait;
     
     }
     
     
     */
    // _currentlyEditingView=nil;
    // _lastEditedView=nil;
    //    moviePlayerController=nil;
    
//    deprecations
   // moviePlayerViewController=nil;
    moviePlayerViewController2 = nil;
    _asset=nil;
    _imageGenerator=nil;
    actionSheet=nil;
    
    self.view.backgroundColor=[UIColor blackColor];
    
    NSLog(@"%@",_videoInfo);
    
    if(_videoInfo)
    {
        mediaFolderPath=[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath];
        NSString  *urlSt = [mediaFolderPath stringByAppendingPathComponent:[[_videoInfo objectForKey:@"videoPath"] lastPathComponent]];
        NSURL *movieURL=[NSURL fileURLWithPath:urlSt];
   //     NSURL *movieURL = [NSURL URLWithString:@"http://app.glflocker.com/Content/MediaFiles/Videos/GLF-1439480442-14243-0-1_Flip.mp4"];
        _asset = [AVURLAsset assetWithURL:movieURL];
        _imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:_asset];
        _imageGenerator.requestedTimeToleranceBefore=kCMTimeZero;
        _imageGenerator.requestedTimeToleranceAfter=kCMTimeZero;
        
        
        NSArray *analysisScreens=[_videoInfo objectForKey:@"analysisScreens"];
        
        
        if(analysisScreens && analysisScreens.count>0)
        {
            [self videoScreensReadyForAnalysis];
        }
        else
        {
            [self performSelectorInBackground:@selector(generateScreensForAnalysis) withObject:nil];
        }
        
        if (shapesArray == nil) {
            shapesArray=[NSMutableArray array];
        }
        else
        {
            //  [self setThumgImgFrame];
            [self performSelector:@selector(displayLayers) withObject:nil afterDelay:0.5];
            
            //              [self performSelector:@selector(setImageOverlayFrame) withObject:nil afterDelay:0.5];
        }
        
        
        
        if (stopVideoFunc != 1) {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(moviePlaybackStateDidChange:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:nil];
            
        }
        
        
        
        /*
         moviePlayerController=[[MPMoviePlayerController alloc] initWithContentURL:movieURL];
         [moviePlayerController prepareToPlay];
         
         //        float f=moviePlayerController.currentPlaybackRate;
         //        NSLog(@"playback rate: %f", f);
         //        moviePlayerController.currentPlaybackRate=0.25f;
         //        f=moviePlayerController.currentPlaybackRate;
         //        NSLog(@"playback rate: %f", f);
         
         //        moviePlayerController.repeatMode=MPMovieRepeatModeOne;
         moviePlayerController.controlStyle=MPMovieControlStyleEmbedded;
         moviePlayerController.shouldAutoplay=NO;
         [moviePlayerController.view setFrame:_thumbnailImageView.frame];
         moviePlayerController.backgroundView.backgroundColor=[UIColor blackColor];
         [self.view addSubview:moviePlayerController.view];
         
         
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(moviePlaybackStateDidChange:)
         name:MPMoviePlayerPlaybackStateDidChangeNotification
         object:nil];
         
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(moviePlayBackDidFinish:)
         name:MPMoviePlayerPlaybackDidFinishNotification
         object:nil];
         */
        
        /*
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(moviePlayerWillEnterFullScreen:)
         name:MPMoviePlayerWillEnterFullscreenNotification
         object:nil];
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(moviePlayerDidEnterFullScreen:)
         name:MPMoviePlayerDidEnterFullscreenNotification
         object:nil];
         */
        /*
         if([_videoInfo objectForKey:@"lessonID"] && [[_videoInfo objectForKey:@"lessonID"] integerValue]>0)
         {
         //            [uploadButton setAlpha:1.0];
         }
         else
         {
         //            [uploadButton setAlpha:0.4];
         }
         */
        
    }
    
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles)];
    [gestureRecognizer setDelegate:self];
    [_mainContainerView addGestureRecognizer:gestureRecognizer];
    //
    //    gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles)];
    //    [gestureRecognizer setDelegate:self];
    //    [moviePlayerController.view addGestureRecognizer:gestureRecognizer];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleThumbnailImageRequestFinishNotification:) name:MPMoviePlayerThumbnailImageRequestDidFinishNotification object:nil];
    
    
    UIBarButtonItem * rightDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"lines_3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
    
    UIBarButtonItem * leftDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arr_left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    leftDrawerButton.imageInsets=UIEdgeInsetsMake(2, 0, 0, 0);
    
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:147.0/255.0 green:149.0/255.0 blue:151.0/255.0 alpha:1.0]];
    
    // CGRect frame=self.navigationController.navigationBar.frame;
    // frame.size.height=64.0;
    // self.navigationController.navigationBar.frame=frame;
    
    self.navigationItem.title=@"Video Analysis";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    currentPlaybackRate=1.0;
    
    [pnvideoFramesToggleButton.slider removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [pnvideoFramesToggleButton.slider addTarget:self
                                         action:@selector(changeSwitchState:)
                               forControlEvents:UIControlEventTouchUpInside];
    [pnvideoFramesToggleButton.onText setText:@"Video"];
    [pnvideoFramesToggleButton.offText setText:@"Frames"];
    [pnvideoFramesToggleButton setOn:YES];
    //    [pnvideoFramesToggleButton.slider addTarget:self action:@selector(toggleContactSortOrder:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //    playbackControlsView.layer.cornerRadius=5.0f;
    //    overlaysButton.layer.cornerRadius=5.0;
    
    // check device orientation change
    //  [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    
    
    
}



-(void)changeSwitchState:(id)sender
{
    [pnvideoFramesToggleButton setOn:![pnvideoFramesToggleButton isOn]];
    
    [pnvideoFramesToggleButton.slider sendActionsForControlEvents:UIControlEventValueChanged];
    
    if ([pnvideoFramesToggleButton isOn]) {
        isFrameClick = NO;
        return;
    }
    
    
    isFrameClick = YES;
}

//-(void)setImageOverlayFrame
//{
//    for (int i = 0; i < shapeViewImageA.count ; i++) {
//
//        SPUserResizableView *shapeViewImage = [shapeViewImageA objectAtIndex:i];
//        [self.view addSubview:shapeViewImage];
//        [self.view bringSubviewToFront:shapeViewImage];
//        [self bringSubviewsToFront];
//
//
//
//            if (self.rotationVal == 1) {
//               CGFloat centreX = shapeViewImage.center.x;
//                CGFloat centreY = shapeViewImage.center.y;
//                [shapeViewImage setCenter:CGPointMake(centreY,  centreX)];
//    }
//
//}}


-(void)displayLayers
{
    
    
    @try {
        for (int i = 0; i <  shapesArray.count; i++) {
            
        NSDictionary *dicObj =  [dicA objectAtIndex:i];
            
            if ([[shapesArray objectAtIndex:i] isKindOfClass:[SPUserResizableView class]]) {
                SPUserResizableView *shapeView=[shapesArray objectAtIndex:i];
                shapeView.delegate=self;
                
                CGFloat perWidth = [[dicObj valueForKey:@"width"] floatValue];
                CGFloat perHeight = [[dicObj valueForKey:@"height"] floatValue];
                CGFloat percentX = [[dicObj valueForKey:@"originx"] floatValue];
                CGFloat percentY = [[dicObj valueForKey:@"originy"] floatValue];
                //   NSValue *rectValue = [dicObj valueForKey:@"previousframe"]  ;
                //   CGRect rect = [rectValue CGRectValue];
                
                CGFloat width = (perWidth * _mainContainerView.frame.size.width)/100;
                CGFloat height = (perHeight * _mainContainerView.frame.size.height)/100;
                CGFloat originX = (percentX * _mainContainerView.frame.size.width)/100;
                CGFloat originY = (percentY * _mainContainerView.frame.size.height)/100;
                CGRect frame= {originX,originY,width,height};
                
                if (shapeView.shapeType != ShapeTypeImage) {
                 
                    
                    
                    if (self.rotationVal == 1 )// && shapeView.shapeType==ShapeTypeImage)
                    {
                        
                        if (shapeView.shapeType == ShapeTypeLine) {
                            
                            CGAffineTransform t = shapeView.transform;
                            shapeView.transform = CGAffineTransformIdentity;
                            shapeView.transform = t;
                        }
                        else
                        {
                            
                            [shapeView setFrame:frame];
                            
                        }}
                    
                }
                
                
                shapeView.preventsPositionOutsideSuperview= YES;
                [_mainContainerView addSubview:shapeView];
                if (shapeView.shapeType == ShapeTypeImage) {
                    [shapeView setFrame:_mainContainerView.bounds];
                }
                
                [_mainContainerView bringSubviewToFront:shapeView];
                
                //  [self.view bringSubviewToFront:_mainContainerView];
                
                [shapeView setContentMode:UIViewContentModeScaleToFill];
                
                
            }
            
            else
            {
      
                if ([[shapesArray objectAtIndex:i]isKindOfClass:[MDrawLine class]]) {
                    
                    MDrawLine *lineObj=[shapesArray objectAtIndex:i];
                      _mainContainerView.showMeasurement = NO;
                    CGFloat perSPX = [[dicObj valueForKey:@"spx"] floatValue];
                    CGFloat perSPY = [[dicObj valueForKey:@"spy"] floatValue];
                    CGFloat perEPX = [[dicObj valueForKey:@"epx"] floatValue];
                    CGFloat perEPY = [[dicObj valueForKey:@"epy"] floatValue];
                    //   NSValue *rectValue = [dicObj valueForKey:@"previousframe"]  ;
                    //   CGRect rect = [rectValue CGRectValue];
                    
                    CGFloat spx = (perSPX * _mainContainerView.frame.size.width)/100;
                    CGFloat spy = (perSPY * _mainContainerView.frame.size.height)/100;
                    CGFloat epx = (perEPX * _mainContainerView.frame.size.width)/100;
                    CGFloat epy = (perEPY * _mainContainerView.frame.size.height)/100;
                    CGPoint startPoint = CGPointMake(spx, spy);
                    CGPoint endPoint = CGPointMake(epx, epy);
                    [lineObj setStartPoint:startPoint];
                    [lineObj setEndPoint:endPoint];
                    [angleObjs addObject:lineObj];
                    
                    
                }
                else
                {
                    MDrawAngle *angleObj=[shapesArray objectAtIndex:i];
                     _mainContainerView.showMeasurement = YES;
                    
                    CGFloat perP1X = [[dicObj valueForKey:@"p1x"] floatValue];
                    CGFloat perP1Y = [[dicObj valueForKey:@"p1y"] floatValue];
                    CGFloat perP2X = [[dicObj valueForKey:@"p2x"] floatValue];
                    CGFloat perP2Y = [[dicObj valueForKey:@"p2y"] floatValue];
                    CGFloat perP3X = [[dicObj valueForKey:@"p3x"] floatValue];
                    CGFloat perP3Y = [[dicObj valueForKey:@"p3y"] floatValue];
                 
                    //   NSValue *rectValue = [dicObj valueForKey:@"previousframe"]  ;
                    //   CGRect rect = [rectValue CGRectValue];
                    
                    CGFloat p1x = (perP1X * _mainContainerView.frame.size.width)/100;
                    CGFloat p1y = (perP1Y * _mainContainerView.frame.size.height)/100;
                    CGFloat p2x = (perP2X * _mainContainerView.frame.size.width)/100;
                    CGFloat p2y = (perP2Y * _mainContainerView.frame.size.height)/100;
                    CGFloat p3x = (perP3X * _mainContainerView.frame.size.width)/100;
                    CGFloat p3y  = (perP3Y * _mainContainerView.frame.size.height)/100;
                    CGPoint p1 = CGPointMake(p1x, p1y);
                    CGPoint p2 = CGPointMake(p2x, p2y);
                    CGPoint p3 = CGPointMake(p3x, p3y);
                    
                    if (angleObj.points.count > 0) {
                        [angleObj.points removeAllObjects];
                        [angleObj.points addObject:[NSValue valueWithCGPoint:p1]];
                          [angleObj.points addObject:[NSValue valueWithCGPoint:p2]];
                          [angleObj.points addObject:[NSValue valueWithCGPoint:p3]];
                    }
                   
            
                    [angleObjs addObject:angleObj];
                }

}
}
        
        
        //  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        if (angleObjs.count > 0) {
            
            
            [ _mainContainerView.tools addObjectsFromArray:angleObjs];
            [_mainContainerView refreshCalibrations];
           [angleObjs removeAllObjects];
           
         
        }
        

        [self bringSubviewsToFront];
        


    }
    @catch (NSException *exception) {
        NSLog(@"exception %@",exception);
    }
   
    
    
}


-(void)showHideView
{
    if (isViewHidden ) {
        [self showViews];
    }
    
    else
    {
        [self hideViews];
    }
}



- (void) bringSubviewsToFront{
    [self.view bringSubviewToFront:_bottomMenuContainerView];
    [self.view bringSubviewToFront:snapshotButton];
    [self.view bringSubviewToFront:playbackControlsView];
    [self.view bringSubviewToFront:superView];
    //    [self.view bringSubviewToFront:overlaysButton];
 //   [self.view bringSubviewToFront:superView];
}

- (void) videoScreensReadyForAnalysis
{
    
    NSArray *analysisScreens=[_videoInfo objectForKey:@"analysisScreens"];
    movieSlider.minimumValue=0;
    movieSlider.maximumValue=analysisScreens.count-1;
    if (nCurrVal > 0) {
        movieSlider.value = nCurrVal;
    }
    else
    {
        movieSlider.value=0;
    }
    
    playbackImageView=[[UIImageView alloc] initWithFrame:[self resizeImageFrame]];
    [playbackImageView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:playbackImageView];
    [self bringSubviewsToFront];
    currentPlaybackTime=0.0;
    [self movieSliderValueChanged:movieSlider];
    // setting main container view frame
    _mainContainerView.translatesAutoresizingMaskIntoConstraints = YES;
      
    [_mainContainerView setFrame: CGRectMake(playbackImageView.frame.origin.x, playbackImageView.frame.origin.y, playbackImageView.frame.size.width, playbackImageView.frame.size.height)];
    
    NSDictionary *d=[analysisScreens lastObject];
    
    float duration=[[d objectForKey:@"analysisScreenTime"] floatValue];//CMTimeGetSeconds(_asset.duration);
    int min=duration/60;
    float sec=duration-(min*60.0);
    
    NSString *minString=[NSString stringWithFormat:@"%d", min];
    if(min<10)
        minString=[NSString stringWithFormat:@"0%@", minString];
    NSString *secString=[NSString stringWithFormat:@"%.1f", sec];
    if(sec<10)
        secString=[NSString stringWithFormat:@"0%@", secString];
    
    movieDurationLabel.text=[NSString stringWithFormat:@"%@:%@", minString, secString];
    
    [self.view bringSubviewToFront:_mainContainerView];
    [self.view bringSubviewToFront:playbackControlsView];
    [self.view bringSubviewToFront:_bottomMenuContainerView];
    [self.view bringSubviewToFront:snapshotButton];
    [self.view bringSubviewToFront:superView];
    
    
      [_mmDrawerController setIsVideoAnalysisDone:NO]  ;
    //    [self setupPlaybackTimer];
}


-(CGRect) resizeImageFrame
{
    NSArray *analysisScreens=[_videoInfo objectForKey:@"analysisScreens"];
    if (analysisScreens.count > 0) {
        NSDictionary *analysisScreenDictionary=[analysisScreens objectAtIndex:0];
        analysisImage=[UIImage imageWithContentsOfFile:[mediaFolderPath stringByAppendingPathComponent:[[analysisScreenDictionary objectForKey:@"analysisScreenPath"] lastPathComponent]]];
        
        CGRect imageVwFrame = _thumbnailImageView.frame;
        
        if ( analysisImage.size.height > analysisImage.size.width &&  analysisImage.size.height >imageVwFrame.size.height) {
            
            CGFloat scale = imageVwFrame.size.height / analysisImage.size.height;
            CGFloat newWidth = analysisImage.size.width * scale;
            if(newWidth<0)
                newWidth = 0;
            CGFloat originX  = (_mainContainerView.frame.size.width - newWidth)/2;
            CGFloat originY  = (_mainContainerView.frame.size.height - imageVwFrame.size.height )/2;
            return   CGRectMake(originX, _mainContainerView.frame.origin.y+ originY , newWidth, imageVwFrame.size.height);
        }
        else if ( analysisImage.size.width > analysisImage.size.height  && analysisImage.size.width >imageVwFrame.size.width )
        {
            CGFloat scale = imageVwFrame.size.width / analysisImage.size.width;
            CGFloat newHeight = analysisImage.size.height * scale;
            if(newHeight<0)
                newHeight = 0;
            CGFloat originX  =  (_mainContainerView.frame.size.width - imageVwFrame.size.width )/2;   //(_mainContainerView.frame.size.width - newWidth)/2;
            CGFloat originY  =(_mainContainerView.frame.size.height - newHeight)/2;
            return   CGRectMake(originX, _mainContainerView.frame.origin.y+ originY , imageVwFrame.size.width, newHeight);
        }
        else
        {
            UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
            UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
            if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight )))
            {
                
                /*
                 
                 while ((_mainContainerView.frame.size.height) > newHeight )
                 {
                 newWidth = i  + analysisImage.size.width ;
                 newHeight = i + analysisImage.size.height;
                 i++;
                 }
                 */
                
                CGFloat scale = _thumbnailImageView.frame.size.height / analysisImage.size.height;
                CGFloat newWidth = analysisImage.size.width* scale;
                if(newWidth<0)
                {
                    newWidth=0;
                }
                CGFloat originX = (_thumbnailImageView.frame.size.width-newWidth)/2;
                
                //   CGFloat originX = (mainViewWidth-overlayWidth)/2;
                //   CGFloat originY = (mainViewHeight-overlayHeight)/2;
                return    CGRectMake(originX,0 , newWidth, _thumbnailImageView.frame.size.height);
                
                
                
            }
            else
            {
                
                
                CGFloat scale = _thumbnailImageView.frame.size.width / analysisImage.size.width;
                CGFloat newHeight = analysisImage.size.height* scale;
                if(newHeight<0)
                {
                    newHeight=0;
                }
                
                CGFloat originY = (_thumbnailImageView.frame.size.height-newHeight)/2;
                
                //   CGFloat originX = (mainViewWidth-overlayWidth)/2;
                //   CGFloat originY = (mainViewHeight-overlayHeight)/2;
                return    CGRectMake(0,originY , _thumbnailImageView.frame.size.width, newHeight);
                
                
                /*
                 while ( _mainContainerView.frame.size.width > newWidth )
                 {
                 newWidth = i  + analysisImage.size.width ;
                 newHeight = j+ analysisImage.size.height;
                 
                 i++;
                 j = j + 1.3;
                 }
                 
                 */
                
                
                
            }
            
            //     CGFloat originX  = (_mainContainerView.frame.size.width -newWidth )/2;
            //     CGFloat originY  = (_mainContainerView.frame.size.height - newHeight)/2;
            //       return CGRectMake(originX,_mainContainerView.frame.origin.y + originY, newWidth, newHeight);
            
            
            
        }
    }
    
    
    return CGRectZero;
}

- (CGRect) getPlaybackImageViewFrame
{
    
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
    
    if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight ))) {
        
        landScapeFrame = _thumbnailImageView.frame;
        CGFloat newThumbHeight=1.11716*(landScapeFrame.size.width/2);
        CGFloat yToDrawThumb = ((landScapeFrame.size.height-newThumbHeight)/2)+10;
        landScapeFrame.size.height=newThumbHeight;
        landScapeFrame.origin.y=yToDrawThumb;
        return landScapeFrame;
    }
    else
    {
        
        if (!isFrameSet) {
            imageViewFrame=_thumbnailImageView.frame;
            newThmbWidth=1.11716*(imageViewFrame.size.height/2);
            xToDrawThmb = (imageViewFrame.size.width-newThmbWidth)/2;
            isFrameSet = YES;
        }
        
        imageViewFrame.size.width=newThmbWidth;
        imageViewFrame.origin.x=xToDrawThmb;
        return imageViewFrame;
    }
}


- (void) setupPlaybackTimer
{
    NSArray *analysisScreens=[_videoInfo objectForKey:@"analysisScreens"];
    [self cancelPlaybackTimer];
    
    float sliderTimer=CMTimeGetSeconds(_asset.duration)/(float)analysisScreens.count;
    playbackTimer=[NSTimer timerWithTimeInterval:sliderTimer target:self selector:@selector(updatePlaybackTime) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:playbackTimer forMode:NSDefaultRunLoopMode];
    //    playPuaseButton.selected=YES;
}

- (void) cancelPlaybackTimer
{
    if(playbackTimer.isValid)
        [playbackTimer invalidate];
    playbackTimer=nil;
}

- (void) updatePlaybackTime
{
    //    currentPlaybackTime+=10;
    //    if(((int)currentPlaybackTime%100)==0)
    //    {
    [self nextFrameButtonClicked:nil];
    //    }
}

- (void) generateScreensForAnalysis
{
    
    if (totalCount == 0) {
        totalCount = totalCount +1;
        return;
    }
    
    
 //   if ( _mmDrawerController.isVideoAnalysisDone == NO) {
         [_mmDrawerController setIsVideoAnalysisDone:YES]  ;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Preparing video for analysis";
            [hud show:YES];
        });
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
          
            currentOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
            _imageGenerator.appliesPreferredTrackTransform = YES;
            _imageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
            _imageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
            _imageGenerator.maximumSize = [self getPlaybackImageViewFrame].size;
            
            
            //    OrbisMediaGallery *orbisMediaGallery=[OrbisMediaGallery sharedGallery];
            //    NSMutableArray *videoAnalysisScreens=[NSMutableArray array];
            Float64 duration=CMTimeGetSeconds(_asset.duration);
            analysisScreenTimes = [NSMutableArray array];
            for(int i=0; i<duration; i++)
            {
                
                /*
                 if (duration >= 180) {
                 for(float j=0.0; j<1.0; j=j+0.5)
                 {
                 //            NSError *err = NULL;
                 //            int k=j*10.0;
                 Float64 frameTime = (Float64)i+roundToN(j, 2);
                 if(frameTime>duration)
                 break;
                 
                 CMTime time = CMTimeMakeWithSeconds(frameTime, 600);
                 [analysisScreenTimes addObject:[NSValue valueWithCMTime:time]];
                 
                 }
                 }
                 
                 else
                 {
                 */
                for(float j=0.0; j<1.0; j=j+0.05)
                {
                    //            NSError *err = NULL;
                    //            int k=j*10.0;
                    Float64 frameTime = (Float64)i+roundToN(j, 2);
                    if(frameTime>duration)
                        break;
                    
                    CMTime time = CMTimeMakeWithSeconds(frameTime, 600);
                    [analysisScreenTimes addObject:[NSValue valueWithCMTime:time]];
                    
                }
                //  }
                
                
                
            }
            
            //    AVVideoComposition * composition = [AVVideoComposition videoCompositionWithPropertiesOfAsset:_asset];
            //    CGSize renderSize = composition.renderSize;
            
            
            
            [NSThread sleepForTimeInterval:0.5];
            
            /*
             dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
             dispatch_async(queue, ^{
             
             CGImageRef imageRef ;
             
             for (int i = 0; i <analysisScreenTimes.count ; i++) {
             
             if (i  % 3  ==0   && i != 0 ) {
             [NSThread sleepForTimeInterval:0.5];
             }
             CMTime timeVal = [[analysisScreenTimes objectAtIndex:i] CMTimeValue];
             imageRef = [_imageGenerator copyCGImageAtTime:timeVal  actualTime:NULL error:NULL];
             // UIImage *    thumbnail = [[UIImage alloc] initWithCGImage:imageRef];
             
             
             
             
             
             
             // UIImage *videoScreen = [[UIImage alloc] initWithCGImage:imageRef];
             //                                                    videoScreen = [[OrbisMediaGallery sharedGallery] rotateImage:videoScreen toOrientation:UIImageOrientationRight];
             NSDictionary *d=[[OrbisMediaGallery sharedGallery] saveAnalysisImage:imageRef atTime:(timeVal.value/600.0) forVideoAtPath:[_videoInfo objectForKey:@"videoPath"]];
             if([_videoInfo objectForKey:@"lessonID"])
             {
             NSMutableDictionary *d2=[NSMutableDictionary dictionaryWithDictionary:d];
             [d2 setObject:[_videoInfo objectForKey:@"lessonID"] forKey:@"lessonID"];
             [d2 setObject:[_videoInfo objectForKey:@"studentID"] forKey:@"studentID"];
             _videoInfo=d2;
             }
             else
             {
             _videoInfo=d;
             }
             
             if (imageRef != nil) {
             CGImageRelease(imageRef);
             }
             
             dispatch_async(dispatch_get_main_queue(), ^{
             
             NSValue *v=[analysisScreenTimes lastObject];
             if(v.CMTimeValue.value==timeVal.value) {
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [self videoScreensReadyForAnalysis];
             }
             
             
             
             });
             
             
             }
             
             
             
             
             
             
             });
             
             */
            
            
            @try {
                
                [_imageGenerator generateCGImagesAsynchronouslyForTimes:analysisScreenTimes
                                                      completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime,
                                                                          AVAssetImageGeneratorResult result, NSError *error) {
                                                          
                                                          // image = [self resizeCGImage:image toWidth:image andHeight:_thumbnailImageView.frame.size.height/2];
                                                          
                                                          //                                             NSString *requestedTimeString = (NSString *)
                                                          //                                             CFBridgingRelease(CMTimeCopyDescription(NULL, requestedTime));
                                                          //
                                                          //                                             NSString *actualTimeString = (NSString *)
                                                          //
                                                          //                                             CFBridgingRelease(CMTimeCopyDescription(NULL, actualTime));
                                                          //
                                                          //                                             NSLog(@"Requested: %@; actual %@", requestedTimeString, actualTimeString);
                                                          //
                                                          if (result == AVAssetImageGeneratorSucceeded) {
                                                              //                                                    UIImage *videoScreen = [[UIImage alloc] initWithCGImage:image];
                                                              //                                                    videoScreen = [[OrbisMediaGallery sharedGallery] rotateImage:videoScreen toOrientation:UIImageOrientationRight];
                                                              
                                                              NSDictionary *d=[[OrbisMediaGallery sharedGallery] saveAnalysisImage:image atTime:(actualTime.value/600.0) forVideoAtPath:[_videoInfo objectForKey:@"videoPath"]];
                                                              if([_videoInfo objectForKey:@"lessonID"])
                                                              {
                                                                  NSMutableDictionary *d2=[NSMutableDictionary dictionaryWithDictionary:d];
                                                                  [d2 setObject:[_videoInfo objectForKey:@"lessonID"] forKey:@"lessonID"];
                                                                  [d2 setObject:[_videoInfo objectForKey:@"studentID"] forKey:@"studentID"];
                                                                  _videoInfo=d2;
                                                              }
                                                              else
                                                              {
                                                                  _videoInfo=d;
                                                              }
                                                              
                                                              
                                                              //                                                 CGImageRelease(image); // CGImageRef won't be released by ARC
                                                              //                                                 videoScreen=nil;
                                                              
                                                          }
                                                          
                                                          if (result == AVAssetImageGeneratorFailed) {
                                                              NSLog(@"Failed with error: %@", [error localizedDescription]);
                                                          }
                                                          if (result == AVAssetImageGeneratorCancelled) {
                                                              NSLog(@"Canceled");
                                                          }
                                                          
                                                          NSValue *v=[analysisScreenTimes lastObject];
                                                          if(v.CMTimeValue.value==requestedTime.value) {
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  
                                                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                  [self videoScreensReadyForAnalysis];
                                                              });
                                                          }}];
                
                
            }
            @catch (NSException *exception) {
                NSLog(@"exception %@",exception);
            }
        });

        
  //  }
    
//    [orbisMediaGallery saveAnalysisImages:videoAnalysisScreens forVideo:_videoInfo];
    
}

- (void) moviePlaybackStateDidChange:(NSNotification*)notification
{
    //    float f=moviePlayerController.currentPlaybackRate;
    //    NSLog(@"in change: playback rate: %f", f);
    //    moviePlayerController.currentPlaybackRate=currentPlaybackRate;
    //    f=moviePlayerController.currentPlaybackRate;
    //    NSLog(@"in change: playback rate: %f", f);
  //deprecations
    /*
    stopVideoFunc = 1;
    //    MPMoviePlayerController *moviePlayer = notification.object;
    //    deprecations
    MPMoviePlaybackState playbackState = moviePlayerViewController.moviePlayer.playbackState;
//    AVPlayerStatus playbackState = moviePlayerViewController2.player.status;
    
    switch (playbackState) {
        case MPMoviePlaybackStateStopped:
        {
            [self reloadView];
            //            moviePlayerController.currentPlaybackRate=currentPlaybackRate;
        }
            break;
        case MPMoviePlaybackStatePlaying:
        {
           //    deprecations
            moviePlayerViewController.moviePlayer.currentPlaybackRate=currentPlaybackRate;
        }
            break;
        case MPMoviePlaybackStatePaused:
        {
            //            moviePlayerController.currentPlaybackRate=currentPlaybackRate;
            [self reloadView];
        }
            break;
        case MPMoviePlaybackStateInterrupted:
        {
            //            moviePlayerController.currentPlaybackRate=currentPlaybackRate;
        }
            break;
        case MPMoviePlaybackStateSeekingForward:
        {
            
        }
            break;
        case MPMoviePlaybackStateSeekingBackward:
        {
            
        }
            break;
    }*/
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    [self resetPlaybackRateControls];
    //deprecations
   /*
    int reason = [[[notification userInfo] valueForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
    if (reason == MPMovieFinishReasonUserExited) {
        //user hit the done button
        //        deprecations

        [moviePlayerViewController.moviePlayer setFullscreen:NO animated:YES];

//        if (@available(iOS 11.0, *)) {
//            [moviePlayerViewController setEntersFullScreenWhenPlaybackBegins:NO];
//        } else {
//            // Fallback on earlier versions
//        }

//        deprecations
        moviePlayerViewController.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
//        moviePlayerViewController.showsPlaybackControls = YES;
    }else if (reason == MPMovieFinishReasonPlaybackEnded) {
        
        //movie finished playin
    }else if (reason == MPMovieFinishReasonPlaybackError) {
        //error
    }
    
    
    //    float f=moviePlayerController.currentPlaybackRate;
    //    NSLog(@"in finish: playback rate: %f", f);
    //    moviePlayerController.currentPlaybackRate=currentPlaybackRate;
    //    f=moviePlayerController.currentPlaybackRate;
    //    NSLog(@"in finish: playback rate: %f", f);
    */
}

- (void) moviePlayerWillEnterFullScreen:(NSNotification*)notification
{
    //    deprecations
/*
    if (notification.object == moviePlayerViewController.moviePlayer)
    {
        //        [moviePlayerController setFullscreen:NO animated:YES];
        //        moviePlayerController.controlStyle = MPMovieControlStyleEmbedded;
    }*/
}

- (void) moviePlayerDidEnterFullScreen:(NSNotification*)notification
{
    //    deprecations
/*
    if (notification.object == moviePlayerViewController.moviePlayer)
    {
        //        [moviePlayerController setFullscreen:NO animated:YES];
        //        moviePlayerController.controlStyle = MPMovieControlStyleEmbedded;
    }*/
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
    NSURL *movieURL=[NSURL fileURLWithPath:[mediaFolderPath stringByAppendingPathComponent:[[_videoInfo objectForKey:@"videoPath"] lastPathComponent]]];
    //deprecations
//    moviePlayerViewController=[[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
    
    moviePlayerViewController2 = [[AVPlayerViewController alloc] init];

    AVPlayer *player = [AVPlayer playerWithURL:movieURL];

    // create a player view controller
    moviePlayerViewController2.player = player;

    if (isVPlay) {
        [self movieSliderValueChanged:movieSlider];
        playPuaseButton.selected=!playPuaseButton.selected;
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self hideViews];
            
        });
    }
    if (isFrameClick ) {
        [pnvideoFramesToggleButton  setOn:NO];
    }
    
    //    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    //    float f=moviePlayerController.currentPlaybackRate;
    //    NSLog(@"playback rate: %f", f);
    //    moviePlayerController.currentPlaybackRate=0.25f;
    //    f=moviePlayerController.currentPlaybackRate;
    //
    //    NSLog(@"playback rate: %f", f);
    
    if (IS_IPHONE_X || IS_IPHONE_XMAX || IS_IPHONE_XR){
        
        botomViewCons.constant = 30 ;
        [_bottomMenuContainerView setNeedsUpdateConstraints];
    }

    [self.view bringSubviewToFront:_mainContainerView];
    [self.view bringSubviewToFront:_bottomMenuContainerView];
    [self.view bringSubviewToFront:playbackControlsView];
}

-(void)viewWillDisappear:(BOOL)animated
{
 if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
    }
    
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}



- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    /*
     if (_mainContainerView  ) {
     _mainContainerView = nil;
     _mainContainerView =   [[UIView alloc]initWithFrame:CGRectMake(playbackImageView.frame.origin.x, playbackImageView.frame.origin.y, playbackImageView.frame.size.width, playbackImageView.frame.size.height)];
     [self.view addSubview:_mainContainerView];
     [_mainContainerView setBackgroundColor:[UIColor clearColor]];
     }
     
     */
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        float f=moviePlayerController.currentPlaybackRate;
    //        NSLog(@"playback rate: %f", f);
    //        moviePlayerController.currentPlaybackRate=0.25f;
    //        f=moviePlayerController.currentPlaybackRate;
    //        NSLog(@"playback rate: %f", f);
    //    });
    
    //   [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
    
}


- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    //    if(!moviePlayerViewController.moviePlayer.isFullscreen)
    //    {
    //        [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerThumbnailImageRequestDidFinishNotification object:nil];
    //
    //        [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    //        [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    /*
     [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidEnterFullscreenNotification object:nil];
     */
    
    //        if(moviePlayerViewController && moviePlayerViewController.moviePlayer.playbackState==MPMoviePlaybackStatePlaying)
    //        {
    //            [moviePlayerViewController.moviePlayer stop];
    //        }
    //
    //        moviePlayerViewController=nil;
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark
#pragma mark Touch Methods

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self hideEditingHandles];
}

#pragma mark
#pragma mark IBAction Methods

- (IBAction)overlaysButtonClicked:(id)sender
{
    self.rotationVal = 0;
    OverlaysViewController *overlayViewController=[[OverlaysViewController alloc] initWithNibName:@"OverlaysViewController" bundle:nil thumbnailImagePath:[_videoInfo objectForKey:@"videoThumbnailPath"] overlaysArray:[[OrbisMediaGallery sharedGallery] getOverlaysForVideoAtPath:[_videoInfo objectForKey:@"videoPath"]] presentingController:self];
    overlayViewController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:overlayViewController animated:YES completion:nil];
}

- (IBAction)toggleSlideMenu:(id)sender {
    //[_mmDrawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
    //cheema
    
    [self presentViewController:SideMenuManager.menuRightNavigationController animated:true completion:nil];
    
}
     
- (UIViewController*) topMostController{
        UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
        }
        
        return topController;
    }

- (IBAction)backButtonClicked:(id)sender
{
    [GlobalClass sharedInstance].shouldAllowLandscape = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerThumbnailImageRequestDidFinishNotification object:nil];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)lineButtonClicked:(id)sender
{
   // [self drawShapeOfType:ShapeTypeLine];
   // NSLog(@"container view  centre %i,%i",_mainContainerView.center.x,_mainContainerView.center.y);
    
    [_mainContainerView beginDrawingForType:[MDrawLine class]];
     [_mainContainerView setShowMeasurement:NO];
    [_mainContainerView drawDown:CGPointMake((_mainContainerView.frame.size.width/2)-30, (_mainContainerView.frame.size.height/2)-30)];
    [_mainContainerView drawMoveFromPoint:CGPointMake((_mainContainerView.frame.size.width/2)-30, (_mainContainerView.frame.size.height/2)-30) toPoint:CGPointMake((_mainContainerView.frame.size.width/2)+30, (_mainContainerView.frame.size.height/2)+30)];
       [_mainContainerView drawUp:CGPointMake((_mainContainerView.frame.size.width/2)+30, (_mainContainerView.frame.size.height/2)+30)];
    
       [shapesArray addObject:[_mainContainerView.tools lastObject]];

}

- (IBAction)circleButtonClicked:(id)sender
{
    [self drawShapeOfType:ShapeTypeCircle];
}

- (IBAction)squareButtonClicked:(id)sender
{
    [self drawShapeOfType:ShapeTypeSquare];
}

- (IBAction)angleButtonClicked:(id)sender
{
    [_mainContainerView beginDrawingForType:[MDrawAngle class]];
    [_mainContainerView setShowMeasurement:YES];
    [_mainContainerView drawDown:CGPointMake((_mainContainerView.frame.size.width/2)-30, _mainContainerView.frame.size.height/2)];
    [_mainContainerView drawMoveFromPoint:CGPointMake((_mainContainerView.frame.size.width/2)-30, _mainContainerView.frame.size.height/2) toPoint:CGPointMake((_mainContainerView.frame.size.width/2)+30, _mainContainerView.frame.size.height/2)];
    [_mainContainerView drawUp:CGPointMake((_mainContainerView.frame.size.width/2)+30, _mainContainerView.frame.size.height/2)];
        [_mainContainerView drawMoveFromPoint:CGPointMake((_mainContainerView.frame.size.width/2)+30, (_mainContainerView.frame.size.height/2)-30) toPoint:CGPointMake((_mainContainerView.frame.size.width/2)+30, (_mainContainerView.frame.size.height/2)-60)];
   
    [_mainContainerView drawUp:CGPointMake((_mainContainerView.frame.size.width/2)+30, (_mainContainerView.frame.size.height/2)-60)];
    [shapesArray addObject:[_mainContainerView.tools lastObject]];
}

- (IBAction)eraseButtonClicked:(id)sender
{
    [self perfromUndo];
}

- (IBAction)eraseAllButtonClicked:(id)sender
{
    [self performUndoAll];
}

- (IBAction)saveButtonClicked:(id)sender
{
    if([_videoInfo objectForKey:@"lessonID"] && [[_videoInfo objectForKey:@"lessonID"] integerValue]>0)
    {
        actionSheet=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Close" destructiveButtonTitle:nil otherButtonTitles:@"SAVE WITH OVERLAYS", @"SAVE TO LESSON", @"VIEW OVERLAYS", nil];
    }
    else
    {
                //holy //added only Upload To Locker

        if (_delegate == nil) {
                    actionSheet=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Close" destructiveButtonTitle:nil otherButtonTitles:@"SAVE WITH OVERLAYS", @"VIEW OVERLAYS", nil];
        }else{
                    actionSheet=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Close" destructiveButtonTitle:nil otherButtonTitles:@"SAVE WITH OVERLAYS", @"VIEW OVERLAYS",@"UPLOAD TO STUDENT LOCKER", nil];
        }
        

    }
    
    //    [actionSheet showFromRect:CGRectMake(0, 0, 100, 100) inView:self.view animated:YES];
    [actionSheet showInView:self.navigationController.navigationBar];
    
    //    CGRect frame= actionSheet.frame;
    //    frame.origin.y=frame.origin.y-50;
    //    actionSheet.frame=frame;
}

- (void) image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Image saved to Photos Album" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    });
}


- (IBAction)uploadButtonClicked:(id)sender
{
    if([_videoInfo objectForKey:@"lessonID"] && [[_videoInfo objectForKey:@"lessonID"] integerValue]>0)
    {
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion:^{
            // TODO: Upload-Button
//            AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//            NSString *jsonString=[self getJSONString:_videoInfo];
//            [mvc.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.lessonVideosPage.uploadVideoWithInfo(%@);", jsonString]];
        }];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"BUTTON IS ONLY ENABLED ON LESSON VIEW" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)snapshotButtonClicked:(id)sender {
    //    if(moviePlayerController && ([moviePlayerController playbackState]==MPMoviePlaybackStatePlaying || [moviePlayerController playbackState]==MPMoviePlaybackStatePaused)){
    //        NSTimeInterval currPlaybackTime=[moviePlayerController currentPlaybackTime];
    //        NSNumber *playbackTime=[NSNumber numberWithInt:currPlaybackTime];
    
    //        UIImage *thumbnail=[self generateThumbnailForCurrentVideoAtTime:playbackTime.intValue];
    
    NSArray *analysisScreens=[_videoInfo objectForKey:@"analysisScreens"];
    int sliderValue = movieSlider.value;
    if ([analysisScreens count] < 1){
        
        return;
    }
    NSDictionary *analysisScreenDictionary=[analysisScreens objectAtIndex:sliderValue];
    UIImage *thumbnail=[UIImage imageWithContentsOfFile:[mediaFolderPath stringByAppendingPathComponent:[[analysisScreenDictionary objectForKey:@"analysisScreenPath"] lastPathComponent]]];
    
    
    //        CGSize thumbnailSize=thumbnail.size;
    UIImage *overlayImage=[self getDrawnSnapshotImage];
    //        CGSize  overlaySize=overlayImage.size;
    
    /*
     //        CGSize finalImageSize=CGSizeMake(overlayImage.size.width, thumbnail.size.height);
     //
     //        UIGraphicsBeginImageContextWithOptions(finalImageSize, NO, 0);
     //        [thumbnail drawInRect:CGRectMake(0, 0, thumbnail.size.width, finalImageSize.height)];
     //        [overlayImage drawInRect:CGRectMake(0, 0, overlayImage.size.width, overlayImage.size.height)];
     */
    CGSize finalImageSize=CGSizeMake(overlayImage.size.width, overlayImage.size.height);
    
    UIGraphicsBeginImageContextWithOptions(finalImageSize, NO, 0);
    
    //        CGSize thubmSize=thumbnail.size;
  //  CGFloat newThumbHeight=overlayImage.size.height;
  //  CGFloat newThumbWidth=1.11716*(overlayImage.size.height/2);// 205.0f;//(overlayImage.size.height/thumbnail.size.height)*overlayImage.size.width;
    
  //  CGFloat xToDrawThumb = (overlayImage.size.width-newThumbWidth)/2;
    
    //     [thumbnail drawInRect:CGRectMake(xToDrawThumb, 0, newThumbWidth, newThumbHeight)];
    //    [overlayImage drawInRect:CGRectMake(0, 0, finalImageSize.width, finalImageSize.height)];
    
    
    [thumbnail drawInRect:CGRectMake(0, 0, _mainContainerView.frame.size.width, _mainContainerView.frame.size.height)];
    [overlayImage drawInRect:CGRectMake(0, 0, _mainContainerView.frame.size.width, _mainContainerView.frame.size.height)];
    
    //  UIImage *finalImage=UIGraphicsGetImageFromCurrentImageContext();
    
    UIImage *finalImageWS=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage *finalImage = [finalImageWS  imageByTrimmingTransparentPixels];
    
    
    if(finalImage!=nil)
    {
        NSString *fileName=[NSString stringWithFormat:@"thumb%f.jpg",[[analysisScreenDictionary objectForKey:@"analysisScreenTime"] floatValue]];
        NSString *filePath=[NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        NSData *jpegData=UIImageJPEGRepresentation(finalImage, 1.0);
        if(jpegData!=nil && jpegData.length>0)
        {
            [jpegData writeToFile:filePath atomically:NO];
            [[OrbisMediaGallery sharedGallery] savePhotoToGalleryAtPath:filePath];
        }
    }
    //    }
}

#pragma mark - Playback Controls

- (IBAction)playPauseButtonClicked:(id)sender {
    
    if(playPuaseButton.selected) {  // pause movie
        isVPlay = NO;
        [self cancelPlaybackTimer];
        playPuaseButton.selected=!playPuaseButton.selected;
    }
    else {  // play the movie
        //deprecations
//        if([pnvideoFramesToggleButton isOn] && moviePlayerViewController)
                if([pnvideoFramesToggleButton isOn] && moviePlayerViewController2){
            //deprecations
            moviePlayerViewController2.modalPresentationStyle = UIModalPresentationFullScreen;
            [moviePlayerViewController2.player play];
            [self presentViewController:moviePlayerViewController2 animated:YES completion:nil];
        }
        
        else
        {
            
            [self.view setNeedsDisplay];
            isVPlay = YES;
            isFrameClick = YES;
            [self movieSliderValueChanged:movieSlider];
            [self setupPlaybackTimer];
            playPuaseButton.selected=!playPuaseButton.selected;
            
            
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self hideViews];
            });
        }
    }
}


- (IBAction)movieSliderValueChanged:(id)sender {
    NSArray *analysisScreens=[_videoInfo objectForKey:@"analysisScreens"];
    
    
    float sliderValue =movieSlider.value;
    nCurrVal = movieSlider.value;
    
    NSDictionary *analysisScreenDictionary=[analysisScreens objectAtIndex:(int)sliderValue];
    UIImage *image=[UIImage imageWithContentsOfFile:[mediaFolderPath stringByAppendingPathComponent:[[analysisScreenDictionary objectForKey:@"analysisScreenPath"] lastPathComponent]]];
    
   

    //   UIImage *rotateImg = [[OrbisMediaGallery sharedGallery] rotateImage:image byDegrees:180.0];
    [playbackImageView setImage:image];
   [playbackImageView setContentMode:UIViewContentModeScaleToFill];
    
    float time=[[analysisScreenDictionary objectForKey:@"analysisScreenTime"] floatValue];
    
    //    NSLog(@"slider Value: %f, %d", sliderValue, (int)sliderValue);
    
    //    sliderValue=(int)sliderValue/20.0;
    //    int min=sliderValue/60;
    //    float sec=sliderValue-(min*60.0);
    
    int min=time/60;
    float sec=time-(min*60.0);
    
    
    //    NSLog(@"slider Value: %f, min: %d, sec: %f", sliderValue, min, sec);
    
    NSString *minString=[NSString stringWithFormat:@"%d", min];
    if(min<10)
        minString=[NSString stringWithFormat:@"0%@", minString];
    NSString *secString=[NSString stringWithFormat:@"%.1f", sec];
    if(sec<10)
        secString=[NSString stringWithFormat:@"0%@", secString];
    
    movieProgressLabel.text=[NSString stringWithFormat:@"%@:%@", minString, secString];
    
}

- (IBAction)movieSliderDragStarted:(id)sender {
    [self cancelPlaybackTimer];
    
}

- (IBAction)movieSliderDragStopped:(id)sender {
    
    if (playPuaseButton.isSelected) {
         [self setupPlaybackTimer];
    }
    
   
}

- (IBAction)previousFrameButtonClicked:(id)sender {
    int currentValue=movieSlider.value;
    currentValue--;
    if(currentValue<movieSlider.minimumValue)
        return;
    movieSlider.value=currentValue;
    [self movieSliderValueChanged:movieSlider];
}

- (IBAction)nextFrameButtonClicked:(id)sender {
    int currentValue=movieSlider.value;
    currentValue++;
    if(currentValue>movieSlider.maximumValue)
    {
        currentValue=movieSlider.minimumValue;
        [self playPauseButtonClicked:playPuaseButton];
        //        return;
    }
    nCurrVal = currentValue;
    movieSlider.value=currentValue;
    [self movieSliderValueChanged:movieSlider];
}



- (NSString*)getJSONString:(NSDictionary*)d
{
    NSError* error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:d
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (error != nil) {
        NSLog(@"NSArray JSONString error: %@", [error localizedDescription]);
        return nil;
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}


#pragma mark - gesture delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view.superview isKindOfClass:[UIToolbar class]])
        return NO;
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark

- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView
{
    [_currentlyEditingView hideEditingHandles];
    _currentlyEditingView = userResizableView;
}

- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView
{
    _lastEditedView = userResizableView;
}


#pragma mark
#pragma Custom Methods
-(UIImage*) getDrawnSnapshotImage
{
    
    //   UIImage *finalImage=nil;
    
    
    CGSize finalImageSize=_mainContainerView.frame.size;
    // CGFloat thumbnaiImageViewY=_mainContainerView.frame.origin.y;
    
    //   CGSize finalImageSize=self.view.frame.size;
    //  CGFloat thumbnaiImageViewY=self.view.frame.origin.y;
    
    UIGraphicsBeginImageContextWithOptions(finalImageSize, NO,0);
    
    
    CGContextRef context=UIGraphicsGetCurrentContext();
    [_mainContainerView.layer renderInContext:context];
    UIImage *snapShotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    /*
     UIImage *finalImage = [[UIImage alloc] initWithCGImage:snapShotImage.CGImage
     scale: [[UIScreen mainScreen] scale]
     orientation: UIImageOrientationLeft];
     
     */
    
    return snapShotImage;
    
   
    
}





-(UIImage*) getDrawnImage
{
    
    //   UIImage *finalImage=nil;
    
    
    CGSize finalImageSize=_mainContainerView.frame.size;
    // CGFloat thumbnaiImageViewY=_mainContainerView.frame.origin.y;
    
    //   CGSize finalImageSize=self.view.frame.size;
    //  CGFloat thumbnaiImageViewY=self.view.frame.origin.y;
    
    UIGraphicsBeginImageContextWithOptions(finalImageSize, NO, [[UIScreen mainScreen] scale]);
    
 
    CGContextRef context=UIGraphicsGetCurrentContext();
    [_mainContainerView.layer renderInContext:context];
    UIImage *snapShotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  
    
    UIImage *finalImage = [[UIImage alloc] initWithCGImage:snapShotImage.CGImage
                                                     scale: [[UIScreen mainScreen] scale]
                                               orientation: UIImageOrientationLeft];
     
    
    
    return finalImage;

    
    /*
    UIGraphicsBeginImageContextWithOptions(finalImageSize, NO, [UIScreen mainScreen].scale);
    [_mainContainerView drawViewHierarchyInRect:_mainContainerView.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    */
     /*
     
     for(int i=0; i<shapesArray.count; i++)
     {
     SPUserResizableView *v=[shapesArray objectAtIndex:i];
     if([v shapeType]==ShapeTypeLine)
     {
     CGFloat angleInRadians = atan2f(v.transform.b, v.transform.a);
     CGRect imgRect = v.frame;
     CGFloat shapeY=imgRect.origin.y-thumbnaiImageViewY;
     //            CGContextRotateCTM(context, angleInRadians);
     CGContextRotateCTM(context, -angleInRadians);
     CGContextTranslateCTM(context, imgRect.origin.x, shapeY);
     
     [[v layer] renderInContext:context];
     
     CGContextTranslateCTM(context, -imgRect.origin.x, -shapeY);
     CGContextRotateCTM(context, angleInRadians);
     //            CGContextRotateCTM(context, -angleInRadians);
     }
     else
     {
     CGFloat shapeY=v.frame.origin.y-thumbnaiImageViewY;
     CGContextTranslateCTM(context, v.frame.origin.x, shapeY);
     [[v layer] renderInContext:context];
     CGContextTranslateCTM(context, -v.frame.origin.x, -shapeY);
     }
     
     }
     
     
     
     finalImage=UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     return finalImage;
     */
    
}




- (void) saveOverlay
{
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
    if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight )))  {
        imgShape =  @"landscape";
    }
    
    else  {
        imgShape =  @"portrait";
        
    }
    
    UIImage *finalImage=[self getDrawnImage];
    
    NSData* pngdata = UIImagePNGRepresentation(finalImage);
    UIImage* imageToSave = [UIImage imageWithData:pngdata];
    NSMutableDictionary *newVideoInfo=[NSMutableDictionary dictionaryWithDictionary:[[OrbisMediaGallery sharedGallery] saveOverlay:imageToSave forVideo:_videoInfo]];
    if([_videoInfo objectForKey:@"lessonID"] && [[_videoInfo objectForKey:@"lessonID"] integerValue]>0)
    {
        [newVideoInfo setObject:[_videoInfo objectForKey:@"lessonID"] forKey:@"lessonID"];
        [newVideoInfo setObject:[_videoInfo objectForKey:@"studentID"] forKey:@"studentID"];
    }
    
    _videoInfo=newVideoInfo;
}

- (void) hideEditingHandles {
    [_lastEditedView hideEditingHandles];
}

- (void) drawShapeOfType:(ShapeType)shapeType
{
    if(shapeType!=ShapeTypeNone)
    {
        CGFloat width = (30 *_mainContainerView.frame.size.width) /100;
        CGFloat height = width;
        CGFloat originX = (_mainContainerView.frame.size.width - width)/2 ;
        CGFloat originY = (_mainContainerView.frame.size.height - height)/2 ;
        CGRect frame = {originX,originY,width,height};
        
       SPUserResizableView *shapeView=[SPUserResizableView getResizeableViewWithShapeType:shapeType frame:frame];
        
        
        shapeView.delegate=self;
        shapeView.preventsPositionOutsideSuperview = YES;
        shapeView.tag=shapesArray.count;
        [shapesArray addObject:shapeView];
        [_mainContainerView addSubview:shapeView];
        [_mainContainerView bringSubviewToFront:shapeView];
        [self.view bringSubviewToFront:_mainContainerView];
        [self bringSubviewsToFront];
        
    }
}

- (void) drawShapeOfImage:(UIImage*)image
{
    if(image)
    {
        
        SPUserResizableView *shapeViewImage =[SPUserResizableView getResizeableViewWithImage:image];
        shapeViewImage.delegate=self;
        shapeViewImage.tag=shapesArray.count;
        [shapeViewImage setContentMode:UIViewContentModeScaleToFill];
        
        //   [shapeViewImage setContentView:UIViewContentModeScaleAspectFill];
        [shapesArray addObject:shapeViewImage];
        [shapeViewImage setBackgroundColor:[UIColor clearColor]];
        //        shapeViewImage.tag=shapeViewImageA.count;
        //        [shapeViewImageA addObject:shapeViewImage];
        //        [self.view addSubview:shapeViewImage];
        
        [_mainContainerView addSubview:shapeViewImage];
        //      [shapeViewImage setFrame:CGRectMake(0, 0, shapeViewImage.frame.size.width, shapeViewImage.frame.size.height)];
        //  [shapeViewImage setCenter:_mainContainerView.center];
        
        [shapeViewImage setContentMode:UIViewContentModeScaleAspectFill];
        shapeViewImage.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
        [shapeViewImage setClipsToBounds:YES];
       
        [_mainContainerView bringSubviewToFront:shapeViewImage];
        [shapeViewImage setFrame:playbackImageView.bounds];
        [self.view bringSubviewToFront:_mainContainerView];
        //      [self.view bringSubviewToFront:shapeViewImage];
        [self bringSubviewsToFront];
        
    }
}


- (void) perfromUndo
{

    if (shapesArray.count > 0) {
      if ([[shapesArray lastObject] isKindOfClass:[SPUserResizableView class]]) {
            UIImageView *shapeView=[shapesArray lastObject];
            [shapesArray removeLastObject];
            [shapeView removeFromSuperview];
        }
        else
        {
           [_mainContainerView deleteCurrentTool:[shapesArray lastObject]];
            [shapesArray removeLastObject];
        }
    }
    
    else
    {
    [_mainContainerView clearTools];
   }
    

}

- (void) performUndoAll
{
    while (shapesArray.count>0) {
        [self perfromUndo];
    }
}


-(void)handleThumbnailImageRequestFinishNotification:(NSNotification*)notification
{
    NSDictionary *userinfo = [notification userInfo];
    NSError* value = [userinfo objectForKey:MPMoviePlayerThumbnailErrorKey];
    if (value != nil)
    {
        NSLog(@"Error creating video thumbnail image. Details: %@", [value debugDescription]);
    }
    else
    {
        UIImage *thumbnail = [userinfo valueForKey:MPMoviePlayerThumbnailImageKey];
        NSNumber *time=[userinfo objectForKey:MPMoviePlayerThumbnailTimeKey];
        NSString *fileName=[NSString stringWithFormat:@"thumb%f.jpg",time.doubleValue];
        NSString *filePath=[NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        
        NSData *jpegData=UIImageJPEGRepresentation(thumbnail, 1.0);
        [jpegData writeToFile:filePath atomically:NO];
        
        [[OrbisMediaGallery sharedGallery] savePhotoToGalleryAtPath:filePath];
        
        UILocalNotification *notification=[[UILocalNotification alloc] init];
        notification.alertBody=@"Snapshot saved";
        notification.fireDate=[NSDate date];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

- (UIImage*)generateThumbnailForCurrentVideoAtTime:(int)playbacktime
{
    NSLog(@"playbacktime: %d", playbacktime);
    UIImage *thumbImage=nil;
    NSError *err = nil;
    CMTime time = CMTimeMake(playbacktime, 1);
    CGImageRef imageRef = [_imageGenerator copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imageRef);
    thumbImage = [[UIImage alloc] initWithCGImage:imageRef];
    thumbImage = [[OrbisMediaGallery sharedGallery] rotateImage:thumbImage byDegrees:90.0f];
    CGImageRelease(imageRef); // CGImageRef won't be released by ARC
    
    return thumbImage;
}

#pragma mark - Resize cgimageref Method
- (CGImageRef)resizeCGImage:(CGImageRef)image toWidth:(int)width andHeight:(int)height {
    // create context, keeping original image properties
    CGColorSpaceRef colorspace = CGImageGetColorSpace(image);
    CGContextRef context = CGBitmapContextCreate(NULL, width, height,
                                                 CGImageGetBitsPerComponent(image),
                                                 CGImageGetBytesPerRow(image),
                                                 colorspace,
                                                 CGImageGetAlphaInfo(image));
                                              CGColorSpaceRelease(colorspace);
    if(context == NULL)
        return nil;
    
    // draw image to context (resizing it)
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
    // extract resulting image from context
    CGImageRef imgRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    return imgRef;
}


#pragma mark - UIActionSheetDelegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    /*
     NSInteger cancelButtonIndex=1;
     if([_videoInfo objectForKey:@"lessonID"] && [[_videoInfo objectForKey:@"lessonID"] integerValue]>0)
     {
     cancelButtonIndex=2;
     }
     
     if(buttonIndex==cancelButtonIndex)
     return;
     
     if(buttonIndex==0)  // SAVE WITH OVERLAYS
     {
     [self saveOverlay];
     }
     else if(buttonIndex==1)     // SAVE TO LESSON
     {
     AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
     MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
     NSString *jsonString=[self getJSONString:_videoInfo];
     [[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion:^{
     [mvc.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.lessonVideosPage.uploadVideoWithInfo(%@);", jsonString]];
     }];
     }
     */
}

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //    NSInteger cancelButtonIndex=2;
    if([_videoInfo objectForKey:@"lessonID"] && [[_videoInfo objectForKey:@"lessonID"] integerValue]>0)
    {
        //        cancelButtonIndex=3;
        switch (buttonIndex) {
            case 0:  // SAVE WITH OVERLAYS
            {
                [self saveOverlay];
            }
                break;
            case 1:    // SAVE TO LESSON
            {
                // CHM Jawad
//                AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//                MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//                NSString *jsonString=[self getJSONString:_videoInfo];
//                [[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion:^{
//                    [mvc.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.lessonVideosPage.uploadVideoWithInfo(%@);", jsonString]];
//                }];
                
                [GlobalClass sharedInstance].shouldAllowLandscape = NO;
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    if (_delegate && [_delegate respondsToSelector:@selector(shapeViewController:didSelectSavetoLesson:)]) {
                        [_delegate shapeViewController:self didSelectSavetoLesson:_videoInfo];
                    }
                }];
            }
                break; // VIEW OVERLAYS
            case 2:
            {
                
                self.rotationVal = 0;
                [self performSelector:@selector(displayOverlyViewController) withObject:nil afterDelay:0.5];
            }
                break;
            case 3: // CANCEL
                
                break;
        }
        
    }
    else
    {
        switch (buttonIndex) {
            case 0:  // SAVE WITH OVERLAYS
            {
                [self saveOverlay];
            }
                break;
            case 1: // VIEW OVERLAYS
            {
                self.rotationVal = 0;
                [self performSelector:@selector(displayOverlyViewController) withObject:nil afterDelay:0.5];
                
            }
                break;
            case 2: // Upload to locaker
            {
               //holy //option added below can removed
                NSLog(@"%@",_videoInfo);
                if (_delegate == nil) {
                    return;
                    break;
                }else{
                [GlobalClass sharedInstance].shouldAllowLandscape = NO;
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    if (_delegate && [_delegate respondsToSelector:@selector(shapeViewController:didSelectSavetoLesson:)]) {
                        [_delegate shapeViewController:self didSelectSavetoLesson:_videoInfo];
                    }
                }];
                }

                
                
            }
                break;
            case 3: // CANCEL
                return;
                break;
        }
    }
    
    //    if(buttonIndex==cancelButtonIndex)
    //        return;
    //
    //    if(buttonIndex==0)  // SAVE WITH OVERLAYS
    //    {
    //        [self saveOverlay];
    //    }
    //    else if(buttonIndex==1)     // SAVE TO LESSON
    //    {
    //        AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    //        MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
    //        NSString *jsonString=[self getJSONString:_videoInfo];
    //        [[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion:^{
    //            [mvc.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.lessonVideosPage.uploadVideoWithInfo(%@);", jsonString]];
    //        }];
    //    }
    
}

-(void)displayOverlyViewController
{
    OverlaysViewController *overlayViewController=[[OverlaysViewController alloc] initWithNibName:@"OverlaysViewController" bundle:nil thumbnailImagePath:[_videoInfo objectForKey:@"videoThumbnailPath"] overlaysArray:[[OrbisMediaGallery sharedGallery] getOverlaysForVideoAtPath:[_videoInfo objectForKey:@"videoPath"]] presentingController:self];
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    overlayViewController.dOrientation = orientation;
    overlayViewController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:overlayViewController animated:YES completion:nil];
    
    
}


- (IBAction)playbackRateChanged:(id)sender {
    UIButton *b=(UIButton*)sender;
    float newPlaybackRate=(float)b.tag/40.0f;
    if(newPlaybackRate<0)
        newPlaybackRate=0.0;
    
    [self resetPlaybackRateControls];
    
    b.selected=YES;
    b.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    
    currentPlaybackRate=newPlaybackRate;
    //    deprecations
/*   moviePlayerViewController.moviePlayer.currentPlaybackRate=newPlaybackRate;
    [moviePlayerViewController.moviePlayer prepareToPlay];
    //    deprecations
    [moviePlayerViewController.moviePlayer play];*/
}

- (void) resetPlaybackRateControls
{
    UIButton *b1=(UIButton*)[playbackControlsView viewWithTag:10];
    b1.selected=NO;
    b1.backgroundColor=[UIColor clearColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIButton *b2=(UIButton*)[playbackControlsView viewWithTag:20];
    b2.selected=NO;
    b2.backgroundColor=[UIColor clearColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIButton *b3=(UIButton*)[playbackControlsView viewWithTag:30];
    b3.selected=NO;
    b3.backgroundColor=[UIColor clearColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIButton *b4=(UIButton*)[playbackControlsView viewWithTag:40];
    b4.selected=NO;
    b4.backgroundColor=[UIColor clearColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0];
}

#pragma mark - set frames
-(void)setButtonsFrames
{
    int emptySpace = superView.frame.size.height - (btn1.frame.size.height * 5);
    NSLog(@"empty space %i",emptySpace);
    int topSpace  = emptySpace / 6;
    btn1.frame = CGRectMake(btn1.frame.origin.x, topSpace, btn1.frame.size.width, btn1.frame.size.height);
    
    int btn2topSpace = (2*topSpace)+btn1.frame.size.width;
    NSLog(@"btn2 top space empty space %i",btn2topSpace);
    btn2.frame = CGRectMake(btn2.frame.origin.x, btn2topSpace, btn2.frame.size.width, btn2.frame.size.height);
    
    int btn3topSpace = (3*topSpace)+btn1.frame.size.width;
    NSLog(@"btn3 top space empty space %i",btn3topSpace);
    btn3.frame = CGRectMake(btn3.frame.origin.x, btn3topSpace, btn3.frame.size.width, btn3.frame.size.height);
    
    int btn4topSpace = (4*topSpace)+btn1.frame.size.width;
    NSLog(@"btn4 top space empty space %i",btn4topSpace);
    btn4.frame = CGRectMake(btn1.frame.origin.x, btn4topSpace, btn4.frame.size.width, btn4.frame.size.height);
    
    int btn5topSpace = (5*topSpace)+btn1.frame.size.width;
    NSLog(@"btn5 top space empty space %i",btn5topSpace);
    btn5.frame = CGRectMake(btn1.frame.origin.x, btn5topSpace, btn5.frame.size.width, btn5.frame.size.height);
    
    
    
}
/*
 
 #pragma mark - check device orientation change
 -(void)didRotate:(NSNotification *)notification {
 
 UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
 
 if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
 
 orientation = newOrientation;
 
 }
 
 // Do your orientation logic here
 
 if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
 
 // Clear the current view and insert the orientation specific view.
 
 [self clearCurrentView];
 
 [self.view insertSubview:mainLandscapeView atIndex:0];
 
 } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
 
 // Clear the current view and insert the orientation specific view.
 
 [self clearCurrentView];
 
 [self.view insertSubview:mainPortraitView atIndex:0];
 
 }
 
 
 }

 -(void) clearCurrentView {
 
 if (mainLandscapeView.superview) {
 
 [mainLandscapeView removeFromSuperview];
 
 } else if (mainPortraitView.superview) {
 
 [mainPortraitView removeFromSuperview];
 
 }
 
 }
 */

#pragma mark - hiden views
-(void)hideViews
{
    
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
    if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight )))  {
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options:UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             // setting frame for toobarview
                             //   isVPlay = NO;
                             [self hideView];
                             
                         //   CGFloat  newYOrigin= ((self.view.frame.size.height-superView.frame.size.height )+20)/2;
                             superView.frame = CGRectMake(self.view.frame.size.width, superView.frame.origin.y, superView.frame.size.width, superView.frame.size.height);
                            // [superView setHidden:YES];
                             
                             
                             
                         }
                         completion:^(BOOL finished){
                         }];
    }
    
    else
    {
    
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options:UIViewAnimationCurveEaseIn
                         animations:^{
                     
                               [self hideView];
                             // set frame for playback control view
                             _bottomMenuContainerView.frame = CGRectMake(0, self.view.frame.size.height, _bottomMenuContainerView.frame.size.width , _bottomMenuContainerView.frame.size.height);
}
                         completion:^(BOOL finished){
                         }];
}
    
    
}


-(void)hideView
{
    isViewHidden = YES;
    [self.navigationItem setRightBarButtonItem:nil animated:YES];
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    CGRect frame=self.navigationController.navigationBar.frame;
    frame.size.height=0;
    self.navigationController.navigationBar.frame=frame;
    
    // set frame for playback control view
    playbackControlsView.frame = CGRectMake(0, self.view.frame.size.height, playbackControlsView.frame.size.width , playbackControlsView.frame.size.height);

}


#pragma mark - show view
-(void)showViews
{

        [self showStatusBar];

}


-(void)showStatusBar
{
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
    if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight )))  {
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             //        if (!isVPlay) {
                             // setting frame for toobarview
                             isViewHidden = NO;
                             CGRect frame=self.navigationController.navigationBar.frame;
                             frame.size.height=30.0;
                             self.navigationController.navigationBar.frame=frame;
                             
                             UIBarButtonItem * rightDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"lines_3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];
                             [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:NO];
                             
                             UIBarButtonItem * leftDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arr_left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
                             leftDrawerButton.imageInsets=UIEdgeInsetsMake(2, 0, 0, 0);
                             
                             [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:NO];
                             
                             superView.frame = CGRectMake(self.view.frame.size.width - superView.frame.size.width,  superView.frame.origin.y, superView.frame.size.width, superView.frame.size.height);
                         
                            // [superView setHidden:NO];
                             //   toolBarVw.frame = CGRectMake(0, self.view.frame.origin.y, playbackControlsView.frame.size.width , playbackControlsView.frame.size.height);
                             
                             
                             // set frame for playback control view
                             playbackControlsView.frame = CGRectMake(0, self.view.frame.size.height-playbackControlsView.frame.size.height, playbackControlsView.frame.size.width , playbackControlsView.frame.size.height);
                             //       }
                         }
                         completion:^(BOOL finished){
                         }];

    }
    
    else
    {
    
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             //        if (!isVPlay) {
                             // setting frame for toobarview
                             isViewHidden = NO;
                             CGRect frame=self.navigationController.navigationBar.frame;
                             frame.size.height=30.0;
                             self.navigationController.navigationBar.frame=frame;
                             
                             UIBarButtonItem * rightDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"lines_3.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];
                             [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:NO];
                             
                             UIBarButtonItem * leftDrawerButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arr_left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
                             leftDrawerButton.imageInsets=UIEdgeInsetsMake(2, 0, 0, 0);
                             
                             [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:NO];
                           
                             //   toolBarVw.frame = CGRectMake(0, self.view.frame.origin.y, playbackControlsView.frame.size.width , playbackControlsView.frame.size.height);
                             
                             
                             // set frame for playback control view
                             playbackControlsView.frame = CGRectMake(0, self.view.frame.size.height-(playbackControlsView.frame.size.height+_bottomMenuContainerView.frame.size.height), playbackControlsView.frame.size.width , playbackControlsView.frame.size.height);
                             
                                 _bottomMenuContainerView.frame = CGRectMake(0, self.view.frame.size.height-_bottomMenuContainerView.frame.size.height, _bottomMenuContainerView.frame.size.width , _bottomMenuContainerView.frame.size.height);
                             
                             
                             //       }
                         }
                         completion:^(BOOL finished){
                         }];
}}


#pragma mark -  rotation delegates

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}
/*
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

    if (isVideoAnalysisDone) {
      
            NSNumber *value = [NSNumber numberWithInt:currentOrientation];
            [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
     
    }

}

*/
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( _mmDrawerController.isVideoAnalysisDone == NO) {
        [self setFrames];
        
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight )))   {
            
            [self getLandScapeXib];
        }
        else {
            
            [self getPortraitXib];
        }
        
        
        self.rotationVal = 1;
        
        [self viewDidLoad];
        [self viewWillAppear:YES];
    }
    

    

    
}

-(void)reloadView
{
    
   
    
    if ( _mmDrawerController.isVideoAnalysisDone == NO) {
        [self setFrames];
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication ] statusBarOrientation];
        if (UIDeviceOrientationIsLandscape(orientation) || ( (orientation ==  UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp)  && (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight ))) {
            
            [self getLandScapeXib];
        }
        else {
            
            [self getPortraitXib];
            
        }
        [self viewWillDisappear:YES];
        [self viewDidLoad];
        [self viewWillAppear:YES];

    }
    
    /*
    else
    {
        NSNumber *value = [NSNumber numberWithInt:currentOrientation];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    }
    
     */
    
}

-(void)getLandScapeXib
{
    
    // CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if(  deviceWidth >= 568 || deviceHeight >= 568 )    {
        
        [[NSBundle mainBundle] loadNibNamed:@"ShapeViewController_Landscape" owner:self options:nil];
    }
    else
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"ShapeViewController_Landscape_4s" owner:self options:nil];
    }
    
    
}


-(void)getPortraitXib
{
    
    //   CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    NSLog(@"%f%f",deviceWidth,deviceHeight);;
    if(  deviceWidth >= 568 || deviceHeight >= 568 ) {
        
        [[NSBundle mainBundle] loadNibNamed:@"ShapeViewController_5" owner:self options:nil];
        
        
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed:@"ShapeViewController" owner:self options:nil];
    }
    
    
}

-(void)setFrames
{
    
//      AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//    appDelegate.changeVal = 0;
    [GlobalClass sharedInstance].changeVal = 0;
    
    [dicA removeAllObjects];
    NSLog(@"%f %f",_mainContainerView.center.x,_mainContainerView.center.y);
    
    for (int i = 0 ; i < shapesArray.count ; i++) {
        if ([[shapesArray objectAtIndex:i] isKindOfClass:[SPUserResizableView class] ]) {
            SPUserResizableView *shapeView=[shapesArray objectAtIndex:i];
            CGAffineTransform transform = shapeView.transform;
            NSValue *transformVal = [NSValue valueWithCGAffineTransform:transform];
            
        CGFloat percentW = (shapeView.frame.size.width/_mainContainerView.frame.size.width) * 100;
        CGFloat percentH = (shapeView.frame.size.height/_mainContainerView.frame.size.height) * 100;
        CGFloat perOriginX = (shapeView.frame.origin.x/_mainContainerView.frame.size.width) *100;
        CGFloat perOriginY = (shapeView.frame.origin.y/_mainContainerView.frame.size.height) *100;
            
            CGFloat rotatedAngle = [shapeView getTotalRotatedAngle];
            NSDictionary *percentDic =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:percentW],@"width",[NSNumber numberWithFloat:percentH],@"height",[NSNumber numberWithFloat:perOriginX],@"originx",[NSNumber numberWithFloat:perOriginY],@"originy",transformVal,@"transform",[NSNumber numberWithFloat:rotatedAngle ],@"rotatedangle",[NSValue valueWithCGRect:shapeView.frame],@"previousframe", nil];
            [dicA addObject:percentDic];
}
    else
    {
        
       if ([[shapesArray objectAtIndex:i]isKindOfClass:[MDrawLine class]]) {
            
            MDrawLine *lineObj=[shapesArray objectAtIndex:i];
           
            CGFloat perSPX = (lineObj.startPoint.x/_mainContainerView.frame.size.width) * 100;
            CGFloat perEPX = (lineObj.endPoint.x/_mainContainerView.frame.size.width) * 100;
            CGFloat perSPY = (lineObj.startPoint.y/_mainContainerView.frame.size.height) *100;
            CGFloat perEPY = (lineObj.endPoint.y/_mainContainerView.frame.size.height) *100;
            
            NSDictionary *percentDic =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:perSPX],@"spx",[NSNumber numberWithFloat:perEPX],@"epx",[NSNumber numberWithFloat:perSPY],@"spy",[NSNumber numberWithFloat:perEPY],@"epy", nil];
            [dicA addObject:percentDic];
        }
        else
        {
            MDrawAngle *angleObj=[shapesArray objectAtIndex:i];
            CGPoint p1  = [angleObj.points[0] CGPointValue];
            CGPoint p2  = [angleObj.points[1] CGPointValue];
            CGPoint p3  = [angleObj.points[2] CGPointValue];
            
        CGFloat p1X = (p1.x/_mainContainerView.frame.size.width) * 100;
        CGFloat p1Y =  (p1.y/_mainContainerView.frame.size.height) * 100;
        CGFloat p2X =  (p2.x/_mainContainerView.frame.size.width) *100;
        CGFloat p2Y =  (p2.y/_mainContainerView.frame.size.height) *100;
        CGFloat p3X =  (p3.x/_mainContainerView.frame.size.width) *100;
        CGFloat p3Y =  (p3.y/_mainContainerView.frame.size.height) *100;
            
      
       NSDictionary *percentDic =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:p1X],@"p1x",[NSNumber numberWithFloat:p1Y],@"p1y",[NSNumber numberWithFloat:p2X],@"p2x",[NSNumber numberWithFloat:p2Y],@"p2y",[NSNumber numberWithFloat:p3X],@"p3x",[NSNumber numberWithFloat:p3Y],@"p3y", nil];
            [dicA addObject:percentDic];
  }
 }
 
}
    }

-(CGAffineTransform) CGAffineTransformFromRectToRect:(CGRect) fromRect andRactTwo:(CGRect) toRect
{
    
    CGAffineTransform trans1 = CGAffineTransformMakeTranslation(-fromRect.origin.x, -fromRect.origin.y);
    CGAffineTransform scale = CGAffineTransformMakeScale(toRect.size.width/fromRect.size.width, toRect.size.height/fromRect.size.height);
    CGAffineTransform trans2 = CGAffineTransformMakeTranslation(toRect.origin.x, toRect.origin.y);
    return CGAffineTransformConcat(CGAffineTransformConcat(trans1, scale), trans2);
    
 
}

@end


