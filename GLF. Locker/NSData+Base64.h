//
//  NSData+Base64.h
//  Orbis
//
//  Created by Nasir Mehmood on 29/04/2014.
//
//

#import <Foundation/Foundation.h>

@interface NSData(Base64)

- (NSString *)base64EncodedString;

@end
