//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
class LessonsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var dataComingLessons:[[String:AnyObject]] = [[String:AnyObject]]()
    var dataPreviosLessons:[[String:AnyObject]] = [[String:AnyObject]]()
    var dataGeneral:[[String:AnyObject]] = [[String:AnyObject]]()
    var upComing = false
    var studentData = [String:AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    var fromDashBoard = false
    var map:GMSMapView?
    @IBOutlet weak var searchFld: UITextField!
    @IBOutlet weak var footerHeigthViewConstraint: NSLayoutConstraint!
    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()
        print(studentData)
        self.nextView.backgroundColor = UIColor.init(red: 199.0/255, green: 209/255, blue: 44.0/255, alpha: 1.0)
        let view1 = self.tabBarController?.view.viewWithTag(999)
        view1?.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        //GetAllAcademiesOrderByLatLong
       settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(LessonsViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
     

    }
    
        override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
        if fromDashBoard {
            footerHeigthViewConstraint.constant = 0
            refreshTableViewForCoachLessons()
            self.navigationItem.title = "UPCOMING LESSON"
            
        }else{
            
            if currentUserLogin == 4{
            }else{
                self.tabBarController?.navigationController?.isNavigationBarHidden = true
            }
            
        if upComing {
            
        }else{
            refreshTableViewForUpComming()
        }
        }

    }
    
    //MARK:- fetching server data
    func refreshTableViewForUpComming()  {
        
        var studentId = ""
        if currentUserLogin == 4 {
            if let id = DataManager.sharedInstance.currentUser()!["playerId"] as? Int{
                studentId = "\(id)"
            }
        }else{
        if let id = studentData["StudentID"] as? Int{
            studentId = "\(id)"
            }
        }
//        https://app.glflocker.com/OrbisAppBeta/api/Academy/GetPreviousLessons/73/4689
        //localechanges
        var parameters = ""
       
        parameters = "\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(studentId)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "academy/GetUpcommingLessons/\(parameters)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                     return
            }
            self.dataComingLessons = object as! [[String : AnyObject]]
            self.dataGeneral = self.dataComingLessons
            self.tableView.reloadData()
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func refreshTableViewForPrevious()  {
       
        var studentId = ""
        if currentUserLogin == 4 {
            if let id = DataManager.sharedInstance.currentUser()!["playerId"] as? Int{
                studentId = "\(id)"
            }
        }else{
            if let id = studentData["StudentID"] as? Int{
                studentId = "\(id)"
            }
        }
        //localechanges
        var parameters = ""
        
        parameters = "\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(studentId)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "academy/GetPreviousLessons/\(parameters)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            self.dataPreviosLessons = object as! [[String : AnyObject]]
             self.dataGeneral = self.dataPreviosLessons
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func refreshTableViewForCoachLessons()  {
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        ////http://app.glfbeta.com/OrbisWebApi/api/Academy/GetCoachUpcommingLessons/73/4163
        //localechanges
        var parameters = ""
        
        parameters = "\(acadmyId as AnyObject)/\(userId)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetCoachUpcommingLessons/\(parameters)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            self.dataComingLessons = object as! [[String : AnyObject]]
            self.dataGeneral = self.dataComingLessons
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataGeneral.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonsTableViewCell", for: indexPath) as! LessonsTableViewCell
        if let title = dataGeneral[indexPath.row]["LessonName"] as? String{
            cell.companyLbl.text = title
        }
        
        if let date = dataGeneral[indexPath.row]["LessonDate"] as? String{
            
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                cell.dateLbl.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
                
                
            }
          
        }
        if let sTime = dataGeneral[indexPath.row]["StartTime"] as? String{
           
            if let eTime = dataGeneral[indexPath.row]["EndTime"] as? String{
                var startTime =  sTime
                 var endTime =  eTime
    
              
                
                //                localechanges
                if is12Houre == "true"{
                    
                }else{
                    
                    if (startTime.count) > 5 {
                        startTime = String(startTime.dropLast(3))
                    }
                    if (endTime.count) > 5 {
                        endTime = String(endTime.dropLast(3))
                    }
                    
                }
                
                cell.timeLbl.text = "from " + startTime + " to " + endTime
                
                    cell.timeLbl.text = "from " + startTime + " to " + endTime
               
            }

        }
        
        if let url = dataGeneral[indexPath.row]["LessonImage"] as? String{
            var originalUrl = url
            originalUrl = imageBaseUrl + originalUrl
            cell.studentImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
                
                DispatchQueue.main.async (execute: {
                    //                    self.activityIndicator.stopAnimating();
                    if let _ = image{
                        
                        cell.studentImage.image = image;
                    }
                    else{
                        cell.studentImage.image = UIImage(named:"photoNot")
                    }
                });
                
            })
            
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
             let currentCell = tableView.cellForRow(at: indexPath) as! LessonsTableViewCell
        if fromDashBoard{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AppointMentViewController") as! AppointMentViewController
            controller.previousData = dataGeneral[indexPath.row]
            controller.userImageData = currentCell.studentImage.image!
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        
        if currentUserLogin == 4 {
        
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AppointMentViewController") as! AppointMentViewController
            controller.previousData = dataGeneral[indexPath.row]
            controller.userImageData = currentCell.studentImage.image!
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AppointMentViewController") as! AppointMentViewController
            controller.previousData = dataGeneral[indexPath.row]
            controller.userImageData = currentCell.studentImage.image!
            self.navigationController?.pushViewController(controller, animated: true)
        }

    }
    
    //MARK:- Buttons Actions
    
    @IBOutlet weak var previousView: UIView!
    
    @IBOutlet weak var nextView: UIView!
    @IBAction func nextBtnAction(_ sender: UIButton) {
        
         upComing = false
        self.nextView.backgroundColor = UIColor.init(red: 199.0/255, green: 209/255, blue: 44.0/255, alpha: 1.0)
        self.previousView.backgroundColor = UIColor.init(red: 55.0/255, green: 55/255, blue: 55.0/255, alpha: 1.0)
        
        if upComing {
            
        }else{
            self.refreshTableViewForUpComming()
        }
    }
    @IBAction func previousBtnAction(_ sender: UIButton) {
        upComing = true
         self.previousView.backgroundColor = UIColor.init(red: 199.0/255, green: 209/255, blue: 44.0/255, alpha: 1.0)
         self.nextView.backgroundColor = UIColor.init(red: 55.0/255, green: 55/255, blue: 55.0/255, alpha: 1.0)
        if upComing {
               self.refreshTableViewForPrevious()
        }else{
        }
    }
    

}

