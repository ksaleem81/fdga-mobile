//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class RecommendedTableViewCell: UITableViewCell {

    @IBOutlet weak var lessonTitleLbl: UILabel!
    @IBOutlet weak var lessonImage: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var coachLbl: UILabel!
    @IBOutlet weak var bookBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lessonImage.layer.borderWidth = 0.25
        lessonImage.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
