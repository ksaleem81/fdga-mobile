//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit



class PaymentStatusForCoachTableViewCell: UITableViewCell,UITextFieldDelegate {


    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var radioBtn: UIButton!
   
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
        // Initialization code
     
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
