//
//  PastViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/9/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu

class AddCoachScheduleViewController: UIViewController {

    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    var durationId = ""
    var selectedDay = 0
    var adhocScheduleOn = false
    var dayDatePickerView = DayDatePickerView()
    var inputView1 = UIView()
    var selectedDate = ""
    @IBOutlet weak var dayOrDateLbl: UILabel!
    var academyScheduleData = [String:AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingRighMenuBtn()
        if adhocScheduleOn {
            dayLbl.text = "Select Date"
            dayOrDateLbl.text = "Date"
            self.title = "ADD ADHOC SCHEDULE"
        }
    }
    
    func settingRighMenuBtn()  {
    
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AddCoachScheduleViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func lessonDayBtnAction(_ sender: UIButton) {
        

        if adhocScheduleOn{
            addDayPicker()
        }else{
        
        let dataAray = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"]
        ActionSheetStringPicker.show(withTitle: "SELECT DAY", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.dayLbl.text = "\(indexes!)"
            self.selectedDay = values + 1
            print(self.selectedDay)
            //dynamicChange
            if isNewUrl{
            Utility.getScheduleTimeOfAcademy(selectedDay: "\(self.selectedDay)", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
                
            }, onFailure: {(error) in
                
            }
                )
                
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
            
        }
    }

 
    
    @IBAction func startTimeBtnAction(_ sender: UIButton) {
        
        let formator = DateFormatter()
        var picker =  ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in

            //            localechanges
            if is12Houre == "true"{
                formator.dateFormat = "hh:mm a"
                
            }else{
                formator.dateFormat = "HH:mm"
            }
            //for start time
            let selectedTimeCurrent = selectedTime as! NSDate
            let endTitle = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            //for end time
            self.startTimeLbl.text = endTitle
        }, cancel: { (picker) -> Void in
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
            
        }else{
           picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
            
        }

        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
        picker?.show()
    }
    
    @IBAction func endTimeBtnAction(_ sender: UIButton) {
        
       let formator = DateFormatter()
       var picker = ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in

        //        localechanges
        if is12Houre == "true"{
            formator.dateFormat = "hh:mm a"
            
        }else{
            formator.dateFormat = "HH:mm"
        }

        //for start time
            let selectedTimeCurrent = selectedTime as! NSDate
            let endTitle = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            //for end time
            self.endTimeLbl.text = endTitle
        }, cancel: { (picker) -> Void in
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
        }
        
        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
        picker?.show()
    }
    

     func convertStrinToDate(time:String)->Date{

        let dateFormatter = DateFormatter()
        //localechanges
        if is12Houre == "true"{
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        }else{
            if time.count < 17{
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            }else{
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            }
        }
        let endDate = (DataManager.sharedInstance.getTodayDate())
        let dateSelected = endDate
        let item = dateSelected +  " " +  time
        let date = dateFormatter.date(from: item)
        return date!// Start: Optional(2000-01-01 19:00:00 +0000)

    }

    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        messageStr = ""
        validateData()

        if messageStr.count > 0{
            
            let alertController = UIAlertController(title: "Error", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
            
            let leaveReturnTime  = convertStrinToDate(time: endTimeLbl.text!)
            
            let leaveStartTime  =  convertStrinToDate(time: startTimeLbl.text!)
            
            if (startTimeLbl.text! == endTimeLbl.text!) || leaveReturnTime < leaveStartTime {
                let alertController = UIAlertController(title: "Error", message: "Select valid start and end time", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
        }

        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }
        
        var parameters  = [String:AnyObject]()
        var urlStr = ""
        //        localechanges

        if adhocScheduleOn {
            parameters = ["ScheduleDate":"\(selectedDate)" as AnyObject,"CoachID":userId as AnyObject!,"CoachScheduleId":"0" as AnyObject!,"startTime":startTimeLbl.text! as AnyObject!,"endTime":endTimeLbl.text! as AnyObject!,"Is12HoursTimeFormat" : "\(is12Houre)" as AnyObject]
            urlStr = "SaveCoachAdhcoSchedule"
        }else{
            parameters = ["dayOfWeek":"\(selectedDay)" as AnyObject,"CoachID":userId as AnyObject!,"CoachScheduleId":"0" as AnyObject!,"startTime":startTimeLbl.text! as AnyObject!,"endTime":endTimeLbl.text! as AnyObject!,"Is12HoursTimeFormat" : "\(is12Houre)" as AnyObject]
            urlStr = "SaveCoachSchedual"
        }
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "Academy/\(urlStr)", parameter:parameters as [String : AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
            break
            }
            if let responce = object as? Int{
                _ = self.navigationController?.popViewController(animated: true)
                if responce == 0 {
                    
                    var mesgeToPrint = ""
                    if self.adhocScheduleOn{
                        mesgeToPrint = "Successfully added adhoc schedule"
                    }else{
                        mesgeToPrint = "Successfully added schedule"
                    }
                   DataManager.sharedInstance.printAlertMessage(message:mesgeToPrint, view:UIApplication.getTopestViewController()!)

                }else {
                DataManager.sharedInstance.printAlertMessage(message:"Unavailable slot", view:UIApplication.getTopestViewController()!)
                    return
                }
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
       _ = self.navigationController?.popViewController(animated: true)
    }
        
    var messageStr = ""
    
    func validateData()  {
        
        if adhocScheduleOn {
            if selectedDate == "" {
                messageStr = messageStr + "Select Date \n"
            }
        }else{
            if dayLbl.text == "SELECT DAY" {
                messageStr = messageStr + "Select Day \n"
            }
        }
        
        if self.startTimeLbl.text == "START TIME" {
            messageStr = messageStr + "Select Start Time \n"
        }
        if self.endTimeLbl.text == "END TIME" {
                 messageStr = messageStr + "Select End Time \n"
        }
    }

 func addDayPicker (){
    
    dayDatePickerView =  DayDatePickerView.init(frame: CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 210))
    let doneButton = UIButton(frame: CGRect(x:self.view.frame.width - 100, y:5, width:100, height:30))
    doneButton.setTitle("Done", for: UIControlState.normal)
    doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    doneButton.setTitleColor(UIColor.init(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0), for: .normal)
    inputView1 = UIView(frame: CGRect(x:0, y:self.view.frame.height - 250, width:self.view.frame.width, height:250))
    inputView1.backgroundColor = UIColor.white
    inputView1.addSubview(doneButton) // add Button to UIView
    doneButton.addTarget(self, action: #selector(AddCoachScheduleViewController.doneBtnAction), for: UIControlEvents.touchUpInside)
    let cancelBtn = UIButton(frame: CGRect(x:10, y:5, width:100, height:30))
    cancelBtn.setTitle("CANCEL", for: UIControlState.normal)
    
    cancelBtn.setTitleColor(UIColor.init(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0), for: .normal)
    cancelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    inputView1.addSubview(cancelBtn)
    cancelBtn.addTarget(self, action: #selector(AddCoachScheduleViewController.cancelBtnAction2), for: UIControlEvents.touchUpInside)
    dayDatePickerView.minimumDate = NSDate(timeIntervalSince1970: 0.0/1000.0) as Date!
    dayDatePickerView.maximumDate = NSDate(timeIntervalSince1970: 4432233446145.0/1000.0) as Date!
    dayDatePickerView.delegate = self
    dayDatePickerView.date = NSDate() as Date
    inputView1.addSubview(dayDatePickerView)
    self.view.addSubview(inputView1)
    
}
    
@objc func doneBtnAction()  {
    print()
    
    let formator = DateFormatter()
    formator.dateFormat = "dd MMM, yyyy"
    
    let date = dayDatePickerView.date as NSDate
    let title = formator.string(from: date as Date)
    
    selectedDate = title
    
    self.dayLbl.text =  DataManager.sharedInstance.getFormatedDate(date: title,formate: "dd MMM, yyyy")
    
    //dynamicChange
    if isNewUrl{
        
        Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(selectedDate,dateFormater:"dd MMM, yyyy"))", controller: self, Success: { data  in
            self.academyScheduleData = data as! [String : AnyObject]
        }, onFailure: {(error) in
            
        })
        
    }

    
    inputView1.removeFromSuperview()
}
@objc func cancelBtnAction2()  {
    inputView1.removeFromSuperview()
}
}
extension AddCoachScheduleViewController : DayDatePickerViewDelegate{
    
    func dayDatePickerView(_ dayDatePickerView: DayDatePickerView!, didSelect date: Date!) {
        
    }
}
extension Date{

    func roundTime(date:Date,minuteInterval:NSInteger)->Date {
    let referenceTimeInterval = date.timeIntervalSinceReferenceDate
    let remainingSeconds = referenceTimeInterval.truncatingRemainder(dividingBy: TimeInterval(minuteInterval*60))
    let timeRoundedToInterval = referenceTimeInterval - remainingSeconds
    return Date(timeIntervalSinceReferenceDate: timeRoundedToInterval)
}

}



