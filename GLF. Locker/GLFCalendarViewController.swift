//
//  GLFCalendarViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/27/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import FSCalendar
import SideMenu

class GLFCalendarViewController: UIViewController {

    @IBOutlet weak var calnderView: FSCalendar!
    @IBOutlet weak var tableView: UITableView!
    var dataArray = [[String:AnyObject]]()
    var programsArray = [[String:AnyObject]]()
    override func viewDidLoad() {
        super.viewDidLoad()

     
        // Do any additional setup after loading the view.
//         calnderView.appearance.headerMinimumDissolvedAlpha = 0.0
        if let programs = DataManager.sharedInstance.currentAcademy()!["AcademyProgramTypes"] as? [[String:AnyObject]]{
            programsArray = programs
        }
       
        if Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() {
            calnderView.translatesAutoresizingMaskIntoConstraints = true
            calnderView.frame  = CGRect.init(x: 0, y: 85, width: self.view.frame.size.width, height: calnderView.frame.size.height)
        }
        
        calnderView.appearance.headerMinimumDissolvedAlpha = 0.0;
        print(programsArray)
        getLesssonsAndEvents()
        settingRighMenuBtn()
        addingLeftRightArrowsOnCalendar()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    func addingLeftRightArrowsOnCalendar()  {
        
        let leftArrow = UIButton.init(frame: CGRect(x: 5, y: 10, width: 30, height: 30))
        leftArrow.setImage(UIImage(named:"arr_left")!, for: .normal)
        leftArrow.addTarget(self, action: #selector(leftBtnTap(btn:)), for:UIControlEvents.touchUpInside)
    
        leftArrow.contentMode = .scaleAspectFit
        calnderView.addSubview(leftArrow)
        let rightArrow = UIButton.init(frame: CGRect(x: self.view.frame.maxX-35, y: 10, width: 30, height: 30))
        rightArrow.addTarget(self, action: #selector(rightBtnTap(btn:)), for:UIControlEvents.touchUpInside)
        rightArrow.setImage(UIImage(named:"arr_right")!, for: .normal)
        rightArrow.contentMode = .scaleAspectFit
        calnderView.addSubview(rightArrow)
        
    }
 
    @objc func leftBtnTap(btn:UIButton) {
        
       let date1 =  calnderView.currentPage  - 1
         calnderView.setCurrentPage(date1, animated: true)
    }
    
    @objc func rightBtnTap(btn:UIButton) {
        
        let date1 =  calnderView.currentPage
        let date2 = Calendar.current.date(byAdding: .month, value: 1, to: date1)
        calnderView.setCurrentPage(date2!, animated: true)
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(GLFCalendarViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getLesssonsAndEvents()  {
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }

        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let TodayDate = formatter.string(from: date)
        
        //        localechanges
        var urlStr = ""
        if is12Houre == "true"{
            urlStr = "GetLessonsAndEventsForCalendarNew"
        }else{
            urlStr = "GetLessonsAndEventsForCalendar"
        }
        
        NetworkManager.performRequest(type:.get, method: "academy/\(urlStr)/\(TodayDate)/\(TodayDate)/\(userId)".trimmingCharacters(in: .whitespaces), parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)

                return

            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            let data = object as! [[String:AnyObject]]
            
            for index in data{
                
                if let titleProgram  = index["Name"] as? String{
                    if titleProgram == "" {
                    continue
                    }else{
                        self.dataArray.append(index)
                    }
                }
            }
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
}

extension GLFCalendarViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GLFCalendarTableViewCell", for: indexPath) as! GLFCalendarTableViewCell
        
        if let start = dataArray[indexPath.row]["StartTime"] as? String {
            //            localechanges
            if is12Houre == "true"{
                cell.timeLbl.text = start
            }else{
                if (start.count) > 5 {
                    cell.timeLbl.text = String(start.dropLast(3))
                }else{
                    cell.timeLbl.text = start
                }
            }
          
        }
        
        if let start = dataArray[indexPath.row]["LessonDate"] as? String {
            let dateOrignal = start.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                
                cell.dateLbl.text = DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
            }
        }
        //ProgramTypeId//AcademyProgramTypes
        if let programKey = dataArray[indexPath.row]["ProgramType"] as? String {
            if programKey == "1" {
                     cell.programTitle.text = "One-To-One Lesson"
            }else {
            
                if let title =  dataArray[indexPath.row]["Name"] as? String{
                    cell.programTitle.text = title
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension GLFCalendarViewController: FSCalendarDataSource, FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
//        let dateString = date.stringFromLocalDateFormate("yyyy-MM-dd")
//        let predicate = NSPredicate(format:"uID == \(LocationManager.sharedInstance.info.user.user_id!) AND dateString CONTAINS[cd] %@", dateString)
//        if let userActivities =  UserActivity.fetchUserActivities(predicate) {
//            return userActivities.count > 0 ? 1 : 0
//        }
        return 0
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        if (currentAcademyId == "1228") || (currentAcademyId == "1227" ){
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EventsViewControllerEMG") as! EventsViewControllerEMG
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateSelectedString = formatter.string(from: date)
            controller.selectedDate = date as NSDate
            controller.selectedDateString = dateSelectedString
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateSelectedString = formatter.string(from: date)
            controller.selectedDate = date as NSDate
            controller.selectedDateString = dateSelectedString
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
   /* func calendar(_ calendar: FSCalendar, didSelect date: Date) {
        
        if (currentAcademyId == "1228") || (currentAcademyId == "1227" ){
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EventsViewControllerEMG") as! EventsViewControllerEMG
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateSelectedString = formatter.string(from: date)
            controller.selectedDate = date as NSDate
            controller.selectedDateString = dateSelectedString
            self.navigationController?.pushViewController(controller, animated: true)

        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateSelectedString = formatter.string(from: date)
            controller.selectedDate = date as NSDate
            controller.selectedDateString = dateSelectedString
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    */
    func calendar(_ calendar: FSCalendar, hasEventFor date: Date) -> Bool {
        return true
    }
//    - (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
//    {
//    self.calendarHeightConstraint.constant = CGRectGetHeight(bounds);
//    // Do other updates here
//    [self.view layoutIfNeeded];
//    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
       // print("yes")
    }
    
}
extension GLFCalendarViewController: FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        
        return UIColor.clear//lightGray
    }
    
}

