//
//  EventDetailViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/27/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import EventKit
import CalendarLib

class EventDetailViewController: MGCDayPlannerEKViewController {

       var dataArray = [[String:AnyObject]]()
     var selectedDate = NSDate()
      var selectedDateString = ""
    var eventsForDate = [EKEvent]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
//    _ = self.dayPlannerView.calendar.date(bySettingHour: 12, minute: 60, second: 60, of: self.selectedDate as Date)
//    getLesssonsAndEvents()
  
    }
    
    func reloadingEvents()  {
        
        self.dayPlannerView.calendar = self.calendar
        let eventStore : EKEventStore = EKEventStore()
        let startDate =  selectedDate.addingTimeInterval(60*60*12) //NSDate().addingTimeInterval(-60*60*24)
        let endDate = selectedDate.addingTimeInterval(60*60*24)//NSDate().addingTimeInterval(60*60*24)//(60*60*24*3)
        let predicate2 = eventStore.predicateForEvents(withStart: startDate as Date, end: endDate as Date, calendars: nil)
        
        print("startDate:\(startDate) endDate:\(endDate)")
        eventsForDate = eventStore.events(matching: predicate2) as [EKEvent]!
        print(eventsForDate.count)
        if eventsForDate.count > 0 {
            for i in eventsForDate {
                print("Title  \(i.title)" )
                print("stareDate: \(i.startDate)" )
                print("endDate: \(i.endDate)" )
                
                if i.title == "Test Title" {
                    print("YES" )
                    // Uncomment if you want to delete
                    //eventStore.removeEvent(i, span: EKSpanThisEvent, error: nil)
                }
            }
        }
      
        
//        self.dayPlannerView.dataSource = self
//        self.dayPlannerView.delegate = self
        // Do any additional setup after loading the view.
//        self.dayPlannerView.register(EventDetailViewController.self, forEventViewWithReuseIdentifier:"EventCellReuseIdentifier")
        self.dayPlannerView.reloadAllEvents()
   
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getLesssonsAndEvents()  {
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }
      
       print(selectedDateString)
        //localechanges
        var parameters = ""
       
        parameters = "\(selectedDateString)/\(selectedDateString)/\(userId)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "academy/GetLessonsAndEventsForCalendar/\(parameters)".trimmingCharacters(in: .whitespaces), parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            self.dataArray = object as! [[String:AnyObject]]
      
            self.reloadingEvents()
//            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    //MARK:- delegate methods
    
    override func dayPlannerView(_ view: MGCDayPlannerView!, numberOfEventsOf type: MGCEventType, at date: Date!) -> Int {
        return eventsForDate.count//dataArray.count
    }
//
    override func dayPlannerView(_ view: MGCDayPlannerView!, createNewEventOf type: MGCEventType, at date: Date!) {
        print("")
    }
//
   
    override func dayPlannerView(_ view: MGCDayPlannerView!, dateRangeForEventOf type: MGCEventType, at index: UInt, date: Date!) -> MGCDateRange! {
        
        if eventsForDate.count < 1 {
            return nil
        }
        
        let index2 = Int(index)
        let ev = self.eventsForDate[index2]
        
        var end = ev.endDate
        if (type == .allDayEventType) {
        //    end = [self.calendar, mgc_nextStartOfDayForDate,:end];
           
            let comps = NSDateComponents()
            comps.day = 1
            end = self.calendar.nextDate(after: end!, matching: comps as DateComponents, matchingPolicy:.nextTime)!
       
        }
        print(ev.startDate)
        
//        var startDate =  selectedDate //NSDate().addingTimeInterval(-60*60*24)
//        var endDate = selectedDate.addingTimeInterval(60*60*6)//
        
        return MGCDateRange(start: ev.startDate as Date!, end:end )
        
//        return [MGCDateRange dateRangeWithStart:ev.startDate end:end];
        
    }
    
    
    
    override func dayPlannerView(_ view: MGCDayPlannerView!, viewForEventOf type: MGCEventType, at index: UInt, date: Date!) -> MGCEventView! {
        
        let index2 = Int(index)
        let ev = self.eventsForDate[index2]
        let customView = self.dayPlannerView.dequeueReusableView(withIdentifier:"EventCellReuseIdentifier" , forEventOf: .allDayEventType, at: index, date: ev.startDate) as! MGCStandardEventView // view.dequeueReusableView(withIdentifier: "EventCellReuseIdentifier", forEventOf: .allDayEventType, at: UInt(index2), date: ev.startDate) as! MGCStandardEventView
     
        customView.backgroundColor = UIColor.red
        customView.title = ev.title

        return customView
    }
    
    
    
    override func dayPlannerView(_ view: MGCDayPlannerView!, viewForNewEventOf type: MGCEventType, at date: Date!) -> MGCEventView! {
        
        let default1 = self.eventStore.defaultCalendarForNewEvents
        let evCell = MGCStandardEventView()
        evCell.title = "New Event"
        evCell.color = UIColor.blue
        return evCell;
        
    }

    override func dayPlannerView(_ view: MGCDayPlannerView!, shouldStartMovingEventOf type: MGCEventType, at index: UInt, date: Date!) -> Bool {
        let index2 = Int(index)
        let ev = self.eventsForDate[index2]
        return ev.calendar.allowsContentModifications
    }
   
    override func dayPlannerView(_ view: MGCDayPlannerView!, canMoveEventOf type: MGCEventType, at index: UInt, date: Date!, to targetType: MGCEventType, date targetDate: Date!) -> Bool {
        let index2 = Int(index)
        let ev = self.eventsForDate[index2]
        return ev.calendar.allowsContentModifications
    }
    
    
    override func dayPlannerView(_ view: MGCDayPlannerView!, didSelectEventOf type: MGCEventType, at index: UInt, date: Date!) {
        print("selected")
        let ev = eventsForDate[Int(index)]
    }
    
    override func dayPlannerView(_ view: MGCDayPlannerView!, willDisplay date: Date!) {
      
    }

}
