//
//  FiltersViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/24/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu

class UpdatePlayerLevelViewController: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var parentViewHeightConstraint: NSLayoutConstraint!
    var dataArray = [[String:AnyObject]]()
     var levelsDictionary = [String:AnyObject]()
    var levelsOptions = [String:AnyObject]()
    var filterCategoryTrack = [[String:AnyObject]]()
    var jsonStringForSkillsTypes = "1"
    var selectedPlayerType = ""

    var forPlayerTypeIdData  = [String:AnyObject]()
    var classPlayerData = [[String:AnyObject]]()
     var previousData = [String:AnyObject]()
    var studentTrackData = [[String:AnyObject]]()
    var optionLevelTrack = [[String:AnyObject]]()
    var selecteStduentId = "0"
    var selecteUserIdForFormSave = "0"
     var selectedLevelId = "0"
    @IBOutlet weak var playerLbl: UILabel!
    @IBOutlet weak var playerViewHieghtConstraint: NSLayoutConstraint!
  
 
    @IBOutlet weak var playerLevelBtn: UIButton!
    @IBOutlet weak var typeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let type = previousData["ProgramType"] as? String {
            
            if type == "2" {
                getClassPlayer()
            }else{
                
                playerViewHieghtConstraint.constant = 0
                if let id = previousData["StudentID"] as? Int{
                    selecteStduentId = "\(id)"
                }
                loadPlayerTypesNLEveles()
            }
        }
        
        settingRighMenuBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(UpdatePlayerLevelViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        GetPlayerTypes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func GetPlayerTypes()  {
        
       
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
            //Student/GetPlayerTypes/post/AcademyId	73
            NetworkManager.performRequest(type:.post, method: "Student/GetPlayerTypes", parameter:["AcademyId":acadmyId as AnyObject], view: self.appDelegate.window, onSuccess: { (object) in
                print(object!)
                switch object {
                case _ as NSNull:
                    
                    DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                    return
                // do one thing
                case _ as [[String:AnyObject]]: break
                    
                default:
                    break
                }
                //GetPlayerTypes
                self.dataArray = object as! [[String:AnyObject]]
                if self.dataArray.count > 0{
                 self.defaultSelectedResultsForType()
                }
                
            }) { (error) in
                print(error!)
                self.showInternetError(error: error!)
            }
        
    }

    func defaultSelectedResultsForType() {
        
        if let levelId = levelsDictionary["PlayerTypeId"] as? Int{
            
            let k = self.dataArray
            let index = k.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic["PlayerTypeId"]  as? Int, value == levelId {
                     typeBtn.setTitle(dic["PlayerTypeName"]  as? String, for: .normal)
                        selectedPlayerType = "\(levelId)"
                        GetPlayerLevels()
                        return true
                    }
                }
                
                return false
            }
        }
    }
    
    
    func GetPlayerLevels()  {
        
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
       
        //PlayerTypeId	1458
        NetworkManager.performRequest(type:.post, method: "Student/GetPlayerLevels", parameter:["AcademyId":acadmyId as AnyObject,"PlayerTypeId":selectedPlayerType as AnyObject], view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            self.levelsOptions = object as! [String:AnyObject]
           self.defaultSelectedLevel()
            
        }) { (error) in
            print(error!)
//            DataManager.sharedInstance.printAlertMessage(message:(error?.description)!, view:self)
            self.showInternetError(error: error!)
        }
        
    }
    
    func defaultSelectedLevel()  {
        
        if let levelId = levelsDictionary["PlayerTypeLevelId"] as? Int{
            
            let k = self.levelsOptions["PlayerLevels"] as! [[String:AnyObject]]
            let index = k.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic["PlayerTypeLevelId"]  as? Int, value == levelId {
                        playerLevelBtn.setTitle(dic["PlayerLevelName"]  as? String, for: .normal)
                        selectedLevelId = "\(levelId)"
                        return true
                    }
                }
                self.playerLevelBtn.setTitle("\("Please Select")", for: .normal)
                
                return false
            }
            
        }
    }
    
    func loadPlayerTypesNLEveles()  {
        

        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        ////https://app.glflocker.com/OrbisAppBeta/api/Student/LoadPlayerTypenLevels/postAcademyId	73PlayerId	4689
        
        NetworkManager.performRequest(type:.post, method: "Student/LoadPlayerTypenLevels", parameter:["AcademyId":acadmyId as AnyObject,"PlayerId":selecteStduentId as AnyObject], view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            self.levelsDictionary = object as! [String:AnyObject]
            if self.dataArray.count > 0 {
                self.defaultSelectedResultsForType()
            }
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    
    }

    
    func settingPickerView() {
     
        var dataAray = [String]()
        filterCategoryTrack = [[String:AnyObject]]()
        for dic in dataArray{
            if let val = dic["PlayerTypeName"] as? String{
                
           
                filterCategoryTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT TYPE", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
          
            self.typeBtn.setTitle("\(indexes!)", for: .normal)

            if let id = self.filterCategoryTrack[values]["PlayerTypeId"] as? Int{
                self.selectedPlayerType = "\(id)"
            }
         self.GetPlayerLevels()
            
            
            
          
        
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: typeBtn)
    
    }
 
    
    @IBAction func typBtnAction(_ sender: UIButton) {
        settingPickerView()
    }
    @IBAction func saveBtnAction(_ sender: UIButton) {
       saveForm()

    }
    
    func saveForm()  {

        var studentId = ""
         if let type = previousData["ProgramType"] as? String {
            if type == "2"  {
            studentId = selecteUserIdForFormSave
            }else{
                
        if let id = forPlayerTypeIdData["UserID"] as? Int{
            studentId = "\(id)"
        }
            }
        }
        //https://app.glflocker.com/OrbisAppBeta/api/Student/UpdatePlayerLevel
        //UserId	24504 PlayerTypeId	1459 PlayerLevelId	2553
        NetworkManager.performRequest(type:.post, method: "Student/UpdatePlayerLevel", parameter:["UserId":studentId as AnyObject,"PlayerTypeId":selectedPlayerType as AnyObject,"PlayerLevelId":selectedLevelId as AnyObject], view: self.appDelegate.window, onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
                
                
            }
            
            _=self.navigationController?.popViewController(animated: true)
             DataManager.sharedInstance.printAlertMessage(message:"Successfully Updated", view:UIApplication.getTopestViewController()!)
            //   self.settingPickerView()
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    
    func getClassPlayer()  {
        
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
                // should never happen!
            }
            
            self.classPlayerData = (object as? [[String:AnyObject]])!
            if self.classPlayerData.count > 0{
                if let typeId =   self.classPlayerData[0]["StudentID"] as? Int{
                self.selecteStduentId = "\(typeId)"
                    self.playerLbl.text = self.classPlayerData[0]["StudentName"] as? String
                    if let selectedTyp = self.classPlayerData[0]["UserID"] as? Int{
                        self.selecteUserIdForFormSave = "\(selectedTyp)"
                    }
                    self.loadPlayerTypesNLEveles()
                }
            }

     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    

    
    @IBAction func playerLevelBtnAction(_ sender: UIButton) {
        
        var dataAray = [String]()
        optionLevelTrack = [[String:AnyObject]]()
        //becauas all option not return in json
        var arrayOfLevels = [[String:AnyObject]]()
        if let aray = levelsOptions["PlayerLevels"] as? [[String:AnyObject]]{
            arrayOfLevels = aray
        }
        for dic in arrayOfLevels{
            if let val = dic["PlayerLevelName"] as? String{
                optionLevelTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT LEVEL", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
         self.playerLevelBtn.setTitle("\(indexes!)", for: .normal)
            if let selectedTyp = self.optionLevelTrack[values]["PlayerTypeLevelId"] as? Int{
                self.selectedLevelId = "\(selectedTyp)"
            }
            
//            self.loadPlayerTypesNLEveles()
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
     _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func selectPlayerBtnAction(_ sender: UIButton) {
        
        var dataAray = [String]()
        studentTrackData = [[String:AnyObject]]()
          //becauas all option not return in json
        for dic in classPlayerData{
            if let val = dic["StudentName"] as? String{
                studentTrackData.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT PLAYER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.playerLbl.text = "\(indexes!)"
            if let selectedTyp = self.studentTrackData[values]["StudentID"] as? Int{
                self.selecteStduentId = "\(selectedTyp)"
            }
            if let selectedTyp = self.studentTrackData[values]["UserID"] as? Int{
                self.selecteUserIdForFormSave = "\(selectedTyp)"
            }
     
            self.loadPlayerTypesNLEveles()
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }

}
//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetClassPlayers/8758
//https://app.glflocker.com/OrbisAppBeta/api/Student/GetPlayerTypes/post/AcademyId	73

//https://app.glflocker.com/OrbisAppBeta/api/Student/GetPlayerLevels/post/AcademyId	73PlayerTypeId	1455
//https://app.glflocker.com/OrbisAppBeta/api/Student/LoadPlayerTypenLevels/postAcademyId	73PlayerId	4689
