//
//  FiltersViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/24/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class GameScoreViewController: UIViewController {

    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!//200
    @IBOutlet weak var parentViewHeightConstraint: NSLayoutConstraint!
    var dataArray = [[String:AnyObject]]()
    var filterCategoryTrack = [[String:AnyObject]]()
    var jsonStringForSkillsTypes = "1"
    var jsonStringForCetegory = ""
    var selectedItems = [[String:AnyObject]]()
    var forPlayerTypeIdData  = [String:AnyObject]()
    var classPlayerData = [[String:AnyObject]]()
     var previousData = [String:AnyObject]()
    var studentTrackData = [[String:AnyObject]]()
    var selecteStduentId = "0"
       var selectedskilId = "0"
    @IBOutlet weak var playerLbl: UILabel!
    @IBOutlet weak var playerViewHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
      tableViewHeightConstraint.constant = 300
        
        self.filterBtn.layer.borderColor = UIColor.init(red: 184/255, green: 184/255, blue: 184/255, alpha: 1.0).cgColor
        
        if let type = previousData["ProgramType"] as? String {
            
                if type == "2" {
         
                    getClassPlayer()
                }else{
                    self.saveBtn.isHidden = true
                    self.bottomView.isHidden = true
                    playerViewHieghtConstraint.constant = 0
                GetPlayerSkillRanking(givenId: "0", studentID:"0")
            }
        }
       settingRighMenuBtn()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
   
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(GameScoreViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func GetPlayerSkillRanking(givenId:String,studentID:String)  {
        
  
        
        var playerTypId = ""
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
             playerTypId = self.selecteStduentId
            }else{
                if let id = forPlayerTypeIdData["StudentID"] as? Int{
                    playerTypId = "\(id)"
                }

            }
        }
       
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerSkillRanking/\(playerTypId)/\(givenId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
                
            default:
                
                break
                
                
            }
            //just to identify level is set for student
            if let data = object as? [[String:AnyObject]]{
                if data.count == 1 {
                    let dic = data[0]
                    if let levelCount = dic["SkillLevelCount"] as? Int{
                        if levelCount == 0 {
                            _ = self.navigationController?.popViewController(animated: true)
                            DataManager.sharedInstance.printAlertMessage(message: "Error \n Please select Player level ", view: UIApplication.getTopestViewController()!)
                            return
                        }
                    }
                }
            }
            //till here
            
    if givenId == "0"{
        self.dataArray =  object as! [[String : AnyObject]]
       // self.selecteStduentId = "0"
        let tv : GameScoreSubViewController = self.childViewControllers[0] as! GameScoreSubViewController
        tv.dataArray = self.dataArray
        tv.isForOverView = true
        tv.delegate = self
        tv.viewWillAppear(true)
        self.saveBtn.isHidden = true
          self.bottomView.isHidden = true
    }else{
        let tv : GameScoreSubViewController = self.childViewControllers[0] as! GameScoreSubViewController
        tv.dataArray = object as! [[String : AnyObject]]
        tv.isForOverView = false
        tv.delegate = self
        tv.selectedDataArray = [[String:AnyObject]]()
        tv.viewWillAppear(true)
            }
         
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

   

    
    func settingPickerView() {
     
        var dataAray = [String]()
        filterCategoryTrack = [[String:AnyObject]]()
        dataAray.append("OVERVIEW")
        let dic = ["0":"SkillId"]
        filterCategoryTrack.append(dic as [String : AnyObject])
        for dic in dataArray{
            if let val = dic["SkillName"] as? String{
                
           
                filterCategoryTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT CATEGORY", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
          
            self.filterBtn.setTitle("\(indexes!)", for: .normal)
            
            if "\(indexes!)" == "OVERVIEW" {
               // self.selecteStduentId = "0"
                let tv : GameScoreSubViewController = self.childViewControllers[0] as! GameScoreSubViewController
                tv.dataArray = self.dataArray
               tv.isForOverView = true
                tv.delegate = self
                tv.viewWillAppear(true)
                self.saveBtn.isHidden = true
                self.bottomView.isHidden = true
                return
            }

            if let skillId = self.filterCategoryTrack[values]["SkillId"] as? Int{
                self.selectedskilId = "\(skillId)"
                self.GetPlayerSkillRanking(givenId: self.selectedskilId, studentID: self.selecteStduentId)
                self.saveBtn.isHidden = false
                  self.bottomView.isHidden = false
            }
            
          
        
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: filterBtn)
    
    }
 
    
    @IBAction func filterBtnAction(_ sender: UIButton) {
        settingPickerView()
    }
    @IBAction func applyFitlerBtnAction(_ sender: UIButton) {
       saveForm()

    }
    
    func saveForm()  {
    
        var playerTypId = ""
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
                    playerTypId =  self.selecteStduentId
            }else{//
                if let id = forPlayerTypeIdData["StudentID"] as? Int{
                    playerTypId = "\(id)"
                }
                
            }
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/SavePlayerSkillRanking/\(playerTypId)/\(jsonStringForSkillsTypes)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
                _=self.navigationController?.popViewController(animated: true)
            DataManager.sharedInstance.printAlertMessage(message:"Successfully Updated", view:UIApplication.getTopestViewController()!)
            
        }) { (error) in
            print(error!)

            self.showInternetError(error: error!)
        }
        
    }

    
    func getClassPlayer()  {
        
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
     
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break
                // should never happen!
            }
            
            self.classPlayerData = (object as? [[String:AnyObject]])!
            
            if self.classPlayerData.count > 0{
                if let id = self.classPlayerData[0]["StudentID"] as? Int{
                      self.playerLbl.text = self.classPlayerData[0]["StudentName"] as? String
                    //asighning default id
                    if let selectedTyp = self.classPlayerData[0]["StudentID"] as? Int{
                        self.selecteStduentId = "\(selectedTyp)"
                    }
                    
                    self.GetPlayerSkillRanking(givenId: "0",studentID:"\(id)")
                }
            }
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    

    
    
    @IBAction func resetFilterBtnAction(_ sender: UIButton) {
      
    
    }
    @IBAction func selectPlayerBtnAction(_ sender: UIButton) {
        
        var dataAray = [String]()
        studentTrackData = [[String:AnyObject]]()
          //becauas all option not return in json
      
     
        for dic in classPlayerData{
            if let val = dic["StudentName"] as? String{
                studentTrackData.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT PLAYER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.playerLbl.text = "\(indexes!)"
            if let selectedTyp = self.studentTrackData[values]["StudentID"] as? Int{
                self.selecteStduentId = "\(selectedTyp)"
              
                self.GetPlayerSkillRanking(givenId: "0",studentID:self.selecteStduentId)
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }

}
extension GameScoreViewController : SelectedGameDelegate{
    func seletedSkillsData(selectedData: [[String:AnyObject]], selectedUserId: String) {
        print("yes")
        
        print(selectedData)
      jsonStringForSkillsTypes = ""
        
        for index in selectedData {
            
            if let value  =  index["SkillId"] as? Int {
                jsonStringForSkillsTypes = jsonStringForSkillsTypes + "\(value)" + "," + "\(index["val"]!)"   + "$"
          
            }

        }
        
        if jsonStringForSkillsTypes != "" {
            jsonStringForSkillsTypes = jsonStringForSkillsTypes.substring(to: jsonStringForSkillsTypes.index(before: jsonStringForSkillsTypes.endIndex))
        }
        print(jsonStringForSkillsTypes)

        
    }
 
}

///OrbisAppBeta/api/Academy/GetPlayerSkillRanking/23527/0
//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetPlayerSkillRanking/23527/2087

//https://app.glflocker.com/OrbisAppBeta/api/Academy/SavePlayerSkillRanking/23527/5224,3$5225,4$5226,5
//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetPlayerSkillRanking/4689/0
//https://app.glflocker.com/OrbisAppBeta/api/Academy/SavePlayerSkillRanking/4689/5262,8$5263,8$5264,9$5265,7
