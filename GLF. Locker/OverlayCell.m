//
//  OverlayCell.m
//  Orbis
//
//  Created by Nasir Mehmood on 08/01/2014.
//
//

#import "OverlayCell.h"

@implementation OverlayCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
