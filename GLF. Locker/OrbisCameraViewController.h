//
//  OrbisCameraViewController.h
//  TestingShapes
//
//  Created by Nasir Mehmood on 07/01/2014.
//  Copyright (c) 2014 Nasir Mehmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLCameraController.h"

@class OrbisCameraViewController;

// CHM Jawad
@protocol OrbisCameraViewControllerDelegate <NSObject>
@optional
- (void)orbisCameraVC:(OrbisCameraViewController *)controller didFinishRecordingVideo:(NSDictionary *)info withCallBackID:(NSString *)callBackID;
- (void)orbisCameraVC:(OrbisCameraViewController *)controller didFinishSavingVideo:(NSDictionary *)info withCallBackID:(NSString *)callBackID;
- (void)orbisCameraVC:(OrbisCameraViewController *)controller didFinishCapturingImage:(NSDictionary *)info withCallBackID:(NSString *)callBackID;
@end



@interface OrbisCameraViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, BLCameraControllerDelegate>
{
    int recordDuration;
    UIDeviceOrientation devOrientation ;
}
@property (weak, nonatomic) IBOutlet UIImageView *footerImageView;

@property (nonatomic, strong) BLCameraController *blCameraController;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIButton *changeCameraButton;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

@property (nonatomic, strong) NSTimer *updateRecordingLabelTimer;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (nonatomic, strong) IBOutlet UIView *overlayView;

@property (weak, nonatomic) id delegate;


+ (OrbisCameraViewController*) getCameraController;


- (void) captureVideoWithCallbackID:(NSString*)callbackID withOptions:(NSDictionary*)options;
- (void) stopVideoCaptureWithCallbackID:(NSString*)callbackID;
- (void) cancelVideoCaptureWithCallbackID:(NSString*)callbackID;


- (IBAction)changeCameraButtonClicked:(id)sender;
- (IBAction)falshButtonClicked:(id)sender;
- (IBAction)recordButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;



@end
