//
//  PastViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/9/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class PastViewController: UIViewController {
    
    @IBOutlet weak var studentLbl: UILabel!
    @IBOutlet weak var lessonDateLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    var durationId = ""
    var durationsArray:[[String:AnyObject]] = [[String:AnyObject]]()
    var selectedData = [String:AnyObject]()
    var selectedDataClone = [String:AnyObject]()
    var usersDataArray = [[String:AnyObject]]()
    var secondLastControllerData = [String:AnyObject]()
    var fromEventController = false
    var delegatedDictionary = [String:AnyObject]()
    var previousData = [String:AnyObject]()
    var currentPlayerSelectedId = ""
    var durationTrackArray = [[String:AnyObject]]()
    var selectedDuration = 0
    @IBOutlet weak var selectStudentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var programViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var programLbl: UILabel!
    var selectedDate = NSDate()
    var selectedTime = ""
    var selectedDateString = ""
    var programsArray = [[String:AnyObject]]()
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    var selectedProgramType = 0
    var academyScheduleData = [String:AnyObject]()

    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        print(previousData)
        webConfigfilteration()
        settingRighMenuBtn()
    }
    
    func webConfigfilteration()  {
        
        if fromEventController{
            self.navigationItem.title = "LESSON BOOKING"
            //            submitBtn.isHidden = true
            cancelBtn.setTitle("Set Leave", for: .normal)
            submitBtn.setTitle("Continue", for: .normal)
            
            selectStudentViewHeightConstraint.constant = 0
           
            //            localechanges
            if is12Houre == "true"{
                
                self.startTimeLbl.text = Utility.convertTimeInto12Formate(timeStr: selectedTime)
            }else{
                if (selectedTime.count) > 5 {
                    self.startTimeLbl.text = String(selectedTime.dropLast(3))
                }else{
                    self.startTimeLbl.text = selectedTime
                }
                
            }

            lessonDateLbl.text = DataManager.sharedInstance.getFormatedDate(date:selectedDateString, formate: "yyyy-MM-dd" )
            print(lessonDateLbl.text!)
            //dynamicChange
            if isNewUrl{
                Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(selectedDateString,dateFormater:"yyyy-MM-dd"))", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
            }, onFailure: {(error) in

            })
                
            }

            if let programs = DataManager.sharedInstance.currentAcademy()!["AcademyProgramTypes"] as? [[String:AnyObject]]{
                print(programs)
                for index in programs {
                    if let oneToOne = index["IsOneToOneLesson"] as? Bool{
                        if let putting = index["IsPuttingLab"] as? Bool{
                            if oneToOne {
                                programsArray.append(index)
                            }else if putting{
                                programsArray.append(index)
                            }
                        }
                    }
                }
            }
        }else{
            programViewHeightConstraint.constant = 0
            getStudents()
            getDurations(program: "")
        }
    }
    
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PastViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- buttons actions
    @IBAction func selectStudentBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectStudentViewController") as! SelectStudentViewController
        controller.originaldata = usersDataArray
        controller.delegate = self
        controller.previousData = previousData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func lessonDateBtnAction(_ sender: UIButton) {

        if fromEventController{
            return
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        let formator = DateFormatter()
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
        
             self.lessonDateLbl.text = title
            
            //dynamicChange
            if isNewUrl{
                Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(title,dateFormater:"dd MMM, yyyy"))", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
            }, onFailure: {(error) in
                
            })
                
            }
        
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        picker?.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        picker?.show()
    }
    
    @IBAction func durationBtnAction(_ sender: UIButton) {
        var dataAray = [String]()
        if durationsArray.count < 1 {
            return
        }
        durationTrackArray = [[String:AnyObject]]()
        for dic in durationsArray{
            if let val = dic["Name"] as? String{
                durationTrackArray.append(dic)
                dataAray.append(val)
            }
        }
        ActionSheetStringPicker.show(withTitle: "SELECT DURATION", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.durationLbl.text = "\(indexes!)"
            print(values,indexes!)
            self.selectedData = self.durationTrackArray[values]
            if let selectedTyp = self.durationTrackArray[values]["DurationId"] as? Int{
                self.durationId = "\(selectedTyp)"
            }
            if let duration = self.durationTrackArray[values]["Id"] as? Int{
                self.selectedDuration = duration
            }

            if let durationId = self.durationTrackArray[values]["DurationId"] as? Int{
                self.selectedDataClone["DurationId"] = durationId as AnyObject?
            }
            
            if self.fromEventController{
                self.decidingEndTime()
                self.getLessonPrice()
            }

            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }

    func decidingEndTime()  {
        //for end time   2017-03-02 16:00:00 +0000
        let formator = DateFormatter()
        print(selectedTime)
        if "\(selectedTime)".count > 5{
            formator.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        }else{
            formator.dateFormat = "yyyy-MM-dd' 'HH:mm"
        }
        
        if globalDateFormate == "dd/MM/yyyy" {
            formator.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            formator.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }
        let date = "\(self.selectedDate)"
        let dateOrignal = date.components(separatedBy: " ")
        let dateAndTime = dateOrignal[0] + " " + self.selectedTime
        let selectedTimeCurrent = formator.date(from:dateAndTime)
        let endDate = selectedTimeCurrent?.addingTimeInterval(Double(self.selectedDuration) * 60.0)

        if is12Houre == "true"{
            formator.dateFormat = "h:mm a"
        }else{
            formator.dateFormat = "HH:mm"
        }
        
        //refixit done after sana reported
        let hours = NSCalendar.current.component(.hour, from: endDate as! Date)
        print(hours)
        if hours > 22 || hours < 7 {
            DataManager.sharedInstance.printAlertMessage(message: "Selected Time Out Of Available Time", view: self)
            return
        }
        
        let endTitle = formator.string(from: endDate! as Date)
        self.endTimeLbl.text = endTitle

    }

    @IBAction func startTimeBtnAction(_ sender: UIButton) {
     
        
        if durationLbl.text! == "SELECT DURATION" {
            DataManager.sharedInstance.printAlertMessage(message: "Select Duration from above First!", view: self)
            return
        }else{
        }
      let formator = DateFormatter()
      var picker =   ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in
        
        if is12Houre == "true"{
            formator.dateFormat = "hh:mm a"
        }else{
            formator.dateFormat = "HH:mm"
            
        }
            //for start time
//            let selectedTimeCurrent = selectedTime as! NSDate
//            let startTitle = formator.string(from: selectedTimeCurrent as Date)
        
        let selectedTimeCurrent = selectedTime as! NSDate
        let startTitle = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))

        let selectedTimeCurrent2 = Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15)
        

        let endDate = selectedTimeCurrent2.addingTimeInterval(Double(self.selectedDuration) * 60.0)
        
        //refixit done after sana reported
        let hours = NSCalendar.current.component(.hour, from: endDate as Date)
        print(hours)
        if hours > 22 || hours < 7 {
            DataManager.sharedInstance.printAlertMessage(message: "Selected Time Out Of Available Time", view: self)
            return
        }
            self.startTimeLbl.text = startTitle
            self.selectedTime = startTitle
            let endTitle = formator.string(from: endDate as Date)
            self.endTimeLbl.text = endTitle
        }, cancel: { (picker) -> Void in
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
            
        }
        picker?.minuteInterval = 15
        
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
        picker?.show()
    }
    
    @IBAction func endTimeBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func programBtnAction(_ sender: UIButton) {
        
        var dataAray = ["Select Program Type"]
        for index in programsArray{
                if let keyVal = index["ProgramTypeName"] as? String{
                     dataAray.append(keyVal)
                }
        }
        ActionSheetStringPicker.show(withTitle: "PROGRAMS", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.programLbl.text = "\(indexes!)"
            
            if values > 0{
                self.previousData = self.programsArray[values - 1]
            if let programId = self.programsArray[values-1]["ProgramTypeId"] as? Int {
                self.selectedProgramType = programId
                self.getDurations(program: "\(programId)")
                }
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }
    
    //MARK:- fetching server json
    func getDurations(program:String)  {
        
        var programType = ""
        if fromEventController{
            programType = program
        }else{
        
        if let id = previousData["ProgramTypeId"] as? Int{
            programType = "\(id)"
            }
        }
        //
        NetworkManager.performRequest(type:.get, method: "Academy/GetProgramTypeFilters/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(programType)/\(currentUserLogin!)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
     
                break
            }
            self.durationsArray  = self.convertToArray(text: object as! String)!
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }


    func getLessonPrice()  {
        
        var DurationId = ""
   
        if let id = selectedDataClone["DurationId"] as? AnyObject{
                DurationId = "\(id)"
        }
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            userId = "\(id)"
        }
        
        var membershipID = ""
        if let userId = DataManager.sharedInstance.currentUser()?["MembershipId"] as? Int{
            membershipID = "\(userId)"
        }
        
        var urlStr = ""
        if isNewUrl{
            urlStr = "Academy/GetCoachLessonPrice/\(userId)/\(DurationId)/\(membershipID)"
        }else{
            urlStr  = "Academy/GetCoachLessonPrice/\(userId)/\(DurationId)/"
        }
        NetworkManager.performRequest(type:.get, method: "\(urlStr)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break
            }

            //mergingcode isnewurl
            
              if isNewUrl{
            
            let data = object as? [String:AnyObject]
            if let priceReturn = data!["LessonPrice"] as? Double{
                self.selectedDataClone["Price"] = "\(priceReturn)" as AnyObject?
            }
            
            if let priceReturn = data!["DiscLessonPrice"] as? Double{
                self.selectedDataClone["DiscPrice"] = "\(priceReturn)" as AnyObject?
                }
                
              }else{
                
                let data = object as? String
                self.selectedDataClone["Price"] = data as AnyObject?

            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    func convertToArray(text: String) -> [[String: AnyObject]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func getStudents()  {
        
        let classID = "0"
        NetworkManager.performRequest(type:.post, method: "academy/GetAcademyPlayerList/", parameter:["AcademyId":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"UserId":DataManager.sharedInstance.currentUser()?["Userid"] as AnyObject!,"ClassId":classID as AnyObject!], view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.usersDataArray = object as! [[String : AnyObject]]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    func checkLessonAvailability()  {
        
        var coachIdis = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachIdis = "\(id)"
        }

        let parameters = ["StartTime":startTimeLbl.text! as AnyObject,"EndTime":endTimeLbl.text! as AnyObject,"LessonId":durationId as AnyObject,"LessonDate":self.lessonDateLbl.text! as AnyObject,"CoachId":coachIdis] as [String : Any]
        
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "Booking/IsLessonTimeAvailable", parameter: parameters as [String : AnyObject], view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            let responce = object as! String
            
            if responce == "2"{
                self.logicToSwitchBookingScreen()

            }
            else if responce == "4"{
                
                DataManager.sharedInstance.printAlertMessage(message: "You have Group Lesson(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)

            }else if responce == "7"{
                DataManager.sharedInstance.printAlertMessage(message: "You have Conflicts in schedule in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)

            }else if responce == "5"{
                
                DataManager.sharedInstance.printAlertMessage(message: "You have Leave(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }else if responce == "3"{
                DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) and Group Lesson(s)  Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)

            }else if responce == "6"{
                //for leave
                DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) Or Group Lesson(s) Or Leave(s)  Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }
            else if responce == "1"{
                self.logicToSwitchBookingScreen()
            }
           else{
                DataManager.sharedInstance.printAlertMessage(message: "You have Conflicts in schedule in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

    }
    
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        
        if fromEventController{
            //changes logic for adding leave option
            self.checkLessonAvailability()
            return
        }else{
            
        }
        
        messageStr = ""
        validateData()
        
        if messageStr.count > 0{
            let alertController = UIAlertController(title: "Error", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        
        NetworkManager.performRequest(type:.post, method: "Academy/LessonBookingPast", parameter: ["PlayerId":currentPlayerSelectedId as AnyObject,"StartTime":startTimeLbl.text! as AnyObject,"EndTime":endTimeLbl.text! as AnyObject,"LessonDurationId":durationId as AnyObject,"CoachId":DataManager.sharedInstance.currentUser()?["coachId"] as AnyObject!,"Date": self.lessonDateLbl.text! as AnyObject,"AcademyID":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!], view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
          let responce = object as! Int
            
            if responce == 4{
                DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }else if responce == 7{
                DataManager.sharedInstance.printAlertMessage(message: "You have Group Lesson(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }else if responce == 6{
                DataManager.sharedInstance.printAlertMessage(message: "You have Leave(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }else if responce == 3{
              DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) and Group Lesson(s)  Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }else if responce == 5{
                DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) , Group Lesson(s) and Leave(s)  Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }
            else if responce > 10{
             
               _ = self.navigationController?.popViewController(animated: true)
                   DataManager.sharedInstance.printAlertMessage(message: "Successfully Booked lesson!", view:UIApplication.getTopestViewController()!)
            }else{
                DataManager.sharedInstance.printAlertMessage(message: "You have Conflicts in schedule in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

        
    }
    
    func logicToSwitchBookingScreen()  {
        
        messageStr = ""
        validateDataForEvents()
        if messageStr.characters.count > 0{
            let alertController = UIAlertController(title: "Error", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        
        if let id =   DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            //1119 for FDGS
            //uncomment replace 1119 for acadmy id
           
//            if id == 1119 || id == 1232 || id == 1234 || id == 1228 || id == 1227 {
                
                print(previousData)
                if let oneToOne = previousData["IsOneToOneLesson"] as? Bool{
                    
                    if let putingLab = previousData["IsPuttingLab"] as? Bool{
                        
                        //popUpfunctionality

                        var showPopUp = false

                        if oneToOne && putingLab{
                            self.showAlertViewForPuttingLab()
                            
                        }else if oneToOne{
                            
                            var dataToPick = [String:AnyObject]()
                            if (currentAcademyId == "1228") || (currentAcademyId == "1227" ){
                                dataToPick =  previousData
                                
                            }else{
                                //popUpfunctionality
                                if isNewUrl{
                                    dataToPick =  previousData

                                }else{
                                    dataToPick =  DataManager.sharedInstance.currentAcademy() as! [String : AnyObject]
                                    showPopUp = true

                                }
                            }
                            
                            if let isStudioEnable =  dataToPick["IsStudioEnable"] as? Bool,isStudioEnable == true{
                                
                                //popUpfunctionality
                                if let popUp = dataToPick["IsStudioPopupEnable"] as? Bool{
                                    showPopUp = popUp
                                }
//
//                                && showPopUp
//                                print(showPopUp,isStudioEnable)
                                if isStudioEnable && showPopUp {
                                    showAlertViewForStudioLesson()
                                }else{
                                    
                                    let defaults = UserDefaults.standard
                                        NSLog("OK Pressed")
                                        defaults.set("NO", forKey: "isPuttingLab")
                                        defaults.set("YES", forKey: "isStudio")
                                      self.moveToNextConroller()
                                }
                                
                            }else{
                                self.moveToNextConroller()

                            }
                            
                        }else{
                            self.moveToNextConroller()
                        }
                        
                    }
                    
//                }
                
            }else{
                let defaults = UserDefaults.standard
                defaults.set("NO", forKey: "isPuttingLab")
                self.moveToNextConroller()
            }
            
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
      /*  if fromEventController{
             messageStr = ""
            validateDataForEvents()
            if messageStr.count > 0{
                let alertController = UIAlertController(title: "Error", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }else {
            }
        
            self.selectedDataClone["BookingStartTime"] = startTimeLbl.text! as AnyObject?
            self.selectedDataClone["BookingEndTime"] = endTimeLbl.text! as AnyObject?
            self.selectedDataClone["RBookingDate"] = selectedDateString  as AnyObject
//            getLessonPrice()
                //first checking Studio availablity
                let defaults = UserDefaults.standard
                if let isStudio2 = defaults.object(forKey: "isStudio") as? String{
                    if isStudio2 == "YES"{
                        checkStudeoAvailablity()
                    }else{
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                        controller.previousData = self.previousData
                        controller.fromEventController = true
                        controller.secondLastControllerData = self.selectedData
                        controller.selectedData = self.selectedDataClone
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }*/
        
        if fromEventController{
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddLeaveViewController") as! AddLeaveViewController
            self.navigationController?.pushViewController(controller, animated: true)

            
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }

    }
    
    func checkStudeoAvailablity()  {
        let startTime = startTimeLbl.text!
        let endTime = endTimeLbl.text!
        let date1 = selectedDateString
        var academyID = ""
        if let acemyId = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(acemyId)"
        }
        print(startTime,endTime,date1,academyID)
        var roldId = "-1"
        if currentUserLogin == 4 {
            roldId = "4"
        }
        var methodToHit = "CheckStudioAvailability"
        var parameters = [String:AnyObject]()
        let keyTo = DataManager.sharedInstance.detectIsPuttingLab(key: "isPuttingLab")
        if keyTo == "true" {
            methodToHit = "CheckPuttingLabAvailability"
            parameters = ["LessonDate":date1 as AnyObject,"StartTime":startTime as AnyObject,"AcademyId":"\(academyID)" as AnyObject,"EndTime":endTime as AnyObject] as [String : Any] as [String : AnyObject]
            
        }else{
            parameters = ["LessonDate":date1 as AnyObject,"StartTime":startTime as AnyObject,"AcademyId":"\(academyID)" as AnyObject,"EndTime":endTime as AnyObject,"RoleId":roldId,"ProgramTypeId":self.selectedProgramType] as [String : Any] as [String : AnyObject]
            
        }
        print(parameters)
        
        NetworkManager.performRequest(type:.post, method:"Booking/\(methodToHit)",parameter:parameters as [String : AnyObject] as [String : AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
            
            print(object!)
            let object2 = object as! Bool
            if object2 {
              
                
                if isNewUrl{
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                    controller.previousData = self.previousData
                    controller.fromEventController = true
                    controller.secondLastControllerData = self.selectedData
                    controller.selectedData = self.selectedDataClone
                    self.navigationController?.pushViewController(controller, animated: true)
                }else{
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
                    controller.previousData = self.previousData
                    controller.fromEventController = true
                    controller.secondLastControllerData = self.selectedData
                    controller.selectedData = self.selectedDataClone
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
                
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"Selected Lesson Not Available!", view:self)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func fillUserInfo()  {
        var fullName = ""
        if let fname = delegatedDictionary["FirstName"] as? String{
           
            fullName = fname
        }
        if let lname = delegatedDictionary["LastName"] as? String{
            
            fullName = fullName + " " + lname
            studentLbl.text = fullName
        }
    }
    
    var messageStr = ""
    func validateData()  {
        
        if studentLbl.text == "STUDENT NAME" {
            messageStr = messageStr + "Select Student \n"
        }
        
        if self.lessonDateLbl.text == "SELECT DATE" {
            messageStr = messageStr + "Select Date \n"
        }
        
        if self.durationLbl.text == "SELECT DURATION" {
                 messageStr = messageStr + "Select Duration \n"
        }
        
        if self.startTimeLbl.text == "START TIME" {
            messageStr = messageStr + "Enter Time \n"
        }
    }
    
    func validateDataForEvents()  {
        
        if programLbl.text == "SELECT PROGRAM TYPE" {
            messageStr = messageStr + "Select Program Type \n"
        }
        
        if self.durationLbl.text == "SELECT DURATION" {
            messageStr = messageStr + "Select Duration \n"
        }
    }
    
    func showAlertViewForPuttingLab()  {
        
        var alertMessage = ""
        var alertTitle = ""
        
        for index in programsArray{
            if let keyVal = index["ProgramTypeId"] as? Int{
                
                if keyVal == selectedProgramType{
                    alertMessage = index["Description"] as! String
                    alertTitle = index["ProgramTypeName"] as! String
                    break
                }
            }
        }
        
        let defaults = UserDefaults.standard

        defaults.set("YES", forKey: "isPuttingLab")
        defaults.set("NO", forKey: "isStudio")
        self.moveToNextConroller()
       return
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage , preferredStyle: .alert)
        // Create the actions
//        let defaults = UserDefaults.standard
        let okAction = UIAlertAction(title: "CONTINUE", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            defaults.set("YES", forKey: "isPuttingLab")
            defaults.set("NO", forKey: "isStudio")
            
            self.moveToNextConroller()
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            defaults.set("NO", forKey: "isPuttingLab")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showAlertViewForStudioLesson()  {
        
        var alertMessage = ""
        var alertTitle = ""
        
        let alertController = UIAlertController(title: "STUDIO CONFIRMATION", message: "Is this Studio Lesson?" , preferredStyle: .alert)
        // Create the actions
        let defaults = UserDefaults.standard
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            defaults.set("NO", forKey: "isPuttingLab")
            defaults.set("YES", forKey: "isStudio")
            self.moveToNextConroller()
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            defaults.set("NO", forKey: "isStudio")
            defaults.set("NO", forKey: "isPuttingLab")
            self.moveToNextConroller()
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func moveToNextConroller()  {
        
        self.selectedDataClone["BookingStartTime"] = startTimeLbl.text! as AnyObject?
        self.selectedDataClone["BookingEndTime"] = endTimeLbl.text! as AnyObject?
        self.selectedDataClone["RBookingDate"] = selectedDateString  as AnyObject
        //            getLessonPrice()
        //first checking Studio availablity
        let defaults = UserDefaults.standard        
        var keyToCheckDecide = ""
        let keyTo = DataManager.sharedInstance.detectIsPuttingLab(key: "isPuttingLab")
        if keyTo == "true" {
            keyToCheckDecide = "isPuttingLab"
        }else{
            keyToCheckDecide = "isStudio"
        }
        if let isStudio2 = defaults.object(forKey:keyToCheckDecide) as? String{
            if isStudio2 == "YES"{
                checkStudeoAvailablity()
            }else{
                
                
                if isNewUrl{
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                    controller.previousData = self.previousData
                    controller.fromEventController = true
                    controller.secondLastControllerData = self.selectedData
                    controller.selectedData = self.selectedDataClone
                    self.navigationController?.pushViewController(controller, animated: true)
                }else{
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
                    controller.previousData = self.previousData
                    controller.fromEventController = true
                    controller.secondLastControllerData = self.selectedData
                    controller.selectedData = self.selectedDataClone
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
            }
        }
        
    }
    //
    func filteringOneToOne()  {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
        controller.previousData = self.previousData
        controller.fromEventController = true
        controller.secondLastControllerData = self.selectedData
        controller.selectedData = self.selectedDataClone
        self.navigationController?.pushViewController(controller, animated: true)
        
    }

}
//MARK:- selected student delegate method
//called when user selected
extension PastViewController : SelectedStudentDelegate{
    
    func seletedUserData(dic: NSDictionary,selectedUserId:String) {
        print(dic)
        currentPlayerSelectedId = selectedUserId
        delegatedDictionary = dic as! [String : AnyObject]
        fillUserInfo()
    }
}

