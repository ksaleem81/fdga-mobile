//
//  PastViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/9/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu

class AddLeaveViewController: UIViewController {

  
    @IBOutlet weak var leaveTypLbl: UILabel!

    @IBOutlet weak var returnDayLbl: UILabel!
    @IBOutlet weak var leaveDayLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    var durationId = ""
    var leaveReturnTime = Date()
    var leaveStartTime = Date()
    var selectedLeaveType = 0
    @IBOutlet weak var othersViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var leaveNotesField: UITextField!
    @IBOutlet weak var otherField: UITextField!
    @IBOutlet weak var returnDayTimeBtn: UIButton!
    @IBOutlet weak var leaveDayTimeBtn: UIButton!

    private var leaveTypeArray: [String] = ["Full Day Leave","Half Day Leave","Short Leave","Management Meeting","Others"]
    
    var shouldEditLeave = false
    var leaveData: [String: AnyObject]?
    var selectedDate = NSDate()
    
    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //getStudents()
        print(leaveData)
        print(selectedDate)
        othersViewHeightConstraint.constant = 0
        leaveDayTimeBtn.isHidden = true
        returnDayTimeBtn.isHidden = true
        settingRighMenuBtn()
        
        if shouldEditLeave {
            title = "EDIT LEAVE"
            cancelBtn.isHidden = false
            self.cancelBtn.setTitle("Delete Leave", for: .normal)
            fillUserInfo()
        }else{
            settingDefaultView()
        }
    }
    
    var academyScheduleData = [String : AnyObject]()
    func settingDefaultView()  {
        
        let date = DataManager.sharedInstance.getTodayDate()
        //dynamicChange
        if isNewUrl{
            Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(date,dateFormater:"yyyy-MM-dd"))", controller: self, Success: { data  in
            self.academyScheduleData = data as! [String : AnyObject]
        }, onFailure: {(error) in
            
        })
            
        }
        
        
        self.leaveDayLbl.text  = DataManager.sharedInstance.getFormatedDateGivenPAttern(date: date, formate: "yyyy-MM-dd", formateReturn: "dd MMM, yyyy")
        self.returnDayLbl.text  = DataManager.sharedInstance.getFormatedDateGivenPAttern(date: date, formate: "yyyy-MM-dd", formateReturn: "dd MMM, yyyy")
        
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AddLeaveViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leaveTypeBtnAction(_ sender: UIButton) {
        
                ActionSheetStringPicker.show(withTitle: "SELECT LEAVE", rows:leaveTypeArray, initialSelection: 0, doneBlock: {
                    picker, values, indexes in
                    self.leaveTypLbl.text = "\(indexes!)"
                    self.selectedLeaveType = values + 1
                    

                    self.updateTimeButtons()
                    
                    print(self.selectedLeaveType)
                    return
                }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func leaveDayBtnAction(_ sender: UIButton) {
    
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        
        
        let formator = DateFormatter()
        
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
            //dynamicChange
            if isNewUrl{
                Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(title,dateFormater:"dd MMM, yyyy"))", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
            }, onFailure: {(error) in
                
            })
                
            }
            
            
            self.leaveDayLbl.text = title
            self.returnDayLbl.text = title
            
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        

        
        picker?.show()

    }
    
    @IBAction func returnDayBtnAction(_ sender: UIButton) {

//        return
        let formator = DateFormatter()
        
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
            //dynamicChange
            if isNewUrl{
                Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(title,dateFormater:"dd MMM, yyyy"))", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
            }, onFailure: {(error) in
                
            })
                
            }
            
            
            self.returnDayLbl.text = title
            
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        
        picker?.show()
    }
    
    @IBAction func leaveDayTimeBtnAction(_ sender: UIButton) {
      
        let formator = DateFormatter()
        var picker =  ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in
            
            if is12Houre == "true"{
                formator.dateFormat = "h:mm a"
                
            }else{
                formator.dateFormat = "HH:mm"
                
            }
                    //for start time
                    let selectedTimeCurrent = selectedTime as! NSDate
                    let endTitle = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            
                    self.leaveStartTime = selectedTimeCurrent as Date
             
                    self.leaveDayTimeBtn.setTitle(endTitle, for: .normal)
        
                }, cancel: { (picker) -> Void in
                    
                }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
            
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
            
        }

        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
        picker?.show()
    }
    @IBAction func returnDayTimeBtnAction(_ sender: UIButton) {
        let formator = DateFormatter()
       var picker = ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in

        if is12Houre == "true"{
            formator.dateFormat = "h:mm a"
            
        }else{
            formator.dateFormat = "HH:mm"
            
        }
        //for start time
            let selectedTimeCurrent = selectedTime as! NSDate
            let endTitle = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            self.leaveReturnTime = selectedTimeCurrent as Date
            //for end time
            
           self.returnDayTimeBtn.setTitle(endTitle, for: .normal)
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
            
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
            
        }
        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
        picker?.show()

    }
  
    @IBAction func saveBtnAction(_ sender: UIButton) {
     
        //crashiphon6SPlus
        

        if Utility.isDevice12Hour(){

        }else{
        if !leaveDayTimeBtn.isHidden{
            if (returnDayTimeBtn.currentTitle! == leaveDayTimeBtn.currentTitle!) || self.leaveReturnTime < self.leaveStartTime {
                let alertController = UIAlertController(title: "Error", message: "Select valid start and end time", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
        }
            
        }
        messageStr = ""
        validateData()
        
        if messageStr.count > 0{
            let alertController = UIAlertController(title: "Error", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        
        var  returnDay = ""
        
        if (returnDayLbl.text?.count)! > 0 {
            returnDay = DataManager.sharedInstance.getFormatedDateForJsonMonthFirst(date: returnDayLbl.text!,formate: "dd MMM, yyyy")
        }
        var  leaveDay = ""
        if (leaveDayLbl.text?.count)! > 0 {
            leaveDay = DataManager.sharedInstance.getFormatedDateForJsonMonthFirst(date: leaveDayLbl.text!,formate: "dd MMM, yyyy")
        }
        
        let coachLeaveID = (shouldEditLeave == false) ? 0 as AnyObject : leaveData!["EvntId"]

        if "\(selectedLeaveType)" == "1" {
          
            var startTimeAc = "07:00:00"
            var endTimeAc = "22:00"
            //dynamicChange
            if isNewUrl{
            if let startTime =  self.academyScheduleData["AcademyStartTime"] as? String{
                startTimeAc = startTime
            }
            
            if let endTime =  self.academyScheduleData["AcademyEndTime"] as? String  {
                endTimeAc = endTime
                }
            }
            
            leaveDayTimeBtn.setTitle(startTimeAc, for: .normal)
            returnDayTimeBtn.setTitle(endTimeAc, for: .normal)
            
        }
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        
        NetworkManager.performRequest(type:.post, method: "Coach/AddCoachLeaves", parameter:["UserID":userId as AnyObject,"CoachId":coachId as AnyObject!,"CoachLeaveId":coachLeaveID as AnyObject!,"LeaveStartDate": leaveDay as AnyObject!,"LeaveEndDate": returnDay as AnyObject!,"LeaveTypeId":"\(selectedLeaveType)" as AnyObject!,"LeaveStartTime":leaveDayTimeBtn.currentTitle! as AnyObject!,"LeaveEndTime":returnDayTimeBtn.currentTitle! as AnyObject!,"LeaveDesc":"\(otherField.text!)" as AnyObject!,"LeaveNotes":"\(leaveNotesField.text!)" as AnyObject!,"AcademyId":academyID as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)",view:self)
                return
                
            default:
            break
            }
            
            if let responce = object as? String{
                
                if responce == "4"{
                    DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) Scheduled in this time.", view:UIApplication.getTopestViewController()!)
                }else if responce == "7"{
                    DataManager.sharedInstance.printAlertMessage(message: "You have Group Lesson(s) Scheduled in this time.", view:UIApplication.getTopestViewController()!)
                }else if responce == "6"{
                    DataManager.sharedInstance.printAlertMessage(message: "You have Leave(s) Scheduled in this time.", view:UIApplication.getTopestViewController()!)
                }else if responce == "3"{
                    DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) and Group Lesson(s)  Scheduled in this time.", view:UIApplication.getTopestViewController()!)
                }else if responce == "5"{
                    DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) , Group Lesson(s) and Leave(s)  Scheduled in this time. ", view:UIApplication.getTopestViewController()!)
                }
                else if responce == "0"{
                    
                    _ = self.navigationController?.popViewController(animated: true)
                    DataManager.sharedInstance.printAlertMessage(message: "Leave added successfully", view:UIApplication.getTopestViewController()!)
                }else if Int(responce)! > 10 {
                    _ = self.navigationController?.popViewController(animated: true)
                    DataManager.sharedInstance.printAlertMessage(message: "Successfully set leave.", view:UIApplication.getTopestViewController()!)
                }else{
                    DataManager.sharedInstance.printAlertMessage(message: "You have Conflicts in schedule in this time.", view:UIApplication.getTopestViewController()!)
                }
                
            }
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

        
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
        if shouldEditLeave {
            
            let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.deleteCoachLeave()
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)

        }else{
            _ = self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    func deleteCoachLeave()  {
        
        var leaveID = ""
        if let id = leaveData?["EvntId"] as? Int{
            leaveID = "\(id)"
        }
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }

        let urlStr = "DeleteAleave"
        NetworkManager.performRequest(type:.get, method: "Academy/\(urlStr)/\(leaveID)/\(coachId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                break
                
            }
            
            if object as! String == "success"{
                _ = self.navigationController?.popViewController(animated: true)

            }else{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    
    
    var messageStr = ""
    func validateData()  {
        
        if leaveTypLbl.text == "SELECT LEAVE TYPE" {
            messageStr = messageStr + "Select Leave Type \n"
        }
        
        if self.leaveDayLbl.text == "LEAVE DAY" {
            messageStr = messageStr + "Select Leave Day \n"
        }
        
        if self.returnDayLbl.text == "RETURN DAY" {
                 messageStr = messageStr + "Select Return Day \n"
        }
    }
    
    
    func fillUserInfo()  {
        leaveTypLbl.text = leaveData?["Notes"] as? String
        var leaveTypeToFind = ""
        if leaveTypeArray.contains(leaveTypLbl.text!) {
            leaveTypeToFind = leaveTypLbl.text!
        }else{
            leaveTypeToFind = "Others"
        }

        selectedLeaveType = leaveTypeArray.index(of: leaveTypeToFind)! + 1
        updateTimeButtons()
        let formatter = DateFormatter.init()
        formatter.dateFormat = "dd MMM, yyyy"
        leaveDayLbl.text = formatter.string(from: selectedDate as Date)
        
        var formaterequired = ""
        if is12Houre == "true"{
            formaterequired = "h:mm a"
        }else{
            formaterequired = "HH:mm"
        }

        
        let endDateArray = (leaveData?["EndDate"] as! String).components(separatedBy: "T")
        if endDateArray.count < 1{
            return
        }
        let endDateString = endDateArray[0]
        returnDayLbl.text = getFormattedDateString(from: endDateString, initialFormat: "yyyy-MM-dd", requiredFormat: "dd MMM, yyyy")
        let startTimeString = getFormattedDateString(from: leaveData?["StartTime"] as! String, initialFormat: "HH:mm:ss", requiredFormat: formaterequired)
        leaveDayTimeBtn.setTitle(startTimeString, for: .normal)
        let endTimeString = getFormattedDateString(from: leaveData?["EndTime"] as! String, initialFormat: "HH:mm:ss", requiredFormat: formaterequired)
        returnDayTimeBtn.setTitle(endTimeString, for: .normal)
        leaveNotesField.text = leaveData?["LeaveNotes"] as? String
        
        //dynamicChange
        if isNewUrl{
            Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(returnDayLbl.text!,dateFormater:"dd MMM, yyyy"))", controller: self, Success: { data  in
            self.academyScheduleData = data as! [String : AnyObject]
        }, onFailure: {(error) in
            
        })
        }
        
        //this is done because of required formate
        if let startTime =  leaveData?["StartTime"] as? String{
            //crashiphon6SPlus

            if Utility.isDevice12Hour(){
            }else{
            leaveStartTime = convertStrinToDate(time: startTimeString)
                
            }
        }

        if let endTime =  leaveData?["EndTime"] as? String{
            //crashiphon6SPlus


            if Utility.isDevice12Hour(){

            }else{
                            leaveReturnTime = convertStrinToDate(time: endTimeString)

            }
        }
        //till here
        
    }
    
     func convertStrinToDate(time:String)->Date{
        
        let dateFormatter = DateFormatter()
        
        // localechanges
        if is12Houre == "true"{
            dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
            
        }else{
            
            if time.count < 17{
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                
            }else{
                
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
            }

//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        }
        //        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        
        let endDateArray = (leaveData?["EndDate"] as! String).components(separatedBy: "T")
        var dateSelected = ""
        if endDateArray.count > 0 {
            dateSelected = endDateArray[0]
        }
        let item = dateSelected +  " " +  time
        let date = dateFormatter.date(from: item)
        return date!// Start: Optional(2000-01-01 19:00:00 +0000)
        
    }

    
    func updateTimeButtons() {
        
        var lesonType = "",discriptionText = ""
        
        if let typ = leaveData?["LessonType"] as? Int{
            lesonType = "\(typ)"
        }
        
        if let dis = leaveData?["Notes"] as? String{
            let disOrig = dis.components(separatedBy: "-")
            if disOrig.count > 1{
                discriptionText = "\(disOrig[1])"
            }

        }

        
        if "Others" == leaveTypLbl.text || lesonType == "5" {
            self.othersViewHeightConstraint.constant = 40
            otherField.text = discriptionText

        }else{
            self.othersViewHeightConstraint.constant = 0
        }
        
        if "Full Day Leave" == leaveTypLbl.text {
            self.returnDayTimeBtn.isHidden = true
            self.leaveDayTimeBtn.isHidden = true
        }else{
            self.returnDayTimeBtn.isHidden = false
            self.leaveDayTimeBtn.isHidden = false
        }
        
        if is12Houre == "true"{
            returnDayTimeBtn.setTitle("02:00 PM", for: .normal)
            leaveDayTimeBtn.setTitle("02:00 PM", for: .normal)
            
        }
    }
    
    func getFormattedDateString(from dateStr: String, initialFormat: String, requiredFormat: String) -> String {
        let formatter = DateFormatter.init()
        if globalDateFormate == "dd/MM/yyyy" {
            formatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            formatter.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }

        formatter.dateFormat = initialFormat
        let endDate = formatter.date(from: dateStr)
        
        formatter.dateFormat = requiredFormat
        return formatter.string(from: endDate!)
    }
}



