//
//  GlobalClass.h
//  GLF. Locker
//
//  Created by Jawad Ahmed on 07/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BLBackgroundSessionDelegate.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6PLUS (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPHONE_X (IS_IPHONE && [[UIScreen mainScreen] nativeBounds].size.height == 2436)
#define IS_IPHONE_XR (IS_IPHONE && [[UIScreen mainScreen] nativeBounds].size.height == 1624)
#define IS_IPHONE_XMAX (IS_IPHONE && [[UIScreen mainScreen] nativeBounds].size.height == 2436)





#define kOrbisVideoQualityKey @"OrbisVideoQuality"


@interface GlobalClass : NSObject <NSURLSessionDownloadDelegate>

@property (nonatomic, strong) IBOutlet UIWindow* window;
//@property (nonatomic, strong) IBOutlet CDVViewController* viewController;

@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSString *currentTargetC;

@property (nonatomic, strong) NSMutableArray *pointsA;

@property (nonatomic, strong) NSDictionary *academyData;


@property (nonatomic, strong) BLBackgroundSessionDelegate *backgroundSessionDelegate;
@property (nonatomic, strong) NSMutableArray *backgroundSessions;

@property(nonatomic,retain) NSArray *tools;
@property int  degree;
@property int  changeVal;
@property (nonatomic) BOOL shouldAllowLandscape;

@property (nonatomic, retain) NSArray *messageRecipients;

float roundToN(float num, int decimals);


+ (GlobalClass *)sharedInstance;
+ (void) appendLogString:(NSString*)string;
+ (void) setLogFileNameString;
+ (void) writeAndSaveLogString;
+ (void) resetLogString;

+ (void) writeLocationToLogFile:(NSString*)logToWrite;

- (instancetype)init;
- (NSURLSession*) createBackgroundSessionWithID:(NSString*)sessionID;

- (void) addSession:(NSURLSession*)sessionToAdd;
- (BOOL) sessionIdentifierAlreadyExists:(NSString*)sessionIdentifier;
- (NSArray*) savedSessionIdentifiers;
- (void) removeSession:(NSURLSession*)sessionToRemove;

+ (NSString*) getJSONDictionaryWithDictionary:(NSDictionary*)requestOptions;
+ (NSString*) getTimeStringFromSeconds:(int)secnds;


- (void)videoUploadingResumed:(NSString *)callBackID;
- (void)videoUploadingStarted:(NSString *)callBackID;
- (void)videoUploadingFailedwithError:(NSString *)errorMessage andCallBackID:(NSString *)callBackID;
- (void)videoUploadingStopped:(NSString *)callBackID;
- (void)videoUploadingCancelled:(NSString *)callBackID;

- (void)setUploadProgressViewController:(UIViewController *)vc;
- (UIViewController *)getUploadProgressViewController;
-(NSString*)parsingNotificationMessage:(NSDictionary*)userInfo;





@end
