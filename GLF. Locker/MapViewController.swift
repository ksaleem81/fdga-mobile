//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
//import Crashlytics
@available(iOS 10.0, *)
class MapViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var latitude = 0.00
    var longitude = 0.00
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var academyName = ""
    var map:GMSMapView?
    @IBOutlet weak var searchFld: UITextField!
    var academySearchMade = false

    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingView()
      
        
       self.title = "First Degree Golf Solutions"
    }
    
    
    func settingView()  {
        
        searchFld.inputAccessoryView = addToolBar()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        refreshTableView()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    //MARK:- placing markers on map
    func placeMarkers()  {
        
        map?.clear()
        var lat = 0.0
        var long = 0.0
        if data.count > 0 {
         
            if academySearchMade {
                lat = Double(data[0]["Latitude"] as! Double)
                long = Double(data[0]["Longitude"] as! Double)
                
            }else{
                
                  if (LocationManager.sharedInstance.currentLocation?.coordinate != nil ){
                   lat = LocationManager.sharedInstance.currentLocation.coordinate.latitude
                    long =  LocationManager.sharedInstance.currentLocation.coordinate.longitude
                }
            }

            let camera = GMSCameraPosition.camera(withLatitude:lat, longitude:long, zoom: 6.0)
            map = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: mapView.frame.size.width, height: mapView.frame.size.height), camera: camera)
            
        }else{
            return
        }
    
        self.map?.delegate = self
        self.mapView.addSubview(map!)
//        if academySearchMade {
//            
//        }else{
//            let marker = GMSMarker()
//            marker.position = CLLocationCoordinate2D(latitude:Double(lat), longitude: Double(long))
//            marker.map = map
//        }

        var index = -1
        for dic in data {
            
            index = index + 1
            var lat = 0.0
            var long = 0.0
            
            if let lat1 = dic["Latitude"] as? Double{
                lat = lat1
            }
            
            if let long1 = dic["Longitude"] as? Double{
               long = long1
            }
            
            if lat == 0.0 || long == 0.0 {
                continue
            }
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude:Double(lat), longitude: Double(long))
            
            if let country = dic["Country"] as? String{
               marker.title = country
            }
            
            if let adress = dic["Address"] as? String{
                marker.snippet  = adress
            }
            
            marker.zIndex = Int32(index)
            marker.map = map
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(MapViewController.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
        
    }
    
   @objc func donePressed() {
        
      academyName = self.searchFld.text!
        self.searchFld.resignFirstResponder()
        if academyName.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
        }else{
            return
        }
        academySearchMade = true
        refreshTableView()
        searchFld.resignFirstResponder()
        print(getLocationInfo())
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    //MARK:- fetching data from server
    func refreshTableView()  {
      
        var lat = ""
        var long = ""
        if (LocationManager.sharedInstance.currentLocation?.coordinate != nil ){
            long = "\(LocationManager.sharedInstance.currentLocation.coordinate.longitude)"
            lat = "\(LocationManager.sharedInstance.currentLocation.coordinate.latitude)"
        }
        
        var parameters = [String:AnyObject]()
        var urlStr = "GetAllAcademiesOrderByLatLong"
        if currentTarget == "Chris Ryan Golf" {
            self.title = currentTarget
         urlStr = "GetAllOwnerAcademiesOrderByLatLong"
            let ownerID1 = 36846
            //        let ownerID1 = 33944bv //beta
            parameters =  ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject,"ownerID":ownerID1 as AnyObject]
        }else if currentTarget == "Playgolf" {
            urlStr = "GetAllOwnerAcademiesOrderByLatLong"
            let ownerID1 = 38130//beta//15337//live//38130
            parameters =  ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject,"ownerID":ownerID1 as AnyObject]
        }else if currentTarget == "TPC Group" {
            urlStr = "GetAllOwnerAcademiesOrderByLatLong"
            let ownerID1 = 44017//live44017//beta 44017
            parameters =  ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject,"ownerID":ownerID1 as AnyObject]
        }else if currentTarget == "ClubCorp Locker" {
            urlStr = "GetAllOwnerAcademiesOrderByLatLong"
            let ownerID1 = 639//live639//beta 639
            parameters =  ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject,"ownerID":ownerID1 as AnyObject]
        }else if currentTarget == "Kemper Sports" {
            urlStr = "GetAllOwnerAcademiesOrderByLatLong"
            let ownerID1 = 3899//live639//beta 3899
            parameters =  ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject,"ownerID":ownerID1 as AnyObject]
        }else if currentTarget == "FDGS" {
            urlStr = "GetAllOwnerAcademiesOrderByLatLong"
            let ownerID1 = 14346//live639//beta 3899
            parameters =  ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject,"ownerID":ownerID1 as AnyObject]
        }
        else{
            parameters = ["academyname":academyName as AnyObject,"latitude":lat
                as AnyObject,"longitude":long as AnyObject]
        }
        
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "academy/\(urlStr)", parameter:parameters , view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case   _ as NSNull :
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            default: break
            }
            
            self.data = object as! [[String : AnyObject]] 
            self.tableView.reloadData()
            self.placeMarkers()
        }) { (error) in
            print(error!)
           self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- tableview dataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell", for: indexPath) as! MapTableViewCell
        if let title = data[indexPath.row]["AcademyName"] as? String{
           cell.companyLbl.text = title
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AcademyViewController") as! AcademyViewController
        if let title = data[indexPath.row]["AcademyID"] as? Int{
            controller.acedmyId = String(title)
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- get location
    func getLocationInfo() -> String {
        
        var city = "" , state = ""  , country = "" , postelCode = ""
         latitude = 0.00
         longitude = 0.00
        
        if( LocationManager.sharedInstance.locationInformation.value(forKey: "city") != nil){
            city = LocationManager.sharedInstance.locationInformation.value(forKey: "city")   as! String
        }
        
        if( LocationManager.sharedInstance.locationInformation.value(forKey: "state") != nil){
            state = LocationManager.sharedInstance.locationInformation.value(forKey: "state")   as! String
        }
        
        if(LocationManager.sharedInstance.locationInformation.value(forKey: "postalcode") != nil){
            
            postelCode = LocationManager.sharedInstance.locationInformation.value(forKey: "postalcode")   as! String
        }
        
        if( LocationManager.sharedInstance.locationInformation.value(forKey: "country") != nil){
            country = LocationManager.sharedInstance.locationInformation.value(forKey: "country")   as! String
        }
        
        if( LocationManager.sharedInstance.locationInformation.value(forKey: "latitude") != nil && LocationManager.sharedInstance.locationInformation.value(forKey: "latitude") != nil ) {
            latitude = LocationManager.sharedInstance.locationInformation.value(forKey: "latitude") as! Double
            longitude = LocationManager.sharedInstance.locationInformation.value(forKey: "longitude") as! Double
        }
        
        _ =  NSCharacterSet(charactersIn:".,_~:;!=\"#%/<>?@\\^`{|}+-*/[]$&() ").inverted
        let locationInfo = "country=\(country)&latitude=\(latitude)&longitude=\(longitude)&state=\(state)&zipCode=\(postelCode)&city=\(city)"
        
        return locationInfo.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
    }
}

@available(iOS 10.0, *)
extension MapViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print(marker.zIndex)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AcademyViewController") as! AcademyViewController
        if let title = data[Int(marker.zIndex)]["AcademyID"] as? Int{
            controller.acedmyId = String(title)
        }
        self.navigationController?.pushViewController(controller, animated: true)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    }
}

extension UIViewController {
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func showInternetError(error:NSError)  {
        if let err = error as? URLError, err.code  == URLError.Code.notConnectedToInternet{
            DataManager.sharedInstance.printAlertMessage(message:"No internet connection", view:self)
        }else if let err = error as? URLError, err.code  == URLError.Code.timedOut{
            DataManager.sharedInstance.printAlertMessage(message:"The request timed out", view:self)
        }
        else{
            DataManager.sharedInstance.printAlertMessage(message:"Network Error", view:UIApplication.getTopestViewController()!)
        }
    }
}

extension String {
    
    func removeLastGivenCharacters(givenString:String,charatersCount:Int) -> String {

        let endIndex = givenString.index(givenString.endIndex, offsetBy: -charatersCount)
        let truncated = givenString.substring(to: endIndex)
        return truncated
        
    }
}

extension UITextField {
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}




