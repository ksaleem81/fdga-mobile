//
//  GalleryViewController.h
//  Orbis
//
//  Created by Nasir Mehmood on 8/25/15.
//
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"

@protocol GalleryViewControllerDelegate <NSObject>
@optional
- (void)galleryViewControllerDidSelectedVideo:(NSDictionary *)videoInfo;
- (void)galleryViewControllerDidSelectedImage:(NSDictionary *)imageInfo;
- (void)galleryViewControllerDidSelectedAnalyse:(NSDictionary *)videoInfo;
- (void)galleryViewControllerDidSelectedTrim:(NSDictionary *)videoInfo;
@end


typedef enum : NSUInteger {
    GalleryTypePhoto,
    GalleryTypeVideo
} GalleryType;

@interface GalleryViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIActionSheetDelegate>
@property (nonatomic, weak) MMDrawerController *mmDrawerController;
@property (weak, nonatomic) IBOutlet UICollectionView *galleryView;
@property (nonatomic) GalleryType galleryType;

@property (nonatomic) BOOL isFromLessonVideosPage;
@property (nonatomic) BOOL isFromSelectMediaPage;
@property (nonatomic) BOOL isFromLessonPhotosPage;

@property (nonatomic) id<GalleryViewControllerDelegate> delegate;

- (void) setVideoGalleryItems:(NSArray*)videoItems;
- (void) setPhotoGalleryItems:(NSArray*)photoItems;


@end
