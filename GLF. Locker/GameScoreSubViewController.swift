//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage

protocol SelectedGameDelegate {
    func seletedSkillsData(selectedData: [[String:AnyObject]],selectedUserId:String)
}

class GameScoreSubViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

  
    var delegate : SelectedGameDelegate?
    var isForOverView = false
    var selectedItems = [[String:AnyObject]]()
    var dataArray = [[String:AnyObject]]()
    var selectedDataArray = [[String:AnyObject]]()
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
 
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if isForOverView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GameScoreDefaultTableViewCell", for: indexPath) as! GameScoreDefaultTableViewCell
            if let title = dataArray[indexPath.row]["ObtainedScore"] as? String{
                cell.scoreLbl.text = title
            }
            if let title = dataArray[indexPath.row]["SkillName"] as? String{
                cell.companyLbl.text = title
            }
            
            return cell
            
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameScoreSubTableViewCell", for: indexPath) as! GameScoreSubTableViewCell
        if let title = dataArray[indexPath.row]["SkillName"] as? String{
           cell.companyLbl.text = title
        }
            if let title = dataArray[indexPath.row]["ObtainedScore"] as? String{
                cell.scoreLbl.text = title
            }
            cell.field.tag = indexPath.row
            cell.delegate = self
           return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    
    }

}
extension GameScoreSubViewController:textFieldDelegate{
    func textFieldTextReturn(text: String,tag:Int) {
        
        var textOFField = text
        var dictionary = [String:AnyObject]()
        if let val =  dataArray[tag]["SkillId"] as? Int {
            textOFField = textOFField + "$" + "\(val)"
            dictionary["SkillId"] = val as AnyObject?
            dictionary["val"] = text as AnyObject?
        }
     var elementAdded = false
        let k = self.selectedDataArray
        let index = k.index {
            if let dic = $0 as? Dictionary<String,AnyObject> {
                if let value = dic["SkillId"]  as? Int, value == dataArray[tag]["SkillId"] as? Int{
               elementAdded = true
                    return true
                }
            }
  
            elementAdded = false
            return false
        }

        if elementAdded {
            selectedDataArray[index!]["val"] = text as AnyObject?
        }else{
            selectedDataArray.append(dictionary)
        }
        
       
       delegate?.seletedSkillsData(selectedData: selectedDataArray, selectedUserId: "")
    }
}

