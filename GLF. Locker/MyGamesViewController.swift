//
//  MyGamesViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 2/7/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
//import Charts
import JYRadarChart
import SideMenu
class MyGamesViewController: UIViewController {

    @IBOutlet weak var graphImage: UIImageView!
    @IBOutlet weak var graphView: UIView!
    var studentData = [String:AnyObject]()
    var graphData = [[String:AnyObject]]()
    @IBOutlet weak var pieGraphView: UIView!//PieChartView!
    var facebookCounter : Int = 0
    var twitterCounter : Int = 0
    var emailCounter : Int = 0
    //Mark:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gettingGraphData()
        settingRighMenuBtn()
        self.navigationItem.title = "My Game"
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MyGamesViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- server data fetch
    func gettingGraphData()  {
   
        var studentId = ""
        if let id = studentData["playerId"] as? Int{
            studentId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetStudentSkillGraph/\(studentId)/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
                // should never happen!
            }
            
            let data = (object as? [[String:AnyObject]])!
            self.pieGraphView.isHidden = true
            let chart = JYRadarChart.init(frame: self.pieGraphView.frame)
            self.pieGraphView.superview?.addSubview(DataManager.sharedInstance.gameScoreGraph(chart: chart, data:data,controller: self))
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    //MARK:- buttons actions
    @IBAction func drillBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PracticeViewController") as! PracticeViewController
        controller.studentData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func goalsBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecommendedViewController") as! RecommendedViewController
        controller.studentData = studentData
        controller.fromGoal = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    @IBAction func recommendedBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecommendedViewController") as! RecommendedViewController
        controller.studentData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }

    
    
    
  

}

