
#import "OrbisCameraViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "OrbisMediaGallery.h"
//#import <Cordova/CDVPluginResult.h>
//#import "MainViewController.h"
//#import "AppDelegate.h"
#import "GlobalClass.h"
#import <CoreVideo/CoreVideo.h>
#import "MBProgressHUD.h"
#import "UIImage+fixOrientation.h"
#import "AVAsset+VideoOrientation.h"
#import "ShapeViewController.h"
#define kMediaTypeVideo 0
#define kMediaTypePhoto 1

#define kVideoSourceTypeLibrary 0
#define kVideoSourceTypeCamera  1
@interface OrbisCameraViewController ()
{
    int videoRecordDurationInSeconds;
    BOOL shouldSaveVideoToDevicePhotoLibrary;
    __weak GlobalClass *_appDelegate;
    BOOL shouldSaveVideoToGallery;
    
    int captuerMediaType;
    NSString *currentVideoPath;
    
    AVCaptureDevicePosition userPreferedCameraDevice;
    NSOperationQueue *analysisQueue;
    NSMutableArray *analysisScreens;
    
    BOOL isRecording;
    BOOL isDoneGeneratingFrames;
    
    float initialBufferTime;
    
    MBProgressHUD *hud;
    BOOL isSavingVideo;
    
    CFMutableArrayRef arrCFImageBuffer;
    
    NSMutableArray *imagesToSave;
    NSTimer *imageTimer;
}

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) NSString *cbID;
@property (nonatomic, strong) NSTimer *recordButtonAnimationTimer;
@property (nonatomic, strong) NSTimer *recordingTimer;


@end

static OrbisCameraViewController *_orbisCameraController=nil;

@implementation OrbisCameraViewController

@synthesize blCameraController, updateRecordingLabelTimer, recordButtonAnimationTimer, recordingTimer;

- (id) init
{
    self=[super init];
    if(self)
    {
        videoRecordDurationInSeconds=60;
        shouldSaveVideoToDevicePhotoLibrary=NO;
//        _appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
        _appDelegate = [GlobalClass sharedInstance];
        shouldSaveVideoToGallery=YES;
        userPreferedCameraDevice=AVCaptureDevicePositionBack;
        isRecording=NO;
        isDoneGeneratingFrames=NO;
        initialBufferTime=-1;
        isSavingVideo=NO;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        videoRecordDurationInSeconds=60;
        shouldSaveVideoToDevicePhotoLibrary=NO;
//        _appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
        _appDelegate = [GlobalClass sharedInstance];
        shouldSaveVideoToGallery=YES;
        userPreferedCameraDevice=AVCaptureDevicePositionBack;
        isRecording=NO;
        isDoneGeneratingFrames=NO;
        initialBufferTime=-1;
        isSavingVideo=NO;
        
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    analysisQueue=[NSOperationQueue new];
    analysisQueue.maxConcurrentOperationCount=1;
    arrCFImageBuffer = CFArrayCreateMutable(NULL, 0, &kCFTypeArrayCallBacks);
    imagesToSave=[NSMutableArray array];
    if(!imageTimer){
        
        NSRunLoop *runloop=[NSRunLoop currentRunLoop];
        imageTimer=[NSTimer timerWithTimeInterval:0.2 target:self selector:@selector(storeImage) userInfo:nil repeats:YES];
        [runloop addTimer:imageTimer forMode:NSDefaultRunLoopMode];
    }
    
    
    

    
  //  [self bringToolToFronView];
}

-(void)bringToolToFronView{
    [self.view bringSubviewToFront:self.cancelButton];
    [self.view bringSubviewToFront:self.flashButton];
    [self.view bringSubviewToFront:self.changeCameraButton];
    [self.view bringSubviewToFront:self.recordButton];
    [self.view bringSubviewToFront:self.timerLabel];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"didReceiveMemoryWarning");
}
#pragma mark -  rotation delegates

- (BOOL) shouldAutorotate
{
 
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}



#pragma mark
#pragma mark UIImagePickerControllerDelegate Methods



- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
   
    
    if(captuerMediaType==kMediaTypeVideo)
    {
        NSString* moviePath = [(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL] path];
   
        //iOS13
        currentVideoPath=[[OrbisMediaGallery sharedGallery] getPathForNextLibraryVideo];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:moviePath toPath:currentVideoPath error:&error];

        NSLog(@"%@",currentVideoPath);
        if (moviePath)
        {
            [self videoCaptureCompletedWithMoviePath:currentVideoPath];
        }
    }
    else
    {
        UIImage *capturedImage=[info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSString *tmpDirectory = NSTemporaryDirectory();
        NSString *imagePath = [tmpDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"tImage%f.png", [NSDate timeIntervalSinceReferenceDate]]];
        
        NSData *jpegData=UIImageJPEGRepresentation(capturedImage, 0.5);
        [jpegData writeToFile:imagePath atomically:NO];
        
        NSLog(@"Temp Image:%@", imagePath);
        
        if (imagePath)
        {
            [self imageCaptureCompletedWithImagePath:imagePath];
        }
    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [self videoCaptureCancelled];
}




#pragma mark
#pragma mark Custom Mehtods

+ (OrbisCameraViewController*) getCameraController
{
    
 //   if(!_orbisCameraController)
   // {
    if(IS_IPHONE_5)
        _orbisCameraController=[[OrbisCameraViewController alloc] initWithNibName:@"OrbisCameraViewController_5" bundle:nil];
    else
        _orbisCameraController=[[OrbisCameraViewController alloc] initWithNibName:@"OrbisCameraViewController" bundle:nil];
  //  }

    return _orbisCameraController;
}

- (void) captureVideoWithCallbackID:(NSString*)callbackID withOptions:(NSDictionary*)options{
    
    shouldSaveVideoToGallery=YES;
    captuerMediaType=kMediaTypeVideo;
    self.cbID=callbackID;
    initialBufferTime=-1;
    NSLog(@"%@",((UINavigationController*)_appDelegate.window.rootViewController).visibleViewController);

    UIImagePickerControllerSourceType videoSourceType=UIImagePickerControllerSourceTypeCamera;
    if(options)
    {
        NSNumber *shoudSave=[options objectForKey:@"shouldSaveVideoToGallery"];
        if(shoudSave){
            shouldSaveVideoToGallery=[shoudSave boolValue];
        }
        
        NSNumber *videoSource=[options objectForKey:@"videoSource"];
        if(videoSource){
            videoSourceType=[videoSource intValue];  // 0 library, 1 camera
            if (videoSourceType == 1) {
                [GlobalClass sharedInstance].shouldAllowLandscape = YES;
            }
        }
        
        NSNumber *mediaTyp=[options objectForKey:@"mediaType"];
        if(mediaTyp && mediaTyp.integerValue==kMediaTypePhoto){
            captuerMediaType=kMediaTypePhoto;
        }
    }
    
    if(videoSourceType==kVideoSourceTypeLibrary || captuerMediaType==kMediaTypePhoto){
        if (videoSourceType==UIImagePickerControllerSourceTypeCamera && ![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            //show error msg
            return;
        }
        
        NSString* mediaType = nil;
        NSArray* types = nil;
        if ([UIImagePickerController respondsToSelector:@selector(availableMediaTypesForSourceType:)]){
            
            types = [UIImagePickerController availableMediaTypesForSourceType:videoSourceType];
            if(captuerMediaType==kMediaTypeVideo)
            {
                if ([types containsObject:(NSString*)kUTTypeMovie]) {
                    mediaType = (NSString*)kUTTypeMovie;
                } else if ([types containsObject:(NSString*)kUTTypeVideo]) {
                    mediaType = (NSString*)kUTTypeVideo;
                }
            }
            else
            {
                if ([types containsObject:(NSString*)kUTTypeImage]) {
                    mediaType = (NSString*)kUTTypeImage;
                }
            }
        }
        
        if (videoSourceType==UIImagePickerControllerSourceTypeCamera && !mediaType)
        {
            NSLog(@"Capture.captureVideo: video mode not available.");
            //show error msg
            return;
        }
        else
        {
            self.imagePicker=[[UIImagePickerController alloc] init];
            _imagePicker.delegate=self;
            _imagePicker.sourceType = videoSourceType;
            
            // iOS 3.0
            if(mediaType)
                _imagePicker.mediaTypes = [NSArray arrayWithObjects:mediaType, nil];
            
            if(videoSourceType==UIImagePickerControllerSourceTypeCamera){
                // iOS 4.0
                if (captuerMediaType==kMediaTypeVideo && [_imagePicker respondsToSelector:@selector(cameraCaptureMode)])
                {
                    _imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
                    _imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
                    
                    NSString *videoQuality=[[NSUserDefaults standardUserDefaults] objectForKey:kOrbisVideoQualityKey];
                    if([videoQuality isEqualToString:@"low"])
                    {
                        _imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
                    }
                    else if([videoQuality isEqualToString:@"medium"])
                    {
                        _imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
                    }
                    else if([videoQuality isEqualToString:@"high"])
                    {
                        _imagePicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
                    }
                    
                    _imagePicker.videoMaximumDuration=videoRecordDurationInSeconds;//  60secs = 1 min
                }
            }
            
            _imagePicker.allowsEditing = NO;
            //cobra change because of side menu if wrong coment and uncomment below
            [[self topMostController] presentViewController:_imagePicker animated:YES completion:^{
                
            }];
            
//            [_appDelegate.window.rootViewController presentViewController:_imagePicker animated:YES completion:^{
//
//            }];
        }
    }
    else
    {
        recordButtonAnimationTimer=nil;
        analysisScreens=[NSMutableArray array];
        self.blCameraController=[[BLCameraController alloc] init];
        blCameraController.delegate=self;
        currentVideoPath=[[OrbisMediaGallery sharedGallery] getPathForNextLibraryVideo];
        [blCameraController setCapturedFilePath:currentVideoPath];
        [self.blCameraController setRotate:YES];
        //    self.imagePicker=[[UIImagePickerController alloc] init];
        //    imagePicker.delegate=self;
        
        NSString* mediaType = nil;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            // there is a camera, it is available, make sure it can do movies
            //        self.imagePicker = [[UIImagePickerController alloc] init];
            
            NSArray* types = nil;
            if ([UIImagePickerController respondsToSelector:@selector(availableMediaTypesForSourceType:)])
            {
                types = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                
                if ([types containsObject:(NSString*)kUTTypeMovie]) {
                    mediaType = (NSString*)kUTTypeMovie;
                } else if ([types containsObject:(NSString*)kUTTypeVideo]) {
                    mediaType = (NSString*)kUTTypeVideo;
                }
            }
        }
        
        if (!mediaType)
        {
            NSLog(@"Capture.captureVideo: video mode not available.");
            //        self.imagePicker = nil;
        }
        else
        {
            [blCameraController setCaptureDevicePosition:userPreferedCameraDevice];
            self.changeCameraButton.selected=(userPreferedCameraDevice==AVCaptureDevicePositionFront);
            
            NSString *videoQuality=[[NSUserDefaults standardUserDefaults] objectForKey:kOrbisVideoQualityKey];
            if([videoQuality isEqualToString:@"low"])
            {
                [blCameraController setSessionPreset:AVCaptureSessionPresetLow];
            }
            else if([videoQuality isEqualToString:@"medium"])
            {
                [blCameraController setSessionPreset:AVCaptureSessionPresetMedium];
            }
            else if([videoQuality isEqualToString:@"high"])
            {
                [blCameraController setSessionPreset:AVCaptureSessionPresetHigh];
            }
            
            [blCameraController setCaptureDuation:videoRecordDurationInSeconds];
            [blCameraController setOverlayView:self.view];
            
            self.cancelButton.enabled=YES;
            self.changeCameraButton.hidden=NO;
            self.timerLabel.text=@"00:00";
            
            [self bringToolToFronView];
            
            // CHM Jawad: Present Main Controller
            
            // cobra change because of side menu if wrong coment and uncomment below
            [[self topMostController] presentViewController:blCameraController animated:YES completion:^{
                [blCameraController setupAndStartCaptureSession];
            }];
//            [_appDelegate.window.rootViewController presentViewController:blCameraController animated:YES completion:^{
//                [blCameraController setupAndStartCaptureSession];
//            }];
            recordDuration=-1;
            [self updateTimerLabel];
            
        //    self.recordingTimer=[NSTimer timerWithTimeInterval:videoRecordDurationInSeconds target:self selector:@selector(recordingTimeUp) userInfo:nil repeats:NO];
          //  [[NSRunLoop mainRunLoop] addTimer:recordingTimer forMode:NSDefaultRunLoopMode];

        }
    }
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (void) stopVideoCaptureWithCallbackID:(NSString*)callbackID
{
    if(_imagePicker)
    {
        [_imagePicker stopVideoCapture];
    }
}

- (void) cancelVideoCaptureWithCallbackID:(NSString*)callbackID
{
    
}

- (void) videoCaptureCompletedWithMoviePath:(NSString*) moviePath
{

    NSLog(@"%@",((UINavigationController*)_appDelegate.window.rootViewController).visibleViewController);

    
    if(shouldSaveVideoToGallery && (blCameraController || self.imagePicker))
    {
        [_appDelegate.window.rootViewController dismissViewControllerAnimated:NO completion:^{
            blCameraController=nil;
            self.imagePicker=nil;
//            [self saveVideo:moviePath];
            //cobra
            double delayInSeconds = 0.2;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self saveVideo:moviePath];
            });
        }];
    }
    else
    {
        
        //dismissing issue refix
        if ([((UINavigationController*)_appDelegate.window.rootViewController).visibleViewController isKindOfClass:ShapeViewController.class]) {
            return;
        }//till here

        [GlobalClass sharedInstance].shouldAllowLandscape = NO;
      
        //cobra
        //repel effect
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        [_appDelegate.window.rootViewController dismissViewControllerAnimated:NO completion:^{
            AVAsset *_asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:moviePath]];
            CMTime assetDuration=_asset.duration;
            
            Float64 duration=CMTimeGetSeconds(assetDuration);
//            NSLog(@"the duration is! %f",duration);
            self.imagePicker=nil;
            if(_cbID)
            {
                // CHM Jawad
                if (_delegate && [_delegate respondsToSelector:@selector(orbisCameraVC:didFinishRecordingVideo:withCallBackID:)]) {
                    [_delegate orbisCameraVC:self didFinishRecordingVideo:@{@"videoPath":moviePath, @"duration": [NSNumber numberWithFloat:duration]} withCallBackID:_cbID];
                }
//                CDVPluginResult *result=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"videoPath":moviePath, @"duration": [NSNumber numberWithFloat:duration]}];
//                [((MainViewController*)_appDelegate.window.rootViewController).commandDelegate sendPluginResult:result callbackId:_cbID];
            }
        }];
//        });
        
    }
}

- (void) imageCaptureCompletedWithImagePath:(NSString*) imagePath
{
    //    if(shouldSaveVideoToGallery)
    //    {
    //        [_appDelegate.window.rootViewController dismissViewControllerAnimated:NO completion:^{
    //            self.imagePicker=nil;
    //            [self saveVideo:moviePath];
    //        }];
    //    }
    //    else
    //    {
    [_appDelegate.window.rootViewController dismissViewControllerAnimated:NO completion:^{
        self.imagePicker=nil;
        if(_cbID)
        {
            // CHM Jawad
//            CDVPluginResult *result=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"imagePath":imagePath}];
//            [((MainViewController*)_appDelegate.window.rootViewController).commandDelegate sendPluginResult:result callbackId:_cbID];
            if (_delegate && [_delegate respondsToSelector:@selector(orbisCameraVC:didFinishCapturingImage:withCallBackID:)]) {
                [_delegate orbisCameraVC:self didFinishCapturingImage:@{@"imagePath":imagePath} withCallBackID:_cbID];
            }
        }
    }];
    //    }
}


- (void) videoCaptureCancelled
{
    [GlobalClass sharedInstance].shouldAllowLandscape = NO;
    [_appDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        self.imagePicker=nil;
        if(_cbID)
        {
            // TODO: Callback
//            CDVPluginResult *result=[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:@{@"errorCode":@(1), @"errorMessage":@"Video captured cancelled"}];
//            [((MainViewController*)_appDelegate.window.rootViewController).commandDelegate sendPluginResult:result callbackId:_cbID];
        }
    }];
}

- (void) saveVideo:(NSString*)moviePath
{
//        NSString *newPath=[[OrbisMediaGallery sharedGallery] saveVideoToGalleryAtPath:moviePath];
    NSDictionary *newVideoInfo=nil;
    if([moviePath.pathExtension caseInsensitiveCompare:@"mp4"]==NSOrderedSame)
    {
        newVideoInfo=[[OrbisMediaGallery sharedGallery] saveVideoToGalleryWithPath:moviePath];
    }
    else
    {
        newVideoInfo=[[OrbisMediaGallery sharedGallery] saveVideoToGalleryAtPath:moviePath];
    }
    
    if(analysisScreens.count>0)
        newVideoInfo=[[OrbisMediaGallery sharedGallery] saveAnalysisImages:analysisScreens forVideoAtPath:currentVideoPath];
    
    if(_cbID)
    {
        // CHM Jawad
//        CDVPluginResult *result=[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:newVideoInfo];
//        [((MainViewController*)_appDelegate.window.rootViewController).commandDelegate sendPluginResult:result callbackId:_cbID];
        if (_delegate && [_delegate respondsToSelector:@selector(orbisCameraVC:didFinishSavingVideo:withCallBackID:)]) {
            [_delegate orbisCameraVC:self didFinishSavingVideo:newVideoInfo withCallBackID:_cbID];
        }
    }
    
    if(shouldSaveVideoToDevicePhotoLibrary)
    {
        UISaveVideoAtPathToSavedPhotosAlbum(moviePath, self, @selector(video:didFinishSavingWithError:contextInfo:),nil);
    }
}
//
-(void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if(error)
    {

    }
    else
    {

    }
}

- (NSDictionary*)getMediaDictionaryFromPath:(NSString*)fullPath ofType:(NSString*)type
{
    NSFileManager* fileMgr = [[NSFileManager alloc] init];
    NSMutableDictionary* fileDict = [NSMutableDictionary dictionaryWithCapacity:5];
    
    [fileDict setObject:[fullPath lastPathComponent] forKey:@"name"];
    [fileDict setObject:fullPath forKey:@"fullPath"];
    NSDictionary* fileAttrs = [fileMgr attributesOfItemAtPath:fullPath error:nil];
    [fileDict setObject:[NSNumber numberWithUnsignedLongLong:[fileAttrs fileSize]] forKey:@"size"];
    NSDate* modDate = [fileAttrs fileModificationDate];
    NSNumber* msDate = [NSNumber numberWithDouble:[modDate timeIntervalSince1970] * 1000];
    [fileDict setObject:msDate forKey:@"lastModifiedDate"];
    
    return fileDict;
}



#pragma mark
#pragma mark IBAction Methods

- (IBAction)changeCameraButtonClicked:(id)sender
{
    if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
    {
        if(userPreferedCameraDevice == AVCaptureDevicePositionBack)
            userPreferedCameraDevice = AVCaptureDevicePositionFront;
        else
            userPreferedCameraDevice = AVCaptureDevicePositionBack;
    }
    
    [blCameraController setCaptureDevicePosition:userPreferedCameraDevice];
    
    UIButton *b=(UIButton*)sender;
    b.selected=!b.selected;
}

- (IBAction)falshButtonClicked:(id)sender
{
    UIButton *b=(UIButton*)sender;
    
    if([blCameraController isFlashAvailable])
    {
        if(b.selected)
            [blCameraController turnFlash:NO];
        else
            [blCameraController turnFlash:YES];
        
        b.selected=!b.selected;
    }
}


- (IBAction)recordButtonClicked:(id)sender
{
    UIButton *b=(UIButton*)sender;
    if(b.selected)
    {
        if([recordButtonAnimationTimer isValid])
            [recordButtonAnimationTimer invalidate];
        self.recordButtonAnimationTimer=nil;
        self.recordButton.highlighted=NO;
        
        if([updateRecordingLabelTimer isValid])
            [updateRecordingLabelTimer invalidate];
        self.updateRecordingLabelTimer=nil;
        
        if([recordingTimer isValid])
            [self.recordingTimer invalidate];
        self.recordingTimer=nil;
        
        b.selected=NO;
        //        [imagePicker stopVideoCapture];
        if(blCameraController!=nil)
        {
            [blCameraController stopVideoRecording];
        }
        else
        {
            [self videoCaptureCompletedWithMoviePath:currentVideoPath];
        }
        
        self.timerLabel.hidden=YES;
        self.cancelButton.enabled=YES;
        self.changeCameraButton.hidden=NO;
        
        
        isRecording=NO;
    }
    else
    {
        
        blCameraController.rotate = NO;
        isRecording=YES;
        b.selected=YES;
        
        //        [imagePicker startVideoCapture];
        [blCameraController startVideoRecording];
        
        self.cancelButton.enabled=NO;
        self.changeCameraButton.hidden=YES;
        self.timerLabel.hidden=NO;
        
        self.recordButtonAnimationTimer=[NSTimer timerWithTimeInterval:0.5 target:self selector:@selector(animateRecordingButton) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:recordButtonAnimationTimer forMode:NSDefaultRunLoopMode];
        
        
        self.updateRecordingLabelTimer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(updateTimerLabel) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:updateRecordingLabelTimer forMode:NSDefaultRunLoopMode];
        
        self.recordingTimer=[NSTimer timerWithTimeInterval:videoRecordDurationInSeconds target:self selector:@selector(recordingTimeUp) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:recordingTimer forMode:NSDefaultRunLoopMode];
    }
}

- (void) animateRecordingButton
{
    self.recordButton.highlighted=!self.recordButton.highlighted;
}


- (IBAction)cancelButtonClicked:(id)sender
{
    [self videoCaptureCancelled];
    [blCameraController stopAndTearDownCaptureSession];
}


- (void) updateTimerLabel
{
    recordDuration++;
    int minutes=recordDuration/60;
    int seconds=recordDuration%60;
    
    if(seconds<0)
        seconds=0;
    
    NSString *minutesString=[NSString stringWithFormat:@"%d",minutes];
    if(minutes<10)
        minutesString=[NSString stringWithFormat:@"0%d",minutes];
    
    NSString *secondsString=[NSString stringWithFormat:@"%d",seconds];
    if(seconds<10)
        secondsString=[NSString stringWithFormat:@"0%d",seconds];
    
    self.timerLabel.text=[NSString stringWithFormat:@"%@:%@", minutesString, secondsString];
    
  }


- (void) recordingTimeUp
{
    [self recordButtonClicked:self.recordButton];
    
}


#pragma mark - BLCameraControllerDelegate Methods

- (void) blCameraControllerDidStartRecordingVideo:(BLCameraController*)blCameraController
{
    
}

- (void) blCameraControllerDidStopRecordingVideo:(BLCameraController*)blCameraController
{
    NSLog(@"blCameraControllerDidStopRecordingVideo method start");
    
    isRecording=NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"blCameraControllerDidStopRecordingVideo method async block start");

        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Saving video...";
        [hud show:YES];

        NSLog(@"blCameraControllerDidStopRecordingVideo method async block end");
        
        if(!isRecording && isDoneGeneratingFrames)
        {
            NSDictionary *d=[analysisScreens lastObject];
            if(d)
            {
                [self saveFramesOfVideo:currentVideoPath startTime:[d objectForKey:@"analysisScreenTime"]];
            }
            else
            {
                [self saveFramesOfVideo:currentVideoPath startTime:@(0)];
            }
        }
        
    });
    
    NSLog(@"blCameraControllerDidStopRecordingVideo method start");

//    [self videoCaptureCompletedWithMoviePath:currentVideoPath];
}

- (void) blCameraControllerWillStartRecordingVideo:(BLCameraController*)blCameraController
{
    
}

- (void) blCameraControllerWillStopRecordingVideo:(BLCameraController*)blCameraController
{
    
}


- (void) blCameraControllerDidCaptureSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
//    shouldSaveVideoToGallery=NO;
    
//    CFArrayAppendValue(arrCFImageBuffer, sampleBuffer);
//    int count=(int)CFArrayGetCount(arrCFImageBuffer);
//    NSLog(@"count: %d", count);
    
    if(shouldSaveVideoToGallery)
    {
        [analysisQueue addOperationWithBlock:^{
            //        CMTime t=CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            CMTime t=CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer);
            float time=CMTimeGetSeconds(t);//t.value;
            if(initialBufferTime==-1)
            {
                initialBufferTime=time;
            }
            
            time=time-initialBufferTime;
            
            
            UIImage *analysisImage=[self imageFromSampleBuffer:sampleBuffer];
            CMSampleBufferInvalidate(sampleBuffer);
            CFRelease(sampleBuffer);
            
            NSString *videoName=[[currentVideoPath lastPathComponent] stringByDeletingPathExtension];
            
            NSString *analysisImagePath=[[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_analysisScreen%d.jpg", videoName, (int)(analysisScreens.count+1)]];
            
            
            //        [[OrbisMediaGallery sharedGallery] CGImageWriteToFile:analysisImage atPath:analysisImagePath];
            //        CGImageRelease(analysisImage);
            
//            [NSThread detachNewThreadSelector:@selector(storeImage:) toTarget:self
//                                   withObject:@{
//                                                @"image":analysisImage,
//                                                @"imagePath":analysisImagePath
//            }];
            
//            [self performSelectorInBackground:@selector(storeImage:) withObject:@{@"image":analysisImage, @"imagePath":analysisImagePath}];
            
//            [imagesToSave addObject:@{@"image":analysisImage, @"imagePath":analysisImagePath}];
            
            
//            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSData *imageData=UIImageJPEGRepresentation(analysisImage, 0.5);
                [imageData writeToFile:analysisImagePath atomically:NO];
//
////                [[OrbisMediaGallery sharedGallery] CGImageWriteToFile:analysisImage.CGImage atPath:analysisImagePath type:kUTTypeJPEG];
//
//            });
            
            NSDictionary *d=@{@"analysisScreenPath": analysisImagePath, @"analysisScreenTime": [NSNumber numberWithFloat:time]};
            NSLog(@"_analysisScreen: %@", d);
            [analysisScreens addObject:d];
            
            NSLog(@"isRecording:%@, analysisQueue.operationCount:%@", @(isRecording), @(analysisQueue.operationCount));
            if(analysisQueue.operationCount<=1)
                isDoneGeneratingFrames=YES;
            if(!isRecording && analysisQueue.operationCount<=1)
            {
                [self saveFramesOfVideo:currentVideoPath startTime:[d objectForKey:@"analysisScreenTime"]];
                //            [[OrbisMediaGallery sharedGallery] saveAnalysisImages:analysisScreens forVideoAtPath:currentVideoPath];
            }
        }];
    }
    else
    {
        isDoneGeneratingFrames=YES;
        CFRelease(sampleBuffer);
    }
}

- (void) storeImage//:(id)paramObject
{
//    if([paramObject isKindOfClass:[NSDictionary class]])
//    {
//    NSDictionary *d=(NSDictionary*)paramObject;

    NSDictionary *d=[imagesToSave lastObject];
    if(d)
    {
//        [imagesToSave removeLastObject];
        
        UIImage *analysisImage=[d objectForKey:@"image"];
        NSString *analysisImagePath=[d objectForKey:@"imagePath"];
        NSData *imageData=UIImageJPEGRepresentation(analysisImage, 0.5);
        [imageData writeToFile:analysisImagePath atomically:NO];
    }
//        [[OrbisMediaGallery sharedGallery] CGImageWriteToFile:analysisImage.CGImage atPath:analysisImagePath type:kUTTypeJPEG];
//    }
}


- (void) saveFramesOfVideo:(NSString*)videoPath startTime:(NSNumber*)startTime
{
    if(shouldSaveVideoToGallery)
    {
        AVAsset *_asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:videoPath]];
        AVAssetImageGenerator *_imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:_asset];
        _imageGenerator.requestedTimeToleranceBefore=kCMTimeZero;
        _imageGenerator.requestedTimeToleranceAfter=kCMTimeZero;
        CMTime assetDuration=_asset.duration;
        Float64 duration=CMTimeGetSeconds(assetDuration);
        NSMutableArray *analysisScreenTimes = [NSMutableArray array];
        for(float i=(startTime.floatValue+0.05); ; i+=0.05)//i<=duration; i+=0.05)
        {
            //        for(float j=0.0; j<1.0; j=j+0.05)
            //        {
            Float64 frameTime = i;//(Float64)i+roundToN(j, 2);
            if(frameTime>duration)
            {
                frameTime=duration;
                CMTime time = CMTimeMakeWithSeconds(frameTime, 600);//assetDuration.timescale);
                [analysisScreenTimes addObject:[NSValue valueWithCMTime:time]];
                break;
            }
            
            CMTime time = CMTimeMakeWithSeconds(frameTime, 600);//assetDuration.timescale);
            [analysisScreenTimes addObject:[NSValue valueWithCMTime:time]];
            //        }
        }
        
        _imageGenerator.appliesPreferredTrackTransform = YES;
        _imageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
        _imageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
        
        NSString *videoName=[[videoPath lastPathComponent] stringByDeletingPathExtension];
        
        [_imageGenerator generateCGImagesAsynchronouslyForTimes:analysisScreenTimes
                                              completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime,
                                                                  AVAssetImageGeneratorResult result, NSError *error) {
                                                  
                                                  if (result == AVAssetImageGeneratorSucceeded) {
                                                      float imageTime=(float)actualTime.value/actualTime.timescale;
                                                      NSString *analysisImagePath=[[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_analysisScreen%d.jpg", videoName, (int)(analysisScreens.count+1)]];
                                                      
                                                      [[OrbisMediaGallery sharedGallery] CGImageWriteToFile:image atPath:analysisImagePath type:kUTTypeJPEG];
                                                      
                                                      NSDictionary *d=@{@"analysisScreenPath": analysisImagePath, @"analysisScreenTime": [NSNumber numberWithFloat:imageTime]};
                                                      NSLog(@"_analysisScreen: %@", d);
                                                      [analysisScreens addObject:d];
                                                      
                                                  }
                                                  else if (result == AVAssetImageGeneratorFailed) {
                                                      NSLog(@"Failed with error: %@", [error localizedDescription]);
                                                  }
                                                  else if (result == AVAssetImageGeneratorCancelled) {
                                                      NSLog(@"Canceled");
                                                  }
                                                  
                                                  NSValue *v=[analysisScreenTimes lastObject];
                                                  if(v.CMTimeValue.value==requestedTime.value)
                                                  {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          //                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                          [hud hide:YES];
                                                          hud=nil;
                                                          //                                                      [self videoScreensReadyForAnalysis];
                                                          [self videoCaptureCompletedWithMoviePath:currentVideoPath];
                                                      });
                                                  }
                                                  
                                              }];
    }
    else
    {
        [self videoCaptureCompletedWithMoviePath:currentVideoPath];
    }

}


#pragma mark - SampleBuffer to CGImageRef

// Create a UIImage from sample buffer data
- (UIImage*) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    uint8_t *baseAddress = (uint8_t*)CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
//    CGContextRef context = CGBitmapContextCreate(baseAddress, width/1.5, height/1.5, 8,
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    
    if (!devOrientation ) {
      devOrientation = [UIDevice currentDevice].orientation;
    }
    
    UIImage *image=nil;
    
//    if (devOrientation == UIDeviceOrientationPortrait || devOrientation == UIDeviceOrientationFaceUp || devOrientation == UIDeviceOrientationFaceDown) {
    
    image=[[UIImage alloc] initWithCGImage:quartzImage];
//          image=[[UIImage alloc] initWithCGImage:quartzImage scale:1.0 orientation:UIImageOrientationRight];
    
//    }
//    else if (devOrientation == UIDeviceOrientationLandscapeRight || devOrientation == UIDeviceOrientationFaceUp || devOrientation == UIDeviceOrientationFaceDown)
//    {
//              image=[[UIImage alloc] initWithCGImage:quartzImage scale:1.0 orientation:UIImageOrientationDown];
//        
//    
//    }
//    else if (devOrientation == UIDeviceOrientationLandscapeLeft || devOrientation == UIDeviceOrientationFaceUp || devOrientation == UIDeviceOrientationFaceDown)
//    {
//     image=[[UIImage alloc] initWithCGImage:quartzImage scale:1.0 orientation:UIImageOrientationUp];
//    }
    

//    NSLog(@"image orientation %li",(long)image.imageOrientation);
    image = [image fixOrientation];
     
// Release the Quartz image
    CGImageRelease(quartzImage);
    
    return image;
    
}




@end


