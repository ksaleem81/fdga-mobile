//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

import SDWebImage
import SideMenu

class CartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{


    var dataGeneral:[[String:AnyObject]] = [[String:Any]]() as [[String : AnyObject]]
     var previousData = [String:AnyObject]()
    var studentData = [String:AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    var secondLastControllerData = [String:AnyObject]()
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var checkOutBtn: UIButton!
    var isRedeemedVoucher = false
    var fromRecommnded = false
    var redeemedData = [String:AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        print(fromRecommnded)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        var cartKey = ""
        if currentUserLogin == 4 {
            cartKey = "studentCart" +  userId
        }else{
            cartKey = "myCart" + userId
        }
        
       fetchingCartData(cartKey: cartKey)
    tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    func fetchingCartData(cartKey:String)  {
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: cartKey){
            if let loadedCart =  NSKeyedUnarchiver.unarchiveObject(with: (UserDefaults.standard.object(forKey: cartKey) as? Data)!) as? [[String:Any]]{
                dataGeneral =  loadedCart as [[String : Any]] as [[String : AnyObject]]
                if dataGeneral.count < 1 {
             
                }else{
                    continueBtn.isHidden = false
                    checkOutBtn.isHidden = false
                }
            }
        }
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
     

    }
    
        override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataGeneral.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell

        if let title = dataGeneral[indexPath.row]["ClassName"] as? String{
            cell.companyLbl.text = title
        }
        
        if let date = dataGeneral[indexPath.row]["ClassStartDate"] as? String{
            
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                cell.dateLbl.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
            }
          
        }
        if let days = dataGeneral[indexPath.row]["ClassDuration"] as? String{
            
            cell.daysLbl.text = "\(days)"
        }
        if let price = dataGeneral[indexPath.row]["Price"] as? Double{
           
            //            membership
            
            if currentUserLogin == 3{
                
                //bolding left/right
                if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                    cell.timeLbl.text =   "\(price)" + currencySign

                }else{
                    cell.timeLbl.text = currencySign + "\(price)"
                }
                cell.discountedPrice.isHidden = true
                
                
            }else{
                
                
                if isNewUrl{
                    
                    if let price2 = dataGeneral[indexPath.row]["DiscPrice"] as? Double{
                        
                        if price == price2 || price2 == 0 {
                            
                            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                                cell.timeLbl.text =  "\(price)" + currencySign

                            }else{
                                cell.timeLbl.text = currencySign + "\(price)"
                            }
                            cell.discountedPrice.isHidden = true
                            
                            
                        }else{
                            
                            //bolding left/right
                            var priceStr = ""
                            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                                priceStr =  "\(price) \(currencySign)"

                            }else{
                                priceStr = "\(currencySign) \(price)"
                            }
                            
                            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:priceStr )
                            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                            
                            cell.timeLbl.attributedText =   attributeString
                            cell.timeLbl.textColor = UIColor.red
                            //                    cell.timeLbl.text = currencySign + "\(price)"
                            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                                cell.discountedPrice.text =   "\(price2)" + currencySign

                            }else{
                                cell.discountedPrice.text = currencySign + "\(price2)"
                            }
                            cell.discountedPrice.isHidden = false
                        }
                        
                    }
                }else{
                    if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                        cell.timeLbl.text =   "\(price)" + currencySign

                    }else{
                        cell.timeLbl.text = currencySign + "\(price)"
                    }
                    cell.discountedPrice.isHidden = true
                    
                }
                
                
            }

        }
        
        if let url = dataGeneral[indexPath.row]["ImageUrl"] as? String{
            var originalUrl = url
            originalUrl = imageBaseUrl + originalUrl
            cell.studentImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
                
                DispatchQueue.main.async (execute: {

                    if let _ = image{
                        cell.studentImage.image = image;
                    }
                    else{
                        cell.studentImage.image = UIImage(named:"photoNot")
                    }
                });
                
            })
            
        }
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action:#selector(deleteBtnAction(sender:)) , for:.touchUpInside)
   
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    //MARK:- Buttons Actions
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
        
        if currentUserLogin == 4{
            _ = self.navigationController?.popToRootViewController(animated: true)
        }else{
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }

    @IBAction func checkOutBtnAction(_ sender: UIButton) {
        
        
        if dataGeneral.count < 1{
            DataManager.sharedInstance.printAlertMessage(message: "There is no class in cart.", view: self)
            return
        }
        
        if isNewUrl{
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
            controller.previousData = self.previousData
            controller.forClassBooking = true
            controller.secondLastControllerData = secondLastControllerData
            if isRedeemedVoucher {
                controller.isRedeemedVoucher = true
                controller.redeemedData = redeemedData
            }
            if fromRecommnded {
                controller.fromRecommnded = true
            }
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
            controller.previousData = self.previousData
            controller.forClassBooking = true
            controller.secondLastControllerData = secondLastControllerData
            if isRedeemedVoucher {
                controller.isRedeemedVoucher = true
                controller.redeemedData = redeemedData
            }
            if fromRecommnded {
                controller.fromRecommnded = true
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }


    }
    
    @objc func deleteBtnAction(sender:UIButton)    {
        
        let dic = dataGeneral[sender.tag]
        var currentClassId  = 0
        var programType = ""
        if let programTyp =  dataGeneral[sender.tag]["LessonType"] as? String {
            programType = programTyp
        }
        var keyToRemoveFromCart = ""
        
        if programType == "C" {
            if let idOFClass = dic["LessonId"] as? Int{
                currentClassId = idOFClass
            }
            keyToRemoveFromCart = "LessonId"
        }else{
        
            if let idOFClass = dic["ClassId"] as? Int{
            currentClassId = idOFClass
            }
            keyToRemoveFromCart = "ClassId"
        }
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        var cartKey = ""
        if currentUserLogin == 4 {
            cartKey = "studentCart" +  userId
        }else{
            cartKey = "myCart" + userId
        }
        self.deletingCartItem(keyName: cartKey, sender: sender, currentClassId:currentClassId,KeyToRemoveFromCart:keyToRemoveFromCart)
 

      
    }
    
    func deletingCartItem(keyName:String,sender:UIButton,currentClassId:Int,KeyToRemoveFromCart:String) {
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: keyName){
            var cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: keyName) as! Data)) as? [[String:AnyObject]])!
            for item in cart {
                
                if let idOFClass = item[KeyToRemoveFromCart] as? Int{
                    if idOFClass == currentClassId {
                        cart.remove(at: sender.tag)
                        let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
                        UserDefaults.standard.set(archiveddata, forKey: keyName)
                        dataGeneral = cart
                        if dataGeneral.count < 1 {
                            
                        }else{
                            continueBtn.isHidden = false
                            checkOutBtn.isHidden = false
                        }
                        self.tableView.reloadData()
                        return
                    }
                }
            }
        }

    }
    
    @IBAction func rightMenuBtnAction(_ sender: UIBarButtonItem) {
    
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
   

}

