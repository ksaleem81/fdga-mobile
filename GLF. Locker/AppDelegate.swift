//
//  AppDelegate.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleMaps
import UserNotifications
//import Fabric
//import Crashlytics
import Stripe
//import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    
    var window: UIWindow?
    var deviceTokenInApp = ""
    var keepMeLoginOn = false
    var synchingInProgress = false
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       UIApplication.shared.isStatusBarHidden = false
       currentTarget = DataManager.sharedInstance.getTargetName()
//       registerForRemoteNotification()
        
        if #available(iOS 10.0, *) {
                   let center  = UNUserNotificationCenter.current()
                   center.delegate = self
                   center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                       if error == nil {
                           DispatchQueue.main.async(execute: {

                               UIApplication.shared.registerForRemoteNotifications()})
                       }
                   }
               }
               else{

                  UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                       UIApplication.shared.registerForRemoteNotifications()
               }
        
//        FirebaseApp.configure()
//       Fabric.with([Crashlytics.self])
       assigningKeysToTargetSelected()
//     GlobalClass.sharedInstance().shouldAllowLandscape = false
       IQKeyboardManager.shared.enable = true
       applyCustomeNavigation()
       if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "keepLogin"){
           if DataManager.sharedInstance.readUserDefaults(key: "keepLogin") == "True" {
                keepMeLoginOn = true
                showDashBoard()
                return true
            }
        }
        
        if currentTarget == "FDGS"{
        }else{
            showLoginController(launchingFirstTime:true)
        }
        
        return true
    }

    func assigningKeysToTargetSelected()  {
        
         if currentTarget == "FDGS" {
            kiOSCalendarName = "FDGS Golf Calendar"
            calenderIdentifier = "EventTrackerPrimaryCalendarFDGS"
            GMSServices.provideAPIKey("AIzaSyA2T2okXS7eusUm7FbDYh5SqpylsK-u0P4")

         }
         else{
            GMSServices.provideAPIKey("AIzaSyA2T2okXS7eusUm7FbDYh5SqpylsK-u0P4")
            kiOSCalendarName = "GLF. Locker Calendar"
            calenderIdentifier = "EventTrackerPrimaryCalendarGLC"
        }
        refreshingData()
        
    }
    
    //call when you update appstore new version and switch account users data keys changed from server
   func refreshingData(){
        
        let defaults = UserDefaults.standard
        let currentAppVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let previousVersion = defaults.string(forKey: "appVersion")
        if previousVersion == nil {
            // first launch
            DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"False")
            DataManager.sharedInstance.deletingAllAccounts(cartKey: "allAccounts")
            defaults.set(currentAppVersion, forKey: "appVersion")
            defaults.synchronize()
        } else if previousVersion == currentAppVersion {
            // same version
        } else {
            // other version
            DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"False")
            DataManager.sharedInstance.deletingAllAccounts(cartKey: "allAccounts")
            defaults.set(currentAppVersion, forKey: "appVersion")
            defaults.synchronize()
        }
    }
    
    func applyCustomeNavigation()  {
        
        UIApplication.shared.statusBarStyle = .lightContent
        let backArrowImage = UIImage(named: "header_arrow")
        let renderedImage = backArrowImage?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = renderedImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = renderedImage
         let navigationBarAppearnce = UINavigationBar.appearance()
        navigationBarAppearnce.barTintColor = UIColor(red: 121/255, green: 121/255, blue: 120/255, alpha: 1.0)
        navigationBarAppearnce.tintColor = UIColor.white
        let titleDict: NSDictionary = [NSAttributedStringKey.font:
            UIFont(name: "Helvetica-Bold", size: 16.0)!,
                                       NSAttributedStringKey.foregroundColor: UIColor.white] as! [NSAttributedStringKey: Any] as NSDictionary as NSDictionary
        UINavigationBar.appearance().titleTextAttributes = titleDict as? [NSAttributedStringKey: Any]
        
//        print(titleDict)
//        for family in UIFont.familyNames {
//            print("\(family)")
//            for name in UIFont.fontNames(forFamilyName: family) {
//                print("   \(name)")
//            }
//        }
        
    }
    
    func showDashBoard() {
        
        let storyBoard = UIStoryboard(name:"Main" , bundle: nil)
        let tabbar = storyBoard.instantiateViewController(withIdentifier: "Dashboard")
        self.window?.rootViewController = tabbar
        let transition = UIViewAnimationOptions.transitionFlipFromRight
        UIView.transition(with: window!, duration: 0.5, options: transition, animations: {
            self.window?.makeKeyAndVisible()
                GlobalClass.sharedInstance().window = self.window
                    }, completion: nil)
    }
    
    func athorizeError()  {
        
        let alertController = UIAlertController(title: "Alert", message:"You will be automatically logged out from this device as you have started a new session on another device.", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.showInitialController()
        }
        alertController.addAction(okAction)
        UIApplication.getTopestViewController()?.present(alertController, animated: true, completion: nil)
        
    }

    func showInitialController()  {
        
        DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"False")
        DataManager.sharedInstance.updateLoginDetails(nil)
        
        if currentTarget == "GLF. Locker" || currentTarget == "Playgolf" || currentTarget == "TPC Group" || currentTarget == "ClubCorp Locker" || currentTarget == "Kemper Sports" || currentTarget == "FDGS"{
        }else{
            showLoginController(launchingFirstTime: false)
            return
        }
        
        DataManager.sharedInstance.updateAcademyDetails(nil)
        let storyBoard = UIStoryboard(name:"Main" , bundle: nil)
        let login = storyBoard.instantiateViewController(withIdentifier: "InitialController")
        self.window?.rootViewController = login
                let transition = UIViewAnimationOptions.transitionFlipFromLeft
        UIView.transition(with: window!, duration: 0.5, options: transition, animations: {
            self.window?.makeKeyAndVisible()
            GlobalClass.sharedInstance().window = self.window
        }, completion: nil)
        
    }
    
    func showLoginController(launchingFirstTime:Bool) {
        
        let storyBoard = UIStoryboard(name:"Main" , bundle: nil)
        let login = storyBoard.instantiateViewController(withIdentifier: "AcademyViewController") as! AcademyViewController
        
        if currentTarget == "Chris Ryan Golf" {
            login.acedmyId = "1188"
        }else if currentTarget == "Playgolf" {
            login.acedmyId = "1190"
        }
        
        if launchingFirstTime{
            login.needToReloadOrNot = true
            let navigationController = UINavigationController(rootViewController: login)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            GlobalClass.sharedInstance().window = self.window
        }else{
            login.needToReloadOrNot = true
            let navigationController = UINavigationController(rootViewController: login)
            self.window?.rootViewController = navigationController
            let transition = UIViewAnimationOptions.transitionFlipFromLeft
            UIView.transition(with: window!, duration: 0.5, options: transition, animations: {
                self.window?.makeKeyAndVisible()
                GlobalClass.sharedInstance().window = self.window
            }, completion: nil)
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        print(url)
//        if url.absoluteString.hasPrefix("butlercustomer://stripe-redirect") {
//            NotificationCenter.default.post(name: .getStripeSourceObject, object: "AliPay")
//            return true
//        }
//
//        if url.absoluteString.hasPrefix("netspay://payment/QRCode") {
//            NotificationCenter.default.post(name: .getNetsQRData, object: url)
//            return true
//        }

        let stripeHandled = Stripe.handleURLCallback(with: url)
        if (stripeHandled) {
            return true
        } else {
            // This was not a stripe url – do whatever url handling your app
            // normally does, if any.
        }
        return false
//            || ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

//        sessiondestroy
//        let currentTime = Date()
//        DataManager.sharedInstance.writeInUserDefaults(key: "appGoesIdle", value:"\(currentTime)")
        
    }

    
    func findIdleTimeForApp() -> Int {

        let start = Date()
         var enddtStr = ""
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "appGoesIdle"){
          enddtStr = DataManager.sharedInstance.readUserDefaults(key: "appGoesIdle")
        }else{
            
            return 0
        }
        print(start)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ssZ"
        let enddt = dateFormatter.date(from: enddtStr)!
//        let enddt = Date(timeIntervalSince1970: 100)
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([ .day])
//        let datecomponenets = calendar.dateComponents(unitFlags, from: enddt, to:start )
        let datecomponenets = calendar.dateComponents(unitFlags, from: start, to:enddt )
        let days = datecomponenets.day
//        print("Seconds: \(days)")
        return days!
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if  let data =  UserDefaults.standard.value(forKey: "loginDetails")  as? Data {
            let dic = NSKeyedUnarchiver.unarchiveObject(with:data) as? NSDictionary
            if dic != nil{
                if let userType = DataManager.sharedInstance.currentUser()?["UserRoleId"] as? Int{
                    
                    if userType == 4 {
                    }else{
                        
                        if self.synchingInProgress {
                        }else{
                            synchingInProgress = true
                            getEventsDataAndSynch()
                        }
                        
                    }
                    
                }
                
                //sessiondestroy
//                if findIdleTimeForApp() > 0 {
//                    self.showInitialController()
//                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if UIApplication.shared.applicationIconBadgeNumber > 0{
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        
        //forcepassword
       //implimented after force change password implimentation
   /*     if  let data =  UserDefaults.standard.value(forKey: "loginDetails")  as? Data {
            let dic = NSKeyedUnarchiver.unarchiveObject(with:data) as? NSDictionary
            if dic != nil{

                if let userType = DataManager.sharedInstance.currentUser()?["UserRoleId"] as? Int{
                    if userType == 4 {
               if let update = DataManager.sharedInstance.currentUser()!["IsPasswordReset"] as? Bool,update == true {
                 addBlurView()
               }else{
                getUserPasswordUpdatedOrNot()//for check status from server
               }
                
            }
            
        }

            }
            
        }*/
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        self.saveContext()
        if DataManager.sharedInstance.readUserDefaults(key: "keepLogin") == "True" {
        }else{
        DataManager.sharedInstance.updateLoginDetails(nil)
        DataManager.sharedInstance.updateAcademyDetails(nil)
        }
    }
 
    
   func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {

         if notificationSettings.types != [] {
            DispatchQueue.main.async(execute: {
                application.registerForRemoteNotifications()
                
            }
            )
        }
    }
   
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("backGround fetched")
    }
    
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        deviceTokenInApp = deviceTokenString
        print("deviceToken",deviceTokenString)
    }
   
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
   
    func registerForRemoteNotification() {
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    DispatchQueue.main.async(execute: {

                        UIApplication.shared.registerForRemoteNotifications()})
                }
            }
        }
        else{

           UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
    
    //forcepassword
   /* func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //forcepassword
        
        let state = application.applicationState
        print(userInfo)
        
     
        var message = "Class Or Lesson Booked"
        
        if state == .active {
            
            //GlobalClass.sharedInstance().parsingNotificationMessage(userInfo)
            if let app = userInfo["aps"] as? [String:AnyObject] {
                
                if let message1 = app["alert"] as? String {
                    message = message1
                }
            }
            
            if message == "Password Changed"{
                //forcepassword
//                if let userType = DataManager.sharedInstance.currentUser()?["UserRoleId"] as? Int{
//                    if userType == 4 {
//                        self.checkPasswordReset(userInfo: userInfo as! [String : AnyObject],state:state)
//
//                    }
//                return
            }
            
            let alertController = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.window?.topMostController()!.present(alertController, animated: true, completion: nil)
        }else{
            //forcepassword
            //          checkPasswordReset(userInfo: userInfo as! [String : AnyObject],state:state)
        }
    }*/
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        let state = application.applicationState
        print(userInfo)
    
        var message = "Class Or Lesson Booked"
        if state == .active {
            
            //GlobalClass.sharedInstance().parsingNotificationMessage(userInfo)
            if let app = userInfo["aps"] as? [String:AnyObject] {
                if let message1 = app["alert"] as? String {
                    message = message1
                }
            }
            
            if message == "Password Changed"{
                //forcepassword
//               checkPasswordReset(userInfo: userInfo as! [String : AnyObject],state:state)
                return
            }
        
        let alertController = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        UIApplication.getTopestViewController()!.present(alertController, animated: true, completion: nil)
        }else{
            //forcepassword
//          checkPasswordReset(userInfo: userInfo as! [String : AnyObject],state:state)
        }
    }
    
    func checkPasswordReset(userInfo:[String:AnyObject],state:UIApplicationState)  {
        
            if let app = userInfo["Event"] as? String {
                var currentUser = app.components(separatedBy: ":")
                   var userID = ""
                    if let userId1 = DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
                        userID = "\(userId1)"
                    }
                  let exctId = currentUser[1].substring(to: currentUser[1].index(before: currentUser[1].endIndex))
                
                   if exctId == userID {
                        let currentUserClone =  DataManager.sharedInstance.currentUser()?.mutableCopy() as? NSMutableDictionary
                        currentUserClone?.setValue(true, forKey: "IsPasswordReset")
                        DataManager.sharedInstance.updateLoginDetails(currentUserClone)
                        if state == .active || state == .inactive || state == .background  {
                           self.addBlurView()
                    }
                }
            }
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
    
        if (GlobalClass.sharedInstance().shouldAllowLandscape == true) {
            return UIInterfaceOrientationMask.all
        }
        return UIInterfaceOrientationMask.portrait
    }
    
    // MARK: - Core Data stack
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "GLF.Locker")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getUserPasswordUpdatedOrNot()  {
        
        var userID = ""
        if let userId1 =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId1)"
        }
        
        let params = ["Userid":userID]
        NetworkManager.performRequest(type:.post, method: "Login/IsPasswordReset", parameter:params as [String : AnyObject], view: UIApplication.getTopestViewController()?.view, onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                return
            // do one thing
            case _ as String:
                break
            case _ as [String:AnyObject]:
            DataManager.sharedInstance.printAlertMessage(message:"\(object)", view:UIApplication.getTopestViewController()!)
                return
            default:
                break
            }
            
            if  let flage = object as? Bool {
               
                if  flage == true {
                    let currentUserClone =  DataManager.sharedInstance.currentUser()?.mutableCopy() as? NSMutableDictionary
                    currentUserClone?.setValue(true, forKey: "IsPasswordReset")
                    DataManager.sharedInstance.updateLoginDetails(currentUserClone)
                    self.addBlurView()
                }
            }
            
        }) { (error) in
            print(error!)
            UIApplication.getTopestViewController()?.showInternetError(error: error!)
        }
    }

    func getEventsDataAndSynch()  {
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetCoachBookedSchedules/\(coachId)", parameter:nil, view: UIApplication.getTopestViewController()?.view, onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
                case _ as String:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                break
                case _ as [String:AnyObject]:
                self.synchingInProgress = false
                return
            case _ as [[String:AnyObject]]:
                self.synchingInProgress = false

            let eventsArray = object as! [[String:AnyObject]]
        DataManager.sharedInstance.synchScheduleInCalendar(eventsArray:eventsArray,view:UIView())
            default:
                break
            }
            
        }) { (error) in
            print(error!)
        }
        
    }
    
    func addBlurView()  {
        
     GlobalClass.sharedInstance().window = self.window
     blurView.frame = CGRect(x: 0, y: -50, width: self.window!.frame.size.width, height: self.window!.frame.size.height  + 50)
     presentTableViewAsPopOver()
        
    }
    
    func presentTableViewAsPopOver() {
     
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "ChangepasswordVC") as! ChangepasswordVC
        menuViewController.blureView = blurView
        menuViewController.blureView.tag = 98765
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: self.window!.frame.size.width-20, height: 440)
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .init(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.window! as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: self.window!.bounds.size.width/2,
                y: self.window!.bounds.origin.y+self.window!.bounds.size.height,
                width: 1,
                height: 1)
     
        if ((self.window?.rootViewController?.view.viewWithTag(98765)) != nil){
            return
        }else{
        }
        self.window!.rootViewController?.present(
            menuViewController,
            animated: true,
            completion: nil)
    }

    
}


/*
 func uncaughtExceptionHandler1 () {
 let uncaughtExceptionHandler : Void = NSSetUncaughtExceptionHandler { exception in
 NSLog("Name:" + exception.name.rawValue)
 if exception.reason == nil
 {
 NSLog("Reason: nil")
 }
 else
 {
 NSLog("Reason:" + exception.reason!)
 }
 }
 }*/

extension AppDelegate:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return    UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
}


extension UIApplication {
    
    class func getTopestViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopestViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopestViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopestViewController(base: presented)
        }
        return base
    }
}


