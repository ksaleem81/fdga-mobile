//
//  StudentDetailsViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/3/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
import JYRadarChart
//import Charts

class AppointMentViewController: UIViewController {

    @IBOutlet weak var imageContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userImage: UIImageView!
    var userImageData = UIImage()
    var garaphData = [[String:AnyObject]]()
    @IBOutlet weak var userGraphView: UIView!//PieChartView!
    @IBOutlet weak var graphImage: UIImageView!
    var previousData = [String:AnyObject]()
     var studentImageData = [String:AnyObject]()
    var allStudents = [[String:AnyObject]]()
    var studentTitle = ""
    @IBOutlet weak var userLbl: UILabel!
    @IBOutlet weak var lessonLbl: UILabel!
    @IBOutlet weak var academyLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var btnsViewHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var graphLbl: UILabel!
    var isComeFromEvents = false
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceLbl: UILabel!
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
    @IBOutlet weak var rescheduleBtnHeightCons: NSLayoutConstraint!
    @IBOutlet weak var resheduleTopLine: UILabel!
    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.graphImage.isHidden = true
        // Do any additional setup after loading the view.
        userImage.image = userImageData
        print(previousData)
        getAllStudents()
        if let lessonPrice1 = previousData["LessonPrice"] as? Double{
            let roundedString = String(format: "%.2f", lessonPrice1)
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                self.priceLbl.text = "Amount Paid" + ": " + "\(roundedString)" + currencySign
            }else{
                self.priceLbl.text = "Amount Paid" + ": "  + currencySign + "\(roundedString)"
            }
        }
        
        if let type = previousData["ProgramType"] as? String {
            self.containerViewHeightConstraint.constant = 690//640
            if type == "2" {
                
                if currentUserLogin == 4 {
                }else{
                    
                self.priceLbl.text = "Class Payments"
                }
                btnsViewHeighConstraint.constant = 55
                self.containerViewHeightConstraint.constant = 580//480
                self.graphLbl.isHidden = false
                self.userGraphView.isHidden = true
                self.userGraphView.superview?.backgroundColor = UIColor.init(red: 204/255, green: 213/255, blue: 35/255, alpha: 1.0)
                
            }else{
                
                self.rescheduleBtnHeightCons.constant = 0
                self.resheduleTopLine.isHidden = true
                //Student able to delete/ just comment below player condition
                if currentUserLogin == 4 {
                    btnsViewHeighConstraint.constant = 55
                    self.containerViewHeightConstraint.constant = 580//480
                }
                
                gettingServerData()
                getStudentsImage()

              }
        }

        self.fillingUserInfo()
        self.navigationItem.title = "STUDENT APPOINTMENT"
        imageContainerHeightConstraint.constant = self.view.frame.size.width / 2
        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
        settingRighMenuBtn()
    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AppointMentViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    @IBAction func priceBtnAction(_ sender: UIButton) {
        
        if currentUserLogin == 4 {
        }else{
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
                blurView.frame = self.view.bounds
                blurView.tag = 1010
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.curveEaseIn,
                                  animations: { self.view.addSubview(self.blurView)
                }, completion: nil)
                
                presentTableViewAsPopOver(sender: sender)
            }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }
        
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
            }else{

                gettingIndividualLessonDetail()
            }
            
        }
    }
    
    //MARK:- fetching server data
    func gettingServerData()  {
        
        var studentId = ""
        if let id = previousData["StudentID"] as? Int{
            studentId = "\(id)"
        }
        
        var academyId = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyId = "\(id)"
        }
       
        NetworkManager.performRequest(type:.get, method: "Academy/GetStudentSkillGraph/\(studentId)/\( academyId)", parameter:nil, view:nil, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
           
            default:
               DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
                // should never happen!
            }
            
             self.garaphData = (object as? [[String:AnyObject]])!
            self.userGraphView.isHidden = true
         // self.userGraphView =  DataManager.sharedInstance.countingCounterForSocialMedias(view: self.userGraphView, graphData: self.garaphData)
            let chart = JYRadarChart.init(frame: self.userGraphView.frame)
            self.userGraphView.superview?.addSubview(DataManager.sharedInstance.gameScoreGraph(chart: chart, data:self.garaphData,controller: self))
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

    }
    
    func fillingUserInfo()  {
        
        if (isComeFromEvents == false) {
            if let name = previousData["LessonName"] as? String {
                userLbl.text = name
                graphLbl.text = name
            }
            if let name = previousData["Notes"] as? String {
                if name == ""{
                     lessonLbl.text = "Lesson"
                }else{
                    lessonLbl.text = name
                }
            }
        }
        else {
            if let name = previousData["Name"] as? String {
                userLbl.text = name
                if let type = previousData["ProgramType"] as? String {
                    if type == "2" {
                        graphLbl.isHidden = false
                        graphLbl.text = name
                        if let name = previousData["Notes"] as? String, name != "" {
                            lessonLbl.text = name
                        } else {
                            lessonLbl.text = "Class"
                        }
                    }else{
                        graphLbl.isHidden = true
                        if let name = previousData["Type"] as? String, name != "" {
                            lessonLbl.text = name
                        } else {
                            lessonLbl.text = "Lesson"
                        }

                    }
                }

            }
            
           
            //studentImageData
            var lessonImage = ""
            if let image = previousData["LessonImage"] as? String{
                lessonImage = image
            }
            userImage.sd_setImage(with: URL.init(string: lessonImage), completed: { (img, error, _, _) in
                if error != nil {
                    self.userImage.image = UIImage.init(named: "photoNot.png")
                }
            })
        
        }
        
        if let name = DataManager.sharedInstance.currentAcademy()!["AcademyName"] as? String{
            academyLbl.text = name
        }
        if let name = previousData["StartTime"] as? String {
          
            //            localechanges
            if is12Houre == "true"{
                startTimeLbl.text = Utility.convertTimeInto12Formate(timeStr: name)
                
            }else{
                if (name.count) > 5 {
                    
                    startTimeLbl.text = String(name.dropLast(3))
                    print(String(name.dropLast(3)))
                }else{
                    startTimeLbl.text = name
                }
                
            }
        }
        if let name = previousData["EndTime"] as? String {
            //            localechanges
            if is12Houre == "true"{
                endTimeLbl.text = Utility.convertTimeInto12Formate(timeStr: name)
                
            }else{
                if (name.count) > 5 {
                    endTimeLbl.text = String(name.dropLast(3))
                }else{
                    endTimeLbl.text = name
                }
                
            }

        
        }
        
        if let date = previousData["LessonDate"] as? String {
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                startDateLbl.text =  DataManager.sharedInstance.getFormatedDateWithoutWeekDay(date:dateOrignal[0],formate:"yyyy-MM-dd")
            }
        }
        
        if let date = previousData["LessonDate"] as? String {
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                
                endDateLbl.text =  DataManager.sharedInstance.getFormatedDateWithoutWeekDay(date:dateOrignal[0],formate:"yyyy-MM-dd")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func scheduleBtnAction(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Resechedule", message: "Are you sure you want to reschedule?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Resechedule", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.resceheduleLessonByCoach()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    @IBAction func gotToLesonBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:
            "GoToClassTableViewController") as! GoToClassTableViewController
        var dic = [String:AnyObject]()
        for requiredStudent in allStudents {
            if let id = requiredStudent["StudentID"] as? Int{
                 if let id2 = studentImageData["PlayerId"] as? Int{
                if id == id2 {
                   dic = requiredStudent
                }
                
                }
            }
        }
        
        if let type = self.previousData["ProgramType"] as? String {
            if type == "2" {
                if currentUserLogin == 4{
                    if self.allStudents.count > 0{
                        dic = self.allStudents[0]
                    }
                }
            }
        }

        controller.forPlayerTypeIdData = dic
        controller.previousData =  previousData
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
             controller.isForClass = true
            }
        }
        if isComeFromEvents{
             controller.isComeFromEvents = true
        }
        controller.imageData = self.userImage.image!
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func moveBtnAction(_ sender: UIButton) {
        
        // Student able to delete/ just uncomment
//        if currentUserLogin == 4{
//            scheduleBtnAction(sender)
//            return
//        }
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:
            "MoveLessonViewController") as! MoveLessonViewController
        controller.garaphData = self.garaphData
        controller.previousData =  previousData
        controller.delegate = self
        controller.imageData = userImageData//self.userImage.image!
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    @IBAction func deleteBtnAction(_ sender: UIButton) {
        //https://app.glflocker.com/OrbisAppBeta/api/Academy/DeleteOnetoOneLesson/37791/73/5093
        let alertController = UIAlertController(title: "Alert", message: "Are You Sure You Want to Delete Lesson?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
           self.deleteLessonByCoach()
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
       
    }
    
    func getAllStudents()  {
        
        var studentId = ""
         if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            studentId = "\(id)"
        }
       
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
  
        NetworkManager.performRequest(type:.get, method: "Academy/GetAllStudents/\(academyID)/\(studentId)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
          
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
 
                return
              
            }
            
            self.allStudents = object as! [[String : AnyObject]]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func getStudentsImage()  {
        
        var studentId = ""
        if let id = previousData["StudentID"] as? Int{
            studentId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetStudentImages/\(studentId)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break;
            }
            
            self.studentImageData = object as! [String : AnyObject]
            
             if let fileUrl = self.studentImageData["ImagerUrl"] as? String {
             
                var originalUrl =
                    fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
             originalUrl = imageBaseUrl + originalUrl
             self.userImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options:.cacheMemoryOnly, completed: { (image, error, cacheType, url) in
             
             DispatchQueue.main.async (execute: {
             if let _ = image{
             self.userImage.image = image;
             }
             else{
             self.userImage.image = UIImage(named:"photoNot")
             
             }
             });
             
             })
             }
         
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func deleteLessonByCoach() {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        
        var cochid = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            cochid = "\(id)"
        }
        
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/DeleteOnetoOneLesson/\(lessonID)/\(academyID)/\(cochid)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
                
            }
            
            _ = self.navigationController?.popViewController(animated: true)
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

    }
    
    func resceheduleLessonByCoach() {
        
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        
        var studentId = ""
        if let id = studentImageData["UserId"] as? Int{
            studentId = "\(id)"
        }
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        
        print(["AcademyID":academyID as AnyObject,"LessonID":lessonID as AnyObject,"UserID":"\(studentId)" as AnyObject])
       
        NetworkManager.performRequest(type:.post, method: "Academy/ReScheduleLesson/", parameter:["AcademyID":academyID as AnyObject,"LessonID":lessonID as AnyObject,"UserID":"\(studentId)" as AnyObject], view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
           _ = self.navigationController?.popViewController(animated: true)
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    
    func gettingIndividualLessonDetail()  {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        //localechanges
        var parameters = ""
            parameters = "\(lessonID)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetIndividualLessonDetails/\(parameters)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
                // should never happen!
            }
            
            let individualInfoObjec = object as? [[String:AnyObject]]
            if (individualInfoObjec?.count)! > 0{
            
                if let status = individualInfoObjec![0]["PaidStatus"] as? String,status == "Not Paid"{
                    
                    self.rescheduleBtnHeightCons.constant = 0
                    self.resheduleTopLine.isHidden = true
                    // Student able to delete/ just uncomment
//                    if currentUserLogin == 4 {
//
//                    self.btnsViewHeighConstraint.constant = 55
//                    self.containerViewHeightConstraint.constant = 580//480
//                                       }
                    
                }else{
                    self.rescheduleBtnHeightCons.constant = 40
                    self.resheduleTopLine.isHidden = false
                    
                    // Student able to delete/ just uncomment
//                    if currentUserLogin == 4 {
//                        self.rescheduleBtnHeightCons.constant = 0
//                        self.resheduleTopLine.isHidden = false
//
//                        self.btnsViewHeighConstraint.constant = 55
//                        self.containerViewHeightConstraint.constant = 580//480
//                    }

                }
                
                //PaidStatus
                
            }
            
        }) { (error) in
            print(error!)
            // DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
            self.showInternetError(error: error!)
        }
        
    }

    
    func presentTableViewAsPopOver(sender:UIButton) {
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "AttendiesViewController") as! AttendiesViewController
        menuViewController.previousData =  previousData
        
//        if let id = forPlayerTypeIdData["UserID"] as? Int{
//            menuViewController.idToSend = "\(id)"
//        }
//        menuViewController.blureView = blurView
//        if let id = forPlayerTypeIdData["StudentName"] as? String{
//            menuViewController.NameToSend = "\(id)"
//        }
//        
//        menuViewController.isForLessonOrClass = true
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: self.view.frame.size.width-20, height: self.view.frame.size.height-150)//CGSize(width: view.frame.size.width-10, height: self.view.frame.size.height-20)
        
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .init(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.view as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: sender.bounds.size.width/2,
                y: self.view.frame.size.height/2,
                width: 1,
                height: 1)
        present(
            menuViewController,
            animated: true,
            completion: nil)
    }

}
//MARK:- showing popover delegate methods for class attendies
extension AppointMentViewController:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return 	UIModalPresentationStyle.none
    }
    internal func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
        self.blurView.removeFromSuperview()
    }
}
extension AppointMentViewController:updateMoveSchedule{
    
    func updatedObject(previousData: [String : AnyObject]) {
        self.previousData = previousData
        fillingUserInfo()
    }
}



      //https://app.glflocker.com/OrbisAppBeta/api/Academy/ReScheduleLesson/
//https://app.glflocker.com/OrbisAppBeta/api/Booking/MoveLesson/

//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetStudentImages/23674
//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetStudentSkillGraph/23674/73
//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetAllStudents/73/4163
