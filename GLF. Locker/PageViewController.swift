//
//  PageViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/20/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
class PageViewController: UIPageViewController{

   var currentIndex = 0
    var orderedViewControllers: [UIViewController] = [UIViewController]()
    private func newColoredViewController(color: String,tag:Int) -> UIViewController {
        
        
        if tag == 0 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: color) as! VideoViewController

            for index in dataArray {
                
                if let mediaTyp = index["MediaTypeId"] as? Int{
                    if mediaTyp == 1 {
                        controller.dataDictionary = index
                    }
                }
            }

            
                return controller
           
        }else if tag == 1{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: color) as! PhotoViewController

                    for index in dataArray {
                        
                        if let mediaTyp = index["MediaTypeId"] as? Int{
                            if mediaTyp == 2 {
                            controller.dataDictionary = index
                            }
                        }
                    }

            
                return controller
        }else {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: color) as! DrillViewController

            for index in dataArray {
                
                if let mediaTyp = index["MediaTypeId"] as? Int{
                    if mediaTyp == 3 {
                        controller.dataDictionary = index
                    }
                }
            }

              return controller
        }
        
 
       
    }

    var dataArray = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      dataSource = self
        
        // Do any additional setup after loading the view.
        
        getMediaInfo()
        
//      settingRighMenuBtn()
    }
    
    func settingLeftRightArrows() {
        let ima = UIButton.init(frame: CGRect(x: 5, y:135, width: 30, height: 30))
        ima.setImage(UIImage(named:"header_arrow"), for: .normal)
        ima.addTarget(self, action: #selector(PageViewController.previous(btn:)), for: .touchUpInside)
        ima.setBackgroundImage(UIImage(named:"blankcircle"), for: .normal)
        self.view.addSubview(ima)
        let ima2 = UIButton.init(frame: CGRect(x: self.view.frame.maxX-35, y: 135, width: 30, height: 30))
        
        ima2.addTarget(self, action: #selector(PageViewController.next(btn:)), for: .touchUpInside)
        ima2.setImage(UIImage(named:"video_arr"), for: .normal)
        ima2.setBackgroundImage(UIImage(named:"blankcircle"), for: .normal)
        self.view.addSubview(ima2)
    }
    
    @objc func next(btn:UIButton)  {
      currentIndex =   currentIndex + 1
        if currentIndex > orderedViewControllers.count-1 {
            currentIndex = 0
        }
        
        if currentIndex > orderedViewControllers.count {
          return
        }
        
     let arr =  [orderedViewControllers[currentIndex]]
        self.setViewControllers(arr,
                                              direction: UIPageViewControllerNavigationDirection.forward,
                                              animated: true,
                                              completion: nil)
        }
    @objc func previous(btn:UIButton)  {
        
        print(currentIndex)
        currentIndex =   currentIndex - 1
        if currentIndex < 0 {
            currentIndex = orderedViewControllers.count - 1
        }
        
        if currentIndex > orderedViewControllers.count {
            return
        }

        
        let arr =  [orderedViewControllers[currentIndex]]
        self.setViewControllers(arr,
                                direction: UIPageViewControllerNavigationDirection.reverse,
                                animated: true,
                                completion: nil)
    }
    func settingRighMenuBtn()  {

        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PageViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    func initializePager() {
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    
    
    func getMediaInfo()  {
        
        var playerId = ""
        if let id = DataManager.sharedInstance.currentUser()?["Userid"] as? Int{
            playerId = "\(id)"
        }
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
//        (UIApplication.getTopestViewController()!.view!)
// self.appDelegate.window
        NetworkManager.performRequest(type:.get, method: "Academy/GetUserRecentMedia/\(playerId)/\(acadmyId)", parameter:nil, view:self.appDelegate.window, onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                break
            }
//            self.settingRighMenuBtn()
            self.settingLeftRightArrows()
            self.dataArray = object as! [[String:AnyObject]]
           
            self.orderedViewControllers = {
                return [self.newColoredViewController(color: "VideoViewController",tag:0),
                        self.newColoredViewController(color: "PhotoViewController",tag:1),
                        self.newColoredViewController(color: "DrillViewController",tag:2)]
            }()
            print(self.orderedViewControllers.count)
            self.initializePager()
            
            //view is visible or not
            if self.isViewLoaded && (self.view.window != nil) {
                let controller =   self.parent as! MediaViewController
                controller.settingRighMenuBtn()
                
            }
            
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
  
    }
    

}

extension PageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        self.currentIndex = previousIndex
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        self.currentIndex = nextIndex
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = orderedViewControllers.index(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
   
}

    

    

