//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu


class PaymentStatusViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var isForCoachOverView = false
    var selectedItems = [[String:AnyObject]]()
    var dataArray = [[String:AnyObject]]()
    var selectedDataArray = [[String:AnyObject]]()
    var eventId =  ""
    var individualInfo  = [String:AnyObject]()
    var previousData = [String:AnyObject]()
    var forPlayerTypeIdData  = [String:AnyObject]()
    var paymentsMethodsArray = [[String:AnyObject]]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(individualInfo)
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
                isForCoachOverView = true
                getClassAttendees()
             
                self.tableView.backgroundColor = UIColor.init(red:230/255, green: 230/255, blue: 230/255, alpha: 1.0)
            }else{
                self.tableView.separatorColor = UIColor.clear
            }
        }
       getPaymentMethods()
       settingRighMenuBtn()
       tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }

 
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PaymentStatusViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
 
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
 
   
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForCoachOverView{
            return self.dataArray.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if isForCoachOverView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentStatusForCoachTableViewCell", for: indexPath) as! PaymentStatusForCoachTableViewCell
            if let title = dataArray[indexPath.row]["PlayerName"] as? String{
                cell.nameLbl.text = title
            }
            if let title = dataArray[indexPath.row]["PlayerEmail"] as? String{
                cell.emailLbl.text = title
            }
            
            if let title = dataArray[indexPath.row]["PaymentType"] as? String{
                cell.typeLbl.text = title
            }
            cell.backgroundColor = UIColor.init(red:230/255, green: 230/255, blue: 230/255, alpha: 1.0)
            if let title = dataArray[indexPath.row]["IsPaid"] as? Bool{
                if title {
                    cell.radioBtn.isSelected = true
                }else  {
                    cell.radioBtn.isSelected = false
                }
            }
            
            if let id = dataArray[indexPath.row]["PaymentTypeId"] as? Int{
                if id == 3{
                    cell.radioBtn.isUserInteractionEnabled = true
                }else{
                    cell.radioBtn.isUserInteractionEnabled = false
                      cell.radioBtn.isSelected = true
                }
                
            }
            cell.radioBtn.tag = indexPath.row
            cell.radioBtn.addTarget(self, action: #selector(PaymentStatusViewController.checkBtnAction(btn:)), for: .touchUpInside)
            return cell
            
        }else{
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentStatusTableViewCell", for: indexPath) as! PaymentStatusTableViewCell
      
            if let title = individualInfo["PaymentType"] as? String{
           cell.pamentTypeLbl.text = title
        }
            if let title = individualInfo["PaidStatus"] as? String{
                cell.pamentStatusLbl.text = title
                if title == "Paid" {
                      cell.markBtn.setTitle("MARK AS UNPAID", for: .normal)
                }else{
                     cell.markBtn.setTitle("MARK AS PAID", for: .normal)
                }
            }
            
            if let type = individualInfo["PaymentType"] as? String{
                if type == "Cash"{
                    cell.markBtn.isHidden = false
                }else{
                    cell.markBtn.isHidden = true
                }
            }
            
            
            
            
            cell.markBtn.tag = indexPath.row
            cell.markBtn.addTarget(self, action:#selector(PaymentStatusViewController.markBtnAction(btn:)), for: .touchUpInside)
            
          
    
           return cell
            
        }
   
     return UITableViewCell()
        
    }
    
    // MARK:- cell buttons actions
    @objc func checkBtnAction(btn:UIButton)  {

        
        if btn.isSelected {
            markAsUnpaid(tag: btn.tag)
        }else{
            print("false")
            self.presentTableViewAsPopOver(sender: btn)
            //    self.markAsPaidOrNot(status: true, lessonID: lessonId, statusFlag: "1")
        }
    }

    @objc func markBtnAction(btn:UIButton)  {
        
        if btn.titleLabel?.text == "MARK AS UNPAID" {
            markAsUnpaid(tag: 0)
        }else{
        
            presentTableViewAsPopOver(sender: btn)
        }
    }
    
    func presentTableViewAsPopOver(sender:UIButton) {
        
        let menuViewController: PaymentPopUpViewController = self.storyboard!.instantiateViewController(withIdentifier: "PaymentPopUpViewController") as! PaymentPopUpViewController
  
        if isForCoachOverView{
            if let event = dataArray[sender.tag]["TransactionId"] as? Int {
                menuViewController.eventId = "\(event)"
            }
        }else{
        if let event = individualInfo["EvntId"] as? Int {
            menuViewController.eventId = "\(event)"
            }
        }
        
        menuViewController.paymentMethdsArray = paymentsMethodsArray
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: view.frame.size.width-60, height: 220)
       menuViewController.delegateForBack = self
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .any
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = sender.superview! as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: sender.bounds.size.width/2,
                y: sender.bounds.origin.y+sender.bounds.size.height,
                width: 1,
                height: 1)
        present(
            menuViewController,
            animated: true,
            completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isForCoachOverView{
            return 90
        }else{
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
   func getClassAttendees()  {
    
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
    
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassAttendees/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
//                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                 break
            }
            
            self.dataArray = (object as? [[String:AnyObject]])!
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    
    }
    
    func getPaymentMethods()  {
        
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetAcademySubPayments/\(academyID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
            default:
                
                break
                
            }
            
            self.paymentsMethodsArray = (object as? [[String:AnyObject]])!
    
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    func markAsUnpaid(tag:Int)  {
        
        var paymentType = ""
        if isForCoachOverView {
            if let typ = dataArray[tag]["SubPaymentType"] as? Int {
                paymentType = "\(typ)"
            }
            if let typ = dataArray[tag]["TransactionId"] as? Int {
                eventId = "\(typ)"
            }
        }else{
        
            if let typ = individualInfo["SubPaymentType"] as? String {
            paymentType = typ
        }
        if let event = individualInfo["EvntId"] as? Int {
            eventId = "\(event)"
            }
        }
        NetworkManager.performRequest(type:.get, method: "Academy/UpdatePaidStatus/\(eventId)/\("False")/\(paymentType)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            if  let flage = object as? Bool {
                if flage{
                    if let type = self.previousData["ProgramType"] as? String {
                        if type == "2" {
                         self.getClassAttendees()
                        }else{
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                    }
                   
                    
            }else{
                  DataManager.sharedInstance.printAlertMessage(message:"SomeThing Went Wrong!", view:self)
                }
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
}

extension PaymentStatusViewController:UIPopoverPresentationControllerDelegate,PopOverDismissed{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return 	UIModalPresentationStyle.none
    }
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
        
    }
    
    func popoverDismissedOrNot(){
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
         
            getClassAttendees()
            }else{
            _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
}

