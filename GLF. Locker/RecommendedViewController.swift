//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
import ActionSheetPicker_3_0
class RecommendedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var dataComingLessons:[[String:AnyObject]] = [[String:AnyObject]]()
    var dataPreviosLessons:[[String:AnyObject]] = [[String:AnyObject]]()
    var dataGeneral:[[String:AnyObject]] = [[String:AnyObject]]()
    var upComing = false
    var studentData = [String:AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    var fromGoal = false
    var fromVideoQuality = false
  
    @IBOutlet weak var shortViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var goalViewHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var longViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var longTermLbl: UILabel!
    @IBOutlet weak var shortTermLbl: UILabel!
    @IBOutlet weak var videoQualityBtn: UIButton!
    @IBOutlet weak var videoQualityHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        if fromGoal {
            self.navigationItem.title = "MY GOALS"
            self.tableView.isHidden = true
             self.goalViewHeighConstraint.constant = 100
              videoQualityHeightConstraint.constant = 0
            getPlayerGoals()
            return
        }else if fromVideoQuality{
            videoQualityHeightConstraint.constant = 80
            self.navigationItem.title = "VIDEO SETTINGS"
            self.tableView.isHidden = true
            self.goalViewHeighConstraint.constant = 0
            self.videoQualityBtn.setTitle(DataManager.sharedInstance.readUserDefaults(key: kOrbisVideoQualityKey), for: .normal)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        else{
            self.goalViewHeighConstraint.constant = 0
            videoQualityHeightConstraint.constant = 0
            getPlayerRecommendedLessons()
        }
         settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(RecommendedViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
     

    }
    
        override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    
    func getPlayerGoals()  {

        
        var studentId = ""
        if let id = studentData["Userid"] as? Int{
            studentId = "\(id)"
        }

        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerGoals/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
          
            default:
                break
            }
            
            let dictionary = object as! [String:AnyObject]
            
        self.assigningHeightAndTextToViews(dic: dictionary)

            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func assigningHeightAndTextToViews(dic:[String:AnyObject])  {
        if let long = dic["LongTermGoal"] as? String{
          longViewHeightConstraint.constant = 55 +  DataManager.sharedInstance.heightForView(text: long, font: UIFont.systemFont(ofSize: 12.0), width:view.frame.size.width - 20 )
            self.longTermLbl.text = long
        }
        if let short = dic["ShortTermGoal"] as? String{
           shortViewHeightConstraint.constant = 55 +   DataManager.sharedInstance.heightForView(text: short, font: UIFont.systemFont(ofSize: 12.0), width:view.frame.size.width - 20 )
            self.shortTermLbl.text = short
        }
        goalViewHeighConstraint.constant = longViewHeightConstraint.constant + shortViewHeightConstraint.constant
    }
    
    
    func getPlayerRecommendedLessons()  {
       
        var studentId = ""
        if let id = studentData["Userid"] as? Int{
            studentId = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerRecommendedLessons/\(studentId)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
          break
            }
            self.dataGeneral = object as! [[String : AnyObject]]
            self.tableView.reloadData()
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataGeneral.count//self.dataComingLessons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecommendedTableViewCell", for: indexPath) as! RecommendedTableViewCell
    
        if let title = dataGeneral[indexPath.row]["LessonName"] as? String{
            cell.lessonTitleLbl.text = title
        }
        
        if let date = dataGeneral[indexPath.row]["LessonDate"] as? String{
                cell.dateLbl.text = date
        }
        
        if let date = dataGeneral[indexPath.row]["CoachesNames"] as? String{
            cell.coachLbl.text = date
        }
    
        if let price = dataGeneral[indexPath.row]["Price"] as?
        Int{
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                cell.priceLbl.text = "\(price) \(currencySign)"

            }else{
                cell.priceLbl.text = "\(currencySign) \(price)"
            }
        }
        
        if let duration = dataGeneral[indexPath.row]["Duration"] as? String{
                cell.timeLbl.text =   "\(duration)"
        }
        
        if let url = dataGeneral[indexPath.row]["LessonImage"] as? String{
            var originalUrl = url
            originalUrl = imageBaseUrl + originalUrl
            cell.lessonImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
                
                DispatchQueue.main.async (execute: {
                    //                    self.activityIndicator.stopAnimating();
                    if let _ = image{
                        
                        cell.lessonImage.image = image;
                    }
                    else{
                        cell.lessonImage.image = UIImage(named:"photoNot")
                    }
                });
                
            })
            
        }
        cell.bookBtn.tag = indexPath.row
        cell.bookBtn.addTarget(self, action: #selector(RecommendedViewController.bookNowBtnAction(btn:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func bookNowBtnAction(btn:UIButton)  {
        
        if let programTyp =  dataGeneral[btn.tag]["LessonType"] as? String {
            
            if programTyp == "C" {
                if let classId =    dataGeneral[btn.tag]["LessonId"] as? Int{
                self.getClassInfo(classId: "\(classId)")
                    return
                }
            }else{
                
            }
        }
        
        var playerId = ""
        if let id = studentData["playerId"] as? Int{
            playerId = "\(id)"
        }
        var startTime = ""
        if let id = dataGeneral[btn.tag]["StartTime"] as? String{
            startTime = "\(id)"
        }
        var endTime = ""
        if let id = dataGeneral[btn.tag]["EndTime"] as? String{
            endTime = "\(id)"
        }

        var coachId = ""
        if let id = dataGeneral[btn.tag]["CoachId"] as? Int{
            coachId = "\(id)"
        }
        var dateLesson = ""
        if let id = dataGeneral[btn.tag]["LessonDate"] as? String{
            
            dateLesson = DataManager.sharedInstance.getFormatedDateForJson(date:  "\(id)", formate: globalDateFormate)
        }
        
        NetworkManager.performRequest(type:.post, method:"Booking/CheckLessonRecommendStatus",parameter:["CoachId":coachId as AnyObject,"LessonDate":dateLesson as AnyObject,"StartTime":"\(startTime)" as AnyObject,"EndTime":"\(endTime)" as AnyObject,"PlayerId":playerId as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
            
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as String:
            if object as! String == "1"{
                
                if isNewUrl{
                    
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                    controller.selectedData = self.dataGeneral[btn.tag]
                    controller.fromRecommnded = true
                    self.navigationController?.pushViewController(controller, animated: true)
                }else{
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
                    controller.selectedData = self.dataGeneral[btn.tag]
                    controller.fromRecommnded = true
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
                
            }else if object as! String == "0" {
                DataManager.sharedInstance.printAlertMessage(message:"The lesson is already booked", view:self)
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                }
                break
                
            default:
               break
            }
           
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func moveToNext(dic:[String:AnyObject])  {
        

        var dicToAdd = dic
        var currentClassId  = 0
        
        var cart = [[String:AnyObject]]()
        
        if let idOFClass = dicToAdd["ClassId"] as? Int{
            currentClassId = idOFClass
        }
        
        var itemAdded = false
        var cartKey = ""
        //appending user id to make unique every cart
        
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        if currentUserLogin == 4 {
            cartKey = "studentCart" +  userId
            
            
        }else{
            cartKey = "myCart" + userId
        }
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: cartKey){
            cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: cartKey) as! Data)) as? [[String:AnyObject]])!
            for item in cart {
                if let idOFClass = item["ClassId"] as? Int{
                    if idOFClass == currentClassId {
                        itemAdded = true
                    }
                }
            }
        }
        if itemAdded {
        }else {
 
            var dic2 = [String:AnyObject]()
            if let idOFprogram = dic["ProgramTypeId"] as? Int{
                
                dic2["ProgramTypeId"] = idOFprogram as AnyObject
                dicToAdd["previousData"] = dic2 as AnyObject?

            }
            
            print(dicToAdd)
            cart.append(dicToAdd)
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: cartKey)
        }
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        controller.previousData = dic
        controller.fromRecommnded = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
    //MARK:- Buttons Actions
  
    func getClassInfo(classId : String)  {
        
        var membershipID = ""
        //mergingcode
        var urlStr = ""
        if isNewUrl{
            if let userId = DataManager.sharedInstance.currentUser()?["MembershipId"] as? Int{
                membershipID = "\(userId)"
            }
            urlStr = "Academy/GetClassByFilter_Native/\(classId)/\(membershipID)"
        }else{
            urlStr = "Academy/GetClassByFilter_Native/\(classId)/"
        }

        NetworkManager.performRequest(type:.get, method: "\(urlStr)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)

                return
            default:
                break
            }
            let responce = object as? [[String:AnyObject]]
            self.moveToNext(dic:(responce?[0])!)
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    @IBAction func videoBtnAction(_ sender: UIButton) {
        let dataAray = ["Low","Medium","High"];
    
        ActionSheetStringPicker.show(withTitle: "VIDEO QUALITY", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.videoQualityBtn.setTitle("\(indexes!)", for:.normal)
            if  "\(indexes!)" == "Low"
            {
            DataManager.sharedInstance.writeInUserDefaults(key: kOrbisVideoQualityKey, value:"low")
                
            }else if "\(indexes!)" == "Medium"
            {
            DataManager.sharedInstance.writeInUserDefaults(key: kOrbisVideoQualityKey, value:"medium")
                
            }else if  "\(indexes!)" == "High"
            {
            DataManager.sharedInstance.writeInUserDefaults(key: kOrbisVideoQualityKey, value:"high")
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }

}


