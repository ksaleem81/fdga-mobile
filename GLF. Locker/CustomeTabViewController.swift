//
//  CustomeTabViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/2/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu

class CustomeTabViewController: UITabBarController {

    var view1 = UIView()
    var buton1 = UIButton()
    var buton2 = UIButton()
    var buton3 = UIButton()
    var buton4 = UIButton()
    var label1Val = UILabel()
    var label2Val = UILabel()
    var label3Val = UILabel()
    var label4Val = UILabel()
    
    var viewAppearedOrNot = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    self.tabBar.isHidden = true
    addingCustomeViewToTabbar()
        
//        for num in 0...self.tabBar.items!.count {
//            self.selectedIndex = num
//        }
        buton2.isSelected = true
        settingRighMenuBtn()
         self.selectedIndex = 3
    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(CustomeTabViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    //making tabbar programmatically
    func addingCustomeViewToTabbar()  {
        
        var screenHeight = view.frame.size.height
        if Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() {
            
            screenHeight = screenHeight - 25
        }
        
        view1 =  UIView.init(frame: CGRect(x:0,y:screenHeight-70,width:view.frame.size.width , height:70))
        view1.tag = 999
         buton1 = UIButton.init(frame:CGRect(x:0, y:0,width:view.frame.size.width/4, height:70) )
        buton1.addTarget(self, action:#selector(tabbarBton1(button:)), for:.touchUpInside)
           //adding bottom label button 1
        let label1 = UILabel.init(frame: CGRect(x:0,y:view1.frame.size.height-25,width:view.frame.size.width/4 , height:25))
        label1.textAlignment = .center
        label1.font = UIFont(name: "Helvetica-Bold", size: 10)
        label1.text = "ADD STUDENT"
        label1.textColor = UIColor.white
        label1.bringSubview(toFront: view)
     
        view1.addSubview(buton1)
        view1.addSubview(label1)
        buton1.setImage(UIImage(named:"add_student"), for: .normal)
        buton1.setImage(UIImage(named:"add_student_selected"), for: .selected)//
        buton1.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton1.imageView?.contentMode = .scaleAspectFit
        buton1.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton1.layer.borderWidth = 0.5
      
        buton2.imageView?.contentMode = .scaleAspectFit
         buton2 = UIButton.init(frame:CGRect(x:view.frame.size.width/4*1, y:0, width:view.frame.size.width/4,height: 70) )
        buton2.setImage(UIImage(named:"while_circle"), for: .normal)//while_circle_ac
        buton2.setImage(UIImage(named:"while_circle_ac"), for: .selected)
        buton2.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton2.imageView?.contentMode = .scaleAspectFit
        buton2.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton2.layer.borderWidth = 0.5
        //adding bottom label button 2
        let label2 = UILabel.init(frame: CGRect(x:view.frame.size.width/4*1,y:view1.frame.size.height-25,width:view.frame.size.width/4 , height:25))
        label2.textAlignment = .center
        label2.font = UIFont(name: "Helvetica-Bold", size: 10)
        label2.text = "TOTAL STUDENTS"
        label2.numberOfLines = 0
        label2.textColor = UIColor.white
        label2.bringSubview(toFront: view)
        view1.addSubview(buton2)
        view1.addSubview(label2)
        
        label2Val = UILabel.init(frame: CGRect(x:view.frame.size.width/4*1,y:13,width:view.frame.size.width/4 , height:25))
        label2Val.tag = 222
        label2Val.textAlignment = .center
        label2Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label2Val.text = "0"
        label2Val.textColor = UIColor.black
        label2Val.bringSubview(toFront: view)
        view1.addSubview(label2Val)
        
         buton3 = UIButton.init(frame:CGRect(x:view.frame.size.width/4*2,y: 0, width:view.frame.size.width/4, height:70) )
        buton3.setImage(UIImage(named:"while_circle"), for: .normal)
        buton3.setImage(UIImage(named:"while_circle_ac"), for: .selected)
        buton3.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton3.imageView?.contentMode = .scaleAspectFit
        buton3.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton3.layer.borderWidth = 0.5
        //adding bottom label button 3
        let label3 = UILabel.init(frame: CGRect(x:view.frame.size.width/4*2,y:view1.frame.size.height-25,width:view.frame.size.width/4 , height:25))
        label3.textAlignment = .center
        label3.font = UIFont(name: "Helvetica-Bold", size: 10)
        label3.text = "ONE TO ONE"
        label3.textColor = UIColor.white
        label3.bringSubview(toFront: view)
        view1.addSubview(buton3)
        view1.addSubview(label3)

        label3Val = UILabel.init(frame: CGRect(x:view.frame.size.width/4*2,y:13,width:view.frame.size.width/4 , height:25))
        label3Val.tag = 333
        label3Val.textAlignment = .center
        label3Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label3Val.text = "0"
        label3Val.textColor = UIColor.black
        label3Val.bringSubview(toFront: view)
        view1.addSubview(label3Val)
        
        buton4 = UIButton.init(frame:CGRect(x:view.frame.size.width/4*3, y:0, width:view.frame.size.width/4, height:70) )
        buton4.setImage(UIImage(named:"while_circle"), for: .normal)
        buton4.setImage(UIImage(named:"while_circle_ac"), for: .selected)
        buton4.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        buton4.imageView?.contentMode = .scaleAspectFit
        buton4.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        buton4.layer.borderWidth = 0.5
   //adding bottom label button 4
        let label4 = UILabel.init(frame: CGRect(x:view.frame.size.width/4*3,y:view1.frame.size.height-25,width:view.frame.size.width/4 , height:25))
        label4.textAlignment = .center
        label4.font = UIFont(name: "Helvetica-Bold", size: 10)
        label4.text = "GROUPS"
        label4.textColor = UIColor.white
        label4.bringSubview(toFront: view)
        view1.addSubview(buton4)
        view1.addSubview(label4)
 
        label4Val = UILabel.init(frame: CGRect(x:view.frame.size.width/4*3,y:13,width:view.frame.size.width/4 , height:25))
        label4Val.tag = 444
        label4Val.textAlignment = .center
        label4Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label4Val.text = "0"
        label4Val.textColor = UIColor.black
        label4Val.bringSubview(toFront: view)
          view1.addSubview(label4Val)
        
        buton2.addTarget(self, action:#selector(tabbarBton2(button:)), for:.touchUpInside)
         buton3.addTarget(self, action:#selector(tabbarBton3(button:)), for:.touchUpInside)
         buton4.addTarget(self, action:#selector(tabbarBton4(button:)), for:.touchUpInside)
       
        view1.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        view1.layer.borderColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        view1.layer.borderWidth = 1.0
        self.view.addSubview(view1)

    }
    
    @objc func tabbarBton1(button:UIButton)  {
     //self.navigationController?.topViewController?.tabBarController?.selectedIndex = 0
       self.selectedIndex = 0
       
        
        if button.isSelected{
           // button.isSelected = false
        }else{
            button.isSelected = true
        }
        buton2.isSelected = false
        buton3.isSelected = false
        buton4.isSelected = false

    }
    
    @objc func tabbarBton2(button:UIButton)  {
      self.selectedIndex = 1
        
       if button.isSelected{
       // button.isSelected = false
        }else{
          button.isSelected = true
        }
        buton1.isSelected = false
        buton3.isSelected = false
        buton4.isSelected = false
    }
    
    @objc func tabbarBton3(button:UIButton)  {
       self.selectedIndex = 2
        if button.isSelected{
         //   button.isSelected = false
        }else{
            button.isSelected = true
        }
        buton1.isSelected = false
        buton2.isSelected = false
        buton4.isSelected = false
    }
    
    @objc func tabbarBton4(button:UIButton)  {
       self.selectedIndex = 3
        if button.isSelected{
          //  button.isSelected = false
        }else{
            button.isSelected = true
        }
        buton1.isSelected = false
        buton3.isSelected = false
        buton2.isSelected = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if viewAppearedOrNot {
        self.selectedIndex = 2
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if viewAppearedOrNot {
            self.selectedIndex = 3
            
        }
        
        viewAppearedOrNot = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
     
    }

}
