//
//  MessagesDetailViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 4/3/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class ComposeMessageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate{
    
    @IBOutlet weak var toField: UITextField!
    @IBOutlet weak var ccField: UITextField!
    @IBOutlet weak var subjectFld: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    var usersDataArray = [[String:AnyObject]]()
    var NameToSend = ""
    var idToSend = ""
     var blureView = UIView()
    private var autocompleteUrls = [String]()
    private var activeField: UITextField!
    private var searchString = ""
    
    var messageData: [String: AnyObject]?
    var isReply = false
    var isForward = false
    
    var isForLessonOrClass = false
    @IBOutlet weak var chooseContactBtn: UIButton!
    var forwardMessageData: [String: AnyObject]!
    @IBOutlet weak var crossBtn: UIButton!
    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currentUserLogin == 4 {
            self.chooseContactBtn.setTitle("Select Coach", for: .normal)
        }else{
            self.chooseContactBtn.setTitle("Select Player", for: .normal)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.isHidden = true
        if isForward || isReply{
            if isForward {
                self.navigationItem.title = "Forward Message"
                updateUIWithForwardMessageData()
            }else{
                self.navigationItem.title = "Reply To Message"
                updateUIWithReplyMessageData()
            }
        
        }else{
            settingPlaceHolderForTextView()
        }
        
        toField.delegate = self
        toField.autocorrectionType = .no
        ccField.delegate = self
        ccField.autocorrectionType = .no
//        toField.clearButtonMode = .always
       //toField.isUserInteractionEnabled = true
        if isForLessonOrClass {
            self.toField.text = NameToSend
            self.chooseContactBtn.setTitle(NameToSend, for: .normal)
            self.chooseContactBtn.isUserInteractionEnabled = false
        }else{
        updateBottomSpace()
        getContacts()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func updateUIWithForwardMessageData() {


        if let desc = forwardMessageData["Subject"] as? String {
            subjectFld.text = desc
        }
        
        if let body = forwardMessageData["Body"] as? String {
            textView.text = body
        }
    }
    
    func updateUIWithReplyMessageData() {
        
    if let title = forwardMessageData["MessageSender"] as? String{
                    self.chooseContactBtn.setTitle(title, for: .normal)
                    toField.text = title
                }
        if let desc = forwardMessageData["Subject"] as? String {
            subjectFld.text = desc
        }
    }

    let placeholderLabel = UILabel()
   
    func settingPlaceHolderForTextView()  {
        
        textView.delegate = self
        placeholderLabel.text = "Enter Message Body"
        placeholderLabel.font = textView.font
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = textView.textColor
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if isReply || isForward {
            self.tabBarController?.navigationController?.isNavigationBarHidden = true

        }else{
            self.navigationController?.isNavigationBarHidden = true
            self.tabBarController?.navigationController?.isNavigationBarHidden = false
        }

        
    }
    
    
    
    
    func getContacts()  {
        
        var academyId = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyId = "\(id)"
        }
        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }

        NetworkManager.performRequest(type:.get, method: "Message/GetAllUsersAsContacts?UserId=\(userId)&AcademyId=\(academyId)&UserRoleId=\(currentUserLogin!)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            default:
                break
          }
            
           self.usersDataArray = object as! [[String : AnyObject]]
            if self.usersDataArray.count > 0{
            self.usersDataArray.remove(at: 0)
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    /****************************************************************************************************/
    //MARK: - Helper Methods
    
    func updateBottomSpace() {
        if isReply == true || isForward == true {
            bottomSpaceConstraint.constant = 0
        } else {
            bottomSpaceConstraint.constant = 20.0
        }
    }
    
    func updateUIWithMessageData() {
        let toRecipients = ["Abc", "Def", "Qatri"]
        let ccRecipients = ["Paul", "King", "Jin"]
        toField.text = toRecipients.joined(separator: ",")
        toField.text?.append(",")
        ccField.text = ccRecipients.joined(separator: ",")
        ccField.text?.append(",")
    }
    
    func searchAutocompleteEntries() {
        autocompleteUrls.removeAll(keepingCapacity: false)
        
        for recipient in GlobalClass.sharedInstance().messageRecipients as! [[String: AnyObject]] {
            if let name = recipient["MessageSender"] as? String, name.hasPrefix(searchString.lowercased()) {
                autocompleteUrls.append(name)
            }
            if let name = recipient["MessageSender"] as? String, name.lowercased().contains(searchString.lowercased()) {
                autocompleteUrls.append(name)
            }
        }
        
        tableView.reloadData()
    }
    
    func getTruncatedString(str: String, toIndex index: Int) -> String {
        let endIndex = str.index(str.endIndex, offsetBy: String.IndexDistance.init(-index))
        return str.substring(to: endIndex)
    }
    
    func lastCharacterIsComma() -> Bool {
        if activeField.text == "" {
            return true
        }
        
        let lastChar = activeField.text![activeField.text!.index(before: activeField.text!.endIndex)]
        return (lastChar == ",")
    }
    
    func updateTextField(with newText: String) {
        // remove search keywords
        activeField.text = getTruncatedString(str: activeField.text!, toIndex: searchString.count)
        
        if lastCharacterIsComma() == true {
            activeField.text?.append("\(newText),")
        } else {
            activeField.text?.append(",\(newText),")
        }
        
        self.tableView.isHidden = true
        searchString = ""
    }
    
    func getRecipientsOf(field: UITextField) -> [String] {
        activeField = field
        var recipients = field.text!.components(separatedBy: ",")
                 if lastCharacterIsComma() == true {
            recipients.removeLast()
        }
        return recipients
    }
    
    /****************************************************************************************************/
    // MARK:- Textfield delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return !isReply
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if activeField != textField {
            searchString = ""
        }
        activeField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "," {
            searchString = ""
            tableView.isHidden = true
            return true
        }
        
        // check if input cursor is at the end
        let cursorPosition = textField.offset(from: textField.endOfDocument, to: (textField.selectedTextRange?.end)!)
        if (cursorPosition == 0) {
            if string == "" {
                if searchString.count > 0 {
                     searchString = getTruncatedString(str: searchString, toIndex: 1)
                }
            }
            else if searchString.count > 0 || lastCharacterIsComma() == true {
                searchString.append(string)
            }
        }
        
        print("Search String: \(searchString)")
        
        if (searchString.count > 0) {
            tableView.isHidden = false
            searchAutocompleteEntries()
        }
        else {
            tableView.isHidden = true
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
    /****************************************************************************************************/
    // MARK:- table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteUrls.count
    }
    
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let autoCompleteRowIdentifier = "ComposeTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: autoCompleteRowIdentifier, for: indexPath as IndexPath) as! ComposeTableViewCell
        cell.titleLbl.text = autocompleteUrls[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        updateTextField(with: autocompleteUrls[indexPath.row])
    }

    /****************************************************************************************************/
    // MARK:- buttons actions
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        toField.text = ""
        if currentUserLogin == 4 {
            self.chooseContactBtn.setTitle("Select Coach", for: .normal)
        }else{
            self.chooseContactBtn.setTitle("Select Player", for: .normal)
        }
        crossBtn.isHidden = true
    }
    
    @IBAction func chooseContctBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectContactViewController") as! SelectContactViewController
        controller.originaldata = usersDataArray
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
   
    }
   
    @IBAction func sendBtnAction(_ sender: UIButton) {
        messageString = ""
        valiDateRegistrationFields()
        if messageString.count > 0{
            let name: String = messageString
            let truncated = name.substring(to: name.index(before: name.endIndex))
            
            let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        submitingForm()
    }
    
    var messageString : String = ""
    
    func valiDateRegistrationFields(){
        
        if let text = toField.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter To Field\n"
        }
//        if let text = ccField.text , !text.isEmpty{
//        }else {
//            messageString = messageString + "Enter CC Field\n"
//        }
        
        if let text = subjectFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter Subject\n"
        }
        if let text = textView.text , !text.isEmpty{
           
            
        }else {
            messageString = messageString + "Enter Body\n"
        }
    }
    //Server fetching
    func submitingForm()  {
        
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        var userID = ""
        if let id =  DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userID = "\(id)"
        }
        
        var roleIdFrom = ""
        var roleIdTo = ""
        if currentUserLogin == 4 {
            roleIdFrom = "4"
             roleIdTo = "4"
        }else{
            roleIdFrom = "3"
            
             roleIdTo = "3"
        }
        var parameters : [String:String] = [String:String]()
        var  receiversIds = ""
        if isForLessonOrClass {
            receiversIds = idToSend
        }else{
            let receiversNames = toField.text!.components(separatedBy: ",")
            if receiversNames.count > 0 {
                for title in receiversNames{
                    for index in usersDataArray {
                        
                        if let name =  index["Text"] as? String {
                            if name == title {
                                if receiversIds == "" {
                                    receiversIds =  "\(index["Value"] as! String)"
                                    
                                }else{
                                    receiversIds = receiversIds + "," +  "\(index["Value"] as! String)"
                                }
                            }
                            
                        }
                    }
                }
            }

        }

        parameters = ["LoggedInUserId":userID ,"UserMessageId":roleIdTo,"MessageId":"" ,"Subject":subjectFld.text! ,"Body":textView.text!,"AuthorId":"" ,"CreatedBy":userID ,"CreationDate":DataManager.sharedInstance.getTodayDate() ,"ModifiedBy":userID,"IsRead":"true" ,"IsTrash":"false" ,"MessageReceiver":receiversIds ,"MessageFolderTypeID":"" ]
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "Message/Compose", parameter:parameters as [String : AnyObject]?, view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            
            let responce = object as! Int

            switch responce {
            case   -1 :
                return
            case   -3 :
                return
            case   -2 :
                return
            case   0 :
                break
            default: break
            }
            
            if self.isForLessonOrClass {
            self.blureView.removeFromSuperview()
            self.dismiss(animated: true, completion:nil)
                DataManager.sharedInstance.printAlertMessage(message:"Message Sent Successfully!", view:self.appDelegate.window!.rootViewController!)
                
            }else{
                self.changingTab()
            }
        
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func changingTab() {
        
        self.refreshFields()
        if isReply || isForward {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }else{
        
        let view1 = self.tabBarController?.view.viewWithTag(999)
        let btn =  view1?.viewWithTag(0001) as! UIButton
        let btn2 =  view1?.viewWithTag(0002) as! UIButton
        let btn3 =  view1?.viewWithTag(0003) as! UIButton
        btn.isSelected = true
        btn2.isSelected = false
        btn3.isSelected = false
            self.tabBarController?.selectedIndex = 0
        }
        DataManager.sharedInstance.printAlertMessage(message:"Message Sent Successfully!", view:UIApplication.getTopestViewController()!)

    }
   
    func refreshFields()  {
        toField.text = ""
        ccField.text = ""
        subjectFld.text = ""
        textView.text = ""
        self.chooseContactBtn.setTitle("", for: .normal)
        self.crossBtn.isHidden = true
    }
    
    //MARK:- buttons Actions
    @IBAction func saveBtnAction(_ sender: UIButton) {
        
    }
    
    @IBAction func discardBtnAction(_ sender: UIButton) {
        
    }

    /****************************************************************************************************/
    // MARK: - Keyboard Hide Notification
    
    @objc func keyboardWillHide() {
        tableView.isHidden = true
    }
}
//MARK:- utility delegate methods
extension ComposeMessageViewController:SelectedContactDelegate{
    func seletedUserData(dic: NSDictionary,selectedUserId:String) {
        print(dic)
        self.crossBtn.isHidden = false
        let  delegatedDictionary = dic as! [String : AnyObject]
        if let title = delegatedDictionary["Text"] as? String{
            self.chooseContactBtn.setTitle(title, for: .normal)
            self.ccField.text = title
            if let text = toField.text , !text.isEmpty{
                toField.text = toField.text! + "," + title
            }else {
                toField.text = title
            }
        }
        
    }
}



