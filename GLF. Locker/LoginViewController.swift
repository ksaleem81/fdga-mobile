//
//  RegisterViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/28/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import MapKit
import ActionSheetPicker_3_0
class LoginViewController: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var userNameFld: UITextField!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var switchBtn: UISwitch!
  
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var keepLoginBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    var data :[[String:AnyObject]] = [[String:AnyObject]]()
    //MARK:- life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        switchBtn.setOn(false, animated: true)
        
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "academyDetails"){
            
        }else{
            print("Dont exhist")
            return
        }
        
        if let titl =  DataManager.sharedInstance.currentAcademy()!["OnlinePaymentType"] as? Int{
            if titl == 0{
                print("braintree")
            }else if titl == 1{
                print("mangoe pay")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        var currentAcademyId  = 0
        var cart = [[String:AnyObject]]()
        
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "academyDetails"){
        }else{
            print("Dont exhist")
            return
        }
        
        if let acemyId = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            currentAcademyId = acemyId
        }
        
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "userCredentials"){
            cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: "userCredentials") as! Data)) as? [[String:AnyObject]])!
            for item in cart {
                if let idOFClass = item["AcademyID"] as? Int{
                    if idOFClass == currentAcademyId {
                        if let savePas = item["savePassword"] as? String,savePas == "1"{
                            self.switchBtn.setOn(true, animated: true)
                            userNameFld.text = item["userName"] as! String?
                            passwordFld.text = item["password"] as! String?
                        }else{
                            self.switchBtn.setOn(false, animated: true)

                        }
                  
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:- buttons action
    @IBAction func keepLoginBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            // DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"False")
        }else{
            sender.isSelected = true
             //DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"True")
        }
    }
    
    func keepLoginLogic(sender:UIButton) {
        
        if sender.isSelected {
            DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"True")
        }else{
            DataManager.sharedInstance.writeInUserDefaults(key: "keepLogin", value:"False")
        }
    }
    
    
    @IBAction func switchBtnAction(_ sender: UISwitch) {
        
       if sender.isOn {
        if (userNameFld.text != "" && passwordFld.text != ""){
            switchBtn.setOn(true, animated: true)
            savingNameAndPasswords()
        }
         }else{
        
        var cart = [[String:AnyObject]]()
        var currentAcademyId  = 0
        if let acemyId = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            currentAcademyId = acemyId
        }
        switchBtn.setOn(false, animated: true)
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "userCredentials"){
            cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: "userCredentials") as! Data)) as? [[String:AnyObject]])!
            var count = 0
            for item in cart {
                if let idOFClass = item["AcademyID"] as? Int{
                    if idOFClass == currentAcademyId {
                      cart[count]["userName"] = "" as AnyObject?
                      cart[count]["password"] = "" as AnyObject?
                      cart[count]["savePassword"] = "0" as AnyObject
                    }
                }
                count = count + 1
            }
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: "userCredentials")
        }
        }
    }
    
    func savingNameAndPasswords() {
    
        var dic = [String:AnyObject]()
        var currentAcademyId  = 0
        var cart = [[String:AnyObject]]()
        if let acemyId = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            currentAcademyId = acemyId
        }
        var itemAdded = false
        var count = 0
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "userCredentials"){
            cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: "userCredentials") as! Data)) as? [[String:AnyObject]])!
            for item in cart {
                
                if let idOFClass = item["AcademyID"] as? Int{
                    if idOFClass == currentAcademyId {
                        itemAdded = true
                        cart[count]["userName"] = userNameFld.text! as AnyObject
                        cart[count]["password"] = passwordFld.text! as AnyObject
                        break;
                    }
                }
                 count = count + 1
            }
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: "userCredentials")
        }
        
        if itemAdded {
            
            if self.switchBtn.isOn == true {
                cart[count].updateValue( "1" as AnyObject, forKey: "savePassword")
            }else{
                cart[count].updateValue( "0" as AnyObject, forKey: "savePassword")

            }
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: "userCredentials")
            
            return
        }else{
            dic["AcademyID"] = currentAcademyId as AnyObject?
            dic["userName"] = userNameFld.text! as AnyObject?
            dic["password"] = passwordFld.text! as AnyObject?
            dic["savePassword"] = "1" as AnyObject
            cart.append(dic)
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: "userCredentials")
        }

    }
    
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        
        messageString = ""
        valiDateRegistrationFields()
        if messageString.count > 0{
            let name: String = messageString
            let truncated = name.substring(to: name.index(before: name.endIndex))
            
            let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        loginService()
    }
    
    var messageString : String = ""
    
    func valiDateRegistrationFields() {
        
        if let text = userNameFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter User Name\n"
//            Utility.viewToShake(viewToShake: userNameFld)
        }
      
        if let text = passwordFld.text , !text.isEmpty{
        }else {
            messageString = messageString + "Enter Password\n"
        }
       
    }
    
    func loginService() {
        
//        ,"DeviceName":UIDevice.current.name as AnyObject
        print(userNameFld.text! as AnyObject,passwordFld.text! as AnyObject)
        NetworkManager.performRequest(type:.post, method: "Login/login", parameter: ["UserName":userNameFld.text! as AnyObject,"Password":passwordFld.text! as AnyObject,"AcademyID":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"Error\nInvalid username/password", view:self)
                return

            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                break
            }
            
            self.data = object as! [[String:AnyObject]]
            if self.data.count > 0{
                let dic = self.data[0]
                if let userType = dic["UserRoleId"] as? Int , userType == 3 || userType == 4 {
                  }else{
          
                DataManager.sharedInstance.printAlertMessage(message:"Error\nInvalid username/password", view:self)
                    return
                }
                self.savingLoginUser()
                self.moveToNextController()
            }else{
                 DataManager.sharedInstance.printAlertMessage(message:"Error\nInvalid username/password", view:self)
            }

        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func moveToNextController()  {
        //write later to save success user
        let btn = UISwitch()
        if switchBtn.isOn {
            btn.setOn(true, animated: false)
            self.switchBtnAction(btn)

        }else{
            btn.setOn(false, animated: false)
            self.switchBtnAction(btn)
        }
        keepLoginLogic(sender: keepLoginBtn)
        //till here can be removed
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
     appDelegate.keepMeLoginOn = false
     appDelegate.synchingInProgress = false
     appDelegate.showDashBoard()
        
    }
    
    func savingLoginUser()  {
        
        if data.count > 0 {
            let dic = data[0]
            print(dic)
            DataManager.sharedInstance.updateLoginDetails(dic as NSDictionary)
            let currentUserClone =  DataManager.sharedInstance.currentUser()?.mutableCopy() as? NSMutableDictionary
            currentUserClone!["userActualPassword"] =  passwordFld.text!
            DataManager.sharedInstance.updateLoginDetails(currentUserClone)
            Utility.savingToAllAccounts()
        }
    }
    
    @IBAction func forgetBtnAction(_ sender: UIButton) {
        
        presentTableViewAsPopOver(sender: sender)
    }
    //MARK:- showing popup for forget password
    func presentTableViewAsPopOver(sender:UIButton) {
        
        let menuViewController : ForgetPasswordViewController = self.storyboard!.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: view.frame.size.width-60, height: 210)
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .down
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = sender as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: sender.bounds.size.width/2,
                y: sender.bounds.origin.y+sender.bounds.size.height,
                width: 1,
                height: 1)
        present(
            menuViewController,
            animated: true,
            completion: nil)
    }
    
}

extension LoginViewController:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return 	UIModalPresentationStyle.none
    }
    
    internal func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
    }
}
