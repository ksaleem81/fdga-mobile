//
//  PastViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/9/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
//import Charts
import ActionSheetPicker_3_0
import SideMenu
import JYRadarChart
protocol updateMoveSchedule {
    func updatedObject(previousData:[String:AnyObject])
}
class MoveLessonViewController: UIViewController {

    var delegate :updateMoveSchedule?
    @IBOutlet weak var lessonLbl: UILabel!
    @IBOutlet weak var lessonDateLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    var durationId = ""
    var imageData = UIImage()
    var durationsArray:[[String:AnyObject]] = [[String:AnyObject]]()
    var studentImageData = [String:AnyObject]()
    var usersDataArray = [[String:AnyObject]]()
    var secondLastControllerData = [String:AnyObject]()
    var previousData = [String:AnyObject]()
    var currentPlayerSelectedId = ""
    var durationTrackArray = [[String:AnyObject]]()
    var garaphData = [[String:AnyObject]]()
    var selectedDuration = 0
    @IBOutlet weak var lessonImage: UIImageView!
    
    @IBOutlet weak var graphView: UIView!//PieChartView!
    var dateOfLesson = ""
    @IBOutlet weak var containerHeightView: NSLayoutConstraint!
    //MARK:- life cycles
    var academyScheduleData = [String : AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(previousData)
//        if let bookDate = previousData["LessonDate"] as? String{
////            print(DataManager.sharedInstance.getDayOfWeek(bookDate,dateFormater: "yyyy-MM-dd'T'HH:mm:ss"))
//        }
        settingGraphDesign()
        getStudentsImage()
        fillUserInfo()
        settingRighMenuBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }
    }

    func settingGraphDesign()  {
        
        self.containerHeightView.constant = self.view.frame.size.width / 2
        var xOfGraph = graphView.frame.origin.x-27
        var yOfGraph = graphView.frame.origin.y-27
        var widthOfGraph = lessonImage.frame.size.width-27
        var heightOfGraph = lessonImage.frame.size.height+27
        if Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() {
            xOfGraph = lessonImage.frame.maxX
            yOfGraph = lessonImage.frame.minY
            widthOfGraph = lessonImage.frame.size.width
            heightOfGraph = lessonImage.frame.size.height
        }
        
        let chart = JYRadarChart.init(frame:CGRect(x:xOfGraph , y:yOfGraph , width:widthOfGraph , height:heightOfGraph))
        self.graphView.superview?.addSubview(DataManager.sharedInstance.gameScoreGraph(chart: chart, data:garaphData,controller: self))
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MoveLessonViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillUserInfo()  {
        lessonImage.clipsToBounds = true
        lessonImage.contentMode = .scaleAspectFill
        lessonImage.image = imageData
        
        if let name = previousData["LessonName"] as? String {
            lessonLbl.text = name
            
        }
        
        if let name = previousData["StartTime"] as? String {
            //            localechanges
            if is12Houre == "true"{
                startTimeLbl.text = Utility.convertTimeInto12Formate(timeStr: name)
            }else{
                if (name.count) > 5 {
                    startTimeLbl.text = String(name.dropLast(3))
                }else{
                    startTimeLbl.text = name
                }
                
            }
            
            
        }
        if let name = previousData["EndTime"] as? String {
            //            localechanges
            if is12Houre == "true"{
                endTimeLbl.text = Utility.convertTimeInto12Formate(timeStr: name)
            }else{
                if (name.count) > 5 {
                    endTimeLbl.text = String(name.dropLast(3))
                }else{
                    endTimeLbl.text = name
                }
                
            }
            
        }
        
        
        
        if let date = previousData["LessonDate"] as? String {
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                lessonDateLbl.text =  DataManager.sharedInstance.getFormatedDateWithoutWeekDay(date:dateOrignal[0],formate:"yyyy-MM-dd")
                dateOfLesson = dateOrignal[0]
            }
        }
        
        var timeStart = ""
        if startTimeLbl.text != "" {
            timeStart = startTimeLbl.text!
            print(timeStart.count)
            
            //            localechanges
            if is12Houre == "true"{
                timeStart = Utility.convertTimeInto12Formate(timeStr: timeStart)
                
            }else{
                
                if (startTimeLbl.text?.count)! > 5 {
                    
                    timeStart = String(timeStart.dropLast(3))
                }
                
            }
            
        }
        var timeEnd = ""
        if endTimeLbl.text != "" {
            timeEnd = endTimeLbl.text!
            
            //            localechanges
            if is12Houre == "true"{
                timeEnd = Utility.convertTimeInto12Formate(timeStr: timeEnd)
                
            }else{
                if (endTimeLbl.text?.count)! > 5 {
                    
                    timeEnd = String(timeEnd.dropLast(3))
                }
                
            }
        }
        let diffrence =  DataManager.sharedInstance.differenceBetweenToTimeStrings(startTime:timeStart, endTime: timeEnd)
        selectedDuration = diffrence
        
        //dynamicChange
        
        if isNewUrl{

            Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(lessonDateLbl.text!,dateFormater:"dd MMM yyyy"))", controller: self, Success: { data  in
            self.academyScheduleData = data as! [String : AnyObject]
        }, onFailure: {(error) in
            
        })
            
        }
        
        
    }
    
    func getStudentsImage()  {
        
        var studentId = ""
        if let id = previousData["StudentID"] as? Int{
            studentId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetStudentImages/\(studentId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break;
                
            }
            
            self.studentImageData = object as! [String : AnyObject]
            if let fileUrl = self.studentImageData["ImagerUrl"] as? String {
                var originalUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
                originalUrl = imageBaseUrl + originalUrl
                self.lessonImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options:.cacheMemoryOnly, completed: { (image, error, cacheType, url) in
                    DispatchQueue.main.async (execute: {
                        if let _ = image{
                            self.lessonImage.image = image;
                        }
                        else{
                            self.lessonImage.image = UIImage(named:"photoNot")
                            
                        }
                    });
                    
                })
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    @IBAction func selectStudentBtnAction(_ sender: UIButton) {

    }

    @IBAction func lessonDateBtnAction(_ sender: UIButton) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        let formator = DateFormatter()
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            print(selectedDate)
            let date = selectedDate as! NSDate
            
            formator.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            self.dateOfLesson = formator.string(from: date as Date)
            
//            print(DataManager.sharedInstance.getDayOfWeek(self.dateOfLesson))
            
            formator.dateFormat = "dd MMM yyyy"
            let title = formator.string(from: date as Date)
            self.lessonDateLbl.text = title
            //dynamicChange
            if isNewUrl{
            
                Utility.getScheduleTimeOfAcademy(selectedDay: "\(DataManager.sharedInstance.getDayOfWeek(self.lessonDateLbl.text!,dateFormater:"dd MMM yyyy"))", controller: self, Success: { data  in
                self.academyScheduleData = data as! [String : AnyObject]
            }, onFailure: {(error) in
                
            })
                
            }
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        
        picker?.show()
    }
    
    @IBAction func durationBtnAction(_ sender: UIButton) {
    
        var dataAray = [String]()
        durationTrackArray = [[String:AnyObject]]()
        for dic in durationsArray{
            if let val = dic["Name"] as? String{
                durationTrackArray.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT DURATION", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.durationLbl.text = "\(indexes!)"
            if let selectedTyp = self.durationTrackArray[values]["DurationId"] as? Int{
                self.durationId = "\(selectedTyp)"
            }
            if let duration = self.durationTrackArray[values]["Id"] as? Int{
                self.selectedDuration = duration
            }

            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }

//if got issue than just remove Date().roundTime that make current time round
    @IBAction func startTimeBtnAction(_ sender: UIButton) {
     
        let formator = DateFormatter()
        
        var picker =  ActionSheetDatePicker.init(title: "Select Time", datePickerMode: .time, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedTime,selectedValue ) -> Void in
            //            localechanges
            if is12Houre == "true"{
                formator.dateFormat = "hh:mm a"
            }else{
                formator.dateFormat = "HH:mm"
            }
            let selectedTimeCurrent = selectedTime as! NSDate
            let startTitle = formator.string(from:Date().roundTime(date: selectedTimeCurrent as Date, minuteInterval: 15))
            if is12Houre == "true"{
                self.startTimeLbl.text = startTitle
            }else{
                
                self.startTimeLbl.text = startTitle
            }
     
            //for end time
            let endDate = selectedTimeCurrent.addingTimeInterval(Double(self.selectedDuration) * 60.0)
            let endTitle = formator.string(from: endDate as Date)
            self.endTimeLbl.text = endTitle

        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        //dynamicChange
        if isNewUrl{
            picker = Utility.dynamicSettingTimeLimitToPicker(datePicker: picker!, data: self.academyScheduleData)
            
        }else{
            picker =  Utility.settingTimeLimitToPicker(datePicker: picker!)
            
        }
        picker?.minuteInterval = 15
        //            localechanges
        if is12Houre == "true"{
            picker?.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        }else{
            picker?.locale = NSLocale(localeIdentifier: "en_GB") as Locale!
        }
            picker?.show()
    }
    
    @IBAction func endTimeBtnAction(_ sender: UIButton) {
        
    }
    
    func convertToArray(text: String) -> [[String: AnyObject]]? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func getStudents()  {
        
        let classID = "0"
        NetworkManager.performRequest(type:.post, method: "academy/GetAcademyPlayerList/", parameter:["AcademyId":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"UserId":DataManager.sharedInstance.currentUser()?["Userid"] as AnyObject!,"ClassId":classID as AnyObject!], view: (self.appDelegate.window), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.usersDataArray = object as! [[String : AnyObject]]
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    //MARK:- buttons actions
    @IBAction func moveBtnAction(_ sender: UIButton) {

        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        //        localechanges
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        var itStudio = false
        if let flag = previousData["IsStudio"] as? Bool,flag == true{
            itStudio = true
        }else{
            itStudio = false
        }

        var itPutting = false
        if let flag = previousData["isPuttingLab"] as? Bool,flag == true{
            itPutting = true
        }else{
            itPutting = false
        }

        NetworkManager.performRequest(type:.post, method: "Booking/MoveLesson/", parameter: ["StartTime":startTimeLbl.text! as AnyObject,"EndTime":endTimeLbl.text! as AnyObject,"LessonId":lessonID as AnyObject,"MoveDate":lessonDateLbl.text as AnyObject!,"Is12HoursTimeFormat":is12Houre as AnyObject!,"AcademyId":acadmyId as AnyObject!,"IsStudio":"\(itStudio)" as AnyObject!,"IsPutting":"\(itPutting)" as AnyObject!], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
          let responce = object as! String
            
            if responce == "2"{
                
                  DataManager.sharedInstance.printAlertMessage(message: "Unable to move lesson because a lesson is already booked at selected time.", view:self)
            }
            else if responce == "4"{
                
                DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
                
            }else if responce == "7"{
                
                DataManager.sharedInstance.printAlertMessage(message: "You have Group Lesson(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
                
            }else if responce == "5"{
                
                DataManager.sharedInstance.printAlertMessage(message: "You have Leave(s) Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
                
            }else if responce == "3"{
               // You have One To One Lesson(s) Or Group Lesson(s)  Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson
                DataManager.sharedInstance.printAlertMessage(message: "Unable to move lesson because a lesson is already booked at selected time.", view:self)
            }else if responce == "6"{
                //for leave
                DataManager.sharedInstance.printAlertMessage(message: "You have One To One Lesson(s) Or Group Lesson(s) Or Leave(s)  Scheduled in this time. Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }
            else if responce == "1"{
                _ = self.navigationController?.popViewController(animated: true)
                self.previousData["StartTime"] = self.startTimeLbl.text! as AnyObject
                self.previousData["EndTime"] = self.endTimeLbl.text! as AnyObject
                self.previousData["LessonDate"] = self.dateOfLesson as AnyObject
                self.delegate?.updatedObject(previousData: self.previousData)
                DataManager.sharedInstance.printAlertMessage(message: "Successfully Moved lesson!", view:UIApplication.getTopestViewController()!)
            }else{
                 DataManager.sharedInstance.printAlertMessage(message: "Lesson slot is not available At this time.Please contact your Academy Mananger to schedule cover for this lesson.", view:self)
            }
     //general meesage//Unable to move lesson because a lesson is already booked at selected time.
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func moveLessonBtnAction(_ sender: UIButton) {
    }
    
    
 
}


