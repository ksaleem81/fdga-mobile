//
//  SideMenuTableViewController.swift
//  MyAlist
//
//  Created by Apple PC on 26/08/2016.
//  Copyright © 2016 ArhamSoft. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class SideMenuTableViewController: UITableViewController {

    @IBOutlet weak var versionLbl: UILabel!
      var defaultArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STORE","image":"icon_26"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"MORE>","image":"plus_icon-1"]]
      var backupDefaultArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STORE","image":"icon_26"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"MORE>","image":"plus_icon-1"]]
        var moreArray = [["name":"MY LOCKER","image":"dashborde"],["name":"STORE","image":"icon_26"],["name":"MEDIA","image":"media_icon"],["name":"VIEW CART","image":"carticon"],["name":"MORE>","image":"minus_icon-1"],["name":"SWITCH ACCOUNT","image":"switchaccount"],["name":"T&C'S","image":"term_icon"],["name":"PRIVACY","image":"privacy_icon"],["name":"LOGOUT","image":"logout-1"]]
    var selectedCellColor = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let appVersion: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        self.tableView.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        self.view.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        let tableViewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        tableViewFooter.backgroundColor = UIColor.clear
        let version = UILabel(frame: CGRect(x:self.view.frame.midX-100, y:0, width:150, height:30))
        version.font = version.font.withSize(11)
        version.text = "\(currentTarget) App " + (appVersion as! String?)!
        version.textColor = UIColor.init(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0)
        version.textAlignment = .center;
        tableViewFooter.addSubview(version)
        tableView.tableFooterView  = tableViewFooter

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
      
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let sideView = UIView.init(frame: CGRect(x: 0, y: -80, width: 1, height: 1000))
        sideView.backgroundColor = UIColor.init(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0)
        sideView.tag = 123
        self.view.addSubview(sideView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.view.viewWithTag(123)?.removeFromSuperview()
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of rows
        return defaultArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuUserTableViewCell", for: indexPath) as! SideMenuUserTableViewCell
        cell.cellTitle.text = defaultArray[indexPath.row]["name"]
        cell.cellImage.image = UIImage(named: defaultArray[indexPath.row]["image"]!)
        if selectedCellColor == indexPath.row {
            cell.backgroundColor =  UIColor.init(red: 204/255, green: 213/255, blue: 35/255, alpha: 1.0)
        }else{
            cell.backgroundColor = UIColor.init(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let window = UIApplication.shared.delegate?.window
        var controller  = UIViewController()
        
        if defaultArray.count == 5 {
             if defaultArray[indexPath.row]["name"] == "MORE>"{
                defaultArray = moreArray
                selectedCellColor = indexPath.row
                tableView.reloadData()
                return
            }
            
        }else if defaultArray.count  == 9 {
             if defaultArray[indexPath.row]["name"] == "MORE>"{
                defaultArray = backupDefaultArray
                selectedCellColor = indexPath.row
                tableView.reloadData()
                return
            }
        }
        
        if indexPath.row == 0 {
            controller = (self.storyboard?.instantiateViewController(withIdentifier: "Dashboard"))!
            self.parent?.dismiss(animated: true, completion: {
                window??.rootViewController = controller
            })
        }
        else if indexPath.row == 8 && defaultArray.count == 9{
            
//            logoutService()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            self.parent?.dismiss(animated: false, completion: {
                appDelegate.showInitialController()
            })
           
        }
        else if indexPath.row == 1 {
            
            var url = storeLink
//            if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
//                url = url + "\(id)"
//            }
            UIApplication.shared.openURL(NSURL(string:url)! as URL)
               self.parent?.dismiss(animated: true, completion: nil)
        }
       else if indexPath.row == 2 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            self.navigationController?.pushViewController(controller, animated: true)
        
        }else if indexPath.row == 5 && defaultArray.count == 9{
            controller = (self.storyboard?.instantiateViewController(withIdentifier: "SwitchAccountViewController"))!
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 3{
       
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }else if indexPath.row == 6  && defaultArray.count == 9{
                
           showingTT()
        }else if indexPath.row == 7  && defaultArray.count == 9{
                
           showingPP()
        }
        else {
            self.parent?.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func showingTT()  {
         
         var titleAcademy = ""
                         if let titl =  DataManager.sharedInstance.currentAcademy()!["Terms"] as? String{
                             titleAcademy = titl
                         }
                         
             //            privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
         //                privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"
                         print(privacyTermsPDF)
                    UIApplication.shared.openURL(NSURL(string:titleAcademy)! as URL)
                         self.parent?.dismiss(animated: true, completion: nil)

     }
    
    func showingPP(){
        
        var titleAcademy = ""
                        if let titl =  DataManager.sharedInstance.currentAcademy()!["PrivacyPolicy"] as? String{
                            privacyTermsPDF = titl
                        }
        //                privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
//                    privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"
                        UIApplication.shared.openURL(NSURL(string:privacyTermsPDF)! as URL)
                        self.parent?.dismiss(animated: true, completion: nil)
        
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42.0
    }
    
    func logoutService() {
        
        var userID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
        }
        
        NetworkManager.performRequest(type:.post, method: "Login/logout", parameter: ["Userid":userID as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"Error\nInvalid username/password", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                break
            }
            
            let success = object as! Bool
            if success{
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                self.parent?.dismiss(animated: false, completion: {
                    appDelegate.showInitialController()
                })
                
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

}
