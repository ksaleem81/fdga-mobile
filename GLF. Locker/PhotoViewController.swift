//
//  PhotoViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/20/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SDWebImage
import SideMenu
class PhotoViewController: UIViewController {

    var dataDictionary = [String:AnyObject]()
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var discriptionLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.startAnimating()
        if let url = dataDictionary["Description"] as? String {
            discriptionLbl.text = url
        }
        var fileUrl = ""
        if let url = dataDictionary["ThumbnilURL"] as? String {
            fileUrl = url
        }
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
        fileUrl = baseUrlForVideo + fileUrl
        
        self.bgImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
            
            DispatchQueue.main.async (execute: {
            self.activityIndicator.isHidden = true
                if let _ = image{
                    
                    self.bgImage.image = image;
                }
                else{
                         self.activityIndicator.startAnimating()
                    self.bgImage.image = UIImage(named:"photoNot")
                }
            });
            
        })
//        settingRighMenuBtn()
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PhotoViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
