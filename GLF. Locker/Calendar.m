#import "Calendar.h"
#import <Cordova/CDV.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface Calendar()
{
    dispatch_queue_t myBackgroundQueue;
    MBProgressHUD *hud;
}
@end

@implementation Calendar
@synthesize eventStore, fetchedEvents;

#pragma mark Initialisation functions
//UIwebview deprecations
//- (CDVPlugin*) initWithWebView:(UIWebView*)theWebView {
//    self = (Calendar*)[super initWithWebView:theWebView];
//    if (self) {
//        [self initEventStoreWithCalendarCapabilities];
//        _isModifying=NO;
//        fetchedEvents=nil;
//        myBackgroundQueue = dispatch_queue_create("com.orbis.glflocker", NULL);
//    }
//    return self;
//}

- (void)initEventStoreWithCalendarCapabilities {
    __block BOOL accessGranted = NO;
    _isModifying=NO;
    fetchedEvents=nil;
    self.eventStore= [[EKEventStore alloc] init];
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    } else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        self.eventStore = eventStore;
    }
}

#pragma mark Helper Functions

- (void)createEventWithCalendar:(CDVInvokedUrlCommand*)command calendar: (EKCalendar *) calendar {
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    
    NSString* title      = [options objectForKey:@"title"];
    NSString* location   = [options objectForKey:@"location"];
    NSString* notes      = [options objectForKey:@"notes"];
    NSNumber* startTime  = [options objectForKey:@"startTime"];
    NSNumber* endTime    = [options objectForKey:@"endTime"];
    
    NSTimeInterval _startInterval = [startTime doubleValue] / 1000; // strip millis
    NSDate *myStartDate = [NSDate dateWithTimeIntervalSince1970:_startInterval];
    
    NSTimeInterval _endInterval = [endTime doubleValue] / 1000; // strip millis
    
    EKEvent *myEvent = [EKEvent eventWithEventStore:self.eventStore];
    myEvent.title = title;
    myEvent.location = location;
    myEvent.notes = notes;
    myEvent.startDate = myStartDate;
    
    int duration = _endInterval - _startInterval;
    int moduloDay = duration % (60*60*24);
    if (moduloDay == 0) {
        //        myEvent.allDay = YES;
        myEvent.endDate = [myStartDate dateByAddingTimeInterval:1];
    } else {
        myEvent.endDate = [NSDate dateWithTimeIntervalSince1970:_endInterval];
    }
    myEvent.calendar = calendar;
    
    // if a custom reminder is required: use createCalendarWithOptions
    //    EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-1*60*60];
    //    [myEvent addAlarm:reminder];
    
    NSError *error = nil;
    [self.eventStore saveEvent:myEvent span:EKSpanThisEvent error:&error];
    
    if (error) {
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    } else {
        NSLog(@"Reached Success");
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }
}


- (void)createEventsWithCalendar:(CDVInvokedUrlCommand*)command calendar: (EKCalendar *) calendar
{
    NSString *callbackId = command.callbackId;
    NSDictionary* mainDictionary = [command.arguments objectAtIndex:0];
    NSError *error = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
        hud = [MBProgressHUD showHUDAddedTo:appDelegate.window.rootViewController.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"syncing calendar events...";
        [hud show:YES];
    });
    
    NSArray *calendarEvents=[mainDictionary objectForKey:@"calendarEvents"];
    
    for(NSDictionary *options in calendarEvents)
    {
//        NSLog(@"options: %@", options);
        NSString* title      = [options objectForKey:@"title"];
        NSString* location   = [options objectForKey:@"location"];
        NSString* notes      = [options objectForKey:@"notes"];
        NSNumber* startTime  = [options objectForKey:@"startTime"];
        NSNumber* endTime    = [options objectForKey:@"endTime"];
        
        NSTimeInterval _startInterval = [startTime doubleValue] / 1000; // strip millis
        NSDate *myStartDate = [NSDate dateWithTimeIntervalSince1970:_startInterval];
        
        NSTimeInterval _endInterval = [endTime doubleValue] / 1000; // strip millis
        
        NSArray *matchingEvents = [self findEKEventsWithTitle:title location:location notes:notes startDate:nil endDate:nil calendar:calendar];
        
        NSError *error = NULL;
        for (EKEvent * event in matchingEvents) {
            [self.eventStore removeEvent:event span:EKSpanThisEvent error:&error];
        }
        
        EKEvent *myEvent = [EKEvent eventWithEventStore:self.eventStore];
        myEvent.title = title;
        myEvent.location = location;
        myEvent.notes = notes;
        myEvent.startDate = myStartDate;
        
        int duration = _endInterval - _startInterval;
        int moduloDay = duration % (60*60*24);
        if (moduloDay == 0) {
            //        myEvent.allDay = YES;
            myEvent.endDate = [myStartDate dateByAddingTimeInterval:1];
        } else {
            myEvent.endDate = [NSDate dateWithTimeIntervalSince1970:_endInterval];
        }
        myEvent.calendar = calendar;

        NSString *eventType=[options objectForKey:kEventTypeKey];
        if([eventType isEqualToString:kEventTypeLeaveKey])
        {
            NSNumber *programTypeID=[options objectForKey:kEventProgramTypeIDKey];
            if(programTypeID.intValue==kEventProgramTypeIDFullDayLeave)
            {
                myEvent.allDay=YES;
            }
        }

        
        // if a custom reminder is required: use createCalendarWithOptions
        //    EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-1*60*60];
        //    [myEvent addAlarm:reminder];
        
        [self.eventStore saveEvent:myEvent span:EKSpanThisEvent error:&error];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hide:YES];
        hud=nil;
    });
    
    if (error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        });
    } else {
        NSLog(@"Reached Success");
        dispatch_async(dispatch_get_main_queue(), ^{
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        });
    }
    
}

- (void)createEventWithCalendar:(CDVInvokedUrlCommand*)command
                       calendar:(EKCalendar *) calendar title:(NSString*)title location:(NSString*)location notes:(NSString*)notes startTime:(NSDate*)myStartDate endTime:(NSDate*)myEndDate{
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    
//    NSLog(@"createEventWithCalendar: %@", options);

    
    //    NSNumber* startTime  = [options objectForKey:@"startTime"];
    //    NSNumber* endTime    = [options objectForKey:@"endTime"];
    
    NSTimeInterval _startInterval = [myStartDate timeIntervalSince1970];
    NSTimeInterval _endInterval = [myEndDate timeIntervalSince1970];
    
    EKEvent *myEvent = [EKEvent eventWithEventStore:self.eventStore];
    myEvent.title = title;
    myEvent.location = location;
    myEvent.notes = notes;
    myEvent.startDate = myStartDate;
    
    int duration = _endInterval - _startInterval;
    int moduloDay = duration % (60*60*24);
    if (moduloDay == 0) {
        //        myEvent.allDay = YES;
        myEvent.endDate = [myStartDate dateByAddingTimeInterval:1];
    } else {
        myEvent.endDate = myEndDate;
    }
    myEvent.calendar = calendar;
    
    NSString *eventType=[options objectForKey:kEventTypeKey];
    if([eventType isEqualToString:kEventTypeLeaveKey])
    {
        NSNumber *programTypeID=[options objectForKey:kEventProgramTypeIDKey];
        if(programTypeID.intValue==kEventProgramTypeIDFullDayLeave)
        {
            myEvent.allDay=YES;
        }
    }
    
    // if a custom reminder is required: use createCalendarWithOptions
    //    EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-1*60*60];
    //    [myEvent addAlarm:reminder];
    
    NSError *error = nil;
    [self.eventStore saveEvent:myEvent span:EKSpanThisEvent error:&error];
    
    self.fetchedEvents = nil;
    
    if (error) {
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    } else {
        NSLog(@"Reached Success");
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }
}

- (void)createEventWithCalendar:(EKCalendar *) calendar title:(NSString*)title location:(NSString*)location notes:(NSString*)notes startTime:(NSDate*)myStartDate endTime:(NSDate*)myEndDate{
    
    NSTimeInterval _startInterval = [myStartDate timeIntervalSince1970];
    NSTimeInterval _endInterval = [myEndDate timeIntervalSince1970];
    
    EKEvent *myEvent = [EKEvent eventWithEventStore:self.eventStore];
    myEvent.title = title;
    myEvent.location = location;
    myEvent.notes = notes;
    myEvent.startDate = myStartDate;
    
    int duration = _endInterval - _startInterval;
    int moduloDay = duration % (60*60*24);
    if (moduloDay == 0) {
        //        myEvent.allDay = YES;
        myEvent.endDate = [myStartDate dateByAddingTimeInterval:1];
    } else {
        myEvent.endDate = myEndDate;
    }
    myEvent.calendar = calendar;
    
    NSError *error = nil;
    [self.eventStore saveEvent:myEvent span:EKSpanThisEvent error:&error];
    
    if (error) {

    } else {
        NSLog(@"Reached Success");
    }
}




-(EKRecurrenceFrequency) toEKRecurrenceFrequency:(NSString*) recurrence {
    if ([recurrence isEqualToString:@"daily"]) {
        return EKRecurrenceFrequencyDaily;
    } else if ([recurrence isEqualToString:@"weekly"]) {
        return EKRecurrenceFrequencyWeekly;
    } else if ([recurrence isEqualToString:@"monthly"]) {
        return EKRecurrenceFrequencyMonthly;
    } else if ([recurrence isEqualToString:@"yearly"]) {
        return EKRecurrenceFrequencyYearly;
    }
    // default to daily, so invoke this method only when recurrence is set
    return EKRecurrenceFrequencyDaily;
}

-(void)modifyEventWithCalendar:(CDVInvokedUrlCommand*)command
                      calendar: (EKCalendar *) calendar {
    NSString *callbackId = command.callbackId;
    
    NSDictionary* options = [command.arguments objectAtIndex:0];
//    NSLog(@"modifyEventWithCalendar: %@", options);
    
    NSString* title      = [options objectForKey:@"title"];
    NSString* location   = [options objectForKey:@"location"];
    NSString* notes      = [options objectForKey:@"notes"];
    NSNumber* startTime  = [options objectForKey:@"startTime"];
    NSNumber* endTime    = [options objectForKey:@"endTime"];
    
    NSString* ntitle     = [options objectForKey:@"newTitle"];
    NSString* nlocation  = [options objectForKey:@"newLocation"];
    NSString* nnotes     = [options objectForKey:@"newNotes"];
    NSNumber* nstartTime = [options objectForKey:@"newStartTime"];
    NSNumber* nendTime   = [options objectForKey:@"newEndTime"];
    
    NSDate *myStartDate = nil;
    if(startTime && ![startTime isEqual:[NSNull null]])
    {
        NSTimeInterval _startInterval = [startTime doubleValue] / 1000; // strip millis
        myStartDate = [NSDate dateWithTimeIntervalSince1970:_startInterval];
    }
    
    NSDate *myEndDate=nil;
    if(endTime && ![endTime isEqual:[NSNull null]])
    {
        NSTimeInterval _endInterval = [endTime doubleValue] / 1000; // strip millis
        myEndDate = [NSDate dateWithTimeIntervalSince1970:_endInterval];
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // Find matches
    NSArray *matchingEvents = [self findEKEventsWithTitle:title location:location notes:notes startDate:myStartDate endDate:myEndDate calendar:calendar];
    
    if (matchingEvents.count > 0) {
        // Presume we have to have an exact match to modify it!
        // Need to load this event from an EKEventStore so we can edit it
        EKEvent *theEvent = [self.eventStore eventWithIdentifier:((EKEvent*)[matchingEvents lastObject]).eventIdentifier];
        if (ntitle) {
            theEvent.title = ntitle;
        }
        if (nlocation) {
            theEvent.location = nlocation;
        }
        if (nnotes) {
            theEvent.notes = nnotes;
        }
        if (nstartTime) {
            NSTimeInterval _nstartInterval = [nstartTime doubleValue] / 1000; // strip millis
            theEvent.startDate = [NSDate dateWithTimeIntervalSince1970:_nstartInterval];
        }
        if (nendTime) {
            NSTimeInterval _nendInterval = [nendTime doubleValue] / 1000; // strip millis
            theEvent.endDate = [NSDate dateWithTimeIntervalSince1970:_nendInterval];
        }
        
        NSString *eventType=[options objectForKey:kEventTypeKey];
        if([eventType isEqualToString:kEventTypeLeaveKey])
        {
            NSNumber *programTypeID=[options objectForKey:kEventProgramTypeIDKey];
            if(programTypeID.intValue==kEventProgramTypeIDFullDayLeave)
            {
                theEvent.allDay=YES;
            }
        }

        // Now save the new details back to the store
        NSError *error = nil;
        [self.eventStore saveEvent:theEvent span:EKSpanThisEvent error:&error];
        
        for (EKEvent * event in matchingEvents) {
            if(![event isEqual:matchingEvents.lastObject])
            {
                [self.eventStore removeEvent:event span:EKSpanThisEvent error:&error];
            }
        }
        
        
        // Check error code + return result
        if (error) {
            CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        } else {
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        }
    } else {
        
        NSDate *nStartDate=nil;
        if (nstartTime) {
            NSTimeInterval _nstartInterval = [nstartTime doubleValue] / 1000; // strip millis
            nStartDate = [NSDate dateWithTimeIntervalSince1970:_nstartInterval];
        }
        NSDate *nEndDate=nil;
        if (nendTime) {
            NSTimeInterval _nendInterval = [nendTime doubleValue] / 1000; // strip millis
            nEndDate = [NSDate dateWithTimeIntervalSince1970:_nendInterval];
        }
        
        [self createEventWithCalendar:command calendar:calendar title:ntitle location:nlocation notes:nnotes startTime:nStartDate endTime:nEndDate];
        //        // Otherwise return a no result error (could be more than 1, but not a biggie)
        //        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        //        [self writeJavascript:[pluginResult toErrorCallbackString:callbackId]];
    }
    
}

- (void) modifyEventWithCalendar:(NSString*)calendarName title:(NSString*)title newTitle:(NSString*)ntitle newLocation:(NSString*)nlocation newNotes:(NSString*) nnotes newStartDate:(NSDate*)nStartDate newEndDate:(NSDate*)nEndDate
{
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar != nil) {
        
        // Find matches
        NSArray *matchingEvents = [self findEKEventsWithTitle:title location:nil notes:nil startDate:nil endDate:nil calendar:calendar];
        
        if (matchingEvents.count == 1) {
            // Presume we have to have an exact match to modify it!
            // Need to load this event from an EKEventStore so we can edit it
            EKEvent *theEvent = [self.eventStore eventWithIdentifier:((EKEvent*)[matchingEvents lastObject]).eventIdentifier];
            if (ntitle) {
                theEvent.title = ntitle;
            }
            if (nlocation) {
                theEvent.location = nlocation;
            }
            if (nnotes) {
                theEvent.notes = nnotes;
            }

            theEvent.startDate = nStartDate;
            theEvent.endDate = nEndDate;
            
            // Now save the new details back to the store
            NSError *error = nil;
            [self.eventStore saveEvent:theEvent span:EKSpanThisEvent error:&error];
            
            // Check error code + return result
            if (error) {

            } else {

            }
        } else {
            [self createEventWithCalendar:calendar title:ntitle location:nlocation notes:nnotes startTime:nStartDate endTime:nEndDate];
        }
    }
    
}

- (void)deleteEventFromCalendar:(CDVInvokedUrlCommand*)command
                       calendar: (EKCalendar *) calendar {
    
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    
//    NSLog(@"deleteEventFromCalendar: %@", options);
    
    NSString* title      = [options objectForKey:@"title"];
    NSString* location   = [options objectForKey:@"location"];
    NSString* notes      = [options objectForKey:@"notes"];
    NSNumber* startTime  = [options objectForKey:@"startTime"];
    NSNumber* endTime    = [options objectForKey:@"endTime"];
    
    NSDate *myStartDate = nil;
    if(startTime && ![startTime isEqual:[NSNull null]])
    {
        NSTimeInterval _startInterval = [startTime doubleValue] / 1000; // strip millis
        myStartDate = [NSDate dateWithTimeIntervalSince1970:_startInterval];
    }
    
    NSDate *myEndDate=nil;
    if(endTime && ![endTime isEqual:[NSNull null]])
    {
        NSTimeInterval _endInterval = [endTime doubleValue] / 1000; // strip millis
        myEndDate = [NSDate dateWithTimeIntervalSince1970:_endInterval];
    }
    
    
    NSArray *matchingEvents = [self findEKEventsWithTitle:title location:location notes:notes startDate:myStartDate endDate:myEndDate calendar:calendar];
    
    NSError *error = NULL;
    for (EKEvent * event in matchingEvents) {
        [self.eventStore removeEvent:event span:EKSpanThisEvent error:&error];
    }
    
    self.fetchedEvents = nil;
    
    if (error) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    } else {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }
    
}

- (void) deleteEventFromCalendar:(NSString*)calendarName withTitle:(NSString*)title
{
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar != nil) {
        NSArray *matchingEvents = [self findEKEventsWithTitle:title location:nil notes:nil startDate:nil endDate:nil calendar:calendar];
        NSError *error = NULL;
        for (EKEvent * event in matchingEvents) {
            [self.eventStore removeEvent:event span:EKSpanThisEvent error:&error];
        }
    }
}

-(NSArray*)findEKEventsWithTitle: (NSString *)title
                        location: (NSString *)location
                           notes: (NSString *)notes
                       startDate: (NSDate *)startDate
                         endDate: (NSDate *)endDate
                        calendar: (EKCalendar *) calendar {
    
    //    // Build up a predicateString - this means we only query a parameter if we actually had a value in it
    //    NSMutableString *predicateString= [[NSMutableString alloc] initWithString:@""];
    //    if (title != (id)[NSNull null] && title.length > 0) {
    //        [predicateString appendString:[NSString stringWithFormat:@"title == '%@'", title]];
    //    }
    //    if (location != (id)[NSNull null] && location.length > 0) {
    //        [predicateString appendString:[NSString stringWithFormat:@" AND location == '%@'", location]];
    //    }
    //    if (notes != (id)[NSNull null] && notes.length > 0) {
    //        [predicateString appendString:[NSString stringWithFormat:@" AND notes == '%@'", notes]];
    //    }
    
    startDate=nil;
    endDate=nil;
    NSPredicate *matches = [NSPredicate predicateWithFormat:@"title contains[c] %@", title];
    
    if (_isModifying) {
        if(self.fetchedEvents!=nil && self.fetchedEvents.count>0) {
            NSArray *matchingEvents = [fetchedEvents filteredArrayUsingPredicate:matches];
            if(matchingEvents.count>0)
                return matchingEvents;
        }
    }
    
    NSArray *calendarArray = [NSArray arrayWithObject:calendar];
    NSCalendar *mcalendar = [NSCalendar currentCalendar];
    if(!startDate)
    {
        // Create the start date components
        NSDateComponents *fiveMonthAgoComponents = [[NSDateComponents alloc] init];
        fiveMonthAgoComponents.month = -5;
        startDate = [mcalendar dateByAddingComponents:fiveMonthAgoComponents
                                               toDate:[NSDate date]
                                              options:0];
    }
    
    if(!endDate)
    {
        // Create the end date components
        NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
        oneYearFromNowComponents.year = 1;
        endDate = [mcalendar dateByAddingComponents:oneYearFromNowComponents
                                             toDate:[NSDate date]
                                            options:0];
    }
    
    // Create the predicate from the event store's instance method
    NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:startDate
                                                                      endDate:endDate
                                                                    calendars:calendarArray];
    
    // Fetch all events that match the predicate
    self.fetchedEvents = [self.eventStore eventsMatchingPredicate:predicate];
    
    NSArray *matchingEvents = [fetchedEvents filteredArrayUsingPredicate:matches];
    
    return matchingEvents;
}

-(EKCalendar*)findEKCalendar: (NSString *)calendarName {
    NSArray * calendars = [self.eventStore calendarsForEntityType:EKEntityTypeEvent];
    for (EKCalendar *thisCalendar in calendars){
        NSLog(@"Calendar: %@", thisCalendar.title);
        if ([thisCalendar.title isEqualToString:calendarName]) {
            return thisCalendar;
        }
    }
    NSLog(@"No match found for calendar with name: %@", calendarName);
    return nil;
}

-(EKSource*)findEKSource {
    
    NSLog(@"sources: %@", self.eventStore.sources);
    
    // if iCloud is on, it hides the local calendars, so check for iCloud first
    for (EKSource *source in self.eventStore.sources) {
        if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"]) {
            return source;
        }
    }
    
    // ok, not found.. so it's a local calendar
    for (EKSource *source in self.eventStore.sources) {
        if (source.sourceType == EKSourceTypeLocal) {
            return source;
        }
    }
    return nil;
}

#pragma mark Cordova functions

- (void)listCalendars:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    //    NSArray * calendars = self.eventStore.calendars;
    NSArray * calendars = [self.eventStore calendarsForEntityType:EKEntityTypeEvent];
    
    // TODO when iOS 5 support is no longer needed, change the line above by the line below (and a few other places as well)
    // NSArray * calendars = [self.eventStore calendarsForEntityType:EKEntityTypeEvent];
    
    NSMutableArray *finalResults = [[NSMutableArray alloc] initWithCapacity:calendars.count];
    for (EKCalendar *thisCalendar in calendars){
        NSMutableDictionary *entry = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                      thisCalendar.calendarIdentifier, @"id",
                                      thisCalendar.title, @"name",
                                      nil];
        [finalResults addObject:entry];
    }
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray:finalResults];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

- (void)createEventInNamedCalendar:(CDVInvokedUrlCommand*)command {
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar == nil) {
        [self createCalendarWithName:kiOSCalendarName];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not find calendar"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    } else {
        [self createEventWithCalendar:command calendar:calendar];
    }
}

- (void) createCalendarWithName:(NSString*)calendarName
{
    @synchronized(self)
    {
        __block EKCalendar *cal = [self findEKCalendar:calendarName];
        NSString *hexColor=@"#FF0000";
        if(cal==nil)
        {
            [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                
                if(granted)
                {
                    cal = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:self.eventStore];
                    if(cal)
                    {
                        cal.title = calendarName;
                        if (hexColor != (id)[NSNull null]) {
                            UIColor *theColor = [self colorFromHexString:hexColor];
                            cal.CGColor = theColor.CGColor;
                        }
                        cal.source = [self findEKSource];
                        
                        NSLog(@"cal.source: %@", cal.source);
                        
                        // if the user did not allow permission to access the calendar, the error Object will be filled
                        @try {
                            NSError *error=nil;
                            BOOL created = [self.eventStore saveCalendar:cal commit:YES error:&error];
                            if (error == nil) {
                                NSLog(@"created calendar: %@", cal.title);
                                
                            } else {
                                NSLog(@"could not create calendar, error: %@", error.description);
                            }
                            
                        }
                        @catch (NSException *exception) {
                            
                        }
                        @finally {
                            
                        }
                    }
                
                }
                else
                {
                    [self showAlertForCalendarError];
                    NSLog(@"could not create calendar, error: %@", error.description);
                }
                
            }];
        }
        else
        {
            NSLog(@"exists calendar: %@", cal.title);
        }
    }
}

- (void)createEventsInNamedCalendar:(CDVInvokedUrlCommand*)command {
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar == nil) {
        [self createCalendarWithName:kiOSCalendarName];

        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not find calendar"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    } else {
        
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
        dispatch_async(myBackgroundQueue, ^{
            [self createEventsWithCalendar:command calendar:calendar];
        });
    }
}

- (void)listEventsInRange:(CDVInvokedUrlCommand*)command {
}

- (void)createEventWithOptions:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    
    NSString* title      = [options objectForKey:@"title"];
    NSString* location   = [options objectForKey:@"location"];
    NSString* notes      = [options objectForKey:@"notes"];
    NSNumber* startTime  = [options objectForKey:@"startTime"];
    NSNumber* endTime    = [options objectForKey:@"endTime"];
    
    NSDictionary* calOptions = [options objectForKey:@"options"];
    NSNumber* firstReminderMinutes = [calOptions objectForKey:@"firstReminderMinutes"];
    NSNumber* secondReminderMinutes = [calOptions objectForKey:@"secondReminderMinutes"];
    NSString* recurrence = [calOptions objectForKey:@"recurrence"];
    NSString* recurrenceEndTime = [calOptions objectForKey:@"recurrenceEndTime"];
    NSString* calendarName = [calOptions objectForKey:@"calendarName"];
    
    NSTimeInterval _startInterval = [startTime doubleValue] / 1000; // strip millis
    NSDate *myStartDate = [NSDate dateWithTimeIntervalSince1970:_startInterval];
    
    NSTimeInterval _endInterval = [endTime doubleValue] / 1000; // strip millis
    
    EKEvent *myEvent = [EKEvent eventWithEventStore: self.eventStore];
    myEvent.title = title;
    myEvent.location = location;
    myEvent.notes = notes;
    myEvent.startDate = myStartDate;
    
    int duration = _endInterval - _startInterval;
    int moduloDay = duration % (60*60*24);
    if (moduloDay == 0) {
        myEvent.allDay = YES;
        myEvent.endDate = [NSDate dateWithTimeIntervalSince1970:_endInterval-1];
    } else {
        myEvent.endDate = [NSDate dateWithTimeIntervalSince1970:_endInterval];
    }
    
    EKCalendar* calendar = nil;
    if (calendarName == (id)[NSNull null]) {
        calendar = self.eventStore.defaultCalendarForNewEvents;
        if (calendar == nil) {
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"No default calendar found. Is access to the Calendar blocked for this app?"];
            [self.commandDelegate sendPluginResult:result callbackId:callbackId];
            return;
        }
    } else {
        calendar = [self findEKCalendar:calendarName];
        if (calendar == nil) {
            [self createCalendarWithName:kiOSCalendarName];

            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not find calendar"];
            [self.commandDelegate sendPluginResult:result callbackId:callbackId];
            return;
        }
    }
    myEvent.calendar = calendar;
    
    if (firstReminderMinutes != (id)[NSNull null]) {
        EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-1*firstReminderMinutes.intValue*60];
        [myEvent addAlarm:reminder];
    }
    
    if (secondReminderMinutes != (id)[NSNull null]) {
        EKAlarm *reminder = [EKAlarm alarmWithRelativeOffset:-1*secondReminderMinutes.intValue*60];
        [myEvent addAlarm:reminder];
    }
    
    if (recurrence != (id)[NSNull null]) {
        EKRecurrenceRule *rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency: [self toEKRecurrenceFrequency:recurrence]
                                                                              interval: 1
                                                                                   end: nil];
        if (recurrenceEndTime != (id)[NSNull null]) {
            NSTimeInterval _recurrenceEndTimeInterval = [recurrenceEndTime doubleValue] / 1000; // strip millis
            NSDate *myRecurrenceEndDate = [NSDate dateWithTimeIntervalSince1970:_recurrenceEndTimeInterval];
            EKRecurrenceEnd *end = [EKRecurrenceEnd recurrenceEndWithEndDate:myRecurrenceEndDate];
            rule.recurrenceEnd = end;
        }
        [myEvent addRecurrenceRule:rule];
    }
    
    NSError *error = nil;
    [self.eventStore saveEvent:myEvent span:EKSpanThisEvent error:&error];
    
    if (error) {
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    } else {
        NSLog(@"Reached Success");
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }
}

- (void)createEventInteractively:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Method not supported on iOS"];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

-(void)deleteEventFromNamedCalendar:(CDVInvokedUrlCommand*)command {
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar == nil) {
        [self createCalendarWithName:kiOSCalendarName];

        NSString *callbackId = command.callbackId;
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not find calendar"];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    } else {
        [self deleteEventFromCalendar:command calendar:calendar];
    }
}


-(void)deleteEvent:(CDVInvokedUrlCommand*)command {
    EKCalendar* calendar = self.eventStore.defaultCalendarForNewEvents;
    if (calendar == nil) {
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"No default calendar found. Is access to the Calendar blocked for this app?"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    } else {
        [self deleteEventFromCalendar:command calendar: calendar];
    }
}


-(void)modifyEventInNamedCalendar:(CDVInvokedUrlCommand*)command {
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar == nil) {
        [self createCalendarWithName:kiOSCalendarName];

        NSString *callbackId = command.callbackId;
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not find calendar"];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    } else {
        [self modifyEventWithCalendar:command calendar:calendar];
    }
}


-(void)modifyEvent:(CDVInvokedUrlCommand*)command {
    EKCalendar* calendar = self.eventStore.defaultCalendarForNewEvents;
    if (calendar == nil) {
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"No default calendar found. Is access to the Calendar blocked for this app?"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    } else {
        [self modifyEventWithCalendar:command calendar: calendar];
    }
}


-(void)findAllEventsInNamedCalendar:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    EKCalendar* calendar = [self findEKCalendar:calendarName];
    if (calendar == nil) {
        [self createCalendarWithName:kiOSCalendarName];

        NSString *callbackId = command.callbackId;
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not find calendar"];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    } else {
        NSDate* endDate =  [NSDate dateWithTimeIntervalSinceNow:[[NSDate distantFuture] timeIntervalSinceReferenceDate]];
        NSArray *calendarArray = [NSArray arrayWithObject:calendar];
        NSPredicate *fetchCalendarEvents = [eventStore predicateForEventsWithStartDate:[NSDate date] endDate:endDate calendars:calendarArray];
        NSArray *matchingEvents = [eventStore eventsMatchingPredicate:fetchCalendarEvents];
        
        NSMutableArray *finalResults = [[NSMutableArray alloc] initWithCapacity:matchingEvents.count];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        // Stringify the results
        for (EKEvent * event in matchingEvents) {
            NSMutableDictionary *entry = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                          event.title, @"title",
                                          [df stringFromDate:event.startDate], @"startDate",
                                          [df stringFromDate:event.endDate], @"endDate",
                                          nil];
            // optional fields
            if (event.location != nil) {
                [entry setObject:event.location forKey:@"location"];
            }
            if (event.notes != nil) {
                [entry setObject:event.notes forKey:@"message"];
            }
            [finalResults addObject:entry];
        }
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray:finalResults];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }
}


-(void)findEvent:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    
    NSDictionary* options = [command.arguments objectAtIndex:0];
    
    NSString* title      = [options objectForKey:@"title"];
    NSString* location   = [options objectForKey:@"location"];
    NSString* notes      = [options objectForKey:@"notes"];
    NSNumber* startTime  = [options objectForKey:@"startTime"];
    NSNumber* endTime    = [options objectForKey:@"endTime"];
    
    NSTimeInterval _startInterval = [startTime doubleValue] / 1000; // strip millis
    NSDate *myStartDate = [NSDate dateWithTimeIntervalSince1970:_startInterval];
    
    NSTimeInterval _endInterval = [endTime doubleValue] / 1000; // strip millis
    NSDate *myEndDate = [NSDate dateWithTimeIntervalSince1970:_endInterval];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //    EKCalendar* calendar = self.eventStore.defaultCalendarForNewEvents;
    EKCalendar *calendar = [self findEKCalendar:@"My Cal Name"];
    
    if (calendar == nil) {
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"No default calendar found. Is access to the Calendar blocked for this app?"];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    } else {
        NSArray *matchingEvents = [self findEKEventsWithTitle:title location:location notes:notes startDate:myStartDate endDate:myEndDate calendar:calendar];
        
        NSMutableArray *finalResults = [[NSMutableArray alloc] initWithCapacity:matchingEvents.count];
        
        // Stringify the results - Cordova can't deal with Obj-C objects
        for (EKEvent * event in matchingEvents) {
            NSMutableDictionary *entry = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                          event.title, @"title",
                                          event.location, @"location",
                                          event.notes, @"message",
                                          [df stringFromDate:event.startDate], @"startDate",
                                          [df stringFromDate:event.endDate], @"endDate", nil];
            [finalResults addObject:entry];
        }
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray:finalResults];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }
}


-(void)createCalendar:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    NSString* hexColor = [options objectForKey:@"calendarColor"];
    
    __block EKCalendar *cal = [self findEKCalendar:calendarName];
    if (cal == nil) {
        
        [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
           
            if(granted)
            {
                cal = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:self.eventStore];
                cal.title = calendarName;
                if (hexColor != (id)[NSNull null]) {
                    UIColor *theColor = [self colorFromHexString:hexColor];
                    cal.CGColor = theColor.CGColor;
                }
                cal.source = [self findEKSource];
                
                // if the user did not allow permission to access the calendar, the error Object will be filled
                NSError* error;
                BOOL created = [self.eventStore saveCalendar:cal commit:YES error:&error];
                if (error == nil) {
                    NSLog(@"created calendar: %@", cal.title);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
                    });
                    
                } else {
                    NSLog(@"could not create calendar, error: %@", error.description);

                    dispatch_async(dispatch_get_main_queue(), ^{
                        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Calendar could not be created. Is access to the Calendar blocked for this app?"];
                        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
                    });
                }

            }
            else
            {
                
                [self showAlertForCalendarError];
                
                NSLog(@"could not create calendar, error: %@", error.description);
                CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Calendar could not be created. Is access to the Calendar blocked for this app?"];
                [self.commandDelegate sendPluginResult:result callbackId:callbackId];
            }
            
        }];
        
        
    } else {
        // ok, it already exists
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:@"OK, Calendar already exists"];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }
}

// Assumes input like "#00FF00" (#RRGGBB)
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void)deleteCalendar:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    NSDictionary* options = [command.arguments objectAtIndex:0];
    NSString* calendarName = [options objectForKey:@"calendarName"];
    
    EKCalendar *thisCalendar = [self findEKCalendar:calendarName];
    
    if (thisCalendar == nil) {
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    } else {
        NSError *error;
        [eventStore removeCalendar:thisCalendar commit:YES error:&error];
        if (error) {
            NSLog(@"Error in deleteCalendar: %@", error.localizedDescription);
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.userInfo.description];
            [self.commandDelegate sendPluginResult:result callbackId:callbackId];
        } else {
            NSLog(@"Deleted calendar: %@", thisCalendar.title);
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:result callbackId:callbackId];
        }
    }
}


#pragma mark - custom

- (void)startingEventsModification:(CDVInvokedUrlCommand*)command
{
    _isModifying=YES;
}

- (void)eventsModificationEnded:(CDVInvokedUrlCommand*)command
{
    _isModifying=NO;
}


- (void)showAlertForCalendarError
{
    NSString *buttonTitle = nil;
    NSString *message = @"This app is not authorized to use Calendar. Please allow it from Settings and then try again.";
    
    if (UIApplicationOpenSettingsURLString)
    {
        buttonTitle = @"Settings";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Calendar Error"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:buttonTitle, nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}


#pragma mark - 

- (void) dealloc
{
    // Release the queue when it is not needed
//    dispatch_release(myBackgroundQueue);
}



@end





