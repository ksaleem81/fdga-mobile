//
//  FiltersViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/24/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
import ActionSheetPicker_3_0
protocol FilterDataAcordingToThisJson {
    func seletedFilteredKeys(skilCategory:String,skillType:String)
}
class FiltersViewController: UIViewController {

    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!//200
    @IBOutlet weak var parentViewHeightConstraint: NSLayoutConstraint!
    var dataArray = [[String:AnyObject]]()
    var filterCategoryTrack = [[String:AnyObject]]()
    var jsonStringForSkillsTypes = "1"
    var jsonStringForCetegory = ""
    var delegate : FilterDataAcordingToThisJson?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      tableViewHeightConstraint.constant = 0
        // Do any additional setup after loading the view.
        getSkillCategories()
        settingRighMenuBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(FiltersViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getSkillCategories()  {
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetSkillCategoriesAndSkills/\(acadmyId)", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
           self.dataArray =  object as! [[String : AnyObject]]
        }) { (error) in
            print(error!)
//            DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
            self.showInternetError(error: error!)
        }
        
    }

    func settingPickerView() {
     
        var dataAray = [String]()
        filterCategoryTrack = [[String:AnyObject]]()
        for dic in dataArray{
            if let val = dic["SkillCategoryName"] as? String{
                
           
                filterCategoryTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT FILTER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
          
            self.filterBtn.setTitle("\(indexes!)", for: .normal)
            self.jsonStringForCetegory = "\(indexes!)"
            if let skillsArray = self.filterCategoryTrack[values]["Skills"] as? [[String:AnyObject]]{
                if skillsArray.count > 0{
                     self.tableViewHeightConstraint.constant = 200
                    let tv : FilteredOptionViewController = self.childViewControllers[0] as! FilteredOptionViewController
                   tv.dataArray = skillsArray
                    tv.delegate = self
                    tv.viewWillAppear(true)

                }else{
                     self.tableViewHeightConstraint.constant = 0
                    
                }
            }
            
          
        
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: filterBtn)
    
    }
 
    
    @IBAction func filterBtnAction(_ sender: UIButton) {
        settingPickerView()
    }
    @IBAction func applyFitlerBtnAction(_ sender: UIButton) {
       
        if jsonStringForSkillsTypes == "" {
            jsonStringForSkillsTypes = "1"
        }
        
      delegate?.seletedFilteredKeys(skilCategory: jsonStringForCetegory, skillType: jsonStringForSkillsTypes)
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetFilterBtnAction(_ sender: UIButton) {
          delegate?.seletedFilteredKeys(skilCategory: "1", skillType: "1")
        _ = self.navigationController?.popViewController(animated: true)
    }

}
extension FiltersViewController : SelectedSkillsDelegate{
    func seletedSkillsData(selectedData: [[String:AnyObject]], selectedUserId: String) {
        print("yes")
        
        print(selectedData)
      jsonStringForSkillsTypes = ""
        
        for index in selectedData {
            
            if let value  =  index["SkillName"] as? String {
                jsonStringForSkillsTypes = jsonStringForSkillsTypes +  value + ","
            }
        }
        
        if jsonStringForSkillsTypes != "" {
            jsonStringForSkillsTypes = jsonStringForSkillsTypes.substring(to: jsonStringForSkillsTypes.index(before: jsonStringForSkillsTypes.endIndex))
        }
        
        print(jsonStringForSkillsTypes)
        
    }
}


