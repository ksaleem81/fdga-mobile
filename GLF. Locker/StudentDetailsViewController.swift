//
//  StudentDetailsViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/3/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import JYRadarChart
import SideMenu
//import Charts

class StudentDetailsViewController: UIViewController {

    @IBOutlet weak var imageContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userImage: UIImageView!
    var userImageData = UIImage()

    @IBOutlet weak var chartView: UIView!
    

    var studentData = [String:AnyObject]()
    var studentTitle = ""
    @IBOutlet weak var userGraphView: UIView!//PieChartView!
    @IBOutlet weak var notesViewHeightCons: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if currentUserLogin == 4{
       notesViewHeightCons.constant = 0
        }else{
        }
        userImage.image = userImageData
        self.navigationItem.title = studentTitle
        imageContainerHeightConstraint.constant = self.view.frame.size.width / 2
        gettingServerData()
        
        if #available(iOS 13.0, *) {

        }else{
            self.navigationController?.isNavigationBarHidden = false

        }

        settingRighMenuBtn()
        
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(StudentDetailsViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let view1 = self.tabBarController?.view.viewWithTag(999)
        view1?.isHidden = true
        //navigation issue detected
        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
            self.navigationController?.isNavigationBarHidden = false
        }

    }
    func gettingServerData()  {

        var studentId = ""
        if let id = studentData["StudentID"] as? Int{
            studentId = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "Academy/GetStudentSkillGraph/\(studentId)/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
           
            default:
               DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
            }
            
            let data = (object as? [[String:AnyObject]])!
            let chart = JYRadarChart.init(frame: self.chartView.frame)
            self.chartView.superview?.addSubview(DataManager.sharedInstance.gameScoreGraph(chart: chart, data:data,controller: self))
         
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func lessonBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LessonsViewController") as! LessonsViewController
        controller.studentData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func profileBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        controller.studentData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func notesBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "StudentNotesViewController") as! StudentNotesViewController
        controller.studentData = studentData

        self.navigationController?.pushViewController(controller, animated: true)

        
    }
    

}
