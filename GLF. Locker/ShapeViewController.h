//
//  ShapeViewController.h
//  TestingShapes
//
//  Created by Nasir Mehmood on 07/01/2014.
//  Copyright (c) 2014 Nasir Mehmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"
#import "MMDrawerController.h"

@class ShapeViewController;

@protocol ShapeViewControllerDelegate <NSObject>
- (void)shapeViewController:(ShapeViewController *)controller didSelectSavetoLesson:(NSDictionary *)videoInfo;
@end

@interface ShapeViewController : UIViewController<SPUserResizableViewDelegate>
{
    IBOutlet UIView *superView;
    IBOutlet UIButton *btn1 , *btn2, *btn3, *btn4, *btn5, *btn6;
    IBOutlet   UIToolbar *toolBarVw;
    __weak IBOutlet NSLayoutConstraint *botomViewCons;

    int  yOrigin;
    BOOL isHidden;
    BOOL isVPlay;
    int nCurrVal;
    BOOL isViewHidden;
    CGRect imageViewFrame;
    NSMutableArray *shapeImages;
    CGRect landScapeFrame;
    
    BOOL isFrameSet;
    CGFloat newThmbWidth;
    CGFloat xToDrawThmb ;
    UIImage *analysisImage;
    //    NSMutableArray *shapeViewImageA;
    int  stopVideoFunc;
    CGFloat deviceWidth ,  deviceHeight;
    
    //  UIDeviceOrientation orientation;
    NSMutableArray *dicA;
    BOOL isFrameClick ;
    NSString *imgShape;
    int angleVal;
    
    
}

@property int rotationVal;
@property (nonatomic, weak) MMDrawerController *mmDrawerController;
@property (nonatomic) id<ShapeViewControllerDelegate> delegate;


- (id) initWithVideoInfo:(NSDictionary*)videoInfo;

- (void) drawShapeOfType:(ShapeType)shapeType;
- (void) drawShapeOfImage:(UIImage*)image;
-(void)reloadView;

@end
