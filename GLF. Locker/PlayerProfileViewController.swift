//
//  PersonalProfileViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/13/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class PlayerProfileViewController: UIViewController {


    @IBOutlet weak var favouritCoursFld: UITextField!
    @IBOutlet weak var favouritPlayerFld: UITextField!
    var playerData = [String:AnyObject]()
    var playerInfo = [String:AnyObject]()
    var controllerInfo = [String:AnyObject]()
    @IBOutlet weak var dreamTextView: UITextView!
    @IBOutlet weak var missionTextView: UITextView!
    @IBOutlet weak var dropFld1: UITextField!
      @IBOutlet weak var dropFld2: UITextField!
      @IBOutlet weak var dropFld3: UITextField!
      @IBOutlet weak var dropFld4: UITextField!
      @IBOutlet weak var dropFld5: UITextField!
      @IBOutlet weak var dropFld6: UITextField!
    var handiCapsData = [[String:AnyObject]]()
      var golfExperienceData = [[String:AnyObject]]()
      var playingFrequencyData = [[String:AnyObject]]()
    var practiceFrequencyData = [[String:AnyObject]]()
    var selectedHandiCap = ""
     var selectedGolfExpe = ""
     var selectedPlaying = ""
    var selectedPractice = ""
    var hadGolfLesson = "0"
     var rotation = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(playerData)
         getControllerInfo()
         getPlayerInfo()
        // Do any additional setup after loading the view.
        settingRighMenuBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    func settingRighMenuBtn()  {
  
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(PlayerProfileViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    //https://app.glflocker.com/OrbisAppBeta/api/Academy/GetPlayerPlayingInfo/14375
    func getControllerInfo()  {

        NetworkManager.performRequest(type:.get, method: "Student/GetPlayingDetails", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
            }
            let data = object as! [String : AnyObject]
            self.controllerInfo = data
            self.assignInfoToRelevantControl()
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func getPlayerInfo()  {
    
        var playerId = ""
        if currentUserLogin == 4 {
            
            if let id = playerData["Userid"] as? Int{
                playerId = "\(id)"
            }
        }else{
        if let id = playerData["UserID"] as? Int{
            playerId = "\(id)"
            }
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerPlayingInfo/\(playerId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
                
                
            }
            
            let data = object as! [String : AnyObject]
            self.playerInfo = data
            self.fillingPlayerInformation()
        
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    
    func assignInfoToRelevantControl() {
        
        if let handi = controllerInfo["HandiCaps"] as? [[String:AnyObject]]{
            handiCapsData = handi
        }
        
        if let handi = controllerInfo["GolfExperiences"] as? [[String:AnyObject]]{
            golfExperienceData = handi
        }
        if let handi = controllerInfo["PlayingFrequencies"] as? [[String:AnyObject]]{
            playingFrequencyData = handi
        }
        if let handi = controllerInfo["PracticeFrequencies"] as? [[String:AnyObject]]{
            practiceFrequencyData = handi
        }
        
        print(handiCapsData.count,golfExperienceData.count,playingFrequencyData.count,practiceFrequencyData.count)
        
    }

    func fillingPlayerInformation()  {
        
        if let titl = playerInfo["FavouriteCourse"] as? String{
            favouritCoursFld.text = titl
        }
        if let fname = playerInfo["DreamFourBall"] as? String{
            if fname == ""{
            }else{
                dreamTextView.text = fname
            }
        }
        if let fname = playerInfo["MissionStatement"] as? String{
            if fname == ""{
            }else{
                missionTextView.text = fname
            }
        }
    
        if let fname = playerInfo["FavouritePlayer"] as? String{
            favouritPlayerFld.text = fname
        }
    
        if let handi = playerInfo["HandiCapId"] as? Int{
           
            for dic in handiCapsData{
                if let val = dic["HandiCapId"] as? Int{
                
                    if val == handi {
                      dropFld1.text = dic["HandiCapName"] as? String
                    }
                    
                }
                
            }
    }
    
    if let handi = playerInfo["PlayFrequency"] as? Int{
        
        for dic in playingFrequencyData{
            if let val = dic["PlayingFrequencyId"] as? Int{
                
                if val == handi {
                    dropFld5.text = dic["FrequencyName"] as? String
                }
                
            }
        }
    }
    
    if let handi = playerInfo["PracticeFrequency"] as? Int{
        
            
        for dic in practiceFrequencyData{
            if let val = dic["PracticeFrequencyId"] as? Int{
                
                if val == handi {
                    dropFld6.text = dic["PracticeFrequencyName"] as? String
                }
                
            }
        }
    }
    if let handi = playerInfo["PlayingDuration"] as? Int{
        
        
        
        for dic in golfExperienceData{
            if let val = dic["GolfExperienceId"] as? Int{
                
                if val == handi {
                    dropFld4.text = dic["ExperienceDuration"] as? String
                }
                
            }
        }
    }
    
    if let handi = playerInfo["Rotation"] as? String {
        
        if handi == "2" {
            dropFld2.text = "RIGHT"
        }else if handi == "1" {
            dropFld2.text = "LEFT"
        }
    }
    
    if let handi = playerInfo["HadGolfLessons"] as? Int {
        
        if handi == 0 {
            dropFld3.text = "NO"
            hadGolfLesson = "0"
        }else if handi == 1 {
            dropFld3.text = "YES"
            hadGolfLesson = "1"
        }
    }
    
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func saveBtnAction(_ sender: UIButton) {
       submitForm()
    }
    @IBAction func dropBtn1Action(_ sender: UIButton) {
        var dataAray = [String]()
       var handiCapsTrack = [[String:AnyObject]]()
        for dic in handiCapsData{
            if let val = dic["HandiCapName"] as? String{
                
                dataAray.append(val)
                handiCapsTrack.append(dic)
                }
            
            }
        
        ActionSheetStringPicker.show(withTitle: "", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            print(picker,"\n",values,"\n",indexes)
            self.dropFld1.text = "\(indexes!)"
            if let selectedTyp = handiCapsTrack[values]["HandiCapId"] as? Int{
                self.selectedHandiCap = "\(selectedTyp)"
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
        
    }
    @IBAction func dropBtn2Action(_ sender: UIButton) {
        let dataAray = ["Are you left or right handed?","RIGHT","LEFT"]
       
        ActionSheetStringPicker.show(withTitle: "", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
           if values == 0 {
            return
            }
            self.dropFld2.text = "\(indexes!)"
            
            if values == 1 {
                self.rotation = "2"
            }else if values == 2 {
                self.rotation = "1"
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }
    @IBAction func dropBtn3Action(_ sender: UIButton) {
        let dataAray = ["Have you had golf lessons in Past?","YES","NO"]

        ActionSheetStringPicker.show(withTitle: "", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            if values == 0 {
                return
            }
            self.dropFld3.text = "\(indexes!)"
            if values == 1{
                self.hadGolfLesson = "1"
            }else if values == 2 {
                self.hadGolfLesson = "0"
            }
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    @IBAction func dropBtn4Action(_ sender: UIButton) {
        var dataAray = [String]()
        var experienceTrack = [[String:AnyObject]]()
        for dic in golfExperienceData{
            if let val = dic["ExperienceDuration"] as? String{
                
                dataAray.append(val)
                experienceTrack.append(dic)
            }
            
        }
        
        ActionSheetStringPicker.show(withTitle: "", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.dropFld4.text = "\(indexes!)"
            if let selectedTyp = experienceTrack[values]["GolfExperienceId"] as? Int{
                self.selectedGolfExpe = "\(selectedTyp)"
            }
            
            
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    @IBAction func dropBtn5Action(_ sender: UIButton) {
        
        var dataAray = [String]()
        var playingTrack = [[String:AnyObject]]()
        for dic in playingFrequencyData{
            if let val = dic["FrequencyName"] as? String{
                
                dataAray.append(val)
                playingTrack.append(dic)
            }
            
        }
        
        ActionSheetStringPicker.show(withTitle: "", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.dropFld5.text = "\(indexes!)"
            if let selectedTyp = playingTrack[values]["PlayingFrequencyId"] as? Int{
                self.selectedPlaying = "\(selectedTyp)"
            }
            
            
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    @IBAction func dropBtn6Action(_ sender: UIButton) {
        var dataAray = [String]()
        var practiceTrack = [[String:AnyObject]]()
        for dic in practiceFrequencyData{
            if let val = dic["PracticeFrequencyName"] as? String{
                dataAray.append(val)
                practiceTrack.append(dic)
            }
            
        }
        
        ActionSheetStringPicker.show(withTitle: "", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.dropFld6.text = "\(indexes!)"
            if let selectedTyp = practiceTrack[values]["PracticeFrequencyId"] as? Int{
                self.selectedPractice = "\(selectedTyp)"
            }
            
            
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }
    
    func submitForm()  {
        

        var userID1 = ""
        if currentUserLogin == 4 {
            
            if let id = playerData["Userid"] as? Int{
                userID1 = "\(id)"
            }
        }else{
            
            if let user1 = playerData["UserID"] as? Int{
             userID1 = "\(user1)"
            }
        }

        let parameters : [String:String] = ["HandiCapId":selectedHandiCap ,"PlayingDuration":selectedGolfExpe ,"PlayFrequency":selectedPlaying,"HadGolfLessons":hadGolfLesson,"Rotation":rotation ,"PracticeFrequency":selectedPractice,"UserId":userID1 ,"FavouritePlayer":favouritPlayerFld.text! ,"FavouriteCourse":favouritCoursFld.text!,"DreamFourBall":dreamTextView.text!  ,"MissionStatement":missionTextView.text!]
           print(parameters)
        NetworkManager.performRequest(type:.post, method: "Student/UpdatePlayerPalyingInfo", parameter:parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let responce = object as! String
            switch responce {
            case   "-1" :
                
                DataManager.sharedInstance.printAlertMessage(message:"Unable To Complete Order!", view:self)
                return
                
            default: break
            }
            print("success")
            if responce == "1" {
                _ = self.navigationController?.popViewController(animated: true)
                DataManager.sharedInstance.printAlertMessage(message: "Successfully Updated Player Profile Details!", view:UIApplication.getTopestViewController()!)
            }
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    @IBAction func cancelBtnAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }

  

}
