//
//  AddStudentViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/2/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
import ActionSheetPicker_3_0
class AddStudentViewController: UIViewController {

    @IBOutlet weak var userNameFld: UITextField!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var fNameFld: UITextField!
    @IBOutlet weak var lNameFld: UITextField!
    @IBOutlet weak var mobilFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var parentFld: UITextField!
    @IBOutlet weak var dobFld: UITextField!
    @IBOutlet weak var checkBoxViewsCons: NSLayoutConstraint!//make its 0 for terms remove
    var emailBtnFlage = false
    var privacyBtnFlage = false
    var underAgeFlage = false
    var authorizeFlage = false
    @IBOutlet weak var childTermViewHeightCons: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //GDPR
//         self.childTermViewHeightCons.constant = 45

    }
    
    override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(true)
        self.tabBarController?.navigationItem.title = "ADD STUDENT"
        if #available(iOS 11, *) {
            self.navigationController?.isNavigationBarHidden = true
            self.tabBarController?.navigationController?.isNavigationBarHidden = false
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = false
        }
       settingRighMenuBtn()
    }
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AddStudentViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @IBAction func termsBtnAction(_ sender: UIButton) {
        showingTT()
    }

     func showingTT()  {
         
         var titleAcademy = ""
                         if let titl =  DataManager.sharedInstance.currentAcademy()!["Terms"] as? String{
                             titleAcademy = titl
                         }
    

             UIApplication.shared.openURL(NSURL(string:titleAcademy)! as URL)
                         self.parent?.dismiss(animated: true, completion: nil)

     }

    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)}
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var messageString : String = ""
    
    func valiDateRegistrationFields(){
        
        if let text = userNameFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString)  Enter User Name\n"
        }
        
        if let text = fNameFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter First Name\n"
        }
        
        if let text = lNameFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter Last Name\n"
        }
        
        if let text = mobilFld.text , !text.isEmpty{
        }else {
            messageString = "\(messageString) Enter Mobile Number\n"
        }
        
        if let text = emailFld.text , !text.isEmpty{
            if  DataManager.sharedInstance.isValidEmail(self.emailFld.text!){
            }else{
                messageString = "\(messageString) Enter Valid Email\n"
            }
        }else {
            messageString = "\(messageString) Enter Email\n"
        }
        //GDPR
        if privacyBtnFlage{

        }else{
            messageString = messageString + "Please Agree Terms & Condition Check\n\n"

        }
        


    }
    
    
    func refreshingFields(){
       userNameFld.text = ""
       passwordFld.text = ""
       fNameFld.text = ""
       lNameFld.text = ""
       mobilFld.text = ""
       emailFld.text = ""
    }

    //MARK:- buttons Actions
    @IBAction func pdfTermsBtnAction(_ sender: UIButton) {
        var titleAcademy = ""
        if let titl =  DataManager.sharedInstance.currentAcademy()!["PrivacyPolicy"] as? String{
            privacyTermsPDF = titl
        }
//        privacyTermsPDF = "https://\(titleAcademy).glflocker.com/TCandPP/Privacy%20Policy_GLF_2018.pdf"
//        privacyTermsPDF = "https://\(titleAcademy).firstdegree.golf/privacy-policy/"

        UIApplication.shared.openURL(NSURL(string:privacyTermsPDF)! as URL)
        dismiss(animated: true, completion: nil)

        

    }
    @IBAction func ageBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            underAgeFlage = false
            
            view.layoutIfNeeded()
            
            UIView.animate(withDuration: 1.0, animations: {
                self.childTermViewHeightCons.constant = 45
                self.view.layoutIfNeeded()
            })
            
        }else{
            sender.isSelected = true
            underAgeFlage = true
            
            view.layoutIfNeeded()
            
            UIView.animate(withDuration: 1.0, animations: {
                self.childTermViewHeightCons.constant = 168
                self.view.layoutIfNeeded()
            })
            
            
        }
    }
    @IBAction func authorizeBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            authorizeFlage = false
        }else{
            sender.isSelected = true
            authorizeFlage = true
        }
        
    }
   
    @IBAction func emailBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            emailBtnFlage = false

        }else{
            sender.isSelected = true
            emailBtnFlage = true
        }
    }
    @IBAction func conditionsBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            privacyBtnFlage = false

        }else{
            sender.isSelected = true
            privacyBtnFlage = true
        }
    }

    @IBAction func doneBtnAction(_ sender: UIButton) {
        messageString = ""
        valiDateRegistrationFields()
        if messageString.count > 0{
            let name: String = messageString
            let truncated = name.substring(to: name.index(before: name.endIndex))
            
            let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        if currentUserLogin == 4 {
        DataManager.sharedInstance.printAlertMessage(message: "Coach is not Login", view:self)
        }else{
            addStudent()
        }
    }
    @IBAction func cancelBtnAction(_ sender: UIButton) {
    }
    
    func addStudent()  {
        
        var dob = ""
        if let text = dobFld.text , !text.isEmpty{
            dob = dobFld.text!
            dob = DataManager.sharedInstance.getFormatedDateForJsonMonthFirst(date: dob,formate: "dd MMM, yyyy")
        }else {
            dob = DataManager.sharedInstance.getTodayDate()
            dob = DataManager.sharedInstance.getFormatedDateForJsonMonthFirst(date: dob, formate: "yyyy-MM-dd")
        }

        let userNameTxt = userNameFld.text!.trimmingCharacters(in: .whitespacesAndNewlines)

        NetworkManager.performRequest(type:.post, method: "Login/RegisterStudent_native", parameter: ["firstName":fNameFld.text! as AnyObject,"lastName":lNameFld.text! as AnyObject,"DataOfBirth":dob as AnyObject,"WorkEmail":emailFld.text! as AnyObject,"Mobile":mobilFld.text! as AnyObject,"UserName":userNameTxt as AnyObject,"Password":passwordFld.text! as AnyObject,"AcademyID":DataManager.sharedInstance.currentAcademy()?["AcademyID"] as AnyObject!,"HomeTel":"" as AnyObject,"coachId":DataManager.sharedInstance.currentUser()?["Userid"] as AnyObject!,"AgreeTerms": "\(privacyBtnFlage)" as AnyObject!,"RecEmail": "\(emailBtnFlage)" as AnyObject!,"IsUnderEighteen":"\(underAgeFlage)" as AnyObject!,"ParentApproval":"\(authorizeFlage)" as AnyObject!,"ParentName":parentFld.text! as AnyObject,"ChildDob":dob as AnyObject,"IsnNewUser":"1" as AnyObject], view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            // do a different thing
            default:
                break
            }
            
            let data = object as? [[String:AnyObject]]
             print(data)
            if  ((data?.count)! > 0)  {
                
                if  let result = data?[0]["Status"] as? String{
                    if result == "1"{
                        DataManager.sharedInstance.printAlertMessage(message:"Successfully Registered", view:UIApplication.getTopestViewController()!)
                        self.refreshingFields()
                        self.tabBarController?.selectedIndex = 1
                    }else if result == "2"{
                        DataManager.sharedInstance.printAlertMessage(message:"User Already exist!", view:self)
                    }
                }
            }else{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            }
    
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    @IBAction func dobBtnAction(_ sender: UIButton) {
        
        let formator = DateFormatter()
        
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            let title = formator.string(from: date as Date)
            self.dobFld.text = title
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        
        picker?.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        picker?.show()
    }
    
}
