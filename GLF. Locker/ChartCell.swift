//
//  ChartCell.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 22/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class ChartCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var detailView: UIView!
    
    @IBOutlet weak var blackOverlay: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
//    @IBOutlet weak var detailLabel: UILabel!
//    @IBOutlet weak var detailMiddleSeperator: UIImageView!
    @IBOutlet weak var detailBottomSeperator: UIImageView!
    @IBOutlet weak var lesnTypImgwidCons: NSLayoutConstraint!
    @IBOutlet weak var lessonTypeImg: UIImageView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func hideSeperators(_ hide: Bool) {
//        detailMiddleSeperator.isHidden = hide
        detailBottomSeperator.isHidden = hide        
    }
}
