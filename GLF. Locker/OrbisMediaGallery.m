//
//  OrbisVideoLibrary.m
//  TestingShapes
//
//  Created by Nasir Mehmood on 07/01/2014.
//  Copyright (c) 2014 Nasir Mehmood. All rights reserved.
//

#import "OrbisMediaGallery.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AVAsset+VideoOrientation.h"

static OrbisMediaGallery *_orbisMediaGallery=nil;

#define kAppName (NSString*)[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]

@implementation OrbisMediaGallery

+ (OrbisMediaGallery*) sharedGallery
{
    if(!_orbisMediaGallery)
    {
        _orbisMediaGallery=[[OrbisMediaGallery alloc] init];
        _orbisMediaGallery.isCompressingVideo=NO;
    }
    
    return _orbisMediaGallery;
}

+ (OrbisMediaGallery*) sharedGalleryWithDelegate:(id<OrbisMediaGalleryDelegate>) d
{
    OrbisMediaGallery *v=[OrbisMediaGallery sharedGallery];
    v.delegate=d;
    return v;
}

- (NSArray*) getAllVideos
{
    return nil;
}

- (NSArray*) getAllVideoDictionaries
{
    NSFileManager *fileManager = [NSFileManager defaultManager];

//    NSArray *filelist;
//    int count;
//    int i;
//    
//    filelist = [fileManager contentsOfDirectoryAtPath:[self getMediaGalleryFolderPath] error:NULL];
//    count = [filelist count];
//
//    for (i = 0; i < count; i++)
//    {
//        NSData *fileData=[NSData dataWithContentsOfFile:[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:filelist[i]]];
//        NSLog(@"FileName: %@, Size: %lu", filelist[i], (unsigned long)fileData.length);
//        
//    }
    
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSArray *records;
    if([fileManager fileExistsAtPath:plistFilePath])
        records=[NSArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSArray array];
    
    NSMutableArray *recordsToSend=[NSMutableArray array];
    NSString *mediaFolderPath=[self getMediaGalleryFolderPath];
    
    for(NSDictionary *d in records)
    {
        NSMutableDictionary *md=[NSMutableDictionary dictionaryWithDictionary:d];
        [md setObject:[mediaFolderPath stringByAppendingPathComponent:[[d objectForKey:@"videoPath"] lastPathComponent]] forKey:@"videoPath"];
        [md setObject:[mediaFolderPath stringByAppendingPathComponent:[[d objectForKey:@"videoThumbnailPath"] lastPathComponent]] forKey:@"videoThumbnailPath"];
        [md setObject:[d objectForKey:@"duration"] forKey:@"duration"];
        
        if([fileManager fileExistsAtPath:[md objectForKey:@"videoPath"]] && [fileManager fileExistsAtPath:[md objectForKey:@"videoThumbnailPath"]])
        {
            [recordsToSend addObject:md];
        }
        
    }
    
    return recordsToSend;
}

- (NSArray*) getAllPhotoDictionaries
{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    NSString *plistFilePath=[self getPhotoPlistFilePath];
    NSArray *records=nil;
    if([fileManager fileExistsAtPath:plistFilePath])
        records=[NSArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSArray array];
    
    NSMutableArray *recordsToSend=[NSMutableArray array];
    NSString *mediaFolderPath=[self getMediaGalleryFolderPath];
    
    for(NSDictionary *d in records)
    {
        NSMutableDictionary *md=[NSMutableDictionary dictionaryWithDictionary:d];
        [md setObject:[mediaFolderPath stringByAppendingPathComponent:[[d objectForKey:@"photoPath"] lastPathComponent]] forKey:@"photoPath"];

        if([fileManager fileExistsAtPath:[md objectForKey:@"photoPath"]])
        {
            [recordsToSend addObject:md];
        }
        
    }
    
    return recordsToSend;
}


- (NSDictionary*) saveVideoToGalleryAtPath:(NSString*) videoPath
{
    NSString *extensionString=[videoPath pathExtension];
    NSDate *time = [NSDate date];
//    NSDateFormatter* df = [NSDateFormatter new];
//    [df setDateFormat:@"dd-MM-yyyy-hh-mm-ss"];
//    [df setDateFormat:@"ddMMyyyyhhmmss"];
//    NSString *timeString = [df stringFromDate:time];
    NSString *timeString = [NSString stringWithFormat:@"%d", (int)[time timeIntervalSince1970]];//[df stringFromDate:time];
//    NSString *newFileName = [kAppName stringByAppendingFormat:@"-%@.%@", timeString, extensionString];
    NSString *newFileName = [@"GLF" stringByAppendingFormat:@"-%@.%@", timeString, extensionString];
    
    
    NSString *newPath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:newFileName];
    [[NSFileManager defaultManager] copyItemAtPath:videoPath toPath:newPath error:nil];
    
    NSLog(@"%i",[[NSFileManager defaultManager] copyItemAtPath:videoPath toPath:newPath error:nil]);
    
    //HUD.labelText = @"Generating thumbnail...";
    NSString *thumbnailPath=[self generateThumbnailForVideoAtPath:newPath];
    NSDictionary *videoInfo=[self saveVideoPath:newPath thumbnailPath:thumbnailPath];
    
    return videoInfo;
}

- (NSDictionary*) saveVideoToGalleryWithPath:(NSString*) videoPath
{
    NSString *thumbnailPath=[self generateThumbnailForVideoAtPath:videoPath];
    NSDictionary *videoInfo=[self saveVideoPath:videoPath thumbnailPath:thumbnailPath];
    return videoInfo;
}


- (NSString*) getPathForNextLibraryVideo
{
    NSString *extensionString=@"mp4";//[videoPath pathExtension];
    NSDate *time = [NSDate date];
//    NSDateFormatter* df = [NSDateFormatter new];
//    [df setDateFormat:@"dd-MM-yyyy-hh-mm-ss"];
//    [df setDateFormat:@"ddMMyyyyhhmmss"];
    NSString *timeString = [NSString stringWithFormat:@"%d", (int)[time timeIntervalSince1970]];//[df stringFromDate:time];
//    NSString *newFileName = [kAppName stringByAppendingFormat:@"-%@.%@", timeString, extensionString];
    NSString *newFileName = [@"GLF" stringByAppendingFormat:@"-%@.%@", timeString, extensionString];
    
    NSString *newPath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:newFileName];
    [[NSFileManager defaultManager] removeItemAtPath:newPath error:nil];
    
    return newPath;
}



- (NSDictionary*) savePhotoToGalleryAtPath:(NSString*) photoPath
{
    NSString *extensionString=[photoPath pathExtension];
    NSDate *time = [NSDate date];
//    NSDateFormatter* df = [NSDateFormatter new];
//    [df setDateFormat:@"dd-MM-yyyy-hh-mm-ss"];
//    NSString *timeString = [df stringFromDate:time];
    NSString *timeString = [NSString stringWithFormat:@"%d", (int)[time timeIntervalSince1970]];//[df stringFromDate:time];
//    NSString *newFileName = [kAppName stringByAppendingFormat:@"-p%@.%@", timeString, extensionString];
    NSString *newFileName = [@"GLF" stringByAppendingFormat:@"-p%@.%@", timeString, extensionString];
    
    NSString *newPath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:newFileName];
    if([[NSFileManager defaultManager] fileExistsAtPath:photoPath])
        [[NSFileManager defaultManager] copyItemAtPath:photoPath toPath:newPath error:nil];
    
    NSDictionary *imageInfo=[self savePhotoPath:newPath];
    
    return imageInfo;
}


- (NSString*) prepareVideoForUploadingAtPath:(NSString*) filePath
{
    return nil;
}

- (NSString*) generateThumbnailForVideoAtPath:(NSString*) videoPath
{
    NSString *thumbnailPath=nil;
    UIImage *thumbImage=nil;
    NSURL *videoURL=[NSURL fileURLWithPath:videoPath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:videoPath])
    {
        videoPath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:videoPath.lastPathComponent];
    }
    
    NSString *thumbName=[[videoPath lastPathComponent] stringByDeletingPathExtension];
/*
    MPMoviePlayerController *player =[[MPMoviePlayerController alloc] initWithContentURL: videoURL];
    float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
    
    [[MPMusicPlayerController applicationMusicPlayer] setVolume:0.0];
    UIImage *thumbImage=[player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    
*/
    AVAsset *asset = [AVAsset assetWithURL:videoURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imageRef);
    thumbImage = [[UIImage alloc] initWithCGImage:imageRef];
   
    
    float angle=90.0f;
    LBVideoOrientation videoOrientation=[asset videoOrientation];
    switch (videoOrientation) {
        case LBVideoOrientationUp:
            angle=90.0f;
            break;
        case LBVideoOrientationDown:
            angle=-90.0f;
            break;
        case LBVideoOrientationLeft:
            angle=180.0f;
            break;
        case LBVideoOrientationRight:
            angle=0.0f;
            break;
            
        default:
            angle=90.0f;
            break;
    }
    
    NSLog(@"angle %f", angle);
    
    thumbImage = [self rotateImage:thumbImage byDegrees:angle];
    /*
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
  
    
    if (orientation == UIDeviceOrientationPortrait ) {
       // thumbImage=[[UIImage alloc] initWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationRight];
         thumbImage = [self rotateImage:thumbImage toOrientation:UIImageOrientationRight];
    }
    else if (orientation == UIDeviceOrientationLandscapeRight)
    {
    //    thumbImage=[[UIImage alloc] initWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationDown];
       thumbImage = [self rotateImage:thumbImage toOrientation:UIImageOrientationDown];
        
    }
    else if (orientation == UIDeviceOrientationLandscapeLeft)
    {
     //   thumbImage=[[UIImage alloc] initWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationUp];
      thumbImage = [self rotateImage:thumbImage toOrientation:UIImageOrientationUp];
    }

    */
    
    
    CGImageRelease(imageRef); // CGImageRef won't be released by ARC
    
    thumbnailPath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_thumb.jpg", thumbName]];
    
    NSData *thumbImageData=UIImageJPEGRepresentation(thumbImage, 1.0);
    [thumbImageData writeToFile:thumbnailPath atomically:YES];
/* 
    [player stop];
    [[MPMusicPlayerController applicationMusicPlayer] setVolume:volume];
*/
//    UIImageWriteToSavedPhotosAlbum(thumbImage, nil, nil, nil);
    
    return thumbnailPath;
}


- (NSDictionary*) saveOverlay:(UIImage*)overlayImage forVideo:(NSDictionary*)videoInfo
{
    NSString *overlayPath=nil;
    NSArray *existingOverlays=[videoInfo objectForKey:@"overlays"];
    
    NSString *videoName=[[[videoInfo objectForKey:@"videoPath"] lastPathComponent] stringByDeletingPathExtension];

    overlayPath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_overlay%d.png", videoName, (existingOverlays.count+1)]];
    
    NSData *overlayImageData=UIImagePNGRepresentation(overlayImage);
    [overlayImageData writeToFile:overlayPath atomically:YES];
    
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSMutableArray *records=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    NSDictionary *videoInfoToUpdate=[self getVideoInfoWithVideoPath:[videoInfo objectForKey:@"videoPath"]];
    
    if(videoInfoToUpdate)
    {
        [records removeObject:videoInfoToUpdate];
    }
    
    NSMutableArray *newOverlays=[NSMutableArray array];
    [newOverlays addObject:overlayPath];
   
    for(NSString *op in existingOverlays)
    {
        [newOverlays addObject:op];
    }
    
    NSMutableDictionary *newVideoInfo=[NSMutableDictionary dictionary];
    [newVideoInfo setObject:[videoInfo objectForKey:@"duration"] forKey:@"duration"];
    [newVideoInfo setObject:[videoInfo objectForKey:@"videoPath"] forKey:@"videoPath"];
    [newVideoInfo setObject:[videoInfo objectForKey:@"videoThumbnailPath"] forKey:@"videoThumbnailPath"];
    [newVideoInfo setObject:[videoInfo objectForKey:@"analysisScreens"] forKey:@"analysisScreens"];
    [newVideoInfo setObject:newOverlays forKey:@"overlays"];
    
    
    [records addObject:newVideoInfo];
    [records writeToFile:plistFilePath atomically:YES];

    return newVideoInfo;
}

- (void) CGImageWriteToFile:(CGImageRef)image atPath:(NSString*)path type:(CFStringRef)imageType {
    CFURLRef url = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL(url, imageType, 1, NULL);
    CGImageDestinationAddImage(destination, image, nil);
    
    if (!CGImageDestinationFinalize(destination)) {
        NSLog(@"Failed to write image to %@", path);
    }
    
    CFRelease(destination);
    

    
}

- (NSDictionary*) saveAnalysisImage:(CGImageRef)analysisImage atTime:(float)time forVideoAtPath:(NSString*)videoPath
{
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSMutableArray *records=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    NSDictionary *videoInfoToUpdate=nil;//[self getVideoInfoWithVideoPath:[videoInfo objectForKey:@"videoPath"]];
    for(NSDictionary *d in records)
    {
        if([[[d objectForKey:@"videoPath"] lastPathComponent] isEqualToString:videoPath.lastPathComponent])
        {
            videoInfoToUpdate=d;
            break;
        }
    }
    
    if(videoInfoToUpdate)
    {
        [records removeObject:videoInfoToUpdate];
        
        NSArray *existingAnalysisScreens=[videoInfoToUpdate objectForKey:@"analysisScreens"];
        
        NSString *videoName=[[videoPath lastPathComponent] stringByDeletingPathExtension];
        
        NSString *analysisImagePath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_analysisScreen%d.png", videoName, (existingAnalysisScreens.count+1)]];
        
        //    NSData *analysisImageData=UIImageJPEGRepresentation(analysisImage, 1.0);
        //    [analysisImageData writeToFile:analysisImagePath atomically:YES];
        
        [self CGImageWriteToFile:analysisImage atPath:analysisImagePath type:kUTTypePNG];

        //    CGImageWriteToFile(analysisImage, analysisImagePath);
        
        
        NSMutableArray *newAnalysisScreens=[NSMutableArray array];
        for(NSString *op in existingAnalysisScreens)
        {
            [newAnalysisScreens addObject:op];
        }
        
        NSDictionary *d = @{@"analysisScreenPath": analysisImagePath, @"analysisScreenTime": [NSNumber numberWithFloat:time]
                            };
        [newAnalysisScreens addObject:d];
        
        NSMutableDictionary *newVideoInfo=[NSMutableDictionary dictionary];
        [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"duration"] forKey:@"duration"];
        [newVideoInfo setObject:videoPath forKey:@"videoPath"];
        [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"videoThumbnailPath"] forKey:@"videoThumbnailPath"];
        [newVideoInfo setObject:newAnalysisScreens forKey:@"analysisScreens"];
        [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"overlays"] forKey:@"overlays"];
        
        [records addObject:newVideoInfo];
        [records writeToFile:plistFilePath atomically:YES];
        
        return newVideoInfo;
    }
    else
        return nil;
}


- (NSDictionary*) saveAnalysisImages:(NSArray*)analysisImages forVideoAtPath:(NSString*)videoPath
{
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSMutableArray *records=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    NSDictionary *videoInfoToUpdate=nil;//[self getVideoInfoWithVideoPath:[videoInfo objectForKey:@"videoPath"]];
    for(NSDictionary *d in records)
    {
        if([[[d objectForKey:@"videoPath"] lastPathComponent] isEqualToString:videoPath.lastPathComponent])
        {
            videoInfoToUpdate=d;
            break;
        }
    }
    
    if(videoInfoToUpdate)
    {
        [records removeObject:videoInfoToUpdate];
        
//        NSArray *existingAnalysisScreens=[videoInfoToUpdate objectForKey:@"analysisScreens"];
        
        NSMutableArray *newAnalysisScreens=[NSMutableArray array];
//        for(NSString *op in existingAnalysisScreens)
//        {
//            [newAnalysisScreens addObject:op];
//        }
        
        for(NSDictionary *d in analysisImages)
        {
            [newAnalysisScreens addObject:d];
        }
        
        NSMutableDictionary *newVideoInfo=[NSMutableDictionary dictionary];
        [newVideoInfo setObject:videoPath forKey:@"videoPath"];
        [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"duration"] forKey:@"duration"];
        [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"videoThumbnailPath"] forKey:@"videoThumbnailPath"];
        [newVideoInfo setObject:newAnalysisScreens forKey:@"analysisScreens"];
        [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"overlays"] forKey:@"overlays"];
        
        
        [records addObject:newVideoInfo];
        [records writeToFile:plistFilePath atomically:YES];
        
        return newVideoInfo;
    }
    else
        return nil;
}




- (NSDictionary*) saveAnalysisImages:(NSArray*)analysisImages forVideo:(NSDictionary*)videoInfo
{    
    NSString *videoName=[[[videoInfo objectForKey:@"videoPath"] lastPathComponent] stringByDeletingPathExtension];
    
    NSMutableArray *analysisScreenPathArray=[NSMutableArray array];
    for(int i=0; i<analysisImages.count; i++)
    {
        NSString *analysisScreenPath = [[self getMediaGalleryFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_analysisScreen%d.png", videoName, i]];

        NSDictionary *analysisScreenDictionary = [analysisImages objectAtIndex:i];
        UIImage *analysisScreen=[analysisScreenDictionary objectForKey:@"analysisScreen"];
        NSData *analysisScreenData=UIImageJPEGRepresentation(analysisScreen, 0.5);
        [analysisScreenData writeToFile:analysisScreenPath atomically:YES];
        
        NSDictionary *d=@{@"analysisScreenPath": analysisScreenPath, @"analysisScreenTime": [analysisScreenDictionary objectForKey:@"analysisScreenTime"]};
        [analysisScreenPathArray addObject:d];
    }
    
    
    
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSMutableArray *records=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    NSDictionary *videoInfoToUpdate=[self getVideoInfoWithVideoPath:[videoInfo objectForKey:@"videoPath"]];
    
    
    NSMutableDictionary *newVideoInfo=[NSMutableDictionary dictionary];
    [newVideoInfo setObject:[videoInfoToUpdate objectForKey:@"duration"] forKey:@"duration"];
    [newVideoInfo setObject:[videoInfo objectForKey:@"videoPath"] forKey:@"videoPath"];
    [newVideoInfo setObject:[videoInfo objectForKey:@"videoThumbnailPath"] forKey:@"videoThumbnailPath"];
    [newVideoInfo setObject:analysisScreenPathArray forKey:@"analysisScreens"];
    [newVideoInfo setObject:[videoInfo objectForKey:@"overlays"] forKey:@"overlays"];
    
    if(videoInfoToUpdate)
    {
        [records removeObject:videoInfoToUpdate];
    }
    
    [records addObject:newVideoInfo];
    [records writeToFile:plistFilePath atomically:YES];
    
    return newVideoInfo;
}


- (NSArray*) getOverlaysForVideoAtPath:(NSString*)videoPath
{
    NSArray *overlays=nil;
    NSDictionary *videoInfo=[self getVideoInfoWithVideoPath:videoPath];
    overlays=[videoInfo objectForKey:@"overlays"];
    return overlays;
}

- (NSDictionary*) getVideoInfoWithVideoPath:(NSString*)videoPath
{
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSMutableArray *records=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    NSDictionary *videoInfo=nil;
    
    for(NSDictionary *d in records)
    {
        if([[[d objectForKey:@"videoPath"] lastPathComponent] isEqualToString:videoPath.lastPathComponent])
        {
            videoInfo=d;
            break;
        }
    }
    
    return videoInfo;
}


- (BOOL) deleteVideoFromGalleryAtPath:(NSString*) videoPath
{
    NSMutableArray *allVideos=[NSMutableArray arrayWithArray:[self getAllVideoDictionaries]];
    NSDictionary *videoToDelete=nil;
    for(NSDictionary *d in allVideos)
    {
        if([[[d objectForKey:@"videoPath"] lastPathComponent] isEqualToString:videoPath.lastPathComponent])
        {
            videoToDelete=d;
            break;
        }
    }
    
    if(videoToDelete)
    {
        NSFileManager *fileManager=[NSFileManager defaultManager];
        
        NSString *mediaFolderPath=[self getMediaGalleryFolderPath];
        NSString *videoPath=[mediaFolderPath stringByAppendingPathComponent:[[videoToDelete objectForKey:@"videoPath"] lastPathComponent]];
        NSString *videoThumbnailPath=[mediaFolderPath stringByAppendingPathComponent:[[videoToDelete objectForKey:@"videoThumbnailPath"] lastPathComponent]];
        
        if([fileManager fileExistsAtPath:videoPath])
            [fileManager removeItemAtPath:videoPath error:nil];
        if([fileManager fileExistsAtPath:videoThumbnailPath])
            [fileManager removeItemAtPath:videoThumbnailPath error:nil];
        
        NSArray *overlays=[videoToDelete objectForKey:@"overlays"];
        for(NSString *overlayPath in overlays)
        {
            NSString *newOverlayPath=[mediaFolderPath stringByAppendingPathComponent:overlayPath.lastPathComponent];
            if([fileManager fileExistsAtPath:newOverlayPath])
                [fileManager removeItemAtPath:newOverlayPath error:nil];
        }
        
        NSArray *analysisScreens=[videoToDelete objectForKey:@"analysisScreens"];
        for(NSDictionary *analysisScreenDictionary in analysisScreens)
        {
            NSString *analysisScreenPath=[mediaFolderPath stringByAppendingPathComponent:[[analysisScreenDictionary objectForKey:@"analysisScreenPath"] lastPathComponent]];
            if([fileManager fileExistsAtPath:analysisScreenPath])
                [fileManager removeItemAtPath:analysisScreenPath error:nil];
        }
        
        [allVideos removeObject:videoToDelete];
        NSString *plistFilePath=[self getVideoPlistFilePath];
        [allVideos writeToFile:plistFilePath atomically:YES];
        
        return YES;
    }
    else
        return NO;
}

- (BOOL) deletePhotoFromGalleryAtPath:(NSString*) photoPath
{
    NSMutableArray *allPhotos=[NSMutableArray arrayWithArray:[self getAllPhotoDictionaries]];
    NSDictionary *photoToDelete=nil;
    for(NSDictionary *d in allPhotos)
    {
        if([[[d objectForKey:@"photoPath"] lastPathComponent] isEqualToString:photoPath.lastPathComponent])
        {
            photoToDelete=d;
            break;
        }
    }
    
    if(photoToDelete)
    {
        NSString *mediaFolderPath=[self getMediaGalleryFolderPath];
        NSFileManager *fileManager=[NSFileManager defaultManager];
        NSString *newPhotoPath=[mediaFolderPath stringByAppendingPathComponent:[[photoToDelete objectForKey:@"photoPath"] lastPathComponent]];
        if([fileManager fileExistsAtPath:newPhotoPath])
            [fileManager removeItemAtPath:newPhotoPath error:nil];
        
        [allPhotos removeObject:photoToDelete];
        NSString *plistFilePath=[self getPhotoPlistFilePath];
        [allPhotos writeToFile:plistFilePath atomically:YES];
        
        return YES;
    }
    else
        return NO;
}

- (NSDictionary*) saveVideoPath:(NSString*)newPath thumbnailPath:(NSString*)thumbnailPath
{
    AVAsset *_asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:newPath]];
    CMTime assetDuration=_asset.duration;
    Float64 duration=CMTimeGetSeconds(assetDuration);
    
    NSDictionary *d=[NSDictionary dictionaryWithObjects:@[newPath, thumbnailPath, @[], @[], [NSNumber numberWithFloat:duration]] forKeys:@[@"videoPath", @"videoThumbnailPath", @"overlays", @"analysisScreens", @"duration"]];
    NSString *plistFilePath=[self getVideoPlistFilePath];
    NSMutableArray *records;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    NSDictionary *recordToDelete=nil;
    for(NSDictionary *dic in records)
    {
        if([newPath.lastPathComponent isEqualToString:[[dic objectForKey:@"videoPath"] lastPathComponent]])
        {
            recordToDelete=dic;
            break;
        }
    }
    
    if(recordToDelete)
    {
        [records removeObject:recordToDelete];
    }
    
    [records addObject:d];
    [records writeToFile:plistFilePath atomically:YES];
    
    return d;
}

- (NSDictionary*) savePhotoPath:(NSString*)newPath
{
    NSDictionary *d=[NSDictionary dictionaryWithObject:newPath forKey:@"photoPath"];
    NSString *plistFilePath=[self getPhotoPlistFilePath];
    NSMutableArray *records;
    if([[NSFileManager defaultManager] fileExistsAtPath:plistFilePath])
        records=[NSMutableArray arrayWithContentsOfFile:plistFilePath];
    else
        records=[NSMutableArray array];
    
    [records addObject:d];
    [records writeToFile:plistFilePath atomically:YES];
    
    return d;
}



- (NSString*) getVideoPlistFilePath
{
    NSString *filePath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:[self getGalleryVideoPathsFileName]];
    return filePath;
}

- (NSString*) getPhotoPlistFilePath
{
    NSString *filePath=[[self getMediaGalleryFolderPath] stringByAppendingPathComponent:[self getGalleryPhotoPathsFileName]];
    return filePath;
}

- (NSString*) getMediaGalleryFolderPath
{
    NSString *folderPath=[[self getDocumentDirectoryPath] stringByAppendingPathComponent:[self getMediaGalleryFolderName]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return folderPath;
}

- (NSString*) getUploadMediaGalleryFolderPath
{
    NSString *folderPath=[[self getDocumentDirectoryPath] stringByAppendingPathComponent:[self getUploadMediaGalleryFolderName]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return folderPath;
}



- (NSString*) getGalleryVideoPathsFileName
{
    return [kAppName stringByAppendingString:@"VideoPaths.plist"];
}

- (NSString*) getGalleryPhotoPathsFileName
{
    return [kAppName stringByAppendingString:@"PhotoPaths.plist"];
}


- (NSString*) getMediaGalleryFolderName
{
    return [kAppName stringByAppendingString:@"VideoGallery"];
}

- (NSString*) getUploadMediaGalleryFolderName
{
    return [kAppName stringByAppendingString:@"Uploads"];
}



- (NSString*) getDocumentDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

- (UIImage *)rotateImage:(UIImage*)srcImage byDegrees:(CGFloat)degrees
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,srcImage.size.width, srcImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-srcImage.size.width / 2, -srcImage.size.height / 2, srcImage.size.width, srcImage.size.height), [srcImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

- (NSString*) moveVideoToMyLibraryAtPath:(NSString*)filePath
{
    NSString *newPath1=[self getPathForNextLibraryVideo];
    NSString *newPath=[newPath1 stringByDeletingPathExtension];
    newPath=[newPath stringByAppendingPathExtension:filePath.pathExtension];
    
    NSError *error=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:newPath error:&error];
        if(!error)
        {
            //            [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        }
        else
        {
            newPath=filePath;
        }
    }
    return newPath;
}


-(UIImage*) rotateImage:(UIImage*) src toOrientation:(UIImageOrientation)orientation
{
    return [self rotateImage:src byDegrees:90.0f];

    //commented below to silent warning
   /*
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context=(UIGraphicsGetCurrentContext());
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, 90/180*M_PI) ;
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, -90/180*M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 90/180*M_PI);
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    */
    
}


@end
