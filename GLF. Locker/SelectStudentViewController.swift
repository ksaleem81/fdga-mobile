//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu

protocol SelectedStudentDelegate {
     func seletedUserData(dic: NSDictionary,selectedUserId:String)
}

protocol SelectedStudentDelegateForUpload {
    func seletedStudentData(selectedData: [[String:AnyObject]],selectedUserId:String)
}



class SelectStudentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var selectedDataArray = [[String:AnyObject]]()
    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
    var delegate : SelectedStudentDelegate?
    var delegateForUpload : SelectedStudentDelegateForUpload?
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var forVocher = false
    var formediaRecomend = false
    var uploadingToLocker = false
    var fromPlayerMedia = false
  var keyForName = ""
    var previousData = [String:AnyObject]()

    @IBOutlet weak var searchFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(previousData)
        if fromPlayerMedia {
            self.title = "COACH SELECTION"
            keyForName = "CoachFullName"

        }else{
            keyForName = "PlayerName"
        }
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        
        ////GetAllAcademiesOrderByLatLong
     
        searchFld.inputAccessoryView = addToolBar()
       data = originaldata
     
        searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        settingRighMenuBtn()
            tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        if uploadingToLocker || fromPlayerMedia {
            button.setImage(UIImage.init(named: ""), for: UIControlState.normal)
            button.setTitle("DONE", for: .normal)
            button.frame = CGRect.init(x: 0, y: 0, width: 60, height: 40)

        }else{
            button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
            button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        }
        button.addTarget(self, action:#selector(SelectStudentViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        if uploadingToLocker  {
        
            _ = self.navigationController?.popViewController(animated: true)
            
        }else if fromPlayerMedia{
             self.delegateForUpload?.seletedStudentData(selectedData: selectedDataArray,selectedUserId:"")
            _ = self.navigationController?.popViewController(animated: true)

        }else{
            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "\(keyForName) CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        
        print ("filterd = \(data)")
        
        

       self.tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SelectStudentViewController.donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar
    }
    
    @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
       
     print(data.count)
        searchFld.resignFirstResponder()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    //   self.tabBarController?.navigationItem.title  = "STUDENTS"
    
        
        
     
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectStudentTableViewCell", for: indexPath) as! SelectStudentTableViewCell
    
        if let title = data[indexPath.row][keyForName] as? String{
           cell.companyLbl.text = title
        }
        if uploadingToLocker || fromPlayerMedia {
            let k = self.selectedDataArray
            let index = k.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic[keyForName]  as? String, value == cell.companyLbl.text!{
                        cell.radioBtn.isSelected = true
                        return true
                    }
                }
                cell.radioBtn.isSelected = false
                return false
            }

        }
        
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action:#selector(selectCheckBtn(sender:)) , for:.touchUpInside)
               return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    
    }
    
    @objc func selectCheckBtn(sender:UIButton)  {
        
        let indexPath =  IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! SelectStudentTableViewCell
        
        if cell.radioBtn.isSelected {
            
            if uploadingToLocker || fromPlayerMedia {
                cell.radioBtn.isSelected = false
                let k = self.selectedDataArray
                let index = k.index {
                    if let dic = $0 as? Dictionary<String,AnyObject> {
                        if let value = dic[keyForName]  as? String, value == cell.companyLbl.text!{
                            return true
                        }
                    }
                    return false
                }
                
                self.selectedDataArray.remove(at: index!)
                self.delegateForUpload?.seletedStudentData(selectedData: selectedDataArray, selectedUserId: "")

            }
        }else{
            
            cell.radioBtn.isSelected = true
            if formediaRecomend {
                self.delegate?.seletedUserData(dic:data[sender.tag] as NSDictionary,selectedUserId:"")
                _ = self.navigationController?.popViewController(animated: true)
                return
            }else if self.uploadingToLocker || fromPlayerMedia{
                
                selectedDataArray.append(data[indexPath.row])
                self.delegateForUpload?.seletedStudentData(selectedData: selectedDataArray,selectedUserId:"")
                
            }else{
                getUserInfo(tag: cell.radioBtn.tag)
            }
        }
    }
    
    func getUserInfo(tag:Int)  {
       // http://app.glfbeta.com/OrbisWebApi/api/Academy/GetPlayerData/24600
        var playerID = ""
        if let id = data[tag]["PlayerId"] as? Int{
            playerID = "\(id)"
        }
        
        var typeID = ""
        if let typeId = previousData["ProgramTypeId"] as? Int{
            typeID = "\(typeId)"
        }
        print(playerID,typeID)
        NetworkManager.performRequest(type:.get, method: "academy/GetPlayerData/\(playerID)/\(typeID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            var data = object as! [[String : AnyObject]]
            data[0]["PlayerId"] = playerID as AnyObject
            self.delegate?.seletedUserData(dic: data[0] as NSDictionary,selectedUserId:playerID)
            _ = self.navigationController?.popViewController(animated: true)
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

   

}

