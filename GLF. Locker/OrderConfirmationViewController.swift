//
//  OrderConfirmationViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/11/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
class OrderConfirmationViewController: UIViewController {

    @IBOutlet weak var confirmationLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    var forClassBooking = false
   var discountInfoGotOrNot = false
       var titleOfClass = ""
    var detailData = [String:AnyObject]()
    var totalDiscount = 0.0
    var responceString = ""
    var emailId = ""
    var allBookingData = [String:AnyObject]()

    @IBOutlet weak var membershpPriceLbl: UILabel!
    var memberDiscountForClass = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        print(detailData)
        // Do any additional setup after loading the view.
        print(discountInfoGotOrNot)
        settingInfoToViews()
        settingRighMenuBtn()
      
       let newBackButton = UIBarButtonItem(image: UIImage(named: "header_arrow"), landscapeImagePhone: UIImage(named: "header_arrow"), style: .plain, target: self, action: #selector(OrderConfirmationViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    func settingInfoToViews()  {
        //for class confirmation code 01~9687~25.00~0.00#
        emailLbl.text = emailId
        let dateOrignal = responceString.components(separatedBy: "~")
        if dateOrignal.count > 2 {

           // let spaceCount = responceString.characters.filter{$0 == "#"}.count
            
            
            if forClassBooking {
                settingPrices()
//                confirmationLbl.text = "\(dateOrignal[0])"
//                if discountInfoGotOrNot{
//                     priceLbl.text = "Price:£ \(dateOrignal[2])" + "," + "Discount:\(dateOrignal[3])"
//                }else{
//                    priceLbl.text = "Price:£ \(dateOrignal[2])"
//                }
//                if let program = detailData["ProgramTypeName"] as? String{
//                    discountLbl.text = "Classes Booked: " +  titleOfClass //program
//                }
                
            }else{
                //lesson Responce: 38297~30.00~0.00~0.00
                //confirmId~price~discount~membershipDiscount
                //class responce : 01~17458~45.00~0.00#1~17459~4.00~0.00#
                
                if discountInfoGotOrNot{
                    if dateOrignal[2] == "0.00"{
                        discountLbl.isHidden = true
                    }else{
                        discountLbl.text = "Discount:\(dateOrignal[2])"
                        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                            priceLbl.text = "Price:\(dateOrignal[2]) \(currencySign)"
                            
                        }else{
                            priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
                        }
                        return
                    }
                }else{
                    discountLbl.isHidden = true
                }
                //mergingcode
                if let bookType = allBookingData["PaymentModeId"] as? String{
                    
                    if bookType == "5" ||  bookType == "2"{
                        
                        priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
                        discountLbl.isHidden = true
                        membershpPriceLbl.isHidden = true
                        
                    }else{
                        
                       
                        
//                        if isNewUrl {
//
//                            if let price = detailData["Price"] as? AnyObject{
//                                crossingActualPrice(originalPrice: "\(price)" , discPrice: dateOrignal[1] as AnyObject)
//                            }
//
//                        }else{
                            
                            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                                priceLbl.text = "Price:\(dateOrignal[1]) \(currencySign)"
                                
                            }else{
                                priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
                                
                            }
//                        }
                        
                        
                        
                    }
                    
                }
                
                
                confirmationLbl.text = "\(dateOrignal[0])"
                //                priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
            }
//            {
//                //38297~30.00~0.00
//                if discountInfoGotOrNot{
//                         discountLbl.text = "Discount:\(dateOrignal[2])"
//                }else{
//                    discountLbl.isHidden = true
//                }
//
//                confirmationLbl.text = "\(dateOrignal[0])"
//                if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
//                    priceLbl.text = "Price:\(dateOrignal[1]) \(currencySign)"
//
//                }else{
//                    priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
//
//                }
//            }
            
        }

    }
    
    
    func crossingActualPrice(originalPrice:String,discPrice:AnyObject) {
        
        
        if let priceDis = detailData["DiscPrice"] as? AnyObject{
            
            if let actualPrice = detailData["Price"] as? AnyObject{
                
                if "\(actualPrice)" == "\(priceDis)" {
                    
                    
                    
                    if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                        priceLbl.text = "Price:\(originalPrice) \(currencySign)"

                    }else{
                        priceLbl.text = "Price:\(currencySign) \(originalPrice)"
                    }
                    
                    discountLbl.isHidden = true
                    membershpPriceLbl.isHidden = true
                    return
                }
                
            }
            
        }
        
        //bolding left/right
        var priceStr = ""
        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
            priceStr = "\(originalPrice) \(currencySign)"

        }else{
            priceStr = "\(currencySign) \(originalPrice)"
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:priceStr)
        
        //        priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        let cost = NSAttributedString(string: "Price: ")
        attributeString.insert(cost, at: 0)
        priceLbl.attributedText =   attributeString
        priceLbl.textColor = UIColor.red
        if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
            membershpPriceLbl.text = "\(discPrice) \(currencySign)"

        }else{
            membershpPriceLbl.text = "\(currencySign) \(discPrice)"
        }
    }

    
    func settingPrices()  {
        
        
        let forOrder = responceString.components(separatedBy: "~")
        let oneString = responceString.components(separatedBy: "#")
        let spaceCount = responceString.characters.filter{$0 == "#"}.count
        
        var totalAmount = 0.0
        totalDiscount = 0.0
        for i in 0...spaceCount-1 {
            let original = oneString[i].components(separatedBy: "~")
            
            totalAmount = totalAmount +  Double(original[2])!
            totalDiscount = totalDiscount + Double(original[3])!
        }
        
        confirmationLbl.text = "\(forOrder[0])"
        if discountInfoGotOrNot{
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                priceLbl.text = "Price:\(totalAmount) \(currencySign) " + "," + "Discount:\(totalDiscount)"
            }else{
                priceLbl.text = "Price:\(currencySign) \(totalAmount)" + "," + "Discount:\(totalDiscount)"
            }
        }else{
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                priceLbl.text = "Price:\(totalAmount) \(currencySign)"

            }else{
                priceLbl.text = "Price:\(currencySign) \(totalAmount)"
                
            }
        }

        discountLbl.text = "Classes Booked: " +  titleOfClass //program
      

    }
    
    @objc func back(sender: UIBarButtonItem) {
       
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(OrderConfirmationViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) 
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
      _ = self.navigationController?.popToRootViewController(animated: true)
    }

}
