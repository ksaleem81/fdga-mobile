//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class EventsTableViewCellEMG: UITableViewCell {

    @IBOutlet weak var timeLbl: UILabel!
   
   
    @IBOutlet weak var subView1: UIView!
    @IBOutlet weak var subViewBtn1: UIButton!
    @IBOutlet weak var firstSlotLbl: UILabel!
    @IBOutlet weak var thirdSlotLbl: UILabel!
    @IBOutlet weak var forthSlotLbl: UILabel!
    @IBOutlet weak var secondSlotLbl: UILabel!
    @IBOutlet weak var subView2: UIView!
    @IBOutlet weak var subView3: UIView!
    @IBOutlet weak var subView4: UIView!
    @IBOutlet weak var subViewBtn3: UIButton!
    @IBOutlet weak var subViewBtn2: UIButton!
    @IBOutlet weak var subViewBtn4: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
