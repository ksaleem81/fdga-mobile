//
//  DataManager.swift
//  MyAlist
//
//  Created by Apple PC on 08/09/2016.
//  Copyright © 2016 ArhamSoft. All rights reserved.
//

import UIKit
//import Charts
import JYRadarChart
import EventKit
class DataManager: NSObject {

    static let sharedInstance: DataManager = { DataManager() }()
    var typeOfUser = ""
    
    func logout() {
        updateLoginDetails(nil)
    }
        //MARK:- user saving
    func updateLoginDetails (_ dic : NSDictionary?) {
        
        let archiveddata = NSKeyedArchiver.archivedData(withRootObject: dic ?? "" )
        UserDefaults.standard.set(archiveddata, forKey: "loginDetails")
        UserDefaults.standard.synchronize()
        
    }
    
    func currentUser  () -> NSDictionary? {
       return NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "loginDetails") as! Data) as! NSDictionary?
    }
    //MARK:- academy saving
    func currentAcademy  () -> NSDictionary? {
          return NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "academyDetails") as! Data) as! NSDictionary?
        
    }
    func updateAcademyDetails (_ dic : NSDictionary?) {
        
        let archiveddata = NSKeyedArchiver.archivedData(withRootObject: dic ?? "" )
        UserDefaults.standard.set(archiveddata, forKey: "academyDetails")
        UserDefaults.standard.synchronize()
    }
    
    func writeInUserDefaults(key:String , value:String){
        UserDefaults.standard.setValue(value, forKey: key)
    }
    
    func readUserDefaults(key:String) -> String {
        
        let value = UserDefaults.standard.value(forKey: key) as? String
        if  (value != nil){
            return value!
        }else {
            return ""
        }
    }

    //MARK:- login or not
    func isLoggedIn() -> (Bool)  {
        
        if ( currentUser() != nil){
            return (true)
        }
        return (false)
    }
    
    func isValidEmail(_ emailAddress:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: emailAddress){
            return false
        }
        
        return true
    }
    
    func printAlertMessage(message:String,view:UIViewController) {
        
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            return
        }
        alertController.addAction(okAction)
        view.present(alertController, animated: true, completion: nil)
        
    }
    
    func getFormatedDate(date:String,formate:String) ->String{
        
       //crash //if crash than made EEEE and in comlete order line 275 "yyyy/dd/MM"
        let formatter = DateFormatter()
        formatter.dateFormat = formate///this is you want to convert format
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date1 = formatter.date(from: date)
        formatter.dateFormat = "EEE MMM dd , yyyy"
        let timeStamp = formatter.string(from: date1!)
        return timeStamp
        
    }
    
    func getFormatedDateGivenPAttern(date:String,formate:String,formateReturn:String) ->String{
        //crash //if crash than made EEEE and in comlete order line 275 "yyyy/dd/MM"
        let formatter = DateFormatter()
        formatter.dateFormat = formate///this is you want to convert format
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date1 = formatter.date(from: date)
        formatter.dateFormat = formateReturn
        let timeStamp = formatter.string(from: date1!)
        return timeStamp
    }
    
    func getDayOfWeek(_ today:String,dateFormater:String) -> Int {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = dateFormater
        guard let todayDate = formatter.date(from: today) else { return 1 }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }

    
    func getFormatedDateWithoutWeekDay(date:String,formate:String) ->String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = formate///this is you want to convert format
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date1 = formatter.date(from: date)
        formatter.dateFormat = "dd MMM yyyy"
        let timeStamp = formatter.string(from: date1!)
        return timeStamp
        
    }
    
    func removeTimeFromDate(date:String,character:String) -> String {
       return date.components(separatedBy: character)[0]
    }
    
    func getTodayDate() ->String{
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let TodayDate = formatter.string(from: date)
        return TodayDate
        
    }
    
    func getFormatedDateForJson(date:String,formate:String) ->String{
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = formate///this is you want to convert format
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date1 = formatter.date(from: date)
        if globalDateFormate == "dd/MM/yyyy" {
            formatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            formatter.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }

        formatter.dateFormat = "yyyy-MM-dd"
        let timeStamp = formatter.string(from: date1!)
        return timeStamp
    }
    
    func getFormatedDateForJsonMonthFirst(date:String,formate:String) ->String{
    
        let formatter = DateFormatter()
        formatter.dateFormat = formate///this is you want to convert format
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date1 = formatter.date(from: date)
        formatter.dateFormat = "MM-dd-yyyy"
        let timeStamp = formatter.string(from: date1!)
        return timeStamp
    }
    
    func userAlreadyExist(kUsernameKey: String) -> Bool {
        
        return UserDefaults.standard.object(forKey: kUsernameKey) != nil
    }
    
    func differenceBetweenToTimeStrings(startTime:String,endTime:String)-> Int  {
        
        let dateFormatter = DateFormatter()
        if globalDateFormate == "dd/MM/yyyy" {
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }
        
        //            localechanges
        if is12Houre == "true"{
            dateFormatter.dateFormat = "hh:mm a"
        }else{
            dateFormatter.dateFormat = "HH:mm"
        }
//        dateFormatter.dateFormat = "HH:mm"
        let startdate = dateFormatter.date(from:startTime)
        let enddate = dateFormatter.date(from:endTime)
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.month, .day, .hour, .minute, .second]
        formatter.maximumUnitCount = 2
        let timeInterval = Int((enddate?.timeIntervalSince(startdate!))!)
         return timeInterval / 60
    }
    
    func differenceBetweenTo(startTime:String,endTime:String)-> Int  {
        
        let dateFormatter = DateFormatter()
        //        localechanges
        if is12Houre == "true"{
            dateFormatter.dateFormat = "h:mm a"
        }else{
            dateFormatter.dateFormat = "HH:mm:ss"
        }
        
        if globalDateFormate == "dd/MM/yyyy" {
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale!
        }else if globalDateFormate == "MM/dd/yyyy"{
            dateFormatter.locale = NSLocale.init(localeIdentifier: "en_GB") as Locale!
        }
        let startdate = dateFormatter.date(from:startTime)
        let enddate = dateFormatter.date(from:endTime)
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.month, .day, .hour, .minute, .second]
        formatter.maximumUnitCount = 2
        let timeInterval = Int((enddate?.timeIntervalSince(startdate!))!)
        // let timeDifference = formatter.string(from: startdate!, to: enddate!)
        return timeInterval / 60
    }
    
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
        
    }


    func gameScoreGraph(chart:JYRadarChart,data:[[String:AnyObject]],controller:UIViewController) -> JYRadarChart {
        
//        if data.count < 1 {
//            let label2Val = UILabel.init(frame: CGRect(x:chart.frame.size.width/2-30,y:chart.frame.size.height/2-12,width:60 , height:25))
//
//            label2Val.textAlignment = .
//            center
//            label2Val.font = UIFont(name: "Helvetica-Bold", size: 12)
//            label2Val.textColor = UIColor.black
//            label2Val.text = "No Data"
//            chart.addSubview(label2Val)
//
//              return chart
//            }
        var arrayOfNames = [""]
        var arrayOfValues = [[String]]()
        arrayOfNames.removeAll()
        arrayOfValues.append([])
        var totalGainedScore = 0.0
        var totalScore = 0.0
        for index in data{
            
            if let name = index["SkillName"] as? String{
                arrayOfNames.append(name)
            }
            
            if let name = index["ObtainedScore"] as? String{
                arrayOfValues[0].append(name)
                totalGainedScore = totalGainedScore + Double(name)!
            }
            
            if let score = index["TotalScore"] as? String{
               totalScore = totalScore  + Double(score)!
            }
        }
//        print(chart.frame)
        var percent = 0

        if totalScore == 0.0 || totalGainedScore == 0.0{
            
        }else{
             percent = Int(round((totalGainedScore / totalScore ) * 100))

        }
        chart.minValue = 0
        chart.maxValue = 10
        chart.r = 40

        if controller.isKind(of: MyGamesViewController.self) {
                   chart.r = 70
        }
        chart.sizeToFit()
        chart.backgroundColor = UIColor.init(red: 220/255, green: 220/255, blue: 0/255, alpha: 1.0)
        let label2Val = UILabel.init(frame: CGRect(x:chart.frame.size.width/2-30,y:chart.frame.size.height/2-12,width:60 , height:25))
        
        label2Val.textAlignment = .
        center
        label2Val.font = UIFont(name: "Helvetica-Bold", size: 12)
        label2Val.textColor = UIColor.black
        label2Val.text = "\(percent)%"
        chart.addSubview(label2Val)
        chart.backgroundFillColor = UIColor.init(red: 220/255, green: 220/255, blue: 0/255, alpha: 1.0)
        chart.dataSeries =  arrayOfValues
        chart.attributes = arrayOfNames

        return chart
    }
    
    //MARK:- deleting account from switch acounts data array
    func deletingAccountItem(keyName:String,sender:Int) -> [[String:AnyObject]] {
        
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: keyName){
            var cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: keyName) as! Data)) as? [[String:AnyObject]])!
            cart.remove(at: sender)
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: keyName)
            return cart
            
        }
        
        return [[String:AnyObject]]()
    }
    
    func deletingAllAccounts(cartKey:String)  {
        
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: cartKey){
            if let loadedCart =  NSKeyedUnarchiver.unarchiveObject(with: (UserDefaults.standard.object(forKey: cartKey) as? Data)!) as? [[String:Any]]{
                var allAcounts =  loadedCart as [[String : Any]] as [[String : AnyObject]]
                allAcounts.removeAll()
                let archiveddata = NSKeyedArchiver.archivedData(withRootObject: allAcounts )
                UserDefaults.standard.set(archiveddata, forKey: cartKey)
            }
        }
        
    }

    
    func creatingCalender() {
        
        // Create an Event Store instance
        let eventStore = EKEventStore();
        
        let newCalendar = EKCalendar(for: .event, eventStore: eventStore)
                // Probably want to prevent someone from saving a calendar
                // if they don't type in a name...
                newCalendar.title = kiOSCalendarName
//                print(kiOSCalendarName)
                // Access list of available sources from the Event Store
                let sourcesInEventStore = eventStore.sources

                var sourceAcess : EKSource? = nil
                for  source in sourcesInEventStore{
                    if (source.sourceType == .calDAV && source.title == "iCloud") {
                      sourceAcess   = source
                        break
                    }
                    
                }
                
                if sourceAcess?.sourceType == nil   {
                for  source in sourcesInEventStore{
                if (source.sourceType == .local) {
                    sourceAcess = source
                    break
                        }
                    }
                }
        
        if sourceAcess?.sourceType == nil {

            return

        }
        
                 newCalendar.source = sourceAcess!
                do {
                    let calenders =  eventStore.calendars(for: .event)
                    for singleCalender in calenders {
                        if singleCalender.title == kiOSCalendarName {
                            try eventStore.removeCalendar(singleCalender, commit: true)
                        }
                    }
                    UserDefaults.standard.set(newCalendar.calendarIdentifier, forKey: calenderIdentifier)//EventTrackerPrimaryCalendar
                    try eventStore.saveCalendar(newCalendar, commit: true)
                    
                } catch {
                    
                    let alert = UIAlertController(title: "Calendar could not save", message: (error as NSError).localizedDescription, preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(OKAction)
                    UIApplication.getTopestViewController()?.present(alert, animated: true, completion: nil)
                    
                }
    }

    func detectIsPuttingLab(key:String) -> String {
        let defaults = UserDefaults.standard
        var IsPuttingLab1 =  "false"
        if let IsPuttingLab2 = defaults.object(forKey: key) as? String{
            if IsPuttingLab2 == "YES"{
                IsPuttingLab1 =    "true"
            }else{
                IsPuttingLab1 =    "false"
            }
        }
        return IsPuttingLab1
    }

    //MARK:- synching events in calender
    func synchScheduleInCalendar(eventsArray:[[String:AnyObject]],view:UIView)  {
//        print(eventsArray)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let eventStore : EKEventStore = EKEventStore()
        let hud = MBProgressHUD.showAdded(to:view, animated:true)
        hud?.mode = MBProgressHUDModeIndeterminate;
        hud?.labelText = "synching calendar events..."
        eventStore.requestAccess(to: .event, completion: {
            (granted, error) in
            if (granted) && (error == nil) {

                if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: calenderIdentifier){
                }else{
                    self.creatingCalender()
                }
                
                 self.removeAllEvents()
                
            if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: calenderIdentifier){
                
                for event1  in eventsArray {
                  let calendarForEvent = eventStore.calendar(withIdentifier: UserDefaults.standard.value(forKey: calenderIdentifier) as! String)
                    let event:EKEvent = EKEvent(eventStore: eventStore)
                    if calendarForEvent != nil{
                    }else{
                        MBProgressHUD.hideAllHUDs(for: UIApplication.getTopestViewController()!.view!, animated: true )
                        return
                    }
                    event.calendar = calendarForEvent!
                    var titleOfEvent = ""
                    if  let title = event1["PlayerName"] as? String {
                        titleOfEvent = title
                    }
                    
                    if  let title = event1["ProgramTypeName"] as? String {
                        titleOfEvent = titleOfEvent + "-" + title
                    }
                    
                    if  let title = event1["LessonId"] as? String {
                     titleOfEvent = titleOfEvent + "-" + "\(title)"
                    }

                    event.title = titleOfEvent
                    if  let title = event1["StartDate"] as? String {
                        let dateOrignal = title.components(separatedBy: "T")
                        if dateOrignal.count > 0 {
                            
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
                            let dateTime = dateOrignal[0] + " " + dateOrignal[1]
                            let date1 = formatter.date(from:dateTime )
                            formatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
                            if date1 != nil {
                                 event.startDate = date1!
                            }else{
                                continue
                            }
                           
                        }
                    }
                    
                    if  let title = event1["EndDate"] as? String {
                        
                        let dateOrignal = title.components(separatedBy: "T")
                        if dateOrignal.count > 0 {
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //  formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
                            let dateTime = dateOrignal[0] + " " + dateOrignal[1]
                            let date1 = formatter.date(from:dateTime)
                            formatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
                            if date1 != nil {
                                event.endDate = date1!
                            }else{
                                continue
                            }
                        }
                    }
                    
                    //check if event exhist
                    let startDate = event.startDate
                    let endDate = event.endDate
                    let predicate = eventStore.predicateForEvents(withStart: startDate!, end: endDate!, calendars: [calendarForEvent!])
                    let existingEvents = eventStore.events(matching: predicate)
//                    print(existingEvents.count)
                
                    //only uncomment below if event redunt
                    var flageForRedundancy = false
                    for singleEvent in existingEvents {
                        if singleEvent.title == event.title && singleEvent.startDate == event.startDate {
                            // Event exist
                            flageForRedundancy = true
                            continue
                        }
                    }
                    
                    if flageForRedundancy{
                        continue
                    }else{
                    }
                
                    _ =  try?eventStore.save(event, span: .thisEvent, commit: true)
                    
                }
                appDelegate.synchingInProgress = false
                }
                _ = DispatchQueue.main.sync {
                    MBProgressHUD.hideAllHUDs(for: UIApplication.getTopestViewController()!.view!, animated: true )
                }
            }else{
                MBProgressHUD.hideAllHUDs(for: UIApplication.getTopestViewController()!.view!, animated: true )
            }
        })
    }
    
    func removeAllEvents()  {
        
        let eventStore : EKEventStore = EKEventStore()
        let startDate =  NSDate().addingTimeInterval(-60*60*24*(100))//(-120*60*24*(100))
        let endDate = NSDate().addingTimeInterval(60*60*24*60)//(120*60*24*60)//
        
    if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: calenderIdentifier){
        
        let calendarForEvent = eventStore.calendar(withIdentifier: UserDefaults.standard.value(forKey: calenderIdentifier) as! String)

        if calendarForEvent != nil {
            
        }else{
            return
        }
        
        let predicate2 = eventStore.predicateForEvents(withStart: startDate as Date, end: endDate as Date, calendars: [calendarForEvent!])
        
        let eventsForDate = eventStore.events(matching: predicate2) as [EKEvent]!
        
        if (eventsForDate?.count)! > 0 {
            for i in eventsForDate! {

                if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: calenderIdentifier){
             let calendarForEvent =  eventStore.calendar(withIdentifier: UserDefaults.standard.value(forKey: calenderIdentifier) as! String)
                i.calendar = calendarForEvent!
                    _ =  try?eventStore.remove(i, span: .thisEvent)
                }
            }
            
                }
        }
}
    
     func getTargetName() -> String {
        
//        return Bundle.main.infoDictionary?["CFBundleName"] as! String
        return "FDGS"
    }
    
}


public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

