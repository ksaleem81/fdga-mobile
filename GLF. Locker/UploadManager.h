//
//  UploadManager.h
//  sibme
//
//  Created by Nasir Mehmood on 3/21/13.
//
//

#import <Foundation/Foundation.h>
#import "UploadRequest.h"


@interface UploadManager : NSObject<UploadRequestDelgate,NSURLSessionTaskDelegate>

@property (nonatomic) BOOL isUploading;
@property (nonatomic, strong) NSMutableDictionary *uploadRequestsDictionary;


+ (UploadManager*) sharedManager;

- (BOOL) isUploading;

- (void) startPendingUploads;
- (void) startUploadingMediaWithDictionary:(NSDictionary*) options;
- (void) resumeUploadingMediaWithDictionary:(NSMutableDictionary*) options;
- (void) stopUploadingMediaWithDictionary:(NSMutableDictionary*) options;
- (void) cancelUploadingMediaWithDictionary:(NSMutableDictionary*) options;

- (NSArray*)getRecentUploadsList;
- (int) getRequestsCount;

//- (NSString*) saveVideoForUploadingAtPath:(NSString*)videoPath;
- (void) truncateFile;

-(void)requestWithMediaInfo:(NSDictionary*)mediaInfo didFailWithError:(NSError *)error;
-(void)requestWithMediaInfo:(NSDictionary*)mediaInfo didCompleteWithResponse:(NSHTTPURLResponse *)response responseData:(NSData*)responseData;
-(void)requestWithMediaInfo:(NSDictionary*)mediaInfo didSendData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite;

- (void) saveInfoOfAllUploadingVideos;

@end







