//
//  PastViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/9/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu

class ProfileViewController: UIViewController {

    @IBOutlet weak var studentLbl: UILabel!
    @IBOutlet weak var lessonDateLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    var durationId = ""
    var durationsArray:[[String:AnyObject]] = [[String:AnyObject]]()
    var usersDataArray = [[String:AnyObject]]()
    var delegatedDictionary = [String:AnyObject]()
    var previousData = [String:AnyObject]()
    var currentPlayerSelectedId = ""
    var durationTrackArray = [[String:AnyObject]]()
    var selectedDuration = 0
    var studentData = [String:AnyObject]()
    @IBOutlet weak var notificationViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var emailViewHeightCons: NSLayoutConstraint!
    var emailBtnFlag = false
    override func viewDidLoad() {
        super.viewDidLoad()

        print(studentData)

      emailViewHeightCons.constant = 0
      settingRighMenuBtn()
      gettingGDPStatus()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }
    
    func gettingGDPStatus()  {


        var userIdKey = ""
        if currentUserLogin == 4{
            userIdKey =  "Userid"
        }else{
            userIdKey =  "UserID"
        }
        
        var userId = ""
        if let id = studentData[userIdKey] as? Int{
            userId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetPlayerGDPR/\(userId)", parameter:nil, view: UIApplication.getTopestViewController()?.view, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            case _ as [String:AnyObject]:
                
                break
            default:
                break
            }
            
            let object2 = object as! [String : AnyObject]
            
            if let id = object2["RecEmail"] as? Bool,id == true{
                self.emailBtnFlag = true
                self.emailBtn.isSelected = true
                
            }else{
               self.emailBtnFlag = false
                self.emailBtn.isSelected = false
                
            }

            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(ProfileViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func personalDetailBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController") as! PersonalProfileViewController
        controller.studentData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func playerProfileBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlayerProfileViewController") as! PlayerProfileViewController
        controller.playerData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func medicalBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MedicalProfileViewController") as! MedicalProfileViewController
        controller.playerData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func playerCoachesBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlayerCoachesViewController") as! PlayerCoachesViewController
        controller.studentData = studentData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func notificationsBtnAction(_ sender: UIButton) {
        
        
        if sender.isSelected {
            
            sender.isSelected = false
            view.layoutIfNeeded()
            UIView.animate(withDuration: 1.0, animations: {
                self.emailViewHeightCons.constant = 0
                self.view.layoutIfNeeded()
            })
            
        }else{
            sender.isSelected = true
            view.layoutIfNeeded()
            UIView.animate(withDuration: 1.0, animations: {
                self.emailViewHeightCons.constant = 85
                self.view.layoutIfNeeded()
            })
        }
        
    }
    
    @IBAction func emailCheckBtnAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            emailBtnFlag = false
        }else{
            sender.isSelected = true
            emailBtnFlag = true
        }

        
    }
    
    @IBAction func updateEmailBtnAction(_ sender: UIButton) {
        
        self.udateEmailNotification()
    }
    
    
    func udateEmailNotification()  {
        
        var userIdKey = ""
        if currentUserLogin == 4{
            userIdKey =  "Userid"
            
        }else{
            userIdKey =  "UserID"
            
        }
        var userId = ""
        
        if let id = studentData[userIdKey] as? Int{
            userId = "\(id)"
        }

        let params = ["UserID":userId,"RecEmail":"\(emailBtnFlag)","AgreeTerms":"\(true)","RecSMS":"\(false)"] as [String : Any]
        print(params)
        NetworkManager.performRequest(type:.post, method: "Academy/UpdatePlayerGDPR", parameter:params as [String : AnyObject]?, view: UIApplication.getTopestViewController()?.view, onSuccess: { (object) in
                print(object!)
                switch object {
                case _ as NSNull:

                    DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                    return
                    
                case _ as [String:AnyObject]:
                    DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                    return
                case _ as [[String:AnyObject]]: break
                    
                default:
                    break
                }
            
                if  let flage = object as? Bool {
                    if  flage {
                        DataManager.sharedInstance.printAlertMessage(message: "Successfully Updated", view: UIApplication.getTopestViewController()!)
                        
                    }else{
                        
                        DataManager.sharedInstance.printAlertMessage(message: "Unable to Update GDPR", view: UIApplication.getTopestViewController()!)
                    }
                    
                }
            
            
            }) { (error) in
                print(error!)
                self.showInternetError(error: error!)
            }
        }

    
}
