//
//  AllBookingViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 2/6/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SDWebImage
import SideMenu
class AllBookingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var programTypBtn: UIButton!
    @IBOutlet weak var fromBtn: UIButton!
    @IBOutlet weak var toBtn: UIButton!
    var paymentsMethodsArray = [[String:AnyObject]]()
    var filterPaymentTrack = [[String:AnyObject]]()
    var selectedPaymentTypeId = ""
    var selectedTypeId = "0"
   
    @IBOutlet weak var typeBtnView: UIView!
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: true)

        self.typeBtnView.layer.borderWidth = 1.0
        self.toBtn.layer.borderWidth = 1.0
        self.fromBtn.layer.borderWidth = 1.0
        
        self.typeBtnView.layer.borderColor = UIColor.init(red: 184/255, green: 184/255, blue: 184/255, alpha: 1.0).cgColor
        self.toBtn.layer.borderColor = UIColor.init(red: 184/255, green: 184/255, blue: 184/255, alpha: 1.0).cgColor
        self.fromBtn.layer.borderColor = UIColor.init(red: 184/255, green: 184/255, blue: 184/255, alpha: 1.0).cgColor
        self.programTypBtn.setTitle("ALL", for: .normal)
        getCoachBookings()
        getPaymentMethods()
        
        settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AllBookingViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- server data fetching
    func getCoachBookings()  {
        
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }
        //localechanges
        
        let parameters = "\(academyID)/\(coachId)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetCoachRecentBookings/\(parameters)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            default:
                break
            }
            
            self.data = (object as? [[String:AnyObject]])!
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    
    func getPaymentMethods()  {
        
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetAcademySubPayments/\(academyID)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            default:
                break
            }
            
            self.paymentsMethodsArray = (object as? [[String:AnyObject]])!
    
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "AllBookingTableViewCell", for: indexPath) as! AllBookingTableViewCell
                if let title = data[indexPath.row]["Name"] as? String{
                    cell.programTitleLbl.text = title
                }
        if let title = data[indexPath.row]["LessonName"] as? String{
            cell.programSubTitleLbl.text = title
        }
        
        if let title = data[indexPath.row]["LessonPrice"] as? Double{
            let roundedString = String(format: "%.2f", title)
            if let curencyPosition =  DataManager.sharedInstance.currentAcademy()!["IsSymbolPlacementRight"] as? Bool,curencyPosition == true{
                cell.priceLbl.text =  "\(roundedString)" + currencySign

            }else{
                cell.priceLbl.text = currencySign + "\(roundedString)"
                
            }
        }
        
        if let title = data[indexPath.row]["Duration"] as? String{
            cell.durationLbl.text = title
        }
        if let title = data[indexPath.row]["StartTime"] as? String{
            //            localechanges
            if is12Houre == "true"{
                cell.startTimeLbl.text = title
            }else{
                if (title.count) > 5 {
                    cell.startTimeLbl.text = String(title.dropLast(3))
                }else{
                    cell.startTimeLbl.text = title
                }
            }
           
        }
        cell.lessonDateLbl.text = "LESSON DATE:"
        if let date = data[indexPath.row]["LessonDate"] as? String{
            
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                cell.lessonDateValueLbl.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
            }
            
        }
        if let date = data[indexPath.row]["BookedDate"] as? String{
            
            let dateOrignal = date.components(separatedBy: "T")
            if dateOrignal.count > 0 {
                cell.bookingValueLbl.text =  DataManager.sharedInstance.getFormatedDate(date:dateOrignal[0],formate:"yyyy-MM-dd")
            }
            
        }
        if let title = data[indexPath.row]["PaymentType"] as? String{
               cell.paymentTypLbl.text = title
            if title == "Voucher" {
                cell.checkBtn.isUserInteractionEnabled = false
            }else  {
                 cell.checkBtn.isUserInteractionEnabled = true
            }

        }
        if let title = data[indexPath.row]["PaidStatus"] as? String{
            if title == "Paid" {
                cell.checkBtn.isSelected = true
            }else  {
                cell.checkBtn.isSelected = false
            }
        }
        //
        var fileUrl = ""
        if let url = data[indexPath.row]["LessonImage"] as? String {
            fileUrl = url
        }
        fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
        fileUrl = baseUrlForVideo + fileUrl
        cell.programImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
            
            DispatchQueue.main.async (execute: {
 
                if let _ = image{
                    cell.programImage.image = image;
                }
                else{
                    cell.programImage.image = UIImage(named:"photoNot")
                    
                }
            });
            
        })

        cell.checkBtn.tag = indexPath.row
        cell.editBtn.tag = indexPath.row
        cell.checkBtn.addTarget(self, action: #selector(AllBookingViewController.checkBtnAction(btn:)), for: .touchUpInside)
          cell.editBtn.addTarget(self, action: #selector(AllBookingViewController.editBtnAction(btn:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
   // MARK:- cell buttons actions
    @objc func checkBtnAction(btn:UIButton)  {
        var lessonId = ""
        if let lessonId1 = data[btn.tag]["LessonID"] as? Int{
            lessonId = "\(lessonId1)"
        }
        
        if btn.isSelected {
         self.markAsPaidOrNot(status: false, lessonID: lessonId, statusFlag: "0")
        }else{
             print("false")
            self.presentTableViewAsPopOver(sender: btn)
        //    self.markAsPaidOrNot(status: true, lessonID: lessonId, statusFlag: "1")
        }
    }
    
    
    @objc func editBtnAction(btn:UIButton)  {
        
        var lessonId = ""
        if let lessonId1 = data[btn.tag]["LessonID"] as? Int{
            lessonId = "\(lessonId1)"
        }
        let alert = UIAlertController(title: "Price", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "cancel", style: UIAlertActionStyle.cancel, handler:nil))
        alert.addTextField { (configurationTextField) in
            //configure here your textfield
            configurationTextField.textColor = UIColor.black
            configurationTextField.keyboardType = .decimalPad
            if let price = self.data[btn.tag]["LessonPrice"] as? Double{
                configurationTextField.text = "\(price)"
            }
        }
        alert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
        
            if let textField = alert.textFields?.first {
                if textField.text! != ""{
                    self.updateLessonPrice(lessonID: lessonId, price: textField.text!)
                }
              
            }
        }))
        self.present(alert, animated: true, completion: {
     
        })
    }
    // MARK:- Controller buttons actions
    @IBAction func programTypeBtnAction(_ sender: UIButton) {
        
         var dataAray = [String]()
         dataAray = ["All","Lessons","Classes"]
        ActionSheetStringPicker.show(withTitle: "SELECT TYPE", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
        
            self.programTypBtn.setTitle("\(indexes!)", for: .normal)
        
            if "\(indexes!)" == "Lessons"{
                  self.selectedTypeId = "\(1)"
            }else if "\(indexes!)" == "Classes" {
                   self.selectedTypeId = "\(2)"
            }else{
                   self.selectedTypeId = "\(0)"
            }
        
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
       
    }
    @IBAction func fromBtnAction(_ sender: UIButton) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        
        
        let formator = DateFormatter()
        
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
            
            self.fromBtn.setTitle(title, for: .normal)

        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        
      
        
        picker?.show()
    }
    @IBAction func toBtnAction(_ sender: UIButton) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        
        
        let formator = DateFormatter()
        
        let picker = ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
            formator.dateFormat = "dd MMM, yyyy"
            let date = selectedDate as! NSDate
            
            let title = formator.string(from: date as Date)
           
            self.toBtn.setTitle(title, for: .normal)
            
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView)
        

        
        picker?.show()
    }
    
        var messageString : String = ""
        
        func valiDateRegistrationFields() {
            
            if ((fromBtn.titleLabel?.text) != nil) && ((toBtn.titleLabel?.text) != nil){
            }else {
                messageString = messageString + "Select Date Range!\n"
    //            Utility.viewToShake(viewToShake: userNameFld)
            }
          
           
           
        }

    
    @IBAction func searchBtnAction(_ sender: UIButton) {

        
        messageString = ""
        valiDateRegistrationFields()
        if messageString.count > 0{
            let name: String = messageString
            let truncated = name.substring(to: name.index(before: name.endIndex))
            
            let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }

        
        searchCoachBookings()
    }

    func searchCoachBookings()  {
        
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }
        //https://app.glflocker.com/OrbisAppBeta/api/Academy/GetCoachBookings/73/4163/2/2017-02-06/2017-02-08

        var fromDate =  DataManager.sharedInstance.getTodayDate()
        var toDate = DataManager.sharedInstance.getTodayDate()
        
        if ((fromBtn.titleLabel?.text) != nil) {
         fromDate  =  DataManager.sharedInstance.getFormatedDateForJson(date: fromBtn.titleLabel!.text!, formate:"dd/MM/yyyy")
        }
        
        if ((toBtn.titleLabel?.text) != nil)  {
         toDate  =   DataManager.sharedInstance.getFormatedDateForJson(date: toBtn.titleLabel!.text!, formate:"dd/MM/yyyy")
        }
        
        var parameters = ""
            parameters = "\(academyID)/\(coachId)/\(selectedTypeId)/\(fromDate)/\(toDate)/\(is12Houre)"
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetCoachBookings/\(parameters)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            default:
                break
            }
            
            self.data = (object as? [[String:AnyObject]])!
            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    func markAsPaidOrNot(status:Bool,lessonID:String,statusFlag:String)  {
        
        NetworkManager.performRequest(type:.get, method: "Academy/UpdatePaidStatus/\(lessonID)/\(status)/\(statusFlag)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            if  let flage = object as? Bool {
                if flage{
                   
                    self.dismiss(animated: true, completion:nil)
                    self.getCoachBookings()
                }else{
                    DataManager.sharedInstance.printAlertMessage(message:"SomeThing Wrong!", view:self)
                }
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func presentTableViewAsPopOver(sender:UIButton) {
        let menuViewController: PaymentPopUpViewController = self.storyboard!.instantiateViewController(withIdentifier: "PaymentPopUpViewController") as! PaymentPopUpViewController
        
        
        if let event = data[sender.tag]["LessonID"] as? Int {
            menuViewController.eventId = "\(event)"
        }
        
        menuViewController.paymentMethdsArray = paymentsMethodsArray
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: view.frame.size.width-60, height: 220)
        menuViewController.delegateForBack = self
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .unknown
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = sender.superview! as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: sender.bounds.size.width/2,
                y: sender.bounds.origin.y+sender.bounds.size.height,
                width: 1,
                height: 1)
        present(
            menuViewController,
            animated: true,
            completion: nil)
    }
    
    func updateLessonPrice(lessonID:String,price:String)  {
       
        NetworkManager.performRequest(type:.get, method: "Academy/UpdateLessonPrice/\(lessonID)/\(price)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            if  let flage = object as? Bool {
                if flage{
                    self.getCoachBookings()
                }else{
                    DataManager.sharedInstance.printAlertMessage(message:"SomeThing Wrong!", view:self)
                }
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    

}
//MARK:- extention for popup 
extension AllBookingViewController:UIPopoverPresentationControllerDelegate,PopOverDismissed{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return 	UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
    }
    
    func popoverDismissedOrNot(){
         self.getCoachBookings()
    }
}
