//
//  SPUserResizableView.m
//  SPUserResizableView
//
//  Created by Stephen Poletto on 12/10/11.
//
//  Modified by Nasir Mehmood on 12/30/13.
//

#import "SPUserResizableView.h"



/* Let's inset everything that's drawn (the handles and the content view)
   so that users can trigger a resize from a few pixels outside of
   what they actually see as the bounding box. */
#define kSPUserResizableViewGlobalInset 5.0

#define kSPUserResizableViewDefaultMinWidth 48.0
#define kSPUserResizableViewDefaultMinHeight 48.0
#define kSPUserResizableViewDefaultLineHeight 20.0
#define kSPUserResizableViewInteractiveBorderSize 10.0

static SPUserResizableViewAnchorPoint SPUserResizableViewNoResizeAnchorPoint = { 0.0, 0.0, 0.0, 0.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewUpperLeftAnchorPoint = { 1.0, 1.0, -1.0, 1.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewMiddleLeftAnchorPoint = { 1.0, 0.0, 0.0, 1.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewLowerLeftAnchorPoint = { 1.0, 0.0, 1.0, 1.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewUpperMiddleAnchorPoint = { 0.0, 1.0, -1.0, 0.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewUpperRightAnchorPoint = { 0.0, 1.0, -1.0, -1.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewMiddleRightAnchorPoint = { 0.0, 0.0, 0.0, -1.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewLowerRightAnchorPoint = { 0.0, 0.0, 1.0, -1.0 };
static SPUserResizableViewAnchorPoint SPUserResizableViewLowerMiddleAnchorPoint = { 0.0, 0.0, 1.0, 0.0 };


@interface SPGripViewBorderView : UIView
{
    ShapeType _shapeType;
}
@end

@implementation SPGripViewBorderView

- (id)initWithFrame:(CGRect)frame shapeType:(ShapeType)shapeType{
    if ((self = [super initWithFrame:frame])) {
        // Clear background to ensure the content view shows through.
        self.backgroundColor = [UIColor clearColor];
        self.contentMode=UIViewContentModeRedraw;
        _shapeType=shapeType;
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    // (1) Draw the bounding box.
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    CGContextStrokePath(context);
    
    // (2) Calculate the bounding boxes for each of the anchor points.
    CGRect upperLeft = CGRectMake(0.0, 0.0, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect upperRight = CGRectMake(self.bounds.size.width - kSPUserResizableViewInteractiveBorderSize, 0.0, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect lowerRight = CGRectMake(self.bounds.size.width - kSPUserResizableViewInteractiveBorderSize, self.bounds.size.height - kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect lowerLeft = CGRectMake(0.0, self.bounds.size.height - kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect upperMiddle = CGRectMake((self.bounds.size.width - kSPUserResizableViewInteractiveBorderSize)/2, 0.0, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect lowerMiddle = CGRectMake((self.bounds.size.width - kSPUserResizableViewInteractiveBorderSize)/2, self.bounds.size.height - kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect middleLeft = CGRectMake(0.0, (self.bounds.size.height - kSPUserResizableViewInteractiveBorderSize)/2, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    CGRect middleRight = CGRectMake(self.bounds.size.width - kSPUserResizableViewInteractiveBorderSize, (self.bounds.size.height - kSPUserResizableViewInteractiveBorderSize)/2, kSPUserResizableViewInteractiveBorderSize, kSPUserResizableViewInteractiveBorderSize);
    
    // (3) Create the gradient to paint the anchor points.
    CGFloat colors [] = { 
        0.4, 0.8, 1.0, 1.0, 
        0.0, 0.0, 1.0, 1.0
    };
    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
    CGColorSpaceRelease(baseSpace), baseSpace = NULL;
    
    // (4) Set up the stroke for drawing the border of each of the anchor points.
    CGContextSetLineWidth(context, 1);
    CGContextSetShadow(context, CGSizeMake(0.5, 0.5), 1);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    
    if(_shapeType==ShapeTypeLine)
    {
        CGRect allPoints[2] = { middleLeft, middleRight };
        for (NSInteger i = 0; i < 2; i++) {
            CGRect currPoint = allPoints[i];
            CGContextSaveGState(context);
            CGContextAddEllipseInRect(context, currPoint);
            CGContextClip(context);
            CGPoint startPoint = CGPointMake(CGRectGetMidX(currPoint), CGRectGetMinY(currPoint));
            CGPoint endPoint = CGPointMake(CGRectGetMidX(currPoint), CGRectGetMaxY(currPoint));
            CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
            CGContextRestoreGState(context);
            CGContextStrokeEllipseInRect(context, CGRectInset(currPoint, 1, 1));
        }

    }
    else if (_shapeType==ShapeTypeCircle)
    {
        CGRect allPoints[4] = { upperMiddle, lowerMiddle, middleLeft, middleRight };
        for (NSInteger i = 0; i < 4; i++) {
            CGRect currPoint = allPoints[i];
            CGContextSaveGState(context);
            CGContextAddEllipseInRect(context, currPoint);
            CGContextClip(context);
            CGPoint startPoint = CGPointMake(CGRectGetMidX(currPoint), CGRectGetMinY(currPoint));
            CGPoint endPoint = CGPointMake(CGRectGetMidX(currPoint), CGRectGetMaxY(currPoint));
            CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
            CGContextRestoreGState(context);
            CGContextStrokeEllipseInRect(context, CGRectInset(currPoint, 1, 1));
        }

    }
    else if (_shapeType==ShapeTypeSquare)
    {
        CGRect allPoints[4] = { upperLeft, upperRight, lowerRight, lowerLeft};
        for (NSInteger i = 0; i < 4; i++) {
            CGRect currPoint = allPoints[i];
            CGContextSaveGState(context);
            CGContextAddEllipseInRect(context, currPoint);
            CGContextClip(context);
            CGPoint startPoint = CGPointMake(CGRectGetMidX(currPoint), CGRectGetMinY(currPoint));
            CGPoint endPoint = CGPointMake(CGRectGetMidX(currPoint), CGRectGetMaxY(currPoint));
            CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
            CGContextRestoreGState(context);
            CGContextStrokeEllipseInRect(context, CGRectInset(currPoint, 1, 1));
        }

    }
    
    CGGradientRelease(gradient), gradient = NULL;
    CGContextRestoreGState(context);
}

@end

@interface SPUserResizableView ()
{
    ShapeType _shapeType;
    CGAffineTransform startTransform;
    
    CGFloat deltaAngle;
    CGPoint prevPoint;
    UIView *leftLineView, *rightLineView;
    BOOL preventsLayoutWhileResizing;
    BOOL preventsResizing;
    CGFloat totalRotatedAngle;
}

@end

@implementation SPUserResizableView

@synthesize contentView, minWidth, minHeight, preventsPositionOutsideSuperview, delegate;

+ (SPUserResizableView*) getResizeableViewWithShapeType:(ShapeType)shapeType frame:(CGRect)frame
{
    return [[SPUserResizableView alloc] initWithFrame:frame shapeType:shapeType];
}

+ (SPUserResizableView*) getResizeableViewWithShapeType:(ShapeType)shapeType
{
    return [[SPUserResizableView alloc] initWithShapeType:shapeType];
}

+ (SPUserResizableView*) getResizeableViewWithImage:(UIImage*)image
{
    return [[SPUserResizableView alloc] initWithImage:image];
}


- (void)setupDefaultAttributes {
    borderView = [[SPGripViewBorderView alloc] initWithFrame:CGRectInset(self.bounds, kSPUserResizableViewGlobalInset, kSPUserResizableViewGlobalInset) shapeType:_shapeType];
    [self addSubview:borderView];
    self.minWidth = kSPUserResizableViewDefaultMinWidth;
    self.minHeight = kSPUserResizableViewDefaultMinHeight;
    self.preventsPositionOutsideSuperview = YES;
    self.backgroundColor=[UIColor clearColor];

    if(_shapeType==ShapeTypeLine)
    {
        deltaAngle = atan2(self.frame.origin.y+self.frame.size.height - self.center.y, self.frame.origin.x+self.frame.size.width - self.center.x);
        totalRotatedAngle=0;
        
        leftLineView=[[UIView alloc] initWithFrame:CGRectMake(0,(self.bounds.size.height-kSPUserResizableViewDefaultLineHeight)/2,
                                                              kSPUserResizableViewDefaultLineHeight, kSPUserResizableViewDefaultLineHeight)];
        leftLineView.backgroundColor=[UIColor clearColor];
        UIPanGestureRecognizer* panResizeGesture = [[UIPanGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(resizeRotateLine:)];
        [leftLineView addGestureRecognizer:panResizeGesture];
        [self addSubview:leftLineView];
        
        rightLineView=[[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width-kSPUserResizableViewDefaultLineHeight,(self.bounds.size.height-kSPUserResizableViewDefaultLineHeight)/2,
                                                               kSPUserResizableViewDefaultLineHeight, kSPUserResizableViewDefaultLineHeight)];
        rightLineView.backgroundColor=[UIColor clearColor];
        panResizeGesture = [[UIPanGestureRecognizer alloc]
                            initWithTarget:self
                            action:@selector(resizeRotateLine:)];
        [rightLineView addGestureRecognizer:panResizeGesture];
        [self addSubview:rightLineView];
        
        preventsLayoutWhileResizing=YES;
        preventsResizing=NO;
        
    }
}

- (id)initWithFrame:(CGRect)frame shapeType:(ShapeType)shapeType{
    if(shapeType==ShapeTypeLine)
    {
        frame.size.height=kSPUserResizableViewDefaultLineHeight;
    }
    
    if ((self = [super initWithFrame:frame])) {
        _shapeType=shapeType;
     [self setupDefaultAttributes];

    }
    return self;
}

- (id)initWithShapeType:(ShapeType)shapeType{
    CGRect frame=CGRectMake(0, 0, 100, 100);
    if(shapeType==ShapeTypeLine)
    {
        frame.size.height=kSPUserResizableViewDefaultLineHeight;
    }

    self=[self initWithFrame:frame shapeType:shapeType];
    if(self)
        self.center=[[[UIApplication sharedApplication] keyWindow] center];
    return self;
}

- (id)initWithImage:(UIImage*)image{
    CGRect frame=CGRectMake(0, 0, image.size.width, image.size.height);
    
    self=[self initWithFrame:frame shapeType:ShapeTypeImage];
    if(self)
    {
       // self.center=[[[UIApplication sharedApplication] keyWindow] center];
        self.imageToDraw=image;
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self setupDefaultAttributes];
    }
    return self;
}

- (void)setContentView:(UIView *)newContentView {
    [contentView removeFromSuperview];
    contentView = newContentView;
    contentView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
    [self addSubview:contentView];
    
    // Ensure the border view is always on top by removing it and adding it to the end of the subview list.
    [borderView removeFromSuperview];
    [self addSubview:borderView];
}

- (void) drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 3);
    CGRect dRect=CGRectInset(self.bounds, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
    
    switch (_shapeType) {
        case ShapeTypeCircle:
        {
            [[UIColor redColor] setStroke];
            CGContextStrokeEllipseInRect(context, dRect);
        }
            break;
        case ShapeTypeSquare:
        {
            [[UIColor greenColor] setStroke];
            CGContextStrokeRect(context, dRect);
        }
            break;
        case ShapeTypeLine:
        {
            [[UIColor yellowColor] setStroke];
            CGFloat initialX=(kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
            CGFloat initialY=0.5 * self.bounds.size.height;
            CGFloat finalX=(kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2)+dRect.size.width;
            CGFloat finalY=0.5 * self.bounds.size.height;
            
            CGContextMoveToPoint(context, initialX, initialY);
            CGContextAddLineToPoint(context, finalX, finalY);
            CGContextStrokePath(context);
        }
            break;
        case ShapeTypeImage:
        {
            CGRect imageRect = CGRectMake(0, 0, self.imageToDraw.size.width/2, self.imageToDraw.size.height/2);
            CGRect imageRect2 = CGRectMake((self.frame.size.width/2) - (imageRect.size.width/2), (self.frame.size.height / 2) - (imageRect.size.height / 2), imageRect.size.width, imageRect.size.height);
            
//            CGRect imageRectToDraw=CGRectInset(imageRect, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
            CGRect imageRectToDraw2=CGRectInset(imageRect2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);

            
//            CGContextDrawImage(context, imageRectToDraw2, self.imageToDraw.CGImage);
            [self.imageToDraw drawInRect:imageRectToDraw2];
        }
            break;
        default:
            break;
    }
    
}

- (void)setFrame:(CGRect)newFrame {
    [super setFrame:newFrame];
    contentView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
    borderView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset, kSPUserResizableViewGlobalInset);
}

static CGFloat SPDistanceBetweenTwoPoints(CGPoint point1, CGPoint point2) {
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy);
};

typedef struct CGPointSPUserResizableViewAnchorPointPair {
    CGPoint point;
    SPUserResizableViewAnchorPoint anchorPoint;
} CGPointSPUserResizableViewAnchorPointPair;

- (SPUserResizableViewAnchorPoint)anchorPointForTouchLocation:(CGPoint)touchPoint {
    // (1) Calculate the positions of each of the anchor points.
    CGPointSPUserResizableViewAnchorPointPair upperLeft = { CGPointMake(0.0, 0.0), SPUserResizableViewUpperLeftAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair upperMiddle = { CGPointMake(self.bounds.size.width/2, 0.0), SPUserResizableViewUpperMiddleAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair upperRight = { CGPointMake(self.bounds.size.width, 0.0), SPUserResizableViewUpperRightAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair middleRight = { CGPointMake(self.bounds.size.width, self.bounds.size.height/2), SPUserResizableViewMiddleRightAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair lowerRight = { CGPointMake(self.bounds.size.width, self.bounds.size.height), SPUserResizableViewLowerRightAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair lowerMiddle = { CGPointMake(self.bounds.size.width/2, self.bounds.size.height), SPUserResizableViewLowerMiddleAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair lowerLeft = { CGPointMake(0, self.bounds.size.height), SPUserResizableViewLowerLeftAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair middleLeft = { CGPointMake(0, self.bounds.size.height/2), SPUserResizableViewMiddleLeftAnchorPoint };
    CGPointSPUserResizableViewAnchorPointPair centerPoint = { CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2), SPUserResizableViewNoResizeAnchorPoint };
    
    
    // (2) Iterate over each of the anchor points and find the one closest to the user's touch.
    CGFloat smallestDistance = MAXFLOAT;
    CGPointSPUserResizableViewAnchorPointPair closestPoint = centerPoint;

    if(_shapeType==ShapeTypeLine)
    {
        CGPointSPUserResizableViewAnchorPointPair allPoints[3] = { middleLeft, middleRight, centerPoint};
        
        for (NSInteger i = 0; i < 3; i++) {
            CGFloat distance = SPDistanceBetweenTwoPoints(touchPoint, allPoints[i].point);
            if (distance < smallestDistance) {
                closestPoint = allPoints[i];
                smallestDistance = distance;
            }
        }
    }
    else if(_shapeType==ShapeTypeCircle)
    {
        CGPointSPUserResizableViewAnchorPointPair allPoints[5] = { upperMiddle, lowerMiddle, middleLeft, middleRight, centerPoint };
        
        for (NSInteger i = 0; i < 5; i++) {
            CGFloat distance = SPDistanceBetweenTwoPoints(touchPoint, allPoints[i].point);
            if (distance < smallestDistance) {
                closestPoint = allPoints[i];
                smallestDistance = distance;
            }
        }
    }
    else if(_shapeType==ShapeTypeSquare)
    {
        CGPointSPUserResizableViewAnchorPointPair allPoints[5] = { upperLeft, upperRight, lowerRight, lowerLeft, centerPoint };
        
        for (NSInteger i = 0; i < 5; i++) {
            CGFloat distance = SPDistanceBetweenTwoPoints(touchPoint, allPoints[i].point);
            if (distance < smallestDistance) {
                closestPoint = allPoints[i];
                smallestDistance = distance;
            }
        }
    }
    
    return closestPoint.anchorPoint;
}

- (BOOL)isResizing {
    return (anchorPoint.adjustsH || anchorPoint.adjustsW || anchorPoint.adjustsX || anchorPoint.adjustsY);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Notify the delegate we've begun our editing session.
    if (self.delegate && [self.delegate respondsToSelector:@selector(userResizableViewDidBeginEditing:)]) {
        [self.delegate userResizableViewDidBeginEditing:self];
    }
    
    [borderView setHidden:NO];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    anchorPoint = [self anchorPointForTouchLocation:touchPoint];
    
    // When resizing, all calculations are done in the superview's coordinate space.
    touchStart = [touch locationInView:self.superview];
    if (![self isResizing]) {
        // When translating, all calculations are done in the view's coordinate space.
        touchStart = [touch locationInView:self];
    }
    
    if(_shapeType==ShapeTypeLine)
    {
        touchStart = [touch locationInView:self.superview];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    // Notify the delegate we've ended our editing session.
    if (self.delegate && [self.delegate respondsToSelector:@selector(userResizableViewDidEndEditing:)]) {
        [self.delegate userResizableViewDidEndEditing:self];
    }
    
    [self setAnchorPoint:CGPointMake(0.5, 0.5) forView:self];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    // Notify the delegate we've ended our editing session.
    if (self.delegate && [self.delegate respondsToSelector:@selector(userResizableViewDidEndEditing:)]) {
        [self.delegate userResizableViewDidEndEditing:self];
    }
}

- (void)showEditingHandles {
    [borderView setHidden:NO];
}

- (void)hideEditingHandles {
    [borderView setHidden:YES];
}

- (void)resizeUsingTouchLocation:(CGPoint)touchPoint {
    // (1) Update the touch point if we're outside the superview.
    if (self.preventsPositionOutsideSuperview) {
        CGFloat border = kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2;
        if (touchPoint.x < border) {
            touchPoint.x = border;
        }
        if (touchPoint.x > self.superview.bounds.size.width - border) {
            touchPoint.x = self.superview.bounds.size.width - border;
        }
        if (touchPoint.y < border) {
            touchPoint.y = border;
        }
        if (touchPoint.y > self.superview.bounds.size.height - border) {
            touchPoint.y = self.superview.bounds.size.height - border;
        }
    }
    
    // (2) Calculate the deltas using the current anchor point.
    CGFloat deltaW = anchorPoint.adjustsW * (touchStart.x - touchPoint.x);
    CGFloat deltaX = anchorPoint.adjustsX * (-1.0 * deltaW);
    CGFloat deltaH = anchorPoint.adjustsH * (touchPoint.y - touchStart.y);
    CGFloat deltaY = anchorPoint.adjustsY * (-1.0 * deltaH);
    
    // (3) Calculate the new frame.
    CGFloat newX = self.frame.origin.x + deltaX;
    CGFloat newY = self.frame.origin.y + deltaY;
    CGFloat newWidth = self.frame.size.width + deltaW;
    CGFloat newHeight = self.frame.size.height + deltaH;
    
    // (4) If the new frame is too small, cancel the changes.
    if (newWidth < self.minWidth) {
        newWidth = self.frame.size.width;
        newX = self.frame.origin.x;
    }
    
    if (newHeight < self.minHeight) {
        newHeight = self.frame.size.height;
        newY = self.frame.origin.y;
    }
    
    // (5) Ensure the resize won't cause the view to move offscreen.
    if (self.preventsPositionOutsideSuperview) {
        if (newX < self.superview.bounds.origin.x) {
            // Calculate how much to grow the width by such that the new X coordintae will align with the superview.
            deltaW = self.frame.origin.x - self.superview.bounds.origin.x;
            newWidth = self.frame.size.width + deltaW;
            newX = self.superview.bounds.origin.x;
        }
        if (newX + newWidth > self.superview.bounds.origin.x + self.superview.bounds.size.width) {
            newWidth = self.superview.bounds.size.width - newX;
        }
        if (newY < self.superview.bounds.origin.y) {
            // Calculate how much to grow the height by such that the new Y coordintae will align with the superview.
            deltaH = self.frame.origin.y - self.superview.bounds.origin.y;
            newHeight = self.frame.size.height + deltaH;
            newY = self.superview.bounds.origin.y;
        }
        if (newY + newHeight > self.superview.bounds.origin.y + self.superview.bounds.size.height) {
            newHeight = self.superview.bounds.size.height - newY;
        }
    }
    
//    if(_shapeType==ShapeTypeLine && newHeight<kSPUserResizableViewDefaultLineHeight)
//        newHeight=kSPUserResizableViewDefaultLineHeight;
    
    self.frame = CGRectMake(newX, newY, newWidth, newHeight);
    touchStart = touchPoint;
    
    [self setNeedsDisplay];
    [borderView setNeedsDisplay];
}

-(void)resizeRotateLine:(UIPanGestureRecognizer *)recognizer
{
    if ([recognizer state]== UIGestureRecognizerStateBegan)
    {
        prevPoint = [recognizer locationInView:self];
        [self setNeedsDisplay];
    }
    else if ([recognizer state] == UIGestureRecognizerStateChanged)
    {
        if (self.bounds.size.width < (4*kSPUserResizableViewDefaultLineHeight) || self.bounds.size.height < kSPUserResizableViewDefaultLineHeight)
        {
            self.bounds = CGRectMake(self.bounds.origin.x,
                                     self.bounds.origin.y,
                                     (4*kSPUserResizableViewDefaultLineHeight),
                                     kSPUserResizableViewDefaultLineHeight);
            rightLineView.frame =CGRectMake(self.bounds.size.width-kSPUserResizableViewDefaultLineHeight,
                                            (self.bounds.size.height-kSPUserResizableViewDefaultLineHeight)/2,
                                            kSPUserResizableViewDefaultLineHeight,
                                            kSPUserResizableViewDefaultLineHeight);
            leftLineView.frame = CGRectMake(0, (self.frame.size.height-kSPUserResizableViewDefaultLineHeight)/2,
                                            kSPUserResizableViewDefaultLineHeight, kSPUserResizableViewDefaultLineHeight);
            prevPoint = [recognizer locationInView:self];
            
        } else {
            CGPoint point = [recognizer locationInView:self];
            float wChange = 0.0, hChange = 0.0;
            
            wChange = (point.x - prevPoint.x);
            hChange = (point.y - prevPoint.y);
            
            if (ABS(wChange) > 20.0f || ABS(hChange) > 20.0f) {
                prevPoint = [recognizer locationInView:self];
                return;
            }
            
            if (YES == preventsLayoutWhileResizing) {
                if (wChange < 0.0f && hChange < 0.0f) {
                    float change = MIN(wChange, hChange);
                    wChange = change;
                    hChange = change;
                }
                if (wChange < 0.0f) {
                    hChange = wChange;
                } else if (hChange < 0.0f) {
                    wChange = hChange;
                } else {
                    float change = MAX(wChange, hChange);
                    wChange = change;
                    hChange = change;
                }
            }
            
            
            if([recognizer.view isEqual:rightLineView])
            {
                
            }
            else
            {
                wChange*=-1;
            }
            
            CGFloat newWidth=self.bounds.size.width + wChange;
            if(newWidth<(4*kSPUserResizableViewDefaultLineHeight))
                newWidth=4*kSPUserResizableViewDefaultLineHeight;
            
            self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y,
                                     newWidth,
                                     self.bounds.size.height );
            
            rightLineView.frame =CGRectMake(self.bounds.size.width-kSPUserResizableViewDefaultLineHeight,
                                            (self.bounds.size.height-kSPUserResizableViewDefaultLineHeight)/2,
                                            kSPUserResizableViewDefaultLineHeight, kSPUserResizableViewDefaultLineHeight);
            leftLineView.frame = CGRectMake(0,
                                            (self.bounds.size.height-kSPUserResizableViewDefaultLineHeight)/2,
                                            kSPUserResizableViewDefaultLineHeight, kSPUserResizableViewDefaultLineHeight);
            prevPoint = [recognizer locationInView:self];
        }
        
        /* Rotation */
        float ang = atan2([recognizer locationInView:self.superview].y - self.center.y,
                          [recognizer locationInView:self.superview].x - self.center.x);
        float angleDiff = deltaAngle - ang;
        
        if([recognizer.view isEqual:rightLineView])
        {
            [self setAnchorPoint:CGPointMake(0, 0.5) forView:self];
        }
        else
        {
            [self setAnchorPoint:CGPointMake(1, 0.5) forView:self];
            angleDiff+=DEGREES_TO_RADIANS(180);
        }
        
        totalRotatedAngle+=angleDiff;
        
        if (NO == preventsResizing) {
            self.transform = CGAffineTransformMakeRotation(-angleDiff);
        }
        
        borderView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset, kSPUserResizableViewGlobalInset);
        
        [self setNeedsDisplay];
        [borderView setNeedsDisplay];
    }
    else if ([recognizer state] == UIGestureRecognizerStateEnded)
    {
        [self setAnchorPoint:CGPointMake(0.5, 0.5) forView:self];
        prevPoint = [recognizer locationInView:self];
        [self setNeedsDisplay];
    }
}

- (void)translateUsingTouchLocation:(CGPoint)touchPoint {
    CGPoint newCenter = CGPointMake(self.center.x + touchPoint.x - touchStart.x, self.center.y + touchPoint.y - touchStart.y);
    if (self.preventsPositionOutsideSuperview) {
        // Ensure the translation won't cause the view to move offscreen.
        CGFloat midPointX = CGRectGetMidX(self.bounds);
        if (newCenter.x > self.superview.bounds.size.width - midPointX) {
            newCenter.x = self.superview.bounds.size.width - midPointX;
        }
        if (newCenter.x < midPointX) {
            newCenter.x = midPointX;
        }
        CGFloat midPointY = CGRectGetMidY(self.bounds);
        if (newCenter.y > self.superview.bounds.size.height - midPointY) {
            newCenter.y = self.superview.bounds.size.height - midPointY;
        }
        if (newCenter.y < midPointY) {
            newCenter.y = midPointY;
        }
    }
    self.center = newCenter;
}

- (void)translateLineUsingTouchLocation:(CGPoint)touchPoint {
    CGPoint newCenter = CGPointMake(self.center.x + touchPoint.x - touchStart.x,
                                    self.center.y + touchPoint.y - touchStart.y);
    if (self.preventsPositionOutsideSuperview) {
        // Ensure the translation won't cause the view to move offscreen.
        CGFloat midPointX = CGRectGetMidX(self.bounds);
        if (newCenter.x > self.superview.bounds.size.width - midPointX) {
            newCenter.x = self.superview.bounds.size.width - midPointX;
        }
        if (newCenter.x < midPointX) {
            newCenter.x = midPointX;
        }
        CGFloat midPointY = CGRectGetMidY(self.bounds);
        if (newCenter.y > self.superview.bounds.size.height - midPointY) {
            newCenter.y = self.superview.bounds.size.height - midPointY;
        }
        if (newCenter.y < midPointY) {
            newCenter.y = midPointY;
        }
    }
    self.center = newCenter;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if(_shapeType==ShapeTypeImage)
        return;
    
    if(_shapeType==ShapeTypeLine)
    {
        CGPoint touchLocation = [[touches anyObject] locationInView:self];
        if (CGRectContainsPoint(leftLineView.frame, touchLocation) || CGRectContainsPoint(rightLineView.frame, touchLocation)) {
            return;
        }
        
        CGPoint touch = [[touches anyObject] locationInView:self.superview];
        [self translateLineUsingTouchLocation:touch];
        touchStart = touch;
    }
    else
    {    UITouch *touch=[touches anyObject];
        if ([self isResizing]) {
            CGPoint touchPoint=[touch locationInView:self.superview];
            [self resizeUsingTouchLocation:touchPoint];
            
        } else {
            [self translateUsingTouchLocation:[touch locationInView:self]];
        }
    }
}


#pragma mark
#pragma mark Custom Methods

- (UIImage*) getImage
{
    UIImage *finalImage=nil;
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0);
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGFloat angleInRadians = atan2f(self.transform.b, self.transform.a);
    CGRect imgRect = self.frame;
    CGContextTranslateCTM(context, imgRect.size.width, imgRect.size.height);
    CGContextRotateCTM(context, angleInRadians);
    CGContextTranslateCTM(context, -(imgRect.size.width), -(imgRect.size.height));
    self.backgroundColor=[UIColor blackColor];
    [[self layer] renderInContext:context];
    finalImage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}

- (CGFloat) getTotalRotatedAngle
{
    return totalRotatedAngle;
}

- (ShapeType) shapeType
{
    return _shapeType;
}

-(void)setAnchorPoint:(CGPoint)newAnchorPoint forView:(UIView *)view
{
    CGPoint newPoint = CGPointMake(view.bounds.size.width * newAnchorPoint.x, view.bounds.size.height * newAnchorPoint.y);
    CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x, view.bounds.size.height * view.layer.anchorPoint.y);
    
    newPoint = CGPointApplyAffineTransform(newPoint, view.transform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, view.transform);
    
    CGPoint position = view.layer.position;
    
    position.x -= oldPoint.x;
    position.x += newPoint.x;
    
    position.y -= oldPoint.y;
    position.y += newPoint.y;
    
    view.layer.position = position;
    view.layer.anchorPoint = newAnchorPoint;
}

- (float) calculateDistanceFromCenter:(CGPoint)point {
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    float dx = point.x - center.x;
    float dy = point.y - center.y;
    return sqrt(dx*dx + dy*dy);
}


- (void)dealloc {
    [contentView removeFromSuperview];
}

@end
