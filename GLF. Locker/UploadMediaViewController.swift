//
//  UploadMediaViewController.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 09/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class UploadMediaViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var titleFieldView: UIView!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var titleFieldViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionField: UITextField!
    
    @IBOutlet weak var skillFieldView: UIView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var skillField: UITextField!
    @IBOutlet weak var skillFieldViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var drillFieldView: UIView!
    @IBOutlet weak var drillField: UITextField!
    @IBOutlet weak var drillFieldViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    private var skillsCategories: [[String: AnyObject]]!
    private var selectedCategory: [String: AnyObject]!
    private var skillsList: [[String: AnyObject]] = []
    private var selectedSkills: [[String: AnyObject]] = []
    var forClass = false
    var showTitle: Bool = true
    var showSelectCategory: Bool = true
    var showDrill: Bool = false
    var skillCategoryJson: String?
    var skillTypesJson: String?
    var forEditingMedia: Bool = false
    var mediaType: OrbisMediaType = kMediaTypeVideo
    var mediaInfo: [String: Any]!
    var studentData: [String: Any]!
    var playersArray = [[String:AnyObject]]()
    var forStudentLocker = false
    @IBOutlet weak var selectStudentBtn: UIButton!
    @IBOutlet weak var studentViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var studentFld: UITextField!
    var selectedUsers  = [[String:AnyObject]]()
    var jsonStringForPlayers = ""
    @IBOutlet weak var titleLbl: UILabel!
    var youTubeUpload = false
    /************************************************************************************************************/
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if forEditingMedia {
             self.title = "EDIT MEDIA"
            filllingDiscriptionForEdit()
            self.uploadBtn.setTitle("Update Media", for: .normal)
        }else{
        self.title = "UPLOAD MEDIA"
        }
        
        if (showTitle == false) {
            titleFieldViewHeightConstraint.constant = 0
            titleFieldView.isHidden = true
        }
        if (showSelectCategory == false) {
            skillFieldViewHeightConstraint.constant = 0
            skillFieldView.isHidden = true
        }
        if (mediaType != kMediaTypeDrill) {
            drillFieldViewHeightConstraint.constant = 0
            drillFieldView.isHidden = true
        }
        
        if (showSelectCategory == true) {
            getSkillsCategories()
        }
            table.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.table.tableFooterView = UIView(frame: CGRect.zero)
        
        if forStudentLocker {
            studentViewHeightCons.constant = 63
            getPlayersList()
        }else{
            studentViewHeightCons.constant = 0
        }
        
        if youTubeUpload{
            titleFieldViewHeightConstraint.constant = 63
            titleLbl.text = "Paste or Write Below YouTube Video Link"
            titleField.placeholder = "YouTube Link"
        }
    }
    
    
    func filllingDiscriptionForEdit()  {
        if let dis = mediaInfo["Description"] as? String {
            descriptionField.text = dis
        }
    }
    /************************************************************************************************************/
    // MARK: - Private Methods
    
    private func updateUI() {
      
        skillField.text = selectedCategory["SkillCategoryName"] as? String
        
        skillsList = selectedCategory["Skills"] as! [[String: AnyObject]]
        selectedSkills.removeAll()
        table.reloadData()
        tableHeightConstraint.constant = (skillsList.count > 0) ? UIScreen.main.bounds.height - 385.0 : 0
        
    }
    
    private func showPickerView() {
        var dataAray = [String]()
        for dic in skillsCategories {
            if let val = dic["SkillCategoryName"] as? String {
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT FILTER", rows: dataAray, initialSelection: 0, doneBlock: { (picker, value, index) in
            self.selectedCategory = self.skillsCategories[value]
            self.updateUI()
        }, cancel: nil,
           origin: skillField)
    }
    
    private func showDrillPickerView() {
        var dataArray = [String]()
        for i in 1...10 {
            dataArray.append("\(i)")
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT NUMBER", rows: dataArray, initialSelection: 0, doneBlock: { (picker, value, index) in
            self.drillField.text = dataArray[value]
        }, cancel: nil,
           origin: drillField)
    }

    /************************************************************************************************************/
    // MARK: - IBActions
    
    @IBAction func uploadAction() {
        //if for editiing of media will run this
        if forEditingMedia {
        updateOfMedia()
            return
        }
        
        if youTubeUpload{
            
            messageStr = ""
            validateData()
            if messageStr.count > 0{
                let name: String = messageStr
                let truncated = name.substring(to: name.index(before: name.endIndex))
                
                let alertController = UIAlertController(title: "Error", message: truncated, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }else {
                uploadVideo()

            }
            
            return
        }
        
        if showSelectCategory == true {
            uploadVideo()
        } else {
            assignMedia()
        }
    }
    
    
    var messageStr = ""
    func validateData()  {
        
        
        if let text = titleField.text , !text.isEmpty{
        }else {
            messageStr = messageStr + "Enter YouTube Video Link\n"
        }
        
        
    }
    
    @IBAction func cancelAction() {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func studentBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectStudentViewController") as! SelectStudentViewController
        controller.originaldata = playersArray
        controller.delegateForUpload = self
        controller.uploadingToLocker = true
        controller.selectedDataArray = selectedUsers
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    /************************************************************************************************************/
    // MARK: - Network Calls
    
    private func getSkillsCategories() {
        
        NetworkManager.performRequest(type:.get, method: "academy/GetSkillCategoriesAndSkills/\(DataManager.sharedInstance.currentUser()!["AcademyId"] as! Int)", parameter: nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
            case _ as [[String:AnyObject]]:
                self.skillsCategories = object as! [[String: AnyObject]]

                if self.skillsCategories.count < 1{
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)

                    return
                }

                self.selectedCategory = self.skillsCategories[0]
                self.updateUI()
                break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
            }
        })
        { (error) in
            print(error!)
            DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
        }
    }
    
    
    private func uploadVideo() {
        print(mediaInfo);
        print(mediaType,kMediaTypeVideo)
        
        if youTubeUpload{
            
//            mediaInfo["fileURL"] = titleField.text!
//            mediaInfo["IsEmbedded"] = "1"
            var acdemyId = ""
            if let academyID = DataManager.sharedInstance.currentAcademy()!["AcademyID"] {
                acdemyId = "\(academyID)"
            }
            var createdBY = ""
           if let createdBy =  (DataManager.sharedInstance.currentUser()!["Userid"]
            ){
            createdBY  = "\(createdBy)"
            }

            let skilCategory = selectedCategory["SkillCategoryName"]
            
            var jsonString = ""
            for skill in selectedSkills {
                if jsonString == "" {
                    jsonString += skill["SkillName"] as! String
                }
                else {
                    jsonString += ",\(skill["SkillName"] as! String)"
                }
            }
            
           let skillTypes = jsonString

//            print(skilCategory,skillTypes)
            let parameters = ["FileURL":"\(titleField.text!)","IsEmbedded":"1","MediaTypeID":"\(mediaType.rawValue)","AcademyID":"\(acdemyId)","Description":"\(descriptionField.text!)","CreatedBy":"\(createdBY)","CreationDate":"\(DataManager.sharedInstance.getTodayDate())","skillCategory":"\(skilCategory!)","skillTypes":"\(skillTypes)","youtubeVideoUpload":"true"]
            print(parameters)
            uploadingYouTubeVideo(mediaInfo:parameters as [String : AnyObject])
            return
        }else{
        if (mediaType == kMediaTypeVideo) {
            
                mediaInfo["mediaPath"] = mediaInfo["videoPath"]
            
        }else if(mediaType == kMediaTypeDrill){
              mediaInfo["mediaPath"] = mediaInfo["videoPath"]
        }
        else {
            if (mediaInfo["imagePath"] != nil) {
                mediaInfo["mediaPath"] = mediaInfo["imagePath"]
            }
            else {
                mediaInfo["mediaPath"] = mediaInfo["photoPath"]
            }
        }
//        mediaInfo["mediaPath"] = (mediaType == kMediaTypePhoto) ? mediaInfo["imagePath"] : mediaInfo["videoPath"]
        if let academyID = DataManager.sharedInstance.currentAcademy()!["AcademyID"] {
            mediaInfo["academyID"] = academyID
        } else {
            mediaInfo["academyID"] = 0
        }
        
        mediaInfo["createdBy"] = DataManager.sharedInstance.currentUser()!["Userid"]
        mediaInfo["modifiedBy"] = DataManager.sharedInstance.currentUser()!["Userid"]
        
     //   print(studentData["LessonType"])
        if (studentData == nil) {
            mediaInfo["lessonType"] = kLessonTypeMedia
        }
        else {
            mediaInfo["lessonType"] = studentData["LessonType"]
            
        }
        
     
        mediaInfo["mediaTypeId"] = mediaType.rawValue
        mediaInfo["numPracticeDrill"] = (mediaType == kMediaTypeDrill) ? Int(drillField.text!) : 0
        
        if (showTitle == true) {
            mediaInfo["title"] = titleField.text
        }
        
        mediaInfo["description"] = descriptionField.text
        
        
        // Add Skill Category //
        
        if (showSelectCategory == true) {
            mediaInfo["skillCategory"] = selectedCategory["SkillCategoryName"]
            
            var jsonString = ""
            for skill in selectedSkills {
                if jsonString == "" {
                    jsonString += skill["SkillName"] as! String
                }
                else {
                    jsonString += ",\(skill["SkillName"] as! String)"
                }
            }
            
            mediaInfo["skillTypes"] = jsonString
        }
        else {
            mediaInfo["skillCategory"] = skillCategoryJson!
            mediaInfo["skillTypes"] = skillTypesJson!
        }
        
        mediaInfo["studendID"] = 0
        if forClass {
            mediaInfo["lessonType"] = "2"
           mediaInfo["studendID"] = mediaInfo["studentID"]
        }

        }
         //call here your uploading method
        
        //Holy
          if forStudentLocker {
       // if let userId =  selectedUser["PlayerId"]! as? Int{
            mediaInfo["PlayersList"] = "\(jsonStringForPlayers)"
           // }
        }
        
        //token basedathentication
        mediaInfo["AcademyId"] = DataManager.sharedInstance.currentUser()?.object(forKey: "AcademyId")
        mediaInfo["UserName"] = DataManager.sharedInstance.currentUser()?.object(forKey: "UserName")
        mediaInfo["UserID"] = DataManager.sharedInstance.currentUser()?.object(forKey: "Userid")
        mediaInfo["AuthenticationToken"] = DataManager.sharedInstance.currentUser()?.object(forKey: "TokenKey")
// till here
        print(mediaInfo)
        UploadManager.shared().startUploadingMedia(with: mediaInfo)
        let count = navigationController?.viewControllers.count
        if forStudentLocker {
            //holy fromDashBoard check can be removed this was for direct save to student locker
            _ = navigationController?.popToRootViewController(animated: true)
        }else{
            _ = navigationController?.popToViewController((navigationController?.viewControllers[count!-3])!, animated: true)
        }
        
    }
    
    func assignMedia() {
        var parameters: [String: Any] = [:]
        parameters["UserId"] = DataManager.sharedInstance.currentUser()!["Userid"]
        parameters["LessonType"] = "Lesson"
        
        if forClass {
          parameters["LessonType"] = "Class"
        }
        
        if (studentData != nil) {
            if ((studentData["LessonType"] as! Int) == 2) {
                parameters["LessonType"] = "Class"
            }
            else if (studentData["LessonType"] as! Int) == 1000 {
                parameters["LessonType"] = "Media"
            }
        }
        
        parameters["LessonId"] = mediaInfo["lessonID"]
        parameters["PlayerId"] = mediaInfo["studentID"]
        parameters["Description"] = descriptionField.text
        parameters["MediaId"] = mediaInfo["MediaId"]
        parameters["DrillCount"] = mediaInfo["DrillCount"]
//        print(parameters)
        NetworkManager.performRequest(type:.post, method: "Student/AssignLessonMedia/", parameter: parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
            case _ as Int:
                print("success")
                let count = self.navigationController?.viewControllers.count
                _ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[count!-3])!, animated: true)
                break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
            }
        })
        { (error) in
            print(error!)
            DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
        }
    }
    
    func updateOfMedia() {
        
        var parameters: [String: Any] = [:]
        parameters["UserId"] = DataManager.sharedInstance.currentUser()!["Userid"]
        
        if (showSelectCategory == true) {
            mediaInfo["skillCategory"] = selectedCategory["SkillCategoryName"]
            
            var jsonString = ""
            for skill in selectedSkills {
                if jsonString == "" {
                    jsonString += skill["SkillName"] as! String
                }
                else {
                    jsonString += ",\(skill["SkillName"] as! String)"
                }
            }
            
            mediaInfo["skillTypes"] = jsonString
        }
        else {
            mediaInfo["skillCategory"] = skillCategoryJson!
            mediaInfo["skillTypes"] = skillTypesJson!
        }
        
        parameters["SkillCategory"] = mediaInfo["skillCategory"]
        parameters["SkillTypes"] =  mediaInfo["skillTypes"]
        parameters["Description"] = descriptionField.text
        parameters["MediaId"] = mediaInfo["MediaId"]
        parameters["MediaTypeId"] = mediaInfo["MediaTypeId"]
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "MediaSection/UpdateMedia", parameter: parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            //photo=2,video=1,
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
            case _ as [String:AnyObject]:
              DataManager.sharedInstance.printAlertMessage(message:"An error occured on server.\n Please try again later", view:self)
                return
            case _ as Int:
                print("success")
                let count = self.navigationController?.viewControllers.count
                _ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[count!-2])!, animated: true)
                break
                
            default:
               
                break
            }
        })
        { (error) in
            print(error!)
            DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
        }
    }
    
    
    /************************************************************************************************************/
    // MARK: - UITextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == skillField) {
            descriptionField.resignFirstResponder()
            showPickerView()
            return false
        }
        else if (textField == drillField) {
            descriptionField.resignFirstResponder()
            showDrillPickerView()
            return false
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    /************************************************************************************************************/
    // MARK: - TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return skillsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilteredOptionTableViewCell", for: indexPath) as! FilteredOptionTableViewCell
        cell.selectionStyle = .none
        
        let skill = skillsList[indexPath.row]
        if let title = skill["SkillName"] as? String{
            cell.companyLbl.text = title
        }
        
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.isUserInteractionEnabled = false
        
        cell.radioBtn.isSelected = selectedSkills.contains(where: { (s) -> Bool in
            return (s["SkillID"] as! Int == skill["SkillID"] as! Int) ? true : false
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FilteredOptionTableViewCell
        let skill = skillsList[indexPath.row]
        cell.radioBtn.isSelected = !cell.radioBtn.isSelected
        
        if cell.radioBtn.isSelected == true {
            selectedSkills.append(skillsList[indexPath.row])
        }
        else {
            for i in 0..<selectedSkills.count {
                let s = selectedSkills[i]
                if s["SkillID"] as! Int == skill["SkillID"] as! Int {
                    selectedSkills.remove(at: i)
                    break
                }
            }
        }
    }
    
     // MARK: - get coach players
    func getPlayersList()  {
        
        var playerID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            playerID = "\(userId)"
        }
        var academyID = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "academy/GetPlayerList/\(academyID)/\(playerID)/0", parameter:nil, view:(UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            let object2 = object as! [[String:AnyObject]]
            switch object2 {
            case _ as NSNull:
                
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
                self.playersArray = (object as? [[String:AnyObject]])!
                break
                
            default:
                
                return
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }

    
    func uploadingYouTubeVideo(mediaInfo:[String:AnyObject]) {
        print(mediaInfo)
        NetworkManager.performRequestForYouTubeUpload(type:.put, method: "Coach/MyPostFile", parameter: mediaInfo as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
            case _ as String:
                let count = self.navigationController?.viewControllers.count
                print(count)
                DispatchQueue.main.async(execute: {
                    
                _ = self.navigationController?.popToViewController((self.navigationController?.viewControllers[count!-3])!, animated: true)
                DataManager.sharedInstance.printAlertMessage(message:"Video Uploaded", view:UIApplication.getTopestViewController()!)
                    if #available(iOS 13.0, *) {
                    DispatchQueue.main.async(execute: {
                        MBProgressHUD.hideAllHUDs(for:self.view, animated: true )})
                    }else{
                        MBProgressHUD.hideAllHUDs(for:self.view, animated: true )
                    }
                })
                break
            case _ as Int:
                print("success")

                break
                
            default:
                
                break
            }
        })
        { (error) in
            print(error!)
            DataManager.sharedInstance.printAlertMessage(message: (error?.description)!, view:self)
        }
    }

    
}
//holy
extension UploadMediaViewController : SelectedStudentDelegateForUpload{
    func seletedStudentData(selectedData: [[String:AnyObject]],selectedUserId:String){
//        let userData = dic as! [String : AnyObject]
//        
//         selectedUser  = userData
//
//        if let title = userData["PlayerName"] as? String{
//            studentFld.text = title
//        }
        
        
        selectedUsers = selectedData
        print(selectedData)
        jsonStringForPlayers = ""
       var jsonNamesForPlayers = ""
        for index in selectedData {
            
            if let value  =  index["PlayerId"] as? Int {
                jsonStringForPlayers = jsonStringForPlayers +  "\(value)" + ","
            }
            if let value  =  index["PlayerName"] as? String {
                jsonNamesForPlayers = jsonNamesForPlayers + value  + ","
            }

        }
        studentFld.text = jsonNamesForPlayers
        if jsonStringForPlayers != "" {
            jsonStringForPlayers = jsonStringForPlayers.substring(to: jsonStringForPlayers.index(before: jsonStringForPlayers.endIndex))
        }
        
        print(jsonStringForPlayers)
    }
}
