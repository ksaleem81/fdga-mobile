//
//  ScheduleViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/25/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import EventKitUI
import SideMenu
//import Crashlytics

class ScheduleViewController: UIViewController {

    @IBOutlet weak var calenderLbl: UILabel!
    @IBOutlet weak var viewLbl: UILabel!
    @IBOutlet weak var setLeavesLbl: UILabel!
    @IBOutlet weak var bookLessonLbl: UILabel!
    @IBOutlet weak var adhocBtnCons: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        fetchData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.asyncAfter(deadline: .now() +  1.0) {
            // change 2 to desired number of seconds
            self.settingRighMenuBtn()
        }
    }
    
    func fetchData()  {
        
        var academyId = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyId = "\(id)"
        }
        
        let refreshCont = RefreshingViewController()
        refreshCont.refreshData(acedmyId: academyId, onSuccess: { (data) in
            let data1 = data as! [[String:AnyObject]]
            if data1.count > 0 {
                let dic = data1[0]
                print(dic)
                DataManager.sharedInstance.updateAcademyDetails(dic as NSDictionary)
            }
            
        }) { (error) in
            self.showInternetError(error: error!)
        }
    }

    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(ScheduleViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func calendarBtnAction(_ sender: UIButton) {

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "GLFCalendarViewController") as! GLFCalendarViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func adhocBtnAction(_ sender: UIButton) {
      
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CoachScheduleViewController") as! CoachScheduleViewController
        controller.adhocScheduleOn = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func viewBtnAction(_ sender: UIButton) {
     
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CoachScheduleViewController") as! CoachScheduleViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func setLeavesBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddLeaveViewController") as! AddLeaveViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func bookLessonBtnAction(_ sender: UIButton) {
      
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookLessonVC") as! BookLessonVC
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
