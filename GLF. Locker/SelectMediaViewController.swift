//
//  SelectMediaViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/23/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
import Photos
class SelectMediaViewController: UIViewController, OrbisCameraViewControllerDelegate, GalleryViewControllerDelegate {

    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoBgImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    
    var mediaType: OrbisMediaType = kMediaTypeVideo
    
    fileprivate var mediaInfo = [String: Any]()
    @IBOutlet weak var youtubBtnHeightCons: NSLayoutConstraint!
    @IBOutlet weak var galeryBtnHeightCons: NSLayoutConstraint!
    @IBOutlet weak var buttonsViewHeightConst: NSLayoutConstraint!
    var forclass = false
    @IBOutlet weak var galeryAboveLine: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if mediaType == kMediaTypePhoto {
            firstButton.setTitle("TAKE PHOTO", for: .normal)
            playButton.isHidden = true
        }
        
//        if mediaType == kMediaTypeDrill || mediaType == kMediaTypePhoto {
//            self.buttonsViewHeightConst.constant = 200
//            self.youtubBtnHeightCons.constant = 0
//        }
        //remove below two lines for youtube video uploading
            self.buttonsViewHeightConst.constant = 200
            self.youtubBtnHeightCons.constant = 0

        if currentUserLogin == 4  {
            galeryBtnHeightCons.constant = 0
            buttonsViewHeightConst.constant = 155
            galeryAboveLine.isHidden = true
        }
        
        settingRighMenuBtn()
    }
    
//    func settingPermissions()  {
//        let status = PHPhotoLibrary.authorizationStatus()
//
//        if (status == PHAuthorizationStatus.authorized) {
//            // Access has been granted.
//        }
//
//        else if (status == PHAuthorizationStatus.denied) {
//            // Access has been denied.
//        }
//
//        else if (status == PHAuthorizationStatus.notDetermined) {
//
//            // Access has not been determined.
//            PHPhotoLibrary.requestAuthorization({ (newStatus) in
//
//                if (newStatus == PHAuthorizationStatus.authorized) {
//
//                }
//
//                else {
//
//                }
//            })
//        }
//
//        else if (status == PHAuthorizationStatus.restricted) {
//            // Restricted access - normally won't happen.
//        }
//    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(SelectMediaViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func rightBarBtnAction() {
    
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    
    func getMediaTypeValue() -> Int {
        return (mediaType == kMediaTypePhoto) ? 1 : 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func takeVideoAction(_ sender: UIButton) {
        let options: [String: Any] = ["shouldSaveVideoToGallery": (false),
                                      "videoSource": (1),
                                      "mediaType": (getMediaTypeValue())]
        
        let callBackID = Utility.createCallBackID()
        let orbisCameraVC = OrbisCameraViewController.getCamera()
        orbisCameraVC?.delegate = self
        orbisCameraVC?.captureVideo(withCallbackID: callBackID, withOptions: options)
    }
    
    @IBAction func choosFromAppAction(_ sender: UIButton) {
        let galleryViewController = GalleryViewController.init(nibName: "GalleryViewController", bundle: Bundle.main)
        galleryViewController.galleryType = (mediaType == kMediaTypePhoto) ? GalleryTypePhoto : GalleryTypeVideo
        galleryViewController.isFromSelectMediaPage = true;
        galleryViewController.delegate = self
        
        let navigationController = UINavigationController.init(rootViewController: galleryViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    @IBAction func youTubeBtnAction(_ sender: UIButton) {
        let uploadMediaVC = storyboard?.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
        uploadMediaVC.youTubeUpload = true
        uploadMediaVC.mediaType = OrbisMediaType(rawValue: 1)
        navigationController?.pushViewController(uploadMediaVC, animated: true)

    }
    
    @IBAction func chooseFromDeviceAction(_ sender: UIButton) {
        let options: [String: Any] = ["shouldSaveVideoToGallery": (false),
                                      "videoSource": (0),
                                      "mediaType": (getMediaTypeValue())]
        
        let callBackID = Utility.createCallBackID()
        let orbisCameraVC = OrbisCameraViewController.getCamera()
        orbisCameraVC?.delegate = self
        orbisCameraVC?.captureVideo(withCallbackID: callBackID, withOptions: options)
    }
    
    @IBAction func confirmBtnAction(_ sender: UIButton) {
        let uploadMediaVC = storyboard?.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
        uploadMediaVC.showTitle = false
        if forclass {
            uploadMediaVC.forClass = true
        }
        uploadMediaVC.mediaInfo = mediaInfo
        uploadMediaVC.mediaType = mediaType
        navigationController?.pushViewController(uploadMediaVC, animated: true)
    }

    @IBAction func playAction(_ sender: UIButton) {
        Utility.playVideo(path: mediaInfo["videoPath"] as! String, on: self)
    }
    
    
    
    /************************************************************************************************/
    // MARK: - Handle VideoInfo
    
    private func handleVideoInfo(info: [AnyHashable : Any], callBack: String?) {
        print("Media Info: \(info)")
       
        mediaInfo = info as! [String : Any]
        if let callBackID = callBack {
            mediaInfo["callBackID"] = callBackID
        }
        
        OrbisHelper.getThumbnailOfVideo(mediaInfo["videoPath"] as! String) { (error, thumbInfo) in
            if error != nil {
                print("Thumb Error: \(error)")
            }
            else {
                self.videoView.isHidden = false
                self.videoBgImage.image = UIImage.init(contentsOfFile: thumbInfo!["thumbPath"]! as! String)
            }
        }
    }
    
    private func handleImageInfo(info: [AnyHashable: Any], callBack: String?) {
        print("Media Info: \(info)")
        mediaInfo = info as! [String : Any]
        
        if let callBackID = callBack {
            mediaInfo["callBackID"] = callBackID
        }
        
        if (mediaInfo["imagePath"] == nil) {
            mediaInfo["imagePath"] = mediaInfo["photoPath"]
        }
        
        self.videoView.isHidden = false
        self.videoBgImage.image = UIImage.init(contentsOfFile: mediaInfo["imagePath"] as! String)
    }
    
    
    
    /************************************************************************************************/
    // MARK: - OrbisCameraViewController Delegate
    
    func orbisCameraVC(_ controller: OrbisCameraViewController!, didFinishRecordingVideo info: [AnyHashable : Any]!, withCallBackID callBackID: String!) {
        
//        cobra
         DispatchQueue.main.asyncAfter(deadline: .now() +  0.1) { // change 2 to desired number of seconds
            self.handleVideoInfo(info: info, callBack: callBackID!)
            
        }
    }
    
    func orbisCameraVC(_ controller: OrbisCameraViewController!, didFinishCapturingImage info: [AnyHashable : Any]!, withCallBackID callBackID: String!) {
        handleImageInfo(info: info, callBack: callBackID)
    }
    
    
    /************************************************************************************************/
    // MARK: - GalleryViewController Delegate
    
    func galleryViewControllerDidSelectedVideo(_ videoInfo: [AnyHashable : Any]!) {
        handleVideoInfo(info: videoInfo, callBack: nil)
    }
    
    func galleryViewControllerDidSelectedImage(_ imageInfo: [AnyHashable : Any]!) {
        handleImageInfo(info: imageInfo, callBack: nil)
    }
}
