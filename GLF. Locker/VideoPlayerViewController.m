#import "VideoPlayerViewController.h"
#import "OrbisMediaGallery.h"
//#import "AppDelegate.h"
//#import "MainViewController.h"
#import "MBProgressHUD.h"
#import "UploadRequest.h"
#import "GlobalClass.h"
@interface VideoPlayerViewController ()
{
    MBProgressHUD *hud;
    NSString * mediaFolderPath;
}
@property (nonatomic) CGFloat startTime;
@property (nonatomic) CGFloat stopTime;
@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property (strong, nonatomic) NSString *tmpVideoPath;

@end


@interface VideoPlayerViewController (Player)
- (void)removePlayerTimeObserver;
- (CMTime)playerItemDuration;
- (BOOL)isPlaying;
- (void)playerItemDidReachEnd:(NSNotification *)notification ;
- (void)observeValueForKeyPath:(NSString*) path ofObject:(id)object change:(NSDictionary*)change context:(void*)context;
- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys;
@end

static void *AVPlayerDemoPlaybackViewControllerRateObservationContext = &AVPlayerDemoPlaybackViewControllerRateObservationContext;
static void *AVPlayerDemoPlaybackViewControllerStatusObservationContext = &AVPlayerDemoPlaybackViewControllerStatusObservationContext;
static void *AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext = &AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext;

#pragma mark -

@implementation UINavigationController (Rotation_IOS6)

-(BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}

@end


@implementation VideoPlayerViewController
@synthesize mPlayerItem,scrollBgView;
- (void)viewDidLoad {
    [super viewDidLoad];
    overlayA = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view from its nib.
    
    // UIImage *sliderTrackImage = [[UIImage imageNamed: @"Slider.png"] stretchableImageWithLeftCapWidth: 7 topCapHeight: 0];
    showHide = NO;
    isSeeking = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClick:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
    
    [self.navigationController.navigationBar setHidden:YES];
    [trimBtn setHidden:YES];
    [titleLbl setText:@"Video Player"];
    
  
    
    if (self.videoInfo.count> 0) {
        
        if (self.isOnlineVideo) {
            
         
            [self.doneBtn setTitle:@"Close" forState:UIControlStateNormal];
            urlSt = [[self.videoInfo valueForKey:@"FileURL"]
                     stringByReplacingOccurrencesOfString:@"~" withString:@""];
            //mergingcode
//iOS13
            NSDictionary *dic = GlobalClass.sharedInstance.academyData;

            BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
            if ( isNew == NO){
                urlSt = [NSString stringWithFormat:@"%@%@",baseUrlForVideoOld,urlSt];
            }else{
                urlSt = [NSString stringWithFormat:@"%@%@",baseUrlForVideo,urlSt];
            }

            //temp
//            urlSt = [NSString stringWithFormat:@"%@",@"https://app.glflocker.com/Content/MediaFiles/Videos/GLF-1566225119-179771-60417-0.mp4"];

            fileURL = [NSURL URLWithString:urlSt];
            
            //  Get image from the video at the given time
            //     [self performSelector:@selector(setSizevideoController) withObject:nil afterDelay:<#(NSTimeInterval)#>];
            [self setSizevideoController:NO];
            [self addOverlaysToScrollView];
        }
        else
        {
            mediaFolderPath=[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath];
            fileURL = [NSURL fileURLWithPath:[mediaFolderPath stringByAppendingPathComponent:[[_videoInfo objectForKey:@"videoPath"] lastPathComponent]]];
            [self setSizevideoController:YES];
        }
    }
    
   [self addVideoPlayer:fileURL];
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading Video!";
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        
        [self addVideoRangeSlider:fileURL];
        [self initScrubberTimer];
        [self syncScrubber];
      [hud hide:YES];
   
        
    });
    
   UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc ]initWithTarget:self action:@selector(TapGesture)];
    [tapGesture setNumberOfTapsRequired:1];
    
    //   [self.videoController.view addGestureRecognizer:tapGesture];
    [vCView addGestureRecognizer:tapGesture];
    [tapGesture setDelegate:(id)self];
    // [self.view bringSubviewToFront:_playBtn];
    [self.view bringSubviewToFront:scrollBgView];
}



-(void)setSizevideoController:(BOOL )isLocalVideo
{
    
    if (isLocalVideo) {
        
        self.view.translatesAutoresizingMaskIntoConstraints = YES;
        vCView.translatesAutoresizingMaskIntoConstraints = YES;
        CGFloat mainViewWidth =  [[UIScreen mainScreen] bounds].size.width;
        CGFloat mainViewHeight =  [[UIScreen mainScreen] bounds].size.height;

        CMTime actualTime;
        NSError *error;
        AVAsset *asset = [AVAsset assetWithURL:fileURL];
        AVAssetImageGenerator   *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform=TRUE;
        CGImageRef halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
        
        while (halfWayImage  == nil) {
            halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
        }
        
        UIImage  *thumbnail = [[UIImage alloc]initWithCGImage:halfWayImage];
      
        NSLog(@"thumbnail size width %f and thumbnail size height %f",thumbnail.size.width,thumbnail.size.height);
        
        if (thumbnail.size.width >thumbnail.size.height  ) {
       
            CGFloat scale = mainViewWidth / thumbnail.size.width;
            CGFloat newHeight = thumbnail.size.height* scale;
            CGFloat originY = (mainViewHeight-newHeight)/2;
            
              NSLog(@"new height %f origin %f",newHeight,originY);
            
            //   CGFloat originX = (mainViewWidth-overlayWidth)/2;
            //   CGFloat originY = (mainViewHeight-overlayHeight)/2;
            [vCView setFrame:CGRectMake(0,originY , mainViewWidth, newHeight)];
            [layer setFrame:vCView.bounds];
            
        }
        else{
            
        if (thumbnail.size.height >= mainViewHeight) {
                
            
                   [vCView setFrame:CGRectMake(0  ,0 , mainViewWidth, mainViewHeight)];
            }
            else
            {
                
                
                CGFloat scale = mainViewWidth / thumbnail.size.width;
                CGFloat newHeight = thumbnail.size.height* scale;
                CGFloat originY = (mainViewHeight-newHeight)/2;
                
               [vCView setFrame:CGRectMake(0  , originY , mainViewWidth, newHeight)];
            
            }
             [layer setFrame:vCView.bounds];
        
        }

    }
    else{
        CGFloat videoWidth = [[self.videoInfo valueForKey:@"VideoWidth"] floatValue];
        CGFloat videoHeight = [[self.videoInfo valueForKey:@"VideoHeight"] floatValue];
        
        if (videoHeight > 0 && videoWidth > 0   ) {
            
            self.view.translatesAutoresizingMaskIntoConstraints = YES;
            vCView.translatesAutoresizingMaskIntoConstraints = YES;
            
            CGFloat mainViewWidth =  [[UIScreen mainScreen] bounds].size.width;
            CGFloat mainViewHeight =  [[UIScreen mainScreen] bounds].size.height;
            
            if ( videoHeight > mainViewHeight  && videoHeight > videoWidth ) {
                
                if (videoHeight > videoWidth) {
                    while (videoHeight > mainViewHeight) {
                        videoHeight = videoHeight/1.1;
                        videoWidth = videoWidth/1.1;
                        
                    }
                    
                    CGFloat scale = mainViewHeight / videoHeight;
                    CGFloat newWidth = videoWidth* scale;
                    CGFloat originX = (mainViewWidth-newWidth)/2;
                    
                    [vCView setFrame:CGRectMake(originX  ,0 , newWidth, mainViewHeight)];
                    [layer setFrame:vCView.bounds];
                    
                }
                
                else{
                    
                    [vCView setFrame:CGRectMake(0  ,0 , mainViewWidth, mainViewHeight)];
                    [layer setFrame:vCView.bounds];
                    
                }
                
            }
            else
            {
                CGFloat scale = mainViewWidth / videoWidth;
                CGFloat newHeight = videoHeight* scale;
                CGFloat originY = (mainViewHeight-newHeight)/2;
                
                //   CGFloat originX = (mainViewWidth-overlayWidth)/2;
                //   CGFloat originY = (mainViewHeight-overlayHeight)/2;
                [vCView setFrame:CGRectMake(0,originY , mainViewWidth, newHeight)];
                [layer setFrame:vCView.bounds];
            }
        }
        else
        {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
                //  int picWidth = 20;
                
//                MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
                //ios13
//                                MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];

                int tryLimit = 0;
                UIImage  *videoThumbnail;
                while (videoThumbnail  == nil && tryLimit < 3) {
                    //ios13
                    videoThumbnail  = [UIImage imageNamed:@"photoNotSmall"];//[player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
                    tryLimit = tryLimit +1;
                }
                
                if (videoThumbnail== nil) {
                    [UploadRequest uploadVideoDimentions:[[self.videoInfo valueForKey:@"MediaId"] intValue] andWidth:[[UIScreen mainScreen] bounds].size.width andHeight:[[UIScreen mainScreen] bounds].size.height];
                }
                else
                {
                    [UploadRequest uploadVideoDimentions:[[self.videoInfo valueForKey:@"MediaId"] intValue] andWidth:videoThumbnail.size.width andHeight:videoThumbnail.size.height];
                    
                }
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.view.translatesAutoresizingMaskIntoConstraints = YES;
                    vCView.translatesAutoresizingMaskIntoConstraints = YES;
                    
                    CGFloat mainViewWidth =  [[UIScreen mainScreen] bounds].size.width;
                    CGFloat mainViewHeight =  [[UIScreen mainScreen] bounds].size.height;
                    
                    
                    CGFloat thumnailWidth ;
                    CGFloat thumnailHeight;
                    
                    if (videoThumbnail == nil) {
                        
                        thumnailWidth = [[UIScreen mainScreen] bounds].size.width;
                        thumnailHeight = [[UIScreen mainScreen] bounds].size.height;
                        
                    }
                    else
                    {
                        
                        thumnailWidth = videoThumbnail.size.width;
                        thumnailHeight = videoThumbnail.size.height;
                    }
                    
                    if ( thumnailHeight > thumnailWidth && thumnailHeight > mainViewHeight) {
                        while (thumnailHeight > mainViewHeight) {
                            thumnailHeight = thumnailHeight/1.1;
                            thumnailWidth = thumnailWidth/1.1;
                            
                        }
                        
                        CGFloat scale = mainViewHeight / thumnailHeight;
                        CGFloat newWidth = thumnailWidth* scale;
                        CGFloat originX = (mainViewWidth-newWidth)/2;
                        
                        [vCView setFrame:CGRectMake(originX  ,0 , newWidth, mainViewHeight)];
                        [layer setFrame:vCView.bounds];
                    }
                    else
                    {
                        
                        while (thumnailWidth > mainViewWidth) {
                            thumnailHeight = thumnailHeight/1.1;
                            thumnailWidth = thumnailWidth/1.1;
                            
                        }
                        
                        CGFloat scale = mainViewWidth / thumnailWidth;
                        CGFloat newHeight = thumnailHeight* scale;
                        CGFloat originY = (mainViewHeight-newHeight)/2;
                        
                        //   CGFloat originX = (mainViewWidth-overlayWidth)/2;
                        //   CGFloat originY = (mainViewHeight-overlayHeight)/2;
                        [vCView setFrame:CGRectMake(0,originY , mainViewWidth, newHeight)];
                        [layer setFrame:vCView.bounds];
                    }
                    
                    
                    //   [self addVideoPlayer:fileURL];
                    
                    
                    //     [self.view bringSubviewToFront:self.playBtn];
                    
                    [activityView setHidden:YES];
                    [self.view bringSubviewToFront:scrollBgView];
                    [scrollBgView  setHidden:NO];
                    
                });
            });
        }
}
}

-(void)addOverlaysToScrollView
{
    
    if (self.isOnlineVideo) {
        NSArray *overlays = [self.videoInfo valueForKey:@"VideoOverlays"];
        scrollViewSize = 10;
        if (overlays == [NSNull null]) {
            return;
        }
        if (overlays.count > 0) {
            for (int i = 0; i < overlays.count+1; i++) {
                
                UIButton *overlayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                [overlayBtn setFrame:CGRectMake(scrollViewSize,  5, 50, srcView.frame.size.height-10)];
                [overlayBtn setTag:i];
                [overlayBtn addTarget:self action:@selector(displayOverlayOnVideo:) forControlEvents:UIControlEventTouchUpInside];
                [overlayBtn.layer setBorderWidth:2.0];
                [overlayBtn.layer setBorderColor:[UIColor blackColor].CGColor];
                if (i == 0) {
                    [overlayBtn setBackgroundImage:[UIImage imageNamed:@"no-image.png"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    //   [overlayBtn setBackgroundImage:[UIImage imageNamed:@"placeholder.jpg"] forState:UIControlStateNormal];
                    UIActivityIndicatorView *activityIndView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    [activityIndView setCenter:overlayBtn.center];
                    [activityIndView startAnimating];
                    [srcView addSubview:activityIndView];
                    [srcView bringSubviewToFront:activityIndView];
                    NSDictionary *overlayDic = [overlays objectAtIndex:i-1];
                    NSString *imgUrlSt = [overlayDic valueForKey:@"FileUrl"];
                    
                    
                    imgUrlSt = [imgUrlSt
                                stringByReplacingOccurrencesOfString:@"~" withString:@""];
                    
                    //mergingcode
                    //iOS13
                    NSDictionary *dic = GlobalClass.sharedInstance.academyData;
                    BOOL isNew = [[dic valueForKey:@"IsNewStore"] boolValue];
                    if ( isNew == NO){
                        imgUrlSt =[NSString stringWithFormat:@"%@%@",baseUrlForVideoOld,imgUrlSt];
                    }else{
                        imgUrlSt =[NSString stringWithFormat:@"%@%@",baseUrlForVideo,imgUrlSt];
                    }

                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                    dispatch_async(queue, ^(void) {
                        
                        UIImage *overlayImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrlSt]]];
                        
                        if (overlayImg) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [overlayBtn setBackgroundImage:overlayImg forState:UIControlStateNormal];
                                
                                [activityIndView stopAnimating];
                                [activityIndView setHidden:YES];
                            });
                        }
                    });
                }
                
                [overlayBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
                [srcView addSubview:overlayBtn];
                scrollViewSize = scrollViewSize + 60;
            }
        }
        
        else
        {
            
            
            
        //    [overlayLbl setHidden:NO];
            
            //  [playPauseBtn setHidden:YES];
            //       [scrollBgView setFrame:CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50) ];
            //    [srcView removeConstraint:srcView.constraints];
        }
        
    }
    
    [self.view bringSubviewToFront:scrollBgView];
}




- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    scrollView.contentSize = CGSizeMake(scrollViewSize, srcView.frame.size.height);

}

-(IBAction)playPuaseBtnClicked:(id)sender
{
    if(!playPauseBtn.selected)
    {
        [self playVideo:nil];
    }
    else
    {
        [self setResumeValue];
        [self stopPlayingVideo:1];
    }
    
//    playPauseBtn.selected=!playPauseBtn.selected;

    
/*
    if ([[playPauseBtn imageForState:UIControlStateNormal]isEqual:[UIImage imageNamed:@"play-icon.png"]]) {
        
        if (YES == seekToZeroBeforePlay)
        {
            seekToZeroBeforePlay = NO;
            [self.videoController seekToTime:kCMTimeZero];
        }
        
        [self.videoController play];
        [self initScrubberTimer];
        [self.playBtn setHidden:YES];
        [playPauseBtn setImage:[UIImage imageNamed:@"pause-icon.png"] forState:UIControlStateNormal];
        [self hide];
  

    }
    else
    {
        [playPauseBtn setImage:[UIImage imageNamed:@"play-icon.png"] forState:UIControlStateNormal];
        [self.videoController pause];
        [self.playBtn setHidden:NO];
        [self removePlayerTimeObserver];
    }
*/
}


-(void)setResumeValue
{

    if ( resumeVal < self.startTime  || resumeVal > self.stopTime) {
        resumeVal = self.startTime;
    }
    else{
        resumeVal =  CMTimeGetSeconds([self.videoController currentTime]);
}
}


-(IBAction)displayOverlayOnVideo:(id)sender
{

    UIButton *overlayBtn = (UIButton *)sender;
    
 
    if (overlayBtn.tag != 0) {
        UIImageView *overlayImg = [[UIImageView alloc]initWithImage:[overlayBtn backgroundImageForState:UIControlStateNormal]];
        [overlayImg setFrame:vCView.bounds];
        [vCView bringSubviewToFront:overlayImg];
        [vCView addSubview:overlayImg];
        [overlayA addObject:overlayImg];
        [vCView bringSubviewToFront:overlayImg];
     //   [self.view bringSubviewToFront:self.playBtn];
    }
    else
    {
       // [[overlayView subviews]
       //  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
        for (id obj in overlayA) {
            [obj removeFromSuperview];
        }
        
    }
}


-(void)addVideoPlayer:(NSURL *)url
{
    
    if ( layer != nil) {
        if (_mTimeObserver) {
            @try {
                [self.videoController removeTimeObserver:_mTimeObserver];
            } @catch (NSException *exception) {
                NSLog(@"Exception: %@", exception);
            }
        }
        
        
        self.videoController  = nil;
        [layer removeFromSuperlayer];
        layer = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    }

    self.videoController = [AVPlayer playerWithURL:url];
    layer = [AVPlayerLayer playerLayerWithPlayer:self.videoController];
    self.videoController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [layer setFrame:vCView.bounds];
    [vCView.layer addSublayer: layer];
    [layer setVideoGravity:AVLayerVideoGravityResize];
    
    /*
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
    
    NSArray *requestedKeys = @[@"playable"];
    
    
     Tells the asset to load the values of any of the specified keys that are not already loaded. */
  //  [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
    // ^{
       //  dispatch_async( dispatch_get_main_queue(),
      //                  ^{
                            /* IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem. */
    //                        [self prepareToPlayAsset:asset withKeys:requestedKeys];
  //                      });
//     }];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.videoController.currentItem];

}


- (void)viewWillDisappear:(BOOL)animated
{
    [self.videoController pause];
    
    [super viewWillDisappear:animated];
}
/*
 
 - (void)MPMoviePlayerLoadStateDidChange:(NSNotification *)notification
 {
 if ((self.videoController.loadState & MPMovieLoadStatePlaythroughOK) == MPMovieLoadStatePlaythroughOK)
 {
 if (!viewAdded) {
 NSLog(@"content play length is %g seconds", self.videoController.duration);
 [self.mySAVideoRangeSlider setMaxDuration:self.videoController.duration];
 [self.mySAVideoRangeSlider   addSliderView];
 viewAdded = YES;
 }
 
 }
 }
 */

-(void)TapGesture
{
    
    if (showHide) {
    
        [self show];
    }
    else
    {
        [self hide];
}
    
    

    
    
    /*
    [self.videoController pause];
    [self.playBtn setHidden:NO];
    [self removePlayerTimeObserver];
     */
}


-(void)show
{
    
    showHide =  NO;
    [sliderView setHidden:NO];
    [scrollBgView setHidden:NO];
    [self.view bringSubviewToFront:scrollBgView];
    
}

-(void)hide
{
    showHide = YES;
    [sliderView setHidden:YES];
    [scrollBgView setHidden:YES];


}


-(void)addVideoRangeSlider:(NSURL *)videoUrl
{
    if (self.mySAVideoRangeSlider!= nil) {
        [self.mySAVideoRangeSlider removeFromSuperview];
        self.mySAVideoRangeSlider = nil;
        
    }

    
    BOOL isOnine = NO ;
    
    if (self.isOnlineVideo) {
        isOnine =YES;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        self.mySAVideoRangeSlider = [[SAVideoRangeSlider alloc] initWithFrame:CGRectMake(0,  65,768, 45) videoUrl:videoUrl andIsOnline:isOnine];
        
    } else {
        
        self.mySAVideoRangeSlider = [[SAVideoRangeSlider alloc] initWithFrame:CGRectMake(0, 65, [[UIScreen mainScreen]bounds].size.width, 45) videoUrl:videoUrl andIsOnline:isOnine ];
        
    }
    
    
    self.mySAVideoRangeSlider.bubleText.font = [UIFont systemFontOfSize:12];
    [self.mySAVideoRangeSlider setPopoverBubbleSize:120 height:60];
    
    [self.mySAVideoRangeSlider setImages:@"w"];
   // Yellow
    self.mySAVideoRangeSlider.topBorder.backgroundColor = [UIColor blackColor];
    self.mySAVideoRangeSlider.bottomBorder.backgroundColor = [UIColor blackColor];
    
    self.mySAVideoRangeSlider.delegate = (id)self;
    [sliderView addSubview:self.mySAVideoRangeSlider];
    
    
    AVPlayerItem *currentItem = self.videoController.currentItem;
    CMTime duration = currentItem.asset.duration; //total time
    //    CMTime currentTime = currentItem.currentTime; //playing time
    
     durationVal = CMTimeGetSeconds(duration);
    while (durationVal ==0) {
         durationVal = CMTimeGetSeconds(duration);
    }
    
    

    self.startTime = 0.00;
    self.stopTime = durationVal;
    
    NSLog(@"duration val %f",durationVal);
    [self.mySAVideoRangeSlider setMaxDuration:durationVal];
    [self.mySAVideoRangeSlider   addSliderView];
 
}

-(void)removeVideoRangeSlider
{
    if (self.mySAVideoRangeSlider != nil) {
        [self.mySAVideoRangeSlider removeFromSuperview];
        self.mySAVideoRangeSlider = nil;
}}


- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    // Stop the video player and remove it from view
    [self.videoController pause];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - select button value
-(IBAction)selectBtn:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]init];
    [actionSheet addButtonWithTitle:@"Trim Original"];
    [actionSheet addButtonWithTitle:@"Save as New Clip"];
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setDelegate:(id)self];
    [actionSheet showInView:self.view];
    
}


-(IBAction)doneBtn:(id)sender{
    
    //ios13
//      [self.navigationController.navigationBar setHidden:false];
//      [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//     return;
    if ([self.doneBtn.titleLabel.text isEqualToString:@"Close"])
    {
       // self.navigationController.viewControllers.count
            [self.navigationController.navigationBar setHidden:false];
        NSLog(@"%ld",self.navigationController.viewControllers.count);
//        if (self.navigationController.isBeingPresented){
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
      //  }
       
       // [self.navigationController.topViewController dismissViewControllerAnimated:true completion:nil];
    }
    else
    {
        
        [trimBtn setHidden:YES];
        self.startTime = 0;
        self.stopTime = 0;
    
        [self.mySAVideoRangeSlider.sliderBgVw setFrame:CGRectMake(self.mySAVideoRangeSlider.leftThumb.frame.size.width, 0, self.mySAVideoRangeSlider.frame.size.width-(self.mySAVideoRangeSlider.leftThumb.frame.size.width +self.mySAVideoRangeSlider.rightThumb.frame.size.width), self.mySAVideoRangeSlider.sliderBgVw.frame.size.height)];
        [self.mySAVideoRangeSlider.sliderObj setFrame:CGRectMake(0, 0, self.mySAVideoRangeSlider.sliderBgVw.frame.size.width, self.mySAVideoRangeSlider.sliderBgVw.frame.size.height)];
        
        
        [self.mySAVideoRangeSlider.sliderObj setMinimumValue:0.0];
        [self.mySAVideoRangeSlider.sliderObj setMaximumValue:durationVal];
        [self.mySAVideoRangeSlider.sliderObj setValue:0.0 animated:YES];
        
        [self.doneBtn setTitle:@"Close" forState:UIControlStateNormal];
        [self.mySAVideoRangeSlider.sliderBgVw setHidden:NO];
        
        if (newLibraryPath.length > 0)
        {
            [self addVideoRangeSlider:[NSURL fileURLWithPath:newLibraryPath]];
        }
        else
        {
            
            [self addVideoRangeSlider:fileURL];
        }
    }
    
}


- (void)videoRange:(SAVideoRangeSlider *)videoRange didChangeLeftPosition:(CGFloat)leftPosition rightPosition:(CGFloat)rightPosition
{
    
    if (!isPause) {
        [self.videoController pause];
     //   [self.playBtn setHidden:NO];
       // [self.view bringSubviewToFront:self.playBtn];
        playPauseBtn.selected = NO;
        isPause = YES;
    }


    self.mySAVideoRangeSlider.topBorder.backgroundColor =[UIColor colorWithRed:255.0f/255.0f green:204.0f/255.0f blue:60.0f/255.0f alpha:1.0];
    self.mySAVideoRangeSlider.bottomBorder.backgroundColor =  [UIColor colorWithRed:255.0f/255.0f green:204.0f/255.0f blue:60.0f/255.0f alpha:1.0];
    
    [self.mySAVideoRangeSlider setImages:@"b"];
    // [self.mySAVideoRangeSlider setFrame:CGRectMake(50, 100, self.view.frame.size.width-100, 50)];
    [trimBtn setHidden:NO];
    self.startTime = leftPosition;
    self.stopTime = rightPosition;
}



- (void)videoRange:(SAVideoRangeSlider *)videoRange didGestureStateEndedLeftPosition:(CGFloat)leftPosition rightPosition:(CGFloat)rightPosition
{
   
}

#pragma mark - action sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        if (newVideoPath.length == 0) {
            newVideoPath = _videoInfo[@"videoPath"];
        }
        
        if (newVideoPath.length > 0) {
            self.videoPath = newVideoPath;
        }
        
        [self videoTriming];
        [[OrbisMediaGallery sharedGallery] deleteVideoFromGalleryAtPath:self.videoPath];
    }
    
    else if (buttonIndex == 1)
    {
        [self videoTriming];
    }
    
    else    if (buttonIndex == 2 ) {
        [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
    
    
}


#pragma mark - video Triming
-(void)videoTriming
{
    @try {
        [activityView setHidden:NO];
        [activityView startAnimating];
        [vCView bringSubviewToFront:activityView];
     //   [self.playBtn setHidden:YES];
        
        
        NSDate *time = [NSDate date];
        NSDateFormatter* df = [NSDateFormatter new];
        [df setDateFormat:@"dd-MM-yyyy-hh-mm-ss"];
        NSString *timeString = [df stringFromDate:time];
        NSString *newFileName = [NSString stringWithFormat:@"app-sibme-%@.%@", timeString, @"mp4"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        self.tmpVideoPath=[documentsDirectory stringByAppendingPathComponent:newFileName];
           newVideoPath =  [[OrbisMediaGallery sharedGallery] moveVideoToMyLibraryAtPath:self.tmpVideoPath];
        
        
        
        NSURL *videoFileUrl;
        if (newLibraryPath.length > 0) {
            videoFileUrl = [NSURL fileURLWithPath:newLibraryPath];
        }
        else
        {
            videoFileUrl = fileURL;
        
        }
        
       
       
        
        AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:videoFileUrl options:nil];
        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
        if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
            
            self.exportSession = [[AVAssetExportSession alloc]
                                  initWithAsset:anAsset presetName:AVAssetExportPresetPassthrough];
            // Implementation continues.
            
            NSURL *furl = [NSURL fileURLWithPath:newVideoPath];
            
            self.exportSession.outputURL = furl;
            self.exportSession.outputFileType = AVFileTypeMPEG4;
            
            CMTime start = CMTimeMakeWithSeconds(self.startTime, anAsset.duration.timescale);
            CMTime duration = CMTimeMakeWithSeconds(self.stopTime-self.startTime, anAsset.duration.timescale);
            CMTimeRange range = CMTimeRangeMake(start, duration);
            self.exportSession.timeRange = range;
            
            trimBtn.hidden = YES;
            
            [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
                
                switch ([self.exportSession status]) {
                    case AVAssetExportSessionStatusFailed:
                        NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                        break;
                    case AVAssetExportSessionStatusCancelled:
                        NSLog(@"Export canceled");
                        break;
                    default:
                        NSLog(@"NONE");
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            
                            
                            [[OrbisMediaGallery sharedGallery]saveVideoToGalleryAtPath:newVideoPath];
                            newLibraryPath=[[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath] stringByAppendingPathComponent:newVideoPath.lastPathComponent];
                         
                            
                            [self addVideoPlayer:[NSURL fileURLWithPath:newLibraryPath ]];
                            
                            [self removeVideoRangeSlider];
                            [self addVideoRangeSlider:[NSURL fileURLWithPath:newLibraryPath]];
                         //   [self.playBtn setHidden:NO];
                           // [self.view bringSubviewToFront:self.playBtn];
//                            [playPauseBtn setBackgroundImage:[UIImage imageNamed:@"play-icon.png"] forState:UIControlStateNormal];
                            
     
                            [self.doneBtn setTitle:@"Close" forState:UIControlStateNormal];
                            [activityView stopAnimating];
                            [activityView setHidden:YES];
                            
                            // TODO: Some callback
//                            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//                            MainViewController *mvc = (MainViewController*)appDelegate.window.rootViewController;
//                            [mvc.commandDelegate evalJs:@"window.orbisVideoGalleryPage.reset();"];
                            
                        });
                        
                        break;
                }
            }];
            
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@",exception);
    }
  
}



#pragma mark -  rotation delegates

- (BOOL) shouldAutorotate
{
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    
    return UIInterfaceOrientationPortrait;
}



-(void)doneButtonClick:(NSNotification*)aNotification{
    //  NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    //  currentTimerVal = self.videoController.currentPlaybackTime;
    //  [self.videoController.view removeFromSuperview];
    //  self.videoController = nil;
    //  [self performSelector:@selector(setFrameControlls) withObject:nil afterDelay:0.1];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/* The user is dragging the movie controller thumb to scrub through the movie. */
- (IBAction)beginScrubbing:(id)sender
{
    mRestoreAfterScrubbingRate = [self.videoController rate];
    [self.videoController setRate:0.f];
    
    
}






-(IBAction)scrub:(id)sender;

{
    
    
    
    /*
     
     CMTime tm = CMTimeMake(sliderVal, 1000);
     [self.videoController seekToTime:tm ];
     // [self.videoController setRate:0.5];
     
     
     //      [self.videoController.currentItem stepByCount:1];
     
     */
    if ([sender isKindOfClass:[UISlider class]] && !isSeeking)
    {
  
        scrubbing = YES;
        isSeeking = YES;
        UISlider* slider = (UISlider *) sender;
        
        [self setResumeValue];
     
      
            CMTime playerDuration = [self playerItemDuration];
            if (CMTIME_IS_INVALID(playerDuration)) {
                return;
            }
            
            double duration = CMTimeGetSeconds(playerDuration);
            if (isfinite(duration))
            {
                
                float minValue,maxValue,value;
                
                
                if ([slider value] > self.stopTime) {
                  
                [slider  setValue:self.stopTime animated:NO];
                }
                else if([slider value] < self.startTime)
                {
                    [slider  setValue:self.startTime animated:NO];
                    
                }
                
                
                
                minValue = [slider minimumValue];
                maxValue = [slider maximumValue];
                value = [slider value];
                
                double time = duration * (value - minValue) / (maxValue - minValue);
                
                /*
                 [self.videoController seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_MSEC) completionHandler:^(BOOL finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                 isSeeking = NO;
                 });
                 }];
                 */
                
                [self.videoController seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        isSeeking = NO;
                    });
                }];
                
       
 }
        
        
        
        

    }
    
    
}

- (IBAction)endScrubbing:(id)sender
{
    
    
    scrubbing = NO;
    if (!_mTimeObserver)
    {
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration))
        {
            return;
        }
        
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration))
        {
            CGFloat width = CGRectGetWidth([self.mySAVideoRangeSlider.sliderObj bounds]);
            double tolerance = 0.5f * duration / width;
            
            __weak VideoPlayerViewController *weakSelf = self;
            _mTimeObserver = [self.videoController addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC) queue:NULL usingBlock:
                             ^(CMTime time)
                             {
                                 [weakSelf syncScrubber];
                             }];
        }
    }
    
    if (mRestoreAfterScrubbingRate)
    {
        [self.videoController setRate:mRestoreAfterScrubbingRate];
        mRestoreAfterScrubbingRate = 0.f;
    }
    
    
}

- (BOOL)isScrubbing
{
    return mRestoreAfterScrubbingRate != 0.f;
}

-(void)enableScrubber
{
    self.mySAVideoRangeSlider.sliderObj.enabled = YES;
}

-(void)disableScrubber
{
    self.mySAVideoRangeSlider.sliderObj.enabled = NO;
}


- (void)syncScrubber
{
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration))
    {
        self.mySAVideoRangeSlider.sliderObj.minimumValue = 0.0;
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration))
    {
      CGFloat minValue ,maxValue, time ;
            minValue = [ self.mySAVideoRangeSlider.sliderObj minimumValue];
            maxValue = [ self.mySAVideoRangeSlider.sliderObj maximumValue];
            time = CMTimeGetSeconds([self.videoController currentTime]);
        
        //   double time  = [self.mySAVideoRangeSlider.sliderObj value];
        CGFloat  value = (maxValue - minValue) * time / duration + minValue;
         [ self.mySAVideoRangeSlider.sliderObj setValue:value animated:NO];
        
        
    
        NSString *stopTimeDecVal = [NSString stringWithFormat:@"%.01f",self.stopTime];
        NSString *valuObj = [NSString stringWithFormat:@"%.01f",value];
        
        if (self.startTime  > 0 && self.stopTime > 0 && !scrubbing) {
            
            if ( [stopTimeDecVal floatValue] == [valuObj floatValue]) {
              
                [self stopPlayingVideo:0];
                
    }
}
}
}



-(void)stopPlayingVideo:(int)resume
{
    if (resume == 0) {
         resumeVal = 0;
    }
    

        [self.videoController  pause];
    //    [self.playBtn setHidden:NO];
    
//    [self.view bringSubviewToFront:self.playBtn];
        [self removePlayerTimeObserver];
        playPauseBtn.selected=NO;
}


-(void)initScrubberTimer
{
    double interval = .1f;
    
    CMTime playerDuration = [self playerItemDuration];
    
    if (CMTIME_IS_INVALID(playerDuration))
    {
        return;
    }
    double duration = CMTimeGetSeconds(playerDuration);
    
 
    
    
    
    if (isfinite(duration))
    {
        CGFloat width = CGRectGetWidth([self.mySAVideoRangeSlider.sliderObj bounds]);
        interval = 0.5f * duration / width;
    }
    
    
    
    
    
    /* Update the scrubber during normal playback. */
    __weak VideoPlayerViewController *weakSelf = self;
    
  _mTimeObserver = [self.videoController addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                       queue:NULL /* If you pass NULL, the main queue is used. */
                                                                  usingBlock:^(CMTime time)
                     {
                         [weakSelf syncScrubber];
                         
                         
                     }];
}


- (CMTime)playerItemDuration
{
    AVPlayerItem *playerItem = [self.videoController currentItem];

    if (playerItem.status == AVPlayerItemStatusReadyToPlay)
    {
        return([playerItem duration]);
    }
    
    return(kCMTimeInvalid);
}


-(void)hidescrum
{
    
    [self.doneBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    
}


-(IBAction)playVideo:(id)sender
{
    
    if (YES == seekToZeroBeforePlay)
    {
        seekToZeroBeforePlay = NO;
        [self.videoController seekToTime:kCMTimeZero];
    }
    
    [self setResumeValue];
    
    if (self.startTime > 0 && self.stopTime > 0) {
        
        if (resumeVal> 0) {
            //ios 13
//            if (@available(iOS 13.0, *)) {
//
//            }else{
                [self.mySAVideoRangeSlider.sliderObj setValue:resumeVal animated:YES];
                
//            }
            [self.videoController seekToTime:CMTimeMakeWithSeconds(resumeVal, NSEC_PER_SEC) ];
        }
        else
        {
            [self.mySAVideoRangeSlider.sliderObj setValue:self.startTime animated:YES];
            [self.videoController seekToTime:CMTimeMakeWithSeconds(self.startTime, NSEC_PER_SEC) ];
        }
        
       
        isPause= NO;
        [self.mySAVideoRangeSlider.sliderBgVw setHidden:NO];
        
        [self.mySAVideoRangeSlider bringSubviewToFront:self.mySAVideoRangeSlider.leftThumb];
        [self.mySAVideoRangeSlider bringSubviewToFront:self.mySAVideoRangeSlider.rightThumb];
        /*
        CGFloat width = self.mySAVideoRangeSlider.rightThumb.frame.origin.x -  self.mySAVideoRangeSlider.leftThumb.frame.origin.x-self.mySAVideoRangeSlider.leftThumb.frame.size.width;
        
        
    [self.mySAVideoRangeSlider.sliderBgVw setFrame:CGRectMake(self.mySAVideoRangeSlider.leftThumb.frame.origin.x+self.mySAVideoRangeSlider.leftThumb.frame.size.width, self.mySAVideoRangeSlider.sliderBgVw.frame.origin.y, width, self.mySAVideoRangeSlider.sliderBgVw.frame.size.height)];
    [self.mySAVideoRangeSlider.sliderObj setFrame:CGRectMake(0, 0, self.mySAVideoRangeSlider.sliderBgVw.frame.size.width, self.mySAVideoRangeSlider.sliderBgVw.frame.size.height)];
    [self.mySAVideoRangeSlider.sliderObj setMinimumValue:0.00];
    [self.mySAVideoRangeSlider.sliderObj setMaximumValue:self.stopTime];
    [self.mySAVideoRangeSlider.sliderObj setValue:self.startTime animated:YES];
      */
        
    }
    
     [self initScrubberTimer];
     [self.videoController play];

   
    
 
    
 //   [self.playBtn setHidden:YES];
//    [playPauseBtn setBackgroundImage:[UIImage imageNamed:@"pause-icon.png"] forState:UIControlStateNormal];
    playPauseBtn.selected = YES;
    
  //  [self hide];
}


- (void)syncPlayPauseButtons
{
    /*
     if ([self isPlaying])
     {
     [self showStopButton];
     }
     else
     {
     [self showPlayButton];
     }
     */
}

-(void)enablePlayerButtons
{
    /*
     self.mPlayButton.enabled = YES;
     self.mStopButton.enabled = YES;
     */
}

-(void)disablePlayerButtons
{
    /*
     self.mPlayButton.enabled = NO;
     self.mStopButton.enabled = NO;
     */
}



@end






@implementation VideoPlayerViewController (Player)

#pragma mark Player Item

- (BOOL)isPlaying
{
    return mRestoreAfterScrubbingRate != 0.f || [self.videoController rate] != 0.f;
}

/* Called when the player item has played to its end time. */
- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    /* After the movie has played to its end time, seek back to time zero
     to play it again. */
    seekToZeroBeforePlay = YES;
    [self.videoController pause];
  //  [self.playBtn setHidden:NO];
   // [self.view bringSubviewToFront:self.playBtn];
    
    playPauseBtn.selected = NO;
//      [playPauseBtn setBackgroundImage:[UIImage imageNamed:@"play-icon.png"] forState:UIControlStateNormal];
    //ios13crash
//    if (@available(iOS 13.0, *)) {
//
//    }else{
    [self.mySAVideoRangeSlider.sliderObj  setValue:0.0 animated:YES];
//    }
  
}

/* ---------------------------------------------------------
 **  Get the duration for a AVPlayerItem.
 ** ------------------------------------------------------- */

- (CMTime)playerItemDuration
{
    AVPlayerItem *playerItem = [self.videoController currentItem];
    if (playerItem.status == AVPlayerItemStatusReadyToPlay)
    {
        return([playerItem duration]);
    }
    
    return(kCMTimeInvalid);
}


/* Cancels the previously registered time observer. */
-(void)removePlayerTimeObserver
{
    if (_mTimeObserver)
    {
        [self.videoController removeTimeObserver:_mTimeObserver];
        _mTimeObserver = nil;
    }
}

#pragma mark -
#pragma mark Loading the Asset Keys Asynchronously

#pragma mark -
#pragma mark Error Handling - Preparing Assets for Playback Failed

/* --------------------------------------------------------------
 **  Called when an asset fails to prepare for playback for any of
 **  the following reasons:
 **
 **  1) values of asset keys did not load successfully,
 **  2) the asset keys did load successfully, but the asset is not
 **     playable
 **  3) the item did not become ready to play.
 ** ----------------------------------------------------------- */

-(void)assetFailedToPrepareForPlayback:(NSError *)error
{
    [self removePlayerTimeObserver];
    [self syncScrubber];
    [self disableScrubber];
    [self disablePlayerButtons];
    
    /* Display the error. */
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                        message:[error localizedFailureReason]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}


#pragma mark Prepare to play asset, URL

/*
 Invoked at the completion of the loading of the values for all keys on the asset that we require.
 Checks whether loading was successfull and whether the asset is playable.
 If so, sets up an AVPlayerItem and an AVPlayer to play the asset.
 */
- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys
{
    /* Make sure that the value of each key has loaded successfully. */
    for (NSString *thisKey in requestedKeys)
    {
        NSError *error = nil;
        AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
        if (keyStatus == AVKeyValueStatusFailed)
        {
            [self assetFailedToPrepareForPlayback:error];
            return;
        }
        /* If you are also implementing -[AVAsset cancelLoading], add your code here to bail out properly in the case of cancellation. */
    }
    
    /* Use the AVAsset playable property to detect whether the asset can be played. */
    if (!asset.playable)
    {
        /* Generate an error describing the failure. */
        NSString *localizedDescription = NSLocalizedString(@"Item cannot be played", @"Item cannot be played description");
        NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made playable.", @"Item cannot be played failure reason");
        NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   localizedDescription, NSLocalizedDescriptionKey,
                                   localizedFailureReason, NSLocalizedFailureReasonErrorKey,
                                   nil];
        NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"StitchedStreamPlayer" code:0 userInfo:errorDict];
        
        /* Display the error to the user. */
        [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];
        
        return;
    }
    
    /* At this point we're ready to set up for playback of the asset. */
    
    /* Stop observing our prior AVPlayerItem, if we have one. */
    if (self.mPlayerItem)
    {
        /* Remove existing player item key value observers and notifications. */
        
        [self.mPlayerItem removeObserver:self forKeyPath:@"status" ];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:self.mPlayerItem];
    }
    
    /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
    self.mPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    /* Observe the player item "status" key to determine when it is ready to play. */
    [self.mPlayerItem addObserver:self
                       forKeyPath:@"status"
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:AVPlayerDemoPlaybackViewControllerStatusObservationContext];
    
    /* When the player item has played to its end time we'll toggle
     the movie controller Pause button to be the Play button */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.mPlayerItem];
    
    seekToZeroBeforePlay = NO;
    
    /* Create new player, if we don't already have one. */
    if (!self.videoController)
    {
        /* Get a new AVPlayer initialized to play the specified player item. */
        [self setPlayer:[AVPlayer playerWithPlayerItem:self.mPlayerItem]];
        
        /* Observe the AVPlayer "currentItem" property to find out when any
         AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did
         occur.*/
        [self.player addObserver:self
                      forKeyPath:@"currentItem"
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.player addObserver:self
                      forKeyPath:@"rate"
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:AVPlayerDemoPlaybackViewControllerRateObservationContext];
    }
    
    /* Make our new AVPlayerItem the AVPlayer's current item. */
    if (self.player.currentItem != self.mPlayerItem)
    {
        /* Replace the player item with a new player item. The item replacement occurs
         asynchronously; observe the currentItem property to find out when the
         replacement will/did occur
         
         If needed, configure player item here (example: adding outputs, setting text style rules,
         selecting media options) before associating it with a player
         */
        [self.videoController replaceCurrentItemWithPlayerItem:self.mPlayerItem];
        
        [self syncPlayPauseButtons];
    }
    
    [self.mySAVideoRangeSlider.sliderObj setValue:0.0];
}

#pragma mark -
#pragma mark Asset Key Value Observing
#pragma mark

#pragma mark Key Value Observer for player rate, currentItem, player item status

/* ---------------------------------------------------------
 **  Called when the value at the specified key path relative
 **  to the given object has changed.
 **  Adjust the movie play and pause button controls when the
 **  player item "status" value changes. Update the movie
 **  scrubber control when the player item is ready to play.
 **  Adjust the movie scrubber control when the player item
 **  "rate" value changes. For updates of the player
 **  "currentItem" property, set the AVPlayer for which the
 **  player layer displays visual output.
 **  NOTE: this method is invoked on the main queue.
 ** ------------------------------------------------------- */

- (void)observeValueForKeyPath:(NSString*) path
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    /* AVPlayerItem "status" property value observer. */
    if (context == AVPlayerDemoPlaybackViewControllerStatusObservationContext)
    {
        [self syncPlayPauseButtons];
        
        AVPlayerItemStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            case AVPlayerItemStatusUnknown:
            {
                [self removePlayerTimeObserver];
                [self syncScrubber];
                
                [self disableScrubber];
                [self disablePlayerButtons];
            }
                break;
                
            case AVPlayerItemStatusReadyToPlay:
            {
                /* Once the AVPlayerItem becomes ready to play, i.e.
                 [playerItem status] == AVPlayerItemStatusReadyToPlay,
                 its duration can be fetched from the item. */
                
                [self initScrubberTimer];
                
                [self enableScrubber];
                [self enablePlayerButtons];
            }
                break;
                
            case AVPlayerItemStatusFailed:
            {
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self assetFailedToPrepareForPlayback:playerItem.error];
            }
                  break;
      
        }
    }
    /* AVPlayer "rate" property value observer. */
    else if (context == AVPlayerDemoPlaybackViewControllerRateObservationContext)
    {
        [self syncPlayPauseButtons];
    }
    /* AVPlayer "currentItem" property observer.
     Called when the AVPlayer replaceCurrentItemWithPlayerItem:
     replacement will/did occur. */
    else if (context == AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext)
    {
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* Is the new player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
            [self disablePlayerButtons];
            [self disableScrubber];
        }
        else /* Replacement of player currentItem has occurred */
        {
            /* Set the AVPlayer for which the player layer displays visual output. */
            //   [self.mPlaybackView setPlayer:mPlayer];
            
            //  [self setViewDisplayName];
            
            /* Specifies that the player should preserve the video’s aspect ratio and
             fit the video within the layer’s bounds. */
            //     [self.mPlaybackView setVideoFillMode:AVLayerVideoGravityResizeAspect];
            
            [self syncPlayPauseButtons];
        }
    }
    else
    {
        [super observeValueForKeyPath:path ofObject:object change:change context:context];
    }
}

-(void) dealloc {
    
    if (_mTimeObserver) {
        @try {
            [self.videoController removeTimeObserver:_mTimeObserver];
        } @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
        }
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self];
   
}
@end
