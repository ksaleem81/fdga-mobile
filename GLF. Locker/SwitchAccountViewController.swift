//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

import SDWebImage
import SideMenu

@available(iOS 10.0, *)
class SwitchAccountViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var dataGeneral:[[String:AnyObject]] = [[String:Any]]() as [[String : AnyObject]]
     var previousData = [String:AnyObject]()
    var studentData = [String:AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    var secondLastControllerData = [String:AnyObject]()
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var checkOutBtn: UIButton!
    var activeAccountIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkOutBtn.isHidden = false
       fetchingAccountsData(cartKey: "allAccounts")
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    
    func fetchingAccountsData(cartKey:String)  {
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: cartKey){
            if let loadedCart =  NSKeyedUnarchiver.unarchiveObject(with: (UserDefaults.standard.object(forKey: cartKey) as? Data)!) as? [[String:Any]]{
                dataGeneral =  loadedCart as [[String : Any]] as [[String : AnyObject]]
            }
        }
        
        detectingActiveAccount()
    }
    
    func detectingActiveAccount()  { 
      
        var userId = 0
        var academyId = 0
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = id
        }
        
        if let acadmy =   DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyId = acadmy
        }
        
        var i = 0
        for item in dataGeneral {
            
            if let userDic = item["loginUser"] as? [String:AnyObject]{
                
                if let id = userDic["Userid"] as? Int{
                    if let academyDic = item["academy"] as? [String:AnyObject]{
                        if let academyID = academyDic["AcademyID"] as? Int{
                            if userId == id && academyID == academyId {
                                activeAccountIndex = i
                            }
                        }
                    }
                }
                
            }
            i = i + 1
        }
    }
    
    func registeringControllerToSideMenu()  {
    
        var tableIdentifier = ""
        if currentUserLogin == 4 {
            tableIdentifier = "SideMenuTableViewController"
        }else {
            tableIdentifier = "SideMenuForCoachTableViewController"
        }
        let conroler = self.storyboard?.instantiateViewController(withIdentifier: tableIdentifier)
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController:conroler!)
        SideMenuManager.menuRightNavigationController = menuRightNavigationController
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataGeneral.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchAccountTableViewCell", for: indexPath) as! SwitchAccountTableViewCell
        
        if let userDic = dataGeneral[indexPath.row]["loginUser"] as? [String:AnyObject]{
            if let name = userDic["UserName"] as? String{
                cell.userNameLbl.text = name
                
            }
          
        }
        
        if let userDic = dataGeneral[indexPath.row]["academy"] as? [String:AnyObject]{
            if let name = userDic["AcademyName"] as? String{
                cell.academyLbl.text = name
                
            }
        }
        
        
        
        if activeAccountIndex == indexPath.row{
            cell.stausLbl.text = "currently active"
            cell.deleteBtn.isHidden = true
        }else{
            cell.stausLbl.text = ""
            cell.deleteBtn.isHidden = false
        }
        
        
   
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action:#selector(deleteBtnAction(sender:)) , for:.touchUpInside)
   
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if indexPath.row == activeAccountIndex{
            return
        }else{
            
            if let academyDic = dataGeneral[indexPath.row]["academy"] as? [String:AnyObject]{
                DataManager.sharedInstance.updateAcademyDetails(academyDic as NSDictionary?)
            }
            if let userDic = dataGeneral[indexPath.row]["loginUser"] as? [String:AnyObject]{
                DataManager.sharedInstance.updateLoginDetails(userDic as NSDictionary?)
            }
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.keepMeLoginOn = false
            appDelegate.showDashBoard()

        }
    }
    
    //MARK:- Buttons Actions
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
       _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func checkOutBtnAction(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showInitialController()
    }
    
    @objc func deleteBtnAction(sender:UIButton)    {
        
           dataGeneral = DataManager.sharedInstance.deletingAccountItem(keyName: "allAccounts", sender: sender.tag)
        //just added below method call
           fetchingAccountsData(cartKey: "allAccounts")
        //till here
             self.tableView.reloadData()
      
    }
    
    @IBAction func rightMenuBtnAction(_ sender: UIBarButtonItem) {
    
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
   
}

