//
//  FiltersViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/24/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SideMenu
class LessonTagsViewController: UIViewController {

    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!//200
    @IBOutlet weak var parentViewHeightConstraint: NSLayoutConstraint!
    var dataArray = [[String:AnyObject]]()
    var filterCategoryTrack = [[String:AnyObject]]()
    var jsonStringForSkillsTypes = "1"
    var jsonStringForCetegory = ""
    var selectedItems = [[String:AnyObject]]()
    var forPlayerTypeIdData  = [String:AnyObject]()
    var classPlayerData = [[String:AnyObject]]()
     var previousData = [String:AnyObject]()
    var studentTrackData = [[String:AnyObject]]()
    var selecteStduentId = "0"
    @IBOutlet weak var playerLbl: UILabel!
    @IBOutlet weak var playerViewHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelLine: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
      tableViewHeightConstraint.constant = 0
      
        if currentUserLogin == 4 {
            playerViewHieghtConstraint.constant = 0
            saveBtn.isHidden = true
            labelLine.isHidden = true
            self.cancelBtn.translatesAutoresizingMaskIntoConstraints = true
            self.cancelBtn.frame = CGRect(x: 0, y: 0, width:((self.view?.frame.size.width)!-20), height: 40)
        }
        
        if let type = previousData["ProgramType"] as? String {
            
                if type == "2" {
                    
                    saveBtn.isHidden = true
                    labelLine.isHidden = true
                    self.cancelBtn.translatesAutoresizingMaskIntoConstraints = true
                    self.cancelBtn.frame = CGRect(x: 0, y: 0, width:((self.view?.frame.size.width)!-20), height: 40)
                    getClassPlayer()
                }else{
                    playerViewHieghtConstraint.constant = 0
                    
            }
        }

        getSkillCategories()
        getSkillTypeByLesson()
            settingRighMenuBtn()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if currentUserLogin == 4{
            
        }else{
            self.tabBarController?.navigationController?.isNavigationBarHidden = true
        }

    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(LessonTagsViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getSkillCategories()  {
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        var playerTypId = ""
        if currentUserLogin == 4 {
            
            if let id = DataManager.sharedInstance.currentUser()!["PlayerTypeId"] as? Int{
                playerTypId = "\(id)"
            }

        }else{
        if let id = forPlayerTypeIdData["PlayerTypeId"] as? Int{
            playerTypId = "\(id)"
            }
        }
        
        var methodToHit = "GetSkillCategoriesAndSkillsById"
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
        if let id = previousData["LessonID"] as? Int{
            playerTypId = "\(id)"
                }
                methodToHit = "GetSkillCategoriesAndSkillsByIdClass"
            }
        }
    
        //Academy/GetSkillCategoriesAndSkillsById/73/388
        NetworkManager.performRequest(type:.get, method: "Academy/\(methodToHit)/\(acadmyId)/\(playerTypId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
                
            default:
                
                break
                
                
            }
            
            //validtation if gamesScore not set
           self.dataArray =  object as! [[String : AnyObject]]
            if currentUserLogin == 4 {
                self.defaultSelectedOption()
            }else{
                if let type = self.previousData["ProgramType"] as? String {
                    if type == "2" {
                        //if class no default selection
                    }else{
                        self.defaultSelectedOption()
                    }
                }

            }
            if self.dataArray.count == 1 {
                    let dic = self.dataArray[0]
                    if let levelCount = dic["SkillCategoryID"] as? Int{
                        if levelCount == 0 {
                       _ = self.navigationController?.popViewController(animated: true)
                    DataManager.sharedInstance.printAlertMessage(message: "Please select skill level for student on platform ", view: UIApplication.getTopestViewController()!)
                        }
                    }
                }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    
    func defaultSelectedOption()  {
        
        if self.dataArray.count > 2{
        if let id = self.dataArray[1]["SkillCategoryID"] as? Int{
            self.jsonStringForCetegory = "\(id)"
        }
        if let val = dataArray[1]["SkillCategoryName"] as? String{
                self.filterBtn.setTitle("\(val)", for: .normal)
        }
        if let skillsArray = self.dataArray[1]["Skills"] as? [[String:AnyObject]]{
            if skillsArray.count > 0{
                self.tableViewHeightConstraint.constant = 200
                let tv : AssignTagsViewController = self.childViewControllers[0] as! AssignTagsViewController
                tv.previousData = self.previousData
                tv.dataArray = skillsArray
                tv.selectedItems = self.selectedItems
                tv.delegate = self
                tv.viewWillAppear(true)
                
            }else{
                self.tableViewHeightConstraint.constant = 0
                
            }
        }
        
        }

    }
    
    func getSkillTypeByLesson()  {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        
        var requestType = "GetSkillTypesByLessonID"
        
        if let type = previousData["ProgramType"] as? String {
            if type == "2" {
                var idTosend = ""
                if self.selecteStduentId == "0" {
                    idTosend = "0"
                }else{
                    idTosend = self.selecteStduentId
                }
              lessonID = lessonID + "/" + "\(self.selecteStduentId)"
                  requestType = "GetSkillTypesByClassId"
            }
        }
     
        NetworkManager.performRequest(type:.get, method: "academy/\(requestType)/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)

                return
                
            default:
                break
            }
            
            self.selectedItems = [[String:AnyObject]]()
            let data =  object as! [[String : AnyObject]]
           
            for index in data{
                var dic = [String:AnyObject]()
                
                if let val = index["SkillTypeName"] as? String{
                    dic["SkillName"] = val as AnyObject?
                }
                if let val = index["SkillTypeID"] as? Int{
                    dic["SkillID"] = val as AnyObject?
                }
                self.selectedItems.append(dic)
            }
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    
    func settingPickerView() {
     
        var dataAray = [String]()
        filterCategoryTrack = [[String:AnyObject]]()
        for dic in dataArray{
            if let val = dic["SkillCategoryName"] as? String{
                
                if val == "Miscellaneous"{
                    continue
                }
                
                filterCategoryTrack.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT CATEGORY", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
          
            if indexes == nil{
                return
            }
            
            self.filterBtn.setTitle("\(indexes!)", for: .normal)

            if let id = self.filterCategoryTrack[values]["SkillCategoryID"] as? Int{
                self.jsonStringForCetegory = "\(id)"
            }
            if let skillsArray = self.filterCategoryTrack[values]["Skills"] as? [[String:AnyObject]]{
                if skillsArray.count > 0{
                     self.tableViewHeightConstraint.constant = 200
                    let tv : AssignTagsViewController = self.childViewControllers[0] as! AssignTagsViewController
                    tv.previousData = self.previousData
                   tv.dataArray = skillsArray
                    var selectedOrNot = false
                    
                    for index in skillsArray{
                        if let id = index["SkillID"] as? Int{
                            if self.selectedItems.count > 0{
                            if let id2 = self.selectedItems[0]["SkillID"] as? Int{
                                if id == id2{
                                    selectedOrNot = true
                                }
                                }
                            }else{
                                selectedOrNot = false
                            }
                        }
                    }
                    
                    if selectedOrNot{
                          tv.selectedItems = self.selectedItems
                    }else{
                       // tv.selectedItems = [[String:AnyObject]]()
                         tv.selectedItems = self.selectedItems
                    }
                    
                    //
                    tv.delegate = self
                    tv.viewWillAppear(true)

                }else{
                     self.tableViewHeightConstraint.constant = 0
                    
                }
                //self.selectedPaymentType = "\(selectedTyp)"
            }
            
          
        
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: filterBtn)
    
    }
 
    
    @IBAction func filterBtnAction(_ sender: UIButton) {
        settingPickerView()
    }
    @IBAction func applyFitlerBtnAction(_ sender: UIButton) {
       saveForm()

    }
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func saveForm()  {
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        
        //https://app.glflocker.com/OrbisAppBeta/api/academy/InsertLessonSkills/40801/2089$5232,5233,5234
        
        NetworkManager.performRequest(type:.get, method: "academy/InsertLessonSkills/\(lessonID)/\(jsonStringForCetegory)\("$")\(jsonStringForSkillsTypes)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                
                break
                
            }
              _=self.navigationController?.popViewController(animated: true)
            DataManager.sharedInstance.printAlertMessage(message:"Successfully Updated", view:UIApplication.getTopestViewController()!)
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }

    
    func getClassPlayer()  {
        
        
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        //    var typId = "0"
        //        if let type = previousData["ProgramType"] as? String {
        //            typId = type
        //            }
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                break
                
                // should never happen!
            }
            
            self.classPlayerData = (object as? [[String:AnyObject]])!
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    

    
    
    @IBAction func resetFilterBtnAction(_ sender: UIButton) {
      
    
    }
    @IBAction func selectPlayerBtnAction(_ sender: UIButton) {
        
        var dataAray = [String]()
        studentTrackData = [[String:AnyObject]]()
          //becauas all option not return in json
        dataAray.append("ALL")
        let dic = ["0":"StudentID"]
        studentTrackData.append(dic as [String : AnyObject])
        for dic in classPlayerData{
            if let val = dic["StudentName"] as? String{
                studentTrackData.append(dic)
                dataAray.append(val)
            }
        }
        
        ActionSheetStringPicker.show(withTitle: "SELECT PLAYER", rows:dataAray, initialSelection: 0, doneBlock: {
            picker, values, indexes in
            self.playerLbl.text = "\(indexes!)"
            if let selectedTyp = self.studentTrackData[values]["StudentID"] as? Int{
                self.selecteStduentId = "\(selectedTyp)"
            }
            //becauas all option not return in json
            if "\(indexes)" == "ALL"{
                self.selecteStduentId = "0"
            }
            
            
            self.getSkillTypeByLesson()
            self.getSkillCategories()
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }

}
extension LessonTagsViewController : SelectedSkillsDelegateForTags{
    func seletedSkillsData(selectedData: [[String:AnyObject]], selectedUserId: String) {
        print("yes")
        
        print(selectedData)
      jsonStringForSkillsTypes = ""
        for index in selectedData {
            if let value  =  index["SkillID"] as? Int {
                jsonStringForSkillsTypes = jsonStringForSkillsTypes +  "\(value)" + ","
            }
        }
        
        if jsonStringForSkillsTypes != "" {
            jsonStringForSkillsTypes = jsonStringForSkillsTypes.substring(to: jsonStringForSkillsTypes.index(before: jsonStringForSkillsTypes.endIndex))
        }
        
        print(jsonStringForSkillsTypes)
        
    }
}
//after select Player
//https://app.glflocker.com/OrbisAppBeta/api/Academy/GetSkillCategoriesAndSkillsByIdClass/73/8758
//https://app.glflocker.com/OrbisAppBeta/api/academy/GetSkillTypesByClassId/8758/4689

//https://app.glflocker.com/OrbisAppBeta/api/academy/InsertLessonSkills///46510/2096$5254,5255,5256,5257,5258,5259!2097$5260,5261,5262,5263,5264,5265!//2098$5266,5267,5268,5269,5270,5271
//https://app.glflocker.com/OrbisAppBeta/api/academy/GetSkillTypesByLessonID/46510
//https://app.glflocker.com/OrbisAppBeta/api/academy/GetPlayerSkillTypesByClassId/9992/4689
//1458
