//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu

class GroupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    var previousData = [String:AnyObject]()
    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
     var latitude = 0.00
     var longitude = 0.00
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var fromClassAttendess = false
    var map:GMSMapView?
    @IBOutlet weak var searchFld: UITextField!
    @IBOutlet weak var allStudentBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
    @IBOutlet weak var sendBtnHeightCons: NSLayoutConstraint!
    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        searchFld.inputAccessoryView = addToolBar()
        if currentUserLogin == 4 {
            
        }else{
            if fromClassAttendess {
                self.navigationItem.title = "CLASS ATTENDEES"
                getClassAttendees()
              
            }else{
                sendBtnHeightCons.constant = 0
                refreshTableView()
                searchView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:50 )
                self.tableView.contentInset = .init(top: 0, left: 0, bottom: 50, right: 0)
            }
        }
        
        searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
 
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(GroupViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if fromClassAttendess {
            
            if currentUserLogin == 4{
                
            }else{
                self.tabBarController?.navigationController?.isNavigationBarHidden = true
            }
            
            
        }else{
            self.tabBarController?.navigationItem.title  = "STUDENTS"
            if #available(iOS 11, *) {
                self.navigationController?.isNavigationBarHidden = true
            }else{
            }
            self.tabBarController?.navigationController?.isNavigationBarHidden = false
            
            let view1 = self.tabBarController?.view.viewWithTag(999)
            view1?.isHidden = false
        }

    }

    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "StudentName CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        
        print ("filterd = \(data)")
        
       self.tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(GroupViewController.donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar
    }
    //MARK:- buttons Actions
    @IBAction func allStudentsBtnAction(_ sender: UIButton) {
        
        if originaldata.count < 1{
            let name: String = "No Attendee In the Class"
            
            let alertController = UIAlertController(title: "Error", message: name, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }else {
        }
        
        
        blurView.frame = self.view.bounds
        blurView.tag = 1010
        self.view.addSubview(blurView)
        presentTableViewAsPopOver(sender: sender)
    }
    @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
       
     print(data.count)
        searchFld.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    //MARK:- fetching server data
    func refreshTableView()  {
        
        NetworkManager.performRequest(type:.get, method: "academy/SelectGroupStudents/\(DataManager.sharedInstance.currentUser()!["coachId"] as AnyObject)", parameter:nil, view: self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
                
            // do one thing
            case _ as [[String:AnyObject]]: break
          
            default:
                     DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                     return
            }

            self.data = object as! [[String : AnyObject]]
              DispatchQueue.main.async {
            self.originaldata = object as! [[String : AnyObject]]
            let lab = self.tabBarController?.view.viewWithTag(444) as? UILabel
            lab?.text = "\(self.originaldata.count)"
                self.tableView.reloadData()
            }
     
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func getClassAttendees()  {
        
        print(previousData)
        var lessonID = ""
        if let id = previousData["LessonID"] as? Int{
            lessonID = "\(id)"
        }
        NetworkManager.performRequest(type:.get, method: "Academy/GetClassPlayers/\(lessonID)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
                
            default:
                break
            }
            
            self.data = object as! [[String : AnyObject]]
            self.originaldata = object as! [[String : AnyObject]]
            self.tableView.reloadData()
            
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupTableViewCell", for: indexPath) as! GroupTableViewCell
        if let title = data[indexPath.row]["StudentName"] as? String{
           cell.companyLbl.text = title
        }
        
        if fromClassAttendess {
            cell.callImage.isHidden = false

            cell.phoneLbl.isHidden = false
            if let title = data[indexPath.row]["PhoneNo"] as? String{
                cell.phoneLbl.text = title
            }
            cell.arrowImg.isHidden = true
            cell.callBtn.tag = indexPath.row
            cell.callBtn.addTarget(self, action: #selector(GroupViewController.callBtnTap(btn:)), for: .touchUpInside)
            
            cell.deletBtn.isHidden = false
            cell.deletBtn.tag = indexPath.row
            cell.deletBtn.addTarget(self, action: #selector(GroupViewController.deletBtnAction(btn:)), for: .touchUpInside)

        }else{
            cell.callImage.isHidden = true
            cell.phoneLbl.isHidden = true
            cell.deletBtn.isHidden = true
        }
        
        if let url = data[indexPath.row]["PhotoURL"] as? String{
            var originalUrl = url.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            originalUrl = imageBaseUrl + originalUrl
            cell.studentImage.sd_setImage(with: NSURL(string: originalUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
                
                DispatchQueue.main.async (execute: {

                    if let _ = image{
                        
                        cell.studentImage.image = image;
                    }
                    else{
                        cell.studentImage.image = UIImage(named:"photoNot")
                    }
                });
                
            })
          
        }
        return cell
    }
    
    @objc func deletBtnAction(btn:UIButton)  {
        
        let alertController = UIAlertController(title: "Alert", message: "Are You Sure You Want to Delete Class attendee?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.deleteAttendyFromClass(tag: btn.tag)
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    func deleteAttendyFromClass(tag:Int) {
        
       
         var lessonID = ""
         var studentID = ""

         if let id = previousData["LessonID"] as? Int{
         lessonID = "\(id)"
         }

        if let studentidentifer = data[tag]["StudentID"] as? Int{
            studentID = "\(studentidentifer)"
        }
        
        var parameters : [String:String] = [String:String]()
        parameters = ["PlayerId":studentID,"ClassId":lessonID]
        print(parameters)

        NetworkManager.performRequest(type:.post, method: "Student/DeleteClassPlayer", parameter:parameters as [String : AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: return
            default:
                break
            }
            self.getClassAttendees()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    @objc func callBtnTap(btn:UIButton)  {
     
        if let number = data[btn.tag]["PhoneNo"] as? String{
            if let url = URL(string: "telprompt://\(number)") {
                UIApplication.shared.openURL(url)
              }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if fromClassAttendess {
        return 70
        }else{
            return 60.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        if fromClassAttendess{
        }else{
        let currentCell = tableView.cellForRow(at: indexPath) as! GroupTableViewCell
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailsViewController") as! StudentDetailsViewController
        if let id = data[indexPath.row]["StudentID"] as? Int{
            print(id)
            controller.studentData = data[indexPath.row]
           controller.userImageData = currentCell.studentImage.image!
            controller.studentTitle = currentCell.companyLbl.text!
        }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func presentTableViewAsPopOver(sender:UIButton) {
        
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "ComposeMessageViewController") as! ComposeMessageViewController

        var receiversIds = ""
        for index in originaldata {
            if let name =  index["UserID"] as? Int {
            receiversIds = receiversIds + "\(name)" + ","
                }
            }
        
        if receiversIds != "" {
            receiversIds = receiversIds.substring(to: receiversIds.index(before: receiversIds.endIndex))
        }
        
        menuViewController.idToSend = receiversIds
        menuViewController.blureView = blurView
        menuViewController.NameToSend = "All Attendees"
        menuViewController.isForLessonOrClass = true
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: self.view.frame.size.width-20, height: 440)
        
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .init(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.view as UIView
        popoverMenuViewController?.sourceRect =
            CGRect(
                x: sender.bounds.size.width/2,
                y: sender.bounds.origin.y+sender.bounds.size.height,
                width: 1,
                height: 1)
        present(
            menuViewController,
            animated: true,
            completion: nil)
    }
    

    
}
//MARK:- popover delegate 
extension GroupViewController:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return 	UIModalPresentationStyle.none
    }
    internal func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
        self.blurView.removeFromSuperview()
    }
}

