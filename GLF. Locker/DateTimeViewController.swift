//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import ActionSheetPicker_3_0
import SideMenu
class DateTimeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    var dataGeneral:[[String:AnyObject]] = [[String:AnyObject]]()
    var selectedData = [String:AnyObject]()
    var redeemedData = [String:AnyObject]()
    var previousData = [String:AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    var classController = false
    @IBOutlet weak var searchFld: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var availbleLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var monthYearView: MonthYearPickerView!
    //DateTimeViewController
    var dayDatePickerView = DayDatePickerView()
    var inputView1 = UIView()
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    var selectedCoachData = [String:AnyObject]()
     var isRedeemedVoucher = false
    @IBOutlet weak var datePickerView: UIView!
    var currentSelectedIndex = -1
    //MARK:- life cycles methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let formatter = DateFormatter()
        print(previousData)
        formatter.dateFormat = "dd MMM, yyyy"
     print(isRedeemedVoucher)
        let TodayDate = formatter.string(from: date)
        if classController {
            formatter.dateFormat = "MMM yyyy"
            let TodayDateForLbl = formatter.string(from: date)
            dateLbl.text! = TodayDateForLbl
            hidingHeaderViewButton()
            refreshTableViewForClass(TodayDate: TodayDate)
            self.monthYearView.isHidden = false
            
        }else{
            dateLbl.text! = TodayDate
            if currentUserLogin == 4 {
                hidingHeaderViewButton()
            }else{
            }
            refreshTableView(TodayDate: TodayDate)
        }
        
        self.navigationController?.isNavigationBarHidden = false
        settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func addDayPicker (){
        
        dayDatePickerView =  DayDatePickerView.init(frame: CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 210))
        let doneButton = UIButton(frame: CGRect(x:self.view.frame.width - 100, y:5, width:100, height:30))
        doneButton.setTitle("Done", for: UIControlState.normal)
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        doneButton.setTitleColor(UIColor.init(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0), for: .normal)
        inputView1 = UIView(frame: CGRect(x:0, y:self.view.frame.height - 250, width:self.view.frame.width, height:250))
        inputView1.backgroundColor = UIColor.white
        inputView1.addSubview(doneButton) // add Button to UIView
        doneButton.addTarget(self, action: #selector(DateTimeViewController.doneBtnAction), for: UIControlEvents.touchUpInside)
        let cancelBtn = UIButton(frame: CGRect(x:10, y:5, width:100, height:30))
        cancelBtn.setTitle("CANCEL", for: UIControlState.normal)
        
        cancelBtn.setTitleColor(UIColor.init(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0), for: .normal)
        cancelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        inputView1.addSubview(cancelBtn) // add Button to UIView
        cancelBtn.addTarget(self, action: #selector(DateTimeViewController.cancelBtnAction), for: UIControlEvents.touchUpInside)
        //dayDatePickerView.setup()
        dayDatePickerView.minimumDate = NSDate(timeIntervalSince1970: 0.0/1000.0) as Date!
        dayDatePickerView.maximumDate = NSDate(timeIntervalSince1970: 4432233446145.0/1000.0) as Date!
        dayDatePickerView.delegate = self
        dayDatePickerView.date = NSDate() as Date//NSDate(timeIntervalSince1970: 1432233446145.0/1000.0) as Date!
        inputView1.addSubview(dayDatePickerView)
        self.view.addSubview(inputView1)
        return
    }
    
    @objc func doneBtnAction()  {
        print()
        
        let formator = DateFormatter()
        formator.dateFormat = "dd MMM, yyyy"
        
        let date = dayDatePickerView.date as NSDate
        let title = formator.string(from: date as Date)
        
        if let oneToOne = previousData["IsOneToOneLesson"] as? Bool{
            
            if oneToOne{
            if validatingQuestCoach(dateSelected: date as Date) {
                    
                    return
                }
            }
        }
       
        
        self.refreshTableView(TodayDate: title)
        self.dateLbl.text =  DataManager.sharedInstance.getFormatedDate(date: title,formate: "dd MMM, yyyy")
        inputView1.removeFromSuperview()
    }
    
    @objc func cancelBtnAction()  {
        inputView1.removeFromSuperview()
    }
    
    func hidingHeaderViewButton()  {
        
        buttonHeightConstraint.constant = 0
        self.headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.headerView.frame.size.height - 40)

            if classController {
                availbleLbl.text = "Classes that are available will show below"//"CLASSES AVAILABLE"
                titleLbl.text = "STEP 3 - SELECT MONTH"
            }else{
                availbleLbl.text = "Lessons that are available will show below"
                titleLbl.text = "STEP 4 - SELECT DATE AND TIME"
            }
        
        
//        no lessons are available on this date
    }
   
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(DateTimeViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    @objc func rightBarBtnAction() {
        
    present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
 
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
  
    }
    
        override func viewWillDisappear(_ animated: Bool) {
 
            super.viewWillDisappear(true)
       // self.map?.removeFromSuperview()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//       self.tableView.reloadData()
    }
    
    //MARK:- fetching server json
    
    func refreshTableView(TodayDate:String)  {
            var durationID = ""
            var inMinutes = ""
            
            if isRedeemedVoucher {
                //mergingcode
                if isNewUrl{
                    
                    if let id = selectedData["DurationId"] as? Int{
                        durationID = "\(id)"
                    }
                    
                    if let id = selectedData["Id"] as? Int{
                        inMinutes = "\(id)"
                    }
                    
                }else{
                    
                    if let durations = redeemedData["Details"] as? String{
                        let components = durations.components(separatedBy: ")")

                        if components.count > 2 {
                            if let id = selectedData["DurationId"] as? Int{
                                durationID = "\(id)"
                            }
                            if let id = selectedData["Id"] as? Int{
                                inMinutes = "\(id)"
                            }
                            
                        }else{
                            if let minutes = redeemedData["DurationId"] as? Int{
                                inMinutes = "\(minutes)"
                            }
                            if let id = redeemedData["LessonDurationId"] as? Int{
                                durationID = "\(id)"
                            }
                        }
                    }
                    
                }
            }else {
                
                if let id = selectedData["DurationId"] as? Int{
                    durationID = "\(id)"
                }
                
                if let id = selectedData["Id"] as? Int{
                    inMinutes = "\(id)"
                }
            }
            
            var currentCoachId = ""
            if currentUserLogin == 4 {
                if let id = selectedCoachData["CoachID"] as? Int{
                    currentCoachId = "\(id)"
                }
            }else{
                
                if let userId = DataManager.sharedInstance.currentUser()?["coachId"] as? Int{
                    currentCoachId = "\(userId)"
                }
            }
            
            let defaults = UserDefaults.standard
            var isStudioYes = "0"
            var isPuttingYes2 = "0"

            if let id =   DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
                
                //1119 for FDGS
                //uncomment replace 1119 for acadmy id
                //bang
    //            if id == 1 || id == 1228{
    //            }else{
                    
                    if let isStudio2 = defaults.object(forKey: "isStudio") as? String{
                        if isStudio2 == "YES"{
                            isStudioYes = "1"
                        }else{
                            isStudioYes = "0"
                            
                        }
                    }
                
                if let isPuttingYes3 = defaults.object(forKey: "isPuttingLab") as? String{
                    if isPuttingYes3 == "YES"{
                        isPuttingYes2 = "1"
                    }else{
                        isPuttingYes2 = "0"
                        
                    }
                }
                

                
                
    //            }
            }
            
            var membershipID = ""
            if let userId = DataManager.sharedInstance.currentUser()?["MembershipId"] as? Int{
                membershipID = "\(userId)"
            }
            
            
            //mergingcode
            var parameters = [String:AnyObject]()
            //localechanges
            if isNewUrl{
                parameters = ["CoachId":currentCoachId as AnyObject,"date":TodayDate as AnyObject,"Duration":durationID as AnyObject,"AcademyId":"\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)" as AnyObject,"NoOfMinutes":inMinutes as AnyObject,"isStudioYes":isStudioYes as AnyObject,"membershipID":membershipID as AnyObject,"Is12HoursTimeFormat": is12Houre as AnyObject,"isPuttingYes": isPuttingYes2 as AnyObject]
            }else{
                parameters = ["CoachId":currentCoachId as AnyObject,"date":TodayDate as AnyObject,"Duration":durationID as AnyObject,"AcademyId":"\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)" as AnyObject,"NoOfMinutes":inMinutes as AnyObject,"isStudioYes":isStudioYes as AnyObject,"Is12HoursTimeFormat": is12Houre as AnyObject,"isPuttingYes": isPuttingYes2 as AnyObject]
            }
            
            print(parameters)
            NetworkManager.performRequest(type:.post, method:"academy/GetCoachScheduleNative",parameter:parameters, view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
                
                print(object!)
                switch object {
                case _ as NSNull:
                    print("figured")
                    DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                    return
                // do one thing
                case _ as [[String:AnyObject]]: break
                default:
                    DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                    return
                }
                
                var array = object as! [[String : AnyObject]]
                var array1 =  [[String : AnyObject]]()
                
                for data  in array {
                    
                    if isStudioYes == "1"{
                        
                        if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == true{
                        array1.append(data)
                        }
                    }else if isPuttingYes2 == "1"{
                                       
                            if let flag5 = data["IsPuttingLabAvailable"] as? Bool ,flag5 == true{
                                array1.append(data)
                            }
                        }else{
                        
                        array1.append(data)
                    }
                }
              
                
                
                array = array1
                self.dataGeneral = array
                
                if self.dataGeneral.count < 1{
                    self.availbleLbl.text = "No lessons are available on this date"//"CLASSES AVAILABLE"
                }else{
                   self.availbleLbl.text = "Lessons that are available will show below"
                }
                self.tableView.reloadData()
            }) { (error) in
                print(error!)
                self.showInternetError(error: error!)
            }
        }

//    func refreshTableView(TodayDate:String)  {
//
//        
//        var durationID = ""
//        var inMinutes = ""
//        if isRedeemedVoucher {
//        if let durations = redeemedData["Details"] as? String{
//            let components = durations.components(separatedBy: ")")
//            //becasue we select time from controller if (15 mint) (20 mint)
//            if components.count > 2 {
//                if let id = selectedData["DurationId"] as? Int{
//                    durationID = "\(id)"
//                }
//                if let id = selectedData["Id"] as? Int{
//                    inMinutes = "\(id)"
//                }
//
//            }else{
//                if let minutes = redeemedData["DurationId"] as? Int{
//                    inMinutes = "\(minutes)"
//                }
//                if let id = redeemedData["LessonDurationId"] as? Int{
//                    durationID = "\(id)"
//                }
//                
//            }
//            }
//        }else{
//      
//        if let id = selectedData["DurationId"] as? Int{
//            durationID = "\(id)"
//        }
//        if let id = selectedData["Id"] as? Int{
//            inMinutes = "\(id)"
//            }
//        }
//        
//        var currentCoachId = ""
//        if currentUserLogin == 4 {
//            
//            if let id = selectedCoachData["CoachID"] as? Int{
//                currentCoachId = "\(id)"
//            }
//        }else{
//        
//            if let userId = DataManager.sharedInstance.currentUser()?["coachId"] as? Int{
//            currentCoachId = "\(userId)"
//            }
//        }
//        let defaults = UserDefaults.standard
//        var isStudioYes = "0"
//        if let id =   DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
//            
//            //1119 for FDGS
//            //uncomment replace 1119 for acadmy id
//            if id == 1 {
//            }else{
//                
//        if let isStudio2 = defaults.object(forKey: "isStudio") as? String{
//            if isStudio2 == "YES"{
//                isStudioYes = "1"
//            }else{
//                isStudioYes = "0"
//
//            }
//            }
//            }
//        }
//        
//        print(["CoachId":currentCoachId as AnyObject,"date":TodayDate as AnyObject,"Duration":durationID as AnyObject,"AcademyId":"\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)" as AnyObject,"NoOfMinutes":inMinutes as AnyObject,"isStudioYes":isStudioYes as AnyObject])
//
//        
//        //GetCoachSchedule
//        NetworkManager.performRequest(type:.post, method:"academy/GetCoachScheduleNative",parameter:["CoachId":currentCoachId as AnyObject,"date":TodayDate as AnyObject,"Duration":durationID as AnyObject,"AcademyId":"\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)" as AnyObject,"NoOfMinutes":inMinutes as AnyObject,"isStudioYes":isStudioYes as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
//            
//            print(object!)
//            switch object {
//            case _ as NSNull:
//                print("figured")
//                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
//                return
//                
//            // do one thing
//            case _ as [[String:AnyObject]]: break
//                
//            default:
//                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
//                return
//            }
//            
//       
//            var array = object as! [[String : AnyObject]]
//            
//            var array1 =  [[String : AnyObject]]()
//            for data  in array {
//
//                if let flag5 = data["IsStudioAvailable"] as? Bool ,flag5 == true{
//                    array1.append(data)
//                }
//            }
//
//            array = array1
//            self.dataGeneral = array
//            self.tableView.reloadData()
//
////            self.dataGeneral = object as! [[String : AnyObject]]
////            self.tableView.reloadData()
//            
//        }) { (error) in
//            print(error!)
//            self.showInternetError(error: error!)
//        }
//    }
    
    
    func refreshTableViewForClass(TodayDate:String)  {
        
        
        var typID = ""
        if let id = previousData["ProgramTypeId"] as? Int{
            typID = "\(id)"
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        let date1 = formatter.date(from: TodayDate)
        let startDate = formatter.string(from: (date1?.startOfMonth())!)
        let endDate = formatter.string(from: (date1?.endOfMonth())!)
        var optionid = ""
        if let id = selectedData["FilterOptionId"] as? Int{
            optionid = "\(id)"
        }
        var academyID = ""
        if let academy = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(academy)"
        }
        print(["FilterOptionId":optionid as AnyObject,"EndDate":endDate as AnyObject,"ProgramTypeId":typID as AnyObject,"AcademyId":academyID as AnyObject,"StartDate":startDate as AnyObject])
        
        var membershipID = ""
        if let userId = DataManager.sharedInstance.currentUser()?["MembershipId"] as? Int{
            membershipID = "\(userId)"
        }
        
        //mergingcode
        var parameters = [String:AnyObject]()
        if isNewUrl{
            parameters = ["FilterOptionId":optionid as AnyObject,"EndDate":endDate as AnyObject,"ProgramTypeId":typID as AnyObject,"AcademyId":academyID as AnyObject,"StartDate":startDate as AnyObject,"membershipID":membershipID as AnyObject,"Is12HoursTimeFormat": is12Houre as AnyObject]
        }else{
            parameters = ["FilterOptionId":optionid as AnyObject,"EndDate":endDate as AnyObject,"ProgramTypeId":typID as AnyObject,"AcademyId":academyID as AnyObject,"StartDate":startDate as AnyObject,"Is12HoursTimeFormat": is12Houre as AnyObject]
        }
    
        print(parameters)
        NetworkManager.performRequest(type:.post, method:"academy/GetClassByFilter/",parameter:parameters, view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            self.dataGeneral = object as! [[String : AnyObject]]
            
            if self.dataGeneral.count < 1{
                self.availbleLbl.text = "No Classes are available on this date"//"CLASSES AVAILABLE"
            }else{
               self.availbleLbl.text = "Classes that are available will show below"
            }

            self.tableView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
//    func refreshTableViewForClass(TodayDate:String)  {
//
//
//        var typID = ""
//        if let id = previousData["ProgramTypeId"] as? Int{
//            typID = "\(id)"
//        }
//
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd MMM, yyyy"
//         let date1 = formatter.date(from: TodayDate)
//        let startDate = formatter.string(from: (date1?.startOfMonth())!)
//        let endDate = formatter.string(from: (date1?.endOfMonth())!)
//        var optionid = ""
//        if let id = selectedData["FilterOptionId"] as? Int{
//            optionid = "\(id)"
//        }
//         var academyID = ""
//        if let academy = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
//            academyID = "\(academy)"
//        }
//
//        print(["FilterOptionId":optionid as AnyObject,"EndDate":endDate as AnyObject,"ProgramTypeId":typID as AnyObject,"AcademyId":academyID as AnyObject,"StartDate":startDate as AnyObject])
//
//        NetworkManager.performRequest(type:.post, method:"academy/GetClassByFilter/",parameter:["FilterOptionId":optionid as AnyObject,"EndDate":endDate as AnyObject,"ProgramTypeId":typID as AnyObject,"AcademyId":academyID as AnyObject,"StartDate":startDate as AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
//
//            print(object!)
//            switch object {
//            case _ as NSNull:
//                print("figured")
//            DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
//                return
//            // do one thing
//            case _ as [[String:AnyObject]]: break
//
//            default:
//                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
//                return
//            }
//
//            self.dataGeneral = object as! [[String : AnyObject]]
//            self.tableView.reloadData()
//
//        }) { (error) in
//            print(error!)
//            self.showInternetError(error: error!)
//        }
//    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataGeneral.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //
        if classController {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DateTimeClassTableViewCell", for: indexPath) as! DateTimeClassTableViewCell
            if let className = dataGeneral[indexPath.row]["ClassName"] as? String{
                cell.classTitleLbl.text = className
            }
            
            if let startDate = dataGeneral[indexPath.row]["ClassStartDate"] as? String{
               
                let dateOrignal = startDate.components(separatedBy: "T")
                if dateOrignal.count > 0 {
                    cell.dateLbl.text = "Start Date: "  + DataManager.sharedInstance.getFormatedDate(date: dateOrignal[0],formate: "yyyy-MM-dd")
                }
            }
            
            if let duration = dataGeneral[indexPath.row]["ClassDuration"] as? String{
                cell.daysLbl.text = duration
            }
            if let duration1 = dataGeneral[indexPath.row]["startTime"] as? String{
                if let duration = dataGeneral[indexPath.row]["endTime"] as? String{

                    if duration1 == "" || duration == "" {

                    }else{
                     cell.numberAvailblBtn.setTitle("\(DataManager.sharedInstance.differenceBetweenTo(startTime: duration1, endTime: duration))", for: .normal)
                    
                        //                        localechanges
                        if is12Houre == "true"{
                            cell.timeLbl.text =  Utility.convertTimeInto12Formate(timeStr: duration1) + "~" +  Utility.convertTimeInto12Formate(timeStr: duration)
                        }else{
                            cell.timeLbl.text = String().removeLastGivenCharacters(givenString:duration1,charatersCount:3) + "~" + String().removeLastGivenCharacters(givenString:duration,charatersCount:3)
                            
                        }
                        
//                        cell.timeLbl.text = String().removeLastGivenCharacters(givenString:duration1,charatersCount:3) + "~" + String().removeLastGivenCharacters(givenString:duration,charatersCount:3)
                        
                    }
                }
               
            }

            cell.checkBtn.tag = indexPath.row
            cell.checkBtn.addTarget(self, action:#selector(selectCheckBtnForClass(sender:)) , for:.touchUpInside)
            cell.checkBtn.setImage(UIImage(named:"circle_icon"), for: .normal)
//            cell.checkBtn.isSelected = false
            if currentSelectedIndex == indexPath.row{
                cell.checkBtn.isSelected = true
                
            }else{
                cell.checkBtn.isSelected = false
            }

            return cell

            
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "DateTimeTableViewCell", for: indexPath) as! DateTimeTableViewCell
      if let startDate = dataGeneral[indexPath.row]["BookingStartTime"] as? String{
        if let enddate = dataGeneral[indexPath.row]["BookingEndTime"] as? String{
            
            if let bookDate = dataGeneral[indexPath.row]["RBookingDate"] as? String{
             
                cell.classTitleLbl.text = startDate + " - " + enddate + " " + DataManager.sharedInstance.getFormatedDate(date: bookDate,formate: globalDateFormate)
            }
            if let id = selectedData["Id"] as? Int{
                cell.numberAvailblBtn.setTitle("\(id)", for: .normal)
            }
            if isRedeemedVoucher {
                let time = DataManager.sharedInstance.differenceBetweenToTimeStrings(startTime: startDate, endTime: enddate)
                cell.numberAvailblBtn.setTitle("\(time)", for: .normal)
            }
        }
        }
        
         cell.checkBtn.tag = indexPath.row
         cell.checkBtn.addTarget(self, action:#selector(selectCheckBtn(sender:)) , for:.touchUpInside)
         cell.checkBtn.setImage(UIImage(named:"circle_icon"), for: .normal)
            
            if currentSelectedIndex == indexPath.row{
                cell.checkBtn.isSelected = true
            }else{
                cell.checkBtn.isSelected = false
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if classController {
            return 70.0
        }else{
            return 60.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        if classController{
            recordSelectedForClass(tag: indexPath.row)
        }else{
            recordSelected(tag: indexPath.row)
        }
    }
    
    //MARK:- Buttons Actions
    
    @IBAction func dateBtnAction(_ sender: UIButton) {
       
        if self.classController{
            self.datePickerView.isHidden = false
            self.monthYearView.selectRow(47, inComponent: 1, animated: false)
        }else{
            inputView1.removeFromSuperview()
            addDayPicker()
            /*
        let formator = DateFormatter()
        ActionSheetDatePicker.init(title: "Select Date", datePickerMode: .date, selectedDate: NSDate() as Date!, doneBlock: {(picker, selectedDate,selectedValue ) -> Void in
            
          
            formator.dateFormat = "dd MMM, yyyy"
            
            let date = selectedDate as! NSDate
            let title = formator.string(from: date as Date)
                self.refreshTableView(TodayDate: title)
            self.dateLbl.text =  DataManager.sharedInstance.getFormatedDate(date: title,formate: "dd MMM, yyyy")
            
        }, cancel: { (picker) -> Void in
            
        }, origin: sender as UIView).show()*/
            
       
        }

    }
    
    //MARK:- buttons actions
    @IBAction func bookPastBtnAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PastViewController") as! PastViewController
        controller.previousData = self.previousData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func selectCheckBtn(sender:UIButton)  {
        recordSelected(tag: sender.tag)
    }
    

    func recordSelectedForClass(tag:Int)  {
        
        let indexPath =  IndexPath(row: tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! DateTimeClassTableViewCell
        if currentSelectedIndex > -1 {
            let indexPath =  IndexPath(row: currentSelectedIndex, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as? DateTimeClassTableViewCell
            cell?.checkBtn.isSelected = false
        }
        
        if cell.checkBtn.isSelected {
            cell.checkBtn.isSelected = false
        }else{
            currentSelectedIndex = tag
            moveToNext(tag: tag)
            cell.checkBtn.isSelected = true
        }
    }
    
    
    func recordSelected(tag:Int)  {
        
     let indexPath =  IndexPath(row: tag, section: 0)
     let cell = self.tableView.cellForRow(at: indexPath) as! DateTimeTableViewCell
        
        if currentSelectedIndex > -1 {
            print(currentSelectedIndex)
            let indexPath =  IndexPath(row: currentSelectedIndex, section: 0)
            let cell = self.tableView.cellForRow(at: indexPath) as? DateTimeTableViewCell
            cell?.checkBtn.isSelected = false
        }

        if cell.checkBtn.isSelected {
        
             cell.checkBtn.isSelected = false
            
                }else{
        
                    currentSelectedIndex = tag
                    cell.checkBtn.isSelected = true
            
                    if classController{}else{
                        //first checking Studio availablity
                        let defaults = UserDefaults.standard
                        var keyToCheckDecide = ""
                        let keyTo = DataManager.sharedInstance.detectIsPuttingLab(key: "isPuttingLab")
                        if keyTo == "true" {
                            keyToCheckDecide = "isPuttingLab"
                            }else{
                            keyToCheckDecide = "isStudio"
                        }
        
                        //just shuffle logic for Putting Lab
                        if let isStudio2 = defaults.object(forKey:keyToCheckDecide ) as? String{
                            if isStudio2 == "YES"{
                              checkStudeoAvailablity(Tag: tag)
                            }else{
                               
                                if isNewUrl{
                                    
                                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                                    controller.selectedData = self.dataGeneral[tag]
                                    controller.previousData = self.previousData
                                    controller.secondLastControllerData = self.selectedData
                                    
                                    if currentUserLogin == 4 {
                                        controller.selectedCoachData = selectedCoachData
                                    }
                                    
                                    if isRedeemedVoucher {
                                        controller.isRedeemedVoucher = true
                                        controller.redeemedData = redeemedData
                                    }
                                    
                                    self.navigationController?.pushViewController(controller, animated: true)
                                    
                                }else{
                                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
                                    controller.selectedData = self.dataGeneral[tag]
                                    controller.previousData = self.previousData
                                    controller.secondLastControllerData = self.selectedData
                                    
                                    if currentUserLogin == 4 {
                                        controller.selectedCoachData = selectedCoachData
                                    }
                                    
                                    if isRedeemedVoucher {
                                        controller.isRedeemedVoucher = true
                                        controller.redeemedData = redeemedData
                                    }
                                    
                                    self.navigationController?.pushViewController(controller, animated: true)
                                }
                            }
                        }
                    }
                }

        
     /*   let indexPath =  IndexPath(row: tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! DateTimeTableViewCell
        
        if currentSelectedIndex > -1 {
            let indexPath =  IndexPath(row: currentSelectedIndex, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as! DateTimeTableViewCell
            cell.checkBtn.isSelected = false
        }
        
        if cell.checkBtn.isSelected {
            
            cell.checkBtn.isSelected = false
        }else{
            
            currentSelectedIndex = tag
            cell.checkBtn.isSelected = true
            if classController{}else{
                //first checking Studio availablity
                let defaults = UserDefaults.standard
                if let isStudio2 = defaults.object(forKey: "isStudio") as? String{
                    if isStudio2 == "YES"{
                        checkStudeoAvailablity(Tag: tag)
                    }else{
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                        controller.selectedData = self.dataGeneral[tag]
                        controller.previousData = self.previousData
                        controller.secondLastControllerData = self.selectedData
                        
                        if currentUserLogin == 4 {
                            controller.selectedCoachData = selectedCoachData
                        }
                        if isRedeemedVoucher {
                            controller.isRedeemedVoucher = true
                            controller.redeemedData = redeemedData
                        }
                        
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
                
                
            }
        }
*/
    }
    
    
    
    @objc func selectCheckBtnForClass(sender:UIButton)  {
        
        recordSelectedForClass(tag: sender.tag)
    }
    
    func moveToNext(tag:Int)  {
        
        var dic = dataGeneral[tag]
        var currentClassId  = 0
        var cart = [[String:AnyObject]]()
        
        if let idOFClass = dic["ClassId"] as? Int{
            currentClassId = idOFClass
        }
       
        var itemAdded = false
        var cartKey = ""
      //appending user id to make unique every cart

        var userId = ""
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = "\(id)"
        }
        
        if currentUserLogin == 4 {
           cartKey = "studentCart" +  userId
        }else{
               cartKey = "myCart" + userId
        }
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: cartKey){
            cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: cartKey) as! Data)) as? [[String:AnyObject]])!
            for item in cart {
                if let idOFClass = item["ClassId"] as? Int{
                    if idOFClass == currentClassId {
                        itemAdded = true
                    }
                }
            }
        }
        if itemAdded {
        }else {
            
            dic["previousData"] = self.previousData as AnyObject?
            dic["secondLastControllerData"] = self.selectedData as AnyObject?
            cart.append(dic)
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: cartKey)
        }

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        //        controller.selectedData = self.dataGeneral[Tag]
       controller.previousData = self.previousData
        controller.secondLastControllerData = self.selectedData
        if isRedeemedVoucher {
            controller.isRedeemedVoucher = true
            controller.redeemedData = redeemedData
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func checkStudeoAvailablity(Tag:Int)  {
        
        
     let dic =   dataGeneral[Tag] as NSDictionary
        var startTime = ""
        if let id = dic["BookingStartTime"] as? String{
            startTime = "\(id)"
        }
        
        var endTime = ""
        if let id = dic["BookingEndTime"] as? String{
            endTime = "\(id)"
        }
      
        var date = ""
        if let id = dic["RBookingDate"] as? String{
           date = DataManager.sharedInstance.getFormatedDateForJson(date: id, formate:globalDateFormate)
            //date = "\(id)"
        }
        var academyID = ""
        if let acemyId = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyID = "\(acemyId)"
        }
     print(startTime,endTime,date,academyID)
        
        var roldId = "-1"
        if currentUserLogin == 4 {
            roldId = "4"
        }
        
        var typID = ""
        if let id = previousData["ProgramTypeId"] as? Int{
            typID = "\(id)"
        }
        
        //sake of Putting Lab
        var methodToHit = "CheckStudioAvailability"
        var parameters = [String:AnyObject]()
        let keyTo = DataManager.sharedInstance.detectIsPuttingLab(key: "isPuttingLab")
        if keyTo == "true" {
            methodToHit = "CheckPuttingLabAvailability"
            parameters = ["LessonDate":date as AnyObject,"startTime":startTime as AnyObject,"AcademyId":"\(academyID)" as AnyObject,"EndTime":endTime as AnyObject]  as [String : AnyObject]
        }else{
            //bang
            parameters = ["LessonDate":date as AnyObject,"startTime":startTime as AnyObject,"AcademyId":"\(academyID)" as AnyObject,"EndTime":endTime as AnyObject,"RoleId":roldId,"ProgramTypeId":typID] as [String : AnyObject]
        }
        
        print(parameters)
//        var roldId = "0"
//        if currentUserLogin == 4 {
//            roldId = "4"
//        }
//        let parameters = ["LessonDate":date as AnyObject,"startTime":startTime as AnyObject,"AcademyId":"\(academyID)" as AnyObject,"EndTime":endTime as AnyObject,"RoleId":roldId] as [String : Any]
        
        NetworkManager.performRequest(type:.post, method:"Booking/\(methodToHit)",parameter:parameters as [String : AnyObject], view: (UIApplication.getTopestViewController()!.view!), onSuccess:{(object) in
            
            print(object!)
            let object2 = object as! Bool
            if object2 {
                
                
                if isNewUrl{
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
                    controller.selectedData = self.dataGeneral[Tag]
                    controller.previousData = self.previousData
                    controller.secondLastControllerData = self.selectedData
                    if currentUserLogin == 4 {
                        controller.selectedCoachData = self.selectedCoachData
                        if self.isRedeemedVoucher {
                            controller.isRedeemedVoucher = true
                            controller.redeemedData = self.redeemedData
                        }
                    }
                    self.navigationController?.pushViewController(controller, animated: true)
                }else{
                    
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
                    controller.selectedData = self.dataGeneral[Tag]
                    controller.previousData = self.previousData
                    controller.secondLastControllerData = self.selectedData
                    if currentUserLogin == 4 {
                        controller.selectedCoachData = self.selectedCoachData
                        if self.isRedeemedVoucher {
                            controller.isRedeemedVoucher = true
                            controller.redeemedData = self.redeemedData
                        }
                    }
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
                
//                            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderOldViewController") as! CompleteOrderOldViewController
//                            controller.selectedData = self.dataGeneral[Tag]
//                            controller.previousData = self.previousData
//                           controller.secondLastControllerData = self.selectedData
//                   if currentUserLogin == 4 {
//                    controller.selectedCoachData = self.selectedCoachData
//                    if self.isRedeemedVoucher {
//                        controller.isRedeemedVoucher = true
//                        controller.redeemedData = self.redeemedData
//                    }
//                   }
//                self.navigationController?.pushViewController(controller, animated: true)
            }else{
                 DataManager.sharedInstance.printAlertMessage(message:"Selected Lesson Not Available!", view:self)
            }
          
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    @IBAction func pickerViewDone(_ sender: UIButton) {
        
        
        
        var dateString = ""

        if self.monthYearView.month == 0 {
            
            let date =  DataManager.sharedInstance.getTodayDate()
            let components = date.components(separatedBy: "-")
            self.dateLbl.text =  String(format: "%@ %d", self.monthYearView.months[monthYearView.month - 1 + (Int(components[1]))!], (Int(components[0]))!)
             dateString = date

        }else{
          self.dateLbl.text =  String(format: "%@ %d", self.monthYearView.months[monthYearView.month-1], monthYearView.year)
             dateString = "\(monthYearView.year)-\(monthYearView.month)-2"

        }

        
       
        datePickerView.isHidden = true
     
  
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateSelected = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd MMM, yyyy"
        
        let TodayDate = dateFormatter.string(from: dateSelected!)
        self.refreshTableViewForClass(TodayDate: TodayDate)
      
    }
    
    func validatingQuestCoach(dateSelected:Date)->Bool{
        
        if currentUserLogin == 4 {
            
            var userId = ""
            if let id = selectedCoachData["CoachID"] as? Int{
                userId = "\(id)"
            }
            
            if userId == "4240" {
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let nextDate =  Calendar.current.date(byAdding: .day, value: 21, to: date as Date)! as Date
                
                if nextDate < dateSelected {
                    DataManager.sharedInstance.printAlertMessage(message:"bookings are only available up to three weeks in advance", view:self)
                    return true
                }else{
                    
                   return false
                }
            }
            
        }
        
        return false
    }
    
    @IBAction func pickerViewCancelBtnAction(_ sender: UIButton) {
        datePickerView.isHidden = true
    }

    

}

//MARK:- date utility extentions

extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}

extension DateTimeViewController : DayDatePickerViewDelegate{
    
    func dayDatePickerView(_ dayDatePickerView: DayDatePickerView!, didSelect date: Date!) {
    }
    
}



