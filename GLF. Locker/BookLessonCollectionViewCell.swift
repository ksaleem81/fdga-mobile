//
//  BookLessonCollectionViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/4/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class BookLessonCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var collectionImage: UIImageView!
    @IBOutlet weak var collectionTitle: UILabel!
    @IBOutlet weak var collectionView: UIView!
    
}
