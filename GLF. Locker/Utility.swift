//
//  Utility.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 09/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import AVKit
import ActionSheetPicker_3_0
import Alamofire

class Utility: NSObject {
    
    typealias FailureBlock = (NSError?) -> Void
    typealias TSuccessBlock = (AnyObject?) -> Void
    
    static var userPassword = ""
    static var GDPRstatusDic = [String:AnyObject]()
    
    static func playVideo(path: String, on viewController: UIViewController) {
        let url = URL.init(fileURLWithPath: path)
        let avplayer = AVPlayer.init(url: url)
        let avplayerVC = AVPlayerViewController.init()
        avplayerVC.player = avplayer
        
        viewController.present(avplayerVC, animated: true) { 
            avplayerVC.player?.play()
        }
    }
    
    static func isDevice12Hour()->Bool{
        
    let locale = NSLocale.current
       let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
       if formatter.contains("a") {
           return true
       } else {
           //phone is set to 24 hours
        return false
       }
        
    }
    
    @objc static func showAlertView(title: String?, message: String?) {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(action)
        let alertWindow = UIWindow.init()
        alertWindow.rootViewController = UIViewController.init()
        let topWindow = UIApplication.shared.windows.last
        alertWindow.windowLevel = (topWindow?.windowLevel)! + 1
        alertWindow.makeKeyAndVisible()
        alertWindow.backgroundColor = UIColor.clear
//        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
       let del = UIApplication.shared.delegate as! AppDelegate
        DispatchQueue.main.async (execute: {
            del.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        )
       

    }
    
    static func createCallBackID() -> String {
        return "VideoHandlerPlugin\(NSDate().timeIntervalSince1970)"
    }

    /************************************************************************************************************/
    // MARK: - Analyze and Trim Methods
    
    static func showShapeViewController(with videoInfo: [AnyHashable : Any], presenter: UIViewController, animated: Bool, delegate: ShapeViewControllerDelegate?) {
        
        let shapeViewController = ShapeViewController.init(videoInfo: videoInfo)
        shapeViewController?.delegate = delegate
        if (shapeViewController?.responds(to: #selector(ShapeViewController.reloadView)))! {
            shapeViewController?.reloadView()
        }
        
        let navigationController = UINavigationController.init(rootViewController: shapeViewController!)
        navigationController.modalPresentationStyle = .fullScreen
        presenter.present(navigationController, animated: animated, completion: nil)
        
    }
    
    static func showVideoPlayerController(with videoInfo: [AnyHashable : Any], presenter: UIViewController, animated: Bool, isOnline: Bool) {
        
        let vPlayObj = VideoPlayerViewController.init(nibName: "VideoPlayerViewController", bundle: nil)
        vPlayObj.videoInfo = videoInfo;
        vPlayObj.isOnlineVideo = isOnline;
        let navigationController = UINavigationController.init(rootViewController: vPlayObj)
        navigationController.restorationIdentifier = "world"
        navigationController.modalPresentationStyle = .fullScreen
        presenter.present(navigationController, animated: animated, completion: nil)
        
    }
    
    //MARK:- animation method
    static func viewToShake(viewToShake:UIView)  {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: viewToShake.center.x - 10, y: viewToShake.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: viewToShake.center.x + 10, y: viewToShake.center.y))
        viewToShake.layer.add(animation, forKey: "position")
        
    }
    
    //MARK:- device detection method

     static func isiPhonX() -> Bool {
        
        if UIDevice().userInterfaceIdiom == .phone {
            let height =  UIScreen.main.nativeBounds.height
            if height == 2436{
                return true
            }
        }
        return false
    }
    
    static func isiPhonXR() -> Bool {
        
        if UIDevice().userInterfaceIdiom == .phone {
            let height =  UIScreen.main.nativeBounds.height
            if height == 1624 ||  height == 1792{
                return true
            }
        }
        return false
    }
    
    static func isiPhonXMAX() -> Bool {
        
        if UIDevice().userInterfaceIdiom == .phone {
            let height =  UIScreen.main.nativeBounds.height
            if height == 2688{
                return true
            }
        }
        return false
    }
    
    //MARK:- picker time retriction

     static func settingTimeLimitToPicker(datePicker:ActionSheetDatePicker) -> ActionSheetDatePicker {
        
        let startHour: Int = 7
        let endHour: Int = 22
        let date1: NSDate = NSDate()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: date1 as Date) as NSDateComponents
        components.hour = startHour
        components.minute = 0
        components.second = 0
        let startDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
        components.hour = endHour
        components.minute = 0
        components.second = 0
        let endDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
        datePicker.minimumDate = startDate as Date?
        datePicker.maximumDate = endDate as Date?
        
        return datePicker
    }
    
    //dynamicChange

    static func dynamicSettingTimeLimitToPicker(datePicker:ActionSheetDatePicker,data:[String:AnyObject]) -> ActionSheetDatePicker {
        
        var startTime = "07:00"
        var endTime = "10:00"

        if let startTime1 =  data["AcademyStartTime"] as? String,startTime1 != ""{
            startTime = startTime1
        } else {
            
        }
        
        if let endTime1 =  data["AcademyEndTime"] as? String,endTime1 != "" {
            endTime = endTime1
        }else {
            
        }
        
        var startHourTime = 0
        var startMinuteTime = 0
        var endHourTime = 0
        var endMinuteTime = 0
        let dateOrignal = startTime.components(separatedBy: ":")
        if dateOrignal.count > 0 {
            startHourTime  =  Int(dateOrignal[0])!
            startMinuteTime  =  Int(dateOrignal[1])!
        }
        
        let dateOrignal2 = endTime.components(separatedBy: ":")
        if dateOrignal2.count > 0 {
            endHourTime  =  Int(dateOrignal2[0])!
            endMinuteTime  =  Int(dateOrignal2[1])!
        }
        
        let startHour: Int = startHourTime
        let endHour: Int = endHourTime
        let date1: NSDate = NSDate()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: date1 as Date) as NSDateComponents
        components.hour = startHour
        components.minute = startMinuteTime
        components.second = 0
        let startDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
        components.hour = endHour
        components.minute = endMinuteTime
        components.second = 0
        let endDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
        datePicker.minimumDate = startDate as Date?
        datePicker.maximumDate = endDate as Date?
        
        return datePicker
    }

    
    
    //MARK:- generic method for password field Strength

   static func passwordStrength(passwordString:String,messageString1:String)->String  {
    
        print(passwordString)
        var messageString = messageString1
        
    if (passwordString.count) < 8 {
        messageString = messageString + "Password Should be 8 characters with at least 1 letter and 1 number.\n"
    }else{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: passwordString, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, passwordString.count)) != nil {
        }else{
//            messageString = messageString + "Password should contain atleast 1 symbol.\n"
        }
        let numbersRange2 = passwordString.rangeOfCharacter(from: .decimalDigits)
        let hasNumbers2 = (numbersRange2 != nil)
        if hasNumbers2{
        }else{
            messageString = messageString + "Password should contain atleast 1 number.\n"
        }
        let numbersRange3 = passwordString.rangeOfCharacter(from: .letters)
        let hasNumbers3 = (numbersRange3 != nil)
        if hasNumbers3{
        }else{
            messageString = messageString + "Password should contain atleast 1 letter.\n"
        }
    }
    return messageString
    }

    static func getAthenticatedHeader(method:String) -> HTTPHeaders {
        
        var headers   = HTTPHeaders()
        let methodsArray = ["GetAllAcademiesOrderByLatLong","GetAllOwnerAcademiesOrderByLatLong","login","ResetPassword","SearchAcademyByID_native","BookingLesson","BookingCartClass","RegisterStudent_native"]
        
        if methodsArray.contains(method.components(separatedBy: "/")[1]){
            headers = [
                "Content-Type": "application/x-www-form-urlencoded"]
        }else{
            
            var token = ""
            if let id = DataManager.sharedInstance.currentUser()!.value(forKey: "TokenKey") as? String {
                token = "\(id)"
                print(token)
            }
            
            headers = [
                "Content-Type": "application/x-www-form-urlencoded",
                "UserName":"\(String(describing: DataManager.sharedInstance.currentUser()!.value(forKey: "UserName")!))",
                "AcademyId":"\(String(describing: DataManager.sharedInstance.currentUser()!.value(forKey: "AcademyId")!))",
                "AuthenticationToken":"\(token)",
                "UserID":"\(String(describing: DataManager.sharedInstance.currentUser()!.value(forKey: "Userid")!))"
            ]
        }
        
        return headers
    }
    
    //MARK:- authorize action
    static func authorizeAlert(){
        let dele = UIApplication.shared.delegate as! AppDelegate
        dele.athorizeError()
        
    }
    
    static func convertTimeInto12Formate(timeStr:String) -> String {
        let dateAsString = timeStr
        let dateFormatter = DateFormatter()
        if "\(timeStr)".count > 5{
            
            let last2 = timeStr.suffix(2)
            
            if last2 == "AM" || last2 == "PM"{
                dateFormatter.dateFormat = "h:mm a"
            }else{
                dateFormatter.dateFormat = "HH:mm:ss"
            }
        }else{
            dateFormatter.dateFormat = "HH:mm"
        }
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        return Date12
    }

    
    
   static func savingToAllAccounts() {
        
        var dic = [String:AnyObject]();
        var userId = 0
        var academyId = 0
        if let id = DataManager.sharedInstance.currentUser()!["Userid"] as? Int{
            userId = id
        }
        var cart = [[String:AnyObject]]()
        if let acadmy =   DataManager.sharedInstance.currentAcademy()?["AcademyID"] as? Int{
            academyId = acadmy
        }
        var itemAdded = false
        var index = 0
        if DataManager.sharedInstance.userAlreadyExist(kUsernameKey: "allAccounts"){
            
            cart = (NSKeyedUnarchiver.unarchiveObject(with:(UserDefaults.standard.object(forKey: "allAccounts") as! Data)) as? [[String:AnyObject]])!
            
            for item in cart {
                if let userDic = item["loginUser"] as? [String:AnyObject]{
                    if let id = userDic["Userid"] as? Int{
                        if let academyDic = item["academy"] as? [String:AnyObject]{
                            if let academyID = academyDic["AcademyID"] as? Int{
                                if userId == id && academyID == academyId {
                                    itemAdded = true
                                    break
                                    
                                }
                            }
                        }
                    }
                }
                index =  index + 1
            }
        }
        
        if itemAdded {

            cart[index].updateValue((DataManager.sharedInstance.currentUser() as AnyObject?)!, forKey: "loginUser")
            cart[index].updateValue((DataManager.sharedInstance.currentAcademy() as AnyObject?)!, forKey: "academy")
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: "allAccounts")
            
        }else{
            
            dic["loginUser"] = DataManager.sharedInstance.currentUser() as AnyObject?
            dic["academy"] =  DataManager.sharedInstance.currentAcademy() as AnyObject?
            cart.append(dic)
            let archiveddata = NSKeyedArchiver.archivedData(withRootObject: cart )
            UserDefaults.standard.set(archiveddata, forKey: "allAccounts")
        }
    }
    
    //dynamicChange
    static func getScheduleTimeOfAcademy(selectedDay:String,controller:UIViewController, Success:@escaping TSuccessBlock , onFailure: @escaping FailureBlock )  {
        
        var acadmyId = ""
        if let id = DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            acadmyId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "academy/GetAcademyStartEndTime/\(acadmyId)/\(selectedDay)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:controller)
                return
            // do one thing
            case _ as [String:AnyObject]: break
            default:
                break
            }
            
            var data = object as! [String:AnyObject]
            
            
            Success(data as AnyObject)
            
        }) { (error) in
            print(error!)
//            self.showInternetError(error: error!)
        }
    }
    
    //urlsplacechanged
    static func decidingAppUrls(){
        
//        return
        
        var titleAcademy = ""
        if let titl =  DataManager.sharedInstance.currentAcademy()!["AcademyTitle"] as? String{
            titleAcademy = titl
        }
        
        if let newurl =  DataManager.sharedInstance.currentAcademy()!["IsNewStore"] as? Bool,newurl == false{
            isNewUrl = true
            let url = baseUrlOld
            baseUrl = url
            
            //live
//            baseUrlForVideo = "https://\(titleAcademy).firstdegree.golf"
//            imageBaseUrl = "https://\(titleAcademy).firstdegree.golf"
//            storeLink = "https://\(titleAcademy).firstdegree.golf/Store/?AcademyId="

            //beta
            baseUrlForVideo = "http://\(titleAcademy).glfbeta.com"
            imageBaseUrl = "http://\(titleAcademy).glfbeta.com"
            storeLink = "http://\(titleAcademy).glfbeta.com/Store"
            
        }else{
            isNewUrl = true
            let url = baseUrl
            
            //live
//            baseUrl = url
//            baseUrlForVideo = "https://\(titleAcademy).firstdegree.golf"
//            imageBaseUrl = "https://\(titleAcademy).firstdegree.golf"
//            storeLink = "https://\(titleAcademy).firstdegree.golf/Store/?AcademyId="
            
            //beta
            baseUrl = url
            baseUrlForVideo = "http://\(titleAcademy).glfbeta.com"
            imageBaseUrl = "http://\(titleAcademy).glfbeta.com"
            storeLink = "http://\(titleAcademy).glfbeta.com/Store"
            
        }

        
    }
    

}

