//
//  TNBackgroundSessionDelegate.m
//  Telenotes2
//
//  Created by Stephen Lynn on 3/12/15.
//  Copyright (c) 2015 TeleNotes. All rights reserved.
//

#import "BLBackgroundSessionDelegate.h"
#import "BLBackgroundSessionConstants.h"
//#import "AppDelegate.h"
#import "UploadManager.h"
#import "GlobalClass.h"


@interface BLBackgroundSessionDelegate ()
{
    BOOL startedNextChunkUploading;
}
@property (nonatomic, strong) NSMutableDictionary *completionHandlers;
@property (nonatomic, strong) NSMutableDictionary *responsesData;

@end


@implementation BLBackgroundSessionDelegate

+ (NSURLSession *)createBackgroundURLSessionWithId:(NSString *)identifier shouldWifiSyncOnly:(BOOL)shouldWifiSyncOnly delegate:(id<NSURLSessionDelegate>)delegate {
    
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"createBackgroundURLSessionWithId: %@, %@", identifier, [NSDate date]]];
    
//  if(![NSURLSessionConfiguration respondsToSelector:@selector(backgroundSessionConfigurationWithIdentifier:)]) {
//    return nil;
//  }

    NSURLSessionConfiguration *config = nil;
    
    if(SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        config=[NSURLSessionConfiguration backgroundSessionConfiguration:identifier];
    }
    else {
        config=[NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
    }
    config.allowsCellularAccess = !shouldWifiSyncOnly;
    config.timeoutIntervalForRequest=0;
    config.timeoutIntervalForResource=0;
//    config.discretionary = YES;
    config.HTTPMaximumConnectionsPerHost=1;
    
//    config.networkServiceType = NSURLNetworkServiceTypeBackground;
    
  NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:delegate delegateQueue:nil];

  return session;
}

- (instancetype)init {
  self = [super init];

  if(self) {
      self.completionHandlers = [NSMutableDictionary dictionary];
      self.responsesData = [NSMutableDictionary dictionary];
      
      startedNextChunkUploading=NO;
  }

  return self;
}

- (void)registerCompletionHandler:(CompletionHandlerType)completionHandler forIdentifier:(NSString *)identifier {
    
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"registerCompletionHandler:identifier: %@, %@", identifier, [NSDate date]]];

    
  if(_completionHandlers[identifier] != nil) {
    NSLog(@"Error: Trying to register multiple completion handlers for identifier '%@'", identifier);
  }

  _completionHandlers[identifier] = completionHandler;
}


#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"session: %@, didBecomeInvalidWithError: %@, %@", session.configuration.identifier, error , [NSDate date]]];

    if(session)
    {
        if(session.configuration.identifier) {
            NSLog(@"background session '%@', didBecomeInvalidWithError: '%@'", session.configuration.identifier, error);
        }
        
        
//        AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
//        [appDelegate removeSession:session];
        [[GlobalClass sharedInstance] removeSession:session];
    }
}


#pragma mark - NSURLSessionTaskDelegate

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session {
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"URLSessionDidFinishEventsForBackgroundURLSession:session: %@, %@", session.configuration.identifier, [NSDate date]]];
    
    if(session.configuration.identifier) {
        NSLog(@"Finished processing events for background session '%@'", session.configuration.identifier);
        
        CompletionHandlerType completionHandler = _completionHandlers[session.configuration.identifier];
        
        if(completionHandler) {
            [_completionHandlers removeObjectForKey:session.configuration.identifier];
           
                [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"URLSessionDidFinishEventsForBackgroundURLSession:session: Calling Completion handler %@, %@", session.configuration.identifier, [NSDate date]]];
                completionHandler();
        }
    }
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"session:didSendBodyData: %@, Task #%@: Sent %@bytes // Total Sent %@bytes of %@bytes, %@", session.configuration.identifier, @(task.taskIdentifier), @(bytesSent), @(totalBytesSent), @(totalBytesExpectedToSend), [NSDate date]]];

    NSLog(@"Task #%@: Sent %@bytes // Total Sent %@bytes of %@bytes", @(task.taskIdentifier), @(bytesSent), @(totalBytesSent), @(totalBytesExpectedToSend));

    NSURLRequest *request=task.originalRequest;
    NSDictionary *dictionary=request.allHTTPHeaderFields;
    NSString *videoID=[dictionary objectForKey:kMediaIDKey];
    if(!videoID)
        return;
    

    [[UploadManager sharedManager] requestWithMediaInfo:dictionary didSendData:bytesSent totalBytesWritten:totalBytesSent totalBytesExpectedToWrite:totalBytesExpectedToSend];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)task didReceiveData:(NSData *)data
{
  NSMutableData *responseData = _responsesData[@(task.taskIdentifier)];
  if (!responseData) {
    responseData = [NSMutableData dataWithData:data];
    _responsesData[@(task.taskIdentifier)] = responseData;
  } else {
    [responseData appendData:data];
  }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    [GlobalClass writeLocationToLogFile:[NSString stringWithFormat:@"URLSession: %@ task: %@, didCompleteWithError: %@, %@", session.configuration.identifier, task, error, [NSDate date]]];
    
    NSLog(@"Background session task completed: %@\nError: %@", task, error);

    NSHTTPURLResponse *response = (NSHTTPURLResponse *) task.response;
    NSURLRequest *request=task.originalRequest;
    NSDictionary *dictionary=request.allHTTPHeaderFields;
    NSString *uniqueMediaID=[dictionary objectForKey:kMediaIDKey];
    if(!uniqueMediaID)
        return;

//    NSString *methodName=[dictionary objectForKey:kMethodNameKey];
//    if([methodName isEqualToString:kMethodNameUploadChunk] && !startedNextChunkUploading)
//    {
//        [[UploadManager sharedManager] startUploadingPartsofVideoWithID:videoID];
//    }
    
    startedNextChunkUploading=NO;
    NSData *responseData = _responsesData[@(task.taskIdentifier)];
    if(error)
    {
        [[UploadManager sharedManager] requestWithMediaInfo:dictionary didFailWithError:error];
    }
    else
    {
        [[UploadManager sharedManager] requestWithMediaInfo:dictionary didCompleteWithResponse:response responseData:responseData];
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    
    NSLog(@"didReceiveChallenge: %@, response headers: %@", task.originalRequest.allHTTPHeaderFields, task.response.MIMEType);
    
  //Auto cancel failed authentication
  completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
}


#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location
{

    NSURLRequest *request=downloadTask.originalRequest;
    NSDictionary *dictionary=request.allHTTPHeaderFields;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    NSString *methodName=[dictionary objectForKey:kMethodNameKey];
/*
    NSString *videoID=[dictionary objectForKey:kVideoIDKey];
    if([methodName isEqualToString:kMethodNameUploadChunk])
    {
        NSString *chunkNumberString=[dictionary objectForKey:kChunkNumberKey];
//        [[UploadManager sharedManager] setChunkStatus:ChunkUploadStatusWaiting withChunkNumber:chunkNumberString.intValue ofVideoWithID:videoID];
//        [[UploadManager sharedManager] startUploadingPartsofVideoWithID:videoID];
        startedNextChunkUploading=YES;
        
        
        [session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
            [AppDelegate writeLocationToLogFile:[NSString stringWithFormat:@"download tasks: %@, %@", downloadTasks, [NSDate date]]];
            
        }];
        
        NSLog(@"URL: %@", location);
        NSLog(@"seesionDownloadTask chunk number: %@, %@", chunkNumberString, [NSDate date]);
        [AppDelegate writeLocationToLogFile:[NSString stringWithFormat:@"sessionDownloadTask chunk number: %@, %@", chunkNumberString, [NSDate date]]];
     
        
        UILocalNotification *localNotification=[[UILocalNotification alloc] init];
        localNotification.alertBody=[NSString stringWithFormat:@"chunks Uploaded: %@, finishing tasks", chunkNumberString];
        localNotification.alertAction=@"View";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        
    }
    else
    {
        startedNextChunkUploading=NO;
*/
    if([methodName isEqualToString:kMethodNameCompleteAndPublishMultipart]){
        
        NSString *responseFileName=[dictionary objectForKey:kResponseFileNameKey];
        NSString *responseFilePath=[documentsDirectory stringByAppendingPathComponent:responseFileName];
        NSError *error=nil;
        if([[NSFileManager defaultManager] fileExistsAtPath:responseFilePath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:responseFilePath error:&error];
            NSLog(@"response file delete error: %@", error);
        }
        
        [[NSFileManager defaultManager] copyItemAtURL:location toURL:[NSURL fileURLWithPath:responseFilePath] error:&error];
        NSLog(@"response file copy error: %@", error);
    }
//    }
    
    
/*
    downloadTaskCalled=YES;
    
    NSLog(@"URL: %@", location);
    NSLog(@"start sessionDownloadTask didFinishDownloadingToURL : %lu, %@, %@", (unsigned long)[downloadTask taskIdentifier], [downloadTask taskDescription], [NSDate date]);
    [AppDelegate writeLocationToLogFile:[NSString stringWithFormat:@"start sesionDownloadTask didFinishDownloadingToURL : %lu, %@, %@", (unsigned long)[downloadTask taskIdentifier], [downloadTask taskDescription], [NSDate date]]];
    
    NSURLRequest *request=downloadTask.originalRequest;
    NSDictionary *dictionary=request.allHTTPHeaderFields;
    NSString *responseFileName=[dictionary objectForKey:@"responseFileName"];
    NSString *videoID=[dictionary objectForKey:@"sibmeVideoID"];
    NSString *sibmeFileName=[dictionary objectForKey:@"sibmeFileName"];
    NSString *methodName=[dictionary objectForKey:@"sibmeMethodName"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *responseFilePath=[documentsDirectory stringByAppendingPathComponent:responseFileName];

    NSError *error=nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:responseFilePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:responseFilePath error:&error];
        NSLog(@"response file delete error: %@", error);
    }
    
    [[NSFileManager defaultManager] copyItemAtURL:location toURL:[NSURL fileURLWithPath:responseFilePath] error:&error];
    NSLog(@"response file copy error: %@", error);
    
    [[UploadManager sharedManager] requestWithVideoInfo:@{@"videoID":videoID, @"fileName": sibmeFileName, @"methodName": methodName} didCompleteWithResponse:nil responseData:nil];
    
    NSLog(@"end sessionDownloadTask didFinishDownloadingToURL : %lu, %@, %@", (unsigned long)[downloadTask taskIdentifier], [downloadTask taskDescription], [NSDate date]);
    [AppDelegate writeLocationToLogFile:[NSString stringWithFormat:@"end sesionDownloadTask didFinishDownloadingToURL : %lu, %@, %@", (unsigned long)[downloadTask taskIdentifier], [downloadTask taskDescription], [NSDate date]]];

    
//    if([methodName isEqualToString:kMethodNameSignUploadPart])
//    {
//        
//    }
    
    __block UIBackgroundTaskIdentifier bgTask=[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        bgTask=UIBackgroundTaskInvalid;
    });
*/
    
}

//- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
//      didWriteData:(int64_t)bytesWritten
// totalBytesWritten:(int64_t)totalBytesWritten
//totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
//{
//    
//}

/* Sent when a download has been resumed. If a download failed with an
 * error, the -userInfo dictionary of the error will contain an
 * NSURLSessionDownloadTaskResumeData key, whose value is the resume
 * data.
 */
//- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
// didResumeAtOffset:(int64_t)fileOffset
//expectedTotalBytes:(int64_t)expectedTotalBytes
//{
//    
//}



#pragma mark - Application State Notifications

- (void) applicationWillEnterForeground:(NSNotification*)notification
{
    
}

- (void) applicationDidBecomeActive:(NSNotification*)notification
{
    
}


@end
