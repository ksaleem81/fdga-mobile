//
//  BookLessonVC.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/4/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SDWebImage
import SideMenu
class BookLessonVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var collectionData:[[String:AnyObject]] = [[String:AnyObject]]()
 
    let arrayOfColors = [UIColor.init(red: 31/255, green: 160/255, blue: 133/255, alpha: 1.0),UIColor.init(red: 44/255, green: 169/255, blue: 145/255, alpha: 1.0),UIColor.init(red: 223/255, green: 50/255, blue: 56/255, alpha: 1.0),UIColor.init(red: 220/255, green: 29/255, blue: 29/255, alpha: 1.0)]
    
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    var vochersArray = [[String:AnyObject]]()
    @IBOutlet weak var voucherBtn: UIButton!
    var delegatedVocherDic = [String:AnyObject]()
    @IBOutlet weak var packgBtn: UIButton!
    var isVocherSelected = false
    @IBOutlet weak var selectTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "BOOK A LESSON"
//      Do any additional setup after loading the view.
        refreshCollectionView()
        self.footerViewHeightConstraint.constant = 0
        if currentUserLogin == 4  {
//            getVochersOrPackages()
        }
        
        settingRighMenuBtn()
//      self.collectionView.contentInset = .init(top: 50, left: 0, bottom: 0, right: 0)
        fetchData()
        if #available(iOS 11, *) {
            self.collectionView.contentInset = .init(top: 50, left: 0, bottom: 0, right: 0)
        }else{
        }
        
        //mergingcode
        if isNewUrl{
            self.voucherBtn.setTitle("Select", for: .normal)
            self.selectTitle.text = "Select"
            
        }else{
        }

    }
    
    func fetchData()  {
        
        var academyId = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyId = "\(id)"
        }
        
        let refreshCont = RefreshingViewController()
        refreshCont.refreshData(acedmyId: academyId, onSuccess: { (data) in
            let data1 = data as! [[String:AnyObject]]
            if data1.count > 0 {
                let dic = data1[0]
                print(dic)
                DataManager.sharedInstance.updateAcademyDetails(dic as NSDictionary)
            }
        }) { (error) in
            self.showInternetError(error: error!)
        }

    }
    
    func settingRighMenuBtn()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(BookLessonVC.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let bound = scrollView.bounds
        let height = scrollView.contentSize.height
        let offset = scrollView.contentOffset
        let inset = scrollView.contentInset
        let y = offset.y + bound.size.height - inset.bottom
        
        if y > height - 50 {
            hideShowFooter(flage: false)
        }else{
            hideShowFooter(flage: true)
        }
       
    }
    
    func refreshCollectionView()  {
        
        var coachId = "0"
        if currentUserLogin == 4 {
        }else{
            if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
                coachId = "\(id)"
            }
        }
//blabla
        NetworkManager.performRequest(type:.get, method: "academy/GetProgramType/\(DataManager.sharedInstance.currentAcademy()!["AcademyID"] as AnyObject)/\(coachId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]: break
                
            default:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            }
            
            self.collectionData = object as! [[String : AnyObject]]
           
            if Utility.isiPhonX() || Utility.isiPhonXMAX() || Utility.isiPhonXR() {
                self.logicToShowVoucherFooter()
            }else{
            }
            
            if self.collectionData.count > 0 && self.collectionData.count < 5{
                self.hideShowFooter(flage: false)
            }
            self.collectionView.reloadData()
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func logicToShowVoucherFooter()  {
        
        if self.collectionData.count < 1{
            self.footerViewHeightConstraint.constant = 0
        }else if self.collectionData.count < 7{
            self.hideShowFooter(flage: false)
        }
    }
    
    //MARK:- UICOllectionview delegate and datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookLessonCollectionViewCell", for: indexPath) as! BookLessonCollectionViewCell
        cell.collectionView.backgroundColor = arrayOfColors[indexPath.row % arrayOfColors.count]
        if let title = collectionData[indexPath.row]["ProgramTypeName"] as? String{
            cell.collectionTitle.text = title
            if title == "Junior Programme" || title == "Juniors" || title == "Events" || title == "Test 1 Filter" {
                cell.collectionImage.image = UIImage(named: "juniorac")
            }else if  title == "Adult Programme" {
               
                 cell.collectionImage.image = UIImage(named: "adult")
            }else{
                cell.collectionImage.image = UIImage(named: "onetoone")
            }
        }
        return cell
    }

    func hideShowFooter(flage:Bool)  {
        
        return
        if flage {
            
            if currentUserLogin == 4  {
                UIView.animate(withDuration: 0.5, animations: {
                    self.footerViewHeightConstraint.constant = 0 // heightCon is the IBOutlet to the constraint
                    self.view.layoutIfNeeded()
                })
            }
        }else{
            if currentUserLogin == 4  {
                //mergingcode
                
                if isNewUrl{
                    if let redeemable = DataManager.sharedInstance.currentAcademy()?["isPackagesRedemable"] as? Bool,redeemable == true {
                        
                        UIView.animate(withDuration: 0.5, animations: {
                            self.footerViewHeightConstraint.constant = 100
                            self.view.layoutIfNeeded()
                        })
                        
                    }
                    
                }else{
                    UIView.animate(withDuration: 0.5, animations: {
                        self.footerViewHeightConstraint.constant = 100
                        self.view.layoutIfNeeded()
                    })
                }
            }

        }
    }
    
    // MARK: - UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return  CGSize(width:self.view.bounds.width/2+5 , height:self.view.bounds.width/2+5)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, -5, 0, -5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return  0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
   
        let defaults = UserDefaults.standard
        if let puttingLab = collectionData[indexPath.row]["IsPuttingLab"] as? Bool{
            //uncomment below if FDGS changes needed working perfectly
            if puttingLab {
                
                defaults.set("NO", forKey: "isStudio")
                showAlertViewForPuttingLab(tag: indexPath.row)
                
            }else{
        
                defaults.set("NO", forKey: "isPuttingLab")
                if let oneToOne = collectionData[indexPath.row]["IsOneToOneLesson"] as? Bool{
                    
                     var showPopUp = false
                    if oneToOne{
                        
                        var dataToPick = [String:AnyObject]()
                        if isNewUrl{
                            dataToPick =  collectionData[indexPath.row]
                        }else{
                            //popUpfunctionality
//                            dataToPick = collectionData[indexPath.row]
                             dataToPick =   DataManager.sharedInstance.currentAcademy() as! [String : AnyObject]
                            showPopUp = true
                        }
                        
                        if let fromAcdemyOneToOne = dataToPick["IsStudioEnable"] as? Bool{
                            
                            //popUpfunctionality
                           
                            if let popUp = dataToPick["IsStudioPopupEnable"] as? Bool{
                                showPopUp = popUp
                            }
//                            && showPopUp
                            if fromAcdemyOneToOne  && showPopUp {
                                showAlertView(tag: indexPath.row)
                            }else{
                                
                                let dic = collectionData[indexPath.row]
                                let contrloller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
                                if currentUserLogin == 4 {
                                    contrloller.vochersArray = vochersArray
                                    contrloller.programsData = self.collectionData
                                }
                                contrloller.selectedData = dic
                                let defaults = UserDefaults.standard
                                if fromAcdemyOneToOne{
                                    defaults.set("YES", forKey: "isStudio")
                                }else{
                                    defaults.set("NO", forKey: "isStudio")
                                }
                                self.navigationController?.pushViewController(contrloller, animated: true)
                            }
                            
                        }
                        
                    }else{
                        let dic = collectionData[indexPath.row]
                        let contrloller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseProgramTypeVC") as! ChooseProgramTypeVC
                        contrloller.selectedData = dic
                        if currentUserLogin == 4 {
                            contrloller.vochersArray = vochersArray
                            contrloller.programsData = self.collectionData
                        }
                        self.navigationController?.pushViewController(contrloller, animated: true)
                    }
                }
            }
            
        }

        
    }
    
    func showAlertView(tag:Int)  {
        
        var alertMessage = ""
        if currentTarget == "Jay Kelly Golf" {
            alertMessage = "Please select whether you would like your lesson in the Coaching Studio.  Please note all lessons after day light have to be in the Studio?"
        }else{
            alertMessage = "Is this Studio Lesson?"
        }
        
        let dic = collectionData[tag]
        let alertController = UIAlertController(title: "STUDIO CONFIRMATION", message: alertMessage, preferredStyle: .alert)
        let contrloller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
        if currentUserLogin == 4 {
            contrloller.vochersArray = vochersArray
            contrloller.programsData = self.collectionData
        }
        contrloller.selectedData = dic
        // Create the actions
              let defaults = UserDefaults.standard
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
      
            defaults.set("YES", forKey: "isStudio")
            
            self.navigationController?.pushViewController(contrloller, animated: true)
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            defaults.set("NO", forKey: "isStudio")
        self.navigationController?.pushViewController(contrloller, animated: true)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertViewForPuttingLab(tag:Int)  {
        
        let alertMessage = collectionData[tag]["Description"] as! String
        
        let dic = collectionData[tag]
        
        var titleOfAlert = ""
        if let alertTitle = collectionData[tag]["ProgramTypeName"] as? String{
            titleOfAlert = alertTitle
        }
        let alertController = UIAlertController(title: titleOfAlert, message: alertMessage , preferredStyle: .alert)
        let contrloller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
        if currentUserLogin == 4 {
            contrloller.vochersArray = vochersArray
            contrloller.programsData = self.collectionData
        }
        contrloller.selectedData = dic
        // Create the actions
        let defaults = UserDefaults.standard
        
        defaults.set("YES", forKey: "isPuttingLab")
       defaults.set("NO", forKey: "isStudio")

              
        self.navigationController?.pushViewController(contrloller, animated: true)
        return
        let okAction = UIAlertAction(title: "CONTINUE", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            defaults.set("YES", forKey: "isPuttingLab")
            
            
            self.navigationController?.pushViewController(contrloller, animated: true)
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            defaults.set("NO", forKey: "isPuttingLab")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    
    //getting Vouchers
    func getVochersOrPackages()  {
        
      var userID = ""
      if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
                userID = "\(userId)"
      }
        
        var academyId = ""
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            academyId = "\(id)"
        }

        //mergingcode
        var urlStr = ""
        if isNewUrl{
            urlStr = "Student/GetPlayerPackages?UserId=\(userID)"
        }else{
            urlStr =  "Booking/GetUserVoucherByAcademy?UserId=\(userID)&AcademyId=\(academyId)&RoleId=\(currentUserLogin!)"
        }
        
        NetworkManager.performRequest(type:.get, method: "\(urlStr)", parameter:nil, view:self.appDelegate.window, onSuccess: { (object) in
            print(object!)
            switch object {
                
            case _ as NSNull:
            DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [[String:AnyObject]]: break
            default:
                return
            }
            
            self.vochersArray = object as! [[String : AnyObject]]
            if self.vochersArray.count > 0{
                //mergingcode
                if isNewUrl{
                    self.voucherBtn.setTitle("Select Package", for: .normal)
                }else{
                    self.voucherBtn.setTitle("Select Voucher", for: .normal)
                }
                
            }else{
                //mergingcode
                if isNewUrl{
                    self.voucherBtn.setTitle("No Package Available", for: .normal)
                    
                }else{
                    self.voucherBtn.setTitle("No Voucher Available", for: .normal)
                    
                }
                
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func isVoucherRedemable() -> Bool {
        
        if let redeemStatus = delegatedVocherDic["IsRedeemAble"] as? Bool ,redeemStatus == true{
            
            return true
        }else{
          
            return false
        }
        
    }
    
    @IBAction func redeemBtnAction(_ sender: UIButton) {
        
        if delegatedVocherDic.count > 0 {
            
            
            //mergingcode
            if isNewUrl{
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
                controller.selectedData = delegatedVocherDic//selectedData
                controller.redeemedData = delegatedVocherDic
                controller.isRedeemedVoucher = true
                self.navigationController?.pushViewController(controller, animated: true)
                
                return
            }
            
            if isVoucherRedemable(){
            
            }else{
                    let alertController = UIAlertController(title: "Alert", message: "Selected Voucher is not Redeemable.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                return
            }
            
            var isOneToOne = false
            var selectedData = [String:AnyObject]()
            for index in collectionData {
                
                if let selectedVocherTyp = delegatedVocherDic["ProgramTypeId"] as? Int{
                    if selectedVocherTyp == index["ProgramTypeId"] as? Int {
                        selectedData = index
                        if let isOneToone =  index["IsOneToOneLesson"] as? Bool{
                            if isOneToone {
                                isOneToOne = true
                                break
                            }
                        }
                    }
                }
            }

            if selectedData.count < 1 {
                
                return
            }
            
            if isOneToOne {
                
                if let durations = delegatedVocherDic["Details"] as? String {
                    let components = durations.components(separatedBy: ")")
                    print(components)
                    if components.count > 2 {
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseDurationVC") as! ChooseDurationVC
                        controller.selectedData = selectedData
                        controller.redeemedData = delegatedVocherDic
                        controller.isRedeemedVoucher = true
                        self.navigationController?.pushViewController(controller, animated: true)
                    }else{
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCoachVC") as! ChooseCoachVC
                        controller.selectedData = selectedData
                        controller.previousData = selectedData
                        controller.redeemedData = delegatedVocherDic
                          controller.isRedeemedVoucher = true
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }

            }else{
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseProgramTypeVC") as! ChooseProgramTypeVC
                controller.selectedData = selectedData
                controller.redeemedData = delegatedVocherDic
                controller.isRedeemedVoucher = true
                self.navigationController?.pushViewController(controller, animated: true)
            }
         
        }else{
            
            //mergingcode
            var redeemType = "Please Select Vocher First!"
            if isNewUrl{
                redeemType = "Please Select Package First!"
            }else{
                redeemType = "Please Select Vocher First!"
            }
            let alertController = UIAlertController(title: "Alert", message: redeemType, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                UIAlertAction in
             
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func voucherBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VoucherViewController") as! VoucherViewController
        controller.originaldata = vochersArray
        controller.delegate = self
        //mergingcode
        if isNewUrl{
            isVocherSelected = false
            controller.forPackage = true
        }else{
            isVocherSelected = true
            controller.forPackage = false
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func packgBtnAction(_ sender: UIButton) {
        isVocherSelected = false

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VoucherViewController") as! VoucherViewController
        controller.originaldata = vochersArray
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
}

extension BookLessonVC : VoucherDelegate{
    
    func seletedVoucherData(dic: NSDictionary) {
        print(dic)
        
        if let pin = dic["VoucherPin"] as? String{
            if isVocherSelected{
                self.voucherBtn.setTitle(pin, for: .normal)
                self.packgBtn.setTitle("Package", for: .normal)

            }else{
                self.voucherBtn.setTitle("Vocher", for: .normal)
                self.packgBtn.setTitle(pin, for: .normal)
            }
        }
        
        if isVocherSelected{
        }else{
            if let pin = dic["Pin"] as? String{
                self.voucherBtn.setTitle(pin, for: .normal)
            }
        }
       delegatedVocherDic = dic as! [String : AnyObject]
    }
    
    func seletedPackageData(dic: NSDictionary) {
    }
    
}

