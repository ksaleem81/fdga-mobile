//
//  ImagePreview.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 21/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
//UIScrollViewDelegate
class ImagePreview: UIView {
//     var scrollView: UIScrollView!
    
    @IBOutlet var mainImageView: UIImageView!
    var imagePath: String?
    
    
   @objc static func loadFromXib() -> ImagePreview {
    
        return Bundle.main.loadNibNamed("ImagePreview", owner: self, options: nil)?.first as! ImagePreview
    }
    
    @objc func addInView(parentView: UIView) {
      
      
        self.frame = parentView.frame
        parentView.addSubview(self)
        if (imagePath != nil) {
            var fileUrl = imagePath!.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            fileUrl = baseUrlForVideo + fileUrl
            mainImageView.sd_setImage(with: URL.init(string: fileUrl), completed: nil)
            layoutSubviews()
        }
        
//                scrollView = UIScrollView.init()
//                scrollView.delegate = self
//                scrollView.bounds = self.bounds
//
//                self.scrollView.addSubview(self)
//                scrollView.minimumZoomScale = 1.0
//                scrollView.maximumZoomScale = 6.0
    }
    
    

    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//       
//        return mainImageView
//    }

    
    @IBAction func removeAction() {
        self.removeFromSuperview()
    }
}
