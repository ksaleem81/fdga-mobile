//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu

protocol SelectedContactDelegate {
     func seletedUserData(dic: NSDictionary,selectedUserId:String)
}

class SelectContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var data:[[String:AnyObject]] = [[String:AnyObject]]()
    var originaldata:[[String:AnyObject]] = [[String:AnyObject]]()
    var delegate : SelectedContactDelegate?
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currentUserLogin == 4 {
            self.title = "COACH SELECTION"
        }else{
            self.title = "STUDENT SELECTION"
        }
        
        searchFld.inputAccessoryView = addToolBar()
        data = originaldata
        searchFld.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        settingRighMenuBtn()
        tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tabBarController?.navigationController?.isNavigationBarHidden = true

    }
  
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(SelectContactViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
   
    @objc func rightBarBtnAction() {
        
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            data = originaldata
            self.tableView.reloadData()
            return
        }else{
        }
        
        let searchPredicate = NSPredicate(format: "Text CONTAINS[C] %@", searchFld.text!)
        let array = (originaldata as NSArray).filtered(using: searchPredicate)
        data = array as! [[String:AnyObject]]
        print ("filterd = \(data)")
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar() -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SelectContactViewController.donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar
    }
    
    
    @objc func donePressed(){
     
        print(searchFld.text!)
        self.searchFld.resignFirstResponder()
        if searchFld.text!.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased() != "" {
            // string contains non-whitespace characters
        }else{
            return
        }
       
     print(data.count)
        searchFld.resignFirstResponder()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
//        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectContactTableViewCell", for: indexPath) as! SelectContactTableViewCell
        if let title = data[indexPath.row]["Text"] as? String{
           cell.companyLbl.text = title
        }
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action:#selector(selectCheckBtn(sender:)) , for:.touchUpInside)
               return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
    @objc func selectCheckBtn(sender:UIButton)  {
        
        let indexPath =  IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! SelectContactTableViewCell
        
        if cell.radioBtn.isSelected {
            
            cell.radioBtn.isSelected = false
        }else{
            
            self.delegate?.seletedUserData(dic:data[sender.tag] as NSDictionary,selectedUserId:"")
                _ = self.navigationController?.popViewController(animated: true)
            
        }
    }
    

   

}

