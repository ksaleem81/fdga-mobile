//
//  OverlaysViewController.m
//  Orbis
//
//  Created by Nasir Mehmood on 08/01/2014.
//
//

#import "OverlaysViewController.h"
#import "OverlayCell.h"
#import "OrbisMediaGallery.h"

@interface OverlaysViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) UIViewController *presentingController;

@end

@implementation OverlaysViewController

@synthesize overlaysArray, thumbImagePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil thumbnailImagePath:(NSString*)thumbnailImagePath overlaysArray:(NSArray*)overlysarray
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSString *mediaFolderPath=[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath];
        self.thumbImagePath=[mediaFolderPath stringByAppendingPathComponent:thumbnailImagePath.lastPathComponent];
        self.overlaysArray=overlysarray;
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil thumbnailImagePath:(NSString*)thumbnailImagePath overlaysArray:(NSArray*)overlysarray presentingController:(UIViewController*)presentngController
{
    if([self initWithNibName:nibNameOrNil bundle:nibBundleOrNil thumbnailImagePath:thumbnailImagePath overlaysArray:overlysarray])
    {
        self.presentingController=presentngController;
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   svc=(ShapeViewController*)self.presentingController;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"OverlayCell" bundle:nil] forCellWithReuseIdentifier:@"OverlayCell"];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if(overlaysArray)
        return overlaysArray.count;
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {

    return 1.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OverlayCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"OverlayCell" forIndexPath:indexPath];
    if(!cell)
    {
        NSLog(@"error");
    }
    
    NSString *mediaFolderPath=[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath];
    UIImage *thumbImage=[UIImage imageWithContentsOfFile:thumbImagePath];
    cell.thumbnailImageView.image=thumbImage;
    
    NSString *overlayPath=[mediaFolderPath stringByAppendingPathComponent:[[overlaysArray objectAtIndex:indexPath.row] lastPathComponent]];
    UIImage *overlayImage=[UIImage imageWithContentsOfFile:overlayPath];
    cell.overlayImageView.image=overlayImage;
    
    return cell;
}

// 4
/*
 - (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
    return [[UICollectionReusableView alloc] init];
 }
 */


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath: %@", indexPath);
    
    NSString *mediaFolderPath=[[OrbisMediaGallery sharedGallery] getMediaGalleryFolderPath];
    NSString *overlayPath=[mediaFolderPath stringByAppendingPathComponent:[[overlaysArray objectAtIndex:indexPath.row] lastPathComponent]];
    UIImage *overlayImage=[UIImage imageWithContentsOfFile:overlayPath];
    UIDeviceOrientation orientaiton =  [[UIDevice currentDevice]orientation];
    
//    UIViewController *v=[self presentingViewController];
 
    if([svc respondsToSelector:@selector(drawShapeOfImage:)])
        [svc drawShapeOfImage:overlayImage];
    
    if (  orientaiton !=  self.dOrientation) {
        
        svc.rotationVal = 1;
    }
    
    if([svc respondsToSelector:@selector(reloadView)])
        [svc reloadView];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

- (IBAction)cancelButtonClicked:(id)sender
{
    if([svc respondsToSelector:@selector(reloadView)])
        svc.rotationVal = 0;
        [svc reloadView];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark -  rotation delegates

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
 svc.rotationVal = 1;
    

}
*/

@end
