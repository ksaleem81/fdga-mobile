//
//  TNBackgroundSessionDelegate.h
//  Telenotes2
//
//  Created by Stephen Lynn on 3/12/15.
//  Copyright (c) 2015 TeleNotes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLBackgroundSessionConstants.h"

typedef void (^CompletionHandlerType)();

@interface BLBackgroundSessionDelegate : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>


+ (NSURLSession *)createBackgroundURLSessionWithId:(NSString *)identifier shouldWifiSyncOnly:(BOOL)shouldWifiSyncOnly delegate:(id <NSURLSessionDelegate>)delegate;

- (void)registerCompletionHandler:(void (^)())completionHandler forIdentifier:(NSString *)identifier;


@end
